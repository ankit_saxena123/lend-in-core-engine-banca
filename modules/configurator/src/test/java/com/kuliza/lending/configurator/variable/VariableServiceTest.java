package com.kuliza.lending.configurator.variable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewVariable;
import com.kuliza.lending.configurator.pojo.SubmitNewVariables;
import com.kuliza.lending.configurator.pojo.UpdateVariable;
import com.kuliza.lending.configurator.service.variableservice.VariableServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.VariableDataValidator;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
@SuppressFBWarnings("SIC_INNER_SHOULD_BE_STATIC_ANON")
public class VariableServiceTest {

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private RuleDao ruleDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private ExpressionDao expressionDao;

	@Mock
	private VariableDataValidator variableDataValidator;

	@InjectMocks
	private VariableServices variableServices;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Variable v1;
	private Variable v2;
	private Variable v3;
	private Variable v4;
	private Group g1;
	private Rule r1;
	private Rule r2;
	private Expression e1;
	private String userId;
	private String productId;
	private String variableId;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.variableId = "1";
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.v2 = new Variable(2, "Gender", "String", "Journey", "Primitive", "Gender Of Customer", null, p1);
		this.v3 = new Variable(3, "Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", null, p1);
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score",
				"Age + Gender + Salaried", p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">=30 && <=50", "5", 2.0, true, this.v1, this.g1);
		this.e1 = new Expression(1, "Age + 2*Age", true, false, null, this.g1);

	}

	// Test For Get All Variables
	@Test
	public void getAllVariables() throws Exception {
		when(variableDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.v1, this.v2, this.v3, this.v4));
		GenericAPIResponse response = variableServices.getAllVariables(this.productId);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.v1, this.v2, this.v3, this.v4), response.getData());
	}

	// Test For Get Single Variable
	@Test
	public void getSingleVariable() throws Exception {
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v3);
		GenericAPIResponse response = variableServices.getSingleVariable(this.variableId);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(this.v3, response.getData());
	}

	// Test For Delete Variable
	@Test
	public void deleteVariable() throws Exception {
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v3);
		GenericAPIResponse response = variableServices.deleteVariable(this.variableId);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Constants.VARIABLE_DELETED_SUCCESS_MESSAGE, response.getData());
	}

	// Test For Update Variable
	@Test
	public void updateVariable1() throws Exception {
		this.variableId = "1";
		String newSource = "JOCATA";
		String newDescription = "Age From Jocata";
		UpdateVariable input = new UpdateVariable(newSource, newDescription, "");
		input.setVariableId(this.variableId);
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		GenericAPIResponse response = variableServices.updateVariable(input);
		Variable responseVariable = (Variable) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(newSource, responseVariable.getSource());
		assertEquals(newDescription, responseVariable.getDescription());
	}

	// Test For Update Variable When Expression Is Given
	@Test
	public void updateVariable2() throws Exception {
		this.variableId = "4";
		String newSource = "Score";
		String newDescription = "Calculated";
		String expressionString = " OR ( TRUE , TRUE )";
		UpdateVariable input = new UpdateVariable(newSource, newDescription, expressionString);
		input.setVariableId(this.variableId);
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v4);
		GenericAPIResponse response = variableServices.updateVariable(input);
		Variable responseVariable = (Variable) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(newSource, responseVariable.getSource());
		assertEquals(newDescription, responseVariable.getDescription());
		assertEquals(expressionString.replaceAll("\\s", ""), responseVariable.getExpression().replaceAll("\\s", ""));
	}

	// Test For Create Variable Primitive
	@Test
	public void createVariable1() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		input.setProductId(this.productId);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = variableServices.createNewVariables(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
	}

	// Test For Create Variable Derived
	@Test
	public void createVariable2() throws Exception {
		NewVariable nv1 = new NewVariable("Score", "Numerical", "Calculated", "Derived", "Calculate Score",
				"Age + Gender + Salaried");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1));
		input.setProductId(this.productId);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		GenericAPIResponse response = variableServices.createNewVariables(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
	}

	// Test For Validate Submit Variable When No Error Occurs in Validation
	@Test
	public void validateSubmitVariable1() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		doNothing().when(variableDataValidator).validateVariable(any(NewVariable.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		GenericAPIResponse response = variableServices.validateSubmitVariables(input, result, this.userId,
				this.productId);
		assertNull(response);
	}

	// Test For Validate Submit Variable When Field Level Error Occurs
	@Test
	public void validateSubmitVariable2() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		doNothing().when(variableDataValidator).validateVariable(any(NewVariable.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		result.rejectValue("newVariablesList", "newVariablesList.invalid", "Variable List cannot be empty");
		GenericAPIResponse response = variableServices.validateSubmitVariables(input, result, this.userId,
				this.productId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Submit Variable When Same Name Variables Submitted
	@Test
	public void validateSubmitVariable3() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Age", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		doNothing().when(variableDataValidator).validateVariable(any(NewVariable.class), any(BindingResult.class));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		GenericAPIResponse response = variableServices.validateSubmitVariables(input, result, this.userId,
				this.productId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Submit Variable When Business Login Error Occured
	@Test
	public void validateSubmitVariable4() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1));
		when(variableDao.findByNameAndProductIdAndIsDeleted(nv1.getVariableName(), Long.parseLong(this.productId),
				false)).thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {

				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("variableName", "variableName.invalid", Constants.VARIABLE_NAME_TAKEN_MESSAGE);
				return result;
			}
		}).when(variableDataValidator).validateVariable(any(NewVariable.class), any(BindingResult.class));
		GenericAPIResponse response = variableServices.validateSubmitVariables(input, result, this.userId,
				this.productId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Delete Variable When Variable Belongs To Product And Product Is
	// In Edit State And Variable Not Used Anywhere
	@Test
	public void validateDeleteVariable1() throws Exception {
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score", "ID_2 + ID_3", p1);
		this.r1 = new Rule(1, "==male", "5", 2.0, true, this.v2, this.g1);
		this.r2 = new Rule(1, "==female", "4", 2.0, true, this.v2, this.g1);
		this.e1 = new Expression(1, "ID_2 + 2 * ID_3", true, false, null, this.g1);
		String groupId = "1";
		this.p1.setStatus(0);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(true);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(Long.parseLong(this.productId), "Derived", false))
				.thenReturn(Arrays.asList(this.v4));
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		when(ruleDao.findByGroupIdAndIsDeleted(Long.parseLong(groupId), false))
				.thenReturn(Arrays.asList(this.r1, this.r2));
		GenericAPIResponse response = variableServices.validateDeleteVariable(this.userId, this.productId,
				this.variableId);
		assertNull(response);
	}

	// Test For Delete Variable When Variable Doesn't Belongs To Product
	@Test
	public void validateDeleteVariable2() throws Exception {
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(false);
		GenericAPIResponse response = variableServices.validateDeleteVariable(this.userId, this.productId,
				this.variableId);
		assertEquals(Constants.INVALID_PRODUCT_VARIABLE_ID_MESSAGE, response.getData());
	}

	// Test For Delete Variable When Variable Belongs To Product But Product Is
	// In Deployed/Undeployed State
	@Test
	public void validateDeleteVariable3() throws Exception {
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score", "ID_2 + ID_3", p1);
		this.r1 = new Rule(1, "==male", "5", 2.0, true, this.v2, this.g1);
		this.r2 = new Rule(1, "==female", "4", 2.0, true, this.v2, this.g1);
		this.e1 = new Expression(1, "ID_2 + 2 * ID_3", true, false, null, this.g1);
		this.p1.setStatus(2);
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(true);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = variableServices.validateDeleteVariable(this.userId, this.productId,
				this.variableId);
		assertEquals(Constants.PRODUCT_NOT_EDITABLE_MESSAGE, response.getData());
	}

	// Test For Delete Variable When Variable Belongs To Product And Product Is
	// In Edit State And Variable Is Used In Group Expression And Rules
	// And Variable Expression
	@Test
	public void validateDeleteVariable4() throws Exception {
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score", "ID_1 + ID_2", p1);
		this.r1 = new Rule(1, "==male", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(1, "==female", "4", 2.0, true, this.v1, this.g1);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_3", true, false, null, this.g1);
		String groupId = "1";
		this.p1.setStatus(0);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(true);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(Long.parseLong(this.productId), "Derived", false))
				.thenReturn(Arrays.asList(this.v4));
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		when(ruleDao.findByGroupIdAndIsDeleted(Long.parseLong(groupId), false))
				.thenReturn(Arrays.asList(this.r1, this.r2));
		GenericAPIResponse response = variableServices.validateDeleteVariable(this.userId, this.productId,
				this.variableId);
		assertEquals(Constants.REQUEST_FAILED_CODE, response.getStatus());
		assertNotNull(response.getData());
	}
}
