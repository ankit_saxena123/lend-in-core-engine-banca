package com.kuliza.lending.configurator.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewUser;
import com.kuliza.lending.configurator.utils.Constants;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreditEngineIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

//	@Autowired
//	private UserTokenDao userTokenDao;

	@Autowired
	private ObjectMapper objectMapper;

	private String token;

	// Create New User When No User Exists Of Same UserName
	@Test
	@SuppressWarnings("unchecked")
	@SuppressFBWarnings({ "NM_METHOD_NAMING_CONVENTION" })
	public void Alogin() throws Exception {
		SubmitNewUser newUser = new SubmitNewUser("Arpit Agrawal", "abcdef12345", "Kuliza Technologies",
				"arpit.agrawal");
		GenericAPIResponse response = restTemplate.postForObject("/register", newUser, GenericAPIResponse.class);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		Map<String, Object> data = (Map<String, Object>) response.getData();
		Map<String, Object> sortedKeysMap = new TreeMap<>();
		sortedKeysMap.putAll(data);
		assertEquals("[token, user]", sortedKeysMap.keySet().toString());
		Map<String, Object> user = (Map<String, Object>) data.get("user");
		sortedKeysMap = new TreeMap<>();
		sortedKeysMap.putAll(user);
		assertEquals("[companyName, name, userName]", sortedKeysMap.keySet().toString());
		assertEquals(newUser.getName(), user.get("name"));
		assertEquals(newUser.getUserName(), user.get("userName"));
		assertEquals(newUser.getCompanyName(), user.get("companyName"));
		assertNotNull(data.get("token"));
		this.token = data.get("token").toString();
	}

	// Create New User When User Exists Of Same UserName
	@Test
	@SuppressWarnings("unchecked")
	@SuppressFBWarnings({ "NM_METHOD_NAMING_CONVENTION" })
	public void Blogin() throws Exception {
		SubmitNewUser newUser = new SubmitNewUser("Arpit Agrawal", "abcdef12345", "Kuliza Technologies",
				"arpit.agrawal");
		GenericAPIResponse response = restTemplate.postForObject("/register", newUser, GenericAPIResponse.class);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
		assertEquals(Constants.FAILURE_MESSAGE, response.getMessage());
		Map<String, Object> data = (Map<String, Object>) response.getData();
		assertEquals("[errors]", data.keySet().toString());
		List<Map<String, Object>> errors = (List<Map<String, Object>>) data.get("errors");
		assertEquals(1, errors.size());
		Map<String, Object> error = (Map<String, Object>) errors.get(0);
		Map<String, Object> sortedKeysMap = new TreeMap<>();
		sortedKeysMap.putAll(error);
		assertEquals("[field, message]", sortedKeysMap.keySet().toString());
		assertEquals(Constants.USER_NAME, error.get("field"));
		assertEquals(Constants.USERNAME_TAKEN_MESSAGE, error.get("message"));
	}
	
	
	// TODO: Integration Test, user Management with keycloak

	// Test For Creating Product Category
//	@Test
//	@SuppressFBWarnings({ "NM_METHOD_NAMING_CONVENTION" })
//	public void CCreateProductCategory() throws Exception {
//		if (this.token == null) {
//			this.token = userTokenDao.findByIdAndIsDeleted(1L, false).getToken();
//		}
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Authorization", "Bearer ".concat(this.token));
//		headers.add("Content-Type", "application/json");
//		String requestBody = "{ \"productCategoryName\" : \"ScoreCard\"}";
//		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
//		ResponseEntity<GenericAPIResponse> result = restTemplate.exchange("/admin/product-category", HttpMethod.POST,
//				entity, GenericAPIResponse.class);
//		JSONAssert.assertEquals("{\"data\":{\"id\":1,\"name\":\"ScoreCard\"},\"message\":\"SUCCESS\",\"status\":200}",
//				objectMapper.writeValueAsString(result.getBody()), false);
//	}

	// Test For Getting Product List When No Product Exists
//	@Test
//	@SuppressFBWarnings({ "NM_METHOD_NAMING_CONVENTION" })
//	public void DProductList() throws Exception {
//		if (this.token == null) {
//			this.token = userTokenDao.findByIdAndIsDeleted(1L, false).getToken();
//		}
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Authorization", "Bearer ".concat(this.token));
//		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
//		ResponseEntity<GenericAPIResponse> result = restTemplate.exchange("/api/product/list", HttpMethod.GET, entity,
//				GenericAPIResponse.class);
//		JSONAssert.assertEquals("{\"data\":[],\"message\":\"SUCCESS\",\"status\":200}",
//				objectMapper.writeValueAsString(result.getBody()), false);
//	}
//
//	// Test For Creating New Product
//	@Test
//	@SuppressWarnings("unchecked")
//	@SuppressFBWarnings({ "NM_METHOD_NAMING_CONVENTION" })
//	public void ECreateProduct() throws Exception {
//		if (this.token == null) {
//			this.token = userTokenDao.findByIdAndIsDeleted(1L, false).getToken();
//		}
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Authorization", "Bearer ".concat(this.token));
//		headers.add("Content-Type", "application/json");
//		String requestBody = "{ \"productName\" : \"HDFCBank\", \"templateProductId\" : \"\", \"productCategoryId\" : \"1\" }";
//		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
//		ResponseEntity<GenericAPIResponse> result = restTemplate.exchange("/api/product", HttpMethod.POST, entity,
//				GenericAPIResponse.class);
//		GenericAPIResponse response = result.getBody();
//		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
//		Map<String, Object> data = (Map<String, Object>) response.getData();
//		Map<String, Object> sortedKeysMap = new TreeMap<>();
//		sortedKeysMap.putAll(data);
//		assertEquals("[id, identifier, name, parentId, productCategory, status]", sortedKeysMap.keySet().toString());
//		assertEquals(1, data.get("id"));
//		assertEquals("HDFCBank", data.get("name"));
//		assertEquals(0, data.get("parentId"));
//		assertEquals(0, data.get("status"));
//		assertNotNull(data.get("identifier"));
//		Map<String, Object> productCategory = (Map<String, Object>) data.get("productCategory");
//		sortedKeysMap = new TreeMap<>();
//		sortedKeysMap.putAll(productCategory);
//		assertEquals("[id, name]", sortedKeysMap.keySet().toString());
//		assertEquals(1, productCategory.get("id"));
//		assertEquals("ScoreCard", productCategory.get("name"));
//	}

}
