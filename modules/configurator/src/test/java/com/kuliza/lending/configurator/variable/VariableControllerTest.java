package com.kuliza.lending.configurator.variable;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.kuliza.lending.configurator.controllers.VariableControllers;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewVariable;
import com.kuliza.lending.configurator.pojo.SubmitNewVariables;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.pojo.UpdateVariable;
import com.kuliza.lending.configurator.service.variableservice.VariableServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.VariableDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebMvcTest(VariableControllers.class)
public class VariableControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;

//	@MockBean
//	private UserTokenDao userTokenDao;

	@MockBean
	private VariableDao variableDao;

	@MockBean
	private GroupDao groupDao;

	private MockMvc mvc;

	@MockBean
	private VariableServices variableServices;

	@MockBean
	private VariableDataValidator variableDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Variable v1;
	private Variable v2;
	private Variable v3;
	private Variable v4;
	private String userId;
	private String productId;
	private String variableId;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.userId = "1";
		this.productId = "2";
		this.variableId = "3";
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.v2 = new Variable(2, "Gender", "String", "Journey", "Primitive", "Gender Of Customer", null, p1);
		this.v3 = new Variable(3, "Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", null, p1);
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score",
				"Age + Gender + Salaried", p1);

	}

	// Test For Get All Variables When Validator Return True
	@Test
	public void getAllVariables1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(v1, v2, v3, v4));
		when(variableDataValidator.validateGetAllVariables(this.userId, this.productId)).thenReturn(true);
		when(variableServices.getAllVariables(this.productId)).thenReturn(response);
		mvc.perform(get("/api/product/{productId}/variables", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Get All Variables When Validator Return False
	@Test
	public void getAllVariables2() throws Exception {
		when(variableDataValidator.validateGetAllVariables(this.userId, this.productId)).thenReturn(false);
		mvc.perform(get("/api/product/{productId}/variables", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));

	}

	// Test For Get All Variables When Service Throws Exception
	@Test
	public void getAllVariables3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(variableDataValidator.validateGetAllVariables(this.userId, this.productId)).thenReturn(true);
		when(variableServices.getAllVariables(this.productId)).thenThrow(new Exception());
		when(variableServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(get("/api/product/{productId}/variables", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Get Single Variable When Validator Return True
	@Test
	public void getSingleVariables1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				v3);
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(true);
		when(variableServices.getSingleVariable(this.variableId)).thenReturn(response);
		mvc.perform(get("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Get Single Variable When Validator Return False
	@Test
	public void getSingleVariables2() throws Exception {
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(false);
		mvc.perform(get("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));

	}

	// Test For Get Single Variable When Service Throws Exception
	@Test
	public void getSingleVariables3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId))
				.thenReturn(true);
		when(variableServices.getSingleVariable(this.variableId)).thenThrow(new Exception());
		when(variableServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(get("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Create Variables When Validator Return No Error
	@Test
	public void createNewVariables1() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", "");
		SubmitNewVariables newVariables = new SubmitNewVariables();
		newVariables.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(v1, v2, v3));
		when(variableServices.validateSubmitVariables(any(SubmitNewVariables.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId))).thenReturn(null);
		when(variableServices.createNewVariables(any(SubmitNewVariables.class))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/variables", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(newVariables)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Create Variables When Validator Returns Error
	@Test
	public void createNewVariables2() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Salaried", "Boolean", "Journey", "Primtive", "Is Customer Salaried?", "");
		SubmitNewVariables newVariables = new SubmitNewVariables();
		newVariables.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(variableServices.validateSubmitVariables(any(SubmitNewVariables.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/variables", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(newVariables)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Create Variables When Service Throws Exception
	@Test
	public void createNewVariables3() throws Exception {
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		NewVariable nv2 = new NewVariable("Gender", "String", "Journey", "Primitive", "Gender Of Customer", "");
		NewVariable nv3 = new NewVariable("Salaried", "Boolean", "Journey", "Primtive", "Is Customer Salaried?", "");
		SubmitNewVariables newVariables = new SubmitNewVariables();
		newVariables.setNewVariablesList(Arrays.asList(nv1, nv2, nv3));
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(variableServices.validateSubmitVariables(any(SubmitNewVariables.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId))).thenReturn(null);
		when(variableServices.createNewVariables(any(SubmitNewVariables.class))).thenThrow(new Exception());
		when(variableServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(SubmitNewVariables.class))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/variables", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(newVariables)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Update Variable When Validator Returns No Error
	@Test
	public void updateVariable1() throws Exception {
		this.variableId = "1";
		String newSource = "PAN";
		String newDescription = "Age Of User From PAN";
		this.v1.setSource(newSource);
		this.v1.setDescription(newDescription);
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.v1);
		when(variableServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(variableServices.updateVariable(any(UpdateVariable.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateVariable(newSource, newDescription, ""))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)))
				.andExpect(jsonPath("$.data.source", is(newSource)))
				.andExpect(jsonPath("$.data.description", is(newDescription)));
	}

	// Test For Update Variable When Validator Returns Error
	@Test
	public void updateVariable2() throws Exception {
		this.variableId = "1";
		String newSource = "PAN";
		String newDescription = "Age Of User From PAN";
		this.v1.setSource(newSource);
		this.v1.setDescription(newDescription);
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(variableServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateVariable(newSource, newDescription, ""))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Update Variable When Service Throws Exception
	@Test
	public void updateVariable3() throws Exception {
		this.variableId = "1";
		String newSource = "PAN";
		String newDescription = "Age Of User From PAN";
		this.v1.setSource(newSource);
		this.v1.setDescription(newDescription);
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(variableServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(variableServices.updateVariable(any(UpdateVariable.class))).thenThrow(new Exception());
		when(variableServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(UpdateProduct.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateVariable(newSource, newDescription, ""))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Delete Variable When Validator Returns No Error
	@Test
	public void deleteVariable1() throws Exception {
		this.variableId = "1";
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.VARIABLE_DELETED_SUCCESS_MESSAGE);
		when(variableServices.validateDeleteVariable(this.userId, this.productId, this.variableId)).thenReturn(null);
		when(variableServices.deleteVariable(this.variableId)).thenReturn(response);
		mvc.perform(delete("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Delete Variable When Validator Returns Error
	@Test
	public void deleteVariable2() throws Exception {
		this.variableId = "4";
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INVALID_PRODUCT_VARIABLE_ID_MESSAGE);
		when(variableServices.validateDeleteVariable(this.userId, this.productId, this.variableId))
				.thenReturn(response);
		mvc.perform(delete("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Delete Variable When Service Throws Exception
	@Test
	public void deleteVariable3() throws Exception {
		this.variableId = "1";
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(variableServices.validateDeleteVariable(this.userId, this.productId, this.variableId)).thenReturn(null);
		when(variableServices.deleteVariable(this.variableId)).thenThrow(new Exception());
		when(variableServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(delete("/api/product/{productId}/variables/{variableId}", this.productId, this.variableId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

}
