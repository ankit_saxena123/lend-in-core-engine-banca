package com.kuliza.lending.configurator.product;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieContainer;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.ProductDeployment;
import com.kuliza.lending.configurator.models.ProductDeploymentDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.UserDao;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.KieContainerBean;
import com.kuliza.lending.configurator.pojo.SubmitNewProduct;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.service.productservice.ProductServices;
import com.kuliza.lending.configurator.utils.Constants;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class ProductServiceTest {

	@Mock
	private ObjectMapper objectMapper;

	@Mock
	private UserDao userDao;

	@Mock
	private RuleDao ruleDao;

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private ExpressionDao expressionDao;

	@Mock
	private ProductCategoryDao productCategoryDao;

	@Mock
	private ProductDeploymentDao productDeploymentDao;

	@Mock
	private KieContainer kieContainer;

	@Mock
	private KieContainerBean kieContainerBean;

	@InjectMocks
	private ProductServices productServices = new ProductServices();

	private ProductCategory pc1;
	private ProductCategory pc2;
	private User user;
	private Product p1;
	private Product p2;
	private Variable v1;
	private Variable v2;
	private Variable v3;
	private Variable v4;
	private Group g1;
	private Rule r1;
	private Rule r2;
	private Expression e1;
	private Expression e2;

	@Before
	public void init() {
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.pc2 = new ProductCategory(2, "Eligibility");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.p2 = new Product(2, "AXIS_Bank_V2", 1, 2, "abdef", this.user, this.pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.v2 = new Variable(2, "Gender", "String", "Journey", "Primitive", "Gender Of Customer", null, p1);
		this.v3 = new Variable(3, "Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", null, p1);
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score",
				"ID_1 + ID_1 + ID_1", p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(2, "> 50", "4", 2.0, true, this.v1, this.g1);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_1", true, false, null, this.g1);
		this.e2 = new Expression(2, "AND ( ID_1 , TRUE )", false, true, this.p1, null);
	}

	// Test for get Product Categories
	@Test
	public void getProductCategories() throws Exception {
		when(productCategoryDao.findByIsDeleted(false)).thenReturn(Arrays.asList(this.pc1, this.pc2));
		GenericAPIResponse response = productServices.getProductCategories();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.pc1, this.pc2), response.getData());
	}

	// Test for get Products List When ProductCategoryId Is Null
	@Test
	public void getProductsList1() throws Exception {
		when(productDao.findByUserIdAndIsDeletedAndStatusGreaterThanEqual(1, false, 0))
				.thenReturn(Arrays.asList(this.p1, this.p2));
		GenericAPIResponse response = productServices.getProductsList("1", null, "0");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.p1, this.p2), response.getData());
	}

	// Test for get Products List When ProductCategoryId Is Not Null
	@Test
	public void getProductsList2() throws Exception {
		when(productDao.findByUserIdAndProductCategoryIdAndIsDeletedAndStatusGreaterThanEqual(1, 1, false, 0))
				.thenReturn(Arrays.asList(this.p1, this.p2));
		GenericAPIResponse response = productServices.getProductsList("1", "1", "0");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.p1, this.p2), response.getData());
	}

	// Test for get All Products
	@Test
	public void getAllProductsData() throws Exception {
		when(productDao.findByUserIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.p1, this.p2));
		GenericAPIResponse response = productServices.getAllProductsData("1");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Arrays.asList(this.p1, this.p2), response.getData());
	}

	// Test for get Single Product
	@Test
	public void getSingleProduct() throws Exception {
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		GenericAPIResponse response = productServices.getSingleProduct("2");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(this.p2, response.getData());
	}

	// Test for Create New Product
	@Test
	public void createNewProduct() throws Exception {
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank", "1", "");
		input.setUserId("1");
		when(productCategoryDao.findByIdAndIsDeleted(1, false)).thenReturn(this.pc1);
		GenericAPIResponse response = productServices.createNewProduct(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
	}

	// Test for Create New Product
	@Test
	public void editProduct() throws Exception {
		this.p1.setStatus(1);
		when(productDao.findByIdAndIsDeleted(1, false)).thenReturn(this.p1);
		GenericAPIResponse response = productServices.editProduct("1");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
	}

	// Test for Create New Product With Template
	@Test
	public void createNewProductWithTemplate() throws Exception {
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank_V2", "1", "1");
		input.setUserId("1");
		when(productCategoryDao.findByIdAndIsDeleted(1, false)).thenReturn(this.pc1);
		when(productDao.findByIdAndIsDeleted(0, false)).thenReturn(this.p2);
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(input.getTemplateProductId()), false))
				.thenReturn(Arrays.asList(this.g1));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(Long.parseLong(input.getTemplateProductId()),
				"Primitive", false)).thenReturn(Arrays.asList(this.v1, this.v2, this.v3));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(Long.parseLong(input.getTemplateProductId()), "Derived",
				false)).thenReturn(Arrays.asList(this.v4));
		when(expressionDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(Arrays.asList(this.e1));
		when(ruleDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(Arrays.asList(this.r1, this.r2));
		when(expressionDao.findByProductIdAndIsDeleted(Long.parseLong(input.getTemplateProductId()), false))
				.thenReturn(Arrays.asList(this.e2));
		when(variableDao.findByIdAndIsDeleted(1L, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2L, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3L, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong("0"), false)).thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong("0"), false)).thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong("0"), false))
				.thenReturn(this.v3);
		when(groupDao.findByIdAndIsDeleted(this.g1.getId(), false)).thenReturn(this.g1);
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong("0"), false))
				.thenReturn(this.g1);
		GenericAPIResponse response = productServices.createNewProduct(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
	}

	// Test for update product
	@Test
	public void updateProduct() throws Exception {
		UpdateProduct input = new UpdateProduct("HDFC_Bank");
		input.setProductId("2");
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		GenericAPIResponse response = productServices.updateProduct(input);
		Product responseProduct = (Product) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(input.getProductName(), responseProduct.getName());
	}

	// Test for clone a deployed/undeployed product
	@Test
	public void cloneProduct() throws Exception {
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		GenericAPIResponse response = productServices.cloneProduct("1", "2");
		Product responseProduct = (Product) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(true, responseProduct.getName().matches("^" + this.p2.getName() + "_[0-9]+$"));
	}

	// Test for Publish New Product
	@Test
	public void publishProduct1() throws Exception {
		this.p2 = new Product(2, "AXIS_Bank_V2", 1, 0, "abdef", this.user, this.pc1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(2, "== abc", "4", 2.0, true, this.v2, this.g1);
		Rule r3 = new Rule(3, "== true", "3", 4.0, true, this.v3, this.g1);
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		when(groupDao.findByProductId(Long.parseLong("2"))).thenReturn(Arrays.asList(this.g1));
		when(ruleDao.findByGroupIdAndIsDeletedAndIsActive(1, false, true))
				.thenReturn(Arrays.asList(this.r1, this.r2, r3));
		GenericAPIResponse response = productServices.publishProduct("2");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Constants.PRODUCT_PUBLISHED_SUCCESS_MESSAGE, response.getData());
	}

	// Test for Deploying Published Product with new identifier
	@Test
	public void deployProduct1() throws Exception {
		this.p2 = new Product(2, "AXIS_Bank", 1, 1, "abdef", this.user, this.pc1);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.p2.getIdentifier(), true, false))
				.thenReturn(null);
		when(userDao.findById(1)).thenReturn(this.user);
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		GenericAPIResponse response = productServices.deployProduct("1", "2");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Constants.PRODUCT_DEPLOYED_SUCCESS_MESSAGE, response.getData());
	}

	// Test for Deploying Published Product with already used identifier
	@Test
	public void deployProduct2() throws Exception {
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", this.user, this.pc1);
		this.p2 = new Product(2, "AXIS_Bank_V2", 1, 1, "abdef", this.user, this.pc1);
		ProductDeployment productDeployment = new ProductDeployment(1, true, this.p1.getIdentifier(), this.p1,
				this.user);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.p2.getIdentifier(), true, false))
				.thenReturn(productDeployment);
		when(userDao.findById(1)).thenReturn(this.user);
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		GenericAPIResponse response = productServices.deployProduct("1", "2");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(Constants.PRODUCT_DEPLOYED_SUCCESS_MESSAGE, response.getData());
	}

}
