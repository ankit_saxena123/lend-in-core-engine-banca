package com.kuliza.lending.configurator.admin;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProductCategory;
import com.kuliza.lending.configurator.service.adminservice.AdminServices;
import com.kuliza.lending.configurator.utils.Constants;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class AdminServiceTest {

	@Mock
	private ObjectMapper objectMapper;

	@Mock
	private ProductCategoryDao productCategoryDao;

	@InjectMocks
	private AdminServices adminServices;

	// Test For Create Product Category Service
	@Test
	public void createProductCategory() throws Exception {
		SubmitNewProductCategory input = new SubmitNewProductCategory("ScoreCard");
		GenericAPIResponse response = adminServices.createProductCategory(input);
		ProductCategory responseProductCategory = (ProductCategory) response.getData();
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
		assertEquals(input.getProductCategoryName(), responseProductCategory.getName());
	}

}
