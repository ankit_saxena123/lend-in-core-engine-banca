package com.kuliza.lending.configurator.product;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.kuliza.lending.configurator.controllers.ProductControllers;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProduct;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.service.productservice.ProductServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.ProductDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebMvcTest(ProductControllers.class)
public class ProductControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;
//
//	@MockBean
//	private UserTokenDao userTokenDao;

	private MockMvc mvc;

	@MockBean
	private ProductServices productServices;

	@MockBean
	private ProductDataValidator productDataValidator;

	private ProductCategory pc1;
	private ProductCategory pc2;
	private User user;
	private Product p1;
	private Product p2;
	private String userId;
	private String productId;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.userId = "1";
		this.productId = "2";
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.pc2 = new ProductCategory(2, "Eligibility");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.p2 = new Product(2, "AXIS_Bank_V2", 1, 0, "abdef", user, pc1);
	}

	// Test For Get Product Categories When Response status is 200
	@Test
	public void getProductCategories1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(pc1, pc2));
		when(productServices.getProductCategories()).thenReturn(response);
		mvc.perform(get("/api/product/categories").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Get Product Categories When Response status is not 200
	@Test
	public void getProductCategories2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				null);
		when(productServices.getProductCategories()).thenReturn(response);
		mvc.perform(get("/api/product/categories").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Get Product Categories When Service Throws Exception
	@Test
	public void getProductCategories3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productServices.getProductCategories()).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(get("/api/product/categories").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Get Products List When Status is 200
	@Test
	public void getProductsList1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(p1, p2));
		when(productServices.getProductsList(this.userId, null, "0")).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "groups", "variable"));
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product/list").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Get Products List When Status is not 200
	@Test
	public void getProductsList2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				null);
		when(productServices.getProductsList(this.userId, null, "0")).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "groups", "variable"));
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product/list").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Get Products List When Service Throws Exception
	@Test
	public void getProductsList3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productServices.getProductsList(this.userId, null, "0")).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "groups", "variable"));
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product/list").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Get Products List When Given Product Category Id Is Invalid
	@Test
	public void getProductsList4() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INVALID_PRODUCT_CATEGORY_ID_MESSAGE);
		when(productDataValidator.validateGetProductList("2a", "0")).thenReturn(response);
		mvc.perform(get("/api/product/list?productCategoryId=2a").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Get Products When Status is 200
	@Test
	public void getProducts1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(p1, p2));
		when(productServices.getAllProductsData(this.userId)).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter("productFilter", SimpleBeanPropertyFilter.serializeAll())
				.addFilter("groupFilter", SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Get Products When Status is not 200
	@Test
	public void getProducts2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				null);
		when(productServices.getAllProductsData(this.userId)).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter("productFilter", SimpleBeanPropertyFilter.serializeAll())
				.addFilter("groupFilter", SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Get Products When Service Throws Exception
	@Test
	public void getProducts3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productServices.getAllProductsData(this.userId)).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter("productFilter", SimpleBeanPropertyFilter.serializeAll())
				.addFilter("groupFilter", SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product").requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Get Single Product When Validator Return True
	@Test
	public void getSingleProduct1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				p2);
		when(productDataValidator.validateProductId(this.productId, this.userId)).thenReturn(true);
		when(productServices.getSingleProduct(this.productId)).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter("productFilter", SimpleBeanPropertyFilter.serializeAll())
				.addFilter("groupFilter", SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		mvc.perform(get("/api/product/{productId}", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Get Single Product When Validator Return False
	@Test
	public void getSingleProduct2() throws Exception {
		this.productId = "3";
		when(productDataValidator.validateProductId(this.productId, this.userId)).thenReturn(false);
		mvc.perform(get("/api/product/{productId}", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Get Single Product When Service Throws Exception
	@Test
	public void getSingleProduct3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productDataValidator.validateProductId(this.productId, this.userId)).thenReturn(true);
		when(productServices.getSingleProduct(this.productId)).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(get("/api/product/{productId}", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Create Product When Validator Return No Error
	@Test
	public void createNewProduct1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.p1);
		when(productServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(productServices.createNewProduct(any(SubmitNewProduct.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "variables", "groups"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(post("/api/product").requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewProduct("AXIS_Bank", "1", ""))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Create Product When Validator Returns Error
	@Test
	public void createNewProduct2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(productServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		mvc.perform(post("/api/product").requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewProduct("AXIS_Bank", "1", ""))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Create Product When Service Throws Exception
	@Test
	public void createNewProduct3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(productServices.createNewProduct(any(SubmitNewProduct.class))).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(SubmitNewProduct.class))).thenReturn(response);
		mvc.perform(post("/api/product").requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewProduct("AXIS_Bank", "1", ""))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Create Clone Of A Product When Validator Return No Error
	@Test
	public void createClone1() throws Exception {
		this.productId = "1";
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.p2);
		when(productDataValidator.validateCloneProduct(this.productId, this.userId)).thenReturn(null);
		when(productServices.cloneProduct(this.userId, this.productId)).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter",
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		mvc.perform(post("/api/product/{productId}", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Create Clone Of A Product When Validator Returns Error
	@Test
	public void createClone2() throws Exception {
		this.productId = "1";
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INVALID_PRODUCT_ID_MESSAGE);
		when(productDataValidator.validateCloneProduct(this.productId, this.userId)).thenReturn(response);
		mvc.perform(post("/api/product/{productId}", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Create Clone Of A Product When Service Throws Exception
	@Test
	public void createClone3() throws Exception {
		this.productId = "1";
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productDataValidator.validateCloneProduct(this.productId, this.userId)).thenReturn(null);
		when(productServices.cloneProduct(this.userId, this.productId)).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(post("/api/product/{productId}", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Update Product When Validator Returns No Error
	@Test
	public void updateProduct1() throws Exception {
		String newProductName = "Axis_bank";
		this.p1.setName(newProductName);
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.p1);
		when(productServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(productServices.updateProduct(any(UpdateProduct.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("productFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "variables", "groups"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateProduct(newProductName))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)))
				.andExpect(jsonPath("$.data.name", is(newProductName)));
	}

	// Test For Update Product When Validator Returns Error
	@Test
	public void updateProduct2() throws Exception {
		String newProductName = "Axis Bank";
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(productServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateProduct(newProductName))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Update Product When Service Throws Exception
	@Test
	public void updateProduct3() throws Exception {
		String newProductName = "Axis_Bank";
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(productServices.updateProduct(any(UpdateProduct.class))).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(UpdateProduct.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateProduct(newProductName))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Publish Product When Validator Returns No Error And Product Is
	// Not Deployed
	@Test
	public void publishProduct1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.PRODUCT_PUBLISHED_SUCCESS_MESSAGE);
		when(productDataValidator.validatePublishProduct(this.productId, this.userId)).thenReturn(null);
		when(productServices.publishProduct(this.productId)).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/publish", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Publish Product When Validator Returns Product Already Deployed
	// Error
	@Test
	public void publishProduct2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
				Constants.PRODUCT_ALREADY_DEPLOYED_MESSAGE);
		when(productDataValidator.validatePublishProduct(this.productId, this.userId)).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/publish", productId).requestAttr("userId", userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.REQUEST_FAILED_CODE)));
	}

	// Test For Publish Product When Service Throws Exception
	@Test
	public void publishProduct3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productDataValidator.validatePublishProduct(this.productId, this.userId)).thenReturn(null);
		when(productServices.publishProduct(this.productId)).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}/publish", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Edit Product When Validator Returns No Error
	@Test
	public void editProduct1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.PRODUCT_EDITABLE_SUCCESS_MESSAGE);
		when(productDataValidator.validateEditProduct(this.productId, this.userId)).thenReturn(null);
		when(productServices.editProduct(this.productId)).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/edit", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Edit Product When Validator Returns No Error
	@Test
	public void editProduct2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INVALID_PRODUCT_ID_MESSAGE);
		when(productDataValidator.validateEditProduct(this.productId, this.userId)).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/edit", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Edit Product When Service Throws Exception
	@Test
	public void editProduct3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productDataValidator.validateEditProduct(productId, userId)).thenReturn(null);
		when(productServices.editProduct(this.productId)).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(put("/api/product/{productId}/edit", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Deploy Product When Validator Returns No Error And Product Is
	// Not Deployed or Undeployed
	@Test
	public void deployProduct1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.PRODUCT_DEPLOYED_SUCCESS_MESSAGE);
		when(productDataValidator.validateDeployProduct(this.productId, this.userId)).thenReturn(null);
		when(productServices.deployProduct(this.userId, this.productId)).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/deploy", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Deploy Product When Validator Returns Error Product Is
	// Not Published
	@Test
	public void deployProduct2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
				Constants.PRODUCT_NOT_PUBLISHED_MESSAGE);
		when(productDataValidator.validateDeployProduct(productId, userId)).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/deploy", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.REQUEST_FAILED_CODE)))
				.andExpect(jsonPath("$.data", is(Constants.PRODUCT_NOT_PUBLISHED_MESSAGE)));
	}

	// Test For Deploy Product When Service Throws Exception
	@Test
	public void deployProduct3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(productDataValidator.validateDeployProduct(productId, userId)).thenReturn(null);
		when(productServices.deployProduct(this.userId, this.productId)).thenThrow(new Exception());
		when(productServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(put("/api/product/{productId}/deploy", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

}
