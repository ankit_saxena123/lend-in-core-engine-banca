package com.kuliza.lending.configurator.rule;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.controllers.RulesControllers;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewRule;
import com.kuliza.lending.configurator.pojo.ResponseGetRule;
import com.kuliza.lending.configurator.pojo.RuleDef;
import com.kuliza.lending.configurator.pojo.SubmitNewRules;
import com.kuliza.lending.configurator.service.ruleservice.RuleServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.RuleDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebMvcTest(RulesControllers.class)
public class RuleControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@MockBean
	private RuleServices ruleServices;

	@MockBean
	private RuleDataValidator ruleDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Variable v1;
	private Group g1;
	private Rule r1;
	private Rule r2;
	private String userId;
	private String productId;
	private String groupId;
	private RuleDef r1RuleDef;
	private RuleDef r2RuleDef;
	private NewRule newRule;
	private SubmitNewRules submitNewRules;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(1, "> 50", "4", 2.0, true, this.v1, this.g1);
		this.userId = "1";
		this.productId = "1";
		this.groupId = "2";
		this.r1RuleDef = new RuleDef("", ">=", "<=", "30", "50", "5");
		this.r2RuleDef = new RuleDef("", ">", "", "50", "", "4");
		this.newRule = new NewRule(Arrays.asList(r1RuleDef, r2RuleDef), "true", "2.0", "Age");
		this.submitNewRules = new SubmitNewRules(Arrays.asList(newRule));
	}

	// Test For Submit New Rules When No Validation Error Occurs
	@Test
	public void ruleSubmitOrUpdate1() throws Exception {
		Set<Rule> savedRules = new HashSet<Rule>();
		savedRules.add(this.r1);
		savedRules.add(this.r2);
		ResponseGetRule allSavedRules = new ResponseGetRule(savedRules);
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				allSavedRules);
		when(ruleServices.validateSubmitRules(any(SubmitNewRules.class), any(BindingResult.class), eq(this.userId),
				eq(this.productId), eq(this.groupId))).thenReturn(null);
		when(ruleServices.createNewRules(eq(this.productId), eq(this.groupId), any(SubmitNewRules.class)))
				.thenReturn(response);
		mvc.perform(post("/api/product/{productId}/group/{groupId}/rules", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.submitNewRules)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Submit New Rules When Validation Error Occurs
	@Test
	public void ruleSubmitOrUpdate2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
				null);
		when(ruleServices.validateSubmitRules(any(SubmitNewRules.class), any(BindingResult.class), eq(this.userId),
				eq(this.productId), eq(this.groupId))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/group/{groupId}/rules", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.submitNewRules)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.REQUEST_FAILED_CODE)));

	}

	// Test For Submit New Rules When Service Throws Exception
	@Test
	public void ruleSubmitOrUpdate3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(ruleServices.validateSubmitRules(any(SubmitNewRules.class), any(BindingResult.class), eq(this.userId),
				eq(this.productId), eq(this.groupId))).thenReturn(null);
		when(ruleServices.createNewRules(eq(this.productId), eq(this.groupId), any(SubmitNewRules.class)))
				.thenThrow(new Exception());
		when(ruleServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(SubmitNewRules.class))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/group/{groupId}/rules", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.submitNewRules)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Delete Rule When No Validation Error Occurs
	@Test
	public void ruleDelete1() throws Exception {
		String ruleId = "2";
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.RULE_DELETED_SUCCESS_MESSAGE);
		when(ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId, ruleId)).thenReturn(null);
		when(ruleServices.deleteRule(ruleId)).thenReturn(response);
		mvc.perform(
				delete("/api/product/{productId}/group/{groupId}/rules/{ruleId}", this.productId, this.groupId, ruleId)
						.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)))
				.andExpect(jsonPath("$.data", is(Constants.RULE_DELETED_SUCCESS_MESSAGE)));

	}

	// Test For Delete Rule When Validation Error Occurs
	@Test
	public void ruleDelete2() throws Exception {
		String ruleId = "2";
		GenericAPIResponse response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INVALID_GROUP_ID_MESSAGE);
		when(ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId, ruleId)).thenReturn(response);
		mvc.perform(
				delete("/api/product/{productId}/group/{groupId}/rules/{ruleId}", this.productId, this.groupId, ruleId)
						.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)))
				.andExpect(jsonPath("$.data", is(Constants.INVALID_GROUP_ID_MESSAGE)));

	}

	// Test For Delete Rule When Service Throws Exception
	@Test
	public void ruleDelete3() throws Exception {
		String ruleId = "2";
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(ruleServices.validateDeleteRule(this.userId, this.productId, this.groupId, ruleId)).thenReturn(null);
		when(ruleServices.deleteRule(ruleId)).thenThrow(new Exception());
		when(ruleServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(
				delete("/api/product/{productId}/group/{groupId}/rules/{ruleId}", this.productId, this.groupId, ruleId)
						.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)))
				.andExpect(jsonPath("$.data", is(Constants.INTERNAL_SERVER_ERROR_MESSAGE)));

	}

}
