package com.kuliza.lending.configurator.expression;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewExpression;
import com.kuliza.lending.configurator.pojo.SubmitNewExpressions;
import com.kuliza.lending.configurator.pojo.UpdateExpression;
import com.kuliza.lending.configurator.service.expressionservice.ExpressionServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.ExpressionDataValidator;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
@SuppressFBWarnings("SIC_INNER_SHOULD_BE_STATIC_ANON")
public class ExpressionServiceTest {

	@Mock
	private ObjectMapper objectMapper;

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private ExpressionDao expressionDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private ExpressionDataValidator expressionDataValidator;

	@InjectMocks
	private ExpressionServices expressionServices;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Group g1;
	private Variable v1;
	private Expression e1;
	private Expression e2;
	private String userId;
	private String productId;
	private String groupId;
	private String expressionId;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.groupId = "1";
		this.expressionId = "1";
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_1", true, false, null, this.g1);
		this.e2 = new Expression(2, "AND ( ID_1 , TRUE )", false, true, this.p1, null);
	}

	// Test For Get Group Expression
	@Test
	public void getExpressions1() throws Exception {
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(this.groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		GenericAPIResponse response = expressionServices.getExpressions(this.productId, this.groupId);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Get Product Expression
	@Test
	public void getExpressions2() throws Exception {
		when(expressionDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false))
				.thenReturn(Arrays.asList(this.e2));
		GenericAPIResponse response = expressionServices.getExpressions(this.productId, "");
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Create Product Expression
	@Test
	public void createExpressions1() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setProductId(this.productId);
		input.setGroupId("");
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(this.productId), false))
				.thenReturn(this.g1);
		GenericAPIResponse response = expressionServices.createNewExpressions(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Create Group Expression
	@Test
	public void createExpressions2() throws Exception {
		NewExpression newExpression = new NewExpression("Age + 3.0 * Age");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		when(groupDao.findByIdAndIsDeleted(Long.parseLong(input.getGroupId()), false)).thenReturn(this.g1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		GenericAPIResponse response = expressionServices.createNewExpressions(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Update Product Expression
	@Test
	public void updateExpressions1() throws Exception {
		this.expressionId = "2";
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setProductId(this.productId);
		input.setGroupId("");
		input.setExpressionId(this.expressionId);
		when(expressionDao.findByIdAndIsDeleted(Long.parseLong(this.expressionId), false)).thenReturn(this.e2);
		when(groupDao.findByNameAndProductIdAndIsDeleted("Personal_Eligibility", Long.parseLong(this.productId), false))
				.thenReturn(this.g1);
		GenericAPIResponse response = expressionServices.updateExpression(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Update Group Expression
	@Test
	public void updateExpressions2() throws Exception {
		NewExpression newExpression = new NewExpression("Age + 3.0 * Age");
		UpdateExpression input = new UpdateExpression(newExpression);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		input.setExpressionId(this.expressionId);
		when(expressionDao.findByIdAndIsDeleted(Long.parseLong(this.expressionId), false)).thenReturn(this.e1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		GenericAPIResponse response = expressionServices.updateExpression(input);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Validate Update Expression When No Validation Error Occurs
	@Test
	public void validateUpdateExpressions1() throws Exception {
		this.expressionId = "2";
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpression");
		doNothing().when(expressionDataValidator).validateUpdateExpression(any(UpdateExpression.class),
				any(BindingResult.class));
		doNothing().when(expressionDataValidator).validateExpression(any(NewExpression.class),
				any(BindingResult.class));
		GenericAPIResponse response = expressionServices.validateUpdateExpression(this.userId, this.productId,
				this.groupId, this.expressionId, input, result);
		assertNull(response);
	}

	// Test For Validate Update Expression When Validation Error Occurs
	@Test
	public void validateUpdateExpressions2() throws Exception {
		this.expressionId = "2";
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateExpression");
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {
				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("expressionId", "expressionId.invalid", Constants.INVALID_EXPRESSION_ID);
				return result;
			}
		}).when(expressionDataValidator).validateUpdateExpression(any(UpdateExpression.class),
				any(BindingResult.class));
		GenericAPIResponse response = expressionServices.validateUpdateExpression(this.userId, this.productId,
				this.groupId, this.expressionId, input, result);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Create Expression When No Validation Error Occurs
	@Test
	public void validateSubmitExpressions1() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpression");
		doNothing().when(expressionDataValidator).validateNewExpression(any(SubmitNewExpressions.class),
				any(BindingResult.class));
		doNothing().when(expressionDataValidator).validateExpression(any(NewExpression.class),
				any(BindingResult.class));
		GenericAPIResponse response = expressionServices.validateSubmitExpressions(input, result, this.userId,
				this.productId, this.groupId);
		assertNull(response);
	}

	// Test For Validate Create Expression When Validation Error Occurs At
	// SubmitNewExpressions Level
	@Test
	public void validateSubmitExpressions2() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpression");
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {
				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("productId", "productId.invalid", Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
				return result;
			}
		}).when(expressionDataValidator).validateNewExpression(any(SubmitNewExpressions.class),
				any(BindingResult.class));
		GenericAPIResponse response = expressionServices.validateSubmitExpressions(input, result, this.userId,
				this.productId, this.groupId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Create Expression When No Validation Error Occurs At
	// NewExpression Level
	@Test
	public void validateSubmitExpressions3() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewExpression");
		doNothing().when(expressionDataValidator).validateNewExpression(any(SubmitNewExpressions.class),
				any(BindingResult.class));
		doAnswer(new Answer<BeanPropertyBindingResult>() {
			@Override
			public BeanPropertyBindingResult answer(final InvocationOnMock invocation) throws Throwable {
				final BeanPropertyBindingResult result = (BeanPropertyBindingResult) (invocation.getArguments())[1];
				result.rejectValue("expressionString", "expressionString.invalid",
						Constants.INVALID_EXPRESSION_MESSAGE);
				return result;
			}
		}).when(expressionDataValidator).validateExpression(any(NewExpression.class), any(BindingResult.class));
		GenericAPIResponse response = expressionServices.validateSubmitExpressions(input, result, this.userId,
				this.productId, this.groupId);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}
}
