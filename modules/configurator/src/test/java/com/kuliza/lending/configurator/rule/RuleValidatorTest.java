package com.kuliza.lending.configurator.rule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.NewRule;
import com.kuliza.lending.configurator.pojo.RuleDef;
import com.kuliza.lending.configurator.pojo.SubmitNewRules;
import com.kuliza.lending.configurator.validators.RuleDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class RuleValidatorTest {

	@Mock
	private VariableDao variableDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private RuleDao ruleDao;

	@Mock
	private GroupDao groupDao;

	@InjectMocks
	private RuleDataValidator ruleDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Variable v1;
	private Group g1;
	private Rule r1;
	private String userId;
	private String productId;
	private String groupId;
	private String variableId;
	private RuleDef r1RuleDef;
	private RuleDef r2RuleDef;
	private NewRule newRule;
	private SubmitNewRules submitNewRules;

	@Before
	public void init() {
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.userId = "1";
		this.productId = "1";
		this.groupId = "1";
		this.variableId = "1";
		this.r1RuleDef = new RuleDef("", ">=", "<=", "30", "50", "5");
		this.r2RuleDef = new RuleDef("", ">", "", "50", "", "4");
		this.newRule = new NewRule(Arrays.asList(r1RuleDef, r2RuleDef), "true", "2.0", "Age");
		this.submitNewRules = new SubmitNewRules(Arrays.asList(newRule));
	}

	// Test For Get All Rules When Product Belongs To User And Group Belongs To
	// Product
	@Test
	public void validateGetAllRules1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		assertEquals(true, ruleDataValidator.validateGetAllRules(this.userId, this.productId, this.groupId));
	}

	// Test For Get All Rules When Product Belongs To User But Group Doesn't
	// Belongs To Product
	@Test
	public void validateGetAllRules2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		assertEquals(false, ruleDataValidator.validateGetAllRules(this.userId, this.productId, this.groupId));
	}

	// Test For Get All Rules When Product Doesn't Belongs To User
	@Test
	public void validateGetAllRules3() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false, ruleDataValidator.validateGetAllRules(this.userId, this.productId, this.groupId));
	}

	// Test For Validate New Rule When No Validation Error Occurs
	@Test
	public void validateRule1() throws Exception {
		this.newRule.setProductId(this.productId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.newRule, "newRule");
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 1L, false)).thenReturn(this.v1);
		ruleDataValidator.validateRule(this.newRule, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate New Rule When Field Level Validation Error Occurs
	@Test
	public void validateRule2() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.newRule, "newRule");
		result.rejectValue("isActive", "isActive.invalid", "Invalid is Active");
		ruleDataValidator.validateRule(this.newRule, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate New Rule When Variable Doesn't Exist
	@Test
	public void validateRule3() throws Exception {
		this.newRule.setProductId(this.productId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.newRule, "newRule");
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 1L, false)).thenReturn(null);
		ruleDataValidator.validateRule(this.newRule, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Form When No Validation Error Occurs
	@Test
	public void validateRuleForm1() throws Exception {
		this.submitNewRules.setGroupId(this.groupId);
		this.submitNewRules.setProductId(this.productId);
		this.submitNewRules.setUserId(this.userId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		ruleDataValidator.validateRuleForm(this.submitNewRules, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Rule Form When Field Level Validation Error Occurs
	@Test
	public void validateRuleForm2() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		result.rejectValue("newRulesList", "newRulesList.invalid", "newRulesList cannot be empty");
		ruleDataValidator.validateRuleForm(this.submitNewRules, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Form When Product Doesn't Belong To User
	@Test
	public void validateRuleForm3() throws Exception {
		this.submitNewRules.setGroupId(this.groupId);
		this.submitNewRules.setProductId(this.productId);
		this.submitNewRules.setUserId(this.userId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		ruleDataValidator.validateRuleForm(this.submitNewRules, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Form When Product Belongs To User But Product Is
	// In Deployed/UnDeployed State
	@Test
	public void validateRuleForm4() throws Exception {
		this.submitNewRules.setGroupId(this.groupId);
		this.submitNewRules.setProductId(this.productId);
		this.submitNewRules.setUserId(this.userId);
		this.p1.setStatus(2);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		ruleDataValidator.validateRuleForm(this.submitNewRules, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Form When Product Belongs To User And Product Is
	// In Edit State But Group Doesn't Belong To Product
	@Test
	public void validateRuleForm5() throws Exception {
		this.submitNewRules.setGroupId(this.groupId);
		this.submitNewRules.setProductId(this.productId);
		this.submitNewRules.setUserId(this.userId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.submitNewRules, "submitNewRules");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		ruleDataValidator.validateRuleForm(this.submitNewRules, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When No Validation Error Occurs
	@Test
	public void validateRuleStructure1() throws Exception {
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Rule Structure When Field Level Validation Error Occurs
	@Test
	public void validateRuleStructure2() throws Exception {
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		result.rejectValue("fn1", "fn1.invalid", "Invalid fn1");
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure3() throws Exception {
		this.r1RuleDef = new RuleDef("1", ">=", "<=", "30a", "", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		this.r1RuleDef.setGroupId(this.groupId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		when(ruleDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.r1RuleDef.getRuleId()),
				Long.parseLong(this.groupId), false)).thenReturn(null);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure4() throws Exception {
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("1", ">=", "<=", "30a", "40p", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		this.r1RuleDef.setGroupId(this.groupId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		when(ruleDao.findByIdAndGroupIdAndIsDeleted(Long.parseLong(this.r1RuleDef.getRuleId()),
				Long.parseLong(this.groupId), false)).thenReturn(this.r1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure5() throws Exception {
		this.v1 = new Variable(1, "Age", "Boolean", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("", ">=", "", "30a", "", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure6() throws Exception {
		this.v1 = new Variable(1, "Age", "Boolean", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("", "==", "!=", "true", "false", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure7() throws Exception {
		this.v1 = new Variable(1, "Age", "String", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("", "<=", "!=", "true", "false", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure8() throws Exception {
		this.v1 = new Variable(1, "Age", "String", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("", "==", "", "abc", "", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure9() throws Exception {
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("", ">", "", "30", "40", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		System.out.println(result.getFieldErrors());
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Rule Structure When Different Different Business
	// Validation Error Occurs
	@Test
	public void validateRuleStructure10() throws Exception {
		this.v1 = new Variable(1, "Age", "String", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.r1RuleDef = new RuleDef("", "==", "", "", "", "5");
		this.r1RuleDef.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.r1RuleDef, "ruleDef");
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		ruleDataValidator.validateRuleStructure(this.r1RuleDef, result);
		System.out.println(result.getFieldErrors());
		assertEquals(true, result.hasErrors());
	}

}
