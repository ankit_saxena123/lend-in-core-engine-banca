package com.kuliza.lending.configurator.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProduct;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.ProductDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class ProductValidatorTest {

	@Mock
	private ProductDao productDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private GroupDao groupDao;

	@Mock
	private RuleDao ruleDao;

	@Mock
	private ExpressionDao expressionDao;

	@Mock
	private ProductCategoryDao productCategoryDao;

	@InjectMocks
	private ProductDataValidator productDataValidator;

	private User user;
	private ProductCategory pc1;
	private Product p1;
	private String userId;
	private String productId;
	private String templateProductId;
	private String productCategoryId;
	private Variable v1;
	private Variable v2;
	private Group g1;
	private Rule r1;
	private Rule r2;
	private Expression e1;
	private Expression e2;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.templateProductId = "2";
		this.productCategoryId = "1";
		this.user = new User(1,"arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.v2 = new Variable(2, "Gender", "String", "Journey", "Primitive", "Gender Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(2, "> 50", "4", 2.0, true, this.v1, this.g1);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_1", true, false, null, this.g1);
		this.e2 = new Expression(2, "AND ( ID_1 , TRUE )", false, true, this.p1, null);
	}

	// Test For Create New Product When No Validation Error Occurs And Template
	// ProductId is Given
	@Test
	public void validateNewProduct1() throws Exception {
		this.templateProductId = "1";
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank", "1", "1");
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProduct");
		when(productDao.findByNameAndUserIdAndIsDeleted(input.getProductName(), Long.parseLong(this.userId), false))
				.thenReturn(null);
		when(productCategoryDao.findByIdAndIsDeleted(Long.parseLong(this.productCategoryId), false))
				.thenReturn(this.pc1);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.templateProductId),
				Long.parseLong(this.userId), false)).thenReturn(this.p1);
		productDataValidator.validateNewProduct(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Create New Product When No Validation Error Occurs And Template
	// ProductId is Not Given
	@Test
	public void validateNewProduct2() throws Exception {
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank", "1", "");
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProduct");
		when(productDao.findByNameAndUserIdAndIsDeleted(input.getProductName(), Long.parseLong(this.userId), false))
				.thenReturn(null);
		when(productCategoryDao.findByIdAndIsDeleted(Long.parseLong(this.productCategoryId), false))
				.thenReturn(this.pc1);
		productDataValidator.validateNewProduct(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Create New Product When Field Level Validation Error Occurs
	@Test
	public void validateNewProduct3() throws Exception {
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank", "1", "");
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProduct");
		result.rejectValue("productName", "productName.invalid", Constants.INVALID_PRODUCT_NAME_MESSAGE);
		productDataValidator.validateNewProduct(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Create New Product When Business Logic Validation Error Occurs
	@Test
	public void validateNewProduct4() throws Exception {
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank", "1", "2");
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProduct");
		when(productDao.findByNameAndUserIdAndIsDeleted(input.getProductName(), Long.parseLong(this.userId), false))
				.thenReturn(this.p1);
		when(productCategoryDao.findByIdAndIsDeleted(Long.parseLong(this.productCategoryId), false)).thenReturn(null);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.templateProductId),
				Long.parseLong(this.userId), false)).thenReturn(null);
		productDataValidator.validateNewProduct(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Create New Product When Template Product Category Is Different
	// From Selected Product Category Id
	@Test
	public void validateNewProduct5() throws Exception {
		ProductCategory pc2 = new ProductCategory(2, "Eigibility");
		Product p2 = new Product(2, "AXIS_Bank_2", 0, 2, "abdef", user, pc2);
		SubmitNewProduct input = new SubmitNewProduct("Axis_Bank", "1", "2");
		input.setUserId(this.userId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewProduct");
		when(productDao.findByNameAndUserIdAndIsDeleted(input.getProductName(), Long.parseLong(this.userId), false))
				.thenReturn(null);
		when(productCategoryDao.findByIdAndIsDeleted(Long.parseLong(this.productCategoryId), false))
				.thenReturn(this.pc1);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.templateProductId),
				Long.parseLong(this.userId), false)).thenReturn(p2);
		productDataValidator.validateNewProduct(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Clone Product When Product Belong to User and Product Is In
	// Deployed Or UnDeployed State
	@Test
	public void validateCloneProduct1() throws Exception {
		this.p1.setStatus(2);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateCloneProduct(this.productId, this.userId);
		assertNull(response);
	}

	// Test For Clone Product When Product Doesn't Belong to User
	@Test
	public void validateCloneProduct2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		GenericAPIResponse response = productDataValidator.validateCloneProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Clone Product When Product Belong to User But Product Is In
	// Edit Or Published State
	@Test
	public void validateCloneProduct3() throws Exception {
		this.p1.setStatus(1);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateCloneProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Edit Product When Product Belong to User and Product Is In
	// Publish State
	@Test
	public void validateEditProduct1() throws Exception {
		this.p1.setStatus(1);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateEditProduct(this.productId, this.userId);
		assertNull(response);
	}

	// Test For Edit Product When Product Belong to User and Product Is In
	// Deployed/Undeployed State
	@Test
	public void validateEditProduct2() throws Exception {
		this.p1.setStatus(2);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateEditProduct(this.productId, this.userId);
		assertEquals(Constants.REQUEST_FAILED_CODE, response.getStatus());
		assertEquals(Constants.PRODUCT_NOT_EDITABLE_MESSAGE, response.getData());
	}

	// Test For Edit Product When Product Belong to User and Product Is In
	// New State
	@Test
	public void validateEditProduct3() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateEditProduct(this.productId, this.userId);
		assertEquals(Constants.REQUEST_FAILED_CODE, response.getStatus());
		assertEquals(Constants.PRODUCT_ALREADY_EDITABLE_MESSAGE, response.getData());
	}

	// Test For Edit Product When Product Doen't Belong to User and Product
	@Test
	public void validateEditProduct4() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		GenericAPIResponse response = productDataValidator.validateEditProduct(this.productId, this.userId);
		assertEquals(Constants.NOT_FOUND_ERROR_CODE, response.getStatus());
		assertEquals(Constants.INVALID_PRODUCT_ID_MESSAGE, response.getData());
	}

	// Test For Publish Product When Product Belong to User and Product Is In
	// Edit State
	@Test
	public void validatePublishProduct1() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		when(variableDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false))
				.thenReturn(Arrays.asList(this.v1, this.v2));
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(ruleDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(Arrays.asList(this.r1, this.r2));
		when(expressionDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(Arrays.asList(this.e1));
		when(expressionDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false))
				.thenReturn(Arrays.asList(this.e2));
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNull(response);
	}

	// Test For Publish Product When Product Doesn't Belong to User
	@Test
	public void validatePublishProduct2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User But Product Is In
	// Deployed Or Undeployed State
	@Test
	public void validatePublishProduct3() throws Exception {
		this.p1.setStatus(2);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User Product Is In
	// Edit State But Product Has No Variables
	@Test
	public void validatePublishProduct4() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		when(variableDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(new ArrayList<>());
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User Product Is In
	// Edit State But Product Has Variables But No Group
	@Test
	public void validatePublishProduct5() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.v1, this.v2));
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(new ArrayList<>());
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User Product Is In
	// Edit State But Product Has Variables And Group But Group Has No Rules And
	// Expressions
	@Test
	public void validatePublishProduct6() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		when(variableDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false))
				.thenReturn(Arrays.asList(this.v1, this.v2));
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(ruleDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(new ArrayList<>());
		when(expressionDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(new ArrayList<>());
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User Product Is In
	// Edit State But Product Has Variables And Group And Group Has Rules And
	// Expressions But Product Expression Is Not Defined
	@Test
	public void validatePublishProduct7() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		when(variableDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false))
				.thenReturn(Arrays.asList(this.v1, this.v2));
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(ruleDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(Arrays.asList(this.r1, this.r2));
		when(expressionDao.findByGroupIdAndIsDeleted(this.g1.getId(), false)).thenReturn(Arrays.asList(this.e1));
		when(expressionDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(new ArrayList<>());
		GenericAPIResponse response = productDataValidator.validatePublishProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Deploy Product When Product Belong to User and Product Is In
	// Published State
	@Test
	public void validateDeployProduct1() throws Exception {
		this.p1.setStatus(1);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateDeployProduct(this.productId, this.userId);
		assertNull(response);
	}

	// Test For Publish Product When Product Doesn't Belong to User
	@Test
	public void validateDeployProduct2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		GenericAPIResponse response = productDataValidator.validateDeployProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User But Product Is In
	// Edit State
	@Test
	public void validateDeployProduct3() throws Exception {
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateDeployProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Publish Product When Product Belong to User But Product Is In
	// Deployed State
	@Test
	public void validateDeployProduct4() throws Exception {
		this.p1.setStatus(2);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(productId), false)).thenReturn(this.p1);
		GenericAPIResponse response = productDataValidator.validateDeployProduct(this.productId, this.userId);
		assertNotNull(response);
	}

	// Test For Update Product When No Validation Error Occurs
	@Test
	public void validateUpdateProduct1() throws Exception {
		UpdateProduct input = new UpdateProduct("HDFC_Bank");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateProduct");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(productDao.findByNameAndUserIdAndIsDeleted(input.getProductName(), Long.parseLong(this.userId), false))
				.thenReturn(null);
		productDataValidator.validateUpdateProduct(input, result);
		assertEquals(false, result.hasErrors());

	}

	// Test For Update Product When Field Level Validation Error Occurs
	@Test
	public void validateUpdateProduct2() throws Exception {
		UpdateProduct input = new UpdateProduct("HDFC_Bank");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateProduct");
		result.rejectValue("productName", "productName.invalid", Constants.INVALID_PRODUCT_NAME_MESSAGE);
		productDataValidator.validateUpdateProduct(input, result);
		assertEquals(true, result.hasErrors());

	}

	// Test For Update Product When Product Doesn't Belong To User
	@Test
	public void validateUpdateProduct3() throws Exception {
		UpdateProduct input = new UpdateProduct("HDFC_Bank");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateProduct");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		productDataValidator.validateUpdateProduct(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Product When Product Belong To User But Product Is In
	// Deployed Or Undeployed State
	@Test
	public void validateUpdateProduct4() throws Exception {
		UpdateProduct input = new UpdateProduct("HDFC_Bank");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(2);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateProduct");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		productDataValidator.validateUpdateProduct(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Product When Product Belong To User But Product Name Is
	// Taken
	@Test
	public void validateUpdateProduct5() throws Exception {
		UpdateProduct input = new UpdateProduct("Axis_Bank");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateProduct");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(productDao.findByNameAndUserIdAndIsDeleted(input.getProductName(), Long.parseLong(this.userId), false))
				.thenReturn(this.p1);
		productDataValidator.validateUpdateProduct(input, result);
		assertEquals(true, result.hasErrors());

	}

	// Test For Validate Get Product List When Given Id Equals Null
	@Test
	public void validateProductCategoryId1() throws Exception {
		GenericAPIResponse response = productDataValidator.validateGetProductList(null, "0");
		assertNull(response);
	}

	// Test For Validate Product Category Id When Given Id Not Equals Null And
	// Id Exists
	@Test
	public void validateProductCategoryId2() throws Exception {
		GenericAPIResponse response = productDataValidator.validateGetProductList(this.productCategoryId, "0");
		assertNull(response);
	}

	// Test For Validate Product Category Id When Given Id Not Equals Null But
	// Id Doesnt Match Regex
	@Test
	public void validateProductCategoryId3() throws Exception {
		GenericAPIResponse response = productDataValidator.validateGetProductList("2a", "0");
		assertEquals(Constants.NOT_FOUND_ERROR_CODE, response.getStatus());
		assertEquals(Constants.INVALID_PRODUCT_CATEGORY_ID_MESSAGE, response.getData());
	}

	// Test For Validate Product Category Id When Status Is Given But
	// It Doesnt Match Regex
	@Test
	public void validateProductCategoryId4() throws Exception {
		GenericAPIResponse response = productDataValidator.validateGetProductList("2", "3b");
		assertEquals(Constants.NOT_FOUND_ERROR_CODE, response.getStatus());
		assertEquals(Constants.INVALID_STATUS_MESSAGE, response.getData());
	}

}
