package com.kuliza.lending.configurator.group;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.kuliza.lending.configurator.controllers.GroupControllers;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewGroup;
import com.kuliza.lending.configurator.pojo.UpdateGroup;
import com.kuliza.lending.configurator.service.groupservice.GroupServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.GroupDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebMvcTest(GroupControllers.class)
public class GroupControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;
//
//	@MockBean
//	private UserTokenDao userTokenDao;

	// @Autowired
	// private FilterRegistrationBean filterRegistrationBean;

	private MockMvc mvc;

	@MockBean
	private GroupServices groupServices;

	@MockBean
	private GroupDataValidator groupDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Group g1;
	private Group g2;
	private String userId;
	private String productId;
	private String groupId;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		// AuthenticationFilter authFilter = (AuthenticationFilter)
		// filterRegistrationBean.getFilter();
		// authFilter.serUserTokenDao(this.userTokenDao);
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.g2 = new Group(2, "Business_Eligibility", p1);
		this.userId = "1";
		this.productId = "1";
		this.groupId = "2";
	}

	// Test For Groups List Controller When Validator Return True
	@Test
	public void getGroupsList1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.g1, this.g2));
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		objectMapper.setFilterProvider(filters);
		// when(userTokenDao.findByTokenAndIsDeleted(this.token,
		// false)).thenReturn(this.userToken);
		when(groupServices.getGroupsList(this.productId)).thenReturn(response);
		when(groupDataValidator.validateGetAllGroup(this.userId, this.productId)).thenReturn(true);
		mvc.perform(get("/api/product/{productId}/group/list", this.productId).requestAttr("userId", this.userId)
		// .header("Authorization", "Bearer " + this.token)
		)
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Groups List Controller When Validator Return False
	@Test
	public void getGroupsList2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.g1, this.g2));
		this.productId = "2";
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		objectMapper.setFilterProvider(filters);
		when(groupServices.getGroupsList(this.productId)).thenReturn(response);
		when(groupDataValidator.validateGetAllGroup(this.userId, productId)).thenReturn(false);
		mvc.perform(get("/api/product/{productId}/group/list", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Groups List Controller When Service Throws Exception
	@Test
	public void getGroupsList3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(groupServices.getGroupsList(this.productId)).thenThrow(new Exception());
		when(groupServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		when(groupDataValidator.validateGetAllGroup(this.userId, this.productId)).thenReturn(true);
		mvc.perform(get("/api/product/{productId}/group/list", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Groups Data Controller When Validator Return True
	@Test
	public void getAllGroups1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.g1, this.g2));
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		when(groupServices.getGroupsData(this.productId)).thenReturn(response);
		when(groupDataValidator.validateGetAllGroup(this.userId, this.productId)).thenReturn(true);
		mvc.perform(get("/api/product/{productId}/group", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Groups Data Controller When Validator Return False
	@Test
	public void getAllGroups2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.g1, this.g2));
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		when(groupServices.getGroupsData(this.productId)).thenReturn(response);
		when(groupDataValidator.validateGetAllGroup(this.userId, this.productId)).thenReturn(false);
		mvc.perform(get("/api/product/{productId}/group", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Groups Data Controller When Service Throws Error
	@Test
	public void getAllGroups3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		Exception e = new Exception();
		when(groupServices.getGroupsData(this.productId)).thenThrow(e);
		when(groupServices.handleException(eq(e), any(HttpServletRequest.class), eq(this.userId))).thenReturn(response);
		when(groupDataValidator.validateGetAllGroup(this.userId, this.productId)).thenReturn(true);
		mvc.perform(get("/api/product/{productId}/group", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Single Group Data Controller When Validator Return True
	@Test
	public void getSingleGroup1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.g2);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		when(groupServices.getGroupData(this.groupId)).thenReturn(response);
		when(groupDataValidator.validateGetSingleGroup(this.userId, this.productId, this.groupId)).thenReturn(true);
		mvc.perform(get("/api/product/{productId}/group/{groupId}", this.productId, this.groupId).requestAttr("userId",
				this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Single Group Data Controller When Validator Return False
	@Test
	public void getSingleGroup2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.g2);
		this.groupId = "3";
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		when(groupServices.getGroupData(this.groupId)).thenReturn(response);
		when(groupDataValidator.validateGetSingleGroup(this.userId, this.productId, this.groupId)).thenReturn(false);
		mvc.perform(get("/api/product/{productId}/group/{groupId}", this.productId, this.groupId).requestAttr("userId",
				this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));
	}

	// Test For Single Group Data Controller When Service Throws Error
	@Test
	public void getSingleGroup3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		Exception e = new Exception();
		when(groupServices.getGroupData(this.groupId)).thenThrow(e);
		when(groupServices.handleException(eq(e), any(HttpServletRequest.class), eq(this.userId))).thenReturn(response);
		when(groupDataValidator.validateGetSingleGroup(this.userId, this.productId, this.groupId)).thenReturn(true);
		mvc.perform(get("/api/product/{productId}/group/{groupId}", this.productId, this.groupId).requestAttr("userId",
				this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	// Test For Create Group Data Controller When Validator Return No Error
	@Test
	public void createNewGroup1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.g2);
		when(groupServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(groupServices.createNewGroup(any(SubmitNewGroup.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(post("/api/product/{productId}/group", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewGroup("Personal_Eligibility"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Create Group Data Controller When Validator Returns Error
	@Test
	public void createNewGroup2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(groupServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(post("/api/product/{productId}/group", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewGroup("Personal_Eligibility"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Create Group Data Controller When Service Throws Exception
	@Test
	public void createNewGroup3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(groupServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(groupServices.createNewGroup(any(SubmitNewGroup.class))).thenThrow(new Exception());
		when(groupServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(UpdateGroup.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(post("/api/product/{productId}/group", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewGroup("Personal_Eligibility"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

	@Test
	public void updateGroup1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.g2);
		when(groupServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(groupServices.updateGroup(any(UpdateGroup.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}/group/{groupId}", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateGroup("Personal_Eligibility"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Create Group Data Controller When Validator Returns Error
	@Test
	public void updateGroup2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(groupServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}/group/{groupId}", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateGroup("Personal_Eligibility"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Create Group Data Controller When Service Throws Exception
	@Test
	public void updateGroup3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(groupServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(groupServices.updateGroup(any(UpdateGroup.class))).thenThrow(new Exception());
		when(groupServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(UpdateGroup.class))).thenReturn(response);
		FilterProvider filters = new SimpleFilterProvider().addFilter("groupFilter",
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", "rules"));
		this.objectMapper.setFilterProvider(filters);
		mvc.perform(put("/api/product/{productId}/group/{groupId}", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new UpdateGroup("Personal_Eligibility"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

}
