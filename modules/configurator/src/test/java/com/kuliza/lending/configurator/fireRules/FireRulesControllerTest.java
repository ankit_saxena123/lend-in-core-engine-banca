package com.kuliza.lending.configurator.fireRules;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.controllers.FireRulesController;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.GetRequiredVariables;
import com.kuliza.lending.configurator.pojo.ProductionFireRulesInput;
import com.kuliza.lending.configurator.pojo.TestFireRulesInput;
import com.kuliza.lending.configurator.service.firerulesservice.FireRulesServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.FireRulesDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebMvcTest(FireRulesController.class)
public class FireRulesControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@MockBean
	private FireRulesServices fireRulesServices;

	@MockBean
	private FireRulesDataValidator fireRulesDataValidator;

	private String userId;
	private ProductionFireRulesInput productionInput;
	private TestFireRulesInput testInput;
	private GetRequiredVariables getRequiredVariables;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.userId = "1";
		this.productionInput = new ProductionFireRulesInput("abc", "", "123456", new HashMap<String, Object>());
		this.testInput = new TestFireRulesInput(Arrays.asList(1L, 2L), new HashMap<String, Object>());
		this.getRequiredVariables = new GetRequiredVariables(Arrays.asList(1L, 2L));
	}

	// Test For Production Fire Rules When No Validation Error Occurs
	@Test
	public void productionFireRulesInput1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				null);
		doNothing().when(fireRulesDataValidator).validateProductionFireRules(any(ProductionFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(fireRulesServices.productionFireRules(any(BindingResult.class), any(ProductionFireRulesInput.class)))
				.thenReturn(response);
		mvc.perform(post("/api/production/fire-rules").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.productionInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Production Fire Rules When Field Level Validation Error Occurs
	@Test
	public void productionFireRulesInput2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		doNothing().when(fireRulesDataValidator).validateProductionFireRules(any(ProductionFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		mvc.perform(post("/api/production/fire-rules").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.productionInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Production Fire Rules When Service Throws Exception
	@Test
	public void productionFireRulesInput3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		doNothing().when(fireRulesDataValidator).validateProductionFireRules(any(ProductionFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(fireRulesServices.productionFireRules(any(BindingResult.class), any(ProductionFireRulesInput.class)))
				.thenThrow(new Exception());
		when(fireRulesServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(ProductionFireRulesInput.class))).thenReturn(response);
		mvc.perform(post("/api/production/fire-rules").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.productionInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Test Fire Rules When No Validation Error Occurs
	@Test
	public void testFireRulesInput1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				null);
		doNothing().when(fireRulesDataValidator).validateTestFireRules(any(TestFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(fireRulesServices.testFireRules(any(TestFireRulesInput.class))).thenReturn(response);
		mvc.perform(post("/api/test/fire-rules").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.testInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Test Fire Rules When Field Level Validation Error Occurs
	@Test
	public void testFireRulesInput2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		doNothing().when(fireRulesDataValidator).validateTestFireRules(any(TestFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		mvc.perform(post("/api/test/fire-rules").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.testInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Test Fire Rules When Service Throws Exception
	@Test
	public void testFireRulesInput3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		doNothing().when(fireRulesDataValidator).validateTestFireRules(any(TestFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(fireRulesServices.testFireRules(any(TestFireRulesInput.class))).thenThrow(new Exception());
		when(fireRulesServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(ProductionFireRulesInput.class))).thenReturn(response);
		mvc.perform(post("/api/test/fire-rules").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.testInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Get Required Variables When No Validation Error Occurs
	@Test
	public void getRequiredVariables1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				null);
		doNothing().when(fireRulesDataValidator).validateTestFireRules(any(TestFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(fireRulesServices.getAllRequiredVariables(any(GetRequiredVariables.class))).thenReturn(response);
		mvc.perform(post("/api/fire-rules/variables").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.testInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Get Required Variables When Field Level Validation Error Occurs
	@Test
	public void getRequiredVariables2() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		doNothing().when(fireRulesDataValidator).validateTestFireRules(any(TestFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(response);
		mvc.perform(post("/api/fire-rules/variables").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.testInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Get Required Variables When When Service Throws Exception
	@Test
	public void getRequiredVariables3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		doNothing().when(fireRulesDataValidator).validateTestFireRules(any(TestFireRulesInput.class),
				any(BindingResult.class));
		when(fireRulesServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(fireRulesServices.getAllRequiredVariables(any(GetRequiredVariables.class))).thenThrow(new Exception());
		when(fireRulesServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(ProductionFireRulesInput.class))).thenReturn(response);
		mvc.perform(post("/api/fire-rules/variables").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(this.testInput)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

}
