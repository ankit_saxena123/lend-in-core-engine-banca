package com.kuliza.lending.configurator.fireRules;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.drools.DroolsEngine;
import com.kuliza.lending.configurator.models.ExecutionDao;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.ProductDeployment;
import com.kuliza.lending.configurator.models.ProductDeploymentDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.DroolsInput;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.GetRequiredVariables;
import com.kuliza.lending.configurator.pojo.ProductionFireRulesInput;
import com.kuliza.lending.configurator.pojo.TestFireRulesInput;
import com.kuliza.lending.configurator.service.firerulesservice.FireRulesServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.FireRulesDataValidator;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
@SuppressFBWarnings("SIC_INNER_SHOULD_BE_STATIC_ANON")
public class FireRulesServiceTest {

	@Mock
	private DroolsEngine droolsEngine;

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private ExpressionDao expressionDao;

	@Mock
	private FireRulesDataValidator fireRulesDataValidator;

	@Mock
	private ProductDeploymentDao productDeploymentDao;

	@Mock
	private ExecutionDao executionDao;

	@Mock
	private ObjectMapper objectMapper;

	@InjectMocks
	private FireRulesServices fireRulesServices;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Product p2;
	private Variable v1;
	private Variable v2;
	private Variable v3;
	private Variable v4;
	private Variable v5;
	private Variable v6;
	private Group g1;
	private Expression e1;
	private Expression e2;
	private Expression e3;
	private Expression e4;
	private Expression e5;
	private Expression e6;
	private ProductDeployment pd1;
	private ProductionFireRulesInput productionInput;
	private TestFireRulesInput testInput;
	private GetRequiredVariables getRequiredVariables;

	@Before
	public void init() {
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.p2 = new Product(2, "AXIS_Bank_V2", 1, 2, "abdef", this.user, this.pc1);
		this.pd1 = new ProductDeployment(1, true, this.p2.getIdentifier(), this.p2, this.user);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.v2 = new Variable(2, "Gender", "String", "Journey", "Primitive", "Gender Of Customer", null, p1);
		this.v3 = new Variable(3, "Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", null, p1);
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score",
				"ID_1 + 2 * ID_1 + 3 * ID_1", p1);
		this.v5 = new Variable(5, "DV_Boolean", "Boolean", "Calculated", "Derived", "Calculate DV",
				"IF ( TRUE , TRUE , FALSE )", p1);
		this.v6 = new Variable(6, "DV_String", "String", "Calculated", "Derived", "Calculate DV",
				"IF ( TRUE , \"Abcdef\" , \"Ghijkl\" )", p1);
		this.g1 = new Group(1, "Personal_Eligibility", this.p2);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_2", true, false, null, this.g1);
		this.e2 = new Expression(1, "IF ( TRUE , TRUE , FALSE )", true, false, null, this.g1);
		this.e3 = new Expression(1, "IF ( TRUE , \" Eligibile \" , \" Not Eligibile \" )", true, false, null, this.g1);
		this.e4 = new Expression(2, "IF ( TRUE , 10.0 , 20.0 )", false, true, this.p1, null);
		this.e5 = new Expression(2, "IF ( TRUE , TRUE , FALSE )", false, true, this.p1, null);
		this.e6 = new Expression(2, "IF ( TRUE , \" Eligibile \" , \" Not Eligibile \" )", false, true, this.p1, null);

		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age", "42");
		inputData.put("Gender", "abc");
		inputData.put("Salaried", "true");
		this.productionInput = new ProductionFireRulesInput("abcdef", "", "123456", inputData);
		this.testInput = new TestFireRulesInput(Arrays.asList(2L), inputData);
		this.getRequiredVariables = new GetRequiredVariables(Arrays.asList(2L));
	}

	// Test For Production Fire Rules When No Validation Error Occurs
	@Test
	@SuppressWarnings("unchecked")
	public void productionFireRules1() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted("abcdef", true, false)).thenReturn(this.pd1);
		doAnswer(new Answer<GenericAPIResponse>() {
			@Override
			public GenericAPIResponse answer(final InvocationOnMock invocation) throws Throwable {
				Set<Variable> primitiveVariables = (Set<Variable>) (invocation.getArguments())[1];
				Set<Variable> derivedVariables = (Set<Variable>) (invocation.getArguments())[2];
				primitiveVariables.add(v1);
				primitiveVariables.add(v2);
				primitiveVariables.add(v3);
				derivedVariables.add(v4);
				derivedVariables.add(v5);
				derivedVariables.add(v6);
				return null;
			}
		}).when(fireRulesDataValidator).validateProductionProductFireRules(any(ProductionFireRulesInput.class),
				any(HashSet.class), any(HashSet.class));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Primitive", false))
				.thenReturn(Arrays.asList(this.v1, this.v2, this.v3));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Derived", false))
				.thenReturn(Arrays.asList(this.v4, this.v5, this.v6));
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 2, false)).thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", 2, false)).thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", 2, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", 2, false)).thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_Boolean", 2, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_String", 2, false)).thenReturn(this.v6);
		when(groupDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.g1, this.g1));
		when(expressionDao.findByGroupIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.e1, this.e2, this.e3));
		when(expressionDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.e4, this.e5, this.e6));
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		doAnswer(new Answer<DroolsInput>() {
			@Override
			public DroolsInput answer(final InvocationOnMock invocation) throws Throwable {
				final DroolsInput droolsInput = (DroolsInput) (invocation.getArguments())[0];
				Map<String, Object> outputScores = new HashMap<String, Object>();
				outputScores.put("Age", 10.0);
				outputScores.put("Gender", 8.0);
				outputScores.put("Salaried", 12.0);
				droolsInput.setOutputScores(outputScores);
				return droolsInput;
			}
		}).when(droolsEngine).getScore(any(DroolsInput.class), any(String.class), any(String.class), any(String.class));
		GenericAPIResponse response = fireRulesServices.productionFireRules(result, this.productionInput);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Production Fire Rules When Group Expression Evaluation Error
	// Occurs
	@Test
	@SuppressWarnings("unchecked")
	public void productionFireRules2() throws Exception {
		this.e1 = new Expression(1, "ID_1 + 2 * ID_2 / 0", true, false, null, this.g1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted("abcdef", true, false)).thenReturn(this.pd1);
		doAnswer(new Answer<GenericAPIResponse>() {
			@Override
			public GenericAPIResponse answer(final InvocationOnMock invocation) throws Throwable {
				Set<Variable> primitiveVariables = (Set<Variable>) (invocation.getArguments())[1];
				Set<Variable> derivedVariables = (Set<Variable>) (invocation.getArguments())[2];
				primitiveVariables.add(v1);
				primitiveVariables.add(v2);
				primitiveVariables.add(v3);
				derivedVariables.add(v4);
				derivedVariables.add(v5);
				derivedVariables.add(v6);
				return null;
			}
		}).when(fireRulesDataValidator).validateProductionProductFireRules(any(ProductionFireRulesInput.class),
				any(HashSet.class), any(HashSet.class));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Primitive", false))
				.thenReturn(Arrays.asList(this.v1, this.v2, this.v3));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Derived", false))
				.thenReturn(Arrays.asList(this.v4, this.v5, this.v6));
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 2, false)).thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", 2, false)).thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", 2, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", 2, false)).thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_Boolean", 2, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_String", 2, false)).thenReturn(this.v6);
		when(groupDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.g1, this.g1));
		when(expressionDao.findByGroupIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.e1, this.e2, this.e3));
		when(expressionDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.e4, this.e5, this.e6));
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		doAnswer(new Answer<DroolsInput>() {
			@Override
			public DroolsInput answer(final InvocationOnMock invocation) throws Throwable {
				final DroolsInput droolsInput = (DroolsInput) (invocation.getArguments())[0];
				Map<String, Object> outputScores = new HashMap<String, Object>();
				outputScores.put("Age", 10.0);
				outputScores.put("Gender", 8.0);
				outputScores.put("Salaried", 12.0);
				droolsInput.setOutputScores(outputScores);
				return droolsInput;
			}
		}).when(droolsEngine).getScore(any(DroolsInput.class), any(String.class), any(String.class), any(String.class));
		GenericAPIResponse response = fireRulesServices.productionFireRules(result, this.productionInput);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Production Fire Rules When Product Expression Evaluation Error
	// Occurs
	@Test
	@SuppressWarnings("unchecked")
	public void productionFireRules3() throws Exception {
		this.e4 = new Expression(2, "IF ( TRUE , 10.0 / 0 , 20.0 / 0)", false, true, this.p1, null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted("abcdef", true, false)).thenReturn(this.pd1);
		doAnswer(new Answer<GenericAPIResponse>() {
			@Override
			public GenericAPIResponse answer(final InvocationOnMock invocation) throws Throwable {
				Set<Variable> primitiveVariables = (Set<Variable>) (invocation.getArguments())[1];
				Set<Variable> derivedVariables = (Set<Variable>) (invocation.getArguments())[2];
				primitiveVariables.add(v1);
				primitiveVariables.add(v2);
				primitiveVariables.add(v3);
				derivedVariables.add(v4);
				derivedVariables.add(v5);
				derivedVariables.add(v6);
				return null;
			}
		}).when(fireRulesDataValidator).validateProductionProductFireRules(any(ProductionFireRulesInput.class),
				any(HashSet.class), any(HashSet.class));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Primitive", false))
				.thenReturn(Arrays.asList(this.v1, this.v2, this.v3));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Derived", false))
				.thenReturn(Arrays.asList(this.v4, this.v5, this.v6));
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 2, false)).thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", 2, false)).thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", 2, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", 2, false)).thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_Boolean", 2, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_String", 2, false)).thenReturn(this.v6);
		when(groupDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.g1, this.g1));
		when(expressionDao.findByGroupIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.e1, this.e2, this.e3));
		when(expressionDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.e4, this.e5, this.e6));
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		doAnswer(new Answer<DroolsInput>() {
			@Override
			public DroolsInput answer(final InvocationOnMock invocation) throws Throwable {
				final DroolsInput droolsInput = (DroolsInput) (invocation.getArguments())[0];
				Map<String, Object> outputScores = new HashMap<String, Object>();
				outputScores.put("Age", 10.0);
				outputScores.put("Gender", 8.0);
				outputScores.put("Salaried", 12.0);
				droolsInput.setOutputScores(outputScores);
				return droolsInput;
			}
		}).when(droolsEngine).getScore(any(DroolsInput.class), any(String.class), any(String.class), any(String.class));
		GenericAPIResponse response = fireRulesServices.productionFireRules(result, this.productionInput);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Production Fire Rules When GroupId Is Provided
	@Test
	@SuppressWarnings("unchecked")
	public void productionFireRules4() throws Exception {
		this.productionInput.setGroupId("1");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted("abcdef", true, false)).thenReturn(this.pd1);
		doAnswer(new Answer<GenericAPIResponse>() {
			@Override
			public GenericAPIResponse answer(final InvocationOnMock invocation) throws Throwable {
				Set<Variable> primitiveVariables = (Set<Variable>) (invocation.getArguments())[1];
				Set<Variable> derivedVariables = (Set<Variable>) (invocation.getArguments())[2];
				primitiveVariables.add(v1);
				primitiveVariables.add(v2);
				primitiveVariables.add(v3);
				derivedVariables.add(v4);
				derivedVariables.add(v5);
				derivedVariables.add(v6);
				return null;
			}
		}).when(fireRulesDataValidator).validateProductionGroupFireRules(any(ProductionFireRulesInput.class),
				any(HashSet.class), any(HashSet.class));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Primitive", false))
				.thenReturn(Arrays.asList(this.v1, this.v2, this.v3));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Derived", false))
				.thenReturn(Arrays.asList(this.v4, this.v5, this.v6));
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 2, false)).thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", 2, false)).thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", 2, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", 2, false)).thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_Boolean", 2, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_String", 2, false)).thenReturn(this.v6);
		when(groupDao.findByIdAndIsDeleted(Long.parseLong("1"), false)).thenReturn(this.g1);
		when(expressionDao.findByGroupIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.e1, this.e2, this.e3));
		when(expressionDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.e4, this.e5, this.e6));
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		doAnswer(new Answer<DroolsInput>() {
			@Override
			public DroolsInput answer(final InvocationOnMock invocation) throws Throwable {
				final DroolsInput droolsInput = (DroolsInput) (invocation.getArguments())[0];
				Map<String, Object> outputScores = new HashMap<String, Object>();
				outputScores.put("Age", 10.0);
				outputScores.put("Gender", 8.0);
				outputScores.put("Salaried", 12.0);
				droolsInput.setOutputScores(outputScores);
				return droolsInput;
			}
		}).when(droolsEngine).getScore(any(DroolsInput.class), any(String.class), any(String.class), any(String.class));
		GenericAPIResponse response = fireRulesServices.productionFireRules(result, this.productionInput);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Production Fire Rules When Group Validator Return Error
	@Test
	@SuppressWarnings("unchecked")
	public void productionFireRules5() throws Exception {
		GenericAPIResponse invalidResponse = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INVALID_GROUP_ID_MESSAGE);
		this.productionInput.setGroupId("1");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted("abcdef", true, false)).thenReturn(this.pd1);
		when(fireRulesDataValidator.validateProductionGroupFireRules(any(ProductionFireRulesInput.class),
				any(HashSet.class), any(HashSet.class))).thenReturn(invalidResponse);

		GenericAPIResponse response = fireRulesServices.productionFireRules(result, this.productionInput);
		assertEquals(Constants.NOT_FOUND_ERROR_CODE, response.getStatus());
	}

	// Test For Test Fire Rules When No Validation Error Occurs
	@SuppressWarnings("unchecked")
	@Test
	public void testFireRules1() throws Exception {
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age-Numerical", "42");
		inputData.put("Gender-String", "abc");
		inputData.put("Salaried-Boolean", "true");
		this.testInput.setInputData(inputData);
		Set<Map<String, String>> allRequiredVariables = new HashSet<>();
		Map<String, String> variable = new HashMap<>();
		variable.put("name", "Age");
		variable.put("type", "Numerical");
		allRequiredVariables.add(variable);
		Map<String, String> variable2 = new HashMap<>();
		variable2.put("name", "Gender");
		variable2.put("type", "String");
		allRequiredVariables.add(variable2);
		Map<String, String> variable3 = new HashMap<>();
		variable3.put("name", "Salaried");
		variable3.put("type", "Boolean");
		allRequiredVariables.add(variable3);
		this.testInput.setUserId(Long.toString(this.user.getId()));
		when(productDao.findByIdAndIsDeleted(2, false)).thenReturn(this.p2);
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Primitive", false))
				.thenReturn(Arrays.asList(this.v1, this.v2, this.v3));
		when(variableDao.findByProductIdAndCategoryAndIsDeleted(2, "Derived", false))
				.thenReturn(Arrays.asList(this.v4, this.v5, this.v6));
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", 2, false)).thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", 2, false)).thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", 2, false)).thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", 2, false)).thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_Boolean", 2, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("DV_String", 2, false)).thenReturn(this.v6);
		when(groupDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.g1, this.g1));
		when(expressionDao.findByGroupIdAndIsDeleted(1, false)).thenReturn(Arrays.asList(this.e1, this.e2, this.e3));
		when(expressionDao.findByProductIdAndIsDeleted(2, false)).thenReturn(Arrays.asList(this.e4, this.e5, this.e6));
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		doAnswer(new Answer<Set<Map<String, String>>>() {
			@Override
			public Set<Map<String, String>> answer(final InvocationOnMock invocation) throws Throwable {
				Set<Variable> primitiveVariables = (Set<Variable>) (invocation.getArguments())[1];
				Set<Variable> derivedVariables = (Set<Variable>) (invocation.getArguments())[2];
				primitiveVariables.add(v1);
				primitiveVariables.add(v2);
				primitiveVariables.add(v3);
				derivedVariables.add(v4);
				derivedVariables.add(v5);
				derivedVariables.add(v6);
				Set<Map<String, String>> allRequiredVariables = new HashSet<>();
				Map<String, String> variable = new HashMap<>();
				variable.put("name", "Age");
				variable.put("type", "Numerical");
				allRequiredVariables.add(variable);
				Map<String, String> variable2 = new HashMap<>();
				variable2.put("name", "Gender");
				variable2.put("type", "String");
				allRequiredVariables.add(variable2);
				Map<String, String> variable3 = new HashMap<>();
				variable3.put("name", "Salaried");
				variable3.put("type", "Boolean");
				allRequiredVariables.add(variable3);
				return allRequiredVariables;
			}
		}).when(fireRulesDataValidator).findAllVariablesRequiredForProduct(any(Long.class), any(HashSet.class),
				any(HashSet.class));
		when(fireRulesDataValidator.validateTestEachProductFireRules(any(Long.class), any(Long.class), any(Map.class),
				any(Set.class))).thenReturn(null);
		doAnswer(new Answer<DroolsInput>() {
			@Override
			public DroolsInput answer(final InvocationOnMock invocation) throws Throwable {
				final DroolsInput droolsInput = (DroolsInput) (invocation.getArguments())[0];
				Map<String, Object> outputScores = new HashMap<String, Object>();
				outputScores.put("Age", 10.0);
				outputScores.put("Gender", 8.0);
				outputScores.put("Salaried", true);
				droolsInput.setOutputScores(outputScores);
				return droolsInput;
			}
		}).when(droolsEngine).getScore(any(DroolsInput.class), any(String.class), any(String.class), any(String.class));
		GenericAPIResponse response = fireRulesServices.testFireRules(this.testInput);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	// Test For Test Fire Rules When One Of The Product Validation Fails
	@SuppressWarnings("unchecked")
	@Test
	public void testFireRules2() throws Exception {
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age-Numerical", "42");
		inputData.put("Gender-String", "abc");
		inputData.put("Salaried-Boolean", "true");
		this.testInput.setInputData(inputData);
		Set<Map<String, String>> allRequiredVariables = new HashSet<>();
		Map<String, String> variable = new HashMap<>();
		variable.put("name", "Age");
		variable.put("type", "Numerical");
		allRequiredVariables.add(variable);
		Map<String, String> variable2 = new HashMap<>();
		variable2.put("name", "Gender");
		variable2.put("type", "String");
		allRequiredVariables.add(variable2);
		Map<String, String> variable3 = new HashMap<>();
		variable3.put("name", "Salaried");
		variable3.put("type", "Boolean");
		allRequiredVariables.add(variable3);
		this.testInput.setUserId(Long.toString(this.user.getId()));
		GenericAPIResponse invalidResponse = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INVALID_PRODUCT_ID_MESSAGE);
		when(fireRulesDataValidator.findAllVariablesRequiredForProduct(any(Long.class), any(HashSet.class),
				any(HashSet.class))).thenReturn(allRequiredVariables);
		when(fireRulesDataValidator.validateTestEachProductFireRules(any(Long.class), any(Long.class), any(Map.class),
				any(Set.class))).thenReturn(invalidResponse);
		GenericAPIResponse response = fireRulesServices.testFireRules(this.testInput);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void getAllRequiredVariables1() throws Exception {
		Set<Map<String, String>> allRequiredVariables = new HashSet<>();
		Map<String, String> variable = new HashMap<>();
		variable.put("name", "Age");
		variable.put("type", "Numerical");
		allRequiredVariables.add(variable);
		Map<String, String> variable2 = new HashMap<>();
		variable2.put("name", "Gender");
		variable2.put("type", "String");
		allRequiredVariables.add(variable2);
		Map<String, String> variable3 = new HashMap<>();
		variable3.put("name", "Salaried");
		variable3.put("type", "Boolean");
		allRequiredVariables.add(variable3);
		this.getRequiredVariables.setUserId(Long.toString(this.user.getId()));
		when(fireRulesDataValidator.findAllVariablesRequiredForProduct(any(Long.class), any(HashSet.class),
				any(HashSet.class))).thenReturn(allRequiredVariables);
		GenericAPIResponse response = fireRulesServices.getAllRequiredVariables(this.getRequiredVariables);
		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
	}

}
