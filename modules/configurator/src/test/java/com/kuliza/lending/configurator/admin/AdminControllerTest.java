package com.kuliza.lending.configurator.admin;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.controllers.AdminController;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProductCategory;
import com.kuliza.lending.configurator.service.adminservice.AdminServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.AdminDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
//@DataJpaTest
@WebMvcTest(AdminController.class)
public class AdminControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;

//	@MockBean
//	private UserTokenDao userTokenDao;

	private MockMvc mvc;

	@MockBean
	private AdminServices adminServices;

	@MockBean
	private AdminDataValidator adminDataValidator;

	private ProductCategory pc1;
	private String userId;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.userId = "1";

	}

	// Test For Create Product Category When Validator Return No Error
	@Test
	public void createProductCategory1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.pc1);
		when(adminServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(adminServices.createProductCategory(any(SubmitNewProductCategory.class))).thenReturn(response);
		mvc.perform(post("/admin/product-category").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewProductCategory("ScoreCard"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
	}

	// Test For Create Product Category When Validator Return Error
	@Test
	public void createProductCategory2() throws Exception {
		GenericAPIResponse errorResponse = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(adminServices.checkErrors(any(BindingResult.class))).thenReturn(errorResponse);
		mvc.perform(post("/admin/product-category").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewProductCategory("ScoreCard"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
	}

	// Test For Create Product Category When Service Return Exception
	@Test
	public void createProductCategory3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		RuntimeException e = new RuntimeException();
		when(adminServices.checkErrors(any(BindingResult.class))).thenReturn(null);
		when(adminServices.createProductCategory(any(SubmitNewProductCategory.class))).thenThrow(e);
		when(adminServices.handleException(eq(e), any(HttpServletRequest.class), eq(this.userId),
				any(SubmitNewProductCategory.class))).thenReturn(response);
		mvc.perform(post("/admin/product-category").requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(new SubmitNewProductCategory("ScoreCard"))))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
	}

}
