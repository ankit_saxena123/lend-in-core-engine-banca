//package com.kuliza.lending.configurator.user;
//
//import static org.hamcrest.Matchers.is;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.eq;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.kuliza.lending.configurator.controllers.UserControllers;
//import com.kuliza.lending.configurator.models.User;
//import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
//import com.kuliza.lending.configurator.pojo.SubmitNewUser;
//import com.kuliza.lending.configurator.pojo.SubmitUserCredentials;
//import com.kuliza.lending.configurator.utils.Constants;
//import com.kuliza.lending.configurator.validators.UserDataValidator;
//
//@RunWith(SpringRunner.class)
//@ActiveProfiles("test")
//@WebMvcTest(UserControllers.class)
//public class UserControllerTest {
//
//	@Autowired
//	private ObjectMapper objectMapper;
//
//	@Autowired
//	private WebApplicationContext context;
//
////	@MockBean
////	private UserTokenDao userTokenDao;
//
//	private MockMvc mvc;
//
////	@MockBean
////	private UserManagementServices userManagementServices;
//
//	@MockBean
//	private UserDataValidator userDataValidator;
//
//	private User user;
//	private String userId;
//	private String token;
//	//private UserToken userToken;
//
//	@Before
//	public void init() {
//		mvc = MockMvcBuilders.webAppContextSetup(context).build();
//		this.userId = "";
//		this.user = new User(1, "arpit.agrawal@kuliza.com");
//		this.token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBcnBpdCIsInJvbGVzIjoidXNlciIsInVzZXJJZCI6IjEiLCJpYXQiOjE0OTk0MTI5MTV9.FxpTUDPFrLhA3pODG-c_9D4Qr8puZ5j2kXpW9pC9wzQ";
//		//this.userToken = new UserToken(1, user, token);
//	}

	// TODO: with Keycloak
	
//	// Test Submit User When Validator Return No Error
//	@Test
//	public void createUser1() throws Exception {
//		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
//				this.user);
//		when(userManagementServices.checkErrors(any(BindingResult.class))).thenReturn(null);
//		when(userManagementServices.createNewUser(any(SubmitNewUser.class))).thenReturn(response);
//		mvc.perform(
//				post("/register").accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
//						.content(this.objectMapper.writeValueAsString(
//								new SubmitNewUser("Arpit", "qwert", "Kuliza", "arpit.agrawal@kuliza.com"))))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
//	}
//
//	// Test Submit User When Validator Return Error
//	@Test
//	public void createUser2() throws Exception {
//		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
//				Constants.FAILURE_MESSAGE, null);
//		when(userManagementServices.checkErrors(any(BindingResult.class))).thenReturn(response);
//		mvc.perform(
//				post("/register").accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
//						.content(this.objectMapper.writeValueAsString(
//								new SubmitNewUser("Arpit", "qwert", "Kuliza", "arpit.agrawal@kuliza.com"))))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));
//	}
//
//	// Test Submit User When Service Throws Exception
//	@Test
//	public void createUser3() throws Exception {
//		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
//				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
//		when(userManagementServices.checkErrors(any(BindingResult.class))).thenReturn(null);
//		when(userManagementServices.createNewUser(any(SubmitNewUser.class))).thenThrow(new Exception());
//		when(userManagementServices.handleException(any(Exception.class), any(HttpServletRequest.class),
//				eq(this.userId), any(SubmitNewUser.class))).thenReturn(response);
//		mvc.perform(
//				post("/register").accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
//						.content(this.objectMapper.writeValueAsString(
//								new SubmitNewUser("Arpit", "qwert", "Kuliza", "arpit.agrawal@kuliza.com"))))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
//	}
//
//	// Test Submit User When Validator Return No Error
//	@Test
//	public void loginUser1() throws Exception {
//		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
//				this.userToken);
//		when(userManagementServices.validateLoginUser(any(SubmitUserCredentials.class), any(BindingResult.class)))
//				.thenReturn(null);
//		when(userManagementServices.loginUser(any(SubmitUserCredentials.class))).thenReturn(response);
//		mvc.perform(
//				post("/login").accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
//						.content(this.objectMapper
//								.writeValueAsString(new SubmitUserCredentials("arpit.agrawal@kuliza.com", "abdef"))))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
//	}
//
//	// Test Submit User When Validator Return Error
//	@Test
//	public void loginUser2() throws Exception {
//		GenericAPIResponse response = new GenericAPIResponse(Constants.AUTHORIZATION_FAILED_STATUS_CODE,
//				Constants.FAILURE_MESSAGE, Constants.INVALID_PASSWORD_MESSAGE);
//		when(userManagementServices.validateLoginUser(any(SubmitUserCredentials.class), any(BindingResult.class)))
//				.thenReturn(response);
//		mvc.perform(
//				post("/login").accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
//						.content(this.objectMapper
//								.writeValueAsString(new SubmitUserCredentials("arpit.agrawal@kuliza.com", "abde"))))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$.status", is(Constants.AUTHORIZATION_FAILED_STATUS_CODE)));
//	}
//
//	// Test Submit User When Service Throws Exception
//	@Test
//	public void loginUser3() throws Exception {
//		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
//				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
//		when(userManagementServices.validateLoginUser(any(SubmitUserCredentials.class), any(BindingResult.class)))
//				.thenReturn(null);
//		when(userManagementServices.loginUser(any(SubmitUserCredentials.class))).thenThrow(new Exception());
//		when(userManagementServices.handleException(any(Exception.class), any(HttpServletRequest.class),
//				eq(this.userId), any(SubmitUserCredentials.class))).thenReturn(response);
//		mvc.perform(
//				post("/login").accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
//						.content(this.objectMapper
//								.writeValueAsString(new SubmitUserCredentials("arpit.agrawal@kuliza.com", "abdef"))))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
//	}
//
//	// Test Submit User When Validator Return No Error
//	@Test
//	public void logoutUser1() throws Exception {
//		this.userId = "1";
//		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
//				Constants.LOGOUT_SUCCESS_MESSAGE);
//		when(userManagementServices.validateLogoutUser(this.userId)).thenReturn(null);
//		when(userManagementServices.logoutUser(this.userId)).thenReturn(response);
//		mvc.perform(get("/api/logout").requestAttr("userId", this.userId))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));
//	}
//
//	// Test Submit User When Validator Return Error
//	@Test
//	public void logoutUser2() throws Exception {
//		this.userId = "1";
//		GenericAPIResponse response = new GenericAPIResponse(Constants.AUTHORIZATION_FAILED_STATUS_CODE,
//				Constants.FAILURE_MESSAGE, Constants.INVALID_TOKEN_MESSAGE);
//		when(userManagementServices.validateLogoutUser(this.userId)).thenReturn(response);
//		mvc.perform(get("/api/logout").requestAttr("userId", this.userId))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$.status", is(Constants.AUTHORIZATION_FAILED_STATUS_CODE)));
//	}
//
//	// Test Submit User When Service Throws Exception
//	@Test
//	public void logoutUser3() throws Exception {
//		this.userId = "1";
//		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
//				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
//		when(userManagementServices.validateLogoutUser(this.userId)).thenReturn(null);
//		when(userManagementServices.logoutUser(this.userId)).thenThrow(new Exception());
//		when(userManagementServices.handleException(any(Exception.class), any(HttpServletRequest.class),
//				eq(this.userId))).thenReturn(response);
//		mvc.perform(get("/api/logout").requestAttr("userId", this.userId))
//				// .andDo(MockMvcResultHandlers.print())
//				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));
//	}

//}
