//package com.kuliza.lending.configurator.user;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertNull;
//import static org.mockito.Mockito.when;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.validation.BeanPropertyBindingResult;
//import org.springframework.validation.BindingResult;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.kuliza.lending.configurator.CreditEngineApplication;
//import com.kuliza.lending.configurator.models.User;
//import com.kuliza.lending.configurator.models.UserDao;
//import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
//import com.kuliza.lending.configurator.pojo.SubmitNewUser;
//import com.kuliza.lending.configurator.pojo.SubmitUserCredentials;
//import com.kuliza.lending.configurator.utils.Constants;
//
//@RunWith(SpringRunner.class)
//@ActiveProfiles("test")
//@SpringBootTest(classes = CreditEngineApplication.class)
//@WebAppConfiguration
//public class UserServiceTest {
//
//	@Mock
//	private ObjectMapper objectMapper;
//
//	@Mock
//	private UserDao userDao;
//
////	@Mock
////	private UserTokenDao userTokenDao;
////
////	@InjectMocks
////	private UserManagementServices userManagementServices;
//
//	private User user;
//	private String token;
//	//private UserToken userToken;
//
//	@Before
//	public void init() {
//		this.user = new User(1, "arpit.agrawal@kuliza.com");
//		this.token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBcnBpdCIsInJvbGVzIjoidXNlciIsInVzZXJJZCI6IjEiLCJpYXQiOjE0OTk0MTI5MTV9.FxpTUDPFrLhA3pODG-c_9D4Qr8puZ5j2kXpW9pC9wzQ";
//		//this.userToken = new UserToken(1, user, token);
//	}
	
	// TODO: Tests for User with keycloak 

	// Test For Service For Creating New User
//	@Test
//	public void createNewUser() throws Exception {
//		SubmitNewUser input = new SubmitNewUser("Arpit", "qwerty", "Kuliza", "arpit.agrawal@kuliza.com");
//		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
//		when(userTokenDao.findByUserIdAndIsDeleted(user.getId(), false)).thenReturn(this.userToken);
//		GenericAPIResponse response = userManagementServices.createNewUser(input);
//		UserToken responseToken = (UserToken) response.getData();
//		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
//		assertEquals(this.token, responseToken.getToken());
//	}
//
//	// Test For Validate Login User When No Validation Error Occurs and User
//	// Exist
//	@Test
//	public void validateLoginUser1() throws Exception {
//		SubmitUserCredentials input = new SubmitUserCredentials("arpit.agrawal@kuliza.com", "qwert");
//		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
//		GenericAPIResponse response = userManagementServices.validateLoginUser(input,
//				new BeanPropertyBindingResult(input, "submitUserCredentials"));
//		assertNull(response);
//	}
//
//	// Test For Validate Login User When Validation Error Occurs
//	@Test
//	public void validateLoginUser2() throws Exception {
//		SubmitUserCredentials input = new SubmitUserCredentials("", "qwert");
//		BindingResult result = new BeanPropertyBindingResult(input, "submitUserCredentials");
//		result.rejectValue("userName", "userName.invalid", "User Name cannot be Empty");
//		GenericAPIResponse response = userManagementServices.validateLoginUser(input, result);
//		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
//		assertEquals(Constants.FAILURE_MESSAGE, response.getMessage());
//	}
//
//	// Test For Validate Login User When No Validation Error Occurs, But User
//	// Doesn't Exist
//	@Test
//	public void validateLoginUser3() throws Exception {
//		SubmitUserCredentials input = new SubmitUserCredentials("arpit@kuliza.com", "qwert");
//		when(userDao.findByUserName(input.getUserName())).thenReturn(null);
//		GenericAPIResponse response = userManagementServices.validateLoginUser(input,
//				new BeanPropertyBindingResult(input, "submitUserCredentials"));
//		assertEquals(Constants.AUTHORIZATION_FAILED_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.FAILURE_MESSAGE, response.getMessage());
//		assertEquals(Constants.INVALID_USERNAME_MESSAGE, response.getData());
//	}
//
//	// Test For Validate Login User When No Validation Error Occurs And User
//	// Exist But Password Doesn't Match
//	@Test
//	public void validateLoginUser4() throws Exception {
//		SubmitUserCredentials input = new SubmitUserCredentials("arpit.agrawal@kuliza.com", "qwerty");
//		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
//		GenericAPIResponse response = userManagementServices.validateLoginUser(input,
//				new BeanPropertyBindingResult(input, "submitUserCredentials"));
//		assertEquals(Constants.AUTHORIZATION_FAILED_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.FAILURE_MESSAGE, response.getMessage());
//		assertEquals(Constants.INVALID_PASSWORD_MESSAGE, response.getData());
//	}
//
//	// Test For Login User When User Token Doesn't Exist
//	@Test
//	public void loginUser1() throws Exception {
//		SubmitUserCredentials input = new SubmitUserCredentials("arpit.agrawal@kuliza.com", "qwerty");
//		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
//		when(userTokenDao.findByUserIdAndIsDeleted(user.getId(), false)).thenReturn(null);
//		GenericAPIResponse response = userManagementServices.loginUser(input);
//		UserToken responseToken = (UserToken) response.getData();
//		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
//		assertNotNull(responseToken);
//	}
//
//	// Test For Login User When User Token Exist
//	@Test
//	public void loginUser2() throws Exception {
//		SubmitUserCredentials input = new SubmitUserCredentials("arpit.agrawal@kuliza.com", "qwerty");
//		when(userDao.findByUserName(input.getUserName())).thenReturn(this.user);
//		when(userTokenDao.findByUserIdAndIsDeleted(user.getId(), false)).thenReturn(this.userToken);
//		GenericAPIResponse response = userManagementServices.loginUser(input);
//		UserToken responseToken = (UserToken) response.getData();
//		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
//		assertEquals(this.userToken.getToken(), responseToken.getToken());
//	}
//
//	// Test For Logout User
//	@Test
//	public void logoutUser() throws Exception {
//		when(userTokenDao.findByUserIdAndIsDeleted(user.getId(), false)).thenReturn(this.userToken);
//		GenericAPIResponse response = userManagementServices.logoutUser(Long.toString(user.getId()));
//		assertEquals(Constants.SUCCESS_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.SUCCESS_MESSAGE, response.getMessage());
//		assertEquals(Constants.LOGOUT_SUCCESS_MESSAGE, response.getData());
//	}
//
//	// Test For Validate Logout User when userToken is Not Null
//	@Test
//	public void validateLogoutUser1() throws Exception {
//		when(userTokenDao.findByUserIdAndIsDeleted(user.getId(), false)).thenReturn(this.userToken);
//		GenericAPIResponse response = userManagementServices.validateLogoutUser(Long.toString(user.getId()));
//		assertNull(response);
//	}
//
//	// Test For Validate Logout User when userToken is Null
//	@Test
//	public void validateLogoutUser2() throws Exception {
//		when(userTokenDao.findByUserIdAndIsDeleted(user.getId(), false)).thenReturn(null);
//		GenericAPIResponse response = userManagementServices.validateLogoutUser(Long.toString(user.getId()));
//		assertEquals(Constants.AUTHORIZATION_FAILED_STATUS_CODE, response.getStatus());
//		assertEquals(Constants.FAILURE_MESSAGE, response.getMessage());
//		assertEquals(Constants.INVALID_TOKEN_MESSAGE, response.getData());
//	}

//}
