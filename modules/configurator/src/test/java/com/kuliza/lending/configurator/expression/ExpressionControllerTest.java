package com.kuliza.lending.configurator.expression;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.controllers.ExpressionControllers;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewExpression;
import com.kuliza.lending.configurator.pojo.SubmitNewExpressions;
import com.kuliza.lending.configurator.pojo.UpdateExpression;
import com.kuliza.lending.configurator.service.expressionservice.ExpressionServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.ExpressionDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebMvcTest(ExpressionControllers.class)
@WebAppConfiguration
public class ExpressionControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;

//	@MockBean
//	private UserTokenDao userTokenDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private GroupDao groupDao;
	
	@Mock
	private ProductDao productDao;

	@Mock
	private ExpressionDao expressionDao;

	private MockMvc mvc;

	@MockBean
	private ExpressionServices expressionServices;

	@MockBean
	private ExpressionDataValidator expressionDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Group g1;
	private Variable v1;
	private Expression e1;
	private Expression e2;
	private String userId;
	private String productId;
	private String groupId;
	private String expressionId;
	
	@InjectMocks
	private ExpressionControllers expressionControllers;

	@Before
	public void init() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.userId = "1";
		this.productId = "1";
		this.groupId = "1";
		this.expressionId = "1";
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_1", true, false, null, this.g1);
		this.e2 = new Expression(2, "AND ( ID_1 , TRUE )", false, true, this.p1, null);
	}

	// Test For Get Product Expression When Product Belongs To User
	@Test
	public void getProductExpressions1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.e2));
		when(expressionDataValidator.validateGetProductExpressions(this.userId, this.productId)).thenReturn(true);
		when(expressionServices.getExpressions(this.productId, "")).thenReturn(response);
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		mvc.perform(get("/api/product/{productId}/expression", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Get Product Expression When Product Doesn't Belongs To User
	@Test
	public void getProductExpressions2() throws Exception {
		when(expressionDataValidator.validateGetProductExpressions(this.userId, this.productId)).thenReturn(false);
		mvc.perform(get("/api/product/{productId}/expression", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));

	}

	// Test For Get Product Expression When Service Throws Exception
	@Test
	public void getProductExpressions3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(expressionDataValidator.validateGetProductExpressions(this.userId, this.productId)).thenReturn(true);
		when(expressionServices.getExpressions(this.productId, "")).thenThrow(new Exception());
		when(expressionServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(get("/api/product/{productId}/expression", this.productId).requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Get Group Expression When Product Belongs To User And Group
	// Belongs To Product
	@Test
	public void getGroupExpressions1() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.e1));
		when(expressionDataValidator.validateGetGroupExpressions(this.userId, this.productId, this.groupId))
				.thenReturn(true);
		when(expressionServices.getExpressions(this.productId, this.groupId)).thenReturn(response);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		mvc.perform(get("/api/product/{productId}/group/{groupId}/expression", this.productId, this.groupId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Get Group Expression When Product Doesn't Belongs To User Or
	// Group Doesn't Belongs To Product
	@Test
	public void getGroupExpressions2() throws Exception {
		when(expressionDataValidator.validateGetGroupExpressions(this.userId, this.productId, this.groupId))
				.thenReturn(false);
		mvc.perform(get("/api/product/{productId}/group/{groupId}/expression", this.productId, this.groupId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.NOT_FOUND_ERROR_CODE)));

	}

	// Test For Get Group Expression When Service Throws Exception
	@Test
	public void getGroupExpressions3() throws Exception {
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(expressionDataValidator.validateGetGroupExpressions(this.userId, this.productId, this.groupId))
				.thenReturn(true);
		when(expressionServices.getExpressions(this.productId, this.groupId)).thenThrow(new Exception());
		when(expressionServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId)))
				.thenReturn(response);
		mvc.perform(get("/api/product/{productId}/group/{groupId}/expression", this.productId, this.groupId)
				.requestAttr("userId", this.userId))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Create Product Expression When No Validation Error Occurs
	@Test
	public void createProductExpression1() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.e2));
		when(expressionServices.validateSubmitExpressions(any(SubmitNewExpressions.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId), eq(""))).thenReturn(null);
		when(expressionServices.createNewExpressions(any(SubmitNewExpressions.class))).thenReturn(response);
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		mvc.perform(post("/api/product/{productId}/expression", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Create Product Expression When Validation Error Occurs
	@Test
	public void createProductExpression2() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(expressionServices.validateSubmitExpressions(any(SubmitNewExpressions.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId), eq(""))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/expression", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Create Product Expression When Service Throws Exception
	@Test
	public void createProductExpression3() throws Exception {
		NewExpression newExpression = new NewExpression("AND(Personal_Eligibility,TRUE)");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(expressionServices.validateSubmitExpressions(any(SubmitNewExpressions.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId), eq(""))).thenReturn(null);
		when(expressionServices.createNewExpressions(any(SubmitNewExpressions.class))).thenThrow(new Exception());
		when(expressionServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(SubmitNewExpressions.class))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/expression", this.productId).requestAttr("userId", this.userId)
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Create Group Expression When No Validation Error Occurs
	@Test
	public void createGroupExpression1() throws Exception {
		NewExpression newExpression = new NewExpression("Age+2*Age");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Arrays.asList(this.e1));
		when(expressionServices.validateSubmitExpressions(any(SubmitNewExpressions.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId), eq(this.groupId))).thenReturn(null);
		when(expressionServices.createNewExpressions(any(SubmitNewExpressions.class))).thenReturn(response);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		mvc.perform(post("/api/product/{productId}/group/{groupId}/expression", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Create Group Expression When Validation Error Occurs
	@Test
	public void createGroupExpression2() throws Exception {
		NewExpression newExpression = new NewExpression("Age+2*Age");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(expressionServices.validateSubmitExpressions(any(SubmitNewExpressions.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId), eq(this.groupId))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/group/{groupId}/expression", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Create Group Expression When Service Throws Exception
	@Test
	public void createGroupExpression3() throws Exception {
		NewExpression newExpression = new NewExpression("Age+2*Age");
		SubmitNewExpressions input = new SubmitNewExpressions(Arrays.asList(newExpression));
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(expressionServices.validateSubmitExpressions(any(SubmitNewExpressions.class), any(BindingResult.class),
				eq(this.userId), eq(this.productId), eq(this.groupId))).thenReturn(null);
		when(expressionServices.createNewExpressions(any(SubmitNewExpressions.class))).thenThrow(new Exception());
		when(expressionServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(SubmitNewExpressions.class))).thenReturn(response);
		mvc.perform(post("/api/product/{productId}/group/{groupId}/expression", this.productId, this.groupId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Update Product Expression When No Validation Error Occurs
	@Test
	public void updateProductExpression1() throws Exception {
		this.expressionId = "2";
		this.e2.setExpressionString("OR ( ID_1 , FALSE )");
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.e2);
		when(expressionServices.validateUpdateExpression(eq(this.userId), eq(this.productId), eq(""),
				eq(this.expressionId), any(UpdateExpression.class), any(BindingResult.class))).thenReturn(null);
		when(expressionServices.updateExpression(any(UpdateExpression.class))).thenReturn(response);
		when(groupDao.findByIdAndIsDeleted(1, false)).thenReturn(this.g1);
		mvc.perform(put("/api/product/{productId}/expression/{expressionId}", this.productId, this.expressionId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Update Product Expression When Validation Error Occurs
	@Test
	public void updateProductExpression2() throws Exception {
		this.expressionId = "2";
		this.e2.setExpressionString("OR ( ID_1 , FALSE )");
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(expressionServices.validateUpdateExpression(eq(this.userId), eq(this.productId), eq(""),
				eq(this.expressionId), any(UpdateExpression.class), any(BindingResult.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/expression/{expressionId}", this.productId, this.expressionId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Update Product Expression When Service Throws Exception
	@Test
	public void updateProductExpression3() throws Exception {
		this.expressionId = "2";
		this.e2.setExpressionString("OR ( ID_1 , FALSE )");
		NewExpression newExpression = new NewExpression("OR(Personal_Eligibility,FALSE)");
		UpdateExpression input = new UpdateExpression(newExpression);
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(expressionServices.validateUpdateExpression(eq(this.userId), eq(this.productId), eq(""),
				eq(this.expressionId), any(UpdateExpression.class), any(BindingResult.class))).thenReturn(null);
		when(expressionServices.updateExpression(any(UpdateExpression.class))).thenThrow(new Exception());
		when(expressionServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(UpdateExpression.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/expression/{expressionId}", this.productId, this.expressionId)
				.requestAttr("userId", this.userId).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

	// Test For Update Group Expression When No Validation Error Occurs
	@Test
	public void updateGroupExpression1() throws Exception {
		this.e1.setExpressionString("ID_1 + 2 * ID_1 + 3 * ID_1");
		NewExpression newExpression = new NewExpression("Age+2*Age+3*Age");
		UpdateExpression input = new UpdateExpression(newExpression);
		GenericAPIResponse response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				this.e1);
		when(expressionServices.validateUpdateExpression(eq(this.userId), eq(this.productId), eq(this.groupId),
				eq(this.expressionId), any(UpdateExpression.class), any(BindingResult.class))).thenReturn(null);
		when(expressionServices.updateExpression(any(UpdateExpression.class))).thenReturn(response);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		mvc.perform(put("/api/product/{productId}/group/{groupId}/expression/{expressionId}", this.productId,
				this.groupId, this.expressionId).requestAttr("userId", this.userId)
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(this.objectMapper.writeValueAsString(input)))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.SUCCESS_STATUS_CODE)));

	}

	// Test For Update Group Expression When Validation Error Occurs
	@Test
	public void updateGroupExpression2() throws Exception {
		this.e1.setExpressionString("ID_1 + 2 * ID_1 + 3 * ID_1");
		NewExpression newExpression = new NewExpression("Age+2*Age+3*Age");
		UpdateExpression input = new UpdateExpression(newExpression);
		GenericAPIResponse response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
				Constants.FAILURE_MESSAGE, null);
		when(expressionServices.validateUpdateExpression(eq(this.userId), eq(this.productId), eq(this.groupId),
				eq(this.expressionId), any(UpdateExpression.class), any(BindingResult.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/group/{groupId}/expression/{expressionId}", this.productId,
				this.groupId, this.expressionId).requestAttr("userId", this.userId)
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status", is(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE)));

	}

	// Test For Update Group Expression When Service Throws Exception
	@Test
	public void updateGroupExpression3() throws Exception {
		this.e1.setExpressionString("ID_1 + 2 * ID_1 + 3 * ID_1");
		NewExpression newExpression = new NewExpression("Age+2*Age+3*Age");
		UpdateExpression input = new UpdateExpression(newExpression);
		GenericAPIResponse response = new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE,
				Constants.FAILURE_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		when(expressionServices.validateUpdateExpression(eq(this.userId), eq(this.productId), eq(this.groupId),
				eq(this.expressionId), any(UpdateExpression.class), any(BindingResult.class))).thenReturn(null);
		when(expressionServices.updateExpression(any(UpdateExpression.class))).thenThrow(new Exception());
		when(expressionServices.handleException(any(Exception.class), any(HttpServletRequest.class), eq(this.userId),
				any(UpdateExpression.class))).thenReturn(response);
		mvc.perform(put("/api/product/{productId}/group/{groupId}/expression/{expressionId}", this.productId,
				this.groupId, this.expressionId).requestAttr("userId", this.userId)
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(this.objectMapper.writeValueAsString(input)))
				// .andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.status", is(Constants.INTERNAL_SERVER_ERROR_CODE)));

	}

}
