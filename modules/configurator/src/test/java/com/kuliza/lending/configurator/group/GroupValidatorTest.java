package com.kuliza.lending.configurator.group;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.pojo.SubmitNewGroup;
import com.kuliza.lending.configurator.pojo.UpdateGroup;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.GroupDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class GroupValidatorTest {

	@Mock
	private GroupDao groupDao;

	@Mock
	private ProductDao productDao;

	@InjectMocks
	private GroupDataValidator groupDataValidator;

	private User user;
	private ProductCategory pc1;
	private Product p1;
	private Group g1;
	private String userId;
	private String productId;
	private String groupId;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.groupId = "1";
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
	}

	// Test For Get All Groups When Product Belong To User
	@Test
	public void validateGetAllGroups1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		assertEquals(true, groupDataValidator.validateGetAllGroup(this.userId, this.productId));
	}

	// Test For Get All Groups When Product Doesn't Belong To User
	@Test
	public void validateGetAllGroups2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false, groupDataValidator.validateGetAllGroup(this.userId, this.productId));
	}

	// Test For Get Single Groups When Product Belong To User And Group Belong
	// To Product
	@Test
	public void validateGetSingleGroups1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		assertEquals(true, groupDataValidator.validateGetSingleGroup(this.userId, this.productId, this.groupId));
	}

	// Test For Get Single Groups When Product Belong To User But Group Doesn't
	// Belong To Product
	@Test
	public void validateGetSingleGroups2() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		assertEquals(false, groupDataValidator.validateGetSingleGroup(this.userId, this.productId, this.groupId));
	}

	// Test For Get Single Groups When Product Doesn't Belong To User
	@Test
	public void validateGetSingleGroups3() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false, groupDataValidator.validateGetSingleGroup(this.userId, this.productId, this.groupId));
	}

	// Test For Create New Group When No Validation Error Occurs
	@Test
	public void validateNewGroup1() throws Exception {
		SubmitNewGroup input = new SubmitNewGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByNameAndProductIdAndIsDeleted(input.getGroupName(), Long.parseLong(this.productId), false))
				.thenReturn(null);
		groupDataValidator.validateNewGroup(input, result);
		assertEquals(false, result.hasErrors());

	}

	// Test For Create New Group When Field Validation Error Occurs
	@Test
	public void validateNewGroup2() throws Exception {
		SubmitNewGroup input = new SubmitNewGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewGroup");
		result.rejectValue("groupName", "groupName.invalid", Constants.INVALID_GROUP_NAME_MESSAGE);
		groupDataValidator.validateNewGroup(input, result);
		assertEquals(true, result.hasErrors());

	}

	// Test For Create New Group When Product Doesn't Belong To User
	@Test
	public void validateNewGroup3() throws Exception {
		SubmitNewGroup input = new SubmitNewGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		groupDataValidator.validateNewGroup(input, result);
		assertEquals(true, result.hasErrors());

	}

	// Test For Create New Group When Product Belongs to User But Product Is In
	// Deployed or Undeployed State
	@Test
	public void validateNewGroup4() throws Exception {
		SubmitNewGroup input = new SubmitNewGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(2);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		groupDataValidator.validateNewGroup(input, result);
		assertEquals(true, result.hasErrors());

	}

	// Test For Create New Group When Product Belongs to User AAnd Product Is In
	// Edit State But Group Name is Already Taken
	@Test
	public void validateNewGroup5() throws Exception {
		SubmitNewGroup input = new SubmitNewGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByNameAndProductIdAndIsDeleted(input.getGroupName(), Long.parseLong(this.productId), false))
				.thenReturn(this.g1);
		groupDataValidator.validateNewGroup(input, result);
		assertEquals(true, result.hasErrors());

	}

	// Test For Update Group When No Validation Error Occurs
	@Test
	public void validateUpdateGroup1() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility_New");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		when(groupDao.findByNameAndProductIdAndIsDeleted(input.getGroupName(), Long.parseLong(this.productId), false))
				.thenReturn(null);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Update Group When Field Validation Error Occurs
	@Test
	public void validateUpdateGroup2() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility_New");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		result.rejectValue("groupName", "groupName.invalid", Constants.INVALID_GROUP_NAME_MESSAGE);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Group When Product Doesn't Belong To User
	@Test
	public void validateUpdateGroup3() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility_New");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Group When Product Belongs To User But Product Is In
	// Deployed/UnDeployed State
	@Test
	public void validateUpdateGroup4() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility_New");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		this.p1.setStatus(2);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Group When Product Belongs To User And Product Is In
	// Edit But Group Doesn't Belong To Product
	@Test
	public void validateUpdateGroup5() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility_New");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Group When Product Belongs To User And Product Is In
	// Edit Group Belong to Product But Group Name Already Taken
	@Test
	public void validateUpdateGroup6() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId("2");
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(input.getGroupId()),
				Long.parseLong(this.productId), false)).thenReturn(this.g1);
		when(groupDao.findByNameAndProductIdAndIsDeleted(input.getGroupName(), Long.parseLong(this.productId), false))
				.thenReturn(this.g1);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Update Group When Product Belongs To User And Product Is In
	// Edit Group Belong to Product And Same Is Updated By Same Name
	@Test
	public void validateUpdateGroup7() throws Exception {
		UpdateGroup input = new UpdateGroup("Personal_Eligibility");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setGroupId(this.groupId);
		this.p1.setStatus(0);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateGroup");
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		when(groupDao.findByNameAndProductIdAndIsDeleted(input.getGroupName(), Long.parseLong(this.productId), false))
				.thenReturn(this.g1);
		groupDataValidator.validateUpdateGroup(input, result);
		assertEquals(false, result.hasErrors());
	}

}
