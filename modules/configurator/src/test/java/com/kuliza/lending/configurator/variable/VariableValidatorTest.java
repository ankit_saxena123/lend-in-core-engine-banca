package com.kuliza.lending.configurator.variable;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.NewVariable;
import com.kuliza.lending.configurator.pojo.SubmitNewVariables;
import com.kuliza.lending.configurator.pojo.UpdateVariable;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.VariableDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class VariableValidatorTest {

	@Mock
	private VariableDao variableDao;

	@Mock
	private ProductDao productDao;

	@InjectMocks
	private VariableDataValidator variableDataValidator;

	private User user;
	private ProductCategory pc1;
	private Product p1;
	private Variable v1;
	private String userId;
	private String productId;
	private String variableId;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.variableId = "1";
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.p1 = new Product(1, "AXIS_Bank", 0, 2, "abdef", user, pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
	}

	// Test For Get All Variables When Product Belongs To User
	@Test
	public void validateGetAllVariables1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		assertEquals(true, variableDataValidator.validateGetAllVariables(this.userId, this.productId));
	}

	// Test For Get All Variables When Product Doesn't Belongs To User
	@Test
	public void validateGetAllVariables2() throws Exception {
		this.productId = "2";
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false, variableDataValidator.validateGetAllVariables(this.userId, this.productId));
	}

	// Test For Get Single Variable When Product Belongs To User And Variable
	// Belongs to Product
	@Test
	public void validateGetSingleVariable1() throws Exception {
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(this.v1);
		assertEquals(true,
				variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId));
	}

	// Test For Get Single Variables When Product Belongs To User But Variable
	// Doesn't Belong To Product
	@Test
	public void validateGetSingleVariable2() throws Exception {
		this.variableId = "2";
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(null);
		assertEquals(false,
				variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId));
	}

	// Test For Get Single Variables When Product Doesn't Belongs To User
	@Test
	public void validateGetSingleVariable3() throws Exception {
		this.productId = "2";
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		assertEquals(false,
				variableDataValidator.validateGetSingleVariable(this.userId, this.productId, this.variableId));
	}

	// Test For Validate A New Variable When No Validation Error Occurs
	@Test
	public void validateVariable1() throws Exception {
		NewVariable input = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		when(variableDao.findByNameAndProductIdAndIsDeleted(input.getVariableName(), Long.parseLong(this.productId),
				false)).thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "newVariable");
		input.setProductId(this.productId);
		variableDataValidator.validateVariable(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate A New Variable When Field Level Validation Error Occurs
	@Test
	public void validateVariable2() throws Exception {
		NewVariable input = new NewVariable("Age Personal", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "newVariable");
		result.rejectValue("variableName", "variableName.invalid", Constants.INVALID_VARIABLE_NAME_MESSAGE);
		input.setProductId(this.productId);
		variableDataValidator.validateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate A New Variable When Variable Already Exists And
	// Variable Name Is Incorrect And Variable is Derived Type But Expression
	// String Is Empty
	@Test
	public void validateVariable3() throws Exception {
		NewVariable input = new NewVariable("OR", "Numerical", "Journey", "Derived", "Age Of Customer", "");
		when(variableDao.findByNameAndProductIdAndIsDeleted(input.getVariableName(), Long.parseLong(this.productId),
				false)).thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "newVariable");
		input.setProductId(this.productId);
		variableDataValidator.validateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate A New Variable When Variable Expression Is Correct
	@Test
	public void validateVariable4() throws Exception {
		NewVariable input = new NewVariable("Score", "Numerical", "Journey", "Derived", "Age Of Customer",
				"Age + 2*Age");
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "newVariable");
		input.setProductId(this.productId);
		variableDataValidator.validateVariable(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate A New Variable When Variable Expression Is InCorrect
	@Test
	public void validateVariable5() throws Exception {
		NewVariable input = new NewVariable("Score", "Numerical", "Journey", "Derived", "Age Of Customer", "Age + 2*");
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "newVariable");
		input.setProductId(this.productId);
		variableDataValidator.validateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate New Variables When Field Level Validation Error Occurs
	@Test
	public void validateSubmitVariables1() throws Exception {
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(new ArrayList<>());
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		result.rejectValue("newVariablesList", "newVariablesList.invalid", "Variable List cannot be empty");
		variableDataValidator.validateNewVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Submit Variables When Product Belong To User And
	// Product Is Not Deployed
	@Test
	public void validateSubmitVariables2() throws Exception {
		this.p1.setStatus(0);
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1));
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		variableDataValidator.validateNewVariable(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Submit Variables When Product Belong To User But
	// Product Is Deployed
	@Test
	public void validateSubmitVariables3() throws Exception {
		this.p1.setStatus(2);
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1));
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		variableDataValidator.validateNewVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Submit Variables When Product Doesn't Belong To User
	@Test
	public void validateSubmitVariables4() throws Exception {
		this.productId = "2";
		NewVariable nv1 = new NewVariable("Age", "Numerical", "Journey", "Primitive", "Age Of Customer", "");
		SubmitNewVariables input = new SubmitNewVariables();
		input.setNewVariablesList(Arrays.asList(nv1));
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "submitNewVariables");
		variableDataValidator.validateNewVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Update Variables When Field Level Validation Error
	// Occurs
	@Test
	public void validateUpdateVariable1() throws Exception {
		UpdateVariable input = new UpdateVariable("Journey", "Age Of Customer", "");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setVariableId(this.variableId);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateVariable");
		result.rejectValue("variableSource", "variableSource.invalid", Constants.VARIABLE_SOURCE_TOO_BIG_MESSAGE);
		variableDataValidator.validateUpdateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Update Variable When Product Belong To User And
	// Product Is Not Deployed And variable Belong To Product
	@Test
	public void validateUpdateVariable2() throws Exception {
		this.p1.setStatus(0);
		UpdateVariable input = new UpdateVariable("Journey", "Age Of Customer", "");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setVariableId(this.variableId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateVariable");
		variableDataValidator.validateUpdateVariable(input, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Update Variable When Product Belong To User And
	// Product Is Not Deployed But variable Doesn't Belong To Product
	@Test
	public void validateUpdateVariable3() throws Exception {
		this.p1.setStatus(0);
		UpdateVariable input = new UpdateVariable("Journey", "Age Of Customer", "");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setVariableId(this.variableId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateVariable");
		variableDataValidator.validateUpdateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Update Variable When Product Belong To User But
	// Product Is Deployed
	@Test
	public void validateUpdateVariable4() throws Exception {
		this.p1.setStatus(2);
		UpdateVariable input = new UpdateVariable("Journey", "Age Of Customer", "");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setVariableId(this.variableId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateVariable");
		variableDataValidator.validateUpdateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate update Variables When Product Doesn't Belong To User
	@Test
	public void validateUpdateVariable5() throws Exception {
		this.productId = "2";
		UpdateVariable input = new UpdateVariable("Journey", "Age Of Customer", "");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setVariableId(this.variableId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateVariable");
		variableDataValidator.validateUpdateVariable(input, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Update Variable When Variable is Derived Variable
	@Test
	public void validateUpdateVariable6() throws Exception {
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Derived", "Age Of Customer", "AND ( TRUE , TRUE )",
				p1);
		this.p1.setStatus(0);
		UpdateVariable input = new UpdateVariable("Journey", "Age Of Customer", "OR ( TRUE , TRUE )");
		input.setUserId(this.userId);
		input.setProductId(this.productId);
		input.setVariableId(this.variableId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.variableId), Long.parseLong(productId),
				false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(Long.parseLong(this.variableId), false)).thenReturn(this.v1);
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(input, "updateVariable");
		variableDataValidator.validateUpdateVariable(input, result);
		assertEquals(false, result.hasErrors());
	}

}
