package com.kuliza.lending.configurator.fireRules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;

import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.ExpressionDao;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.models.ProductDeployment;
import com.kuliza.lending.configurator.models.ProductDeploymentDao;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.models.RuleDao;
import com.kuliza.lending.configurator.models.User;
import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.GetRequiredVariables;
import com.kuliza.lending.configurator.pojo.ProductionFireRulesInput;
import com.kuliza.lending.configurator.pojo.TestFireRulesInput;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.FireRulesDataValidator;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = CreditEngineApplication.class)
@WebAppConfiguration
public class FireRulesValidatorTest {

	@Mock
	private RuleDao ruleDao;

	@Mock
	private ProductDao productDao;

	@Mock
	private VariableDao variableDao;

	@Mock
	private GroupDao groupDao;

	@Mock
	private ExpressionDao expressionDao;

	@Mock
	private ProductDeploymentDao productDeploymentDao;

	@InjectMocks
	private FireRulesDataValidator fireRulesDataValidator;

	private ProductCategory pc1;
	private User user;
	private Product p1;
	private Product p2;
	private Variable v1;
	private Variable v2;
	private Variable v3;
	private Variable v4;
	private Variable v5;
	private Group g1;
	private Rule r1;
	private Rule r2;
	private Rule r3;
	private Rule r4;
	private Rule r5;
	private Expression e1;
	private ProductDeployment pd1;
	private String userId;
	private String productId;
	private String groupId;
	private ProductionFireRulesInput productionInput;
	private TestFireRulesInput testInput;
	private GetRequiredVariables getRequiredVariables;

	@Before
	public void init() {
		this.userId = "1";
		this.productId = "1";
		this.groupId = "1";
		this.pc1 = new ProductCategory(1, "ScoreCard");
		this.user = new User(1, "arpit.agrawal@kuliza.com");
		this.p1 = new Product(1, "AXIS_Bank", 0, 3, "abdef", this.user, this.pc1);
		this.p2 = new Product(2, "AXIS_Bank_2", 1, 2, "abdef", this.user, this.pc1);
		this.v1 = new Variable(1, "Age", "Numerical", "Journey", "Primitive", "Age Of Customer", null, p1);
		this.v2 = new Variable(2, "Gender", "String", "Journey", "Primitive", "Gender Of Customer", null, p1);
		this.v3 = new Variable(3, "Salaried", "Boolean", "Journey", "Primitive", "Is Customer Salaried?", null, p1);
		this.v4 = new Variable(4, "Score", "Numerical", "Calculated", "Derived", "Calculate Score",
				"ID_1 + ID_2 + ID_3", p1);
		this.v5 = new Variable(5, "Score_2", "Numerical", "Calculated", "Derived", "Calculate Score 2",
				"ID_1 + ID_2 + ID_3 + ID_4", p1);
		this.g1 = new Group(1, "Personal_Eligibility", p1);
		this.r1 = new Rule(1, ">= 30 && <= 50", "5", 2.0, true, this.v1, this.g1);
		this.r2 = new Rule(2, "> 50", "4", 2.0, true, this.v1, this.g1);
		this.r3 = new Rule(3, "== male", "5", 1.0, true, this.v2, this.g1);
		this.r4 = new Rule(4, "== true", "4", 2.0, true, this.v3, this.g1);
		this.r5 = new Rule(5, "> 100", "4", 2.0, true, this.v5, this.g1);
		this.e1 = new Expression(1, "ID_1 + 2 * ID_2 + 3 * ID_3 + 4 * ID_4", true, false, null, this.g1);
		this.pd1 = new ProductDeployment(1, true, "abc", this.p1, this.user);
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age", "42");
		inputData.put("Gender", "male");
		inputData.put("Salaried", "true");
		this.productionInput = new ProductionFireRulesInput("abc", "", "123", inputData);
		this.testInput = new TestFireRulesInput(Arrays.asList(1L, 2L), inputData);
		this.getRequiredVariables = new GetRequiredVariables(Arrays.asList(1L));
	}

	// Test For Validate All Required Variables When No Validation Error Occurs
	@Test
	public void validateAllRequiredVariables1() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.getRequiredVariables,
				"getRequiredVariables");
		this.getRequiredVariables.setUserId(this.userId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(this.p1);
		fireRulesDataValidator.validateAllRequiredVariables(this.getRequiredVariables, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate All Required Variables When Product Doesn't Exist or
	// doesn't belong to user
	@Test
	public void validateAllRequiredVariables2() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.getRequiredVariables,
				"getRequiredVariables");
		this.getRequiredVariables.setUserId(this.userId);
		when(productDao.findByIdAndUserIdAndIsDeleted(Long.parseLong(this.productId), Long.parseLong(this.userId),
				false)).thenReturn(null);
		fireRulesDataValidator.validateAllRequiredVariables(this.getRequiredVariables, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate All Required Variables When Field Level Validation
	// Error Exists
	@Test
	public void validateAllRequiredVariables3() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.getRequiredVariables,
				"getRequiredVariables");
		result.rejectValue(Constants.PRODUCT_IDS, Constants.INVALID_PRODUCT_IDS, "productIds is a required key");
		this.getRequiredVariables.setUserId(this.userId);
		fireRulesDataValidator.validateAllRequiredVariables(this.getRequiredVariables, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Test Fire Rules When No Validation Error Occurs
	@Test
	public void validateTestFireRules1() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.testInput, "testFireRulesInput");
		this.testInput.setUserId(this.userId);
		when(productDao.findByIdAndUserIdAndIsDeleted(1L, Long.parseLong(this.userId), false)).thenReturn(this.p1);
		when(productDao.findByIdAndUserIdAndIsDeleted(2L, Long.parseLong(this.userId), false)).thenReturn(this.p2);
		fireRulesDataValidator.validateTestFireRules(this.testInput, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Test Fire Rules When Product Not Published Or Product
	// Doesn't Belong to user or doesn't Exist
	@Test
	public void validateTestFireRules2() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.testInput, "testFireRulesInput");
		this.testInput.setUserId(this.userId);
		this.p1.setStatus(0);
		when(productDao.findByIdAndUserIdAndIsDeleted(1L, Long.parseLong(this.userId), false)).thenReturn(this.p1);
		when(productDao.findByIdAndUserIdAndIsDeleted(2L, Long.parseLong(this.userId), false)).thenReturn(null);
		fireRulesDataValidator.validateTestFireRules(this.testInput, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Test Fire Rules When Field Level Validation Error
	// Occurs
	@Test
	public void validateTestFireRules3() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.testInput, "testFireRulesInput");
		result.rejectValue(Constants.PRODUCT_IDS, Constants.INVALID_PRODUCT_IDS, "productIds is a required key");
		this.testInput.setUserId(this.userId);
		fireRulesDataValidator.validateTestFireRules(this.testInput, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Each Product Fire Rules When No Error Occurs
	@Test
	public void validateEachProductFireRules1() throws Exception {
		Set<Map<String, String>> allRequiredVariables = new HashSet<>();
		Map<String, String> variable = new HashMap<>();
		variable.put("name", "Age");
		variable.put("type", "Numerical");
		allRequiredVariables.add(variable);
		Map<String, String> variable2 = new HashMap<>();
		variable2.put("name", "Gender");
		variable2.put("type", "String");
		allRequiredVariables.add(variable2);
		Map<String, String> variable3 = new HashMap<>();
		variable3.put("name", "Salaried");
		variable3.put("type", "Boolean");
		allRequiredVariables.add(variable3);
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age-Numerical", "42");
		inputData.put("Gender-String", "male");
		inputData.put("Salaried-Boolean", "true");
		this.testInput.setInputData(inputData);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		GenericAPIResponse response = fireRulesDataValidator.validateTestEachProductFireRules(
				Long.parseLong(this.productId), Long.parseLong(this.userId), this.testInput.getInputData(),
				allRequiredVariables);
		assertNull(response);
	}

	// Test For Validate Each Product Fire Rules When Variable is Missing Or
	// Variable Value is incorrect
	@Test
	public void validateEachProductFireRules2() throws Exception {
		Set<Map<String, String>> allRequiredVariables = new HashSet<>();
		Map<String, String> variable = new HashMap<>();
		variable.put("name", "Age");
		variable.put("type", "Numerical");
		allRequiredVariables.add(variable);
		Map<String, String> variable2 = new HashMap<>();
		variable2.put("name", "Gender");
		variable2.put("type", "String");
		allRequiredVariables.add(variable2);
		Map<String, String> variable3 = new HashMap<>();
		variable3.put("name", "Salaried");
		variable3.put("type", "Boolean");
		allRequiredVariables.add(variable3);
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age-Numerical", "42a");
		inputData.put("Salaried-Boolean", "treu");
		this.testInput.setInputData(inputData);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		GenericAPIResponse response = fireRulesDataValidator.validateTestEachProductFireRules(
				Long.parseLong(this.productId), Long.parseLong(this.userId), this.testInput.getInputData(),
				allRequiredVariables);
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());

	}

	// Test For Validate Production Fire Rules When No Validation Error Occurs
	@Test
	public void validateProductionFireRules1() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		this.productionInput.setUserId(this.userId);
		when(productDeploymentDao.findByIdentifierAndUserIdAndStatusAndIsDeleted(this.productionInput.getIdentifier(),
				Long.parseLong(this.userId), true, false)).thenReturn(this.pd1);
		fireRulesDataValidator.validateProductionFireRules(this.productionInput, result);
		assertEquals(false, result.hasErrors());
	}

	// Test For Validate Production Fire Rules When Product Is Not Deployed Or
	// Identifier Is Wrong
	@Test
	public void validateProductionFireRules2() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		this.productionInput.setUserId(this.userId);
		when(productDeploymentDao.findByIdentifierAndUserIdAndStatusAndIsDeleted(this.productionInput.getIdentifier(),
				Long.parseLong(this.userId), true, false)).thenReturn(null);
		fireRulesDataValidator.validateProductionFireRules(this.productionInput, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Production Fire Rules When Field Level Validation
	// Error Exists
	@Test
	public void validateProductionFireRules3() throws Exception {
		BeanPropertyBindingResult result = new BeanPropertyBindingResult(this.productionInput,
				"productionFireRulesInput");
		result.rejectValue(Constants.INPUT_DATA, Constants.INVALID_INPUT_DATA, "inputData is a required key");
		this.productionInput.setUserId(this.userId);
		fireRulesDataValidator.validateProductionFireRules(this.productionInput, result);
		assertEquals(true, result.hasErrors());
	}

	// Test For Validate Production Product Fire Rules When No Error Occurs
	@Test
	public void validateProductionProductFireRules1() throws Exception {
		this.productionInput.setUserId(this.userId);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.productionInput.getIdentifier(), true,
				false)).thenReturn(this.pd1);
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(ruleDao.findByGroupIdAndIsDeletedAndIsActive(Long.parseLong(this.groupId), false, true))
				.thenReturn(Arrays.asList(this.r1, this.r2, this.r3, this.r4, this.r5));
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(this.groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByIdAndIsDeleted(4, false)).thenReturn(this.v4);
		when(variableDao.findByIdAndIsDeleted(5, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", Long.parseLong(this.productId), false))
				.thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score_2", Long.parseLong(this.productId), false))
				.thenReturn(this.v5);
		GenericAPIResponse response = fireRulesDataValidator.validateProductionProductFireRules(this.productionInput,
				new HashSet<>(), new HashSet<>());
		assertNull(response);
	}

	// Test For Validate Production Product Fire Rules When Variable is Missing
	// Or Variable Value is incorrect
	@Test
	public void validateProductionProductFireRules2() throws Exception {
		this.productionInput.setUserId(this.userId);
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age", "42a");
		inputData.put("Salaried", "tru");
		this.productionInput.setInputData(inputData);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.productionInput.getIdentifier(), true,
				false)).thenReturn(this.pd1);
		when(groupDao.findByProductIdAndIsDeleted(Long.parseLong(this.productId), false))
				.thenReturn(Arrays.asList(this.g1));
		when(ruleDao.findByGroupIdAndIsDeletedAndIsActive(Long.parseLong(this.groupId), false, true))
				.thenReturn(Arrays.asList(this.r1, this.r2, this.r3, this.r4, this.r5));
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(this.groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByIdAndIsDeleted(4, false)).thenReturn(this.v4);
		when(variableDao.findByIdAndIsDeleted(5, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", Long.parseLong(this.productId), false))
				.thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score_2", Long.parseLong(this.productId), false))
				.thenReturn(this.v5);
		GenericAPIResponse response = fireRulesDataValidator.validateProductionProductFireRules(this.productionInput,
				new HashSet<>(), new HashSet<>());
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Production Group Fire Rules When No Error Occurs
	@Test
	public void validateProductionGroupFireRules1() throws Exception {
		this.productionInput.setUserId(this.userId);
		this.productionInput.setGroupId(this.groupId);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.productionInput.getIdentifier(), true,
				false)).thenReturn(this.pd1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		when(groupDao.findByIdAndIsDeleted(Long.parseLong(this.groupId), false)).thenReturn(this.g1);
		when(ruleDao.findByGroupIdAndIsDeletedAndIsActive(Long.parseLong(this.groupId), false, true))
				.thenReturn(Arrays.asList(this.r1, this.r2, this.r3, this.r4, this.r5));
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(this.groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		when(productDao.findByIdAndIsDeleted(Long.parseLong(this.productId), false)).thenReturn(this.p1);
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByIdAndIsDeleted(4, false)).thenReturn(this.v4);
		when(variableDao.findByIdAndIsDeleted(5, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", Long.parseLong(this.productId), false))
				.thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score_2", Long.parseLong(this.productId), false))
				.thenReturn(this.v5);
		GenericAPIResponse response = fireRulesDataValidator.validateProductionGroupFireRules(this.productionInput,
				new HashSet<>(), new HashSet<>());
		assertNull(response);
	}

	// Test For Validate Production Group Fire Rules When Variable is Missing
	// Or Variable Value is incorrect
	@Test
	public void validateProductionGroupFireRules2() throws Exception {
		this.productionInput.setUserId(this.userId);
		this.productionInput.setGroupId(this.groupId);
		Map<String, Object> inputData = new HashedMap<String, Object>();
		inputData.put("Age", "42a");
		inputData.put("Salaried", "tru");
		this.productionInput.setInputData(inputData);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.productionInput.getIdentifier(), true,
				false)).thenReturn(this.pd1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(this.g1);
		when(groupDao.findByIdAndIsDeleted(Long.parseLong(this.groupId), false)).thenReturn(this.g1);
		when(ruleDao.findByGroupIdAndIsDeletedAndIsActive(Long.parseLong(this.groupId), false, true))
				.thenReturn(Arrays.asList(this.r1, this.r2, this.r3, this.r4, this.r5));
		when(expressionDao.findByGroupIdAndIsDeleted(Long.parseLong(this.groupId), false))
				.thenReturn(Arrays.asList(this.e1));
		when(variableDao.findByIdAndIsDeleted(1, false)).thenReturn(this.v1);
		when(variableDao.findByIdAndIsDeleted(2, false)).thenReturn(this.v2);
		when(variableDao.findByIdAndIsDeleted(3, false)).thenReturn(this.v3);
		when(variableDao.findByIdAndIsDeleted(4, false)).thenReturn(this.v4);
		when(variableDao.findByIdAndIsDeleted(5, false)).thenReturn(this.v5);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Age", Long.parseLong(this.productId), false))
				.thenReturn(this.v1);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Gender", Long.parseLong(this.productId), false))
				.thenReturn(this.v2);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Salaried", Long.parseLong(this.productId), false))
				.thenReturn(this.v3);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score", Long.parseLong(this.productId), false))
				.thenReturn(this.v4);
		when(variableDao.findByNameAndProductIdAndIsDeleted("Score_2", Long.parseLong(this.productId), false))
				.thenReturn(this.v5);
		GenericAPIResponse response = fireRulesDataValidator.validateProductionGroupFireRules(this.productionInput,
				new HashSet<>(), new HashSet<>());
		assertEquals(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, response.getStatus());
	}

	// Test For Validate Production Group Fire Rules When GroupId Is Invalid
	@Test
	public void validateProductionGroupFireRules3() throws Exception {
		this.productionInput.setUserId(this.userId);
		this.productionInput.setGroupId(this.groupId);
		when(productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(this.productionInput.getIdentifier(), true,
				false)).thenReturn(this.pd1);
		when(groupDao.findByIdAndProductIdAndIsDeleted(Long.parseLong(this.groupId), Long.parseLong(this.productId),
				false)).thenReturn(null);
		GenericAPIResponse response = fireRulesDataValidator.validateProductionGroupFireRules(this.productionInput,
				new HashSet<>(), new HashSet<>());
		assertEquals(Constants.NOT_FOUND_ERROR_CODE, response.getStatus());
	}

}
