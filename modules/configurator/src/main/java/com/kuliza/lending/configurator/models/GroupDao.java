package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface GroupDao extends CrudRepository<Group, Long> {
	public Group findById(long id);

	public Group findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Group> findByProductId(long productId);

	public List<Group> findByProductIdAndIsDeleted(long productId, boolean isDeleted);

	public Group findByIdAndProductIdAndIsDeleted(long id, long productId, boolean isDeleted);

	public Group findByNameAndProductId(String name, long productId);

	public Group findByNameAndProductIdAndIsDeleted(String name, long productId, boolean isDeleted);
}
