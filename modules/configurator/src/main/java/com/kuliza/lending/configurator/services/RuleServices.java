package com.kuliza.lending.configurator.services;

import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.*;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import com.kuliza.lending.configurator.validators.RuleDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import java.util.*;

@Service
public class RuleServices extends GenericServicesCE {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private RuleDao ruleDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private RuleDataValidator ruleDataValidator;

	public GenericAPIResponse validateSubmitRules(SubmitNewRules input, BindingResult result, String userId,
			String productId, String groupId) throws Exception {
		Map<String, Object> data = new HashMap<>();
		GenericAPIResponse response = null;
		input.setUserId(userId);
		input.setProductId(productId);
		input.setGroupId(groupId);
		ruleDataValidator.validateRuleForm(input, result);
		response = checkErrors(result);
		Set<String> usedVariables = new HashSet<>();
		Boolean isError = false;
		if (response == null) {
			List<NewRule> rules = input.getNewRulesList();
			List<Map<String, Object>> ruleErrors = new ArrayList<>();
			for (NewRule rule : rules) {
				List<RuleDef> ruleList = rule.getRules();
				rule.setProductId(productId);
				BindingResult bindingResult = new BeanPropertyBindingResult(rule, "rule");
				// validates each rule
				if (usedVariables.contains(rule.getVariableName())) {
					bindingResult.rejectValue(Constants.VARIABLE_NAME, Constants.INVALID_VARIABLE_NAME,
							Constants.VARIABLE_ALREADY_USED);
				} else {
					usedVariables.add(rule.getVariableName());
				}
				ruleDataValidator.validateRule(rule, bindingResult);
				if (bindingResult.hasErrors()) {
					Map<String, Object> ruleError = new HashMap<>();
					ruleError.put(Constants.VARIABLE_NAME, rule.getVariableName());
					ruleError.put(Constants.DATA_ERRORS_KEY,
							HelperFunctions.generateErrorResponseData(bindingResult.getFieldErrors()));
					ruleErrors.add(ruleError);
					isError = true;
				} else {
					Variable variableObj = variableDao.findByNameAndProductIdAndIsDeleted(rule.getVariableName(),
							Long.parseLong(productId), false);
					Boolean errorInRule = false;
					List<Map<String, Object>> errorInSingleVariableRule = new ArrayList<>();
					for (RuleDef ruleMap : ruleList) {
						BindingResult bindingResult2 = new BeanPropertyBindingResult(ruleMap, "ruleMap");
						// validates rule structure i.e. fn1 fn2 values
						ruleMap.setGroupId(groupId);
						ruleMap.setVariableId(Long.toString(variableObj.getId()));
						ruleDataValidator.validateRuleStructure(ruleMap, bindingResult2);
						if (bindingResult2.hasErrors()) {
							Map<String, Object> ruleStructureError = new HashMap<>();
							ruleStructureError.put(Constants.DATA_ERRORS_KEY,
									HelperFunctions.generateErrorResponseData(bindingResult2.getFieldErrors()));
							errorInSingleVariableRule.add(ruleStructureError);
							errorInRule = true;
							isError = true;
						}
					}
					if (errorInRule) {
						Map<String, Object> ruleError = new HashMap<>();
						ruleError.put(Constants.VARIABLE_NAME, rule.getVariableName());
						ruleError.put(Constants.DATA_ERRORS_KEY, errorInSingleVariableRule);
						ruleErrors.add(ruleError);
					} else {
						ruleErrors.add(null);
					}
				}
			}
			if (isError) {
				data.put(Constants.DATA_ERRORS_KEY, ruleErrors);
				response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
						Constants.FAILURE_MESSAGE, data);
			}
		}
		return response;
	}

	public GenericAPIResponse validateDeleteRule(String userId, String productId, String groupId, String ruleId)
			throws Exception {
		GenericAPIResponse response = null;
		if (!ruleDataValidator.validateProductId(productId, userId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else if (!ruleDataValidator.validateGroupId(groupId, productId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_GROUP_ID_MESSAGE);
		} else if (!ruleDataValidator.validateRuleId(ruleId, groupId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_RULE_ID_MESSAGE);
		} else if (productDao.findByIdAndIsDeleted(Long.parseLong(productId), false).getStatus() > 0) {
			response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
					Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
		}

		if (response == null) {
			Rule rule = ruleDao.findByIdAndIsDeleted(Long.parseLong(ruleId), false);
			Variable variable = rule.getVariable();
			List<Rule> allVariableRules = ruleDao.findByVariableIdAndIsDeleted(variable.getId(), false);
			if (allVariableRules.size() < 2) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						Constants.CANNOT_DELETE_RULE_MESSAGE);
			}
		}
		return response;
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse createNewRules(String productId, String groupId, SubmitNewRules input) throws Exception {
		Group group = groupDao.findByIdAndIsDeleted(Long.parseLong(groupId), false);
		List<NewRule> rules = input.getNewRulesList();
		Set<Rule> savedRules = saveRulesInDB(rules, productId, group);
		ResponseGetRule allSavedRules = new ResponseGetRule(savedRules);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allSavedRules);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse deleteRule(String ruleId) throws Exception {
		Rule rule = ruleDao.findByIdAndIsDeleted(Long.parseLong(ruleId), false);
		rule.setIsDeleted(true);
		ruleDao.save(rule);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.RULE_DELETED_SUCCESS_MESSAGE);
	}

	public Set<Rule> saveRulesInDB(List<NewRule> rules, String productId, Group group) {
		Set<Rule> savedRules = new HashSet<>();
		for (NewRule rule : rules) {
			List<RuleDef> ruleList = rule.getRules();
			Variable variableObj = variableDao.findByNameAndProductIdAndIsDeleted(rule.getVariableName(),
					Long.parseLong(productId), false);
			for (RuleDef ruleMap : ruleList) {
				StringBuilder ruleString = new StringBuilder();
				ruleString.append(ruleMap.getFn1()).append(" ").append(ruleMap.getValue1());
				if (!ruleMap.getValue2().equals(""))
					ruleString.append(" && ").append(ruleMap.getFn2()).append(" ").append(ruleMap.getValue2());
				Rule newRule = new Rule(ruleString.toString(), ruleMap.getOutputValue(),
						Double.parseDouble(rule.getOutputWeight()), variableObj,
						Boolean.parseBoolean(rule.getIsActive()), group);
				if (!ruleMap.getRuleId().equals("")) {
					long ruleId = Long.parseLong(ruleMap.getRuleId());
					newRule.setId(ruleId);
				}
				ruleDao.save(newRule);
				ruleString.setLength(0);
				savedRules.add(newRule);
			}
		}
		return savedRules;
	}

}
