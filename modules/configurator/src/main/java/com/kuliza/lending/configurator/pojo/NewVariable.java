package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NewVariable {

	private String variableId;

	@NotNull(message = "variableName is a required key")
	@Pattern(regexp = Constants.NAME_REGEX, message = Constants.INVALID_VARIABLE_NAME_MESSAGE)
	@Size(max = Constants.MAX_LENGTH_NAME_STRING, message = Constants.VARIABLE_NAME_TOO_BIG_MESSAGE)
	private String variableName;

	@NotNull(message = "variableType is a required key")
	@Pattern(regexp = Constants.VARIABLE_TYPE_REGEX, message = Constants.INVALID_VARIABLE_TYPE)
	private String variableType;

	@NotNull(message = "variableSource is a required key")
	@Size(min = Constants.MIN_LENGTH_STRING, max = Constants.MAX_LENGTH_STRING, message = Constants.VARIABLE_SOURCE_TOO_BIG_MESSAGE)
	private String variableSource;

	@NotNull(message = "variableCategory is a required key")
	@Pattern(regexp = Constants.VARIABLE_CATEGORY_REGEX, message = Constants.INVALID_VARIABLE_CATEGORY)
	private String variableCategory;

	@NotNull(message = "variableDescription is a required key")
	@Size(min = Constants.MIN_LENGTH_STRING, max = Constants.MAX_LENGTH_STRING, message = Constants.VARIABLE_DESCRIPTION_TOO_BIG_MESSAGE)
	private String variableDescription;

	@NotNull(message = "expressionString is a required key")
	@Size(min = Constants.MIN_LENGTH_VARIABLE_EXPRESSION, max = Constants.MAX_LENGTH_EXPRESSION, message = Constants.VARIABLE_EXPRESSION_TOO_BIG_MESSAGE)
	private String expressionString;

	private String productId;
	
	private int variableOrder;

	public NewVariable() {
		this.variableName = "";
		this.variableType = "";
		this.variableSource = "";
		this.variableCategory = "";
		this.variableDescription = "";
		this.expressionString = "";
		this.setVariableOrder(0);
	}

	public NewVariable(String variableName, String variableType, String variableSource, String variableCategory,
			String variableDescription, String expressionString, int variableOrder) {
		super();
		this.variableName = variableName;
		this.variableType = variableType;
		this.variableSource = variableSource;
		this.variableCategory = variableCategory;
		this.variableDescription = variableDescription;
		this.expressionString = expressionString;
		this.setVariableOrder(variableOrder);
	}

	public String getVariableId() {
		return variableId;
	}

	public void setVariableId(String variableId) {
		this.variableId = variableId;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getVariableType() {
		return variableType;
	}

	public void setVariableType(String variableType) {
		this.variableType = variableType;
	}

	public String getVariableSource() {
		return variableSource;
	}

	public void setVariableSource(String variableSource) {
		this.variableSource = variableSource;
	}

	public String getVariableCategory() {
		return variableCategory;
	}

	public void setVariableCategory(String variableCategory) {
		this.variableCategory = variableCategory;
	}

	public String getVariableDescription() {
		return variableDescription;
	}

	public void setVariableDescription(String variableDescription) {
		this.variableDescription = variableDescription;
	}

	public String getExpressionString() {
		return expressionString;
	}

	public void setExpressionString(String expressionString) {
		this.expressionString = expressionString;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getVariableOrder() {
		return variableOrder;
	}

	public void setVariableOrder(int variableOrder) {
		this.variableOrder = variableOrder;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("variableName : " + variableName + ", ");
	// inputData.append("variableType : " + variableType + ", ");
	// inputData.append("variableSource : " + variableSource + ", ");
	// inputData.append("variableCategory : " + variableCategory + ", ");
	// inputData.append("variableDescription : " + variableDescription + ", ");
	// inputData.append("expressionString : " + expressionString);
	// inputData.append(" }");
	// return inputData.toString();
	// }

}
