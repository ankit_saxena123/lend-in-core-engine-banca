package com.kuliza.lending.configurator.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Iterators;
import com.kuliza.lending.configurator.drools.DroolsEngine;
import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.*;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import com.kuliza.lending.configurator.utils.SortVariablesList;
import com.kuliza.lending.configurator.validators.FireRulesDataValidator;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.drools.core.util.IoUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static com.kuliza.lending.configurator.utils.Constants.DECIMAL_VALUE;

@Service
public class FireRulesServices extends GenericServicesCE {

	private static final Logger logger = LoggerFactory.getLogger(FireRulesServices.class);

	@Autowired
	private DroolsEngine droolsEngine;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private ExpressionDao expressionDao;

	@Autowired
	private FireRulesDataValidator fireRulesDataValidator;

	@Autowired
	private ProductDeploymentDao productDeploymentDao;

	@Autowired
	private ExecutionDao executionDao;

	@Autowired
	private ObjectMapper objectMapper;

	private int currentRow;
	private Map<String, Object> finalInputData;
	private Boolean flag;
	private Map<String, Object> responseData;

	public XSSFCell setExpressionForCell(XSSFSheet spreadsheet, XSSFRow newrow, FormulaEvaluator evaluator,
			String[] splitExpression, Map<String, String> variableCellLocations) throws Exception {
		logger.info("--> Entering setExpressionForCell()");
		XSSFCell cell = newrow.createCell(1);
		cell.setCellType(CellType.FORMULA);
		StringBuilder excelExpression = new StringBuilder();
		for (String var : splitExpression) {
			if (var.matches(Constants.NAME_REGEX) && !var.matches(Constants.NOT_VARIABLE_NAMES_REGEX)) {
				excelExpression.append(variableCellLocations.get(var));
			} else {
				excelExpression.append(var);
			}
		}
		logger.debug("Excel Expression: " + excelExpression.toString());
		cell.setCellFormula(excelExpression.toString());
		CellReference cellReference = new CellReference("B" + (this.currentRow + 1));
		XSSFRow rowsFormula = spreadsheet.getRow(cellReference.getRow());
		cell = rowsFormula.getCell(cellReference.getCol());
		if (cell.getCellTypeEnum() == CellType.FORMULA) {
			logger.debug("################### FORMULA CELL " + cell.getCellFormula() + "#############################");
		} else {
			logger.debug("###################  NON FORMULA CELL " + cell.getCellTypeEnum()
					+ "#############################");
		}
		evaluator.evaluateInCell(cell);
		logger.info("<-- Exiting setExpressionForCell()");
		return cell;
	}

	public GenericAPIResponse evaluateVariable(Product product, XSSFSheet spreadsheet,
			Map<String, String> variableCellLocations, Map<String, Object> input, XSSFWorkbook workbook,
			List<Variable> primitiveVariables, List<Variable> derivedVariables) throws Exception {
		logger.info("--> Entering evaluateVariable()");
		GenericAPIResponse response = null;
		Map<String, Object> data = new HashMap<>();
		for (Variable variable : primitiveVariables) {
			logger.debug("evaluate primitive variable:" + variable.getName());
			XSSFRow newrow = spreadsheet.createRow(this.currentRow);
			XSSFCell cell = newrow.createCell(0);
			cell.setCellType(CellType.STRING);
			cell.setCellValue(variable.getName());
			cell = newrow.createCell(1);
			if (variable.getType().equals(Constants.NUMERICAL)) {
				cell.setCellType(CellType.NUMERIC);
				cell.setCellValue(Double.parseDouble(input.get(variable.getName()).toString()));
				this.finalInputData.put(variable.getName(), cell.getNumericCellValue());
			} else if (variable.getType().equals(Constants.STRING)) {
				cell.setCellType(CellType.STRING);
				cell.setCellValue(input.get(variable.getName()).toString());
				this.finalInputData.put(variable.getName(), cell.getStringCellValue());
			} else {
				cell.setCellType(CellType.BOOLEAN);
				cell.setCellValue(Boolean.parseBoolean(input.get(variable.getName()).toString()));
				this.finalInputData.put(variable.getName(), cell.getBooleanCellValue());
			}
			variableCellLocations.put(variable.getName(), "B" + (this.currentRow + 1));
			this.currentRow++;
		}
		logger.debug("============== derivedVariables====================" + derivedVariables);
		logger.debug("============== variable Cell Locations XLS====================" + variableCellLocations);
		for (Variable variable : derivedVariables) {
			logger.debug("evaluate derived variables:" + variable.getName());
			XSSFRow newrow = spreadsheet.createRow(this.currentRow);
			XSSFCell cell = newrow.createCell(0);
			cell.setCellType(CellType.STRING);
			cell.setCellValue(variable.getName());
			cell = newrow.createCell(1);
			cell.setCellType(CellType.FORMULA);
			String[] expression = HelperFunctions
					.makeReadableExpressionString(variable.getExpression(), variableDao, groupDao, true).split("\\s");
			StringBuilder excelExpression = new StringBuilder();
			for (String var : expression) {
				if (var.matches(Constants.NAME_REGEX) && !var.matches(Constants.NOT_VARIABLE_NAMES_REGEX)) {
					excelExpression.append(variableCellLocations.get(var));
				} else {
					excelExpression.append(var);
				}
			}
			cell.setCellFormula(excelExpression.toString());
			variableCellLocations.put(variable.getName(), "B" + (this.currentRow + 1));
			this.currentRow++;
		}
		logger.debug("============== variable Cell Locations XLS====================" + variableCellLocations);
		workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
		for (Map.Entry<String, String> variable : variableCellLocations.entrySet()) {
			CellReference cellReference = new CellReference(variable.getValue());
			XSSFRow cellRow = spreadsheet.getRow(cellReference.getRow());
			XSSFCell cell = cellRow.getCell(cellReference.getCol());
			if (cell != null) {
				switch (cell.getCellTypeEnum()) {
				case BOOLEAN:
					this.finalInputData.put(variable.getKey(), cell.getBooleanCellValue());
					break;
				case NUMERIC:
					 BigDecimal value = BigDecimal.valueOf(cell.getNumericCellValue());
					 value = value.setScale(DECIMAL_VALUE, RoundingMode.HALF_EVEN);
					 this.finalInputData.put(variable.getKey(), value.toPlainString());
					break;
				case STRING:
					this.finalInputData.put(variable.getKey(), cell.getStringCellValue());
					break;
				case ERROR:
					this.flag = true;
					break;
				case FORMULA:
					Variable var = variableDao.findByNameAndProductIdAndIsDeleted(variable.getKey(), product.getId(),
							false);
					if (var.getType().equals("Boolean")) {
						this.finalInputData.put(variable.getKey(), cell.getBooleanCellValue());
					} else if (var.getType().equals(Constants.NUMERICAL)) {
						value = BigDecimal.valueOf(cell.getNumericCellValue());
						value = value.setScale(DECIMAL_VALUE, RoundingMode.HALF_EVEN);
						this.finalInputData.put(variable.getKey(), value.toPlainString());
					} else {
						this.finalInputData.put(variable.getKey(), cell.getStringCellValue());
					}
					break;
				default:
					break;
				}
			}
			logger.debug("============Final Input Data=====" + this.finalInputData);
			if (this.flag) {
				data.put(Constants.DATA_MESSAGE_KEY,
						"Error While Calculating Value for Derived Variable : " + variable.getKey());
				response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
						Constants.FAILURE_MESSAGE, data);
			}
		}
		logger.info("<-- Exiting evaluateVariable()");
		return response;
	}

	public GenericAPIResponse evaluateGroup(Group group, Product product, DroolsInput droolsInput,
			XSSFSheet spreadsheet, Map<String, String> variableCellLocations, FormulaEvaluator evaluator,
			String environment) throws Exception {
		logger.info("--> Entering evaluateGroup()");
		GenericAPIResponse response = null;
		Map<String, Object> data = new HashMap<>();
		String groupName = group.getName();
		String fileName = "Rules_" + groupName + "_" + Long.toString(group.getId());
		droolsEngine.getScore(droolsInput, fileName, environment, Constants.CONTAINER_ID + product.getId());
		for (Map.Entry<String, Object> variable : droolsInput.getOutputScores().entrySet()) {
			CellReference cellReference = new CellReference(variableCellLocations.get(variable.getKey()));
			XSSFRow cellRow = spreadsheet.getRow(cellReference.getRow());
			XSSFCell cell = cellRow.createCell(2);
			if (variable.getValue().toString().matches(Constants.VALUE_REGEX)) {
				cell.setCellType(CellType.NUMERIC);
				BigDecimal value = BigDecimal.valueOf(Double.parseDouble(variable.getValue().toString()));
				value = value.setScale(DECIMAL_VALUE, RoundingMode.HALF_EVEN);
				cell.setCellValue(value.doubleValue());
			} else {
				cell.setCellType(CellType.BOOLEAN);
				cell.setCellValue(Boolean.parseBoolean(variable.getValue().toString()));
			}
			variableCellLocations.put(variable.getKey(), "C" + (cellRow.getRowNum() + 1));
		}
		logger.debug("===============variable Cell Locations ================= " + variableCellLocations);
		List<Expression> allExpressions = expressionDao.findByGroupIdAndIsDeleted(group.getId(), false);
		logger.debug("========== All expressions for group with name: " + group.getName() + " :" + allExpressions);
		for (Expression expression : allExpressions) {
			String expressionString = HelperFunctions.makeReadableExpressionString(expression.getExpressionString(),
					variableDao, groupDao, true);
			String[] splitExpression = expressionString.split("\\s");
			XSSFRow newrow = spreadsheet.createRow(this.currentRow);
			XSSFCell cell = newrow.createCell(0);
			cell.setCellType(CellType.STRING);
			cell.setCellValue(group.getName());
			cell = setExpressionForCell(spreadsheet, newrow, evaluator, splitExpression, variableCellLocations);
			switch (cell.getCellTypeEnum()) {
			case BOOLEAN:
				droolsInput.setSingleOutputScore(group.getName().concat("-").concat(Constants.OUTPUT),
						cell.getBooleanCellValue());
				break;
			case NUMERIC:
				BigDecimal value = BigDecimal.valueOf(cell.getNumericCellValue());
				value = value.setScale(DECIMAL_VALUE, RoundingMode.HALF_EVEN);
				droolsInput.setSingleOutputScore(group.getName().concat("-").concat(Constants.OUTPUT),
						value.toPlainString());
				break;
			case STRING:
				droolsInput.setSingleOutputScore(group.getName().concat("-").concat(Constants.OUTPUT),
						cell.getStringCellValue());
				break;
			default:
				this.flag = true;
				break;
			}
			variableCellLocations.put(group.getName(), "B" + (this.currentRow + 1));
			logger.debug("===============variable Cell Locations ================= " + variableCellLocations);
			this.currentRow++;
			if (this.flag) {
				data.put(Constants.DATA_MESSAGE_KEY, "Error While Calculating Expression : " + expressionString
						+ " of Product : " + product.getName());
				response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
						Constants.FAILURE_MESSAGE, data);
			}
		}
		logger.info("<-- Exiting evaluateGroup()");
		return response;
	}

	public GenericAPIResponse evaluateProductExpressions(Product product, DroolsInput droolsInput,
			XSSFSheet spreadsheet, Map<String, String> variableCellLocations, FormulaEvaluator evaluator)
			throws Exception {
		logger.info("--> Entering evaluateProductExpressions()");
		GenericAPIResponse response = null;
		Map<String, Object> data = new HashMap<>();
		List<Expression> allProductExpressions = expressionDao.findByProductIdAndIsDeleted(product.getId(), false);
		for (Expression expression : allProductExpressions) {
			String expressionString = HelperFunctions.makeReadableExpressionString(expression.getExpressionString(),
					variableDao, groupDao, false);
			String[] splitExpression = expressionString.split("\\s");
			XSSFRow newrow = spreadsheet.createRow(this.currentRow);
			XSSFCell cell = newrow.createCell(0);
			cell.setCellType(CellType.STRING);
			cell.setCellValue(product.getName());
			cell = setExpressionForCell(spreadsheet, newrow, evaluator, splitExpression, variableCellLocations);
			switch (cell.getCellTypeEnum()) {
			case BOOLEAN:
				droolsInput.setSingleOutputScore(product.getName(), cell.getBooleanCellValue());
				break;
			case NUMERIC:
				BigDecimal value = BigDecimal.valueOf(cell.getNumericCellValue());
				value = value.setScale(DECIMAL_VALUE, RoundingMode.HALF_EVEN);
				droolsInput.setSingleOutputScore(product.getName(),
						value.toPlainString());
				break;
			case STRING:
				droolsInput.setSingleOutputScore(product.getName(), cell.getStringCellValue());
				break;
			default:
				this.flag = true;
				break;
			}
			variableCellLocations.put(product.getName(), "B" + (this.currentRow + 1));
			logger.debug("===============variable Cell Locations ================= " + variableCellLocations);
			this.currentRow++;
			if (this.flag) {
				data.put(Constants.DATA_MESSAGE_KEY, "Error While Calculating Expression : " + expressionString
						+ " of Product : " + product.getName());
				response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
						Constants.FAILURE_MESSAGE, data);
			}
		}
		logger.info("<-- Exiting evaluateProductExpressions()");
		return response;
	}

	public GenericAPIResponse fireRules(Product product, String groupId, Map<String, Object> input,
			Map<String, Object> responseData, String environment, List<Variable> primitiveVariables,
			List<Variable> derivedVariables) throws Exception {
		logger.info("--> Entering FireRules()");
		GenericAPIResponse response = null;
		DroolsInput droolsInput = null;
		// FileOutputStream out = new FileOutputStream(new
		// File("createworkbook.xlsx"));
		XSSFWorkbook workbook = new XSSFWorkbook();
		try {
			XSSFSheet spreadsheet = workbook.createSheet("group_sheet");
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			this.finalInputData = new HashMap<>();
			Map<String, String> variableCellLocations = new HashMap<>();
			this.currentRow = 0;
			logger.debug("evaluating variables");
			response = evaluateVariable(product, spreadsheet, variableCellLocations, input, workbook,
					primitiveVariables, derivedVariables);
			logger.debug("variables evaluated successfully");
			if (response == null) {
				droolsInput = new DroolsInput(finalInputData);
				if (groupId.equals("")) {
					logger.debug("getting all groups for product with id:" + product.getId());
					List<Group> allGroups = groupDao.findByProductIdAndIsDeleted(product.getId(), false);
					for (Group group : allGroups) {
						logger.debug("Evaluating group:" + group.getName());
						response = evaluateGroup(group, product, droolsInput, spreadsheet, variableCellLocations,
								evaluator, environment);
						if (response != null) {
							break;
						} else {
							if (responseData.containsKey(Constants.OUTPUT)) {
								@SuppressWarnings("unchecked")
								Map<String, Object> output = (Map<String, Object>) responseData.get(Constants.OUTPUT);
								output.put(group.getName(), droolsInput.getOutputScores());
								droolsInput.setOutputScores(new HashMap<String, Object>());
								responseData.put(Constants.OUTPUT, output);
							} else {
								Map<String, Object> output = new HashMap<>();
								output.put(group.getName(), droolsInput.getOutputScores());
								droolsInput.setOutputScores(new HashMap<String, Object>());
								responseData.put(Constants.OUTPUT, output);
							}
							logger.debug("Drools output scores for All groups:" + responseData.get(Constants.OUTPUT));
						}
					}
				} else {
					logger.debug("evaluating group");
					Group group = groupDao.findByIdAndIsDeleted(Long.parseLong(groupId), false);
					response = evaluateGroup(group, product, droolsInput, spreadsheet, variableCellLocations, evaluator,
							environment);
					if (response == null) {
						Map<String, Object> output = new HashMap<>();
						output.put(group.getName(), droolsInput.getOutputScores());
						droolsInput.setOutputScores(new HashMap<String, Object>());
						responseData.put(Constants.OUTPUT, output);
					}
					logger.debug("Drools output scores for '" + group.getName() + " 'group:"
							+ responseData.get(Constants.OUTPUT));
				}
				if (response == null && groupId.equals("")) {
					logger.debug("evaluating product expressions");
					response = evaluateProductExpressions(product, droolsInput, spreadsheet, variableCellLocations,
							evaluator);
					if (responseData.containsKey(Constants.OUTPUT)) {
						@SuppressWarnings("unchecked")
						Map<String, Object> output = (Map<String, Object>) responseData.get(Constants.OUTPUT);
						output.put(product.getName(), droolsInput.getSingleOutputScore(product.getName()));
						droolsInput.setOutputScores(new HashMap<String, Object>());
						responseData.put(Constants.OUTPUT, output);
					} else {
						Map<String, Object> output = new HashMap<>();
						output.put(product.getName(), droolsInput.getSingleOutputScore(product.getName()));
						droolsInput.setOutputScores(new HashMap<String, Object>());
						responseData.put(Constants.OUTPUT, output);
					}
					logger.debug("drools output scores for product::" + responseData.get(Constants.OUTPUT));
				}
				if (response == null) {
					// workbook.write(out);
					responseData.put("input", droolsInput.getInputValues());
					response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
							responseData);
				}
			}
			logger.info("<--Exiting FireRules()");
			return response;
		} catch (Exception e) {
			throw e;

		} finally {
			// out.close();
			workbook.close();
		}
	}

	@SuppressWarnings("unchecked")
	public GenericAPIResponse productionFireRules(BindingResult result, ProductionFireRulesInput input)
			throws Exception {
		logger.info("--> Entering productionFireRules()");
		GenericAPIResponse response = null;
		this.flag = false;
		this.responseData = new HashMap<>();
		Product product = productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(input.getIdentifier(), true, false)
				.getProduct();
		Set<Variable> primitiveVariables = new HashSet<>();
		Set<Variable> derivedVariables = new HashSet<>();
		if (input.getGroupId().equals("")) {
			response = fireRulesDataValidator.validateProductionProductFireRules(input, primitiveVariables,
					derivedVariables);
		} else {
			response = fireRulesDataValidator.validateProductionGroupFireRules(input, primitiveVariables,
					derivedVariables);
		}
		if (response == null) {
			List<Variable> allUsedPrimitiveVariables = new ArrayList<>();
			allUsedPrimitiveVariables.addAll(primitiveVariables);
			List<Variable> allUsedDerivedVariables = new ArrayList<>();
			allUsedDerivedVariables.addAll(derivedVariables);
			Collections.sort(allUsedDerivedVariables, new SortVariablesList());
			response = fireRules(product, input.getGroupId(), input.getInputData(), responseData,
					Constants.PRODUCTION_ENVIRONMENT, allUsedPrimitiveVariables, allUsedDerivedVariables);
		}
		logger.debug("Creating new Execution");
		Execution newExecution = new Execution(input.getIdentifier(), objectMapper.writeValueAsString(input),
				HelperFunctions.mapToString(responseData), product, true);
		executionDao.save(newExecution);
		logger.debug("Execution saved successfully");
		if (response.getStatus() != Constants.NOT_FOUND_ERROR_CODE) {
			responseData = (Map<String, Object>) response.getData();
			responseData.put("executionId", newExecution.getId());
			responseData.put("processInstanceId", input.getProcessInstanceId());
			response.setData(responseData);
		}
		logger.info("<--Exiting productionFireRules()");
		return response;
	}

	@SuppressWarnings("unchecked")
	public GenericAPIResponse testFireRules(TestFireRulesInput input) throws Exception {
		logger.info("--> Entering testFireRules()");
		GenericAPIResponse response;
		List<GenericAPIResponse> allResponses = new ArrayList<>();
		List<Long> productIds = input.getProductIds();
		for (Long id : productIds) {
			logger.debug("getting primitive and derived Variables for productId:" + id);
			Set<Variable> primitiveVariables = new HashSet<>();
			Set<Variable> derivedVariables = new HashSet<>();
			logger.debug("getting all required variables for product");
			Set<Map<String, String>> allRequiredVariables = fireRulesDataValidator
					.findAllVariablesRequiredForProduct(id, primitiveVariables, derivedVariables);
			logger.debug("All required variables ::" + allRequiredVariables);
			response = fireRulesDataValidator.validateTestEachProductFireRules(id, Long.parseLong(input.getUserId()),
					input.getInputData(), allRequiredVariables);
			if (response != null) {
				allResponses.add(response);
			} else {
				Map<String, Object> inputData = new HashMap<>();
				for (Map<String, String> variable : allRequiredVariables) {
					inputData.put(variable.get("name"),
							input.getInputData().get(variable.get("name") + "-" + variable.get("type")));
				}
				this.responseData = new HashMap<>();
				this.flag = false;
				Product product = productDao.findByIdAndIsDeleted(id, false);
				List<Variable> allUsedPrimitiveVariables = new ArrayList<>();
				allUsedPrimitiveVariables.addAll(primitiveVariables);
				List<Variable> allUsedDerivedVariables = new ArrayList<>();
				allUsedDerivedVariables.addAll(derivedVariables);
				Collections.sort(allUsedDerivedVariables, new SortVariablesList());
				response = fireRules(product, "", inputData, responseData, Constants.TEST_ENVIRONMENT,
						allUsedPrimitiveVariables, allUsedDerivedVariables);
				logger.debug("Creating new Execution");
				Execution newExecution = new Execution("", objectMapper.writeValueAsString(input),
						HelperFunctions.mapToString(responseData), product, false);
				executionDao.save(newExecution);
				logger.debug("Execution saved successfully:: executionId:" + newExecution.getId());
				responseData = (Map<String, Object>) response.getData();
				responseData.put("executionId", newExecution.getId());
				response.setData(responseData);
				allResponses.add(response);
			}
		}
		logger.info("<-- Exiting testFireRules()");
		response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allResponses);
		return response;
	}

	public GenericAPIResponse getAllRequiredVariables(GetRequiredVariables input) throws Exception {
		logger.info("--> Entering getAllRequiredVariables()");
		GenericAPIResponse response;
		List<Long> productIds = input.getProductIds();
		Set<Map<String, String>> allRequiredVariablesForAllProducts = new HashSet<>();
		for (Long id : productIds) {
			logger.debug("getting all required variables for productId:" + id);
			Set<Map<String, String>> allRequiredVariablesForProduct = fireRulesDataValidator
					.findAllVariablesRequiredForProduct(id, new HashSet<Variable>(), new HashSet<Variable>());
			allRequiredVariablesForAllProducts.addAll(allRequiredVariablesForProduct);
			logger.debug("Successfully got required variables");
		}
		response = new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				allRequiredVariablesForAllProducts);
		logger.info("<-- Exiting getAllRequiredVariables()");
		return response;
	}

	@SuppressFBWarnings({ "OBL_UNSATISFIED_OBLIGATION_EXCEPTION_EDGE", "DMI_HARDCODED_ABSOLUTE_FILENAME" })
	public GenericAPIResponse uploadTestUsingExcel(Long productId, MultipartFile xlsFile, BindingResult result,
			HttpServletRequest request, HttpServletResponse resp) throws Exception {
		logger.info("--> Entering uploadTestUsingExcel()");
		GenericAPIResponse response = null;
		Calendar calendar = Calendar.getInstance();
		File file = new File("/tmp/" + calendar.getTimeInMillis() + "-" + xlsFile.getOriginalFilename());
		Workbook workbook = null;
		List<String> errors = new ArrayList<>();
		OutputStream outputStream = new FileOutputStream(file);
		IoUtils.copy(xlsFile.getInputStream(), outputStream);
		outputStream.flush();
		outputStream.close();
		Map<String, Object> errorsMap = new HashMap<>();
		workbook = new XSSFWorkbook(file);
		logger.debug("parse workbook with test cases");
		JSONArray res = parseWorkBook(workbook, errors);
		if (!errors.isEmpty()) {
			return new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, HttpStatus.BAD_REQUEST.getReasonPhrase(),
					errors);
		}
		logger.debug("Workbook parsed Successfully");
		int numberOfTestCases = res.size();
		List outPutData = new ArrayList<>();
		String userId = String.valueOf(request.getAttribute("userId"));
		logger.debug("getting result for each TestCase");
		Iterator itr = res.iterator();
		while (itr.hasNext()) {
			TestFireRulesInput input = new TestFireRulesInput();
			input.setUserId(userId);
			List<Long> list = new ArrayList<>();
			list.add(productId);
			input.setProductIds(list);
			Map<String, Object> map = objectMapper.convertValue(itr.next(), Map.class);
			input.setInputData(map);
			fireRulesDataValidator.validateTestFireRules(input, result);
			response = checkErrors(result);
			response = testFireRules(input);

			if (response != null && response.getStatus() == 200) {
				logSuccessResponse(request, input.getUserId(), response, input);
			} else {
				logErrorResponse(request, input.getUserId(), response, input);
			}
			outPutData.add(response.getData());
		}
		logger.debug("added test results to excel");
		appendTestResultsToXLS(workbook, outPutData, resp, numberOfTestCases, errorsMap);
		if (!errorsMap.isEmpty())
			return new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, HttpStatus.BAD_REQUEST.getReasonPhrase(),
					errorsMap);
		logger.info("Downloading Excel");
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=" + "TestResults.xls");
		workbook.write(resp.getOutputStream());
		logger.info("Successfully Downloaded");
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, HttpStatus.OK.getReasonPhrase(), outPutData);
	}

	/*
	 * This Method is used to append Test Results to XLS
	 * 
	 * @param workbook
	 * 
	 * @param outPutData
	 * 
	 * @param resp
	 * 
	 * @param numberOfTestCases
	 * 
	 * @param errors
	 * 
	 * @throws IOException
	 */
	private void appendTestResultsToXLS(Workbook workbook, List outPutData, HttpServletResponse resp,
			int numberOfTestCases, Map<String, Object> errors) throws IOException {
		logger.info("--> Entering appendTestResultsToXLS()");
		Sheet sheet = workbook.getSheetAt(0);
		List<List<String>> testResults = new ArrayList<>();
		int numberOfRowsInSheet = sheet.getPhysicalNumberOfRows();
		int outPutStartRow = numberOfRowsInSheet + 2;
		int appendColStart = 1;
		Row row = sheet.createRow(outPutStartRow++);
		Cell cell = row.createCell(appendColStart);
		cell.setCellValue(Constants.OUTPUT.toUpperCase());
		boolean outputVarFlag = true;
		logger.debug("Appending test result for each test case");
		for (int i = 0; i < outPutData.size(); i++) {
			List<String> testRes = new ArrayList<>();
			JsonNode errorsList = objectMapper.valueToTree(outPutData.get(i)).get(0).get(Constants.OUTPUT_DATA)
					.get(Constants.DATA_ERRORS_KEY);
			JsonNode original = objectMapper.valueToTree(outPutData.get(i)).get(0).get(Constants.OUTPUT_DATA)
					.get(Constants.OUTPUT);
			if (original != null) {
				String executionId = objectMapper.valueToTree(outPutData.get(i)).get(0).get(Constants.OUTPUT_DATA)
						.get(Constants.DATA_EXECUTION_ID).asText();
				((ObjectNode) original).put(Constants.DATA_EXECUTION_ID, executionId);
				Iterator<String> fieldNamesIterator = original.fieldNames();
				while (fieldNamesIterator.hasNext()) {
					String fieldName = fieldNamesIterator.next();
					Row outRow = sheet.createRow(outPutStartRow++);
					if (outputVarFlag) {
						Cell outCell = outRow.createCell(1);
						outCell.setCellValue(fieldName);
					}
					JsonNode inner = original.get(fieldName);
					Iterator innerItr = inner.fieldNames();
					if (Iterators.size(inner.fieldNames()) > 0) {
						int rowIdx = 0;
						while (innerItr.hasNext()) {
							if (rowIdx != 0)
								outRow = sheet.createRow(outPutStartRow++);
							Cell outColCell = outRow.createCell(2);
							String fieldName1 = (String) innerItr.next();
							if (outputVarFlag) {
								outColCell.setCellValue(fieldName1);
							}
							testRes.add(inner.get(fieldName1).textValue());
							rowIdx++;
						}
					} else {
						testRes.add(inner.asText());
					}
				}
				outputVarFlag = false;
			} else {
				if (errorsList != null) {
					testRes.add(errorsList.toString());
				} else {
					String faildeMsg = objectMapper.valueToTree(outPutData.get(i)).get(0).get(Constants.OUTPUT_DATA)
							.get(Constants.DATA_MESSAGE_KEY).asText();
					String executionId = objectMapper.valueToTree(outPutData.get(i)).get(0).get(Constants.OUTPUT_DATA)
							.get(Constants.DATA_EXECUTION_ID).asText();
					testRes.add(faildeMsg);
					int outputRows = (outPutStartRow - numberOfRowsInSheet - 4);
					for (int idx = 1; idx < outputRows; idx++)
						testRes.add("");
					testRes.add(executionId);
				}
			}
			testResults.add(testRes);
		}
		int colStart = 3;
		for (int i = 0; i < testResults.size(); i++) {
			int rowStart = numberOfRowsInSheet + 3;
			List<String> testResultOutputList = testResults.get(i);
			for (String val : testResultOutputList) {
				Row row1 = sheet.getRow(rowStart);
				if (row1 == null)
					row1 = sheet.createRow(rowStart);
				Cell cell1 = row1.createCell(colStart);
				cell1.setCellValue(val);
				rowStart++;
			}
			colStart++;
		}
		logger.info("<-- Successfully appended :: Exiting appendTestResultsToXLS()");
	}

	private JSONArray parseWorkBook(Workbook workbook, List<String> errors) {
		logger.info("--> Entering parseWorkBook()");
		Sheet sheet = workbook.getSheetAt(0);
		List<String> varTypeList = new ArrayList<>();
		List<List<Object>> vals = new ArrayList<>();
		JSONArray varTypeArray = new JSONArray();
		int sheetFilledCoulmnsCount = 0;
		if (sheet.getPhysicalNumberOfRows() > 1) {
			int rowCount = 0;
			for (Row row : sheet) {
				if (rowCount == 0) {
					sheetFilledCoulmnsCount = row.getPhysicalNumberOfCells();
				}
				if (rowCount++ >= 1) {
					if (row.getCell(0) != null) {
						varTypeList
								.add(row.getCell(0).getStringCellValue() + "-" + row.getCell(1).getStringCellValue());
					}
				}
			}
			int testsStartCol = 3;
			int testCase = 1;
			if (sheetFilledCoulmnsCount <= 3) {
				errors.add(Constants.NO_TEST_CASES);
			}
			while (testsStartCol < sheetFilledCoulmnsCount) {
				List<Object> testList = new ArrayList<>();
				boolean skipHeadersRow = true;
				for (Row row : sheet) {
					if (testsStartCol <= row.getPhysicalNumberOfCells()) {
						if (skipHeadersRow) {
							skipHeadersRow = false;
							continue;
						}
						if (row.getCell(testsStartCol) == null
								|| row.getCell(testsStartCol).getCellTypeEnum() == CellType.BLANK) {
							errors.add(Constants.MISSING_FIELD_VALUE + row.getCell(0) + "' in Test" + testCase);
						}
						testList.add(row.getCell(testsStartCol));
					}
				}
				testsStartCol++;
				testCase++;
				vals.add(testList);
			}
		}
		for (int i = 0; i < vals.size(); i++) {
			List<Object> list = vals.get(i);
			JSONObject obj = new JSONObject();
			for (int j = 0; j < varTypeList.size(); j++) {
				obj.put(varTypeList.get(j), String.valueOf(list.get(j)));
			}
			varTypeArray.add(obj);
		}
		logger.info("<-- Exiting parseWorkBook()");
		return varTypeArray;

	}
}
