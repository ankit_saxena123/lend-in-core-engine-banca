package com.kuliza.lending.configurator.models;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Where;

@Entity
//@Table(name = "ce_del_model", uniqueConstraints = { @UniqueConstraint(columnNames = { "name"}) })
//@Where(clause = "is_deleted=false")
public class DRLModel extends BaseModelWithName{
	
	@Where(clause = "is_deleted=false")
	@OrderBy(clause = "id ASC")
	@Column(columnDefinition="LONGTEXT")
	private String drl;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;
	
	@Column
	private String path;
	
	public String getDrl() {
		return drl;
	}

	public void setDrl(String drl) {
		this.drl = drl;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public DRLModel() {
		super();
		this.setIsDeleted(false);
	}
	
	public DRLModel(String drl, String path, Product product, String name) {
		this.drl = drl;
		this.path = path;
		this.product = product;
		this.setName(name);
		this.setIsDeleted(false);
		
	}

}
