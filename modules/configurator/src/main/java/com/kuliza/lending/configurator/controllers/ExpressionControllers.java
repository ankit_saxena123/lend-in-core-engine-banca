package com.kuliza.lending.configurator.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewExpressions;
import com.kuliza.lending.configurator.pojo.UpdateExpression;
import com.kuliza.lending.configurator.services.ExpressionServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.ExpressionDataValidator;

@RestController
@RequestMapping(value = "/api/product/{productId:^[1-9]+[0-9]*$}")
public class ExpressionControllers {

	private static final Logger logger = LoggerFactory.getLogger(ExpressionControllers.class);

	@Autowired
	private ExpressionServices expressionServices;

	@Autowired
	private ExpressionDataValidator expressionDataValidator;

	// API to get Product expressions
	@RequestMapping(method = RequestMethod.GET, value = "/expression")
	public Object getProductsExpressions(@PathVariable(value = "productId") String productId,
			HttpServletRequest request) {
		logger.info("--> Entering get product Expression()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			logger.debug("getting expression for ProductId:" + productId + " and  userId :" + userId);
			if (!expressionDataValidator.validateGetProductExpressions(userId, productId)) {
				logger.error("Invalid product Id :" + productId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_ID_MESSAGE);
			} else {
				logger.debug("Getting product Expressions for ProductId :" + productId);
				response = expressionServices.getExpressions(productId, "");
			}
			if (response.getStatus() == 200) {
				logger.debug("--Product Expression fetched-- ");
				expressionServices.logSuccessResponse(request, userId, response);
			} else {
				expressionServices.logErrorResponse(request, userId, response);
			}
			logger.info("<--Exiting get product Expression()");
			return response;
		} catch (Exception e) {
			return expressionServices.handleException(e, request, userId);
		}
	}

	// API to get Group expressions
	@RequestMapping(method = RequestMethod.GET, value = "/group/{groupId:^[1-9]+[0-9]*$}/expression")
	public Object getGroupsExpressions(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, HttpServletRequest request) {
		logger.info("-->Entering get Group Expression()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!expressionDataValidator.validateGetGroupExpressions(userId, productId, groupId)) {
				logger.error("--Invalid group Id: " + groupId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_GROUP_ID_MESSAGE);
			} else {
				logger.debug("geting expressiuon for groupId :" + groupId + " productId: " + productId);
				response = expressionServices.getExpressions(productId, groupId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully fetched Expressions for group");
				expressionServices.logSuccessResponse(request, userId, response);
			} else {
				expressionServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get Group Expression()");
			return response;
		} catch (Exception e) {
			return expressionServices.handleException(e, request, userId);
		}
	}

	// API to create new expressions for Product
	@RequestMapping(method = RequestMethod.POST, value = "/expression")
	public Object productExpressionSubmit(@PathVariable(value = "productId") String productId,
			@RequestBody @Valid SubmitNewExpressions input, BindingResult result, HttpServletRequest request) {
		logger.info("-->Entering Create new product Expression()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = expressionServices.validateSubmitExpressions(input, result, userId, productId, "");
			if (response == null) {
				logger.debug("Creating new Product Expression : productId: " + productId);
				response = expressionServices.createNewExpressions(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully created product Expression");
				expressionServices.logSuccessResponse(request, userId, response, input);
			} else {
				expressionServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<--Exiting Create new product Expression()");
			return response;
		} catch (Exception e) {
			return expressionServices.handleException(e, request, userId, input);
		}
	}

	// API to create new expressions for Group
	@RequestMapping(method = RequestMethod.POST, value = "/group/{groupId:^[1-9]+[0-9]*$}/expression")
	public Object groupExpressionSubmit(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, @RequestBody @Valid SubmitNewExpressions input,
			BindingResult result, HttpServletRequest request) {
		logger.info("--> Entering create new group Expression()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = expressionServices.validateSubmitExpressions(input, result, userId, productId, groupId);
			if (response == null) {
				logger.debug("creating new group expression for groupId: " + groupId + " productId: " + productId);
				response = expressionServices.createNewExpressions(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully created group expresson");
				expressionServices.logSuccessResponse(request, userId, response, input);
			} else {
				expressionServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting create new group Expression()");
			return response;
		} catch (Exception e) {
			return expressionServices.handleException(e, request, userId, input);
		}
	}

	// API to update Product expression
	@RequestMapping(method = RequestMethod.PUT, value = "/expression/{expressionId:^[1-9]+[0-9]*$}")
	public Object productExpressionUpdate(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "expressionId") String expressionId, @RequestBody @Valid UpdateExpression input,
			BindingResult result, HttpServletRequest request) {
		logger.info("-->Entering product Expression Update()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = expressionServices.validateUpdateExpression(userId, productId, "", expressionId, input, result);
			if (response == null) {
				logger.debug("updating product expression productid: " + productId);
				response = expressionServices.updateExpression(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully update product expression");
				expressionServices.logSuccessResponse(request, userId, response, input);
			} else {
				expressionServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<--Exiting product Expression Update()");
			return response;
		} catch (Exception e) {
			return expressionServices.handleException(e, request, userId, input);
		}
	}

	// API to update Group expression
	@RequestMapping(method = RequestMethod.PUT, value = "/group/{groupId:^[1-9]+[0-9]*$}/expression/{expressionId:^[1-9]+[0-9]*$}")
	public Object groupExpressionUpdate(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, @PathVariable(value = "expressionId") String expressionId,
			@RequestBody @Valid UpdateExpression input, BindingResult result, HttpServletRequest request) {
		logger.info("-->Entering group Expression Update()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = expressionServices.validateUpdateExpression(userId, productId, groupId, expressionId, input,
					result);
			if (response == null) {
				logger.debug("Updating group expression for productId:" + productId + " groupId: " + groupId);
				response = expressionServices.updateExpression(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully updated group Expression");
				expressionServices.logSuccessResponse(request, userId, response, input);
			} else {
				expressionServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<--Exiting group Expression Update()");
			return response;
		} catch (Exception e) {
			return expressionServices.handleException(e, request, userId, input);
		}
	}
}
