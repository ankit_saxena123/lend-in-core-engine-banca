package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateVariable {

	private String userId;
	private String productId;
	private String variableId;

	@NotNull(message = "variableSource is a required key")
	@Size(min = Constants.MIN_LENGTH_STRING, max = Constants.MAX_LENGTH_STRING, message = Constants.VARIABLE_SOURCE_TOO_BIG_MESSAGE)
	private String variableSource;

	@NotNull(message = "variableDescription is a required key")
	@Size(min = Constants.MIN_LENGTH_STRING, max = Constants.MAX_LENGTH_STRING, message = Constants.VARIABLE_DESCRIPTION_TOO_BIG_MESSAGE)
	private String variableDescription;

	@NotNull(message = "expressionString is a required key")
	@Size(min = Constants.MIN_LENGTH_EXPRESSION, max = Constants.MAX_LENGTH_EXPRESSION, message = Constants.VARIABLE_EXPRESSION_TOO_BIG_MESSAGE)
	private String expressionString;

	public UpdateVariable() {
		this.variableSource = "";
		this.variableDescription = "";
		this.expressionString = "";
	}

	public UpdateVariable(String variableSource, String variableDescription, String expressionString) {
		this.variableSource = variableSource;
		this.variableDescription = variableDescription;
		this.expressionString = expressionString;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getVariableId() {
		return variableId;
	}

	public void setVariableId(String variableId) {
		this.variableId = variableId;
	}

	public String getVariableSource() {
		return variableSource;
	}

	public void setVariableSource(String variableSource) {
		this.variableSource = variableSource;
	}

	public String getVariableDescription() {
		return variableDescription;
	}

	public void setVariableDescription(String variableDescription) {
		this.variableDescription = variableDescription;
	}

	public String getExpressionString() {
		return expressionString;
	}

	public void setExpressionString(String expressionString) {
		this.expressionString = expressionString;
	}

	// @Override
	// public String toString() {
	// StringBuilder updateVariable = new StringBuilder();
	// updateVariable.append("{ ");
	// updateVariable.append("variableSource : " + variableSource + ", ");
	// updateVariable.append("variableDescription : " + variableDescription);
	// updateVariable.append(" }");
	// return updateVariable.toString();
	//
	// }

}
