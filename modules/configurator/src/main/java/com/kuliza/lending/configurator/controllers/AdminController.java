package com.kuliza.lending.configurator.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProductCategory;
import com.kuliza.lending.configurator.services.AdminServices;
import com.kuliza.lending.configurator.validators.AdminDataValidator;

@RestController
@RequestMapping("/admin")
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	private AdminServices adminServices;

	@Autowired
	private AdminDataValidator adminDataValidator;

	// API to create new product categories
	@RequestMapping(method = RequestMethod.POST, value = "/product-category")
	public Object createProductCategory(@Valid @RequestBody SubmitNewProductCategory input, BindingResult result,
			HttpServletRequest request) {
		logger.debug("-->Entering Create Product Category()");
		String userId = String.valueOf(request.getAttribute("userId"));
		try {
			GenericAPIResponse response = null;
			adminDataValidator.validateNewProductCategory(input, result);
			response = adminServices.checkErrors(result);
			if (response == null) {
				response = adminServices.createProductCategory(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("SuccessFully Created product Category for userId :" + userId);
				adminServices.logSuccessResponse(request, userId, response, input);
			} else {
				adminServices.logErrorResponse(request, userId, response, input);
			}
			logger.debug("<-- Exiting Create product Category()");
			return response;
		} catch (Exception e) {
			return adminServices.handleException(e, request, userId, input);
		}
	}
}
