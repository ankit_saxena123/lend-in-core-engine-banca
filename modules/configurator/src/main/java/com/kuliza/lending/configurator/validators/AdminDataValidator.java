package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.pojo.SubmitNewProductCategory;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class AdminDataValidator extends BasicDataValidator {

	@Autowired
	private ProductCategoryDao productCategoryDao;

	public void validateNewProductCategory(SubmitNewProductCategory obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (productCategoryDao.findByNameAndIsDeleted(obj.getProductCategoryName(), false) != null) {
			errors.rejectValue(Constants.PRODUCT_CATEGORY_NAME, Constants.INVALID_PRODUCT_CATEGORY_NAME,
					Constants.PRODUCT_CATEGORY_NAME_TAKEN_MESSAGE);
		}
	}

}
