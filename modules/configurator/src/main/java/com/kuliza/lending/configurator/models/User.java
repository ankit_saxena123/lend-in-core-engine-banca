package com.kuliza.lending.configurator.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kuliza.lending.common.model.BaseModel;

//@JsonFilter("userFilter")
@Entity
@Table(name = "ce_user")
@JsonIgnoreProperties({ "id" })
public class User extends BaseModel {

	@Column(unique = true, nullable = false)
	private String userName;
	

	public User() {
		super();
		this.setIsDeleted(false);
	}

	public User(long id, String userName) {
		super();
		this.setId(id);
		this.userName = userName;
		this.setIsDeleted(false);
	}

	public User(String userName) {
		this.userName = userName;
		this.setIsDeleted(false);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
