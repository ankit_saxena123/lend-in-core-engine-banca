package com.kuliza.lending.configurator.pojo;

import java.util.Map;

import org.kie.api.runtime.KieContainer;

public class KieContainerBean {

	private Map<String, Map<String, KieContainer>> kieContainers;

	public KieContainerBean(Map<String, Map<String, KieContainer>> kieContainers) {
		super();
		this.kieContainers = kieContainers;
	}

	public KieContainerBean() {
		super();
	}

	public Map<String, Map<String, KieContainer>> getKieContainers() {
		return kieContainers;
	}

	public void setKieContainers(Map<String, Map<String, KieContainer>> kieContainers) {
		this.kieContainers = kieContainers;
	}

	public void setSingleContainer(String environment, String containerId, KieContainer kieContainer) {
		Map<String, KieContainer> containers = kieContainers.get(environment);
		containers.put(containerId, kieContainer);
	}

	public KieContainer getSingleContainer(String environment, String containerId) {
		Map<String, KieContainer> containers = kieContainers.get(environment);
		return containers.get(containerId);
	}

}
