package com.kuliza.lending.configurator.models;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface DRLModelDao extends CrudRepository<DRLModel, Long> {
	public DRLModel findById(long id);
	
	public List<DRLModel> findByIsDeleted(boolean isDeleted);

	public DRLModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<DRLModel> findByProductIdAndIsDeleted(long productId, boolean isDeleted);
	
	public DRLModel findByPathAndIsDeleted(String path, boolean isDeleted);
}
