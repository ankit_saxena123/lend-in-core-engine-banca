package com.kuliza.lending.configurator.controllers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.models.FileUploadModel;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.GetRequiredVariables;
import com.kuliza.lending.configurator.pojo.ProductionFireRulesInput;
import com.kuliza.lending.configurator.pojo.TestFireRulesInput;
import com.kuliza.lending.configurator.services.FireRulesServices;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import com.kuliza.lending.configurator.validators.FireRulesDataValidator;

@RestController
@RequestMapping(value = "/api")
public class FireRulesController {

	private static final  Logger logger = LoggerFactory.getLogger(FireRulesController.class);

	@Autowired
	private FireRulesServices fireRulesServices;

	@Autowired
	private FireRulesDataValidator fireRulesDataValidator;

	// API to fire rules in production environment
	@RequestMapping(method = RequestMethod.POST, value = "/production/fire-rules")
	public Object productionFireRules(@Valid @RequestBody ProductionFireRulesInput input, BindingResult result,
			HttpServletRequest request) {
		logger.info("-->Entering production Fire Rules()");
		String userId = String.valueOf(request.getAttribute("userId"));
		try {
			GenericAPIResponse response = null;
			input.setUserId(userId);
			fireRulesDataValidator.validateProductionFireRules(input, result);
			response = fireRulesServices.checkErrors(result);
			if (response == null) {
				logger.debug("firing rule in production groupId: " + input.getGroupId() + " userId: " + userId);
				response = fireRulesServices.productionFireRules(result, input);
			}
			if (response.getStatus() == 200) {
				logger.debug("successfully fired rules in production environment");
				fireRulesServices.logSuccessResponse(request, userId, response, input);
			} else {
				fireRulesServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting production Fire Rules()");
			return response;
		} catch (Exception e) {
			return fireRulesServices.handleException(e, request, userId, input);
		}
	}

	// API to fire rules in test environment
	@RequestMapping(method = RequestMethod.POST, value = "/test/fire-rules")
	public Object testFireRules(@Valid @RequestBody TestFireRulesInput input, BindingResult result,
			HttpServletRequest request) {
		logger.info("--> Entering test fire rules()");
		String userId = String.valueOf(request.getAttribute("userId"));
		try {
			GenericAPIResponse response;
			input.setUserId(userId);
			fireRulesDataValidator.validateTestFireRules(input, result);
			response = fireRulesServices.checkErrors(result);
			if (response == null) {
				logger.debug("firing rules in test userId: " + userId);
				response = fireRulesServices.testFireRules(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully fired rules in test environment");
				fireRulesServices.logSuccessResponse(request, userId, response, input);
			} else {
				fireRulesServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting test fire rules()");
			return response;
		} catch (Exception e) {
			return fireRulesServices.handleException(e, request, userId, input);
		}
	}

	// API to Get Required Variable To Fire Rules For Products
	@RequestMapping(method = RequestMethod.POST, value = "/fire-rules/variables")
	public Object getRequiredVariablesList(@Valid @RequestBody GetRequiredVariables input, BindingResult result,
			HttpServletRequest request) {
		logger.info("--> Entering get required variables List()");
		String userId = String.valueOf(request.getAttribute("userId"));
		try {
			GenericAPIResponse response;
			input.setUserId(userId);
			fireRulesDataValidator.validateAllRequiredVariables(input, result);
			response = fireRulesServices.checkErrors(result);
			if (response == null) {
				logger.debug("getting all required variables");
				response = fireRulesServices.getAllRequiredVariables(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully fetched required variables");
				fireRulesServices.logSuccessResponse(request, userId, response, input);
			} else {
				fireRulesServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<--Exiting get required variables List()");
			return response;
		} catch (Exception e) {
			return fireRulesServices.handleException(e, request, userId, input);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/fire-rules/variables/generateXLS")
	public void getRequiredVariablesListXLS(@Valid @RequestBody GetRequiredVariables input, BindingResult result,
			HttpServletRequest request, HttpServletResponse resp) throws Exception {
		logger.info("--> Entering get required variables List in XLS()");
		String userId = String.valueOf(request.getAttribute("userId"));
		try {
			GenericAPIResponse response = null;
			input.setUserId(userId);
			fireRulesDataValidator.validateAllRequiredVariables(input, result);
			response = fireRulesServices.checkErrors(result);
			if (response == null) {
				logger.debug("getting all required variables");
				response = fireRulesServices.getAllRequiredVariables(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully fetched required variables");
				fireRulesServices.logSuccessResponse(request, userId, response, input);
			} else {
				fireRulesServices.logErrorResponse(request, userId, response, input);
			}
			if (response != null) {
				File temp = File.createTempFile("tempfile", ".csv");
				List<Map<String, Object>> variablesList = new ArrayList<>(
						(Set<Map<String, Object>>) response.getData());
				logger.debug("Adding Headers in XLS for variables");
				List<String> headers = new ArrayList<>();
				headers.add("Variable Name");
				headers.add("Type");
				headers.add("Description");

				List<String> dataName = new ArrayList<>();
				dataName.add("name");
				dataName.add("type");
				dataName.add("description");

				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(temp), "UTF-8"));
				HelperFunctions.writeToCSV(writer, variablesList, headers, dataName);
				writer.close();
				logger.debug("Downloading XLS");
				HelperFunctions.generateResponseXls(temp, resp, "ProductVariable.xls");
				logger.debug("Successfully downloaded excel sheet with variables");
			}
			logger.info("<--Exiting get required variables List in XLS()");
		} catch (Exception e) {
			logger.error("Error while getting required variables for product " + e);
			e.printStackTrace();
		}
	}

	@PostMapping(value = "/test/fire-rules/{productId}/XLSDownload", consumes = "multipart/form-data")
	public GenericAPIResponse uploadTestXLSForProduct(@PathVariable(name = "productId") Long productId,
			@Valid FileUploadModel file, BindingResult result, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		GenericAPIResponse resp = fireRulesServices.uploadTestUsingExcel(productId, file.getFile(), result, request,
				response);
		return resp;
	}

}
