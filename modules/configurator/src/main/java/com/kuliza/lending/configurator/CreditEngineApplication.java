package com.kuliza.lending.configurator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Lazy;

import com.kuliza.lending.configurator.models.DRLModelDao;
import com.kuliza.lending.configurator.pojo.KieContainerBean;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;

import liquibase.integration.spring.SpringLiquibase;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kuliza" }, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class) })
@EnableConfigurationProperties(LiquibaseProperties.class)
public class CreditEngineApplication extends SpringBootServletInitializer {

	@Autowired
	private LiquibaseProperties properties;

	@Autowired
	private DataSource dataSource;

	@Autowired
	@Lazy
	private DRLModelDao drlModelDao;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CreditEngineApplication.class);
	}

	public static void main(String[] args) throws IOException {
		SpringApplication.run(CreditEngineApplication.class, args);
	}

	@Bean
	@DependsOn("liquibase")
	public KieContainerBean getKieContainerBean() throws FileNotFoundException, IOException {
		Map<String, Map<String, KieContainer>> kieContainers = new HashMap<>();
		Map<String, KieContainer> prodContainer = new HashMap<>();
		for (Map.Entry<Long, String> product : HelperFunctions.getAllProductPaths(drlModelDao).entrySet()) {
			Long productId = product.getKey();
			String path = product.getValue();
			String[] kiePathArray = path.split("/");
			KieContainer kieContainer = HelperFunctions.buildKieContainerAgain(
					Constants.RULES_PATH + kiePathArray[kiePathArray.length - 3] + "/", productId, drlModelDao);
			prodContainer.put(kiePathArray[kiePathArray.length - 3], kieContainer);
		}
		kieContainers.put(Constants.TEST_ENVIRONMENT, prodContainer);
		kieContainers.put(Constants.PRODUCTION_ENVIRONMENT, prodContainer);
		KieContainerBean containerBean = new KieContainerBean(kieContainers);
		return containerBean;
	}

	@Bean(name = "liquibase")
	@LiquibaseDataSource
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		liquibase.setLabels(this.properties.getLabels());
		liquibase.setChangeLogParameters(this.properties.getParameters());
		liquibase.setRollbackFile(this.properties.getRollbackFile());
		liquibase.setDatabaseChangeLogLockTable("CE_DATABASECHANGELOGLOCK");
		liquibase.setDatabaseChangeLogTable("CE_DATABASECHANGELOG");
		return liquibase;

	}
}
