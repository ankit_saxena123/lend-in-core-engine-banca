package com.kuliza.lending.authorization.unittest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.controller.AuthorizationController;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.authorization.utils.UnitTestConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.pojo.UserLoginDetails;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(AuthorizationController.class)
public class AuthorizationControllerTest {
	@Mock
	KeyCloakManager keyCloakManager;

	@Mock
	KeyCloakService keyCloakService;

	@InjectMocks
	AuthorizationController authorizationController;

	@Test
	public void loginTest() {
		String loginRequestBody = UnitTestConstants.userLoginDetailsJSONString;
		try {			
			ObjectMapper objectMapper = new ObjectMapper();
			UserLoginDetails userDetails = objectMapper.readValue(loginRequestBody, UserLoginDetails.class);
			Mockito.when(keyCloakManager.loginWithEmailAndPassword(userDetails.getEmail(), userDetails.getPassword(), false))
					.thenReturn(new ApiResponse(HttpStatus.OK, ""));
			Assert.assertEquals(200, authorizationController.login(userDetails).getStatus(), 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
