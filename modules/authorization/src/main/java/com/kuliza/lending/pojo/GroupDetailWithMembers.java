package com.kuliza.lending.pojo;

import java.util.List;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

public class GroupDetailWithMembers extends GroupRepresentation {

  List<UserRepresentation> memberList;

  public List<UserRepresentation> getMemberList() {
    return memberList;
  }

  public void setMemberList(List<UserRepresentation> memberList) {
    this.memberList = memberList;
  }

  public GroupDetailWithMembers(GroupRepresentation groupRepresentation){
    super();
    this.setName(groupRepresentation.getName());
    this.setAttributes(groupRepresentation.getAttributes());
    this.setId(groupRepresentation.getId());
    this.setAccess(groupRepresentation.getAccess());
    this.setClientRoles(groupRepresentation.getClientRoles());
    this.setPath(groupRepresentation.getPath());
    this.setRealmRoles(groupRepresentation.getRealmRoles());
    this.setSubGroups(groupRepresentation.getSubGroups());
  }
}
