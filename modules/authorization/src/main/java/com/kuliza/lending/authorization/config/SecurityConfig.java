package com.kuliza.lending.authorization.config;

import com.kuliza.lending.authorization.config.keycloak.*;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.utils.AuthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

import static com.kuliza.lending.common.utils.Constants.CE_ADMIN_USER_ROLE;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private KeyCloakAuthFilter keyCloakAuthFilter;

  @Autowired
  private KeyCloakAuthenticationEntryPoint keyCloakAuthenticationEntryPoint;

  @Autowired
  KeyCloakAuthenticationProvider keyCloakAuthenticationProvider;

  @Autowired
  KeyCloakAccessDeniedHandler keyCloakAccessDeniedHandler;

  @Autowired
  private KeyCloakConfig keyCloakConfig;
  
  @Value(value="${actuator.username}")
  String username;
  
  @Value(value="${actuator.password}")
  String password;

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(keyCloakAuthenticationProvider);
    auth.inMemoryAuthentication()
    .withUser(username).password(password).roles(Constants.ACTUATOR_ROLE);
  }

  @Bean
  public FilterRegistrationBean registration(KeyCloakAuthFilter filter) {
    FilterRegistrationBean registration = new FilterRegistrationBean(filter);
    registration.setEnabled(false);
    return registration;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    final CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(Arrays.asList(keyCloakConfig.getAllowedOrigins().split(",")));
    configuration.setAllowedMethods(
        Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
    configuration.addExposedHeader("x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN, Content-Disposition");
    // setAllowCredentials(true) is important, otherwise:
    // The value of the 'Access-Control-Allow-Origin' header in the response
    // must not be the
    // wildcard '*' when the request's credentials mode is 'include'.
    configuration.setAllowCredentials(true);
    // setAllowedHeaders is important! Without it, OPTIONS preflight request
    // will fail with 403 Invalid CORS request
    configuration
        .setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    
    http.authorizeRequests().antMatchers(AuthConstants.VIEW_PDF_NO_AUTH, AuthConstants.FEC_VTIGER, AuthConstants.INSURER_SCHEDULAR,AuthConstants.EXPIRE_JOURNEY_SCHEDULAR,AuthConstants.IPAT_REPAY_SCHEDULAR,AuthConstants.OFFLINE_POLICY_SAVE).permitAll()
    .antMatchers(Constants.CE_PROD_DEPLOYMENT_ENDPOINT)
        .hasAnyRole(CE_ADMIN_USER_ROLE)
        .antMatchers(Constants.WF_MPIN_VALIDATION_ENDPOINT, Constants.WF_MPIN_SET_ENDPOINT,Constants.WF_USER_ENDPOINT
        		, Constants.WF_WORKFLOW_ENDPOINT)
        .hasAnyRole(CE_ADMIN_USER_ROLE, AuthConstants.DEFAULT_USER_ROLE)
        .antMatchers(Constants.WF_VALIDATION_ENDPOINT, Constants.WF_CONFIG_ENDPOINT, Constants.WF_EMAIL_CONFIRM_ENDPOINT).permitAll()
        .antMatchers(Constants.WF_USER_DOCUMENT_ENDPOINT)
        .hasAnyRole(AuthConstants.DEFAULT_USER_ROLE)
        .antMatchers(Constants.WF_DOCUMENT_PORTAL_DOWNLOAD_ENDPOINT)
        .not().hasAnyRole(AuthConstants.DEFAULT_USER_ROLE)
        .antMatchers(HttpMethod.GET, Constants.CE_PRODUCT_ENDPOINT,
            Constants.CE_PRODUCT_LIST_ENDPOINT, Constants.CE_PRODUCT_ALL)
        .hasAnyRole(CE_ADMIN_USER_ROLE, Constants.CE_MANAGER_USER_ROLE,
            Constants.CE_SUPERVISOR_USER_ROLE)
        .antMatchers(Constants.CE_PRODUCT_TEST_ENDPOINT)
        .hasAnyRole(CE_ADMIN_USER_ROLE, Constants.CE_MANAGER_USER_ROLE,
            Constants.CE_SUPERVISOR_USER_ROLE)
        .antMatchers(Constants.CE_PRODUCT_LIST_ALL_ENDPOINT, Constants.CE_PRODUCT_CATEGORY_ENDPOINT,
            Constants.CE_PRODUCT_CATEGORY_LIST_ENDPOINT, Constants.CE_PRODUCT_FIRE_RULES_ENDPOINT,
            Constants.CE_PRODUCT_PUBLISH_ENDPOINT)
        .hasAnyRole(CE_ADMIN_USER_ROLE, Constants.CE_SUPERVISOR_USER_ROLE)
        .antMatchers(AuthConstants.AUTH_LIST_GROUPS_ENDPOINT,
            AuthConstants.AUTH_CREATE_GROUPS_ENDPOINT, AuthConstants.AUTH_CREATE_ROLES_ENDPOINT,
            AuthConstants.AUTH_ADD_ROLES_TO_USER_ENDPOINT, AuthConstants.AUTH_LIST_USERS_ENDPOINT,
            AuthConstants.AUTH_LOGOUT_ACTIVE_SESSION_ENDPOINT)
        .hasAnyRole(AuthConstants.AUTH_REALM_ADMIN_USER)
        .antMatchers(AuthConstants.AUTH_LOGIN_ENDPOINT, AuthConstants.AUTH_LOGOUT_ENDPOINT,
            AuthConstants.AUTH_SIGNUP_ENDPOINT, AuthConstants.AUTH_REFRESH_TOKEN_ENDPOINT,
            Constants.JOURNEY_CUST_ENDPOINT, Constants.JOURNEY_DEV_ENDPOINT,
            Constants.JOURNEY_LOGIN_ENDPOINT, Constants.JOURNEY_OTP_ENDPOINT,
            Constants.CONFIG_API_ENDPOINT, Constants.LOS_LMS_CONFIG_ENDPOINTS,
            Constants.COLLECTIONS_API_ENDPOINTS, AuthConstants.AUTH_ENCODE_USER_PASSWORDS,Constants.FEC_END_POINT)
        .permitAll().antMatchers(Constants.CE_PRODUCT_API_ENDPOINTS)
        .hasAnyRole(CE_ADMIN_USER_ROLE, Constants.CE_SUPERVISOR_USER_ROLE)
        .antMatchers(Constants.COLLECTIONS_API_ENDPOINTS).permitAll()
        .antMatchers(Constants.PORTAL_DASHBOARD_BASE_URL).permitAll()
//        .antMatchers(Constants.PORTAL_DASHBOARD_BASE_URL).hasAnyRole(Constants.PORTAL_DASHBOARD_LOGIN__ROLE_ADMIN)
        .antMatchers(Constants.JOURNEY_INITIATE_ENDPOINTS)
        .hasAnyRole(AuthConstants.DEFAULT_USER_ROLE, CE_ADMIN_USER_ROLE).anyRequest().authenticated().and()
        .addFilterBefore(keyCloakAuthFilter, SecurityContextHolderAwareRequestFilter.class)
        .exceptionHandling().accessDeniedHandler(keyCloakAccessDeniedHandler)
        .authenticationEntryPoint(keyCloakAuthenticationEntryPoint).and().headers().cacheControl()
        .disable().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and().csrf().disable().logout().disable().cors().configurationSource(source);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources",
        "/configuration/security", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**");
  }

}
