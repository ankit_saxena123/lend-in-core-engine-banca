package com.kuliza.lending.authorization.exception;

public class InvalidAlgorithmException extends RuntimeException {

  private static final long serialVersionUID = 7169518961042958739L;

  public InvalidAlgorithmException(String message) {
    super(message);
  }

}
