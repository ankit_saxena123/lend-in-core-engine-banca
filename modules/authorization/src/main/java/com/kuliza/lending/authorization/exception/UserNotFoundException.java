package com.kuliza.lending.authorization.exception;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 7169518961042958739L;

	public UserNotFoundException(String message) {
		super(message);
	}
}
