package com.kuliza.lending.authorization.config.keycloak;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "keycloak")
public class KeyCloakConfig {

	private String host;
	private String clientId;
	private String clientSecret;
	private String realm;
	private String adminUsername;
	private String adminEmail;
	private String adminPassword;
	private String allowedOrigins;

	private int offlineSessions;
	
	private int activeSessions;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getAdminUsername() {
		return adminUsername;
	}

	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getAuthEndpoint() {
		return this.host.concat("auth");
	}

	public String getRealmEndpoint() {
		return this.host.concat("auth/realms/").concat(this.realm).concat("/");
	}

	public String getOpenidConnectEndpoint() {
		return this.getRealmEndpoint().concat("protocol/openid-connect/");
	}

	public String getOpenidConnectLoginEndpoint() {
		return this.getOpenidConnectEndpoint().concat("token");
	}

	public String getOpenidConnectAuthEndpoint() {
		return this.getOpenidConnectEndpoint().concat("token/introspect");
	}

	public String getOpenidConnectLogoutEndpoint() {
		return this.getOpenidConnectEndpoint().concat("logout");
	}

	public int getOfflineSessions() {
		return this.offlineSessions;
	}

	public void setOfflineSessions(int offlinesessions) {
		this.offlineSessions = offlinesessions;
	}

	public int getActiveSessions() {
		return activeSessions;
	}

	public void setActiveSessions(int activeSessions) {
		this.activeSessions = activeSessions;
	}

	public String getAllowedOrigins() {
		return allowedOrigins;
	}

	public void setAllowedOrigins(String allowedOrigins) {
		this.allowedOrigins = allowedOrigins;
	}
}
