package com.kuliza.lending.authorization.constants;

public interface Constants {

  // Sample API Parameters
  String GROUP_ID_EXAMPLE = "a2db397c-3e93-4681-be30-69291d1e9315";
  String ENCODING_SUCCESS_MESSAGE = "Journey users passwords encoded successfully";
}
