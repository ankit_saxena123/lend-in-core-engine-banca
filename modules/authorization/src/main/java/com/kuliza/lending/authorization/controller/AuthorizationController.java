package com.kuliza.lending.authorization.controller;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.constants.Constants;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.pojo.UserDetails;
import com.kuliza.lending.pojo.UserLoginDetails;
import com.kuliza.lending.pojo.UserRoles;
import com.kuliza.lending.pojo.UserToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/****
 * 
 * @author Naveen Dhalaria
 *
 */
@RestController
@RequestMapping("/auth")
@Api(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AuthorizationController {

  @Autowired
  KeyCloakManager keyCloakManager;

  @Autowired
  KeyCloakService keyCloakService;

  /**
   * This method is to register a User
   * 
   * @return UserRepresentation is Successfully created.
   */
  @PostMapping(value = "/signup")
  public ApiResponse signup(@Valid @RequestBody UserDetails user) {
    return keyCloakManager.signup(user.getFirstname(), user.getLastname(), user.getUsername(),
        user.getEmail(), user.getPassword(), user.getRoles(), user.getAttributes(), false);
  }

  /**
   * This method is used to login using email and password
   * 
   * @param user - details for user
   * @return json response with user token and other details
   */
  @PostMapping(value = "/login")
  public ApiResponse login(@Valid @RequestBody UserLoginDetails user) {
    return keyCloakManager.loginWithEmailAndPassword(user.getEmail(), user.getPassword(), user.isEncoded());
  }

  /**
   * This method will take refresh token and return access token and other user details if token is
   * vaild
   * 
   * @param refresh_token
   * @return json response with token details
   */
  @PostMapping(value = "/refresh-token")
  public ApiResponse refresh(@RequestBody UserToken token) {
    return keyCloakManager.getAccessTokenByRefreshToken(token.getRefresh_token());
  }

  /**
   * This method is used for logout user from current active session
   * 
   * @param token
   * @param request
   * @return Success or fail response
   */
  @PostMapping(value = "/logout")
  public ApiResponse logout(@RequestBody UserToken token, HttpServletRequest request) {
    return keyCloakManager.logout(token.getRefresh_token());
  }

  /**
   * to get all the registered users
   * 
   * @return usernames
   */
  @GetMapping("/all-users")
  public ApiResponse allUsersList() {
    List<String> allUsers = keyCloakService.allUsers();
    return new ApiResponse(HttpStatus.OK, "OK", allUsers);
  }

  /***
   * This end Point is for clear all the Active Sessions for a user associated with user while
   * creating them
   * 
   * @param request
   * @return
   */
  @PostMapping("/sessions/logout")
  public ApiResponse allSessionLogout(HttpServletRequest request,
      @RequestBody Map<String, String> user) {

    UserRepresentation rep = keyCloakService.findUserByEmail(user.get("email"));
    String userID = null;
    if (rep != null) {
      userID = rep.getId();
    }
    keyCloakService.logoutUserFromAllSessions(userID);
    return new ApiResponse(HttpStatus.OK, "OK", "cleared all Sessions for User " + rep.getEmail());
  }

  /***
   * This end Point is for creating client level roles these roles can be associated with user while
   * creating them
   * 
   * @param UserRoles
   * @return a Map for roles with status "Created" or "Existing" role
   */
  @PostMapping("/roles/add-new-role")
  public ApiResponse createNewRole(@RequestBody UserRoles roles, HttpServletRequest request) {

    Map<String, String> rolesMap = keyCloakManager.createNewRole(roles.getRoles());
    return new ApiResponse(HttpStatus.OK, "OK", rolesMap);
  }

  /***
   * This end Point is for adding client level roles to the user
   * 
   * @param UserRoles
   * @return a Map for roles with status "Created" or "Existing" role
   */
  @PostMapping("/users/roles/add-user-roles")
  public ApiResponse adduserToRole(@RequestBody UserRoles user) {

    return new ApiResponse(HttpStatus.OK, "OK",
        keyCloakManager.addUserToRole(user.getEmail(), user.getRoles()));

  }

  /***
   * This end Point is for creating new Groups in Realm
   * 
   * @param requestBody
   * @return a Map for roles with status "Created" or "Existing" Group
   */
  @PostMapping("/groups/create-new-group")
  public ApiResponse createNewGroups(@RequestBody Map<String, List<String>> requestBody) {

    return new ApiResponse(HttpStatus.OK, "OK",
        keyCloakManager.createGroups(requestBody.get("groups")));
  }

  /***
   * This end Point is for Listing all Groups in Realm
   * 
   * @param requestBody
   * @return a Map for roles with status "Created" or "Existing" Group
   */
  @GetMapping("/groups/all-groups")
  public ApiResponse createNewGroups() {

    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.AllGroups());
  }

  /***
   * Return Groups associated with the user.
   * 
   * @param request
   * @return list of groups with which user is associated.
   */
  @GetMapping("/user/groups-with-username")
  @ApiOperation("List of groups associated with user")
  public ApiResponse userGroups(ServletRequest request) {
    // Retrieving userName(email) from request object
    String email = (String) request.getAttribute("userName");
    // TODO: modify request object to return userId, and retrieve information based on that.
    // Retrieving user details based on` userName
    UserRepresentation userRepresentation = this.keyCloakService.findUserByEmail(email);
    return keyCloakService.userGroupsWithUserNAme(userRepresentation.getId(), email);
  }

  /***
   * Group Details API
   * 
   * @param id : Group ID.
   * @return group details
   */
  @GetMapping("/group/details")
  @ApiOperation("Group details")
  public ApiResponse groupDetails(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.groupDetails(id));

  }

  /***
   * Group Members Listing API
   * 
   * @param id(Group ID)
   * @return list of all members associated with Group
   */
  @GetMapping("/group/members")
  @ApiOperation("List members associated with Group")
  public ApiResponse groupMembers(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.groupMembers(id));

  }


  /***
   * Group+SubGroup Members Listing API
   * 
   * @param id(Group ID)
   * @return list of all members associated with Group and it's SubGroups
   */
  @GetMapping("/group/members/all")
  @ApiOperation("List members associated with Group and it's subgroups")
  public ApiResponse nestedGroupMembers(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.subGroupDetailsWithMembers(id));

  }

  /***
   * Group+SubGroup Members Listing API
   *
   * @param id(Group ID)
   * @return list of all members associated with Group and it's SubGroups
   */
  @GetMapping("/group/details/sub")
  @ApiOperation("List sub groups associated with Group with member list")
  public ApiResponse subGroupDetailsWithMembers(
      @RequestParam @ApiParam(value = "Group ID", example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.subGroupDetailsWithMembers(id));

  }

  /***
   * Retrieve Parent Group
   * 
   * @param id(Group ID)
   * @return Parent Group if any.
   */
  @GetMapping("/group/parent")
  @ApiOperation("Parent Group for a SubGroup")
  public ApiResponse parentGroup(@RequestParam @ApiParam(value = "SubGroup ID",
      example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.parentGroup(id));

  }

  /***
   * Parent Groups Members Listing API
   * 
   * @param id(Group ID)
   * @return list of all members associated with Parent Group
   */
  @GetMapping("/group/parent/members")
  @ApiOperation("Parent Group Members for a SubGroup")
  public ApiResponse parentGroupMembers(@RequestParam @ApiParam(value = "SubGroup ID",
      example = Constants.GROUP_ID_EXAMPLE) String id) {
    return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.parentGroupMembers(id));

  }

  // TODO: Delete below code once all implementations are using this branch as base branch.
  /***
   * Password hashing for all journey users.
   *
   * @return Success if all users hashing is completed else Error.
   */
  @GetMapping("/encode-user-passwords")
  @ApiOperation("Encode journey users password")
  public ApiResponse encodeJourneyUsersPasswords() {
    keyCloakService.hashPasswordForAllJourneyUsers();
    return new ApiResponse(HttpStatus.OK, "OK", Constants.ENCODING_SUCCESS_MESSAGE);
  }
}
