package com.kuliza.lending.utils;

import com.kuliza.lending.authorization.exception.InvalidAlgorithmException;
import com.kuliza.lending.authorization.exception.RoleDoesNotExistException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.UserSessionRepresentation;

import static com.kuliza.lending.authorization.exception.ErrorMessages.INVALID_ENCODING_ALGORITHM;

/***
 * 
 * @author Naveen Dhalaria
 *
 */

public class AuthUtils {

	/**
	 * Detects if the current device is an iPhone.
	 */
	public static boolean detectIphone(String userAgent) {
		// The iPod touch says it's an iPhone! So let's disambiguate.
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPHONE) != -1 && !detectIpod(userAgent);
	}

	/**
	 * Detects if the current device is an iPod Touch.
	 */
	public static boolean detectIpod(String userAgent) {
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPHONE) != -1;
	}

	/**
	 * Detects if the current device is an iPhone or iPod Touch.
	 */
	public static boolean detectIphoneOrIpod(String userAgent) {
		// We repeat the searches here because some iPods may report themselves as an
		// iPhone, which would be okay.
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPHONE) != -1
				|| userAgent.indexOf(AuthConstants.DEVICE_TYPE_IPOD) != -1;
	}

	/**
	 * Detects if the current device is an Android OS-based device.
	 */
	public static boolean detectAndroid(String userAgent) {
		return userAgent.indexOf(AuthConstants.DEVICE_TYPE_ANDRIOD) != -1;
	}
	
	
	/**
	 * get the ClientID from client Name
	 * @param clientName
	 * @return
	 */
	public static String getClientID(String clientName, RealmResource resource) {
		String clientId = resource.clients().findAll().stream().filter(cr -> cr.getClientId().equals(clientName))
				.findFirst().get().getId();
		return clientId;

	}
	
	/***
	 * get offline Sessions for user in Client
	 * @param clientName
	 * @param userId
	 * @param resource
	 * @return
	 */
	public static List<UserSessionRepresentation> getOfflineSessionForUserInClient(String clientName, String userId,
			RealmResource resource) {
		List<UserSessionRepresentation> offlineSession = resource.users().get(userId)
				.getOfflineSessions(getClientID(clientName, resource));
		return offlineSession;

	}
	
	public static boolean isAdminUser(String accessToken) {
		boolean isAdmin = false;
		String[] split_string = accessToken.split("\\.");
		String base64EncodedBody = split_string[1];
		Base64 base64 = new Base64(true);
		String body = new String(base64.decode(base64EncodedBody));
		JSONObject realm_resource = (JSONObject) (new JSONObject(body)).get(AuthConstants.REALM_ACCESS_RESOURCE);
		if (realm_resource != null && realm_resource.has(AuthConstants.REALM_MANAGEMENT_RESOURCE)) {
			if (realm_resource.getJSONObject(AuthConstants.REALM_MANAGEMENT_RESOURCE).has(AuthConstants.ROLES_RESOURCE)) {
				int count = realm_resource.getJSONObject(AuthConstants.REALM_MANAGEMENT_RESOURCE)
						.getJSONArray(AuthConstants.ROLES_RESOURCE).length();
				isAdmin = true;
			}
		}
		return isAdmin;
	}

	public static String checkRoleInRequest(HttpServletRequest request) {
		String roleName = "";
		if (request.getAttribute("userRole") != null) {
			roleName = request.getAttribute("userRole").toString();
			if (roleName.startsWith("[ROLE_")) {
				roleName = roleName.substring(1, roleName.length()-1).split(",")[0].trim().substring(5);
			}
		} else {
			throw new RoleDoesNotExistException("Role not available in request object");
		}
		return roleName;
	}


	/* Text encoding with algorithm passed in parameters
	 * @param: text
	 * @param: encodingTechnique algorithm to use for encoding
	 * @return: hashed_text
	 */
	public static String textEncoding(String text, String encodingTechnique) {
		String encodedText = text;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(encodingTechnique);
			byte[] messageDigestBytes = messageDigest.digest(text.getBytes());
			// Convert byte array into signum representation
			BigInteger signNum = new BigInteger(1, messageDigestBytes);
			// Convert message digest into hex value
			String hashtext = signNum.toString(16);
			// Add preceding 0s to make it 32 bit
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			encodedText = signNum.toString();
		} catch (NoSuchAlgorithmException ex){
			throw new InvalidAlgorithmException(INVALID_ENCODING_ALGORITHM.getErrorMessage());
		}
		return encodedText;
	}
	
	/* Text encoding with default algorithm
	 * @param: text
	 * @return: hashed_text
	 */
	public static String textEncoding(String text) {
		// If encoding algorithm is not provided then use default one.
		return textEncoding(text, AuthConstants.ENCODING_ALGORITHM);
	}

}
