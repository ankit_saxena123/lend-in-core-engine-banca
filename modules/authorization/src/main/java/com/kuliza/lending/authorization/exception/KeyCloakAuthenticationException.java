package com.kuliza.lending.authorization.exception;

import org.springframework.security.core.AuthenticationException;

public class KeyCloakAuthenticationException extends AuthenticationException {

	private static final long serialVersionUID = -9219191078339310616L;

	public KeyCloakAuthenticationException(String msg, Throwable t) {
		super(msg, t);
	}

	public KeyCloakAuthenticationException(String msg) {
		super(msg);
	}
}
