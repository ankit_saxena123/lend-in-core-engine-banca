package com.kuliza.lending.authorization.exception;

public enum ErrorMessages {

	COULD_NOT_GET_GROUPS("Error while getting gropus in realm "),
	COULD_NOT_CREATE_GROUP("Could not create user group in realm"),
	COULD_NOT_GET_RESPONSE_OR_INVALID("Error while getting token response : invalid credentials "),
	COULD_NOT_DELETE_USER_PROFILE("Could not delete user profile"), NO_RECORD_FOUND("No record found for provided id"),
	RECORD_ALREADY_EXISTS("Record already exists"),
	INTERNAL_SERVER_ERROR("Something went wrong. Please repeat this operation later."),
	COULD_NOT_LOGOUT_FROM_ALL_ACTIVE_SESSIONS("Error while logout user from all sessions"),
	COULD_NOT_CREATE_USER("Error while creating user "),
	COULD_NOT_GET_ROLES("Error while getting client level roles for user "),
	COULD_NOT_GET_USER_WITH_UID("Error while getting user for given ID: "),
	COULD_NOT_LOGOUT_USER("Error while user logout "), COULD_NOT_UPDATE_USER("Error while updating user "),
	ERROR_FINDING_USER_WITH_EMAIL("Error while serching user with emailId :"),
	COULD_NOT_GET_CLIENT_ROLES("Error while getting roles in client "),
	COULD_NOT_GET_USER_RESOURCE("Error while getting user resource "),
	COULD_NOT_GET_OFFLINE_SESSIONS("Error while getting offline sessions for user"),
	COULD_NOT_ADD_ROLE_TO_USER("Error while adding role to user "),
	COULD_NOT_GET_USER_ACTIVE_SESSIONS("Error while getting user's active sessions "),
	COULD_NOT_GET_GROUP_WITH_NAME("Error while getting group with given name: "),
	
	COULD_NOT_GET_USER_LIST("Error while getting user list"),
	INVALID_USER_CREDENTIALS("Invalid User credentials"),
	INVALID_ENCODING_ALGORITHM("Invalid Encoding Algorithm/Algorithm not supported");

	private String errorMessage;

	ErrorMessages(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
