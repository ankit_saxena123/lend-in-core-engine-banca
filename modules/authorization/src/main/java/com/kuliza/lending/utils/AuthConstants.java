package com.kuliza.lending.utils;

public final class AuthConstants {

  private AuthConstants() {
    throw new UnsupportedOperationException();
  }

  public static final int CREATED_STATUS_CODE = 201;

  public static final String DEFAULT_USER_ROLE = "user";

  public static final String DEFAULT_USER_GROUP = "employees";

  public static final String DEVICE_TYPE_AGENT = "User-Agent";

  // Initialize some initial smartphone string variables.
  public static final String DEVICE_TYPE_MOBILE = "Mobile";
  public static final String DEVICE_TYPE_ANDRIOD = "android";
  public static final String DEVICE_TYPE_IPHONE = "iphone";
  public static final String DEVICE_TYPE_IPOD = "ipod";

  // OS Detection
  public static final String OS_WINDOWS = "windows";

  // Constants for realm management
  public static final String REALM_MANAGEMENT_RESOURCE = "realm-management";
  public static final String REALM_ACCESS_RESOURCE = "resource_access";
  public static final String ROLES_RESOURCE = "roles";
  public static final String AUTH_REALM_ADMIN_USER = "realm-admin";

  // Constants for token details
  public static final String AUTH_GRANT_TYPE = "grant_type";
  public static final String AUTH_PASSWORD = "password";
  public static final String AUTH_USER_NAME = "username";
  public static final String AUTH_ACCESS_TOKEN = "access_token";
  public static final String AUTH_ACCESS_TOKEN_EXPIRES_IN = "expires_in";
  public static final String AUTH_REFRESH_TOKEN = "refresh_token";
  public static final String AUTH_REFRESH_TOKEN_EXPIRES_IN = "refresh_expires_in";
  public static final String AUTH_TOKEN_TYPE = "token_type";
  public static final String AUTH_ROLE = "role";
  public static final String AUTH_GROUPS = "groups";

  public static final String AUTH_CLIENT_ID = "client_id";
  public static final String AUTH_CLIENT_SECRET = "client_secret";

  public static final String AUTH_TOKEN = "token";

  public static final String AUTH_EMPTY_TOKEN_MESSAGE = "Token is empty";
  public static final String AUTH_INVALID_TOKEN_MESSAGE = "Invalid token";
  public static final String INVALID_REFRESH_TOKEN = "Invalid refresh token";
  public static final String AUTH_CREATED_SUCCESSFULLY = "Created Successfully";
  public static final String AUTH_ALREADY_EXISTING = "Already Existing";

  // Authorization API endpoints constants
  public static final String AUTH_BASE_URI = "/auth";
  public static final String AUTH_SIGNUP_ENDPOINT = AUTH_BASE_URI + "/signup";
  public static final String AUTH_LOGIN_ENDPOINT = AUTH_BASE_URI + "/login";
  public static final String AUTH_REFRESH_TOKEN_ENDPOINT = AUTH_BASE_URI + "/refresh-token";
  public static final String AUTH_LOGOUT_ENDPOINT = AUTH_BASE_URI + "/logout";
  public static final String AUTH_LIST_USERS_ENDPOINT = AUTH_BASE_URI + "/all-users";
  public static final String AUTH_LOGOUT_ACTIVE_SESSION_ENDPOINT =
      AUTH_BASE_URI + "/sessions/logout";
  public static final String AUTH_CREATE_ROLES_ENDPOINT = AUTH_BASE_URI + "/roles/add-new-role";
  public static final String AUTH_ADD_ROLES_TO_USER_ENDPOINT =
      AUTH_BASE_URI + "/users/roles/add-user-roles";
  public static final String AUTH_CREATE_GROUPS_ENDPOINT =
      AUTH_BASE_URI + "/groups/create-new-group";
  public static final String AUTH_LIST_GROUPS_ENDPOINT = AUTH_BASE_URI + "/groups/all-groups";
  public static final String AUTH_ENCODE_USER_PASSWORDS = AUTH_BASE_URI + "/encode-user-passwords";

  public static final String USERNAME = "userName";
  public static final String USER_ROLES = "userRole";
  public static final String USER_ID = "userId";

  public static final String SESSION = "SESSION";
  public static final String NO_SESSION = "NO_SESSION";

  public static final String ENCODING_ALGORITHM = "SHA-1";
  
  public static final String VIEW_PDF_NO_AUTH="/wf-user/view-pdf-no-auth";
  
  public static final String FEC_VTIGER="/fec-vtiger/**";
  public static final String INSURER_SCHEDULAR="/insurer/scheduler";

  public static final String EXPIRE_JOURNEY_SCHEDULAR="/journey/expire";
  public static final String IPAT_REPAY_SCHEDULAR="/ipat/repay"; 
  
  public static final String OFFLINE_POLICY_SAVE="/offline/saveOfflinePolicyDetails";
}
