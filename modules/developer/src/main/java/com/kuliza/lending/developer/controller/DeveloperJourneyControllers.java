package com.kuliza.lending.developer.controller;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.developer.service.DeveloperJourneyServices;

import java.util.Map;

@RestController
@RequestMapping("/lending/journey/dev/")
public class DeveloperJourneyControllers {

	@Autowired
	private DeveloperJourneyServices developerJourneyServices;

	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory.getLogger(DeveloperJourneyControllers.class);

	// API to get all process variables for a process instance
	@RequestMapping(method = RequestMethod.GET, value = "/processVariables/procInstId/{procInstId:^[1-9]+[0-9]*$}")
	public ResponseEntity<Object> getAllProcessVariables(@PathVariable(value = "procInstId") String procInstId) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(developerJourneyServices.getAllProcessVariables(procInstId));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/processVariables/{applicationNumber}/")
	public ApiResponse processVariablesForApplication(@PathVariable String applicationNumber){
		
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceBusinessKey(applicationNumber).includeProcessVariables().singleResult();
		Map<String, Object> processVariables = processInstance.getProcessVariables();
		return new ApiResponse(HttpStatus.OK, "success", processVariables);
	}

	// API to get all process variables for a User Id
	@RequestMapping(method = RequestMethod.GET, value = "/processVariables/userId/{userId}")
	public ResponseEntity<Object> getAllProcessVariablesUsingMobileNumber(
			@PathVariable(value = "userId") String userId) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(developerJourneyServices.getAllProcessVariablesFromUserId(userId));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	// API to get kill all process active processInstances
	@RequestMapping(method = RequestMethod.GET, value = "/kill-all")
	public ResponseEntity<Object> killAll(
			@RequestParam(required = false, value = "procDefinitionKey") String processDefinitionKey) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(developerJourneyServices.killAllProcessInstances(processDefinitionKey));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

	// API to get kill single active processInstance
	@RequestMapping(method = RequestMethod.GET, value = "/kill-one")
	public ResponseEntity<Object> killSingle(
			@RequestParam(required = true, value = "procInstId") String processInstanceId) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(developerJourneyServices.killSingleProcessInstance(processInstanceId));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE));
		}
	}

}
