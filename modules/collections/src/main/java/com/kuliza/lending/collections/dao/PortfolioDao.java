package com.kuliza.lending.collections.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.collections.model.PortfolioModel;

@Repository
public interface PortfolioDao extends CrudRepository<PortfolioModel, Long> {

	public PortfolioModel findById(long id);

	public PortfolioModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public PortfolioModel findByCustomerIdAndIsActiveAndIsDeleted(String customerId, boolean isActive, boolean isDeleted);

}
