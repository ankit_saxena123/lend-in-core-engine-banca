package com.kuliza.lending.collections.services;

import com.kuliza.lending.collections.utils.CollectionConstants;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.portal.PortalJourneyServiceTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.CaseInstanceQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ParentToChildServiceTask")
public class ParentToChildServiceTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParentToChildServiceTask.class);

	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;

	public void setChildCaseVariables(DelegatePlanItemInstance loanCaseInstance, String roleKey, String roleBasedSuffixes, String caseVariables)
			throws IOException, InterruptedException, Exception {
		try {
			String portfolioId = CommonHelperFunctions.getStringValue(loanCaseInstance.getVariable("portfolioId"));
			if (portfolioId != null && !portfolioId.equals("")) {
				LOGGER.debug("Parent/Portfolio case id : " + portfolioId);
				CaseInstance portfolioCaseInstance = cmmnRuntimeService.createCaseInstanceQuery()
						.caseInstanceId(portfolioId).includeCaseVariables().singleResult();
				System.out.println(portfolioCaseInstance.getCaseVariables());
				Map<String, Object> caseVariablesToSet = new HashMap<String, Object>();
				if (caseVariables != null && !caseVariables.equals("")) {
					for (String caseVariable : caseVariables.split(",")) {
						caseVariablesToSet.put(caseVariable.trim(),
								portfolioCaseInstance.getCaseVariables().get(caseVariable.trim()));
					}
				}
				if (roleBasedSuffixes != null && !roleBasedSuffixes.equals("")) {
					String role = null;
					if (roleKey != null && !roleKey.equals("")) {
						role = CommonHelperFunctions
								.getStringValue(portfolioCaseInstance.getCaseVariables().get(roleKey.trim()));
					} else {
						throw new InterruptedException(
								"Error in setting proc variables from parent to child: roleKey(Parameter 2) should not be null or empty.");
					}
					if (role != null) {
						for (String roleSuffix : roleBasedSuffixes.split(",")) {
							caseVariablesToSet.put(role + roleSuffix.trim(),
									portfolioCaseInstance.getCaseVariables().get(role + roleSuffix.trim()));
						}
					} else {
						throw new InterruptedException(
								"Error in setting proc variables from parent to child: role is null in given role key.");
					}
				}
				LOGGER.debug("Case variables to set : " + caseVariablesToSet);
				cmmnRuntimeService.setVariables(loanCaseInstance.getCaseInstanceId(),
						caseVariablesToSet != null && !caseVariablesToSet.isEmpty() ? caseVariablesToSet
								: portfolioCaseInstance.getCaseVariables());
			} else {
				throw new InterruptedException(
						"Error in setting proc variables from parent to child: parent id is null or emplty for caseid: "
								+ loanCaseInstance.getCaseInstanceId());
			}
		} catch (Exception e) {
			LOGGER.debug("In catch : " + e.getMessage());
			throw new InterruptedException("Error in setting case variables in child from parent: " + e.getMessage());
		}
	}

	public void copyDataToChildren(DelegatePlanItemInstance portfolioCaseInstance, String roleKey,
			String roleBasedSuffixes, String variablesToSet) throws IOException, InterruptedException {
		Map<String, Object> portfolioVariables = portfolioCaseInstance.getVariables();
		Map<String, Object> variableToSave = new HashMap<String, Object>();
		List<CaseInstance> loanCaseInstances = cmmnRuntimeService.createCaseInstanceQuery()
				.caseInstanceParentId(portfolioCaseInstance.getCaseInstanceId()).list();
		try {
			if (roleBasedSuffixes != null && !roleBasedSuffixes.equals("")) {
				String role = null;
				if (roleKey != null && !roleKey.equals("")) {
					role = CommonHelperFunctions.getStringValue(portfolioVariables.get(roleKey.trim()));
				} else {
					throw new InterruptedException(
							"Error in setting proc variables from parent to child: roleKey(Parameter 2) should not be null or empty.");
				}
				if (role != null) {
					for (String roleSuffix : roleBasedSuffixes.split(",")) {
						variableToSave.put(role + roleSuffix.trim(), portfolioVariables.get(role + roleSuffix.trim()));
					}
				} else {
					throw new InterruptedException(
							"Error in setting proc variables from parent to child: role is null in given role key.");
				}
			}
			if (variablesToSet != null && !"".equals(variablesToSet)) {
				for (String variableKey : variablesToSet.split(",")) {
					variableToSave.put(variableKey.trim(), portfolioVariables.get(variableKey.trim()));
				}
			}
			for (CaseInstance loanCaseInstance : loanCaseInstances) {
				LOGGER.debug("Settng case variables for child : " + loanCaseInstance.getId());
				cmmnRuntimeService.setVariables(loanCaseInstance.getId(),
						variableToSave != null && !variableToSave.isEmpty() ? variableToSave : portfolioVariables);
			}
		} catch (Exception e) {
			LOGGER.debug("In catch : " + e.getMessage());
			throw new InterruptedException("Error in setting case variables from parent to all children: " + e.getMessage());
		}
	}
}
