package com.kuliza.lending.collections;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.*;

@SpringBootApplication
@ComponentScan("com.kuliza.lending")
@EnableConfigurationProperties(LiquibaseProperties.class)
public class CollectionApplication extends SpringBootServletInitializer
{
    @Autowired
    private LiquibaseProperties properties;
    @Autowired
    private DataSource dataSource;
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CollectionApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CollectionApplication.class, args);
    }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(this.properties.getChangeLog());
        liquibase.setContexts(this.properties.getContexts());
        liquibase.setDefaultSchema(this.properties.getDefaultSchema());
        liquibase.setDropFirst(this.properties.isDropFirst());
        liquibase.setShouldRun(this.properties.isEnabled());
        liquibase.setLabels(this.properties.getLabels());
        liquibase.setChangeLogParameters(this.properties.getParameters());
        liquibase.setRollbackFile(this.properties.getRollbackFile());
        liquibase.setDatabaseChangeLogLockTable("LOS_DATABASECHANGELOGLOCK");
        liquibase.setDatabaseChangeLogTable("LOS_DATABASECHANGELOG");
        return liquibase;
    }
}
