package com.kuliza.lending.collections.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.kuliza.lending.collections.utils.CollectionConstants;

import java.util.List;
import java.util.Map;

/*
    * This is the model for accepting input for the creation/updation of a decision table
    * The user can create a new decision table(version 1), update the current version or add a new version of
      the existing decision table.
*/
public class Portfolio {

	@NotNull(message = "Customer Id cannot be null")
	private String customerId;

	@NotNull(message = "Asset Type cannot be null")
	private String assetType;

	@Size(min = 1)
	private String customerType;
	
	private Map<String, Object> variablesToSave;

	@NotNull(message = "Loan Applications Cannot be null")
	@Size(min = CollectionConstants.LIST_MIN_SIZE)
	List<LoanApplication> loanApplications;

	public Portfolio() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Portfolio(String customerId, String assetType, String customerType, List<LoanApplication> loanApplications, Map<String, Object> variablesToSave) {
		super();
		this.customerId = customerId;
		this.assetType = assetType;
		this.customerType = customerType;
		this.loanApplications = loanApplications;
		this.variablesToSave = variablesToSave;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public List<LoanApplication> getLoanApplications() {
		return loanApplications;
	}

	public void setLoanApplications(List<LoanApplication> loanApplications) {
		this.loanApplications = loanApplications;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public Map<String, Object> getVariablesToSave() {
		return variablesToSave;
	}

	public void setVariablesToSave(Map<String, Object> variablesToSave) {
		this.variablesToSave = variablesToSave;
	}

}
