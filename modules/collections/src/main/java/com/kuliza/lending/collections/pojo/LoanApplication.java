package com.kuliza.lending.collections.pojo;

import com.kuliza.lending.collections.utils.CollectionConstants;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Map;

/*
    * This is the model for accepting input for the creation/updation of a decision table
    * The user can create a new decision table(version 1), update the current version or add a new version of
      the existing decision table.
*/
public class LoanApplication {

	@NotNull(message = "Loan Application Id cannot be null")
	public String loanApplicationId;

	@NotNull(message = "DPD cannot be null")
	public int dpd;

	@NotNull(message = "Due Amount cannot be null")
	@Min(CollectionConstants.AMOUNT_MIN_VALUE)
	public long dueAmount;

	@NotNull(message = "Outstanding cannot be null")
	@Min(CollectionConstants.AMOUNT_MIN_VALUE)
	public long outstandingAmount;

	@NotNull(message = "Disbursed amount cannot be null")
	@Min(CollectionConstants.AMOUNT_MIN_VALUE)
	public long disbursedAmount;

	@NotNull(message = "Current EMI cannot be null")
	@Min(CollectionConstants.AMOUNT_MIN_VALUE)
	public long currentEmi;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public Date dueDate;

	public String journeyType;
	
	public Map<String, Object> variablesToSave;

	public LoanApplication() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoanApplication(String loanApplicationId, int dpd, long dueAmount, long outstandingAmount,
			long disbursedAmount, long currentEmi, Date dueDate, String journeyType, Map<String, Object> variablesToSave) {
		super();
		this.loanApplicationId = loanApplicationId;
		this.dpd = dpd;
		this.dueAmount = dueAmount;
		this.outstandingAmount = outstandingAmount;
		this.disbursedAmount = disbursedAmount;
		this.currentEmi = currentEmi;
		this.dueDate = dueDate;
		this.journeyType = journeyType;
		this.variablesToSave = variablesToSave;
	}

	public String getLoanApplicationId() {
		return loanApplicationId;
	}

	public void setLoanApplicationId(String loanApplicationId) {
		this.loanApplicationId = loanApplicationId;
	}

	public int getDpd() {
		return dpd;
	}

	public void setDpd(int dpd) {
		this.dpd = dpd;
	}

	public long getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(long dueAmount) {
		this.dueAmount = dueAmount;
	}

	public long getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(long outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public long getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(long disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public long getCurrentEmi() {
		return currentEmi;
	}

	public void setCurrentEmi(long currentEmi) {
		this.currentEmi = currentEmi;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	public Map<String, Object> getVariablesToSave() {
		return variablesToSave;
	}

	public void setVariablesToSave(Map<String, Object> variablesToSave) {
		this.variablesToSave = variablesToSave;
	}
	
	

}
