package com.kuliza.lending.collections.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.kuliza.lending.common.model.BaseModel;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "collection_portfolio")
public class PortfolioModel extends BaseModel {

	@Column(nullable = false)
	private String customerId;

	@Column(nullable = false)
	private String assetType;

	@Column(nullable = true)
	private String customerType;

	@Column(nullable = true, unique = true)
	private String caseModelId;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(nullable=false)
	@ColumnDefault("1")
	private Boolean isActive = true;

	@Column(nullable = true)
	private String recoveryStatus;

	@Column(nullable = false, columnDefinition = "int default 0")
	private int dpd;

	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long dueAmount;

	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long outstandingAmount;

	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long disbursedAmount;

	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long currentEmi;

	public PortfolioModel() {
		super();
		this.setIsDeleted(false);
		this.setActive(true);
		// TODO Auto-generated constructor stub
	}

	public PortfolioModel(String customerId, String assetType, String customerType, String caseModelId,
			String recoveryStatus, int dpd, long dueAmount, long outstandingAmount,
			long disbursedAmount, long currentEmi) {
		super();
		this.customerId = customerId;
		this.assetType = assetType;
		this.customerType = customerType;
		this.caseModelId = caseModelId;
		this.recoveryStatus = recoveryStatus;
		this.dpd = dpd;
		this.dueAmount = dueAmount;
		this.outstandingAmount = outstandingAmount;
		this.disbursedAmount = disbursedAmount;
		this.currentEmi = currentEmi;
		this.setIsDeleted(false);
		this.setActive(true);
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getCaseModelId() {
		return caseModelId;
	}

	public void setCaseModelId(String caseModelId) {
		this.caseModelId = caseModelId;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getRecoveryStatus() {
		return recoveryStatus;
	}

	public void setRecoveryStatus(String recoveryStatus) {
		this.recoveryStatus = recoveryStatus;
	}

	public int getDpd() {
		return dpd;
	}

	public void setDpd(int dpd) {
		this.dpd = dpd;
	}

	public long getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(long dueAmount) {
		this.dueAmount = dueAmount;
	}

	public long getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(long outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public long getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(long disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public long getCurrentEmi() {
		return currentEmi;
	}

	public void setCurrentEmi(long currentEmi) {
		this.currentEmi = currentEmi;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public void setAggregatedFields(int dpd, long dueAmount, long outstandingAmount, long disbursedAmount,
			long currentEmi) {
		this.dpd = dpd;
		this.dueAmount = dueAmount;
		this.outstandingAmount = outstandingAmount;
		this.disbursedAmount = disbursedAmount;
		this.currentEmi = currentEmi;

	}

}
