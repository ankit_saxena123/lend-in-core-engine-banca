package com.kuliza.lending.collections.services;

import com.kuliza.lending.collections.dao.LoanApplicationDao;
import com.kuliza.lending.collections.dao.PortfolioDao;
import com.kuliza.lending.collections.model.PortfolioModel;
import com.kuliza.lending.collections.pojo.LoanApplication;
import com.kuliza.lending.collections.pojo.Portfolio;
import com.kuliza.lending.collections.utils.CollectionConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.portal.pojo.InitiatePortalData;
import com.kuliza.lending.portal.service.CMMNService;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CollectionEngineService {

	@Autowired
	private PortfolioDao portfolioDao;

	@Autowired
	private LoanApplicationDao loanApplicationDao;

	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	private CMMNService cmmnService;
	

	// This is function to on board Portfolio.
	public ApiResponse onboardPortfolio(Portfolio portfolio) throws Exception {
		ApiResponse apiResponse;
		try {
			Map<String, Object> aggregatedFieldMap = null;
			List<LoanApplication> loanApplications = portfolio.getLoanApplications();
			Map<String, Object> portfolioProcessVariables = new HashMap<String, Object>();
			if (portfolio.getVariablesToSave() != null && !portfolio.getVariablesToSave().isEmpty())
				portfolioProcessVariables.putAll(portfolio.getVariablesToSave());
			portfolioProcessVariables.put("loanCount", loanApplications.size());
			portfolioProcessVariables.put("customerId", portfolio.getCustomerId());
			portfolioProcessVariables.put("assetType", portfolio.getAssetType());
			portfolioProcessVariables.put("customerType", portfolio.getCustomerType());
			aggregatedFieldMap = aggregationLogic(loanApplications);
			portfolioProcessVariables.putAll(aggregatedFieldMap);
			for (LoanApplication loan : loanApplications) {
				Map<String, Object> loanCaseVariables = new HashMap<String, Object>();
				System.out.println(loan.getVariablesToSave());
				if (loan.getVariablesToSave() != null && !loan.getVariablesToSave().isEmpty())
					loanCaseVariables.putAll(loan.getVariablesToSave());
				
				loanCaseVariables.put("customerId", portfolio.getCustomerId());
				loanCaseVariables.put("portalPortfolioKey", "customerId");
				InitiatePortalData initiatePortalData = new InitiatePortalData();
				initiatePortalData.setPortalType(CollectionConstants.COLLECTION_PORTAL_TYPE);
				initiatePortalData.setPortfolioVariablesToSave(portfolioProcessVariables);
				initiatePortalData.setVariablesToSave(loanCaseVariables);
				ApiResponse initiateCase = cmmnService.startOrResumeCase(CollectionConstants.COLLECTION_CMMN_CASE_NAME,
						CommonHelperFunctions.getStringValue(loan.getLoanApplicationId()), null, initiatePortalData, true);
				
				if (initiateCase.getData() != null) {
					HashMap<String, Object> initiateCaseData = (HashMap<String, Object>) initiateCase.getData();
					if (initiateCaseData.get("caseInstanceId") == null) {
						throw new Exception("Error in initiate");
					}
				} else {
					throw new Exception("Error in initiate");
				}
			}
			apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in initiate");
		}
		return apiResponse;
	}

	// This is function to save loan details and give portfolio level aggregation
	// fields.
	public Map<String, Object> aggregationLogic(List<LoanApplication> loanApplications)
			throws Exception {
		Map<String, Object> aggregatedFieldMap = new HashMap<String, Object>();
		for (LoanApplication loan : loanApplications) {
			Map<String, Object> loanCaseVariables = new HashMap<String, Object>();
			Field[] fields = loan.getClass().getDeclaredFields();
			loanCaseVariables.put("loanApplicationId", loan.getLoanApplicationId());
			loanCaseVariables.put("journeyType", loan.getJourneyType());
			for (Field field : fields) {
				if (CollectionConstants.aggregationConfig.containsKey(field.getName())) {
					loanCaseVariables.put(field.getName(), field.get(loan));
					Method method = null;
					if (field.get(loan) instanceof Long) {
						method = field.get(loan).getClass().getMethod(
								CollectionConstants.aggregationConfig.get(field.getName()), long.class, long.class);
					} else {
						method = field.get(loan).getClass().getMethod(
								CollectionConstants.aggregationConfig.get(field.getName()), int.class, int.class);

					}
					aggregatedFieldMap.put(field.getName(), method.invoke(Long.class,
							aggregatedFieldMap.getOrDefault(field.getName(), 0), field.get(loan)));
				}
			}
			loan.getVariablesToSave().putAll(loanCaseVariables);
		}
		return aggregatedFieldMap;
	}
}
