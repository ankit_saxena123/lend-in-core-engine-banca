package com.kuliza.lending.collections.controllers;

import java.util.Map;

import javax.validation.Valid;

import com.kuliza.lending.collections.utils.CollectionConstants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.collections.pojo.Portfolio;
import com.kuliza.lending.collections.pojo.PortfolioList;
import com.kuliza.lending.collections.services.CollectionEngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(CollectionConstants.COLLECTION_ENGINE_URL)
public class CollectionEngineController {

	@Autowired
	private CollectionEngineService collectionEngineService;

	@Autowired
	private ObjectMapper objectMapper;

	@PostMapping(CollectionConstants.ONBOARD_PORTFOLIO_URL)
	public ResponseEntity<Object> onboardPortfolio(@Valid @RequestBody Portfolio request) throws Exception {
		return CommonHelperFunctions
				.buildResponseEntity(collectionEngineService.onboardPortfolio(request));

	}
	
	@PostMapping(CollectionConstants.ONBOARD_BULK_PORTFOLIO_URL)
	public ResponseEntity<Object> onboardBulkPortfolio(@Valid @RequestBody PortfolioList portfolioList) throws Exception {
		for (Portfolio portfolio : portfolioList.getPortfolios()) {
			collectionEngineService.onboardPortfolio(portfolio);
		}
		return CommonHelperFunctions
				.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE));

	}

}
