package com.kuliza.lending.collections.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "collection_loan_application")
public class LoanApplicationModel extends BaseModel {

	@Column(nullable = false)
	private String loanApplicationId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "portfoloId", nullable = false)
	private PortfolioModel portfolioModel;

	@Column(nullable = false)
	private int dpd;

	@Column(nullable = true)
	private String loanType;

	@Column(nullable = false)
	@Type(type = "date")
	private Date dueDate;

	@Column(nullable = false)
	private long dueAmount;

	@Column(nullable = false)
	private long outstandingAmount;

	@Column(nullable = false)
	private long disbursedAmount;

	@Column(nullable = false)
	private long currentEmi;

	@Column(nullable = true)
	private String disbursmentType;

	@Column(nullable = false)
	private String currency;

	@Column(nullable = true)
	private int tenure;

	public LoanApplicationModel() {
		super();
		this.setIsDeleted(false);
		// TODO Auto-generated constructor stub
	}

	public LoanApplicationModel(String loanApplicationId, PortfolioModel portfolioModel, int dpd, String loanType,
			Date dueDate, long dueAmount, long outstandingAmount, long disbursedAmount, long currentEmi,
			String disbursmentType, String currency, int tenure) {
		super();
		this.loanApplicationId = loanApplicationId;
		this.portfolioModel = portfolioModel;
		this.dpd = dpd;
		this.loanType = loanType;
		this.dueDate = dueDate;
		this.dueAmount = dueAmount;
		this.outstandingAmount = outstandingAmount;
		this.disbursedAmount = disbursedAmount;
		this.currentEmi = currentEmi;
		this.disbursmentType = disbursmentType;
		this.currency = currency;
		this.tenure = tenure;
		this.setIsDeleted(false);
	}

	public String getLoanApplictaionId() {
		return loanApplicationId;
	}

	public void setLoanApplicationId(String loanApplicationId) {
		this.loanApplicationId = loanApplicationId;
	}

	public PortfolioModel getPortfolioModel() {
		return portfolioModel;
	}

	public void setPortfolioModel(PortfolioModel portfolioModel) {
		this.portfolioModel = portfolioModel;
	}

	public int getDpd() {
		return dpd;
	}

	public void setDpd(int dpd) {
		this.dpd = dpd;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public long getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(long dueAmount) {
		this.dueAmount = dueAmount;
	}

	public long getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(long outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public long getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(long disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public long getCurrentEmi() {
		return currentEmi;
	}

	public void setCurrentEmi(long currentEmi) {
		this.currentEmi = currentEmi;
	}

	public String getDisbursmentType() {
		return disbursmentType;
	}

	public void setDisbursmentType(String disbursmentType) {
		this.disbursmentType = disbursmentType;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	public void setLoanDetails(int dpd, String loanType, Date dueDate, long dueAmount, long outstandingAmount,
			long disbursedAmount, long currentEmi, String disbursmentType, String currency, int tenure) {
		this.dpd = dpd;
		this.loanType = loanType;
		this.dueDate = dueDate;
		this.dueAmount = dueAmount;
		this.outstandingAmount = outstandingAmount;
		this.disbursedAmount = disbursedAmount;
		this.currentEmi = currentEmi;
		this.disbursmentType = disbursmentType;
		this.currency = currency;
		this.tenure = tenure;
	}
}
