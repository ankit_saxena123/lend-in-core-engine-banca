package com.kuliza.lending.notifications.jms;

import java.util.HashMap;
import java.util.Map;

import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.dmn.api.DmnRuleService;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.decision_table.controllers.DecisionTableController;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.utils.NotificationConstants;

@Service
// @ConditionalOnExpression("${broker.name.rabbit:true}")
public class NotificationMessageProducerJMS {

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	private DecisionTableService dmnService;

	@Autowired
	CmmnRuntimeService cmmnRuntimeService;

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationMessageProducerJMS.class);

	public Boolean createNotificationData(String caseId, String decisionKey)
			throws JsonProcessingException, JSONException {
		try {
			Map<String, Object> caseVariables = cmmnRuntimeService.getVariables(caseId);

//			Map<String, Object> notificationConfig = dmnService.executeDecisionTable(decisionKey, caseVariables);
			Map<String, Object> notificationConfig = new HashMap<String, Object>();
//			notificationConfig.put("emailTemplate", 32);
//			notificationConfig.put("emailFrequency", 11);
			notificationConfig.put("emailTemplate", 32);
			notificationConfig.put("emailFrequency", 4);
			if (notificationConfig != null) {
				LOGGER.info("In create notification data received");
				NotificationData notificationData = new NotificationData(caseId, notificationConfig, caseVariables);
				jmsTemplate.convertAndSend(NotificationConstants.JMS_NOTIFICATION_ROUTING_KEY, notificationData);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
	}

}
