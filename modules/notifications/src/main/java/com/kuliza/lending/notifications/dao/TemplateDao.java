package com.kuliza.lending.notifications.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.TemplateModel;

@Transactional
public interface TemplateDao extends CrudRepository<TemplateModel, Long> {

	public TemplateModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<TemplateModel> findByNotificationChannelModelAndIsDeleted(NotificationChannelModel notificationChannelModel, boolean isDeleted);

}
