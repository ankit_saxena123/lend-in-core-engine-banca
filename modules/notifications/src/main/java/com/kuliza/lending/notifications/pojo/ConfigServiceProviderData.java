package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;

public class ConfigServiceProviderData {

	@NotNull(message = "Provider Id cannot be null")
	public long providerId;
	
	@NotNull(message = "Number of retries cannot be null")
	public int retries;

	public ConfigServiceProviderData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigServiceProviderData(long providerId, int retries) {
		super();
		this.providerId = providerId;
		this.retries = retries;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}
	
}
