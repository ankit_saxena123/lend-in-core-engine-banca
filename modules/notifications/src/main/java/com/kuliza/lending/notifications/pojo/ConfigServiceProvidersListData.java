package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;

public class ConfigServiceProvidersListData {

	@NotNull(message = "Service Providers list cannot be null")
	@Size(min = 1)
	List<ConfigServiceProviderData> serviceProviders;

	public ConfigServiceProvidersListData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigServiceProvidersListData(List<ConfigServiceProviderData> serviceProviders) {
		super();
		this.serviceProviders = serviceProviders;
	}

	public List<ConfigServiceProviderData> getServiceProviders() {
		return serviceProviders;
	}

	public void setServiceProviders(List<ConfigServiceProviderData> serviceProviders) {
		this.serviceProviders = serviceProviders;
	}

}
