package com.kuliza.lending.notifications.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.decision_table.models.DecisionTableModel;
import com.kuliza.lending.notifications.dao.NotificationModuleDao;
import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.models.NotificationRangesModel;
import com.kuliza.lending.notifications.models.NotificationVariablesModel;
import com.kuliza.lending.notifications.models.ServiceProviderModel;
import com.kuliza.lending.notifications.pojo.ConfigChannelListData;
import com.kuliza.lending.notifications.pojo.ConfigModuleRequestData;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderInputData;
import com.kuliza.lending.notifications.pojo.DraftOrPublishNotificationTable;
import com.kuliza.lending.notifications.pojo.NotificationTableRequestData;
import com.kuliza.lending.notifications.pojo.RangesListRequestData;
import com.kuliza.lending.notifications.pojo.VariableListRequestData;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;
import com.kuliza.lending.notifications.utils.NotificationConstants.VariableTypes;

public class NotificationTestConstants {
	private NotificationTestConstants() {
		throw new UnsupportedOperationException();
	}

	public ObjectMapper objectMapper = new ObjectMapper();
	// JMS Configuration
	public static final String ASSIGNEE = "assignee";

	public static final boolean IS_DELETED = false;

//	Request JSONs
	
	public static final String moduleJson = "{ \"groupName\": \"Post-Due Notifications 2\", \"description\": \"DMN for post-due notification  222222 1.\", \"system\":\"COLLECTIONS\"}";

	public static final String moduleEditJson = "{ \"groupName\": \"Post-Due Notifications 2\", \"description\": \"DMN for post-due notification  222222 1.\", \"system\":\"COLLECTIONS\", \"id\": 1 }";
	
	public static final String serviceProvidorJson = "{\n" + 
			"	\"name\": \"SMS Integration\",\n" + 
			"	\"ibEndpoint\": \"/ib/valueFirst/sms\"\n" + 
			"}";
	
	public static final String serviceProvidorEditJson = "{\n" + 
			"	\"name\": \"SMS Integration\",\n" + 
			"	\"ibEndpoint\": \"/ib/valueFirst/sms\",\n" + 
			"	\"id\": 1\n" + 
			"}";
	
	public static final String channelsJson = "{\n" + 
			"	\"moduleId\" : 1,\n" + 
			"	\"channels\" :  [\n" + 
			"		{\n" + 
			"			\"startTime\" : \"2011-11-02T08:00:00.000 +0530\",\n" + 
			"			\"endTime\" : \"2011-11-02T18:00:00.000 +0530\",\n" + 
			"			\"channelType\" : \"SMS\",\n" + 
			"			\"serviceProviders\" : [\n" + 
			"						{	\"providerId\" : 1,\n" + 
			"							\"retries\": 2\n" + 
			"						},\n" + 
			"						{	\"providerId\" : 2,\n" + 
			"							\"retries\": 3\n" + 
			"						}\n" + 
			"				]\n" + 
			"		},\n" + 
			"		{\n" + 
			"			\"startTime\" : \"2011-11-02T07:00:00.000 +0530\",\n" + 
			"			\"endTime\" : \"2011-11-02T20:00:00.000 +0530\",\n" + 
			"			\"channelType\" : \"EMAIL\",\n" + 
			"			\"serviceProviders\" : [\n" + 
			"						{	\"providerId\" : 1,\n" + 
			"							\"retries\": 5\n" + 
			"						},\n" + 
			"						{	\"providerId\" : 1,\n" + 
			"							\"retries\": 2\n" + 
			"						}\n" + 
			"			]\n" + 
			"		}\n" + 
			"	]\n" + 
			"}";
	
	public static final String channelsEditJson = "{\n" + 
			"	\"moduleId\" : 1,\n" + 
			"	\"channels\" :  [\n" + 
			"		{\n" + 
			"			\"id\": 1,\n" + 
			"			\"startTime\" : \"2011-11-02T07:00:00.000 +0530\",\n" + 
			"			\"endTime\" : \"2011-11-02T20:00:00.000 +0530\",\n" + 
			"			\"channelType\" : \"EMAIL\",\n" + 
			"			\"serviceProviders\" : [\n" + 
			"					{	\"providerId\" : 1,\n" + 
			"						\"retries\": 2\n" + 
			"					},\n" + 
			"					{	\"providerId\" : 2,\n" + 
			"						\"retries\": 5\n" + 
			"					}\n" + 
			"			]\n" + 
			"			\n" + 
			"		}\n" + 
			"	]\n" + 
			"}";
	
	public static final String variablesJson = "{\n" + 
			"	\"moduleId\": 1,\n" + 
			"	\"variables\" : [\n" + 
			"		{\n" + 
			"			\"key\":\"loanAmount\",\n" + 
			"			\"label\": \"Loan Amount\",\n" + 
			"			\"system\": \"LMS\",\n" + 
			"			\"description\": \"EMI amount field\",\n" + 
			"			\"variableType\":\"STRING\"\n" + 
			"		}, \n" + 
			"		{\n" + 
			"			\"key\":\"dpd\",\n" + 
			"			\"label\": \"DPD\",\n" + 
			"			\"system\": \"COLLECTIONS\",\n" + 
			"			\"description\": \"DPD field\",\n" + 
			"			\"variableType\":\"NUMBER\"\n" + 
			"		}\n" + 
			"	]\n" + 
			"}";
	
	
	public static final String variablesEditJson = "{\n" + 
			"	\"moduleId\":1,\n" + 
			"	\"variables\" : [{\n" + 
			"		\"id\": 1,\n" + 
			"		\"key\":\"loanAmount\",\n" + 
			"		\"label\": \"Loan Amount\",\n" + 
			"		\"system\": \"LMS\",\n" + 
			"		\"description\": \"EMI amount field\",\n" + 
			"		\"variableType\":\"STRING\"\n" + 
			"	},\n" + 
			"	{\n" + 
			"		\"id\": 1,\n" + 
			"		\"key\":\"dpd\",\n" + 
			"		\"label\": \"Dpd\",\n" + 
			"		\"system\": \"COLLECTIONS\",\n" + 
			"		\"description\": \"DPD field\",\n" + 
			"		\"variableType\":\"NUMBER\"\n" + 
			"	}\n" + 
			"	]\n" + 
			"}";
	
	public static final String rangesJson = "{\n" + 
			"	\"moduleId\": 1,\n" + 
			"	\"ranges\": [\n" + 
			"			{   \n" + 
			"				\"variableId\": 1,\n" + 
			"				\"minValue\": -100,\n" + 
			"				\"maxValue\": 10000,\n" + 
			"				\"values\": [\"-100 0\", \"1 5000\", \"5001 10000\"]\n" + 
			"			},\n" + 
			"			{   \n" + 
			"				\"variableId\": 1,\n" + 
			"				\"minValue\": -100,\n" + 
			"				\"maxValue\": 10000,\n" + 
			"				\"values\": [\"-100 0\", \"1 5000\", \"5001 10000\"]\n" + 
			"			}\n" + 
			"			\n" + 
			"		]\n" + 
			"}";
	
	public static final String rangesEditJson = "{\n" + 
			"	\"moduleId\": 1,\n" + 
			"	\"ranges\": [\n" + 
			"			{   \"id\": 1,\n" + 
			"				\"variableId\": 1,\n" + 
			"				\"minValue\": 0,\n" + 
			"				\"maxValue\": 90,\n" + 
			"				\"values\": [\"0 30\", \"31 60\", \"61 90\"]\n" + 
			"			},\n" + 
			"				{   \"id\": 1,\n" + 
			"				\"variableId\": 1,\n" + 
			"				\"minValue\": 0,\n" + 
			"				\"maxValue\": 90,\n" + 
			"				\"values\": [\"0 30\", \"31 60\", \"61 90\"]\n" + 
			"			}\n" + 
			"		]\n" + 
			"}";
	
	public static final String notificationTableJson = "{\n" + 
			"	\"moduleId\": 1,\n" + 
			"	\"decisionTable\": {\n" + 
			"		\"name\": \"Post-Due Notification\",\n" + 
			"		\"key\": \"Post-Due Notification\",\n" + 
			"		\"description\": \"Post-Due Notification table for dpd  > 0\",\n" + 
			"		\"newVersion\": false,\n" + 
			"		\"decisionTableDefinition\": {\n" + 
			"    \"modelVersion\": \"2\",\n" + 
			"    \"description\": \"abcd\",\n" + 
			"    \"hitIndicator\": \"FIRST\",\n" + 
			"    \"inputExpressions\": [\n" + 
			"      {\n" + 
			"        \"id\": \"1\",\n" + 
			"        \"variableId\": \"input2\",\n" + 
			"        \"variableType\": null,\n" + 
			"        \"type\": \"number\",\n" + 
			"        \"label\": \"Input 2\",\n" + 
			"        \"entries\": [],\n" + 
			"        \"newVariable\": false,\n" + 
			"        \"complexExpression\": false\n" + 
			"      },\n" + 
			"      {\n" + 
			"        \"id\": \"3\",\n" + 
			"        \"variableId\": \"input1\",\n" + 
			"        \"variableType\": null,\n" + 
			"        \"type\": \"string\",\n" + 
			"        \"label\": \"Input 1\",\n" + 
			"        \"entries\": [],\n" + 
			"        \"newVariable\": false,\n" + 
			"        \"complexExpression\": false\n" + 
			"      }\n" + 
			"    ],\n" + 
			"    \"outputExpressions\": [\n" + 
			"      {\n" + 
			"        \"id\": \"2\",\n" + 
			"        \"variableId\": \"output1\",\n" + 
			"        \"variableType\": null,\n" + 
			"        \"type\": \"string\",\n" + 
			"        \"label\": \"Output 1\",\n" + 
			"        \"entries\": [],\n" + 
			"        \"newVariable\": false,\n" + 
			"        \"complexExpression\": false\n" + 
			"      },\n" + 
			"      {\n" + 
			"        \"id\": \"4\",\n" + 
			"        \"variableId\": \"output2\",\n" + 
			"        \"variableType\": null,\n" + 
			"        \"type\": \"string\",\n" + 
			"        \"label\": \"Output 2\",\n" + 
			"        \"entries\": [\n" + 
			"          \"2\"\n" + 
			"        ],\n" + 
			"        \"newVariable\": false,\n" + 
			"        \"complexExpression\": false\n" + 
			"      }\n" + 
			"    ],\n" + 
			"    \"rules\": [\n" + 
			"      {\n" + 
			"        \"2\": \"1\",\n" + 
			"        \"4\": \"2\",\n" + 
			"        \"1_operator\": \">\",\n" + 
			"        \"1_expression\": \"30\",\n" + 
			"        \"3_expression\": \"test\",\n" + 
			"        \"3_operator\": \"==\"\n" + 
			"      },\n" + 
			"      {\n" + 
			"        \"2\": \"abc\",\n" + 
			"        \"4\": \"2\",\n" + 
			"        \"1_operator\": \"<=\",\n" + 
			"        \"1_expression\": 30,\n" + 
			"        \"3_operator\": \"!=\",\n" + 
			"        \"3_expression\": \"test1\"\n" + 
			"      }\n" + 
			"    ]\n" + 
			"  }\n" + 
			"	}\n" + 
			"}";
	
	public static final String draftJson = "{\n" + 
			"	\"moduleId\": 1,\n" + 
			"	\"moduleStatus\": \"DRAFT\"\n" + 
			"}";
	
	public static final String publishJson = "{\n" + 
			"	\"moduleId\": 1,\n" + 
			"	\"moduleStatus\": \"DRAFT\"\n" + 
			"}";
	
	
//	POJOS
	
	public static final ConfigModuleRequestData configModuleRequestData = (ConfigModuleRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(moduleJson, ConfigModuleRequestData.class);
	
	public static final ConfigModuleRequestData configModuleEditRequestData = (ConfigModuleRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(moduleEditJson, ConfigModuleRequestData.class);
	
	public static final ConfigServiceProviderInputData configSPRequestData = (ConfigServiceProviderInputData) NotificationHelperFunction
			.getPOJOFromJsonString(serviceProvidorJson, ConfigServiceProviderInputData.class);
	
	public static final ConfigServiceProviderInputData configSPEditRequestData = (ConfigServiceProviderInputData) NotificationHelperFunction
			.getPOJOFromJsonString(serviceProvidorEditJson, ConfigServiceProviderInputData.class);
	
	public static final ConfigChannelListData configChannelsRequestData = (ConfigChannelListData) NotificationHelperFunction
			.getPOJOFromJsonString(channelsJson, ConfigChannelListData.class);
	
	public static final ConfigChannelListData configChannelsEditRequestData = (ConfigChannelListData) NotificationHelperFunction
			.getPOJOFromJsonString(channelsEditJson, ConfigChannelListData.class);
	
	public static final VariableListRequestData configVariablesRequestData = (VariableListRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(variablesJson, VariableListRequestData.class);
	
	public static final VariableListRequestData configVariablesEditRequestData = (VariableListRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(variablesEditJson, VariableListRequestData.class);
	
	public static final RangesListRequestData configRangesRequestData = (RangesListRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(rangesJson, RangesListRequestData.class);
	
	public static final RangesListRequestData configRangesEditRequestData = (RangesListRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(rangesEditJson, RangesListRequestData.class);
	
	public static final NotificationTableRequestData notificationTableRequestData = (NotificationTableRequestData) NotificationHelperFunction
			.getPOJOFromJsonString(notificationTableJson, NotificationTableRequestData.class);
	
	public static final DraftOrPublishNotificationTable draftRequestData = (DraftOrPublishNotificationTable) NotificationHelperFunction
			.getPOJOFromJsonString(draftJson, DraftOrPublishNotificationTable.class);
	
	public static final DraftOrPublishNotificationTable publishRequestData = (DraftOrPublishNotificationTable) NotificationHelperFunction
			.getPOJOFromJsonString(publishJson, DraftOrPublishNotificationTable.class);
	
	
//	Objects
	
	public static final NotificationModuleModel moduleObject = new NotificationModuleModel(configModuleRequestData.getGroupName(), 
			configModuleRequestData.getSystem(), configModuleRequestData.getDescription(), ModuleStatus.CREATED, null, ASSIGNEE, null);
	

	public static final ServiceProviderModel spObject = new ServiceProviderModel(configSPRequestData.getName(),
			configSPRequestData.getFromAddress(), configSPRequestData.getIbEndpoint());
	
	public static final NotificationChannelModel channelObject1 = new NotificationChannelModel(moduleObject,
			configChannelsRequestData.getChannels().get(0).getChannelType(),
			configChannelsRequestData.getChannels().get(0).getStartTime(),
			configChannelsRequestData.getChannels().get(0).getEndTime(),
			configChannelsRequestData.getChannels().get(0).getServiceProviders());

	public static final NotificationChannelModel channelObject2 = new NotificationChannelModel(moduleObject,
			configChannelsRequestData.getChannels().get(1).getChannelType(),
			configChannelsRequestData.getChannels().get(1).getStartTime(),
			configChannelsRequestData.getChannels().get(1).getEndTime(),
			configChannelsRequestData.getChannels().get(1).getServiceProviders());
	
	public static final NotificationVariablesModel variableObject1 = new NotificationVariablesModel(configVariablesRequestData.getVariables().get(0).getKey(),
			configVariablesRequestData.getVariables().get(0).getLabel(),configVariablesRequestData.getVariables().get(0).getSystem(),
			configVariablesRequestData.getVariables().get(0).getVariableType(), configVariablesRequestData.getVariables().get(0).getDescription());
	
	public static final NotificationVariablesModel variableObject2 = new NotificationVariablesModel(configVariablesRequestData.getVariables().get(1).getKey(),
			configVariablesRequestData.getVariables().get(1).getLabel(),configVariablesRequestData.getVariables().get(1).getSystem(),
			configVariablesRequestData.getVariables().get(1).getVariableType(), configVariablesRequestData.getVariables().get(1).getDescription());
	
	public static final NotificationRangesModel rangeObject1 = new NotificationRangesModel(moduleObject,
			variableObject1, configRangesRequestData.getRanges().get(0).getMinValue(),
			configRangesRequestData.getRanges().get(0).getMaxValue(),
			configRangesRequestData.getRanges().get(0).getValues(),
			configRangesRequestData.getRanges().get(0).isMandatory());
	
	public static final NotificationRangesModel rangeObject2 = new NotificationRangesModel(moduleObject,
			variableObject1, configRangesRequestData.getRanges().get(1).getMinValue(),
			configRangesRequestData.getRanges().get(1).getMaxValue(),
			configRangesRequestData.getRanges().get(1).getValues(),
			configRangesRequestData.getRanges().get(1).isMandatory());
	
	public static final DecisionTableModel decisionTableModel = new DecisionTableModel(notificationTableRequestData.getDecisionTable());
	

//	Objects List
	
	public static final List<NotificationModuleModel> moduleObjectList = Arrays.asList(moduleObject, moduleObject);

	public static final List<ServiceProviderModel> spObjectList = Arrays.asList(spObject, spObject);
	
	public static final List<NotificationChannelModel> channelObjectList = Arrays.asList(channelObject1, channelObject2);
	
	public static final List<NotificationVariablesModel> variableObjectList = Arrays.asList(variableObject1, variableObject2);
	
	public static final List<NotificationRangesModel> rangeObjectList = Arrays.asList(rangeObject1, rangeObject2);
	
	public static final long MODEL_ID = 1;
	
	public static final Systems SYSTEM = Systems.COLLECTIONS;
	
	public static final int STEP = 2;
}
