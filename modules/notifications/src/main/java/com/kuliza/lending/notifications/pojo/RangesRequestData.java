package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.notifications.models.NotificationVariablesModel;

public class RangesRequestData {

	Long id;

	@NotNull(message = "Variable id cannot be null.")
	@NotEmpty(message = "Variable id cannot be empty.")
	Long variableId;
	
	NotificationVariablesModel variable;

	Float minValue;

	Float maxValue;

	List<String> values;

	Boolean isMandatory;

	public RangesRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RangesRequestData(Long id, Long variableId, Float minValue, Float maxValue, List<String> values,
			Boolean isMandatory) {
		super();
		this.id = id;
		this.variableId = variableId;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.values = values;
		this.isMandatory = isMandatory;
	}
	
	public RangesRequestData(Long id, Long variableId, NotificationVariablesModel variable, Float minValue, Float maxValue, List<String> values,
			Boolean isMandatory) {
		super();
		this.id = id;
		this.variableId = variableId;
		this.variable = variable;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.values = values;
		this.isMandatory = isMandatory;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVariableId() {
		return variableId;
	}

	public void setVariableId(Long variableId) {
		this.variableId = variableId;
	}

	public NotificationVariablesModel getVariable() {
		return variable;
	}

	public void setVariable(NotificationVariablesModel variable) {
		this.variable = variable;
	}

	public Float getMinValue() {
		return minValue;
	}

	public void setMinValue(Float minValue) {
		this.minValue = minValue;
	}

	public Float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Float maxValue) {
		this.maxValue = maxValue;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public Boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
}
