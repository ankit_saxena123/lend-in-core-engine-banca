package com.kuliza.lending.notifications.jms;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@Service
// @ConditionalOnExpression("${broker.name.rabbit:true}")
public class NotificationProducerJMS {

	@Autowired
	JmsTemplate jmsTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationProducerJMS.class);

	public void sendNotification(NotificationModel notificationObject) throws JsonProcessingException, JSONException {
		LOGGER.info("In notification sender");
		if (notificationObject.getTemplateModel() != null
				&& notificationObject.getTemplateModel().getNotificationChannelModel() != null) {
			try {
				ChannelTypes channelType = notificationObject.getTemplateModel().getNotificationChannelModel()
						.getChannelType();
				LOGGER.info("Sent notification in queue: " + channelType.toString());
				System.out.println(NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(2));
				jmsTemplate.convertAndSend(NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(2),
						notificationObject.getId());
			} catch (Exception e) {
				// TODO: handle exception
				LOGGER.info("Error in queueing notification object: " + e.getStackTrace());
			}
		} else {
			LOGGER.info("No template or channel found for notification object");
		}
	}

}
