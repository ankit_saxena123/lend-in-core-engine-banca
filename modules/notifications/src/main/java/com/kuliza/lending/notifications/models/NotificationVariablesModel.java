package com.kuliza.lending.notifications.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;
import com.kuliza.lending.notifications.utils.NotificationConstants.VariableTypes;

@Entity
@Table(name = "notification_global_variables")
public class NotificationVariablesModel extends BaseModel {

	@Column(nullable = false)
	private String variableKey;
	
	@Column
	private String variableLabel;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Systems system;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private VariableTypes variableType;
	
	@Column
	private String description;

	public NotificationVariablesModel() {
		super();
		this.setIsDeleted(false);
		// TODO Auto-generated constructor stub
	}

	public NotificationVariablesModel(String variableKey, String variableLabel, Systems system, VariableTypes variableType, String description) {
		super();
		this.variableKey = variableKey;
		this.variableLabel = variableLabel;
		this.system = system;
		this.variableType = variableType;
		this.description = description;
		this.setIsDeleted(false);
	}

	public String getVariableKey() {
		return variableKey;
	}

	public void setVariableKey(String variableKey) {
		this.variableKey = variableKey;
	}

	public String getVariableLabel() {
		return variableLabel;
	}

	public void setVariableLabel(String variableLabel) {
		this.variableLabel = variableLabel;
	}

	public Systems getSystem() {
		return system;
	}

	public void setSystem(Systems system) {
		this.system = system;
	}

	public VariableTypes getVariableType() {
		return variableType;
	}

	public void setVariableType(VariableTypes variableType) {
		this.variableType = variableType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
