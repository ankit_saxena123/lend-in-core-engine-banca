package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;

public class VariableListRequestData {

	@NotNull(message = "Channels list cannot be null")
	@Size(min = 1)
	List<VariablesRequestData> variables;

	@NotNull(message = "Module Id cannot be null")
	public long moduleId;

	public VariableListRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VariableListRequestData(List<VariablesRequestData> variables, long moduleId) {
		super();
		this.variables = variables;
		this.moduleId = moduleId;
	}

	public List<VariablesRequestData> getVariables() {
		return variables;
	}

	public void setVariables(List<VariablesRequestData> variables) {
		this.variables = variables;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

}
