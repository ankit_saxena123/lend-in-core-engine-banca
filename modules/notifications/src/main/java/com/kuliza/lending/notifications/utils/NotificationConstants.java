package com.kuliza.lending.notifications.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationConstants {
    private NotificationConstants() {
        throw new UnsupportedOperationException();
    }

    // JMS Configuration
    public static final String JMS_NOTIFICATION_ROUTING_KEY = "route.jms.notificationQueue";
    
    public static final String JMS_SMS_NOTIFICATION_ROUTING_KEY = "route.jms.smsQueue";
    
    public static final String JMS_EMAIL_NOTIFICATION_ROUTING_KEY = "route.jms.emailQueue";
    
    public static final String JMS_PUSH_NOTIFICATION_ROUTING_KEY = "route.jms.pushNotificationQueue";
    
    
    public static final String NOTIFICATION_CONTROLLER_ENDPOINT = "/lending/notifications/";
    
    public static final String NOTIFICATION_CONFIG_CONTROLLER_ENDPOINT = "/lending/notifications/config/";
    

    public static final String CREATE_NOTIFICATION_API_ENDPOINT = "create-notification";
    
    public static final String SEND_NOTIFICATION_API_ENDPOINT = "send-notification";
    
    public static final String CONFIG_MODULES_API_ENDPOINT = "modules";
    
    public static final String CONFIG_EDIT_MODULES_API_ENDPOINT = "edit";
    
    public static final String CONFIG_SYSTEM_LIST_API_ENDPOINT = "system";
    
    public static final String CONFIG_VARIABLE_TYPES_LIST_API_ENDPOINT = "variable-types";
    
    public static final String CONFIG_SERVICE_PROVIDERS_API_ENDPOINT = "service-providers";
    
    public static final String CONFIG_MODULE_EDIT_API_ENDPOINT = "modules/{id}";
    
    public static final String CONFIG_STEPPER_API_ENDPOINT = "stepper/{stepper}";
    
    public static final String CONFIG_BACK_STEPPER_API_ENDPOINT = "back/{stepper}";
    
    
    public static final String CREATE_DUMMY_API_ENDPOINT = "create-dummy";
    
    public static final String TRIGGER_SMS_API_ENDPOINT = "trigger-sms";
    
    public static final String TRIGGER_EMAIL_API_ENDPOINT = "trigger-email";

    public static final String SEND_EMAIL_API_ENDPOINT = "send-email";
    
    public static final Map<ChannelTypes, List<String>> CHANNEL_KEY_MAPPING;
	static {
		CHANNEL_KEY_MAPPING = new HashMap<ChannelTypes, List<String>>();
		CHANNEL_KEY_MAPPING.put(ChannelTypes.SMS, Arrays.asList("smsTemplate", "smsFrequency", JMS_SMS_NOTIFICATION_ROUTING_KEY));
		CHANNEL_KEY_MAPPING.put(ChannelTypes.EMAIL, Arrays.asList("emailTemplate", "emailFrequency", JMS_EMAIL_NOTIFICATION_ROUTING_KEY));
		CHANNEL_KEY_MAPPING.put(ChannelTypes.PUSH, Arrays.asList("pushNotificationTemplate", "pushNotificationFrequency", JMS_PUSH_NOTIFICATION_ROUTING_KEY));
	}
	
	public enum Systems{
		LOS, LMS, BACKOFFICE, COLLECTIONS, COLLATERAL;
	}
	
	public enum ModuleStatus{
		CREATED, DRAFT, PUBLISHED;
	}
	
	public enum ChannelTypes{
		SMS, EMAIL, PUSH;
	}
	
	public enum VariableTypes{
		NUMBER, STRING, DATE, BOOLEAN;
	}

	public enum NotificationResponses{
		SUCCESS, FAILURE, PENDING;
	}
	
	public static final String MODULE_DATE_FORMAT = "dd MMM yyyy";

	public static final String DMN_MODEL_ID = "2";
	
	public static final String DMN_HIT_POLICY = "FIRST";
}
