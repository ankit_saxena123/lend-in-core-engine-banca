package com.kuliza.lending.notifications.jms;

import java.io.IOException;
import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.integrations.EmailSMTPIntegration;
import com.kuliza.lending.notifications.models.*;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@Component
@EnableJms
// @ConditionalOnExpression("${broker.name.jms:true}")
public class EmailNotificationRecieverJMS implements MessageListener {

	@Autowired
	TemplateDao templateDao;

	@Autowired
	CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
    private EmailSMTPIntegration emailSMTPIntegration;

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailNotificationRecieverJMS.class);

	@JmsListener(destination = NotificationConstants.JMS_EMAIL_NOTIFICATION_ROUTING_KEY, containerFactory = "jmsFactory")
	@Override
	@Transactional
	public void onMessage(Message message) {
		try {
			if (message instanceof TextMessage) {
				String textMessage = HelperFunctions.extractStringFromMessage((TextMessage) message);
				ObjectMapper mapper = new ObjectMapper();
				try {
					String uuid = mapper.readValue(textMessage, String.class);
					NotificationModel notificationObject = notificationDao.findById(uuid);
					TemplateModel templateObject = notificationObject.getTemplateModel();
					if (templateObject != null) {
						sendEmail(notificationObject);
					} else {
						System.out.println("Template not found.");
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (message instanceof BytesMessage) {
				byte[] byteArray = HelperFunctions.extractByteArrayFromMessage((BytesMessage) message);
			} else if (message instanceof MapMessage) {
				Map<String, Object> mapMessage = HelperFunctions.extractMapFromMessage((MapMessage) message);
			} else if (message instanceof ObjectMessage) {
				Serializable object = HelperFunctions.extractSerializableFromMessage((ObjectMessage) message);
			} else {
				LOGGER.info("Received: " + message);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				message.acknowledge();
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}

	private String substituteVariabes(Map<String, Object> variables, String str) {
		StrSubstitutor sub = new StrSubstitutor(variables, "{{", "}}");
		return sub.replace(str);

	}
	
	private ResponseEntity<String> sendEmail(NotificationModel notificationObject) {
		// TODO Auto-generated method stub
		ResponseEntity<String> response = null;
		try {
			Map<String, String> payload = emailSMTPIntegration.buildRequestPayload(notificationObject);
			System.out.println(payload);
			response = emailSMTPIntegration.hitIB(payload, notificationObject.getTemplateModel().getNotificationChannelModel());
			emailSMTPIntegration.parseResponse(response, notificationObject);
			System.out.println(response);
			return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getStackTrace());
		}
		return response;
	}

}