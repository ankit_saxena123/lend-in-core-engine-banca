package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

public class RangesListRequestData {

	@NotNull(message = "Module id cannot be null.")
	@NotEmpty(message = "Module id cannot be empty.")
	long moduleId;

	@NotNull(message = "Ranges list cannot be null")
	List<RangesRequestData> ranges;
	
	public RangesListRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RangesListRequestData(long moduleId, List<RangesRequestData> ranges) {
		super();
		this.moduleId = moduleId;
		this.ranges = ranges;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public List<RangesRequestData> getRanges() {
		return ranges;
	}

	public void setRanges(List<RangesRequestData> ranges) {
		this.ranges = ranges;
	}
	
}
