package com.kuliza.lending.notifications.jms;

import java.io.IOException;
import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.transaction.Transactional;

import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.NotificationHistoryDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.models.*;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

@Component
@EnableJms
// @ConditionalOnExpression("${broker.name.jms:true}")
public class NotificationMessageRecieverJMS implements MessageListener {

	@Autowired
	TemplateDao templateDao;

	@Autowired
	CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
	NotificationHistoryDao notificationHistoryDao;

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationMessageRecieverJMS.class);

	@JmsListener(destination = NotificationConstants.JMS_NOTIFICATION_ROUTING_KEY, containerFactory = "jmsFactory")
	@Override
	@Transactional
	public void onMessage(Message message) {
		try {
			if (message instanceof TextMessage) {
				String textMessage = HelperFunctions.extractStringFromMessage((TextMessage) message);
				ObjectMapper mapper = new ObjectMapper();
				try {
					NotificationData notificationData = mapper.readValue(textMessage, NotificationData.class);
					System.out.println("Notification Template: " + notificationData.toString());
					if (notificationData.getNotificationConfig().containsKey("smsTemplate")) {
						createNotificationObjects(ChannelTypes.SMS, notificationData);
					} 
					if (notificationData.getNotificationConfig().containsKey("emailTemplate")) {
						createNotificationObjects(ChannelTypes.EMAIL, notificationData);
					}
					if (notificationData.getNotificationConfig().containsKey("pushTemplate")){
						createNotificationObjects(ChannelTypes.PUSH, notificationData);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (message instanceof BytesMessage) {
				byte[] byteArray = HelperFunctions.extractByteArrayFromMessage((BytesMessage) message);
			} else if (message instanceof MapMessage) {
				Map<String, Object> mapMessage = HelperFunctions.extractMapFromMessage((MapMessage) message);
			} else if (message instanceof ObjectMessage) {
				Serializable object = HelperFunctions.extractSerializableFromMessage((ObjectMessage) message);
			} else {
				LOGGER.info("Received: " + message);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				message.acknowledge();
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}

	@Transactional
	private void createNotificationObjects(ChannelTypes channelType, NotificationData notificationData) {
		TemplateModel templateModel = templateDao.findByIdAndIsDeleted(
				CommonHelperFunctions.getLongValue(
						notificationData.getNotificationConfig().get(
								NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(0)))
				, false);
		NotificationChannelModel notificationChannelModel = templateModel.getNotificationChannelModel();

		List<Date> timeFrames = getFrames(notificationChannelModel,
				CommonHelperFunctions.getIntegerValue(notificationData.getNotificationConfig()
						.get(NotificationConstants.CHANNEL_KEY_MAPPING.get(channelType).get(1))));

		Map<String, Object> caseVariables = notificationData.getCaseVariables();

		
		List<String> templateVariableKeys = templateModel.getVariables();
		
		caseVariables = templateVariableKeys.stream().filter(caseVariables::containsKey)
				.collect(Collectors.toMap(Function.identity(), caseVariables::get));
		for (Date timeFrame : timeFrames) {
			NotificationModel notificationObject = new NotificationModel();
			notificationObject.setSourceId(notificationData.getCaseInstanceId());
			notificationObject.setSendTime(timeFrame);
			notificationObject.setSendDate(new Date());
			notificationObject.setTemplateModel(templateModel);
			notificationObject.setUserAttributes(caseVariables);
			notificationObject.setIsSync(false);
			notificationObject = notificationDao.save(notificationObject);
			NotificationHistoryModel notificationHistoryModel = new NotificationHistoryModel(notificationObject);
			notificationHistoryDao.save(notificationHistoryModel);	
		}
	}

	private List<Date> getFrames(NotificationChannelModel notificationChannelModel, Integer frequency) {
		List<Date> dateFrames = new ArrayList<Date>();
		Calendar startTime = Calendar.getInstance();
		startTime.setTime(notificationChannelModel.getStartTime());
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(notificationChannelModel.getEndTime());
		if (endTime.compareTo(startTime) == 1) {
			long hoursBetween = ChronoUnit.HOURS.between(startTime.toInstant(), endTime.toInstant());
			long duration = hoursBetween / frequency;
			dateFrames.add(startTime.getTime());
			for (int i = 1; i < frequency; i++) {
				startTime.add(Calendar.HOUR_OF_DAY, (int) duration);
				dateFrames.add(startTime.getTime());
			}
		}
		return dateFrames;
	}
}