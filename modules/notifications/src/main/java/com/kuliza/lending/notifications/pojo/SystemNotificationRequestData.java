package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;

public class SystemNotificationRequestData {

	@NotNull(message = "Case Ids cannot be null")
	@Size(min = 1)
	List<String> caseIds;

	@NotNull(message = "Module Id cannot be null")
	public long moduleId;

	public SystemNotificationRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SystemNotificationRequestData(List<String> caseIds, long moduleId) {
		super();
		this.caseIds = caseIds;
		this.moduleId = moduleId;
	}

	public List<String> getCaseIds() {
		return caseIds;
	}

	public void setCaseIds(List<String> caseIds) {
		this.caseIds = caseIds;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

}
