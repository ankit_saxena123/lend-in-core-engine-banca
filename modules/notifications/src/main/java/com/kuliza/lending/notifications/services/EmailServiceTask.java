package com.kuliza.lending.notifications.services;

import com.kuliza.lending.notifications.integrations.EmailSMTPIntegration;
import com.kuliza.lending.notifications.integrations.SMSValueFirstIntegration;
import com.kuliza.lending.notifications.models.NotificationModel;
import java.io.IOException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("EmailServiceTask")
public class EmailServiceTask {

	@Autowired
	EmailSMTPIntegration emailSMTPIntegration;

	public ResponseEntity<String> sendViaGoogleSMTP(String sourceId, Long templateId, Map<String, Object> variables)
			throws IOException, InterruptedException, Exception {

		ResponseEntity<String> response = null;
		try {
			NotificationModel notificationObject = emailSMTPIntegration.createSyncNotificationObject(sourceId,
					templateId, variables);
			Map<String, String> payload = emailSMTPIntegration.buildRequestPayload(notificationObject);
			System.out.println(payload);
			response = emailSMTPIntegration.hitIB(payload,
					notificationObject.getTemplateModel().getNotificationChannelModel());
			emailSMTPIntegration.parseResponse(response, notificationObject);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedException("Error in Email Service Task.");
		}
	}
}
