package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.kuliza.lending.decision_table.pojo.DecisionTableModelRepresentation;

import java.util.List;

public class NotificationTableRequestData {

	DecisionTableModelRepresentation decisionTable;

	Long moduleId;

	public NotificationTableRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotificationTableRequestData(DecisionTableModelRepresentation decisionTable, Long moduleId) {
		super();
		this.decisionTable = decisionTable;
		this.moduleId = moduleId;
	}

	public DecisionTableModelRepresentation getDecisionTable() {
		return decisionTable;
	}

	public void setDecisionTable(DecisionTableModelRepresentation decisionTable) {
		this.decisionTable = decisionTable;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
}
