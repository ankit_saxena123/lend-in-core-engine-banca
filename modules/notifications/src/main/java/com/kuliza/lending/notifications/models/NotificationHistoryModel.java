package com.kuliza.lending.notifications.models;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.id.UUIDGenerator;

import com.kuliza.lending.notifications.pojo.NotificationProviderResponseData;
import com.kuliza.lending.notifications.utils.NotificationConstants.NotificationResponses;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "notification_history")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class NotificationHistoryModel {

	@Id
	@ApiModelProperty(required = false, hidden = true)
	@Column(updatable = false, nullable = false)
	private String id;

	@Column(nullable = true)
	private String sourceId;

	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private Boolean isSync;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date sendDate;

	@Column(nullable = false)
	@Temporal(TemporalType.TIME)
	private Date sendTime;

	@Column(nullable = true)
	@Temporal(TemporalType.TIME)
	private Date sentTime;

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	private NotificationResponses status;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "templateId", nullable = false)
	private TemplateModel templateModel;

	@Convert(converter = HashMapConverter.class)
	@Column(columnDefinition = "LONGTEXT")
	private Map<String, Object> userAttributes;

	@Type(type = "json")
	@Column(columnDefinition = "json", nullable = true)
	private NotificationProviderResponseData response;

	public NotificationHistoryModel() {
		super();
		this.setIsSync(false);
		// TODO Auto-generated constructor stub
	}

	public NotificationHistoryModel(String id, String sourceId, Boolean isSync, Date sendDate, Date sendTime, Date sentTime,
			NotificationResponses status, TemplateModel templateModel, Map<String, Object> userAttributes,
			NotificationProviderResponseData response) {
		super();
		this.id = id;
		this.sourceId = sourceId;
		this.sendDate = sendDate;
		this.sendTime = sendTime;
		this.sentTime = sentTime;
		this.status = status;
		this.templateModel = templateModel;
		this.userAttributes = userAttributes;
		this.response = response;
		this.setIsSync(false);
	}

	public NotificationHistoryModel(NotificationModel notificationModel) {
		super();
		this.id = notificationModel.getId();
		this.sourceId = notificationModel.getSourceId();
		this.sendDate = notificationModel.getSendDate();
		this.sendTime = notificationModel.getSendTime();
		this.templateModel = notificationModel.getTemplateModel();
		this.userAttributes = notificationModel.getUserAttributes();
		this.isSync = notificationModel.getIsSync();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsSync() {
		return isSync;
	}

	public void setIsSync(Boolean isSync) {
		this.isSync = isSync;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Date getSentTime() {
		return sentTime;
	}

	public void setSentTime(Date sentTime) {
		this.sentTime = sentTime;
	}

	public NotificationResponses getStatus() {
		return status;
	}

	public void setStatus(NotificationResponses status) {
		this.status = status;
	}

	public TemplateModel getTemplateModel() {
		return templateModel;
	}

	public void setTemplateModel(TemplateModel templateModel) {
		this.templateModel = templateModel;
	}

	public Map<String, Object> getUserAttributes() {
		return userAttributes;
	}

	public void setUserAttributes(Map<String, Object> userAttributes) {
		this.userAttributes = userAttributes;
	}

	public NotificationProviderResponseData getResponse() {
		return response;
	}

	public void setResponse(NotificationProviderResponseData response) {
		this.response = response;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
}
