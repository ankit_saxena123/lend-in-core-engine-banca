package com.kuliza.lending.notifications.pojo;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;

import java.util.List;

public class DraftOrPublishNotificationTable {
	
	@NotNull(message = "Module Id cannot be null")
	public long moduleId;
	
	@NotNull(message = "Module Status cannot be null.")
	@NotEmpty(message = "Module Status cannot be empty.")
	@Enumerated(EnumType.STRING)
	ModuleStatus moduleStatus;

	public DraftOrPublishNotificationTable() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DraftOrPublishNotificationTable(long moduleId, ModuleStatus moduleStatus) {
		super();
		this.moduleId = moduleId;
		this.moduleStatus = moduleStatus;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public ModuleStatus getModuleStatus() {
		return moduleStatus;
	}

	public void setModuleStatus(ModuleStatus moduleStatus) {
		this.moduleStatus = moduleStatus;
	}

}
