package com.kuliza.lending.notifications.pojo;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class EmailRequestData {

	@NotNull(message = "To cannot be null.")
	@NotEmpty(message = "To cannot be empty")
	String to;
	
	String docIds;	
	
	@NotNull(message = "Subject cannot be null.")
	@NotEmpty(message = "Subject cannot be empty")
	String subject;
	
	@NotNull(message = "Body cannot be null.")
	@NotEmpty(message = "Body cannot be empty")
	String body;

	public EmailRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmailRequestData(String to, String docIds, String subject, String body) {
		super();
		this.to = to;
		this.docIds = docIds;
		this.subject = subject;
		this.body = body;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getDocIds() {
		return docIds;
	}

	public void setDocIds(String docIds) {
		this.docIds = docIds;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
}
