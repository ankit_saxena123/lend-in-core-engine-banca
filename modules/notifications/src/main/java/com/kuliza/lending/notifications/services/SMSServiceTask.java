package com.kuliza.lending.notifications.services;

import com.kuliza.lending.notifications.integrations.SMSValueFirstIntegration;
import com.kuliza.lending.notifications.models.NotificationModel;
import java.io.IOException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("SMSServiceTask")
public class SMSServiceTask {

	@Autowired
	SMSValueFirstIntegration smsValueFirstIntegration;

	public ResponseEntity<String> sendViaValueFirst(String sourceId, Long templateId, Map<String, Object> variables)
			throws IOException, InterruptedException, Exception {

		ResponseEntity<String> response = null;
		try {
			NotificationModel notificationObject = smsValueFirstIntegration.createSyncNotificationObject(sourceId,
					templateId, variables);
			Map<String, String> payload = smsValueFirstIntegration.buildRequestPayload(notificationObject);
			System.out.println(payload);
			response = smsValueFirstIntegration.hitIB(payload,
					notificationObject.getTemplateModel().getNotificationChannelModel());
			smsValueFirstIntegration.parseResponse(response, notificationObject);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedException("Error in Allocation Logic.");
		}
	}
}
