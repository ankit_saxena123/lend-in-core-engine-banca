package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

public class ConfigModuleRequestData {

	@NotNull(message = "Group name cannot be null.")
	@NotEmpty(message = "Group name cannot be empty.")
	@Size(max = 255)
	String groupName;

	@Size(max = 255)
	String description;

	Long id;

	@NotNull(message = "System name cannot be null.")
	Systems system;
	
	String assignee;
	
	String modified;
	
	ModuleStatus status;

	Integer step;
	
	public ConfigModuleRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigModuleRequestData(String groupName, String description, Long id, Systems system, String assignee,
			String modified, ModuleStatus status, Integer step) {
		super();
		this.groupName = groupName;
		this.description = description;
		this.id = id;
		this.system = system;
		this.assignee = assignee;
		this.modified = modified;
		this.status = status;
		this.step = step;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Systems getSystem() {
		return system;
	}

	public void setSystem(Systems system) {
		this.system = system;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public ModuleStatus getStatus() {
		return status;
	}

	public void setStatus(ModuleStatus status) {
		this.status = status;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

}
