package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.kuliza.lending.decision_table.pojo.DecisionTableModelRepresentation;

import java.util.List;

public class NotificationLogicTableData {

	@NotNull(message = "Ranges cannot be null")
	@Size(min = 1)
	List<RangesRequestData> ranges;
	
    List<RangesRequestData> outputRanges;

	DecisionTableModelRepresentation decisionTable;

	Boolean isSaved;

	public NotificationLogicTableData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotificationLogicTableData(List<RangesRequestData> ranges, List<RangesRequestData> outputRanges,
			DecisionTableModelRepresentation decisionTable, Boolean isSaved) {
		super();
		this.ranges = ranges;
		this.outputRanges = outputRanges;
		this.decisionTable = decisionTable;
		this.isSaved = isSaved;
	}

	public List<RangesRequestData> getRanges() {
		return ranges;
	}

	public void setRanges(List<RangesRequestData> ranges) {
		this.ranges = ranges;
	}

	public DecisionTableModelRepresentation getDecisionTable() {
		return decisionTable;
	}

	public void setDecisionTable(DecisionTableModelRepresentation decisionTable) {
		this.decisionTable = decisionTable;
	}

	public Boolean getIsSaved() {
		return isSaved;
	}

	public void setIsSaved(Boolean isSaved) {
		this.isSaved = isSaved;
	}

	public List<RangesRequestData> getOutputRanges() {
		return outputRanges;
	}

	public void setOutputRanges(List<RangesRequestData> outputRanges) {
		this.outputRanges = outputRanges;
	}

}
