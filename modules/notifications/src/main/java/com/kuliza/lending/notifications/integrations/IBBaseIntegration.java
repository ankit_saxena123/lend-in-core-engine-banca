package com.kuliza.lending.notifications.integrations;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.engine_common.configs.JourneyConfig;
import com.kuliza.lending.notifications.NotificationIBConfig;
import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.TemplateModel;

public interface IBBaseIntegration {

	public NotificationModel createSyncNotificationObject(String sourceId, Long templateId, Map<String, Object> caseVariables) throws Exception;
	
	public Map<String, String> buildRequestPayload(NotificationModel notificationObject) throws Exception;
	
	public ResponseEntity<String> hitIB(Map<String, String> payload,
			NotificationChannelModel channelObject) throws Exception;

	// Here you have to return the Map which needs to be stored in process
	// variables.
	public void parseResponse(ResponseEntity<String> responseFromIB, NotificationModel notificationObject) throws Exception;

}
