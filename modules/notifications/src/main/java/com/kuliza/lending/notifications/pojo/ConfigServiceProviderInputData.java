package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

public class ConfigServiceProviderInputData {

	@NotNull(message = "SP Name cannot be null.")
	@NotEmpty(message = "SP Name cannot be empty.")
	String name;

	@NotNull(message = "SP Name cannot be null.")
	@NotEmpty(message = "SP Name cannot be empty.")
	String ibEndpoint;

	String fromAddress;

	Long id;

	public ConfigServiceProviderInputData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigServiceProviderInputData(String name, String ibEndpoint, String fromAddress, Long id) {
		super();
		this.name = name;
		this.ibEndpoint = ibEndpoint;
		this.fromAddress = fromAddress;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIbEndpoint() {
		return ibEndpoint;
	}

	public void setIbEndpoint(String ibEndpoint) {
		this.ibEndpoint = ibEndpoint;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
