package com.kuliza.lending.notifications.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.configs.DmsConfig;
import com.kuliza.lending.engine_common.services.EngineCommonService;
import com.kuliza.lending.notifications.dao.NotificationChannelDao;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.NotificationModuleDao;
import com.kuliza.lending.notifications.dao.ServiceProviderDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.integrations.SMSValueFirstIntegration;
import com.kuliza.lending.notifications.jms.NotificationMessageProducerJMS;
import com.kuliza.lending.notifications.jms.NotificationProducerJMS;
import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.models.ServiceProviderModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.pojo.SystemNotificationRequestData;
import com.kuliza.lending.notifications.utils.NotificationHelperFunction;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
public class NotificationService {

	@Autowired
	private NotificationMessageProducerJMS messageProducerJMS;

	@Autowired
	private NotificationProducerJMS notificationProducerJMS;

	@Autowired
	private NotificationModuleDao notificationModuleDao;

	@Autowired
	private NotificationChannelDao notificationChannelDao;

	@Autowired
	private TemplateDao templateDao;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private NotificationDao notificationDao;
	
	@Autowired
	private DmsConfig dmsConfig;

	@Autowired
	private SMSValueFirstIntegration smsValueFirstIntegration;
	
	@Autowired
    private EngineCommonService engineCommonService;
	
	
	
	public ApiResponse sendEmail(String toAddress, String subject, String body, String docs)
			throws IOException, UnirestException {
		// TODO Auto-generated method stub
		ApiResponse apiResponse = null;
		
		MimeMessage msg = javaMailSender.createMimeMessage();
		// true = multipart message
		MimeMessageHelper helper;
		try {
            // Close the file
			helper = new MimeMessageHelper(msg, true);
			List<File> attachments = new ArrayList<File>();
			if (docs != null && !"".equals(docs)) {
				for (String doc : docs.split(",")) {
					Map<String, Object> responseFromDMS = engineCommonService.downloadFileInfo(doc.trim());
					File file = new File("/tmp/" + doc.trim() + "_" + new Date().getTime() + "."
							+ CommonHelperFunctions.getStringValue(responseFromDMS.get("fileName")).split("\\.")[1]);
					OutputStream os = new FileOutputStream(file);
					os.write((byte[]) responseFromDMS.get("bytes"));
					os.close();
					helper.addAttachment(CommonHelperFunctions.getStringValue(responseFromDMS.get("fileName")), file);
					attachments.add(file);
				}
			}
            helper.setTo(toAddress);
			helper.setSubject(subject);
			helper.setText(body, true);
			javaMailSender.send(msg);
			apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Email sent successfully.");
			for (File file : attachments) {
				file.delete();
			}
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;

	}
	
	public ApiResponse createNotification(SystemNotificationRequestData systemRequest) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndStatusAndIsDeleted(systemRequest.getModuleId(),
					ModuleStatus.PUBLISHED, false);
			if (notificationModule != null) {
				if (systemRequest.getCaseIds() != null && !systemRequest.getCaseIds().isEmpty()) {
					for (String caseId : systemRequest.getCaseIds()) {
						if (!messageProducerJMS.createNotificationData(caseId, notificationModule.getDecisionTableKey())) {
							return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
									"Error in making Notification Objects.");
						}
					}
				}
			}
			apiResponse = new ApiResponse(HttpStatus.OK, "Success");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in initiate");
		}
		return apiResponse;
	}

	public ApiResponse createDummy() throws Exception {
		ApiResponse apiResponse;
		try {
//			NotificationModuleModel notificationModuleObject = new NotificationModuleModel();
//			notificationModuleObject.setName("Pre-Due Notifications");
//			// notificationModuleObject.setSystem("collections");
//			notificationModuleObject.setDescription("DMN for pre-due notification.");
//			// notificationModuleObject.setDecisionTableKey("preDueNotificationCollections");
//			notificationModuleObject.setAssignee("notificationUser@kuliza.com");
//			// notificationModuleObject.setStatus("Published");
//			notificationModuleObject = notificationModuleDao.save(notificationModuleObject);
//
//			ServiceProviderModel smsServiceProviderObject = new ServiceProviderModel();
//			smsServiceProviderObject.setIbEndpoint("/notification-engine/default-sms/");
//			smsServiceProviderObject.setName("Sms Default Integration");
//			smsServiceProviderObject = serviceProviderDao.save(smsServiceProviderObject);
//
//			ServiceProviderModel emailServiceProviderObject1 = new ServiceProviderModel();
//			emailServiceProviderObject1.setIbEndpoint("/notification-engine/default-email-1/");
//			emailServiceProviderObject1.setName("Email Default Integration 1");
//			emailServiceProviderObject1 = serviceProviderDao.save(emailServiceProviderObject1);
//
//			ServiceProviderModel emailServiceProviderObject2 = new ServiceProviderModel();
//			emailServiceProviderObject2.setIbEndpoint("/notification-engine/default-email-2/");
//			emailServiceProviderObject2.setName("Email Default Integration 2");
//			emailServiceProviderObject2 = serviceProviderDao.save(emailServiceProviderObject2);
//
//			ServiceProviderModel pushServiceProviderObject = new ServiceProviderModel();
//			pushServiceProviderObject.setIbEndpoint("/notification-engine/default-push-notification/");
//			pushServiceProviderObject.setName("Push Notification Default Integration");
//			pushServiceProviderObject = serviceProviderDao.save(pushServiceProviderObject);
//
//			Calendar start = Calendar.getInstance();
//			start.set(Calendar.HOUR_OF_DAY, 8);
//			start.set(Calendar.MINUTE, 0);
//			start.set(Calendar.SECOND, 0);
//			start.set(Calendar.MILLISECOND, 0);
//
//			NotificationChannelModel smsNotificationChannelObject = new NotificationChannelModel();
//			// smsNotificationChannelObject.setChannelType("sms");
//			smsNotificationChannelObject.setNotificationModule(notificationModuleObject);
//			smsNotificationChannelObject.setStartTime(start.getTime());
//			start.set(Calendar.HOUR_OF_DAY, 18);
//			smsNotificationChannelObject.setEndTime(start.getTime());
//			JSONObject spsms = new JSONObject();
//			spsms.append("" + smsServiceProviderObject.getId(), 3);
//			JSONArray spsmsList = new JSONArray();
//			spsmsList.put(spsms);
//			// smsNotificationChannelObject.setServiceProviders(spsmsList.toString());
//			smsNotificationChannelObject = notificationChannelDao.save(smsNotificationChannelObject);
//
//			NotificationChannelModel emailNotificationChannelObject = new NotificationChannelModel();
//			// emailNotificationChannelObject.setChannelType("email");
//			emailNotificationChannelObject.setNotificationModule(notificationModuleObject);
//			start.set(Calendar.HOUR_OF_DAY, 10);
//			emailNotificationChannelObject.setStartTime(start.getTime());
//			start.set(Calendar.HOUR_OF_DAY, 21);
//			emailNotificationChannelObject.setEndTime(start.getTime());
//			JSONObject spemail = new JSONObject();
//			spemail.append("" + emailServiceProviderObject1.getId(), 2);
//			JSONObject spemail1 = new JSONObject();
//			spemail1.append("" + emailServiceProviderObject2.getId(), 3);
//			JSONArray spemaillist = new JSONArray();
//			spemaillist.put(spemail);
//			spemaillist.put(spemail1);
//			// emailNotificationChannelObject.setServiceProviders(spemaillist.toString());
//			emailNotificationChannelObject = notificationChannelDao.save(emailNotificationChannelObject);
//
//			NotificationChannelModel pushNotificationChannelObject = new NotificationChannelModel();
//			// pushNotificationChannelObject.setChannelType("push");
//			pushNotificationChannelObject.setNotificationModule(notificationModuleObject);
//			start.set(Calendar.HOUR_OF_DAY, 10);
//			pushNotificationChannelObject.setStartTime(start.getTime());
//			start.set(Calendar.HOUR_OF_DAY, 21);
//			pushNotificationChannelObject.setEndTime(start.getTime());
//			JSONObject sppush = new JSONObject();
//			sppush.append("" + pushServiceProviderObject.getId(), 5);
//			JSONArray sppushlist = new JSONArray();
//			sppushlist.put(sppush);
//			// pushNotificationChannelObject.setServiceProviders(sppushlist.toString());
//			pushNotificationChannelObject = notificationChannelDao.save(pushNotificationChannelObject);

			NotificationChannelModel smsChannelObject = notificationChannelDao.findByIdAndIsDeleted(60, false);
			
			TemplateModel smsDefaultTemplate = new TemplateModel();
			smsDefaultTemplate.setName("Default SMS Template");
			smsDefaultTemplate.setToAddress("{{applicantMobileNumber}}");
			smsDefaultTemplate.setNotificationChannelModel(smsChannelObject);
			List<String> y = Arrays.asList("applicantFirstName", "applicantMobileNumber");
			smsDefaultTemplate.setBody("Your OTP from kuliza is {{applicantFirstName}}. Kindly don’t share it with anyone");
			smsDefaultTemplate.setVariables(y);
			smsDefaultTemplate = templateDao.save(smsDefaultTemplate);
			
			NotificationChannelModel emailChannelObject = notificationChannelDao.findByIdAndIsDeleted(61, false);

			TemplateModel emailDefaultTemplate1 = new TemplateModel();
			emailDefaultTemplate1.setName("Default Email Template 1");
			emailDefaultTemplate1.setToAddress("{{applicantEmailID}}");
			emailDefaultTemplate1.setNotificationChannelModel(emailChannelObject);
			emailDefaultTemplate1.setSubject("Dummy mail for Mr. {{applicantLastName}}");
			
			emailDefaultTemplate1.setBody(
					"<html><body> Hi {{applicantFirstName}}. <br> This is your dummy email template 1.<body> <html>");
			List<String> x = Arrays.asList("applicantFirstName", "applicantEmailID", "applicantLastName");
			emailDefaultTemplate1.setVariables(x);
			emailDefaultTemplate1 = templateDao.save(emailDefaultTemplate1);

//			TemplateModel emailDefaultTemplate2 = new TemplateModel();
//			emailDefaultTemplate2.setName("Default Email Template 2");
////			emailDefaultTemplate2.setTo("{{applicantEmailID}}");
//			emailDefaultTemplate2.setNotificationChannelModel(emailNotificationChannelObject);
//			emailDefaultTemplate2.setSubject("Dummy mail for Mr. {{applicantLastName}}");
//			emailDefaultTemplate2.setBody(
//					"<html><body> Hi {{applicantFirstName}}. <br> This is your dummy email template 2.<body> <html>");
//			emailDefaultTemplate2 = templateDao.save(emailDefaultTemplate2);
//
//			TemplateModel pushNotiDefaultTemplate = new TemplateModel();
//			pushNotiDefaultTemplate.setName("Default Push Notification Template");
////			pushNotiDefaultTemplate.setTo("{{applicantId}}");
//			JSONObject obj = new JSONObject();
//			obj.append("deviceType", "android");
//			pushNotiDefaultTemplate.setToMetadata(obj.toString());
//			pushNotiDefaultTemplate.setSubject("Dummy mail for Mr. {{applicantLastName}}");
//			pushNotiDefaultTemplate.setNotificationChannelModel(pushNotificationChannelObject);
//			pushNotiDefaultTemplate.setBody("Hi {{applicantFirstName}}. <br> This is your dummy push notification.");
//			pushNotiDefaultTemplate = templateDao.save(pushNotiDefaultTemplate);

			apiResponse = new ApiResponse(HttpStatus.OK, "Success");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in initiate");
		}
		return apiResponse;
	}

	public ApiResponse sendNotificatio() throws Exception {
		ApiResponse apiResponse;
		try {

			MimeMessage msg = javaMailSender.createMimeMessage();

			// true = multipart message
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);

			helper.setTo("shrey.bhasin0695@gmail.com");

			helper.setSubject("Testing from Spring Boot");

			// default = text/plain
			// helper.setText("Check attachment for image!");

			helper.setText("<h1>Check attachment for image!</h1>", true);

			// hard coded a file path
			// FileSystemResource file = new FileSystemResource(new
			// File("path/android.png"));

			// helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

			javaMailSender.send(msg);

			apiResponse = new ApiResponse(HttpStatus.OK, "Successfully sent email.");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in initiate");
		}
		return apiResponse;
	}

	public ApiResponse sendNotification() throws Exception {
		ApiResponse apiResponse;
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
			Calendar currentTime = Calendar.getInstance();
			Date sendTime = simpleDateFormat.parse(CommonHelperFunctions.getStringValue(new Integer(currentTime.get(Calendar.HOUR_OF_DAY) - 1)) + ":00:00");
			List<NotificationModel> notificationObjects = notificationDao.findByIsSyncAndStatusAndSendTime(false, null, sendTime);
			for (NotificationModel notificationObject : notificationObjects) {
				notificationProducerJMS.sendNotification(notificationObject);
			}
			apiResponse = new ApiResponse(HttpStatus.OK, "Send Notification @ ", sendTime.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in initiate");
		}
		return apiResponse;
	}

}
