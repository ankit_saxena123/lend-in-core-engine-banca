package com.kuliza.lending.notifications.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderData;
import com.kuliza.lending.notifications.pojo.ConfigServiceProvidersListData;
import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

@Entity
@Table(name = "config_channel")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class NotificationChannelModel extends BaseModel {

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "moduleId", nullable = false)
	private NotificationModuleModel notificationModule;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ChannelTypes channelType;

	@Column(nullable = false)
	@Temporal(TemporalType.TIME)
	private Date startTime;

	@Column(nullable = false)
	@Temporal(TemporalType.TIME)
	private Date endTime;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = ListOfServiceProviderConverter.class)
	private List<ConfigServiceProviderData> serviceProviders;

	public NotificationChannelModel() {
		super();
		this.setIsDeleted(false);
		// TODO Auto-generated constructor stub
	}

	public NotificationChannelModel(NotificationModuleModel notificationModule, ChannelTypes channelType, Date startTime,
			Date endTime, List<ConfigServiceProviderData> serviceProviders) {
		super();
		this.notificationModule = notificationModule;
		this.channelType = channelType;
		this.startTime = startTime;
		this.endTime = endTime;
		this.serviceProviders = serviceProviders;
		this.setIsDeleted(false);
	}

	public NotificationModuleModel getNotificationModule() {
		return notificationModule;
	}

	public void setNotificationModule(NotificationModuleModel notificationModule) {
		this.notificationModule = notificationModule;
	}

	public ChannelTypes getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelTypes channelType) {
		this.channelType = channelType;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public List<ConfigServiceProviderData> getServiceProviders() {
		return serviceProviders;
	}

	public void setServiceProviders(List<ConfigServiceProviderData> serviceProviders) {
		this.serviceProviders = serviceProviders;
	}

}
