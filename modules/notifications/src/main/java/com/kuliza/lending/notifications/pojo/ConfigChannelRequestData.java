package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.notifications.utils.NotificationConstants.ChannelTypes;

public class ConfigChannelRequestData {

	Long id;

	@NotNull(message = "Start Time cannot be null.")
	Date startTime;

	@NotNull(message = "End Time cannot be null.")
	Date endTime;

	@NotNull(message = "Service Providers list cannot be null")
	@Size(min = 1)
	List<ConfigServiceProviderData> serviceProviders;

	@NotNull(message = "Channel type cannot be null.")
	@NotEmpty(message = "Channel type cannot be empty.")
	@Enumerated(EnumType.STRING)
	ChannelTypes channelType;

	public ConfigChannelRequestData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigChannelRequestData(Long id, Date startTime, Date endTime,
			List<ConfigServiceProviderData> serviceProviders, ChannelTypes channelType) {
		super();
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.serviceProviders = serviceProviders;
		this.channelType = channelType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public List<ConfigServiceProviderData> getServiceProviders() {
		return serviceProviders;
	}

	public void setServiceProviders(List<ConfigServiceProviderData> serviceProviders) {
		this.serviceProviders = serviceProviders;
	}

	public ChannelTypes getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelTypes channelType) {
		this.channelType = channelType;
	}

}
