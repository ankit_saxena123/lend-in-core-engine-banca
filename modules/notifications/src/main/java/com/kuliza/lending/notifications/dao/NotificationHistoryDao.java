package com.kuliza.lending.notifications.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationHistoryModel;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;

@Transactional
public interface NotificationHistoryDao extends CrudRepository<NotificationHistoryModel, Long> {

	public NotificationHistoryModel findById(String uuid);
	
	public List<NotificationHistoryModel> findByIsSyncAndStatusAndSendTime(boolean isSync, String status, Date sendTime);
	
	public List<NotificationHistoryModel> findByIsSyncAndStatus(boolean isSync, String status);

}
