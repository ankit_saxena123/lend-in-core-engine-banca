package com.kuliza.lending.notifications.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;

public class ConfigChannelListData {

	@NotNull(message = "Channels list cannot be null")
	@Size(min = 1)
	List<ConfigChannelRequestData> channels;
	
	@NotNull(message = "Module Id cannot be null")
	public long moduleId;

	public ConfigChannelListData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigChannelListData(List<ConfigChannelRequestData> channels, long moduleId) {
		super();
		this.channels = channels;
		this.moduleId = moduleId;
	}

	public List<ConfigChannelRequestData> getChannels() {
		return channels;
	}

	public void setChannels(List<ConfigChannelRequestData> channels) {
		this.channels = channels;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	
}
