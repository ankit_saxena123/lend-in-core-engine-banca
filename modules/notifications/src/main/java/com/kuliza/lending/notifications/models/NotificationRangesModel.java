package com.kuliza.lending.notifications.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;
import com.kuliza.lending.notifications.utils.NotificationConstants.VariableTypes;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "notification_variable_ranges")
public class NotificationRangesModel extends BaseModel{

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "moduleId", nullable = false)
	private NotificationModuleModel notificationModule;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "variableId", nullable = false)
	private NotificationVariablesModel notificationVariablesModel;
	
	@Column(nullable = true)
	private Float minValue;
	
	@Column(nullable = true)
	private Float maxValue;

	@Column(columnDefinition = "LONGTEXT", nullable = true)
	@Convert(converter = ListConverter.class)
	public List<String> valuesList;
	
	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private Boolean isMandatory;

	public NotificationRangesModel() {
		super();
		this.setIsMandatory(false);
		this.setIsDeleted(false);
		// TODO Auto-generated constructor stub
	}

	public NotificationRangesModel(NotificationModuleModel notificationModule,
			NotificationVariablesModel notificationVariablesModel, float minValue, float maxValue, List<String> valuesList,
			Boolean isMandatory) {
		super();
		this.notificationModule = notificationModule;
		this.notificationVariablesModel = notificationVariablesModel;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.valuesList = valuesList;
		this.setIsMandatory(false);
		this.setIsDeleted(false);
	}

	public NotificationModuleModel getNotificationModule() {
		return notificationModule;
	}

	public void setNotificationModule(NotificationModuleModel notificationModule) {
		this.notificationModule = notificationModule;
	}

	public NotificationVariablesModel getNotificationVariablesModel() {
		return notificationVariablesModel;
	}

	public void setNotificationVariablesModel(NotificationVariablesModel notificationVariablesModel) {
		this.notificationVariablesModel = notificationVariablesModel;
	}

	public Float getMinValue() {
		return minValue;
	}

	public void setMinValue(Float minValue) {
		this.minValue = minValue;
	}

	public Float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Float maxValue) {
		this.maxValue = maxValue;
	}

	public List<String> getValuesList() {
		return valuesList;
	}

	public void setValuesList(List<String> valuesList) {
		this.valuesList = valuesList;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	
}
