package com.kuliza.lending.notifications.models;

import java.io.IOException;
import java.util.Map;

import javax.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HashMapConverter implements AttributeConverter<Map<String, Object>, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(HashMapConverter.class);

	@Override
	public String convertToDatabaseColumn(Map<String, Object> customerInfo) {
		ObjectMapper objectMapper = new ObjectMapper();
		String customerInfoJson = null;
		try {
			if (customerInfo != null)
				customerInfoJson = objectMapper.writeValueAsString(customerInfo);
		} catch (final JsonProcessingException e) {
			LOGGER.error("JSON writing error", e);
		}

		return customerInfoJson;
	}

	@Override
	public Map<String, Object> convertToEntityAttribute(String customerInfoJSON) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> customerInfo = null;
		try {
			if (customerInfoJSON != null)
				customerInfo = objectMapper.readValue(customerInfoJSON, new TypeReference<Map<String, Object>>() {
			});
		} catch (final IOException e) {
			LOGGER.error("JSON reading error", e);
		}

		return customerInfo;
	}

}
