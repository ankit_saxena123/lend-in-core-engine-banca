package com.kuliza.lending.notifications.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

@Transactional
public interface NotificationModuleDao extends CrudRepository<NotificationModuleModel, Long> {

	public NotificationModuleModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public NotificationModuleModel findByIdAndStatusAndIsDeleted(long id, ModuleStatus status, boolean isDeleted);
	
	public NotificationModuleModel findByIdAndAssigneeAndIsDeleted(long id, String assignee, boolean isDeleted);
	
	public NotificationModuleModel findByIdAndAssigneeAndStatusAndIsDeleted(long id, String assignee, ModuleStatus status, boolean isDeleted);

	public List<NotificationModuleModel> findByAssigneeAndIsDeleted(String assignee, boolean isDeleted);
	
	public NotificationModuleModel findByNameAndSystemAndIsDeleted(String name, Systems systems, boolean isDeleted);

}
