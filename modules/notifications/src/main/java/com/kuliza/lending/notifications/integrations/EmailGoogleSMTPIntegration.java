package com.kuliza.lending.notifications.integrations;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.notifications.NotificationIBConfig;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.NotificationHistoryDao;
import com.kuliza.lending.notifications.dao.ServiceProviderDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationHistoryModel;
import com.kuliza.lending.notifications.models.NotificationModel;
import com.kuliza.lending.notifications.models.ServiceProviderModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderData;
import com.kuliza.lending.notifications.pojo.NotificationProviderResponseData;
import com.kuliza.lending.notifications.utils.NotificationHelperFunction;
import com.kuliza.lending.notifications.utils.NotificationConstants.NotificationResponses;

@Service
public class EmailGoogleSMTPIntegration implements IBBaseIntegration {

	@Autowired
	private NotificationIBConfig notificationIBConfig;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	@Autowired
	private NotificationHistoryDao notificationHistoryDao;
	
	@Autowired
	private NotificationDao notificationDao;
	
	@Autowired
	private TemplateDao templateDao;
	

	@Autowired
	private ObjectMapper objectMapper;
	

	public NotificationModel createSyncNotificationObject(String sourceId, Long templateId, Map<String, Object> caseVariables) throws Exception {
		NotificationModel notificationObject = new NotificationModel();
		Date today = new Date();
		TemplateModel templateObject = templateDao.findByIdAndIsDeleted(templateId, false);

		caseVariables = templateObject.getVariables().stream().filter(caseVariables::containsKey)
				.collect(Collectors.toMap(Function.identity(), caseVariables::get));
		notificationObject.setSourceId(sourceId);
		notificationObject.setSendTime(today);
		notificationObject.setSendDate(today);
		notificationObject.setTemplateModel(templateObject);
		notificationObject.setUserAttributes(caseVariables);
		notificationObject.setIsSync(true);
		notificationObject = notificationDao.save(notificationObject);
		NotificationHistoryModel notificationHistoryModel = new NotificationHistoryModel(notificationObject);
		notificationHistoryDao.save(notificationHistoryModel);	
		return notificationObject;
	}

	
	public Map<String, String> buildRequestPayload(NotificationModel notificationObject)
			throws Exception {
		Map<String, String> payload = new HashMap<String, String>();
		payload.put("qp#username", notificationIBConfig.getValueFirstUsername());
		payload.put("qp#password", notificationIBConfig.getValueFirstPassword());
		payload.put("qp#to",
				NotificationHelperFunction.substituteVariabes(notificationObject.getUserAttributes(), notificationObject.getTemplateModel().getToAddress()));
		payload.put("qp#from", notificationIBConfig.getValueFirstFrom());
		payload.put("qp#text",
				NotificationHelperFunction.substituteVariabes(notificationObject.getUserAttributes(), notificationObject.getTemplateModel().getBody()));
		return payload;
	};

	public ResponseEntity<String> hitIB(Map<String, String> payload,
			NotificationChannelModel channelObject) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		String ibEndpoint = NotificationHelperFunction.getIBEndpoint(notificationIBConfig.getProtocol(),
				notificationIBConfig.getHost(), notificationIBConfig.getPort(), notificationIBConfig.getSubURL());
		if (channelObject != null && channelObject.getServiceProviders() != null
				&& !channelObject.getServiceProviders().isEmpty()) {
			ConfigServiceProviderData primaryServiceProvider = channelObject.getServiceProviders().get(0);
			ServiceProviderModel primaryProviderObj = serviceProviderDao
					.findByIdAndIsDeleted(primaryServiceProvider.getProviderId(), false);
			if (primaryProviderObj != null) {
				response = restTemplate.postForEntity(
						ibEndpoint + primaryProviderObj.getIbEndpoint() + notificationIBConfig.getHash(), payload,
						String.class);
			}
		}
		return response;
	}

	// Here you have to return the Map which needs to be stored in process
	// variables.
	public void parseResponse(ResponseEntity<String> responseFromIB, NotificationModel notificationObject) throws Exception {
		HashMap<String, Object> responseMap = CommonHelperFunctions.getHashMapFromJsonString(responseFromIB.getBody());
		NotificationHistoryModel historyObject = notificationHistoryDao.findById(notificationObject.getId());
		NotificationProviderResponseData providorResponse = new NotificationProviderResponseData(
				CommonHelperFunctions.getIntegerValue(responseMap.get("sp#statusCode")),
				CommonHelperFunctions.getStringValue(responseMap.getOrDefault("sp#statusMessage", "")),
				CommonHelperFunctions.getStringValue(responseMap.getOrDefault("requestId", "")),
				objectMapper.writeValueAsString(responseMap.get("sp#dataToSave")));
		if (responseFromIB.getStatusCode().equals(HttpStatus.OK) 
				&& CommonHelperFunctions.getStringValue(responseMap.get("status")).equals("200")
				&& CommonHelperFunctions.getStringValue(responseMap.get("sp#statusCode")).equals("200")) {
			notificationObject.setStatus(NotificationResponses.SUCCESS);
			historyObject.setStatus(NotificationResponses.SUCCESS);
			historyObject.setResponse(providorResponse);
			historyObject.setSentTime(new Date());
		} else {
			notificationObject.setStatus(NotificationResponses.FAILURE);
			historyObject.setStatus(NotificationResponses.FAILURE);
			historyObject.setResponse(providorResponse);
			historyObject.setSentTime(new Date());
		}
		notificationDao.save(notificationObject);
		notificationHistoryDao.save(historyObject);
	}
}
