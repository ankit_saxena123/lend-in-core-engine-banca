package com.kuliza.lending.notifications.pojo;

import java.util.Map;

import javax.validation.constraints.NotNull;

public class NotificationData {

	@NotNull(message = "Case Instance id cannot be null.")
	String caseInstanceId;

	@NotNull(message = "Notification template Info cannot be null.")
	Map<String, Object> notificationConfig;
	
	@NotNull(message = "Case varables cannot be null.")
	Map<String, Object> caseVariables;

	public NotificationData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotificationData(String caseInstanceId, Map<String, Object> notificationConfig,
			Map<String, Object> caseVariables) {
		super();
		this.caseInstanceId = caseInstanceId;
		this.notificationConfig = notificationConfig;
		this.caseVariables = caseVariables;
	}

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public Map<String, Object> getNotificationConfig() {
		return notificationConfig;
	}

	public void setNotificationConfig(Map<String, Object> notificationConfig) {
		this.notificationConfig = notificationConfig;
	}
	
	public Map<String, Object> getCaseVariables() {
		return caseVariables;
	}

	public void setCaseVariables(Map<String, Object> caseVariables) {
		this.caseVariables = caseVariables;
	}

	@Override
	public String toString() {
		return "Notification Data Set: { caseInstanceId = " + caseInstanceId + ", template = " + notificationConfig.toString();
	}
	
}
