package com.kuliza.lending.notifications.controllers;

import java.util.Map;

import javax.validation.Valid;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.notifications.pojo.EmailRequestData;
import com.kuliza.lending.notifications.pojo.NotificationData;
import com.kuliza.lending.notifications.pojo.SystemNotificationRequestData;
import com.kuliza.lending.notifications.services.EmailServiceTask;
import com.kuliza.lending.notifications.services.NotificationService;
import com.kuliza.lending.notifications.services.SMSServiceTask;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(NotificationConstants.NOTIFICATION_CONTROLLER_ENDPOINT)
public class NotificationController {

	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private SMSServiceTask smsServiceTask;
	
	@Autowired
	private EmailServiceTask emailServiceTask;

	@PostMapping(NotificationConstants.CREATE_NOTIFICATION_API_ENDPOINT)
	public ApiResponse createNotification(@Valid @RequestBody SystemNotificationRequestData request) throws Exception {
		return notificationService.createNotification(request);
	}
	
	@PostMapping(NotificationConstants.SEND_NOTIFICATION_API_ENDPOINT)
	public ApiResponse sendNotificaion(@Valid @RequestBody String request) throws Exception {
		return notificationService.sendNotification();
	}
	
	
	@PostMapping(NotificationConstants.CREATE_DUMMY_API_ENDPOINT)
	public Object createDummyData(@Valid @RequestBody String request) throws Exception {
		try {
		return notificationService.createDummy();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}
	
	@PostMapping(NotificationConstants.TRIGGER_SMS_API_ENDPOINT)
	public Object triggerSMS(@Valid @RequestBody Map<String, Object> request) throws Exception {
		try {
		return smsServiceTask.sendViaValueFirst(CommonHelperFunctions.getStringValue(request.get("sourceId")),
				CommonHelperFunctions.getLongValue(request.get("templateId")), CommonHelperFunctions.getHashMapFromJsonString(
						CommonHelperFunctions.getStringValue(request.get("variables"))));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}
	
	@PostMapping(NotificationConstants.TRIGGER_EMAIL_API_ENDPOINT)
	public Object triggerEmail(@Valid @RequestBody Map<String, Object> request) throws Exception {
		try {
			return emailServiceTask.sendViaGoogleSMTP(CommonHelperFunctions.getStringValue(request.get("sourceId")),
				CommonHelperFunctions.getLongValue(request.get("templateId")), CommonHelperFunctions.getHashMapFromJsonString(
						CommonHelperFunctions.getStringValue(request.get("variables"))));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}
	
	@PostMapping(NotificationConstants.SEND_EMAIL_API_ENDPOINT)
	public Object sendEmail(@Valid @RequestBody EmailRequestData emailObject) throws Exception {
		try {
		return notificationService.sendEmail(emailObject.getTo(), emailObject.getSubject(), emailObject.getBody(), emailObject.getDocIds());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}


}
