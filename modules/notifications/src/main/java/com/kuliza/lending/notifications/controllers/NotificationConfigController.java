package com.kuliza.lending.notifications.controllers;

import java.security.Principal;

import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.notifications.pojo.ConfigChannelListData;
import com.kuliza.lending.notifications.pojo.ConfigModuleRequestData;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderInputData;
import com.kuliza.lending.notifications.pojo.DraftOrPublishNotificationTable;
import com.kuliza.lending.notifications.pojo.NotificationTableRequestData;
import com.kuliza.lending.notifications.pojo.RangesListRequestData;
import com.kuliza.lending.notifications.pojo.VariableListRequestData;
import com.kuliza.lending.notifications.pojo.VariablesRequestData;
import com.kuliza.lending.notifications.services.NotificationConfigService;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(NotificationConstants.NOTIFICATION_CONFIG_CONTROLLER_ENDPOINT)
public class NotificationConfigController {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private NotificationConfigService notificationConfigService;

	@PostMapping(NotificationConstants.CONFIG_MODULES_API_ENDPOINT)
	public ApiResponse addModule(Principal principal,
			@Valid @RequestBody ConfigModuleRequestData configModuleRequestData) throws Exception {
		return notificationConfigService.createOrUpdatModule(principal.getName(), configModuleRequestData);
	}

	@GetMapping(NotificationConstants.CONFIG_MODULES_API_ENDPOINT)
	public ApiResponse getModule(Principal principal) throws Exception {
		return notificationConfigService.getModule(principal.getName());
	}

	@DeleteMapping(NotificationConstants.CONFIG_MODULES_API_ENDPOINT)
	public ApiResponse deleteModule(Principal principal, @RequestParam(required = true, value = "id") long id)
			throws Exception {
		return notificationConfigService.deleteModule(principal.getName(), id);
	}

	@GetMapping(NotificationConstants.CONFIG_SYSTEM_LIST_API_ENDPOINT)
	public ApiResponse getSystems() throws Exception {
		return notificationConfigService.getSystems();
	}
	
	@GetMapping(NotificationConstants.CONFIG_VARIABLE_TYPES_LIST_API_ENDPOINT)
	public ApiResponse getVariableTypes() throws Exception {
		return notificationConfigService.getVariableTypes();
	}

	@PostMapping(NotificationConstants.CONFIG_SERVICE_PROVIDERS_API_ENDPOINT)
	public ApiResponse addServiceProvider(@Valid @RequestBody ConfigServiceProviderInputData serviceProviderData)
			throws Exception {
		return notificationConfigService.createOrUpdateServiceProviders(serviceProviderData);
	}

	@GetMapping(NotificationConstants.CONFIG_SERVICE_PROVIDERS_API_ENDPOINT)
	public ApiResponse getServiceProviders() throws Exception {
		return notificationConfigService.getServiceProviders();
	}

	@DeleteMapping(NotificationConstants.CONFIG_SERVICE_PROVIDERS_API_ENDPOINT)
	public ApiResponse deleteServiceProvider(@RequestParam(required = true, value = "id") long id) throws Exception {
		return notificationConfigService.deleteServiceProvider(id);
	}

	@PutMapping(NotificationConstants.CONFIG_EDIT_MODULES_API_ENDPOINT)
	public ApiResponse editNotificationConfig(Principal principal,
			@RequestParam(required = true, value = "id") Long moduleId) throws Exception {
		return notificationConfigService.editNotificationConfig(principal.getName(), moduleId);
	}

	@PostMapping(NotificationConstants.CONFIG_STEPPER_API_ENDPOINT)
	public ApiResponse configStepperPost(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@Valid @RequestBody String request) throws Exception {
		switch (stepper) {
		case 1:
			ConfigChannelListData channels = objectMapper.readValue(request, ConfigChannelListData.class);
			return notificationConfigService.createOrUpdateChannels(principal.getName(), channels);
		case 2:
			VariableListRequestData variables = objectMapper.readValue(request, VariableListRequestData.class);
			return notificationConfigService.createOrUpdateVariables(principal.getName(), variables);
		case 3:
			RangesListRequestData rangeList = objectMapper.readValue(request, RangesListRequestData.class);
			return notificationConfigService.createOrUpdateRanges(principal.getName(), rangeList);
		case 4:
			NotificationTableRequestData notificationtableRequest = objectMapper.readValue(request,
					NotificationTableRequestData.class);
			return notificationConfigService.createOrUpdateNotificationTable(principal.getName(),
					notificationtableRequest);
		case 5:
			DraftOrPublishNotificationTable draftOrPublishNotificationTable = objectMapper.readValue(request,
					DraftOrPublishNotificationTable.class);
			return notificationConfigService.draftOrPublishNotificationTable(principal.getName(),
					draftOrPublishNotificationTable);
		default:
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed.");
		}
	}

	@GetMapping(NotificationConstants.CONFIG_STEPPER_API_ENDPOINT)
	public ApiResponse configStepperGet(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@RequestParam(required = false, value = "id") Long id,
			@RequestParam(required = false, value = "system") Systems system) throws Exception {
		switch (stepper) {
		case 1:
			return notificationConfigService.getChannels(principal.getName(), id);
		case 2:
			return notificationConfigService.getGlobalVariables(system);
		case 3:
			return notificationConfigService.getVariableRanges(principal.getName(), id);
		case 4:
			return notificationConfigService.getNotificationTableLogic(principal.getName(), id);
		case 5:
			return notificationConfigService.getNotificationTableLogic(principal.getName(), id);
		default:
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed.");
		}
	}

	@DeleteMapping(NotificationConstants.CONFIG_STEPPER_API_ENDPOINT)
	public ApiResponse configStepperDelete(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@RequestParam(required = true, value = "id") Long id) throws Exception {
		switch (stepper) {
		case 1:
			return notificationConfigService.deleteChannel(principal.getName(), id);
		case 2:
			return notificationConfigService.deleteGlobalVariables(id);
		case 3:
			return notificationConfigService.deleteVariableRange(principal.getName(), id);
		case 4:
			return notificationConfigService.deleteNotificationTable(principal.getName(), id);
		default:
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed.");
		}
	}

	@PutMapping(NotificationConstants.CONFIG_BACK_STEPPER_API_ENDPOINT)
	public ApiResponse configStepperGet(Principal principal, @PathVariable(name = "stepper") @NotEmpty int stepper,
			@RequestParam(required = false, value = "id") Long id) throws Exception {
		switch (stepper) {
		case 2:
			return notificationConfigService.decrementStep(principal.getName(), id, stepper);
		case 3:
			notificationConfigService.deleteAllVariablesRanges(principal.getName(), id);
			return notificationConfigService.decrementStep(principal.getName(), id, stepper);
		case 4:
			notificationConfigService.deleteNotificationTable(principal.getName(), id);
			return notificationConfigService.decrementStep(principal.getName(), id, stepper);
		case 5:
			return notificationConfigService.decrementStep(principal.getName(), id, stepper);
		default:
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"Stepper " + stepper + "for the given request type not allowed.");
		}
	}

}
