package com.kuliza.lending.notifications.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.decision_table.models.DecisionTableModel;
import com.kuliza.lending.decision_table.pojo.DecisionTableDefinitionRepresentation;
import com.kuliza.lending.decision_table.pojo.DecisionTableModelRepresentation;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import com.kuliza.lending.notifications.dao.NotificationChannelDao;
import com.kuliza.lending.notifications.dao.NotificationDao;
import com.kuliza.lending.notifications.dao.NotificationModuleDao;
import com.kuliza.lending.notifications.dao.NotificationRangesDao;
import com.kuliza.lending.notifications.dao.NotificationVariablesDao;
import com.kuliza.lending.notifications.dao.ServiceProviderDao;
import com.kuliza.lending.notifications.dao.TemplateDao;
import com.kuliza.lending.notifications.models.NotificationChannelModel;
import com.kuliza.lending.notifications.models.NotificationModuleModel;
import com.kuliza.lending.notifications.models.NotificationRangesModel;
import com.kuliza.lending.notifications.models.NotificationVariablesModel;
import com.kuliza.lending.notifications.models.ServiceProviderModel;
import com.kuliza.lending.notifications.models.TemplateModel;
import com.kuliza.lending.notifications.pojo.ConfigChannelListData;
import com.kuliza.lending.notifications.pojo.ConfigChannelRequestData;
import com.kuliza.lending.notifications.pojo.ConfigModuleRequestData;
import com.kuliza.lending.notifications.pojo.ConfigServiceProviderInputData;
import com.kuliza.lending.notifications.pojo.DraftOrPublishNotificationTable;
import com.kuliza.lending.notifications.pojo.NotificationLogicTableData;
import com.kuliza.lending.notifications.pojo.NotificationTableRequestData;
import com.kuliza.lending.notifications.pojo.RangesListRequestData;
import com.kuliza.lending.notifications.pojo.RangesRequestData;
import com.kuliza.lending.notifications.pojo.VariableListRequestData;
import com.kuliza.lending.notifications.pojo.VariablesRequestData;
import com.kuliza.lending.notifications.utils.NotificationConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.notifications.utils.NotificationConstants.Systems;
import com.kuliza.lending.notifications.utils.NotificationConstants.VariableTypes;

@Service
public class NotificationConfigService {

	@Autowired
	private NotificationModuleDao notificationModuleDao;

	@Autowired
	private NotificationChannelDao notificationChannelDao;

	@Autowired
	private NotificationRangesDao notificationRangesDao;

	@Autowired
	private NotificationVariablesDao notificationVariablesDao;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	@Autowired
	NotificationDao notificationDao;
	
	@Autowired
	private TemplateDao templateDao;

	@Autowired
	private DecisionTableService decisionTableService;

	public ApiResponse getSystems() throws Exception {
		ApiResponse response;
		try {
			List<String> systems = Stream.of(Systems.values()).map(Enum::name).collect(Collectors.toList());
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, systems);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}
	
	public ApiResponse getVariableTypes() throws Exception {
		ApiResponse response;
		try {
			List<String> variableTypes = Stream.of(VariableTypes.values()).map(Enum::name).collect(Collectors.toList());
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, variableTypes);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse createOrUpdatModule(String assignee, ConfigModuleRequestData configModuleRequestData)
			throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModuleObject = null;
			if (configModuleRequestData.getId() != null) {
				notificationModuleObject = notificationModuleDao
						.findByIdAndAssigneeAndIsDeleted(configModuleRequestData.getId().longValue(), assignee, false);
			} else {
				notificationModuleObject = notificationModuleDao
						.findByNameAndSystemAndIsDeleted(configModuleRequestData.getGroupName(), configModuleRequestData.getSystem(), false);
				if (notificationModuleObject == null)
					notificationModuleObject = new NotificationModuleModel();
				else {
					apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
							configModuleRequestData.getGroupName() + " module for ths system alreay exists.");
					return apiResponse;
				}
			}
			if (notificationModuleObject != null) {
				notificationModuleObject.setName(configModuleRequestData.getGroupName());
				notificationModuleObject.setSystem(configModuleRequestData.getSystem());
				notificationModuleObject.setDescription(configModuleRequestData.getDescription());
				notificationModuleObject.setStatus(ModuleStatus.CREATED);
				notificationModuleObject.setAssignee(assignee);
				notificationModuleObject = notificationModuleDao.save(notificationModuleObject);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationModuleObject);
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"Notification module object is null.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}

	public ApiResponse getModule(String assignee) throws Exception {
		ApiResponse response;
		try {
			List<NotificationModuleModel> notificationModuleObjects = notificationModuleDao
					.findByAssigneeAndIsDeleted(assignee, false);
			if (notificationModuleObjects != null && !notificationModuleObjects.isEmpty()) {
				List<ConfigModuleRequestData> notificationModules = new ArrayList<ConfigModuleRequestData>();
				for (NotificationModuleModel notificationObject : notificationModuleObjects) {
					ConfigModuleRequestData notificationModule = new ConfigModuleRequestData();
					notificationModule.setGroupName(notificationObject.getName());
					notificationModule.setDescription(notificationObject.getDescription());
					notificationModule.setSystem(notificationObject.getSystem());
					notificationModule.setId(notificationObject.getId());
					notificationModule.setModified( notificationObject.getModified() != null ? 
							CommonHelperFunctions.getDateInFormat(notificationObject.getModified(), NotificationConstants.MODULE_DATE_FORMAT) : "");
					notificationModule.setAssignee(notificationObject.getAssignee());
					notificationModule.setStatus(notificationObject.getStatus());
					notificationModule.setStep(notificationObject.getStep());
					notificationModules.add(notificationModule);
				}
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationModules);
			} else {
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationModuleObjects);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse deleteModule(String assignee, long id) throws Exception {
		ApiResponse response;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndAssigneeAndIsDeleted(id,
					assignee, false);
			if (notificationModule != null) {
				notificationModule.setIsDeleted(true);
				notificationModule = notificationModuleDao.save(notificationModule);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Module deleted succesfully.");
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No notification module found for given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse createOrUpdateServiceProviders(ConfigServiceProviderInputData serviceProviderData)
			throws Exception {
		ApiResponse apiResponse;
		try {
			ServiceProviderModel serviceProvider = null;
			if (serviceProviderData.getId() != null) {
				serviceProvider = serviceProviderDao.findByIdAndIsDeleted(serviceProviderData.getId(), false);
			} else {
				serviceProvider = new ServiceProviderModel();
			}
			if (serviceProvider != null) {
				serviceProvider.setFromAddress(serviceProviderData.getFromAddress());
				serviceProvider.setIbEndpoint(serviceProviderData.getIbEndpoint());
				serviceProvider.setName(serviceProviderData.getName());
				serviceProvider = serviceProviderDao.save(serviceProvider);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, serviceProvider);
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"Service Provider object is null.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}

	public ApiResponse getServiceProviders() throws Exception {
		ApiResponse response;
		try {
			List<ServiceProviderModel> serviceProviders = serviceProviderDao.findByIsDeleted(false);
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, serviceProviders);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse deleteServiceProvider(long id) throws Exception {
		ApiResponse response;
		try {
			ServiceProviderModel serviceProvider = serviceProviderDao.findByIdAndIsDeleted(id, false);
			if (serviceProvider != null) {
				serviceProvider.setIsDeleted(true);
				serviceProvider = serviceProviderDao.save(serviceProvider);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						"Service Provider deleted succesfully.");
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No service provider found for given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse createOrUpdateChannels(String assignee, ConfigChannelListData channels) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModuleObject = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(channels.getModuleId(), assignee, false);
			if (notificationModuleObject != null) {
				for (ConfigChannelRequestData channel : channels.getChannels()) {
					NotificationChannelModel channelObject = null;
					
					if (channel.getId() != null)
						channelObject = notificationChannelDao.findByIdAndIsDeleted(channel.getId().longValue(), false);
					else {
						channelObject = notificationChannelDao.findByNotificationModuleAndChannelTypeAndIsDeleted(
								notificationModuleObject, channel.getChannelType(), false);
						if (channelObject == null)
							channelObject = new NotificationChannelModel();
						else {
							apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
									channel.getChannelType() + " channel already exists.");
							return apiResponse;
						}
					}
					if (channelObject != null) {
						channelObject.setChannelType(channel.getChannelType());
						channelObject.setNotificationModule(notificationModuleObject);
						channelObject.setStartTime(channel.getStartTime());
						channelObject.setEndTime(channel.getEndTime());
						channelObject.setServiceProviders(channel.getServiceProviders());
						notificationChannelDao.save(channelObject);
					} else {
						apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
								"Notification channel object is null.");
						return apiResponse;
					}
				}
				notificationModuleObject.setStep(2);
				notificationModuleObject = notificationModuleDao.save(notificationModuleObject);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationModuleObject);
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"Notification module object is null.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}

	public ApiResponse getChannels(String assignee, long moduleId) throws Exception {
		ApiResponse response;
		try {
			NotificationModuleModel notificationModuleObject = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(moduleId, assignee, false);
			if (notificationModuleObject != null) {
				List<NotificationChannelModel> notificationChannels = notificationChannelDao
						.findByNotificationModuleAndIsDeleted(notificationModuleObject, false);
				if (notificationChannels != null && !notificationChannels.isEmpty()) {
					ConfigChannelListData channelListData = new ConfigChannelListData();
					channelListData.setModuleId(moduleId);
					List<ConfigChannelRequestData> channels = new ArrayList<ConfigChannelRequestData>();
					for (NotificationChannelModel channelObject : notificationChannels) {
						ConfigChannelRequestData channel = new ConfigChannelRequestData();
						channel.setId(channelObject.getId());
						channel.setChannelType(channelObject.getChannelType());
						channel.setStartTime(channelObject.getStartTime());
						channel.setEndTime(channelObject.getEndTime());
						channel.setServiceProviders(channelObject.getServiceProviders());
						channels.add(channel);
					}
					channelListData.setChannels(channels);
					response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, channelListData);
				} else {
					response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationChannels);
				}
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No module found for the given Id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse deleteChannel(String assignee, long channelId) throws Exception {
		ApiResponse response;
		try {
			NotificationChannelModel notificationChannel = notificationChannelDao.findByIdAndIsDeleted(channelId,
					false);
			if (notificationChannel != null
					&& assignee != null && assignee.equals(notificationChannel.getNotificationModule().getAssignee())) {
				notificationChannel.setIsDeleted(true);
				notificationChannel = notificationChannelDao.save(notificationChannel);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationChannel);
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"Either the channel Id is incorrect or the user is not authorized.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse createOrUpdateVariables(String assignee, VariableListRequestData variables) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModuleObject = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(variables.getModuleId(), assignee, false);
			if (notificationModuleObject != null) {
				for (VariablesRequestData variable : variables.getVariables()) {
					NotificationVariablesModel variableObject = null;
					if (variable.getId() != null)
						variableObject = notificationVariablesDao.findByIdAndIsDeleted(variable.getId(), false);
					else {
						variableObject = notificationVariablesDao
								.findByVariableKeyAndSystemAndIsDeleted(variable.getKey(), variable.getSystem(), false);
						if (variableObject == null)
							variableObject = new NotificationVariablesModel();
						else {
							apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
									variable.getKey() + " variable already exists.");
							return apiResponse;
						}
					}
					if (variableObject != null) {
						variableObject.setVariableKey(variable.getKey().toLowerCase());
						variableObject.setVariableLabel(variable.getLabel());
						variableObject.setDescription(variable.getDescription());
						variableObject.setSystem(variable.getSystem());
						variableObject.setVariableType(variable.getVariableType());
						notificationVariablesDao.save(variableObject);
					} else {
						apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
								"Variable object is null.");
						return apiResponse;
					}
				}
				notificationModuleObject.setStep(3);
				notificationModuleObject = notificationModuleDao.save(notificationModuleObject);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Variables added successfully.");
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "Notification Module is null.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}

	public ApiResponse getGlobalVariables(Systems system) throws Exception {
		ApiResponse response;
		try {
			List<NotificationVariablesModel> notificationVariablesObjects = notificationVariablesDao
					.findBySystemAndIsDeleted(system, false);
			if (notificationVariablesObjects != null && !notificationVariablesObjects.isEmpty()) {
				List<VariablesRequestData> variables = new ArrayList<VariablesRequestData>();
				for (NotificationVariablesModel variableObject : notificationVariablesObjects) {
					VariablesRequestData variable = new VariablesRequestData(variableObject.getId(),
							variableObject.getVariableKey(), variableObject.getVariableLabel(),
							variableObject.getDescription(), variableObject.getSystem(),
							variableObject.getVariableType());
					variables.add(variable);
				}
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, variables);
			} else {
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationVariablesObjects);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse deleteGlobalVariables(Long variableId) throws Exception {
		ApiResponse response;
		try {
			NotificationVariablesModel variableObject = notificationVariablesDao.findByIdAndIsDeleted(variableId,
					false);
			if (variableObject != null) {
				variableObject.setIsDeleted(true);
				variableObject = notificationVariablesDao.save(variableObject);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, variableObject);
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No variable found for the given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse createOrUpdateRanges(String assignee, RangesListRequestData ranges) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(ranges.getModuleId(), assignee, false);
			if (notificationModule != null) {
				for (RangesRequestData range : ranges.getRanges()) {
					NotificationVariablesModel variableObject = notificationVariablesDao
							.findByIdAndIsDeleted(range.getVariableId(), false);
					if (variableObject != null) {
						NotificationRangesModel rangeObject = null;
						if (range.getId() != null)
							rangeObject = notificationRangesDao.findByIdAndIsDeleted(range.getId(), false);
						else {
							rangeObject = notificationRangesDao
									.findByNotificationModuleAndNotificationVariablesModelAndIsDeleted(
											notificationModule, variableObject, false);
							if (rangeObject == null)
								rangeObject = new NotificationRangesModel();
							else {
								apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
										range.getVariable() + " variable range already exists.");
								return apiResponse;
							}
						}
						if (rangeObject != null) {
							rangeObject.setMinValue(range.getMinValue());
							rangeObject.setMaxValue(range.getMaxValue());
							rangeObject.setValuesList(range.getValues());
							rangeObject.setNotificationModule(notificationModule);
							rangeObject.setNotificationVariablesModel(variableObject);
							if (range.isMandatory() != null)
								rangeObject.setIsMandatory(range.isMandatory());
							notificationRangesDao.save(rangeObject);
						} else {
							apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
									"Variable object is null.");
							return apiResponse;
						}
					} else {
						apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
								"No variable object found with the given id.");
						return apiResponse;
					}
				}
				notificationModule.setStep(4);
				notificationModule = notificationModuleDao.save(notificationModule);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Ranges added successfully");
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No module found with given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}

	public ApiResponse getVariableRanges(String assignee, long moduleId) throws Exception {
		ApiResponse response;
		try {
			NotificationModuleModel notificationModuleObject = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(moduleId, assignee, false);
			if (notificationModuleObject != null) {
				List<NotificationRangesModel> notificationRangeObjects = notificationRangesDao
						.findByNotificationModuleAndIsDeleted(notificationModuleObject, false);
				if (notificationRangeObjects != null && !notificationRangeObjects.isEmpty()) {
					RangesListRequestData rangesData = new RangesListRequestData();
					rangesData.setModuleId(moduleId);
					List<RangesRequestData> ranges = new ArrayList<RangesRequestData>();
					for (NotificationRangesModel rangeObject : notificationRangeObjects) {
						List<String> values = rangeObject.getValuesList();
						RangesRequestData range = new RangesRequestData(rangeObject.getId(),
								rangeObject.getNotificationVariablesModel().getId(),
								rangeObject.getNotificationVariablesModel(), rangeObject.getMinValue(),
								rangeObject.getMaxValue(), values, rangeObject.getIsMandatory());
						ranges.add(range);
					}
					rangesData.setRanges(ranges);
					response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, rangesData);
				} else {
					response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationRangeObjects);
				}
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No module found for the given Id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse deleteVariableRange(String assignee, long rangeId) throws Exception {
		ApiResponse response;
		try {
			NotificationRangesModel notificationRange = notificationRangesDao.findByIdAndIsDeleted(rangeId, false);
			if (notificationRange != null && assignee != null && assignee.equals(notificationRange.getNotificationModule().getAssignee())) {
				notificationRange.setIsDeleted(true);
				notificationRange = notificationRangesDao.save(notificationRange);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationRange);
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"Either the range Id is incorrect or the user is not authorized.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}
	
	public void deleteAllVariablesRanges(String assignee, long moduleId) throws Exception {
		try {
			NotificationModuleModel notificationModuleObject = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(moduleId, assignee, false);
			if (notificationModuleObject != null) {
				List<NotificationRangesModel> notificationRanges = notificationRangesDao
						.findByNotificationModuleAndIsDeleted(notificationModuleObject, false);
				for (NotificationRangesModel notificationRange : notificationRanges) {
					notificationRange.setIsDeleted(true);
					notificationRange = notificationRangesDao.save(notificationRange);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ApiResponse getNotificationTableLogic(String assignee, long moduleId) throws Exception {
		ApiResponse response;
		try {
			NotificationModuleModel notificationModuleObject = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(moduleId, assignee, false);
			if (notificationModuleObject != null) {
				List<NotificationRangesModel> notificationRangeObjects = notificationRangesDao
						.findByNotificationModuleAndIsDeleted(notificationModuleObject, false);
				NotificationLogicTableData notificationTable = new NotificationLogicTableData();
				notificationTable.setOutputRanges(createOutputRanges(notificationModuleObject));
				List<RangesRequestData> ranges = new ArrayList<RangesRequestData>();
				for (NotificationRangesModel rangeObject : notificationRangeObjects) {
					List<String> values = rangeObject.getValuesList();
					RangesRequestData range = new RangesRequestData(rangeObject.getId(),
							rangeObject.getNotificationVariablesModel().getId(),
							rangeObject.getNotificationVariablesModel(), rangeObject.getMinValue(),
							rangeObject.getMaxValue(), values, rangeObject.getIsMandatory());
					ranges.add(range);
				}
				notificationTable.setRanges(ranges);
				if (notificationModuleObject.getDecisionTableKey() != null
						&& !"".equals(notificationModuleObject.getDecisionTableKey())) {
					DecisionTableModelRepresentation tableRepresentation = decisionTableService
							.fetchDecisionTableDefinition(notificationModuleObject.getDecisionTableKey(), null);
					notificationTable.setDecisionTable(tableRepresentation);
					notificationTable.setIsSaved(true);
				} else {
					DecisionTableModelRepresentation tableRepresentation = new DecisionTableModelRepresentation();
					tableRepresentation.setKey(notificationModuleObject.getName().replaceAll(" ", "").toLowerCase() + "-"
							+ notificationModuleObject.getSystem().toString().toLowerCase());
					tableRepresentation.setName(notificationModuleObject.getName());
					tableRepresentation.setDescription(notificationModuleObject.getDescription());
					tableRepresentation.setNewVersion(false);
					
					DecisionTableDefinitionRepresentation decisionTableDefinition = new DecisionTableDefinitionRepresentation();
					decisionTableDefinition.setModelVersion(NotificationConstants.DMN_MODEL_ID);
					decisionTableDefinition.setDescription(notificationModuleObject.getDescription());
					decisionTableDefinition.setHitIndicator(NotificationConstants.DMN_HIT_POLICY);
					decisionTableDefinition.setName(notificationModuleObject.getName());
					decisionTableDefinition.setKey(notificationModuleObject.getName().replaceAll(" ", "").toLowerCase() + "-"
							+ notificationModuleObject.getSystem().toString().toLowerCase());
					tableRepresentation.setDecisionTableDefinition(decisionTableDefinition);
					
					notificationTable.setDecisionTable(tableRepresentation);
					notificationTable.setIsSaved(false);
					
				}
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationTable);
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No module found for the given Id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	private List<RangesRequestData> createOutputRanges(
			NotificationModuleModel notificationModuleObject) {
		List<NotificationChannelModel> channelObjects = notificationChannelDao
				.findByNotificationModuleAndIsDeleted(notificationModuleObject,
						false);
		long i = -1;
		List<RangesRequestData> ranges = new ArrayList<RangesRequestData>();
		for (NotificationChannelModel chanelObject : channelObjects) {
			
			long hours = (chanelObject.getEndTime().getTime()
					- chanelObject.getStartTime().getTime())
					/ (60 * 60 * 1000);
			List<String> stringValues = new ArrayList<String>();
			long itr = 1;
			while (itr <= hours) {
				stringValues.add(Long.toString(itr));
				itr = itr + 1;
			}
			NotificationVariablesModel variable = new NotificationVariablesModel(
					NotificationConstants.CHANNEL_KEY_MAPPING
							.get(chanelObject.getChannelType()).get(1),
							chanelObject.getChannelType().toString() + " Frequency",
							chanelObject.getNotificationModule().getSystem(), NotificationConstants.VariableTypes.STRING, null);
			RangesRequestData range = new RangesRequestData(i, null, variable, null, null, stringValues, false);
			ranges.add(range);
			i = i - 1;
			
			variable = new NotificationVariablesModel(
					NotificationConstants.CHANNEL_KEY_MAPPING
					.get(chanelObject.getChannelType()).get(0),
					chanelObject.getChannelType().toString() + " Template",
					chanelObject.getNotificationModule().getSystem(), NotificationConstants.VariableTypes.STRING, null);
			List<TemplateModel> templateObjects = templateDao.findByNotificationChannelModelAndIsDeleted(chanelObject, false);
			stringValues = new ArrayList<String>();
			for (TemplateModel templateObject : templateObjects) {
				Map<String, Object> templateMap = new HashMap<String, Object>();
				templateMap.put("name", templateObject.getName());
				templateMap.put("id", templateObject.getId());
				stringValues.add(templateMap.toString());
			}
			range = new RangesRequestData(i, null, variable, null, null, stringValues, false);
			ranges.add(range);
			i = i - 1;
		}
		return ranges;
  }

  public ApiResponse createOrUpdateNotificationTable(String assignee,
			NotificationTableRequestData notificationTableRequest) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndAssigneeAndIsDeleted(
					notificationTableRequest.getModuleId().longValue(), assignee, false);
			if (notificationModule != null) {
				DecisionTableModelRepresentation decisionTableRepresentation = notificationTableRequest
						.getDecisionTable();
				decisionTableRepresentation.setKey(notificationModule.getName().replaceAll(" ", "").toLowerCase() + "-"
						+ notificationModule.getSystem().toString().toLowerCase());
				DecisionTableModel decisionModel = decisionTableService.addDecisionTable(decisionTableRepresentation);
				notificationModule.setDecisionTableKey(decisionModel.getNameKey());
				notificationModule.setStep(5);
				notificationModule = notificationModuleDao.save(notificationModule);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						"Notification Table saved succssfully.");
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No notification module found for given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}

	public ApiResponse deleteNotificationTable(String assignee, long moduleId) throws Exception {
		ApiResponse response;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndAssigneeAndIsDeleted(moduleId,
					assignee, false);
			if (notificationModule != null) {
				decisionTableService.deleteDecisionTable(notificationModule.getDecisionTableKey(), null);
				notificationModule.setDecisionTableKey(null);
				notificationModuleDao.save(notificationModule);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						"Notification table deleted succesfully.");
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No notification module found for given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}

	public ApiResponse draftOrPublishNotificationTable(String assignee,
			DraftOrPublishNotificationTable draftOrPublishNotificationTable) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao
					.findByIdAndAssigneeAndIsDeleted(draftOrPublishNotificationTable.getModuleId(), assignee, false);
			if (notificationModule != null) {
				if (draftOrPublishNotificationTable.getModuleStatus().equals(ModuleStatus.DRAFT)) {
					notificationModule.setStatus(ModuleStatus.DRAFT);
				} else if (draftOrPublishNotificationTable.getModuleStatus().equals(ModuleStatus.PUBLISHED)) {
					decisionTableService.deployDecisionTable(notificationModule.getDecisionTableKey(), null);
				} else {
					apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
							"Status provided is not allowed.");
				}
				notificationModule.setStatus(draftOrPublishNotificationTable.getModuleStatus());
				notificationModuleDao.save(notificationModule);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						"Notification Table status saved successfully.");
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No notification module found for given id.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}
	
	public ApiResponse decrementStep(String assignee, long moduleId, Integer step) throws Exception {
		ApiResponse apiResponse;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndAssigneeAndIsDeleted(
					moduleId, assignee, false);
			if (notificationModule != null) {
				notificationModule.setStep(step - 1);
				notificationModule = notificationModuleDao.save(notificationModule);
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						notificationModule);
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "No notification module exists");
			}
		} catch (Exception e) {
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return apiResponse;
	}
	
	public ApiResponse editNotificationConfig(String assignee, long moduleId) throws Exception {
		ApiResponse response;
		try {
			NotificationModuleModel notificationModule = notificationModuleDao.findByIdAndAssigneeAndStatusAndIsDeleted(
					moduleId, assignee, ModuleStatus.PUBLISHED, false);
			if (notificationModule != null) {
				notificationModule.setStep(4);
				notificationModule.setStatus(ModuleStatus.CREATED);
				notificationModule = notificationModuleDao.save(notificationModule);

				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, notificationModule);
			} else {
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "Module not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, e.getMessage());
		}
		return response;
	}
}
