package com.kuliza.lending.notifications.service;

import static org.junit.Assert.assertEquals;

import java.security.Principal;
import java.util.*;

import com.kuliza.lending.decision_table.services.DecisionTableService;
import com.kuliza.lending.notifications.dao.NotificationChannelDao;
import com.kuliza.lending.notifications.dao.NotificationModuleDao;
import com.kuliza.lending.notifications.dao.NotificationRangesDao;
import com.kuliza.lending.notifications.dao.NotificationVariablesDao;
import com.kuliza.lending.notifications.dao.ServiceProviderDao;
import com.kuliza.lending.notifications.services.NotificationConfigService;
import com.kuliza.lending.notifications.utils.NotificationTestConstants;
import com.kuliza.lending.notifications.utils.NotificationConstants.ModuleStatus;
import com.kuliza.lending.pojo.GroupDetailWithMembers;
import com.mashape.unirest.http.HttpResponse;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.CaseInstanceQuery;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.api.FormInfo;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.TestConfiguration;

import org.springframework.test.context.ContextConfiguration;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class NotificationConfigServiceTest {

	@Spy
	@InjectMocks
	private NotificationConfigService notificationConfigService;

	@Mock
	private NotificationModuleDao notificationModuleDao;

	@Mock
	private NotificationChannelDao notificationChannelDao;

	@Mock
	private NotificationRangesDao notificationRangesDao;

	@Mock
	private NotificationVariablesDao notificationVariablesDao;

	@Mock
	private ServiceProviderDao serviceProviderDao;
	
	@Mock
	private DecisionTableService decisionTableService;

	private Principal principal = Mockito.mock(Principal.class);

	@Test
	public void getModuleTest() throws Exception {
		Mockito.when(notificationModuleDao.findByAssigneeAndIsDeleted(principal.getName(),
				NotificationTestConstants.IS_DELETED)).thenReturn(NotificationTestConstants.moduleObjectList);
		assertEquals(200, notificationConfigService.getModule(principal.getName()).getStatus());
	}

	@Test
	public void deleteModuleTest() throws Exception {
		Mockito.when(notificationModuleDao.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.MODEL_ID,
				principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
				.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService
				.deleteModule(principal.getName(), NotificationTestConstants.MODEL_ID).getStatus());
	}

	@Test
	public void createModuleTest() throws Exception {
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
				.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200,
				notificationConfigService
						.createOrUpdatModule(principal.getName(), NotificationTestConstants.configModuleRequestData)
						.getStatus());
	}

	@Test
	public void updateModuleTest() throws Exception {
		Mockito.when(notificationModuleDao.findByIdAndAssigneeAndIsDeleted(
				NotificationTestConstants.configModuleEditRequestData.getId(), principal.getName(),
				NotificationTestConstants.IS_DELETED)).thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
				.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200,
				notificationConfigService
						.createOrUpdatModule(principal.getName(), NotificationTestConstants.configModuleEditRequestData)
						.getStatus());
	}

	@Test
	public void getServiceProvidersTest() throws Exception {
		Mockito.when(serviceProviderDao.findByIsDeleted(NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.spObjectList);
		assertEquals(200, notificationConfigService.getServiceProviders().getStatus());
	}

	@Test
	public void deleteServiceProviderTest() throws Exception {
		Mockito.when(serviceProviderDao.findByIdAndIsDeleted(NotificationTestConstants.MODEL_ID,
				NotificationTestConstants.IS_DELETED)).thenReturn(NotificationTestConstants.spObject);
		Mockito.when(serviceProviderDao.save(NotificationTestConstants.spObject))
				.thenReturn(NotificationTestConstants.spObject);
		assertEquals(200,
				notificationConfigService.deleteServiceProvider(NotificationTestConstants.MODEL_ID).getStatus());
	}

	@Test
	public void createServiceProvidersTest() throws Exception {
		Mockito.when(serviceProviderDao.save(NotificationTestConstants.spObject))
				.thenReturn(NotificationTestConstants.spObject);
		assertEquals(200, notificationConfigService
				.createOrUpdateServiceProviders(NotificationTestConstants.configSPRequestData).getStatus());
	}

	@Test
	public void updateServiceProvidersTest() throws Exception {
		Mockito.when(serviceProviderDao.findByIdAndIsDeleted(NotificationTestConstants.configSPEditRequestData.getId(),
				false)).thenReturn(NotificationTestConstants.spObject);
		Mockito.when(serviceProviderDao.save(NotificationTestConstants.spObject))
				.thenReturn(NotificationTestConstants.spObject);
		assertEquals(200, notificationConfigService
				.createOrUpdateServiceProviders(NotificationTestConstants.configSPEditRequestData).getStatus());
	}

	@Test
	public void getChannelsTest() throws Exception {
		Mockito.when(notificationModuleDao.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.MODEL_ID,
				principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationChannelDao
				.findByNotificationModuleAndIsDeleted(NotificationTestConstants.moduleObject, NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.channelObjectList);
		assertEquals(200, notificationConfigService.getChannels(principal.getName(), NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
	
	@Test
	public void deleteChannelsTest() throws Exception {
		Mockito.when(notificationChannelDao.findByIdAndIsDeleted(NotificationTestConstants.MODEL_ID,
				NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.channelObject1);
		Mockito.when(notificationChannelDao.save(NotificationTestConstants.channelObject1))
				.thenReturn(NotificationTestConstants.channelObject1);
		assertEquals(200, notificationConfigService.deleteChannel(NotificationTestConstants.ASSIGNEE, NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
	
	@Test
	public void createChannelsTest() throws Exception {
		Mockito.when(notificationModuleDao.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.configChannelsRequestData.getModuleId(),
				principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationChannelDao.save(NotificationTestConstants.channelObject1))
				.thenReturn(NotificationTestConstants.channelObject1);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateChannels(principal.getName(), NotificationTestConstants.configChannelsRequestData)
				.getStatus());
	}
	
	@Test
	public void updateChannelsTest() throws Exception {
		Mockito.when(notificationModuleDao.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.configChannelsEditRequestData.getModuleId(),
				principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationChannelDao.findByIdAndIsDeleted(NotificationTestConstants.configChannelsEditRequestData.getChannels().get(0).getId(), false))
		.thenReturn(NotificationTestConstants.channelObject1);
		Mockito.when(notificationChannelDao.save(NotificationTestConstants.channelObject1))
				.thenReturn(NotificationTestConstants.channelObject1);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateChannels(principal.getName(), NotificationTestConstants.configChannelsEditRequestData)
				.getStatus());
	}
	
	@Test
	public void getGlobalVariablesTest() throws Exception {
		Mockito.when(notificationVariablesDao.findBySystemAndIsDeleted(NotificationTestConstants.SYSTEM, NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.variableObjectList);
		assertEquals(200, notificationConfigService.getGlobalVariables(NotificationTestConstants.SYSTEM).
				getStatus());
	}
	
	@Test
	public void deleteGlobalVariablesTest() throws Exception {
		Mockito.when(notificationVariablesDao.findByIdAndIsDeleted(NotificationTestConstants.MODEL_ID, NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.variableObject1);
		Mockito.when(notificationVariablesDao.save(NotificationTestConstants.variableObject1))
		.thenReturn(NotificationTestConstants.variableObject1);
		assertEquals(200, notificationConfigService.deleteGlobalVariables(NotificationTestConstants.MODEL_ID).
				getStatus());
	}
	
	@Test
	public void createVariablesTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.configVariablesRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationVariablesDao.save(NotificationTestConstants.variableObject1))
				.thenReturn(NotificationTestConstants.variableObject1);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateVariables(principal.getName(), NotificationTestConstants.configVariablesRequestData)
				.getStatus());
	}
	
	@Test
	public void updateVariablesTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.configVariablesEditRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationVariablesDao.findByIdAndIsDeleted(NotificationTestConstants.configVariablesEditRequestData.getVariables().get(1).getId(), NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.variableObject2);
		Mockito.when(notificationVariablesDao.save(NotificationTestConstants.variableObject2))
				.thenReturn(NotificationTestConstants.variableObject2);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateVariables(principal.getName(), NotificationTestConstants.configVariablesEditRequestData)
				.getStatus());
	}
	
	@Test
	public void getRangesTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.MODEL_ID, principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationRangesDao.findByNotificationModuleAndIsDeleted(NotificationTestConstants.moduleObject, NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.rangeObjectList);
		assertEquals(200, notificationConfigService.getVariableRanges(principal.getName(), NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
	
	@Test
	public void deleteRangesTest() throws Exception {
		Mockito.when(notificationRangesDao.findByIdAndIsDeleted(NotificationTestConstants.MODEL_ID, false))
				.thenReturn(NotificationTestConstants.rangeObject1);
		Mockito.when(notificationRangesDao.save(NotificationTestConstants.rangeObject1))
		.thenReturn(NotificationTestConstants.rangeObject1);
		assertEquals(200, notificationConfigService.deleteVariableRange(NotificationTestConstants.ASSIGNEE, NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
	
	@Test
	public void createRangesTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.configRangesRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationVariablesDao.findByIdAndIsDeleted(NotificationTestConstants.configRangesRequestData.getRanges().get(0).getVariableId(),
				NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.variableObject1);
		Mockito.when(notificationRangesDao.save(NotificationTestConstants.rangeObject1))
				.thenReturn(NotificationTestConstants.rangeObject1);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateRanges(principal.getName(), NotificationTestConstants.configRangesRequestData)
				.getStatus());
	}
	
	@Test
	public void updateRangesTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.configRangesEditRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationVariablesDao.findByIdAndIsDeleted(NotificationTestConstants.configRangesEditRequestData.getRanges().get(1).getVariableId(),
				NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.variableObject2);

		Mockito.when(notificationRangesDao.findByIdAndIsDeleted(NotificationTestConstants.configRangesEditRequestData.getRanges().get(1).getId(),
				NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.rangeObject2);

		Mockito.when(notificationRangesDao.save(NotificationTestConstants.rangeObject1))
				.thenReturn(NotificationTestConstants.rangeObject1);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateRanges(principal.getName(), NotificationTestConstants.configRangesEditRequestData)
				.getStatus());
	}
	
	@Test
	public void getNotificationTableTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.MODEL_ID, principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationRangesDao.findByNotificationModuleAndIsDeleted(NotificationTestConstants.moduleObject, NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.rangeObjectList);
		assertEquals(200, notificationConfigService.getNotificationTableLogic(principal.getName(), NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
	
	@Test
	public void deleteNotificationTableTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.MODEL_ID, principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(decisionTableService.deleteDecisionTable(NotificationTestConstants.moduleObject.getDecisionTableKey(), null))
		.thenReturn(NotificationTestConstants.decisionTableModel);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.deleteNotificationTable(principal.getName(), NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
	
	@Test
	public void createOrUpdateNotificationTableTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.notificationTableRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(decisionTableService.addDecisionTable(NotificationTestConstants.notificationTableRequestData.getDecisionTable()))
		.thenReturn(NotificationTestConstants.decisionTableModel);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.createOrUpdateNotificationTable(principal.getName(), NotificationTestConstants.notificationTableRequestData)
				.getStatus());
	}
	
	@Test
	public void draftNotificationTableTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.draftRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.draftOrPublishNotificationTable(principal.getName(), NotificationTestConstants.draftRequestData)
				.getStatus());
	}
	
	@Test
	public void publishNotificationTableTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.publishRequestData.getModuleId(), principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		
		Mockito.when(decisionTableService.deployDecisionTable(NotificationTestConstants.moduleObject.getDecisionTableKey(), null))
		.thenReturn(NotificationTestConstants.decisionTableModel);
		
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.draftOrPublishNotificationTable(principal.getName(), NotificationTestConstants.publishRequestData)
				.getStatus());
	}
	
	@Test
	public void decrementStepTest() throws Exception {
		Mockito.when(notificationModuleDao
				.findByIdAndAssigneeAndIsDeleted(NotificationTestConstants.MODEL_ID, principal.getName(), NotificationTestConstants.IS_DELETED))
				.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.decrementStep(principal.getName(), NotificationTestConstants.MODEL_ID, NotificationTestConstants.STEP)
				.getStatus());
	}
	
	@Test
	public void editNotificationConfigTest() throws Exception {
		Mockito.when(notificationModuleDao.findByIdAndAssigneeAndStatusAndIsDeleted(
				NotificationTestConstants.MODEL_ID, principal.getName(), ModuleStatus.PUBLISHED, NotificationTestConstants.IS_DELETED))
		.thenReturn(NotificationTestConstants.moduleObject);
		Mockito.when(notificationModuleDao.save(NotificationTestConstants.moduleObject))
		.thenReturn(NotificationTestConstants.moduleObject);
		assertEquals(200, notificationConfigService.editNotificationConfig(principal.getName(), NotificationTestConstants.MODEL_ID)
				.getStatus());
	}
}
