package com.kuliza.lending.logging;

import static org.junit.Assert.assertEquals;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.SimpleMessage;
import org.junit.BeforeClass;
import org.junit.Test;
import com.kuliza.lending.logging.utils.LogMaskingConverter;
import com.kuliza.lending.logging.utils.LoggingMarkers;

public class LogMaskingTest {
  private static LogMaskingConverter converter;

  @BeforeClass
  public static void setupLogging() {
    converter = LogMaskingConverter.newInstance(null);
  }

  @Test
  public void noneMaskTest() {
    SimpleMessage message = new SimpleMessage("{\"noMask\": \"foo\"}");
    LogEvent logEvent = new Log4jLogEvent("LogMaskingConverterSpecLogger",
        new MarkerManager.Log4jMarker(LoggingMarkers.JSON.getName()), null, null, message, null,
        null);
    StringBuilder builder = new StringBuilder();
    converter.format(logEvent, builder);
    assertEquals(builder.toString(), "{\"noMask\": \"foo\"}");
  }

  @Test
  public void confidentialMaskTest() {
    SimpleMessage message = new SimpleMessage(
        "{\"pannumber\": \"1234567890\", \"id\": \"ABC-123\", \"private\": \"someKey\"}");
    LogEvent logEvent = new Log4jLogEvent("LogMaskingConverterSpecLogger",
        new MarkerManager.Log4jMarker(LoggingMarkers.JSON.getName()), null, null, message, null,
        null);
    StringBuilder builder = new StringBuilder();
    converter.format(logEvent, builder);
    assertEquals(builder.toString(),
        "{\"pannumber\": \"1234567890\", \"id\": \"ABC-123\", \"private\": \"someKey\"}");
  }

  @Test
  public void confidentialCaseInsensitiveMaskTest() {
    SimpleMessage message = new SimpleMessage(
        "{\"PANNumber\": \"1234567890\", \"id\": \"ABC-123\", \"Private\": \"someKey\"}");
    LogEvent logEvent = new Log4jLogEvent("LogMaskingConverterSpecLogger",
        new MarkerManager.Log4jMarker(LoggingMarkers.JSON.getName()), null, null, message, null,
        null);
    StringBuilder builder = new StringBuilder();
    converter.format(logEvent, builder);
    assertEquals(builder.toString(),
        "{\"PANNumber\": \"1234567890\", \"id\": \"ABC-123\", \"Private\": \"someKey\"}");
  }

  @Test
  public void InvalidMaskTest() {
    SimpleMessage message = new SimpleMessage("Invalid data to log");
    LogEvent logEvent = new Log4jLogEvent("LogMaskingConverterSpecLogger",
        new MarkerManager.Log4jMarker(LoggingMarkers.JSON.getName()), null, null, message, null,
        null);
    StringBuilder builder = new StringBuilder();
    converter.format(logEvent, builder);
    assertEquals(builder.toString(), "Invalid data to log");
  }
}

