package com.kuliza.lending.logging;

import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.kuliza.lending.logging.interceptor.RequestInterceptor;

public class RequestInterceptorTest {
  private MockHttpServletRequest request;
  private MockHttpServletResponse response;
  private MockFilterChain chain;
  private RequestInterceptor filter;
  private HttpServlet servlet;

  private List<RequestInterceptor> invocations;

  @Before
  @SuppressWarnings("serial")
  public void setup() {
    servlet = new HttpServlet() {};
    request = new MockHttpServletRequest();
    response = new MockHttpServletResponse();
    chain = new MockFilterChain();
    invocations = new ArrayList<RequestInterceptor>();
    filter =
        new RequestInterceptor() {
          @Override
          protected void doFilterInternal(
              HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
              throws ServletException, IOException {
            invocations.add(this);
            filterChain.doFilter(request, response);
          }
        };
  }

  @Test
  public void doFilterOnce() throws ServletException, IOException {
    filter.doFilter(request, response, chain);
    assertThat(invocations, hasItem((filter)));
  }

  @Test
  public void doFilterMultiOnlyIvokesOnce() throws ServletException, IOException {
    filter.doFilter(request, response, new MockFilterChain(servlet, filter));
    assertThat(invocations, hasItem((filter)));
  }

  @Test
  public void doFilterOtherSubclassInvoked() throws ServletException, IOException {
    RequestInterceptor filter2 =
        new RequestInterceptor() {
          @Override
          protected void doFilterInternal(
              HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
              throws ServletException, IOException {
            invocations.add(this);
            filterChain.doFilter(request, response);
          }
        };
    filter.doFilter(request, response, new MockFilterChain(servlet, filter2));

    assertThat(invocations, either(hasItem(filter)).or(hasItem(filter2)));
  }
}
