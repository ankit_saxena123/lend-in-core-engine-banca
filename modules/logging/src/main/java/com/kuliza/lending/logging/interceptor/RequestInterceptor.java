package com.kuliza.lending.logging.interceptor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import com.kuliza.lending.logging.utils.LoggingMarkers;
import com.kuliza.lending.logging.utils.Constants;

/** @author manjunathch */
@Component
public class RequestInterceptor extends OncePerRequestFilter {
  // Media types which we will intercept
  private static final List<MediaType> VISIBLE_TYPES =
      Arrays.asList(MediaType.valueOf("text/*"), MediaType.APPLICATION_FORM_URLENCODED,
          MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON_UTF8, MediaType.APPLICATION_XML,
          MediaType.valueOf("application/*+json"),
          MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE),
          MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE),
          MediaType.valueOf("application/*+xml"), MediaType.MULTIPART_FORM_DATA);

  private static Set<String> logRequestHeadersList =
      new HashSet<String>(Arrays.asList(Constants.getLogRequestHeaderNames().split(",")));

  private static final String rootURLToIntercept = "/lending";

  private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

  public RequestInterceptor() {
    
    super();
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    if (isAsyncDispatch(request)) {
      filterChain.doFilter(request, response);
    } else {
      doFilterWrapped(wrapRequest(request), wrapResponse(response), filterChain);
    }
  }

  protected void doFilterWrapped(ContentCachingRequestWrapper request,
      ContentCachingResponseWrapper response, FilterChain filterChain)
      throws ServletException, IOException {
    // Commented portions note: After response logging overriden while mail send on ib-core
    // try {
    String transactionId = UUID.randomUUID().toString();
    ThreadContext.put("transactionId", transactionId);
    beforeRequest(request, transactionId);
    filterChain.doFilter(request, response);
//     } finally {
     afterRequest(request, response);
     response.copyBodyToResponse();
//     }
  }

  protected void beforeRequest(ContentCachingRequestWrapper request, String requestID)
      throws IOException {
    if (logger.isInfoEnabled() & request.getRequestURI().contains(rootURLToIntercept)) {
      logRequest(request, getRemoteAddr(request));
    }
  }

  protected void afterRequest(ContentCachingRequestWrapper request,
      ContentCachingResponseWrapper response) throws IOException {
    if (logger.isInfoEnabled() & request.getRequestURI().contains(rootURLToIntercept)) {
      logResponse(response, request, getRemoteAddr(request) + "|<");
    }
  }

  private void logRequest(ContentCachingRequestWrapper request, String prefix) throws IOException {
    StringBuilder logString = new StringBuilder(
        "Lending Request ==> IP: {}, Method: {}, URI: {}, Query String: {}, Headers:");

    Collections.list(request.getHeaderNames()).forEach(headerName -> {
      if (logRequestHeadersList.contains(headerName)) {
        Collections.list(request.getHeaders(headerName)).forEach(headerValue -> {
          logString.append(headerName).append(" = ").append(headerValue);
        });
      }
    });
    logger.info(logString.toString(), prefix, request.getMethod(), request.getRequestURI(),
        request.getQueryString());
  }

  private void logResponse(ContentCachingResponseWrapper response,
      ContentCachingRequestWrapper request, String prefix) throws IOException {
    int status = response.getStatus();
    StringBuilder logString = new StringBuilder(
        "Response with status code: {}, and with status message: {}, Response body ");
    byte[] reContent = request.getContentAsByteArray();

    String RequestContent = null;
    if (reContent.length > 0) {
      RequestContent =
          logContent(reContent, request.getContentType(), request.getCharacterEncoding());
    }
    if (request.getContentType() != null
        && IsJsonMediaType(MediaType.valueOf(request.getContentType()))) {
      logger.info(LoggingMarkers.JSON, RequestContent);
    } else {
      logger.info("Request Body" + RequestContent);
    }
    byte[] content = response.getContentAsByteArray();
    String responseContent = null;
    if (content.length > 0) {
      responseContent =
          logContent(content, response.getContentType(), response.getCharacterEncoding());
    }
    if (status > 199 && status < 300) {
      if (response.getContentType() != null
          && IsJsonMediaType(MediaType.valueOf(response.getContentType()))) {
        logger.info(logString.toString(), status, HttpStatus.valueOf(status).getReasonPhrase());
        logger.info(LoggingMarkers.JSON, responseContent);
      } else {
        logString.append(responseContent);
        logger.info(logString.toString(), status, HttpStatus.valueOf(status).getReasonPhrase());
      }
    } else {
      logString.append(responseContent);
      logger.error(logString.toString(), status, HttpStatus.valueOf(status).getReasonPhrase());
    }
  }

  private String logContent(byte[] content, String contentType, String contentEncoding) {
    MediaType mediaType = MediaType.valueOf(contentType);
    boolean visible =
        VISIBLE_TYPES.stream().anyMatch(visibleType -> visibleType.includes(mediaType));
    StringBuilder logString = new StringBuilder();
    if (visible) {
      try {
        String contentString = new String(content, contentEncoding);
        Stream.of(contentString.split("\r\n|\r|\n")).forEach(line -> {
          logString.append(line);
        });
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }

    } else {
      logString.append("Unknown media type");
    }
    return logString.toString();
  }

  private String getRemoteAddr(final HttpServletRequest request) {
    final String ipFromHeader = request.getHeader("X-FORWARDED-FOR");
    if (ipFromHeader != null && ipFromHeader.length() > 0) {
      return ipFromHeader;
    }
    return request.getRemoteAddr();
  }

  private static ContentCachingRequestWrapper wrapRequest(HttpServletRequest request) {
    if (request instanceof ContentCachingRequestWrapper) {
      return (ContentCachingRequestWrapper) request;
    } else {
      return new ContentCachingRequestWrapper(request);
    }
  }

  private static ContentCachingResponseWrapper wrapResponse(HttpServletResponse response) {
    if (response instanceof ContentCachingResponseWrapper) {
      return (ContentCachingResponseWrapper) response;
    } else {
      return new ContentCachingResponseWrapper(response);
    }
  }

  private static boolean IsJsonMediaType(MediaType mediaType) {
    if (mediaType.equals(MediaType.APPLICATION_JSON_UTF8)
        || mediaType.equals(MediaType.APPLICATION_JSON)
        || mediaType.toString().equals(MediaType.APPLICATION_JSON_UTF8_VALUE)
        || mediaType.toString().equals(MediaType.APPLICATION_JSON_VALUE))
      return true;
    else
      return false;
  }
}
