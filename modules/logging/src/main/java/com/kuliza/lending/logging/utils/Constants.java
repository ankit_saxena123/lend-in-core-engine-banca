package com.kuliza.lending.logging.utils;

import java.util.HashMap;
import java.util.regex.Pattern;

public class Constants {
	static final String JSON_KEYS = "private|creditCard|aadhaar|password|pan|panNumber|cvv|email|mobileNumber|phoneNumber|";
	private static final String LOG_REQUEST_HEADER_NAMES = "accept,accept-encoding";
	static final String JSON_REPLACEMENT_REGEX = "\"$1\": \"******\"";
	static final String BASE64_REPLACEMENT_REGEX = "******";
	static final Pattern JSON_PATTERN = Pattern.compile("\"(" + Constants.JSON_KEYS + ")\" ?: ?\"([^\"]+)\"",
			Pattern.CASE_INSENSITIVE);
	public static final String BASE64_REGEX = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$";
	public static String getLogRequestHeaderNames() {
		return LOG_REQUEST_HEADER_NAMES;
	}
}
