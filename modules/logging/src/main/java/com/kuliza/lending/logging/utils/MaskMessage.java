package com.kuliza.lending.logging.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.NameAlreadyBoundException;
import org.springframework.stereotype.Component;

@Component
public class MaskMessage {

  /**
   * @param matcher: Matcher instance
   * @param replacementString: String to replace with the matched word
   * @param length:
   * @return String after replacement of matched word
   *
   * Replace the string only when length of matched word is greater than input length
   * */
  private String maskUtil(Matcher matcher, String replacementString, int length){
    StringBuffer buffer = new StringBuffer();
    while (matcher.find()) {
      if(matcher.group().length()>length)
        matcher.appendReplacement(buffer, replacementString);
    }
    matcher.appendTail(buffer);
    return buffer.toString();
  }

  public String mask(String message) {
    Matcher matcher = Constants.JSON_PATTERN.matcher(message);
    String maskedJSONKeys = maskUtil(matcher, Constants.JSON_REPLACEMENT_REGEX,0);
    Matcher base64Matcher = Pattern.compile(Constants.BASE64_REGEX).matcher(maskedJSONKeys);
    return maskUtil(base64Matcher, Constants.BASE64_REPLACEMENT_REGEX, 512);
  }
}

