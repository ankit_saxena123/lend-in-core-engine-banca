package com.kuliza.lending.logging.utils;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * 		@ConverterKeys({"cm"}) is used to check custom message pattern
 *         presence in log4j fomatter and invokes when requried
 */
@Plugin(name = "logmask", category = "Converter")
@ConverterKeys({ "cm" })
public class LogMaskingConverter extends LogEventPatternConverter {

	// This is constant which we will be using in log pattern in log4j2.xml
	private static final String CONVERTER_NAME = "cm";

	@Autowired
	MaskMessage maskMessage;

	protected LogMaskingConverter(String[] options) {
		super(CONVERTER_NAME, CONVERTER_NAME);
	}

	public static LogMaskingConverter newInstance(final String[] options) {
		return new LogMaskingConverter(options);
	}

	@Override
	public void format(LogEvent event, StringBuilder outputMessage) {
		String message = event.getMessage().getFormattedMessage();
		String maskedMessage = message;
		if (event.getMarker() != null && event.getMarker().getName().equals(LoggingMarkers.JSON.getName())) {
			try {
				maskedMessage = maskMessage.mask(message);
			} catch (Exception e) {
				maskedMessage = message; // Although if this fails, it may be better to not log the message
			}
		}
		outputMessage.append(maskedMessage);
	}
}
