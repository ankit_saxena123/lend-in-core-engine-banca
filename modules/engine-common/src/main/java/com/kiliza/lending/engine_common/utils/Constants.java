package com.kiliza.lending.engine_common.utils;

public class Constants {

  public static final String PORTAL_INITIATE_API_URL = "/lending/case/initiate";
  public static final String BACK_TO_JOURNEY_API_URL = "/lending/journey/back-to-journey";

}
