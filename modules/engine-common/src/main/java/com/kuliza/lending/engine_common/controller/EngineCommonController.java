package com.kuliza.lending.engine_common.controller;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.configs.EditableTableQuery;
import com.kuliza.lending.engine_common.services.EngineCommonService;
import com.kuliza.lending.engine_common.utils.EngineCommonConstants;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.swagger.annotations.*;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/lending")
public class EngineCommonController {

    @Autowired
    private EngineCommonService engineCommonService;
    
   
    @ApiImplicitParam(name= Constants.AUTH, value= Constants.AUTH_TOKEN,
            required=true, dataType="string", paramType="header", example= Constants.BEARER_TOKEN)
    @ApiOperation(value= EngineCommonConstants.ECC_UPLOAD_FILE)
    @ApiResponses(value={@ApiResponse(message= EngineCommonConstants.ECC_UPLOAD_FILE_RESPONSE, code=200)})
    @RequestMapping(method = RequestMethod.POST, value = "/upload-file", consumes = "multipart/form-data")
    public ResponseEntity<Object> uploadFile(@ApiParam(value = EngineCommonConstants.ECC_UPLOAD_FILE_SELECT ,
                                                    required = true) @RequestParam("file") MultipartFile file,
                                                   @ApiParam(value = EngineCommonConstants.ECC_DOCUMENT_TYPE ,
                                                           required = false, example = "1")
                                                   @RequestParam(value = "document_type", required = false) Integer documentType,
                                                   @ApiParam(value = EngineCommonConstants.ECC_LABEL ,
                                                           required = false, example = "file_type")
                                                   @RequestParam(value = "label", required = false) String label) throws IOException{
        return CommonHelperFunctions.buildResponseEntity(engineCommonService.upload(file, documentType, label));
    }


    @ApiImplicitParam(name= Constants.AUTH, value= Constants.AUTH_TOKEN,
        required=true, dataType="string", paramType="header", example= Constants.BEARER_TOKEN)
    @ApiOperation(value= EngineCommonConstants.ECC_DOWNLOAD_FILE)
    @RequestMapping(method = RequestMethod.GET, value = "/download-file")
    public byte[] downloadFile(
        @ApiParam(value = EngineCommonConstants.ECC_DOCUMENT_ID ,
            required = true, example = "7029")
        @RequestParam(value = "document_id") String documentId) throws IOException, UnirestException {
        return engineCommonService.download(documentId);
    }

    @ApiImplicitParam(name= Constants.AUTH, value= Constants.AUTH_TOKEN,
            required=true, dataType="string", paramType="header", example= Constants.BEARER_TOKEN)
    @ApiOperation(value= EngineCommonConstants.ECC_GET_QUERY)
    @ApiResponses(value={@ApiResponse(message= EngineCommonConstants.ECC_GET_QUERY_RESPONSE, code=200)})
    @RequestMapping(method = RequestMethod.POST, value = "/get-query-result/{caseInstanceId}")
    public ResponseEntity<Object> getQueryResult(@RequestBody EditableTableQuery editableTableQuery,
                                                 @PathVariable String caseInstanceId){
        Object result = engineCommonService.getQueryResult(editableTableQuery, caseInstanceId);
        com.kuliza.lending.common.pojo.ApiResponse apiResponse = null;
        if(result == null){
            apiResponse = new com.kuliza.lending.common.pojo.ApiResponse(HttpStatus.BAD_REQUEST,
                    Constants.SQL_ERROR);
        }else{
            apiResponse = new com.kuliza.lending.common.pojo.ApiResponse(HttpStatus.OK,
                    Constants.SUCCESS_MESSAGE, result);
        }
        return CommonHelperFunctions.buildResponseEntity(apiResponse);
    }


    @ApiOperation(value= EngineCommonConstants.ECC_FETCH_MASTER_API)
    @ApiResponses(value={@ApiResponse(message= EngineCommonConstants.ECC_FETCH_MASTER_API_RESPONSE, code=200)})
    @RequestMapping(method = RequestMethod.POST, value = "/{slug}")
    public ResponseEntity<Object> fetchMasterApi(@ApiParam(value = "Map", required = true) @RequestBody Map<String, Object> mapObject,
                  @RequestHeader(value = "Content-Type") String contentType,
                  @ApiParam(value = "Slug" , required = true , example = EngineCommonConstants.ECC_SLUG)
                  @PathVariable(value = "slug") String slug){
        return CommonHelperFunctions.buildResponseEntity(engineCommonService.fetchMasterApi(mapObject, contentType,
                slug));
    }

    @ApiImplicitParam(name= Constants.AUTH, value= Constants.AUTH_TOKEN,
            required=true, dataType="string", paramType="header", example= Constants.BEARER_TOKEN)
    @ApiOperation(value= EngineCommonConstants.MASTER_DATA_API)
    @RequestMapping(method = RequestMethod.POST, value = com.kuliza.lending.engine_common.utils.Constants.MASTER_DATA)
    public ResponseEntity<Object> getMasterData(@RequestBody Map<String,Object> mapObject,@RequestHeader(value = "Content-Type") String contentType,
    		@PathVariable(value = "slug") String slug) {
    	return CommonHelperFunctions.buildResponseEntity(engineCommonService.getMasterData(mapObject,contentType,slug));
    }
}
