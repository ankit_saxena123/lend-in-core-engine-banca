package com.kuliza.lending.engine_common.models;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface CustomURLMapperDao extends CrudRepository<CustomURLMapper, Long> {

    public CustomURLMapper findBySlugAndIsDeleted(String id , Boolean isDeleted);

    public List<CustomURLMapper> findBySlug(String slug, Boolean isDeleted);

    public List<CustomURLMapper> findByIsDeleted(Boolean isDeleted);

}
