package com.kuliza.lending.engine_common.controller;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.pojo.CustomActionRequest;
import com.kuliza.lending.engine_common.pojo.CustomBulkActionRequest;
import com.kuliza.lending.engine_common.services.CustomActionService;
import com.kuliza.lending.engine_common.utils.EngineCommonConstants;
import com.kuliza.lending.engine_common.utils.EngineCommonSwaggerConstants;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/lending")
public class CustomActionController {

    @Autowired
    private CustomActionService customActionService;

    @ApiOperation(value = EngineCommonSwaggerConstants.PC_FIRE_CUSTOM_ACTION)
    @ApiResponses(value = {@ApiResponse(message = EngineCommonSwaggerConstants.PC_FIRE_CUSTOM_RESPONSE, code = 200)})
    @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
            required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
    @RequestMapping(method = RequestMethod.POST, value = EngineCommonConstants.CUSTOM_ACTION_URL + "{slug}")
    public ResponseEntity<Object> fireCustomAction(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerCibil") @PathVariable(value = "slug") String slug,
                                                   @ApiParam(value="Role name", required = false,
                                                           example = "cmmnCreditAnalystPOCAdmin") @NotNull @RequestParam String roleName,
                                                   @ApiParam(value = "Tab Key", required = false,
                                                           example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
                                                   @Valid @RequestBody CustomActionRequest customActionRequest,
                                                   @ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
                                                   @RequestHeader(value = "Authorization") String token) throws Exception{
        return CommonHelperFunctions.buildResponseEntity(customActionService.fireCustomAction(request, slug, customActionRequest, contentType,
                token, null, null, roleName, principal.getName()));
    }

    @ApiOperation(value = EngineCommonSwaggerConstants.PC_FIRE_CUSTOM_ACTION)
    @ApiResponses(value = {@ApiResponse(message = EngineCommonSwaggerConstants.PC_FIRE_CUSTOM_RESPONSE, code = 200)})
    @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
            required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
    @RequestMapping(method = RequestMethod.POST, value = EngineCommonConstants.PORTAL_CUSTOM_ACTION_URL + "{slug}")
    public ResponseEntity<Object> firePortalCustomAction(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerCibil") @PathVariable(value = "slug") String slug,
                                                   @ApiParam(value="Role name",required = true,
                                                           example = "cmmnCreditAnalystPOCAdmin") @NotNull @RequestParam String roleName,
                                                   @ApiParam(value = "Case Instance id" , required = true,
                                                           example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @NotNull @RequestParam String caseInstanceId,
                                                   @ApiParam(value = "Tab Key" , required = true,
                                                           example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
                                                   @Valid @RequestBody CustomActionRequest customActionRequest,
                                                   @ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
                                                   @RequestHeader(value = "Authorization") String token) throws Exception{
        return CommonHelperFunctions.buildResponseEntity(customActionService.fireCustomAction(request, slug, customActionRequest, contentType,
                token, caseInstanceId, null, roleName, principal.getName()));
    }

    @ApiOperation(value = EngineCommonSwaggerConstants.PC_FIRE_CUSTOM_ACTION)
    @ApiResponses(value = {@ApiResponse(message = EngineCommonSwaggerConstants.PC_FIRE_CUSTOM_RESPONSE, code = 200)})
    @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
            required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
    @RequestMapping(method = RequestMethod.POST, value = EngineCommonConstants.JOURNEY_CUSTOM_ACTION_URL + "{slug}")
    public ResponseEntity<Object> fireJourneyCustomAction(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerCibil") @PathVariable(value = "slug") String slug,
                                                         @ApiParam(value="Role name",required = true,
                                                                 example = "cmmnCreditAnalystPOCAdmin") @RequestParam String roleName,
                                                         @ApiParam(value = "Process Instance id" , required = true,
                                                                 example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @NotNull @RequestParam String processInstanceId,
                                                         @ApiParam(value = "Tab Key" , required = true,
                                                                 example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
                                                         @Valid @RequestBody CustomActionRequest customActionRequest,
                                                         @ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
                                                         @RequestHeader(value = "Authorization") String token) throws Exception{
        return CommonHelperFunctions.buildResponseEntity(customActionService.fireCustomAction(request, slug, customActionRequest, contentType,
                token, null, processInstanceId, roleName, principal.getName()));
    }

  @ApiOperation(value = EngineCommonSwaggerConstants.PC_FIRE_BULK_ACTION)
  @ApiResponses(value = {@ApiResponse(message = EngineCommonSwaggerConstants.PC_FIRE_BULK_ACTION_RESPONSE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.POST, value = EngineCommonConstants.BULK_ACTION_URL +"{slug}")
	public ResponseEntity<Object> fireBulkCustomActions(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerId") @PathVariable(value = "slug") String slug,
												  @ApiParam(value="Role name",required = true,
														  example = "cmmnCreditAnalystPOCAdmin") @NotNull @RequestParam String roleName,
												  @ApiParam(value = "Tab Key" , required = true,
														  example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
												  @Valid @RequestBody List<CustomBulkActionRequest> customBulkActionRequestList,
												  @ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
												  @RequestHeader(value = "Authorization") String token){
		return CommonHelperFunctions.buildResponseEntity(customActionService.fireBulkCustomActions(request, slug, customBulkActionRequestList,
				contentType, token, roleName, principal.getName()));
  }

    @ApiOperation(value = EngineCommonSwaggerConstants.PC_FIRE_BULK_ACTION)
    @ApiResponses(value = {@ApiResponse(message = EngineCommonSwaggerConstants.PC_FIRE_BULK_ACTION_RESPONSE, code = 200)})
    @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
            required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
    @RequestMapping(method = RequestMethod.POST, value = EngineCommonConstants.PORTAL_BULK_ACTION_URL +"{slug}")
    public ResponseEntity<Object> firePortalBulkCustomActions(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerId") @PathVariable(value = "slug") String slug,
                                                  @ApiParam(value="Role name",required = true,
                                                          example = "cmmnCreditAnalystPOCAdmin") @NotNull @RequestParam String roleName,
                                                  @ApiParam(value = "Tab Key" , required = true,
                                                          example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
                                                  @Valid @RequestBody List<CustomBulkActionRequest> customBulkActionRequestList,
                                                  @ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
                                                  @RequestHeader(value = "Authorization") String token){
        return CommonHelperFunctions.buildResponseEntity(customActionService.fireBulkCustomActions(request, slug, customBulkActionRequestList,
                contentType, token, roleName, principal.getName()));
    }

}
