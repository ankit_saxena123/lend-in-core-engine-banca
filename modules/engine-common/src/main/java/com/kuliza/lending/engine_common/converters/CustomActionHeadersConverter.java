package com.kuliza.lending.engine_common.converters;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.pojo.CustomActionHeaders;
import com.kuliza.lending.engine_common.utils.CustomActionHelperFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomActionHeadersConverter implements AttributeConverter<List<CustomActionHeaders>, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomActionHeadersConverter.class);

    @Override
    public String convertToDatabaseColumn(List<CustomActionHeaders> customActionHeadersList) {
        ArrayList<String> headerList = new ArrayList<>();
        try {
            if(customActionHeadersList != null) {
                for (CustomActionHeaders customActionHeaders : customActionHeadersList) {
                    if (customActionHeaders != null) {
                        Map<String, Object> customActionHeaadersMap = new HashMap<>();
                        customActionHeaadersMap.put("key", customActionHeaders.getKey());
                        customActionHeaadersMap.put("value", customActionHeaders.getValue());

                        headerList.add(CommonHelperFunctions
                                .getJsonString(customActionHeaadersMap));
                    }
                }
            }
        } catch (Exception exception) {
            LOGGER.error("convertToDatabaseColumn excepotion : " + exception);
            exception.printStackTrace();
        }
        return CommonHelperFunctions.getStringValue(headerList);
    }

    @Override
    public List<CustomActionHeaders> convertToEntityAttribute(String externalHeadersList) {
        ArrayList<CustomActionHeaders> response = new ArrayList<>();
        try {
            response = CustomActionHelperFunctions.getCustomActionHeadersList(externalHeadersList);
        } catch (Exception exception) {
            LOGGER.error("convertToEntityAttribute excepotion : " + exception);
            exception.printStackTrace();
        }
        return response;
    }

}
