package com.kuliza.lending.engine_common.services;

import com.kuliza.lending.common.connection.MultipartUtility;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

public abstract class BaseFileProcessor {

    private static final Logger logger = LoggerFactory.getLogger(BaseFileProcessor.class);

    public abstract Object upload(MultipartFile file, Integer documentType, String label) throws IOException;

    public abstract Object download(String id) throws IOException, UnirestException;

    public ApiResponse uploadFile(MultipartFile multipartFile, Integer documentType, String label) throws IOException {
        ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
        String url = HelperFunctions.getDMSUrl();
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "multipart/form-data");

        String document_type = CommonHelperFunctions.getStringValue(documentType);

        Map<String, Object> result = new HashMap<>();
        try {
            String charset = "UTF-8";
            MultipartUtility multipart = new MultipartUtility(url, charset);
            for (String key : headers.keySet()) {
                multipart.addHeaderField(key, headers.get(key));
            }
            if (document_type != null && !document_type.isEmpty())
                multipart.addFormField("document_type", document_type);

            if (label != null && !label.isEmpty()) {
                multipart.addFormField("label", label);
            }

            File file = CommonHelperFunctions.convertMultipartFiletoFile(multipartFile);
            multipart.addFilePart("file", file);

            List<String> response = multipart.finish();
            String jsonString = "";
            for (String line : response) {
                jsonString = jsonString.concat(line);
            }
            result = CommonHelperFunctions.getHashMapFromJsonString(jsonString);
            file.delete();
            apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, result);

        } catch (Exception e) {
            apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FILE_UPLOAD_ERROR);
            logger.warn("==========uploadFile==========exception : " + e);
        }
        return apiResponse;
    }

    public byte[] downloadFile(String id) throws IOException, UnirestException {
        String url = HelperFunctions.getDMSUrl() + "?document_id=" + id;
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        byte[] bytes = null;
        try {
            HttpResponse response = Unirest.get(url).headers(headers).asBinary();
            bytes = IOUtils.toByteArray(response.getRawBody());
        } catch (Exception exception) {
            logger.warn("==========downloadFile==========exception : " + exception);
            throw exception;
        }
        return bytes;
    }
}
