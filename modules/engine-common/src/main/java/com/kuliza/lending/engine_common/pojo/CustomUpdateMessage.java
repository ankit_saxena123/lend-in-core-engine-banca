package com.kuliza.lending.engine_common.pojo;

import com.kuliza.lending.engine_common.utils.EnumConstants;

import java.io.Serializable;

public class CustomUpdateMessage implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CustomUpdateMessage() {
		super();
		// TODO Auto-generated constructor stub
	}
	String requestId;
	String data;
	EnumConstants.Events event;
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public EnumConstants.Events getEvent() {
		return event;
	}
	public void setEvent(EnumConstants.Events event) {
		this.event = event;
	}
	public CustomUpdateMessage(String requestId, String data, EnumConstants.Events event) {
		super();
		this.requestId = requestId;
		this.data = data;
		this.event = event;
	}
	@Override
	public String toString() {
		return "CustomUpdateMessage [requestId=" + requestId + ", data=" + data + ", event=" + event + "]";
	}
	
	

}
