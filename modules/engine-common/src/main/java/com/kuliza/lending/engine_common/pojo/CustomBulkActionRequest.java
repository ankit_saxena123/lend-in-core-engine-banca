package com.kuliza.lending.engine_common.pojo;

public class CustomBulkActionRequest {
    private String caseInstanceId;
    private CustomActionRequest customActionRequest;

    public CustomBulkActionRequest() {
        super();
    }

    public CustomBulkActionRequest(long id, CustomActionRequest customActionRequest, String caseInstanceId) {
        this.customActionRequest = customActionRequest;
        this.caseInstanceId = caseInstanceId;
    }

    public String getCaseInstanceId() {
        return caseInstanceId;
    }

    public void setCaseInstanceId(String caseInstanceId) {
        this.caseInstanceId = caseInstanceId;
    }

    public CustomActionRequest getCustomActionRequest() {
        return customActionRequest;
    }

    public void setCustomActionRequest(CustomActionRequest customActionRequest) {
        this.customActionRequest = customActionRequest;
    }

    @Override
    public String toString() {
        return "CustomBulkActionURLBody{" +
                ", caseInstanceId='" + caseInstanceId + '\'' +
                ", customActionRequest=" + customActionRequest +
                '}';
    }
}
