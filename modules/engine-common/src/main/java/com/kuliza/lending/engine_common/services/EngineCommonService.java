package com.kuliza.lending.engine_common.services;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.common.connection.UnirestConnection;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;

import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.MasterConfig;
import com.kuliza.lending.engine_common.utils.EngineCommonConstants;
import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.kuliza.lending.engine_common.configs.EditableTableQuery;
import java.util.List;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.keycloak.representations.idm.GroupRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.DataSource;
import javax.servlet.ServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kuliza.lending.common.utils.Constants.INVALID_USER;
import static com.kuliza.lending.engine_common.utils.EngineCommonConstants.*;

@Service
public class EngineCommonService extends BaseFileProcessor{

    @Autowired
    DataSource dataSource;

    @Autowired
    CmmnRuntimeService cmmnRuntimeService;

    private static final Logger LOGGER = LoggerFactory.getLogger(EngineCommonService.class);

    @Autowired
    private KeyCloakService keyCloakService;
    @Override
    public ApiResponse upload(MultipartFile file, Integer documentType, String label) throws IOException {
        return super.uploadFile(file, documentType, label);
    }

    @Override
    public byte[] download(String id) throws IOException, UnirestException {
        return super.downloadFile(id);
    }

    public String getTableName(String caseInstanceId, String tableKey){
        caseInstanceId = caseInstanceId.replace("-", "");
        String tableName = caseInstanceId + "_" + tableKey;
        LOGGER.info("Table name = " + tableName);
        return tableName;
    }

    public String getCreateTableQuery(Map<String, String> columnNames, String tableName){
        String createTableQuery = "CREATE TABLE " + tableName + " (";
        for(String key : columnNames.keySet()){
            createTableQuery = createTableQuery + " " + key + " " + columnNames.get(key) + " ,";
        }
        createTableQuery = createTableQuery.substring(0, createTableQuery.length()-1) + " );";
        LOGGER.info("create table query = " + createTableQuery);
        return createTableQuery;
    }

    public String getInsertQuery(JSONObject editableTableJsonObject, Map<String, String> columnNames,
                                 String tableName){
        String insertQuery = "INSERT INTO " + tableName + " ";
        String columnKeys = "(";
        String columnValues = "(";
        for(String key : columnNames.keySet()){
            if(editableTableJsonObject.has(key)) {
                switch (columnNames.get(key)) {
                    case "BOOLEAN":
                    case "boolean":
                        boolean boolValue = editableTableJsonObject.getBoolean(key);
                        columnValues = columnValues + boolValue + ",";
                        break;
                    case "BIGINT":
                    case "bigint":
                        int intValue = editableTableJsonObject.getInt(key);
                        columnValues = columnValues + intValue + ",";
                        break;
                    case "DOUBLE":
                    case "double":
                        double doubleValue = editableTableJsonObject.getDouble(key);
                        columnValues = columnValues + doubleValue + ",";
                        break;
                    default:
                        String stringValue = editableTableJsonObject.getString(key);
                        columnValues = columnValues + "'" + stringValue + "' ,";
                        break;
                }
                columnKeys = columnKeys + key + ",";
            }
        }
        columnKeys = columnKeys.substring(0, columnKeys.length()-1) + ")";
        LOGGER.info("column keys = " + columnKeys);
        columnValues = columnValues.substring(0, columnValues.length() - 1) + ");";
        LOGGER.info("column values = " + columnValues);
        insertQuery = insertQuery + columnKeys + "values " + columnValues;
        LOGGER.info("insert query string = " + insertQuery);
        return insertQuery;
    }

    public Object getQueryResult(EditableTableQuery editableTableQuery, String caseInstanceId) {
        Object runtimeVariableObject = cmmnRuntimeService.getVariable(caseInstanceId,
                editableTableQuery.getTableKey());
        LOGGER.info("caseInstanceId = " + caseInstanceId + " tableKey = " + editableTableQuery.getTableKey());
        LOGGER.info("Run time Variable received = " + runtimeVariableObject);
        String tableName = getTableName(caseInstanceId, editableTableQuery.getTableKey());
        Map<String, String> columnNames = editableTableQuery.getTableColumns();
        String createTableQuery = getCreateTableQuery(columnNames, tableName);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<Map<String, Object>> result = null;
        try {
            JSONArray editableTableJsonArray = CommonHelperFunctions.objectToJSONArray(runtimeVariableObject);
            LOGGER.info("editableTableJsonArray = " + editableTableJsonArray);
            jdbcTemplate.execute(createTableQuery);
            LOGGER.info("Table Created = " + createTableQuery);
            for (int i = 0; i < editableTableJsonArray.length(); i++) {
                String insertQuery = getInsertQuery(editableTableJsonArray.getJSONObject(i), columnNames, tableName);
                jdbcTemplate.execute(insertQuery);
            }
            String searchQuery = editableTableQuery.getQuery().replace(editableTableQuery.getTableKey(), tableName);
            LOGGER.info("Sql search query : " + searchQuery);
            result = jdbcTemplate.queryForList(searchQuery);
            LOGGER.info("Result of the search query = " + result);
            if (editableTableQuery.isSaveVariable() ) {
                LOGGER.info("checking variable to save");
                if(editableTableQuery.getVariableToBeSaved() != null &&
                        !editableTableQuery.getVariableToBeSaved().isEmpty()) {
                    Map<String, Object> variableToSave = new HashMap<>();
                    //fetching value from result map key
                    Object value = null;
                    for (String key : result.get(0).keySet()) {
                        value = result.get(0).get(key);
                    }
                    variableToSave.put(editableTableQuery.getVariableToBeSaved(), value);
                    cmmnRuntimeService.setVariables(caseInstanceId, variableToSave);
                    LOGGER.info("Saving Variable");
                }else{
                    result = null;
                    throw new Exception("VariableToSave either null or empty");
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error = " + e.getLocalizedMessage());
        } finally {
            String dropQuery = "drop table " + tableName + ";";
            jdbcTemplate.execute(dropQuery);
            LOGGER.info("Drop table query - " + dropQuery);
        }
        return result;
    }

    public HttpResponse getHttpResponse(JSONObject jsonObject, Map<String, String> headers, String url){
        return UnirestConnection.sendPOST(jsonObject, headers, url);
    }

    public ApiResponse getApiResponse(JSONObject jsonObject, Map<String, String> headers, String url){
        ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
        HttpResponse httpResponse = getHttpResponse(jsonObject, headers, url);
        LOGGER.info("httpResponse = " + httpResponse.getBody());
        try {
            String responseString = CommonHelperFunctions.getStringValue(httpResponse.getBody());
            Map<String, Object> result = CommonHelperFunctions.getHashMapFromJsonString(responseString);
            response = new ApiResponse(CommonHelperFunctions.getIntegerValue(result.get("status")),
                    CommonHelperFunctions.getStringValue(result.get("message")),
                    result.get("data"));
        }catch (Exception e){
            LOGGER.warn("===========Error============ : " + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }

    public ApiResponse fetchMasterApi(Map<String, Object> map, String contentType, String slug){

        ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
        LOGGER.info("Master Fetch Api");
        try {
            JSONObject jsonbject = CommonHelperFunctions.toJson(map);
            LOGGER.info("JsonObject = " + jsonbject);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonbject);
            LOGGER.info("Converted object to Array = " + jsonArray);
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", contentType);
            JSONObject finalJsonObject = new JSONObject();
            finalJsonObject.put("keyList", jsonArray);
            LOGGER.info("Final JsonObject = " + finalJsonObject);
            String url = StaticContextAccessor.getBean(MasterConfig.class).getProtocol() + "://"
                    + StaticContextAccessor.getBean(MasterConfig.class).getHost() + ":"
                    + StaticContextAccessor.getBean(MasterConfig.class).getPort()
                    + StaticContextAccessor.getBean(MasterConfig.class).getSubURL() +
                    EngineCommonConstants.MASTER_FETCH_API + "/" + slug;
            response = getApiResponse(finalJsonObject, headers, url);
        }catch (Exception e){
            LOGGER.warn("=======Error============= : " + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }

    public Map<String, Object> downloadFileInfo(String id) throws IOException, UnirestException {
        String url = HelperFunctions.getDMSUrl() + "?document_id=" + id;
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        byte[] bytes = null;
        try {
            HttpResponse<InputStream> response = Unirest.get(url).headers(headers).asBinary();
            bytes = IOUtils.toByteArray(response.getRawBody());
            result.put("fileName", response.getHeaders().get("Content-Disposition").get(0).split(";")[1].split("=")[1]);
            result.put("bytes", bytes);
            result.put("type", response.getHeaders().get("Content-Type").get(0));
        } catch (Exception exception) {
            LOGGER.warn("==========downloadFileInfo==========exception : " + exception);
            throw exception;
        }
        return result;
    }


    public ApiResponse getMasterData(Map<String, Object> map, String contentType, String slug){

        ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
        LOGGER.info("Master Fetch Api");
        try {
        	JSONObject jsonbject = new JSONObject();
            jsonbject.put("keyList", HelperFunctions.getMasterDataList(map));
            LOGGER.info("JsonObject = " + jsonbject);
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", contentType);
            String url = StaticContextAccessor.getBean(MasterConfig.class).getProtocol() + "://"
                    + StaticContextAccessor.getBean(MasterConfig.class).getHost() + ":"
                    + StaticContextAccessor.getBean(MasterConfig.class).getPort()
                    + StaticContextAccessor.getBean(MasterConfig.class).getSubURL() +
                    EngineCommonConstants.MASTER_FETCH_API + "/" + slug;
            response = getApiResponse(jsonbject, headers, url);
        }catch (Exception e){
            LOGGER.warn("=======Error============= : " + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }


    /**
     * This functions validates the object level user access.
     *
     * @param userId
     * @param roleName
     * @param processVariables
     * @return void
     * @throws Exception
     *
     * @author Mandip Gothadiya
     */
    public Boolean isValidUser(String userId, String roleName, Map<String, Object> processVariables) throws Exception {
        Boolean isValidUser = true;
        String assignedUser = CommonHelperFunctions.getStringValue(processVariables.get(roleName + ROLE_ASSIGNEE_VALUE));
        String roleNameStatus = CommonHelperFunctions.getStringValue(processVariables.get(roleName + STATUS_VARIABLE_KEY));
        if (!roleNameStatus.equals(CLAIM_BUCKET_KEY) && !userId.equals(assignedUser)){
            isValidUser = false;
        }
        return isValidUser;
    }

    /**
     * This functions validates the object level user access for group id & roleName.
     *
     * @param request
     * @param roleName
     * @param groupId
     * @param assignee
     * @param processVariable
     * @return void
     * @throws Exception
     *
     * @author Mandip Gothadiya
     */
    public void validateRequestResponse(ServletRequest request, String roleName, String groupId, String assignee, Map<String, Object> processVariable) throws Exception {
        Map<String, Object> keyCloakResponse = (Map<String, Object>) keyCloakService.userGroupsWithUserNAme(request).getData();
        List<GroupRepresentation> groupList = (List<GroupRepresentation>) keyCloakResponse.get("groups");
        String userId = CommonHelperFunctions.getStringValue(keyCloakResponse.get("username"));
        Boolean isValidGroup = false;
        Boolean isValidRoleName = false;
        Boolean isValidAssignee = false;
        String  ownerRoleName;

        //check for null only as empty groupId & userId are not allowed.
        if (groupId == null) {
            isValidGroup = true;
        }
        if (assignee == null){
            isValidAssignee = true;
        }
        for (GroupRepresentation groupRepresentation: groupList) {
            ownerRoleName = groupRepresentation.getRealmRoles().get(0);
            if (!isValidGroup && groupId.equals(groupRepresentation.getId())) {
                isValidGroup = true;
                if (!ownerRoleName.equals(roleName)) {
                    throw new Exception(INVALID_USER);
                }
                isValidRoleName = true;
            } else if (ownerRoleName.equals(roleName)) {
                isValidRoleName = true;
            }
            if (!isValidAssignee && isValidUser(userId, roleName, processVariable)){
                isValidAssignee = true;
            }
        }
        if (!isValidRoleName || !isValidGroup || !isValidAssignee) {
            throw new Exception(INVALID_USER);
        }
    }


    /**
     * This functions validates the object level user access for group id & roleName.
     *
     * @param groupId
     * @return void
     * @throws Exception
     *
     * @author Mandip Gothadiya
     */
    public void validateRequestResponse(ServletRequest request, String roleName, String groupId) throws Exception {
        validateRequestResponse(request, roleName, groupId, null, null);
    }
    
}
