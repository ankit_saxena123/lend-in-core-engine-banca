package com.kuliza.lending.engine_common.utils;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetaReadingUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetaReadingUtils.class);

    public static Map<String, Object> getActionProcessVariables(Object response, Map<String, String> responseMapping) {
        LOGGER.info("getActionProcessVariables response : " + response);
        LOGGER.info("getActionProcessVariables responseMapping : " + responseMapping);
        Map<String, Object> processVariables = new HashMap<>();
        for (String key : responseMapping.keySet()) {
            Object subResponse = response;
            for (String subResponseKey: responseMapping.get(key).split("\\.")) {
                if (subResponseKey.contains("[") && subResponseKey.contains("]")) {
                subResponse = ((List<Object>) response).get(CommonHelperFunctions.getIntegerValue(subResponseKey.substring(subResponseKey.indexOf("["), subResponseKey.indexOf("]"))));
                } else {
                    if (!((Map<String, Object>) subResponse).containsKey(subResponseKey)) {
                        subResponse = responseMapping.get(key);
                        break;
                    }
                    subResponse = ((Map<String, Object>) subResponse).get(subResponseKey);
                }
            }
            processVariables.put(key, subResponse);
        }
        LOGGER.info("getActionProcessVariables processVariables : " + processVariables);
        return processVariables;
    }

    public static final Map<String, Object> getSuccessActionProcessVariables(Object response, Object actionMeta) {
        Map<String, Object> processVariables = new HashMap<>();
        if (actionMeta != null) {
            List<Map<String, Object>> successActions = ((List<Map<String, Object>>) ((Map<String, Object>) actionMeta).get("successActions"));
            for (Map<String, Object> action : successActions) {
                if (action.get("type").equals("partialSave")) {
                    processVariables = getActionProcessVariables(response, (Map<String, String>) action.get("mapping"));
                }
            }
        }
        return processVariables;
    }

    public static final Map<String, Object> getErrorActionProcessVariables(Object response, Object actionMeta) {
        Map<String, Object> processVariables = new HashMap<>();
        if (actionMeta != null) {
            List<Map<String, Object>> successActions = ((List<Map<String, Object>>) ((Map<String, Object>) actionMeta).get("errorActions"));
            for (Map<String, Object> action : successActions) {
                if (action.get("type").equals("partialSave")) {
                    processVariables = getActionProcessVariables(response, (Map<String, String>) action.get("mapping"));
                }
            }
        }
        return processVariables;
    }
}
