package com.kuliza.lending.engine_common;

import com.itelg.spring.actuator.rabbitmq.health.RabbitQueueCheckHealthIndicator;
import com.itelg.spring.actuator.rabbitmq.metric.configuration.EnableRabbitMetrics;
import com.kuliza.lending.engine_common.utils.Constants;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

@Configuration
@EnableRabbit
@EnableRabbitMetrics
@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ")
public class RabbitMQConfiguration {
	 @Value(value = "${broker.host}")
	 private String hostname;
	
	 @Value(value="${broker.port}")
	 private Integer port;
	 
	 @Value(value="${broker.username}")
	 private String username;
	 
	 @Value(value="${broker.password}")
	 private String password;
	 
	 @Value(value="${broker.VHost}")
	 private String virtalHost;
	 
	 Queue auditQueue;
	 Queue genericQueue;
	 
	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public TopicExchange appExchange() {
		return new TopicExchange(Constants.EXCHANGE_NAME);
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public Queue appQueueGeneric() {
		genericQueue = new Queue(Constants.QUEUE_GENERIC_NAME);
		genericQueue.setAdminsThatShouldDeclare(amqpAdmin());
		return genericQueue;
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public Queue auditSpecificQueue() {
		auditQueue = new Queue(Constants.AUDIT_QUEUE_NAME);
		auditQueue.setAdminsThatShouldDeclare(amqpAdmin());
		return auditQueue;
	}
	
	@Bean
	public HealthIndicator rabbitQueueCheckHealthIndicator()
	{
	  RabbitQueueCheckHealthIndicator healthIndicator = new RabbitQueueCheckHealthIndicator();
	  healthIndicator.addQueueCheck(auditSpecificQueue(), 10000, 1);
	  return healthIndicator;
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public Binding declareBindingSpecific() {
		return BindingBuilder.bind(auditSpecificQueue()).to(appExchange()).with(Constants.AUDIT_ROUTING_KEY);
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public Binding declareBindingGeneric() {
		return BindingBuilder.bind(appQueueGeneric()).to(appExchange()).with(Constants.ROUTING_KEY);
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		connectionFactory.setPort(port);
		connectionFactory.setHost(hostname);
		return connectionFactory;
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public RetryOperationsInterceptor workMessagesRetryInterceptor() {
		return RetryInterceptorBuilder.stateless().maxAttempts(Constants.AUDIT_MAX_RETRY_COUNT)
				.backOffOptions(Constants.AUDIT_INITIAL_INTERVAL, Constants.AUDIT_MULTIPLIER,
						Constants.AUDIT_MAX_INTERVAL)
				.recoverer(new RejectAndDontRequeueRecoverer()).build();
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
		rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
		return rabbitTemplate;
	}

	@Bean(name = "rabbitListenerContainerFactory")
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public SimpleRabbitListenerContainerFactory rabbitListenerContainerlistenerFactory() {
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory());
		factory.setAutoStartup(true);
		factory.setPrefetchCount(1);
		factory.setAdviceChain(workMessagesRetryInterceptor());
		factory.setMessageConverter(producerJackson2MessageConverter());
		return factory;
	}

	@Bean
	@ConditionalOnProperty(name="broker.name", havingValue="RABBITMQ",matchIfMissing=true)
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
}
