package com.kuliza.lending.engine_common.pojo;

public class CustomActionHeaders {

    String key;
    String value;

    public CustomActionHeaders() {
        super();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
