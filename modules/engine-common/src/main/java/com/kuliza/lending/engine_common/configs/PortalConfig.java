package com.kuliza.lending.engine_common.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "portal")
public class PortalConfig {
	String protocol;
	String host;
	Integer port;
	String subURL;
	int timeout;

	@Value("${portal.audit.service.init.enabled}")
	boolean auditServiceInitEnabled;

	@Value("${portal.audit.service.enabled}")
	boolean auditServiceEnabled;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getSubURL() {
		return subURL;
	}

	public void setSubURL(String subURL) {
		this.subURL = subURL;
	}

	public boolean isAuditServiceInitEnabled() {
		return auditServiceInitEnabled;
	}

	public void setAuditServiceInitEnabled(boolean auditServiceInitEnabled) {
		this.auditServiceInitEnabled = auditServiceInitEnabled;
	}

	public boolean isAuditServiceEnabled() {
		return auditServiceEnabled;
	}

	public void setAuditServiceEnabled(boolean auditServiceEnabled) {
		this.auditServiceEnabled = auditServiceEnabled;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
}
