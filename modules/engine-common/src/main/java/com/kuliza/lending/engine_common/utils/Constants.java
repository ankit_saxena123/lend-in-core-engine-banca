package com.kuliza.lending.engine_common.utils;

public class Constants {

	public static final String PORTAL_INITIATE_API_URL = "/lending/case/initiate";
	public static final String BACK_TO_JOURNEY_API_URL = "/lending/journey/back-to-journey";
	public static final String JOURNEY_INITIATE_BY_PORTAL_API_URL = "/lending/journey/initiate-by-portal";
    public static final String MASTER_DATA = "/get-master-data/{slug}";

	public static final String EXCHANGE_NAME = "exchange";
	public static final String QUEUE_GENERIC_NAME = "generic-queue";
	public static final String QUEUE_SPECIFIC_NAME = "task-queue";
	public static final String AUDIT_ROUTING_KEY = "route.audit";
	public static final String AUDIT_QUEUE_NAME = "audit-queue";
	public static final String ROUTING_KEY = "route.generic";
	public static final Integer AUDIT_MAX_RETRY_COUNT = 3;
	public static final Integer AUDIT_INITIAL_INTERVAL = 2000;
	public static final Integer AUDIT_MULTIPLIER = 2;
	public static final Integer AUDIT_MAX_INTERVAL = 10000;
	public static final String JMS_AUDIT_ROUTING_KEY = "route.jms.auditQueue";
	
	// Constants for RabbitMQ configuration
	public static final String BROKER_RABBIT_MQ_HOST = "35.200.152.178";
	public static final Integer BROKER_RABBIT_MQ_PORT = 5672;
	public static final String BROKER_RABBIT_MQ_USERNAME = "admin";
	public static final String BROKER_RABBIT_MQ_PASSWORD = "test";
	public static final String BROKET_RABBIT_MQ_VIRTUAL_HOST = "/";
	
	//Constants for ActiveMQ configuration
	public static final String BROKER_ACTIVEMQ_URL = "tcp://localhost:61616"; 
	public static final String BROKER_ACTIVEMQ_USERNAME = "admin"; 
	public static final String BROKER_ACTIVEMQ_PASSWORD = "admin";
	
	//Constants for JMS
	public static final String JMS_CONTAINER_CONCURRENCY = "5-10";
	
	public static final String EMPTY_STRING = "";

	//Constants for Logging
	public static final String LOG_RESPONSE = "For user {} for processInstanceId {} for ApplicationId {} for LosId {} Outgoing requestbody is: {}";
	public static final String LOG_REQUEST = "For user {} for processInstanceId {} for ApplicationId {} for LosId {} Incoming response is: {}";

}
