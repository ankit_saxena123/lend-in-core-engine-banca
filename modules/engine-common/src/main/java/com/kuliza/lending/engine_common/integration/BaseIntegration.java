package com.kuliza.lending.engine_common.integration;

import static com.kuliza.lending.engine_common.utils.Constants.LOG_REQUEST;
import static com.kuliza.lending.engine_common.utils.Constants.LOG_RESPONSE;

import com.kuliza.lending.common.annotations.LogMethodDetails;
import java.util.Map;
import java.util.function.Function;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import com.kuliza.lending.common.connection.HTTPConnection;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.common.utils.Constants;

public abstract class BaseIntegration implements JavaDelegate {

	private static final Logger logger = LoggerFactory.getLogger(BaseIntegration.class);

	public void saveSingleObjectInProcessInstance(Boolean flag, DelegateExecution execution, String variableId,
			Object dataToSave) {
		if (flag) {
			if (execution == null || variableId == null) {
				throw new IllegalArgumentException(
						"DelegateExecution argument or variable Id argument cannot be null if request payload or response object needs to be saved in process instance.");
			} else {
				execution.setVariable(variableId, dataToSave);
			}
		}
	}

	@LogMethodDetails(userId = "userIdKeyInProcess", businessKey = "applicationIdKeyInProcess")
	public Object callApi(String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod,
			String protocol, String url, Integer port, String suburl, String token, DelegateExecution execution,
			Map<String, Object> dataToBuildRequestPayload,
			Function<Map<String, Object>, JSONObject> buildRequestPayload,
			Function<HTTPResponse, Object> successCallback, Function<HTTPResponse, Object> errorCallback,
			Boolean saveRequestPayloadInProcess, String requestPayloadIdInProcess, Boolean saveResponseObjectInProcess,
			String responseObjectIdInProcess) throws Exception {
		String userId = execution != null
				? execution.getVariable(userIdKeyInProcess != null && !userIdKeyInProcess.equals("")
						? userIdKeyInProcess : Constants.PROCESS_TASKS_ASSIGNEE_KEY) != null
								? execution.getVariable(userIdKeyInProcess != null && !userIdKeyInProcess.equals("")
										? userIdKeyInProcess : Constants.PROCESS_TASKS_ASSIGNEE_KEY).toString()
								: ""
				: "";
		String processInstanceId = execution != null ? execution.getProcessInstanceId() : "";
		String appId = execution != null
				? execution.getVariable(applicationIdKeyInProcess != null && !applicationIdKeyInProcess.equals("")
						? applicationIdKeyInProcess : Constants.APPLICATION_ID_KEY) != null
								? execution
										.getVariable(applicationIdKeyInProcess != null
												&& !applicationIdKeyInProcess.equals("")
														? applicationIdKeyInProcess : Constants.APPLICATION_ID_KEY)
										.toString()
								: execution.getProcessInstanceBusinessKey()
				: "";
		String losId = execution != null ? execution.getProcessInstanceBusinessKey() : "";
		HTTPResponse response = null;
		String requestBody = null;
		if (buildRequestPayload != null) {
			requestBody = buildRequestPayload.apply(dataToBuildRequestPayload).toString();
			logger.info(LOG_REQUEST, userId, processInstanceId, appId, losId, requestBody);
			saveSingleObjectInProcessInstance(saveRequestPayloadInProcess, execution, requestPayloadIdInProcess,
					requestBody);
		}
		response = HTTPConnection.send(protocol, url, port, suburl, requestBody, httpMethod.name(), token);
		logger.info(LOG_RESPONSE, userId, processInstanceId, appId, losId, response);
		saveSingleObjectInProcessInstance(saveResponseObjectInProcess, execution, responseObjectIdInProcess,
				response.getResponse());
		if (response.getStatusCode() != HttpStatus.OK.value()
				&& response.getStatusCode() != HttpStatus.ACCEPTED.value()) {
			return successCallback.apply(response);
		}
		return errorCallback.apply(response);
	}

	public Object callApi(String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod,
			String protocol, String url, Integer port, String suburl, DelegateExecution execution,
			Map<String, Object> dataToBuildRequestPayload,
			Function<Map<String, Object>, JSONObject> buildRequestPayload,
			Function<HTTPResponse, Object> successCallback, Function<HTTPResponse, Object> errorCallback,
			Boolean saveRequestPayloadInProcess, String requestPayloadIdInProcess, Boolean saveResponseObjectInProcess,
			String responseObjectIdInProcess) throws Exception {
		return callApi(userIdKeyInProcess, applicationIdKeyInProcess, httpMethod, protocol, url, port, suburl, null,
				execution, dataToBuildRequestPayload, buildRequestPayload, successCallback, errorCallback,
				saveRequestPayloadInProcess, requestPayloadIdInProcess, saveResponseObjectInProcess,
				responseObjectIdInProcess);
	}

	// For Api without Request Body without Token
	public Object callApi(String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod,
			String protocol, String url, Integer port, String suburl, DelegateExecution execution,
			Function<HTTPResponse, Object> successCallback, Function<HTTPResponse, Object> errorCallback,
			Boolean saveResponseObjectInProcess, String responseObjectIdInProcess) throws Exception {
		return callApi(userIdKeyInProcess, applicationIdKeyInProcess, httpMethod, protocol, url, port, suburl, null,
				execution, null, null, successCallback, errorCallback, false, null, saveResponseObjectInProcess,
				responseObjectIdInProcess);
	}

	// For API call without request body with token
	public Object callApi(String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod,
			String protocol, String url, Integer port, String suburl, String token, DelegateExecution execution,
			Function<HTTPResponse, Object> successCallback, Function<HTTPResponse, Object> errorCallback,
			Boolean saveResponseObjectInProcess, String responseObjectIdInProcess) throws Exception {
		return callApi(userIdKeyInProcess, applicationIdKeyInProcess, httpMethod, protocol, url, port, suburl, token,
				execution, null, null, successCallback, errorCallback, false, null, saveResponseObjectInProcess,
				responseObjectIdInProcess);
	}

	// For API call without request body with token and not saving request
	// response in execution
	public Object callApi(String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod,
			String protocol, String url, Integer port, String suburl, String token, DelegateExecution execution,
			Function<HTTPResponse, Object> successCallback, Function<HTTPResponse, Object> errorCallback)
			throws Exception {
		return callApi(userIdKeyInProcess, applicationIdKeyInProcess, httpMethod, protocol, url, port, suburl, token,
				execution, null, null, successCallback, errorCallback, false, null, false, null);
	}

	// For API call with request body with token and not saving request
	// response in execution
	public Object callApi(String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod,
			String protocol, String url, Integer port, String suburl, String token, DelegateExecution execution,
			Function<Map<String, Object>, JSONObject> buildRequestPayload,
			Function<HTTPResponse, Object> successCallback, Function<HTTPResponse, Object> errorCallback)
			throws Exception {
		return callApi(userIdKeyInProcess, applicationIdKeyInProcess, httpMethod, protocol, url, port, suburl, token,
				execution, null, buildRequestPayload, successCallback, errorCallback, false, null, false, null);
	}

}
