package com.kuliza.lending.engine_common.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "journey")
public class JourneyConfig {

  String protocol;
  String host;
  Integer port;
  String subURL;

  public String getProtocol() {
    return protocol;
  }

  public void setProtocol(String protocol) {
    this.protocol = protocol;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getSubURL() {
    return subURL;
  }

  public void setSubURL(String subURL) {
    this.subURL = subURL;
  }

}
