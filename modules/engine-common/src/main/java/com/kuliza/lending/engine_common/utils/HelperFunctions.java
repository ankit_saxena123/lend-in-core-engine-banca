package com.kuliza.lending.engine_common.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.DmsConfig;

public class HelperFunctions {

    	/**
	 * Extract a String from the given TextMessage.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting String
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static String extractStringFromMessage(TextMessage message) throws JMSException {
		return message.getText();
	}

	/**
	 * Extract a byte array from the given {@link BytesMessage}.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting byte array
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static byte[] extractByteArrayFromMessage(BytesMessage message) throws JMSException {
		byte[] bytes = new byte[(int) message.getBodyLength()];
		message.readBytes(bytes);
		return bytes;
	}

	/**
	 * Extract a Map from the given {@link MapMessage}.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting Map
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static Map<String, Object> extractMapFromMessage(MapMessage message) throws JMSException {
		Map<String, Object> map = new HashMap<>();
		Enumeration<?> en = message.getMapNames();
		while (en.hasMoreElements()) {
			String key = (String) en.nextElement();
			map.put(key, message.getObject(key));
		}
		return map;
	}

	/**
	 * Extract a Serializable object from the given {@link ObjectMessage}.
	 * 
	 * @param message
	 *            the message to convert
	 * @return the resulting Serializable object
	 * @throws JMSException
	 *             if thrown by JMS methods
	 */
	public static Serializable extractSerializableFromMessage(ObjectMessage message) throws JMSException {
		return message.getObject();
	}

    public static String getDMSUrl(){
        return StaticContextAccessor.getBean(DmsConfig.class).getProtocol() + "://" +
                StaticContextAccessor.getBean(DmsConfig.class).getHost() + ":" +
                StaticContextAccessor.getBean(DmsConfig.class).getPort() +
                StaticContextAccessor.getBean(DmsConfig.class).getSubUrl();
    }
    public static List<Map<String, Object>> getMasterDataList(Map<String, Object> map) {
		List<Map<String,Object> > masterDataList = new ArrayList<>();
		for(String key : map.keySet()) {
			Map<String,Object> m = new HashMap<>();
			if(key == null || key.equals(""))
				continue;
			m.put("key" , key);
			m.put("value" , map.get(key));
			masterDataList.add(m);
		}
		return masterDataList;
    }
}
