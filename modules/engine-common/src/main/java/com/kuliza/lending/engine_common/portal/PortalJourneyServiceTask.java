package com.kuliza.lending.engine_common.portal;

import static com.kiliza.lending.engine_common.utils.Constants.BACK_TO_JOURNEY_API_URL;
import static com.kiliza.lending.engine_common.utils.Constants.PORTAL_INITIATE_API_URL;
import static com.kuliza.lending.common.utils.Constants.NO_TASKS_MESSAGE;
import static com.kuliza.lending.engine_common.utils.Constants.JOURNEY_INITIATE_BY_PORTAL_API_URL;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakConfig;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.connection.HTTPConnection;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.configs.JourneyConfig;
import com.kuliza.lending.engine_common.configs.PortalConfig;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("PortalJourneyServiceTask")
public class PortalJourneyServiceTask {

  private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PortalJourneyServiceTask.class);

  @Autowired
  private RuntimeService runtimeService;

  @Autowired
  private KeyCloakManager keyCloakManager;

  @Autowired
  private PortalConfig portalConfig;

  @Autowired
  private KeyCloakConfig keyCloakConfig;

  @Autowired
  private JourneyConfig journeyConfig;

  public Object firePortal(String processInstanceId, String destinationKey, String destinationType, Boolean portfolioFlag) throws IOException, InterruptedException {
    ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
        .processInstanceId(processInstanceId).singleResult();
    Map<String, Object> requestPayload = new HashMap<>();
    Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
    requestPayload.put("variablesToSave", processVariables);
    requestPayload.put("portalType", destinationType);
    requestPayload.put("sourceProcessName", processVariables.get("journeyType"));
    requestPayload.put("sourceType", "JOURNEY");
    requestPayload.put("sourceId", processInstanceId);
    JSONObject request = new JSONObject(requestPayload);
    LOGGER.info("firePortal requestPayload : " + requestPayload);
    LOGGER.info("firePortal request : " + request);
    String queryParams = Constants.CASENAME + "="+destinationKey+"&" + Constants.APPLICATION_NUMBER + "=" +
            processInstance.getBusinessKey() + "&" + Constants.PORTFOLIO_FLAG + "=" + portfolioFlag;
    HTTPResponse response = sendPortalRequest(request, queryParams);
    LOGGER.info("firePortal response : " + response.getResponse());
    if(response.getStatusCode() == 200 || (response.getStatusCode() == 400 && CommonHelperFunctions.getHashMapFromJsonString(response.getResponse()).getOrDefault("message", "").equals(NO_TASKS_MESSAGE))){
      return response;
    }
    throw new InterruptedException();
  }
  
  public Object firePortal(String processInstanceId, String destinationKey, String destinationType, String sourceType,
		  String sourceId, String sourceProcessName, String applicationNumberVariable, Boolean portfolioFlag) throws IOException, InterruptedException {
	    Map<String, Object> requestPayload = new HashMap<>();
	    Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
	    requestPayload.put("variablesToSave", processVariables);
	    requestPayload.put("portalType", destinationType);
	    requestPayload.put("sourceProcessName", sourceProcessName);
	    requestPayload.put("sourceType", sourceType);
	    requestPayload.put("sourceId", sourceId);
	    JSONObject request = new JSONObject(requestPayload);
	    LOGGER.info("firePortal requestPayload : " + requestPayload);
	    LOGGER.info("firePortal request : " + request);
	    String queryParams = Constants.CASENAME + "="+destinationKey+"&" + Constants.APPLICATION_NUMBER + "=" +
	    		CommonHelperFunctions.getStringValue(processVariables.get(applicationNumberVariable)) + "&" + Constants.PORTFOLIO_FLAG + "=" + portfolioFlag;
	    HTTPResponse response = sendPortalRequest(request, queryParams);
	    LOGGER.info("firePortal response : " + response.getResponse());
	    if(response.getStatusCode() == 200 || (response.getStatusCode() == 400 && CommonHelperFunctions.getHashMapFromJsonString(response.getResponse()).getOrDefault("message", "").equals(NO_TASKS_MESSAGE))){
	      return response;
	    }
	    throw new InterruptedException();
	  }

  public HTTPResponse sendPortalRequest(JSONObject requestData, String queryParams) {
    String requestBody = requestData.toString();
    HTTPResponse response = null;
    try {
      String token = getAdminAccessToken();
      response = HTTPConnection
          .sendPOST(portalConfig.getProtocol(), portalConfig.getHost(), portalConfig.getPort(), portalConfig.getSubURL() + PORTAL_INITIATE_API_URL + "?" + queryParams, requestBody, token);
    } catch (Exception e) {
      response = new HTTPResponse(Constants.FAILURE_CODE, e.toString());
    }
    return response;

  }

  public HTTPResponse sendJourneyRequest(JSONObject requestData) {
    String requestBody = requestData.toString();
    HTTPResponse response = null;
    try {
      String token = getAdminAccessToken();
      response = HTTPConnection
          .sendPOST(journeyConfig.getProtocol(), journeyConfig.getHost(), journeyConfig.getPort(), journeyConfig.getSubURL() + BACK_TO_JOURNEY_API_URL, requestBody, token);
    } catch (Exception e) {
      response = new HTTPResponse(Constants.FAILURE_CODE, e.toString());
      e.printStackTrace();
    }
    return response;

  }

  public String getAdminAccessToken() {
    String token = null;
    ApiResponse response = null;
    try {
       response = keyCloakManager.loginWithEmailAndPassword(keyCloakConfig.getAdminEmail(), keyCloakConfig.getAdminPassword(), false);
       if (response.getStatus() == 200) {
         token = CommonHelperFunctions.getStringValue(((Map<String, Object>)response.getData()).getOrDefault("access_token", ""));
       }
      } catch (Exception e) {
      e.printStackTrace();
    }
    return token;

  }
  
  public HTTPResponse sendJourneyRequestfromPortal(JSONObject requestData) {
	    String requestBody = requestData.toString();
	    HTTPResponse response = null;
	    try {
	      String token = getAdminAccessToken();
	      response = HTTPConnection
	          .sendPOST(journeyConfig.getProtocol(), journeyConfig.getHost(), journeyConfig.getPort(), journeyConfig.getSubURL() + JOURNEY_INITIATE_BY_PORTAL_API_URL, requestBody, token);
	    } catch (Exception e) {
	      response = new HTTPResponse(Constants.FAILURE_CODE, e.toString());
	      e.printStackTrace();
	    }
	    return response;

	  }

}
