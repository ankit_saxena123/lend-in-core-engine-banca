package com.kuliza.lending.engine_common.utils;

public class EnumConstants {
	public enum Events {
		IncomingRequestEvent, OutgoingRequestEvent, IncomingResponseEvent, OutgoingResponseEvent, SetVariablesEvent, SubmitFormEvent;
	}

}
