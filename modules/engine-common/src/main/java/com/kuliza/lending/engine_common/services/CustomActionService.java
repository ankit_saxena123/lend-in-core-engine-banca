package com.kuliza.lending.engine_common.services;

import com.kuliza.lending.common.connection.UnirestConnection;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.JourneyConfig;
import com.kuliza.lending.engine_common.configs.PortalConfig;
import com.kuliza.lending.engine_common.models.CustomURLMapper;
import com.kuliza.lending.engine_common.models.CustomURLMapperDao;
import com.kuliza.lending.engine_common.pojo.CallableBulkActionRequestThread;
import com.kuliza.lending.engine_common.pojo.CustomActionHeaders;
import com.kuliza.lending.engine_common.pojo.CustomActionRequest;
import com.kuliza.lending.engine_common.pojo.CustomBulkActionRequest;
import com.kuliza.lending.engine_common.portal.PortalJourneyServiceTask;
import com.kuliza.lending.engine_common.utils.CustomActionHelperFunctions;
import com.mashape.unirest.http.HttpResponse;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.engine.RuntimeService;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.kuliza.lending.engine_common.utils.EngineCommonConstants.*;
import static com.kuliza.lending.engine_common.utils.MetaReadingUtils.getErrorActionProcessVariables;
import static com.kuliza.lending.engine_common.utils.MetaReadingUtils.getSuccessActionProcessVariables;

@Service
public class CustomActionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomActionService.class);

    @Autowired
    private CmmnRuntimeService cmmnRuntimeService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private CustomURLMapperDao customURLMapperDao;

    @Autowired
    private EngineCommonService engineCommonService;
    
    @Autowired
    private PortalJourneyServiceTask portalJourneyServiceTask;

    public ApiResponse fireCustomAction(HttpServletRequest request, String slug, CustomActionRequest customActionRequest, String contentType, String token,
                                        String caseInstanceId, String processInsatnceId, String roleName, String userId) throws Exception
    {
        Map<String, Object> processVariables = getProcessVariables(caseInstanceId, processInsatnceId);
        if (caseInstanceId != null && !caseInstanceId.isEmpty()) {
            engineCommonService.validateRequestResponse(request, roleName, null, userId, processVariables);
        }
        ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, CUSTOM_URL_NOT_EXISTS);
        CustomURLMapper customUrlMapper = customURLMapperDao.findBySlugAndIsDeleted(slug, false);
        if(customUrlMapper != null){
            String url = CustomActionHelperFunctions.setPathVariables(customUrlMapper.getUrl(), customActionRequest.getPathVariables());
            url = CustomActionHelperFunctions.setRequestParams(url, customActionRequest.getRequestParams());
            Map<String, String> headers =  new HashMap<>();
            if(customUrlMapper.getId() < 0 && !customUrlMapper.isExternalApi()){
                url = getBaseURL(caseInstanceId, processInsatnceId, url);
                System.out.println("===========URL=======" + url);
                headers.put("Content-Type", contentType);
                if(!customUrlMapper.isOpenApi()) {
                    headers.put("Authorization", token);
                }
            }
            else{
                for(CustomActionHeaders customActionHeader : customUrlMapper.getHeaders()){
                    headers.put(customActionHeader.getKey(), customActionHeader.getValue());
                }
                if(!customUrlMapper.isOpenApi()) {
                    headers.put("Authorization", "Bearer " + portalJourneyServiceTask.getAdminAccessToken());
                }
            }
            response = getApiResponse(customUrlMapper, customActionRequest, headers, url);
        }
        Integer actionCount = 0;
        Map<String, Object> responseData = new HashMap<>();
        Map<String, Object> processVariablesToSave = new HashMap<>();
        if(customActionRequest.getKey() != null && !customActionRequest.getKey().isEmpty()){
            actionCount = CommonHelperFunctions.getIntegerValue(processVariables.get(
                    customActionRequest.getKey() + CUSTOM_ACTION_COUNT_SUFFIX));
        }
        responseData.put("actionValue", false);
        responseData.put("data", response.getData());
        if(response.getStatus() == 200){
            responseData.put("actionValue", true);
            actionCount += 1;
            if(customActionRequest.getKey() != null && !customActionRequest.getKey().isEmpty()){
                processVariablesToSave.putAll(getSuccessActionProcessVariables(responseData, customActionRequest.getActionMeta()));
                processVariablesToSave.put(customActionRequest.getKey(), true);
                processVariablesToSave.put(customActionRequest.getKey() + CUSTOM_ACTION_COUNT_SUFFIX, actionCount);
                processVariablesToSave.put(customActionRequest.getKey() + CUSTOM_ACTION_TIMESTAMP_SUFFIX, CommonHelperFunctions.getCurrentDateInFormat(CUSTOM_ACTION_DATE_FORMAT));
                processVariablesToSave.put(customActionRequest.getKey() + CUSTOM_ACTION_COMMENT_SUFFIX, customActionRequest.getMessage());
                setProcessVariables(caseInstanceId, processInsatnceId, processVariablesToSave);
            }
        } else {
            if (customActionRequest.getActionMeta() != null) {
                processVariables.putAll(getErrorActionProcessVariables(responseData, customActionRequest.getActionMeta()));
            }
        }
        responseData.put("actionCount", actionCount);
        response.setData(responseData);
        return response;
    }

    public ApiResponse fireBulkCustomActions(HttpServletRequest request, String slug, List<CustomBulkActionRequest> customBulkActionRequestList, String contentType,
                                             String token, String roleName, String userId) {
        ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, ERROR_BUILDING_THREAD);
        LOGGER.info("Starting Bulk Api with slug = " + slug);
        if (customBulkActionRequestList != null && customBulkActionRequestList.size() > 0) {
            int threadPoolSize = THREAD_POOL_SIZE;
            int customBulkActionRequestListSize = customBulkActionRequestList.size();
            ExecutorService threadPool = Executors.newFixedThreadPool(threadPoolSize);
            LOGGER.info("size of custom actionUrl body = " + customBulkActionRequestListSize);
            CallableBulkActionRequestThread[] callableBulkActionRequestThreads = new CallableBulkActionRequestThread[customBulkActionRequestListSize];
            Map<String, ApiResponse> result = new HashMap<>();
            Future<ApiResponse>[] futures = new Future[customBulkActionRequestListSize];
            for (int i = 0; i < customBulkActionRequestListSize; ++i) {
                LOGGER.info("Custom action url body case instance id = " + customBulkActionRequestList.get(i).getCaseInstanceId());
                LOGGER.info("Custom action url body request body = " + customBulkActionRequestList.get(i).getCustomActionRequest().getRequestBody());LOGGER.info("Custom action url body case instance id = " + customBulkActionRequestList.get(i).getCaseInstanceId());
                LOGGER.info("Custom action url body request body = " + customBulkActionRequestList.get(i).getCustomActionRequest().getRequestBody());
                RequestAttributes context =
                        RequestContextHolder.currentRequestAttributes();
                callableBulkActionRequestThreads[i] = new CallableBulkActionRequestThread(request, slug, this,
                        customBulkActionRequestList.get(i), contentType, token, context, roleName, userId);
                futures[i] = threadPool.submit(callableBulkActionRequestThreads[i]);
            }
            for (int i = 0; i < customBulkActionRequestListSize; i++) {
                try {
                    LOGGER.info("future at index = " + i);
                    LOGGER.info("timeout = " + StaticContextAccessor.getBean(PortalConfig.class).getTimeout());
                    LOGGER.info("future at = " + futures[i].get().getMessage());
                    result.put(customBulkActionRequestList.get(i).getCaseInstanceId(),
                            futures[i].get(StaticContextAccessor.getBean(PortalConfig.class).getTimeout(),
                                    TimeUnit.MILLISECONDS));
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.warn("exception = " + e.toString());
                    result.put(customBulkActionRequestList.get(i).getCaseInstanceId(),
                            new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
                                    THREAD_ID_FAILED + " : " +
                                            customBulkActionRequestList.get(i).getCaseInstanceId()));
                    continue;
                }
            }
            threadPool.shutdown();
            response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, result);
        }
        return response;
    }

    public ApiResponse getApiResponse(CustomURLMapper customUrlMapper, CustomActionRequest customActionRequest,
                                      Map<String, String> headers, String customUrl) {
        HttpResponse<String> httpResponse = null;
        if (StaticContextAccessor.getBean(PortalConfig.class).isAuditServiceEnabled()) {
            httpResponse = getHttpResponse(customUrlMapper,
                    customActionRequest, headers, customUrl);
        } else {
            httpResponse = getHttpResponse(customUrlMapper, customActionRequest, headers, customUrl);
        }
        ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
        if (httpResponse != null) {
            if(httpResponse.getStatus() == 200) {
                try {
                    Object responseObject = CommonHelperFunctions.getHashMapFromJsonString(CommonHelperFunctions.
                            getStringValue(httpResponse.getBody()));
                    response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseObject);
                } catch (Exception e) {
//				TODO : Check for JSONArray as response
                    response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, httpResponse.getBody());
                } }else {
                try {
                    Object responseObject = CommonHelperFunctions
                            .getHashMapFromJsonString(CommonHelperFunctions.
                                    getStringValue(httpResponse.getBody()));
                    response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.SOMETHING_WRONG_MESSAGE, responseObject);
                } catch (Exception e) {
//				TODO : Check for JSONArray as response
                    response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.SOMETHING_WRONG_MESSAGE,
                            httpResponse.getBody());
                }
            }
        }
        return response;
    }

    @SuppressWarnings("unchecked")
    public HttpResponse getHttpResponse(CustomURLMapper customUrlMapper, CustomActionRequest customActionRequest,
                                        Map<String, String> headers, String customUrl){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        String requestId = "";
        if (request != null && !CommonHelperFunctions.getStringValue(request.getAttribute("requestId")).isEmpty()) {
            requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
        }
        if (!requestId.isEmpty()) {
            if (customUrl.contains(CASE_SET_VARIABLE_API_URL)) {
                customUrl = customUrl + "&requestId=" + requestId;
            } else if (customUrl.contains(CASE_SUBMIT_FORM_API_URL)) {
                customUrl = customUrl + "?requestId=" + requestId;
            }
        }
        HttpResponse<String> httpResponse = null;
        Map<String, Object> requestBody = new HashMap<>();
        JSONArray requestArrayBody = new JSONArray();
        if(customActionRequest.getRequestBody() != null && customActionRequest.getRequestBody() instanceof Map) {
            requestBody = (Map<String, Object>) customActionRequest.getRequestBody();
        }else if(customActionRequest.getRequestBody() != null && customActionRequest.getRequestBody() instanceof JSONArray
                || customActionRequest.getRequestBody() instanceof ArrayList){
            requestArrayBody = CommonHelperFunctions.toJson((List<Object>) customActionRequest.getRequestBody());
        }
        switch (customUrlMapper.getRequestType()){
            case GET:
                httpResponse = UnirestConnection.sendGET(headers, customUrl);
                break;
            case DELETE:
                httpResponse = UnirestConnection.sendDELETE(requestBody, headers, customUrl);
                break;
            case POST:
                if(customActionRequest.getRequestBody() instanceof Map) {
                    httpResponse = UnirestConnection.sendPOST(requestBody, headers, customUrl);
                }
                else if(customActionRequest.getRequestBody() != null && customActionRequest.getRequestBody() instanceof JSONArray
                        || customActionRequest.getRequestBody() instanceof ArrayList){
                    httpResponse = UnirestConnection.sendPOST(requestArrayBody, headers, customUrl);
                }
                break;
            case PUT:
                if(customActionRequest.getRequestBody() instanceof Map) {
                    httpResponse = UnirestConnection.sendPUT(requestBody, headers, customUrl);
                }
                else{
                    httpResponse = UnirestConnection.sendPUT(requestArrayBody, headers, customUrl);
                }
                break;
        }
        return httpResponse;
    }

    public Map<String, Object> getProcessVariables(String caseInstanceId, String processInstanceId) {
        Map<String, Object> processVariables = new HashMap<>();
        if (caseInstanceId != null && !caseInstanceId.isEmpty()) {
            processVariables = cmmnRuntimeService.getVariables(caseInstanceId);
        } else if (processInstanceId != null && !processInstanceId.isEmpty()) {
            processVariables = runtimeService.getVariables(processInstanceId);
        }
        return processVariables;
    }

    public void setProcessVariables(String caseInstanceId, String processInstanceId, Map<String, Object> processVariables) {
        if (caseInstanceId != null && !caseInstanceId.isEmpty()) {
            cmmnRuntimeService.setVariables(caseInstanceId, processVariables);
        } else if (processInstanceId != null && !processInstanceId.isEmpty()) {
            runtimeService.setVariables(processInstanceId, processVariables);
        }
    }

    public String getBaseURL(String caseInstanceId, String processInstanceId, String url) {

        if (processInstanceId != null && !processInstanceId.isEmpty()) {
            url = StaticContextAccessor.getBean(PortalConfig.class).getProtocol() + "://"
                    + StaticContextAccessor.getBean(JourneyConfig.class).getHost() +":"
                    + StaticContextAccessor.getBean(JourneyConfig.class).getPort()
                    + StaticContextAccessor.getBean(JourneyConfig.class).getSubURL() + url;
        } else {
            url = StaticContextAccessor.getBean(PortalConfig.class).getProtocol() + "://"
                    + StaticContextAccessor.getBean(PortalConfig.class).getHost() +":"
                    + StaticContextAccessor.getBean(PortalConfig.class).getPort()
                    + StaticContextAccessor.getBean(PortalConfig.class).getSubURL() + url;
        }
        return url;
    }
}
