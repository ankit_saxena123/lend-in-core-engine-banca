package com.kuliza.lending.engine_common.utils;

public class EngineCommonConstants {
    public static final String LENDIN_URL = "/lending";


    public static final String ECC_UPLOAD_FILE = "This function is used to upload file using dms.";
    public static final String ECC_DOWNLOAD_FILE = "This function is used to download file using dms.";

    /********** PORTAL CONSTANT ***********/
    public static final String CLAIM_BUCKET_KEY = "claim";
    public static final String ROLE_ASSIGNEE_VALUE = "Assignee";
    public static final String STATUS_VARIABLE_KEY = "Status";

    /********** CUSTOM ACTION CONSTANT ***********/
    public static final String CUSTOM_ACTION_DATE_FORMAT = "MM-dd-yyyy hh:mm:ss";
    public static final String BULK_ACTION_URL = "/fire-bulk-action/";
    public static final String CUSTOM_ACTION_URL = "/fire-custom-action/";
    public static final String PORTAL_BULK_ACTION_URL = "/portal/fire-bulk-action/";
    public static final String PORTAL_CUSTOM_ACTION_URL = "/portal/fire-custom-action/";
    public static final String JOURNEY_CUSTOM_ACTION_URL = "/journey/fire-custom-action/";
    public static final String CUSTOM_URL_NOT_EXISTS = "Custom url with this is does not exist";
    public static final String CUSTOM_ACTION_COUNT_SUFFIX = "Count";
    public static final String CUSTOM_ACTION_TIMESTAMP_SUFFIX = "Timestamp";
    public static final String CUSTOM_ACTION_COMMENT_SUFFIX = "Comment";
    public static final String THREAD_ID_FAILED = "Thread with this caseInstanceId failed to get response due to timeout error";
    public static final String ERROR_BUILDING_THREAD = "Unable to build thread";
    public static final int THREAD_POOL_SIZE = 3;

    // Constants for Portal Audit
    public static final String REQUEST_PATH_VARIABLES = "org.springframework.web.servlet.View.pathVariables";
    public static final String ROLE_NAME_KEY = "roleName";
    public static final String REQUEST_ID_KEY = "requestId";
    public static final String PATH_VARIABLE_KEY = "pathVariables";
    public static final String CASE_INSTANCE_ID = "caseInstanceId";
    public static final String TAB_KEY = "tabKey";
    public static final String COMMENT_MESSAGE_KEY = "message";
    public static final String AUDIT_COMMENT_IDENTIFIER = "auditActionComment";
    public static final String CUSTOM_API_URL = "/lending/portal/fire-custom-action/*";
    public static final String CASE_SET_VARIABLE_API_URL = "/lending/case/submit-variables";
    public static final String CASE_SUBMIT_FORM_API_URL = "/lending/case/submit-form";
    public static final String BULK_SUBMIT_API = "/lending/portal/fire-bulk-action/*";

    public static final String ECC_GET_QUERY_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"count\": 12\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String ECC_UPLOAD_FILE_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"error\": \"\",\n" +
            "        \"response\": {\n" +
            "            \"description\": \"\",\n" +
            "            \"document_type\": 1,\n" +
            "            \"id\": 6974,\n" +
            "            \"label\": \"agg.txt\",\n" +
            "            \"language\": \"eng\"\n" +
            "        }\n" +
            "    }\n" +
            "}";

    public static final String ECC_FETCH_MASTER_API_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"pincode\": 395010,\n" +
            "            \"city\": \"Surat\",\n" +
            "            \"state\": \"Gujarat\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"pincode\": 100937,\n" +
            "            \"city\": \"Mumbai\",\n" +
            "            \"state\": \"Maharastra\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"pincode\": 560068,\n" +
            "            \"city\": \"Bangalore\",\n" +
            "            \"state\": \"Karanataka\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"pincode\": 600127,\n" +
            "            \"city\": \"Chennai\",\n" +
            "            \"state\": \"Tamilnadu\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"errorMessage\": \"\"\n" +
            "}";

    public static final String ECC_UPLOAD_FILE_SELECT = "Choose the file to be selected";
    public static final String ECC_DOCUMENT_TYPE = "Document Type";
    public static final String ECC_DOCUMENT_ID = "Document ID";
    public static final String ECC_GET_QUERY = "To fetch result from custom query on editable component";
    public static final String ECC_LABEL = "Label";
    public static final String MASTER_FETCH_API = "/api/masters";
    public static final String ECC_FETCH_MASTER_API = "To fetch State City pincode";
    public static final String ECC_SLUG = "lendin-state-city-pincode";
    public static final String MASTER_DATA_API = "To fetch master data based on slug";
}
