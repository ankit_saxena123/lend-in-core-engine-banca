package com.kuliza.lending.engine_common.utils;

public class EngineCommonSwaggerConstants {


    public static final String PC_FIRE_CUSTOM_ACTION = "Fire custom action with id and proper request body";
    public static final String PC_FIRE_BULK_ACTION = "Fire bulk action api with id and list of proper request bodies";

    public static final String PC_FIRE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"method\": \"http\",\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\",\n" +
            "            \"openApi\": false,\n" +
            "            \"url\": \"http://localhost:8080/lending/portal/configurator/roleName/productType\",\n" +
            "            \"portalType\": \"localhost\",\n" +
            "            \"headers\": [\n" +
            "                {\n" +
            "                    \"key\": \"Content-Type\",\n" +
            "                    \"value\": \"application/json\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"key\": \"Authorization\",\n" +
            "                    \"value\": \"Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBNVFJLUVsYmFGV1d1YXZleGpFVEtFRUROQ2JvM2hkUVREYm9iSFlndldNIn0.eyJqdGkiOiJlNzQ1ZmRlZC1mMjAyLTRlOTMtYmU0Ny1jNTAwNzBjZjZhMzciLCJleHAiOjE1NTY4MjI2OTcsIm5iZiI6MCwiaWF0IjoxNTU1NTA1MDk3LCJpc3MiOiJodHRwOi8vMzUuMjAwLjE4OC4xMjE6ODAwNS9hdXRoL3JlYWxtcy9rdWxpemFfY2UiLCJhdWQiOiJsZW5kaW5nIiwic3ViIjoiYmM0ZjYyMmUtMGExNi00YzY4LWJmNjQtZDE1MjgxZjViMWFlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGVuZGluZyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjIzYTFiZGEyLWVmOTAtNDEyMS1hMmNlLWY2N2VkNDA1ZDI3ZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY21tbkNyZWRpdEFuYWx5c3RQT0NTVUIxIiwiY21tbkNyZWRpdEFuYWx5c3RQT0NBZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsibGVuZGluZyI6eyJyb2xlcyI6WyJjbW1uQ3JlZGl0QW5hbHlzdCJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwibmFtZSI6ImNtbW4gdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNtbW51c2VyIiwiZ2l2ZW5fbmFtZSI6ImNtbW4iLCJmYW1pbHlfbmFtZSI6InVzZXIiLCJlbWFpbCI6ImNtbW51c2VyQGt1bGl6YS5jb20ifQ.hoy3YzQOb_EUedqGIkqZaYLrnktFUL__YofNdLzwJ0SPfiq_0XRojLe6qjIpmiCHBJwoYirTc8wOtLtmkkyloPqD9dyOR6QIZG5GtNTidRl9GbMwd4BBm67g47frtviIbRdDSsoORGCGxNKSVUganvYe4iECapLjOhDT6hUSJmjHbTHC2wF6wNg2RRwD60vSE5hYyn9onkjjYxGy79VM5Zw5_qcQTmyrev-v1MOQO6SCLODdB_KGsrKOzC9nXIzwmxL-GDsP0j2FZmbzRs1wNs6yJmu7BlO1OJEkQKekvSt7djzSHN4XCPDexVbwuvuorvqtFHeUuqEiCMiUUzJjNQ\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"externalApi\": false,\n" +
            "            \"requestType\": \"GET\",\n" +
            "            \"httpProtocol\": \"http\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_FIRE_BULK_ACTION_RESPONSE = "{\n"+
            "    \"status\": 200,\n"+
            "    \"message\": \"SUCCESS!\",\n"+
            "    \"data\": {\n"+
            "        \"4b03cf82-557b-11e9-ac94-42010aa0001b\": {\n"+
            "            \"status\": 200,\n"+
            "            \"message\": \"SUCCESS!\",\n"+
            "            \"data\": {\n"+
            "                \"id\": 4,\n"+
            "                \"roleName\": \"cmmnCreditAnalyst\",\n"+
            "                \"productType\": \"personalLoan\",\n"+
            "                \"bucketList\": [\n"+
            "                    {\n"+
            "                        \"id\": \"pending\",\n"+
            "                        \"label\": \"PENDING\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    },\n"+
            "                    {\n"+
            "                        \"id\": \"approved\",\n"+
            "                        \"label\": \"APPROVED\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    },\n"+
            "                    {\n"+
            "                        \"id\": \"history\",\n"+
            "                        \"label\": \"HISTORY\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    },\n"+
            "                    {\n"+
            "                        \"id\": \"claim\",\n"+
            "                        \"label\": \"CLAIM\",\n"+
            "                        \"editable\": false,\n"+
            "                        \"comment\": 0,\n"+
            "                        \"variables\": [\n"+
            "                            {\n"+
            "                                \"id\": \"v1\",\n"+
            "                                \"label\": \"V1\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v2\",\n"+
            "                                \"label\": \"V2\",\n"+
            "                                \"type\": \"String\",\n"+
            "                                \"actionConfigurationList\": []\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"v3\",\n"+
            "                                \"label\": \"V3\",\n"+
            "                                \"type\": \"listOfAction\",\n"+
            "                                \"actionConfigurationList\": [\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"DELETE\",\n"+
            "                                        \"value\": false\n"+
            "                                    },\n"+
            "                                    {\n"+
            "                                        \"id\": \"ca1\",\n"+
            "                                        \"label\": \"CA1\",\n"+
            "                                        \"meta\": {},\n"+
            "                                        \"iconKey\": \"EDIT\",\n"+
            "                                        \"value\": false\n"+
            "                                    }\n"+
            "                                ]\n"+
            "                            }\n"+
            "                        ],\n"+
            "                        \"bucketActions\": [\n"+
            "                            {\n"+
            "                                \"id\": \"ba1\",\n"+
            "                                \"label\": \"BA1\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"DELETE\",\n"+
            "                                \"value\": false\n"+
            "                            },\n"+
            "                            {\n"+
            "                                \"id\": \"ba2\",\n"+
            "                                \"label\": \"BA2\",\n"+
            "                                \"meta\": {},\n"+
            "                                \"iconKey\": \"ADD\",\n"+
            "                                \"value\": false\n"+
            "                            }\n"+
            "                        ]\n"+
            "                    }\n"+
            "                ],\n"+
            "                \"userId\": \"cmmnuser@kuliza.com\"\n"+
            "            }\n"+
            "        }\n"+
            "    }\n"+
            "}";

    public static final String PC_GET_CUSTOM_RESPOSNE = "{\n"+
            "    \"status\": 200,\n"+
            "    \"message\": \"SUCCESS!\",\n"+
            "    \"data\": {\n"+
            "        \"id\": -3,\n"+
            "        \"method\": \"configurePortal\",\n"+
            "        \"userId\": null,\n"+
            "        \"openApi\": false,\n"+
            "        \"url\": \"/lending/portal/configurator/add-product-config\",\n"+
            "        \"portalType\": \"backOffice\",\n"+
            "        \"headers\": [],\n"+
            "        \"externalApi\": false,\n"+
            "        \"requestType\": \"POST\",\n"+
            "        \"httpProtocol\": \"http\"\n"+
            "    }\n"+
            "}";

    public static final String PC_CREATE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"method\": \"http\",\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\",\n" +
            "            \"openApi\": false,\n" +
            "            \"url\": \"http://localhost:8080/lending/portal/configurator/roleName/productType\",\n" +
            "            \"portalType\": \"localhost\",\n" +
            "            \"headers\": [\n" +
            "                {\n" +
            "                    \"key\": \"Content-Type\",\n" +
            "                    \"value\": \"application/json\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"key\": \"Authorization\",\n" +
            "                    \"value\": \"Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBNVFJLUVsYmFGV1d1YXZleGpFVEtFRUROQ2JvM2hkUVREYm9iSFlndldNIn0.eyJqdGkiOiJlNzQ1ZmRlZC1mMjAyLTRlOTMtYmU0Ny1jNTAwNzBjZjZhMzciLCJleHAiOjE1NTY4MjI2OTcsIm5iZiI6MCwiaWF0IjoxNTU1NTA1MDk3LCJpc3MiOiJodHRwOi8vMzUuMjAwLjE4OC4xMjE6ODAwNS9hdXRoL3JlYWxtcy9rdWxpemFfY2UiLCJhdWQiOiJsZW5kaW5nIiwic3ViIjoiYmM0ZjYyMmUtMGExNi00YzY4LWJmNjQtZDE1MjgxZjViMWFlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGVuZGluZyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjIzYTFiZGEyLWVmOTAtNDEyMS1hMmNlLWY2N2VkNDA1ZDI3ZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY21tbkNyZWRpdEFuYWx5c3RQT0NTVUIxIiwiY21tbkNyZWRpdEFuYWx5c3RQT0NBZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsibGVuZGluZyI6eyJyb2xlcyI6WyJjbW1uQ3JlZGl0QW5hbHlzdCJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwibmFtZSI6ImNtbW4gdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNtbW51c2VyIiwiZ2l2ZW5fbmFtZSI6ImNtbW4iLCJmYW1pbHlfbmFtZSI6InVzZXIiLCJlbWFpbCI6ImNtbW51c2VyQGt1bGl6YS5jb20ifQ.hoy3YzQOb_EUedqGIkqZaYLrnktFUL__YofNdLzwJ0SPfiq_0XRojLe6qjIpmiCHBJwoYirTc8wOtLtmkkyloPqD9dyOR6QIZG5GtNTidRl9GbMwd4BBm67g47frtviIbRdDSsoORGCGxNKSVUganvYe4iECapLjOhDT6hUSJmjHbTHC2wF6wNg2RRwD60vSE5hYyn9onkjjYxGy79VM5Zw5_qcQTmyrev-v1MOQO6SCLODdB_KGsrKOzC9nXIzwmxL-GDsP0j2FZmbzRs1wNs6yJmu7BlO1OJEkQKekvSt7djzSHN4XCPDexVbwuvuorvqtFHeUuqEiCMiUUzJjNQ\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"externalApi\": false,\n" +
            "            \"requestType\": \"GET\",\n" +
            "            \"httpProtocol\": \"http\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String PC_UPDATE_CUSTOM_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": {\n" +
            "        \"id\": -7,\n" +
            "        \"method\": \"http\",\n" +
            "        \"userId\": null,\n" +
            "        \"openApi\": false,\n" +
            "        \"url\": \"http://localhost:8080/configurator/roleName/productType\",\n" +
            "        \"portalType\": \"localhost\",\n" +
            "        \"headers\": [],\n" +
            "        \"externalApi\": false,\n" +
            "        \"requestType\": \"GET\",\n" +
            "        \"httpProtocol\": \"http\"\n" +
            "    }\n" +
            "}";


    public static final String GET_USER_TOKEN_RESPONSE = "{\n" +
            "    \"status\": 200,\n" +
            "    \"message\": \"SUCCESS!\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": 1,\n" +
            "            \"method\": \"http\",\n" +
            "            \"userId\": \"cmmnuser@kuliza.com\",\n" +
            "            \"openApi\": false,\n" +
            "            \"url\": \"http://localhost:8080/lending/portal/configurator/roleName/productType\",\n" +
            "            \"portalType\": \"localhost\",\n" +
            "            \"headers\": [\n" +
            "                {\n" +
            "                    \"key\": \"Content-Type\",\n" +
            "                    \"value\": \"application/json\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"key\": \"Authorization\",\n" +
            "                    \"value\": \"Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJBNVFJLUVsYmFGV1d1YXZleGpFVEtFRUROQ2JvM2hkUVREYm9iSFlndldNIn0.eyJqdGkiOiJlNzQ1ZmRlZC1mMjAyLTRlOTMtYmU0Ny1jNTAwNzBjZjZhMzciLCJleHAiOjE1NTY4MjI2OTcsIm5iZiI6MCwiaWF0IjoxNTU1NTA1MDk3LCJpc3MiOiJodHRwOi8vMzUuMjAwLjE4OC4xMjE6ODAwNS9hdXRoL3JlYWxtcy9rdWxpemFfY2UiLCJhdWQiOiJsZW5kaW5nIiwic3ViIjoiYmM0ZjYyMmUtMGExNi00YzY4LWJmNjQtZDE1MjgxZjViMWFlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGVuZGluZyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjIzYTFiZGEyLWVmOTAtNDEyMS1hMmNlLWY2N2VkNDA1ZDI3ZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY21tbkNyZWRpdEFuYWx5c3RQT0NTVUIxIiwiY21tbkNyZWRpdEFuYWx5c3RQT0NBZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsibGVuZGluZyI6eyJyb2xlcyI6WyJjbW1uQ3JlZGl0QW5hbHlzdCJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwibmFtZSI6ImNtbW4gdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNtbW51c2VyIiwiZ2l2ZW5fbmFtZSI6ImNtbW4iLCJmYW1pbHlfbmFtZSI6InVzZXIiLCJlbWFpbCI6ImNtbW51c2VyQGt1bGl6YS5jb20ifQ.hoy3YzQOb_EUedqGIkqZaYLrnktFUL__YofNdLzwJ0SPfiq_0XRojLe6qjIpmiCHBJwoYirTc8wOtLtmkkyloPqD9dyOR6QIZG5GtNTidRl9GbMwd4BBm67g47frtviIbRdDSsoORGCGxNKSVUganvYe4iECapLjOhDT6hUSJmjHbTHC2wF6wNg2RRwD60vSE5hYyn9onkjjYxGy79VM5Zw5_qcQTmyrev-v1MOQO6SCLODdB_KGsrKOzC9nXIzwmxL-GDsP0j2FZmbzRs1wNs6yJmu7BlO1OJEkQKekvSt7djzSHN4XCPDexVbwuvuorvqtFHeUuqEiCMiUUzJjNQ\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"externalApi\": false,\n" +
            "            \"requestType\": \"GET\",\n" +
            "            \"httpProtocol\": \"http\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

}
