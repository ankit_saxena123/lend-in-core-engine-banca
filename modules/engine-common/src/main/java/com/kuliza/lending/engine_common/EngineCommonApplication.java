package com.kuliza.lending.engine_common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kuliza" })
public class EngineCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(EngineCommonApplication.class, args);
	}
}
