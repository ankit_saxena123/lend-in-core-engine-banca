package com.kuliza.lending.engine_common.pojo;

import java.io.Serializable;
import java.util.Map;

public class CustomActionRequest implements Serializable {

    private Map<String, String> pathVariables;

    private Map<String, String> requestParams;

    private Map<String, Object> actionMeta;

    private Object requestBody;

    private String message;

    private String key;

    public CustomActionRequest() {
        super();
    }

    public CustomActionRequest(Map<String, String> pathVariables, Map<String, String> requestParams, Map<String, Object> actionMeta, Object requestBody,
                               String key, String message) {
        this.pathVariables = pathVariables;
        this.requestParams = requestParams;
        this.actionMeta = actionMeta;
        this.requestBody = requestBody;
        this.key = key;
        this.message = message;
    }

    public Map<String, String> getPathVariables() {
        return pathVariables;
    }

    public void setPathVariables(Map<String, String> pathVariables) {
        this.pathVariables = pathVariables;
    }

    public Map<String, String> getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(Map<String, String> requestParams) {
        this.requestParams = requestParams;
    }

    public Map<String, Object> getActionMeta() {
        return actionMeta;
    }

    public void setActionMeta(Map<String, Object> actionMeta) {
        this.actionMeta = actionMeta;
    }

    public Object getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(Object requestBody) {
        this.requestBody = requestBody;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "CustomActionURLBody{" +
                "pathVariables=" + pathVariables +
                ", requestParams=" + requestParams +
                ", requestBody=" + requestBody +
                ", message='" + message + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}

