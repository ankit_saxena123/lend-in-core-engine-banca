package com.kuliza.lending.engine_common.configs;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class EditableTableQuery {

    @NotNull(message = "Table Key cannot be null")
    @NotEmpty(message = "Table Key cannot be empty")
    public String tableKey;

    @NotNull(message = "Table Columns can not br null")
    @NotEmpty(message = "Table Columns can not be empty")
    public Map<String, String> tableColumns;

    @NotNull(message = "SQL query can not be null")
    @NotEmpty(message = "SQL query can not be empty")
    public String query;

    public boolean saveVariable;

    public String variableToBeSaved;

    public EditableTableQuery() {
    }

    public EditableTableQuery(String tableKey, Map<String, String> tableColumns, String query, boolean saveVariable, String variableToBeSaved) {
        this.tableKey = tableKey;
        this.tableColumns = tableColumns;
        this.query = query;
        this.saveVariable = saveVariable;
        this.variableToBeSaved = variableToBeSaved;
    }

    public boolean isSaveVariable() {
        return saveVariable;
    }

    public void setSaveVariable(boolean saveVariable) {
        this.saveVariable = saveVariable;
    }

    public String getTableKey() {
        return tableKey;
    }

    public void setTableKey(String tableKey) {
        this.tableKey = tableKey;
    }

    public Map<String, String> getTableColumns() {
        return tableColumns;
    }

    public void setTableColumns(Map<String, String> tableColumns) {
        this.tableColumns = tableColumns;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getVariableToBeSaved() {
        return variableToBeSaved;
    }

    public void setVariableToBeSaved(String variableToBeSaved) {
        this.variableToBeSaved = variableToBeSaved;
    }
}
