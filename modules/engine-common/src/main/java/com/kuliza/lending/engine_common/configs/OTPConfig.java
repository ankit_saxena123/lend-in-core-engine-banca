package com.kuliza.lending.engine_common.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
public class OTPConfig {

	@Value("${otp.OTPThresholdInMinutes}")
	private Integer otpThresholdInMinutes;

	@Value("${otp.otpLength}")
	private Integer otpLength;

	@Value("${otp.userBlockTimeInMinutes}")
	private Integer userBlockTimeInMinutes;

	@Value("${otp.otpExpiryTimeInMinutes}")
	private Integer otpExpiryTimeInMinutes;

	@Value("${otp.maxOtpGenerateAttempts}")
	private Integer maxOtpGenerateAttempts;

	@Value("${otp.maxOtpValidateAttempts}")
	private Integer maxOtpValidateAttempts;

	@Value("${otp.useCacheForOTP}")
	private Boolean useCacheForOTP;

	public Integer getOtpLength() {
		return otpLength;
	}

	public void setOtpLength(Integer otpLength) {
		this.otpLength = otpLength;
	}

	public Boolean getUseCacheForOTP() {
		return useCacheForOTP;
	}

	public void setUseCacheForOTP(Boolean useCacheForOTP) {
		this.useCacheForOTP = useCacheForOTP;
	}

	public Integer getOtpThresholdInMinutes() {
		return otpThresholdInMinutes;
	}

	public void setOtpThresholdInMinutes(Integer otpThresholdInMinutes) {
		this.otpThresholdInMinutes = otpThresholdInMinutes;
	}

	public Integer getUserBlockTimeInMinutes() {
		return userBlockTimeInMinutes;
	}

	public void setUserBlockTimeInMinutes(Integer userBlockTimeInMinutes) {
		this.userBlockTimeInMinutes = userBlockTimeInMinutes;
	}

	public Integer getOtpExpiryTimeInMinutes() {
		return otpExpiryTimeInMinutes;
	}

	public void setOtpExpiryTimeInMinutes(Integer otpExpiryTimeInMinutes) {
		this.otpExpiryTimeInMinutes = otpExpiryTimeInMinutes;
	}

	public Integer getMaxOtpGenerateAttempts() {
		return maxOtpGenerateAttempts;
	}

	public void setMaxOtpGenerateAttempts(Integer maxOtpGenerateAttempts) {
		this.maxOtpGenerateAttempts = maxOtpGenerateAttempts;
	}

	public Integer getMaxOtpValidateAttempts() {
		return maxOtpValidateAttempts;
	}

	public void setMaxOtpValidateAttempts(Integer maxOtpValidateAttempts) {
		this.maxOtpValidateAttempts = maxOtpValidateAttempts;
	}

}
