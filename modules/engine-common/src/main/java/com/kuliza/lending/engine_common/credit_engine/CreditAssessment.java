package com.kuliza.lending.engine_common.credit_engine;

import static com.kuliza.lending.engine_common.utils.Constants.LOG_REQUEST;
import static com.kuliza.lending.engine_common.utils.Constants.LOG_RESPONSE;

import com.kuliza.lending.common.connection.HTTPConnection;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;


@Service("CreditAssessment")
public class CreditAssessment {

	@Autowired
	private RuntimeService runtimeService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditAssessment.class);

	public void creditAssessmentIntegration(DelegateExecution execution, int prodId,
			String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod, String protocol,
			String url, Integer port, String suburl, String token) throws Exception {
		List<Integer> processIds=Arrays.asList(prodId);
		if (execution == null || processIds == null) {
			throw new IllegalArgumentException(
					"DelegateExecution argument or processIds argument cannot be null if request payload or response object needs to be saved in process instance.");
		} else {
			String userId = "";
			String appId = "";
			String processInstanceId = execution != null ? execution.getProcessInstanceId() : "";
			String losId = execution != null ? execution.getProcessInstanceBusinessKey() : "";

			if (execution != null) {
				if (userIdKeyInProcess != null && !userIdKeyInProcess.equals("")
						&& execution.getVariable(userIdKeyInProcess) != null) {
					userId = execution.getVariable(userIdKeyInProcess).toString();
				} else {
					if (execution.getVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY) != null) {
						userId = execution.getVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY).toString();
					}
				}
				if (applicationIdKeyInProcess != null && !applicationIdKeyInProcess.equals("")
						&& execution.getVariable(applicationIdKeyInProcess) != null) {
					appId = execution.getVariable(applicationIdKeyInProcess).toString();
				} else {
					if (execution.getVariable(Constants.APPLICATION_ID_KEY) != null) {
						appId = execution.getVariable(Constants.APPLICATION_ID_KEY).toString();
					} else {
						appId = execution.getProcessInstanceBusinessKey();
					}
				}
			}

			Map<String, Object> processVariables = execution.getVariables();
			JSONObject requestPayload = buildCreditEngineAssessmentRequestPayload(processVariables, processIds);
			LOGGER.info("CE: " + LOG_REQUEST, userId, processInstanceId, appId, losId, requestPayload);
			HTTPResponse response = fireCreditEngine(requestPayload, httpMethod, protocol, url, port, suburl, token);
			LOGGER.info("CE: " + LOG_RESPONSE, userId, processInstanceId, appId, losId, response);
			JSONObject jsonResponse = new JSONObject(response.getResponse());
			Map<String, Object> processVariablesToSave = parseCreditAssessmentResponse(jsonResponse);
			execution.setVariables(processVariablesToSave);

		}

	}

	public void creditAssessmentIntegration(String processInstanceId, int prodId,
											String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod, String protocol,
											String url, Integer port, String suburl, String token) throws Exception {
		List<Integer> processIds=Arrays.asList(prodId);
		if (processInstanceId == null || processInstanceId.isEmpty() || processIds == null) {
			throw new IllegalArgumentException(
					"processInstanceId or processIds argument cannot be null if request payload or response object needs to be saved in process instance.");
		} else {
			String userId = "";
			String appId = "";
			Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
			String losId = CommonHelperFunctions.getStringValue(processVariables.getOrDefault("applicationId", ""));

			if (processInstanceId != null) {
				if (userIdKeyInProcess != null && !userIdKeyInProcess.equals("")
						&& processVariables.get(userIdKeyInProcess) != null) {
					userId = processVariables.get(userIdKeyInProcess).toString();
				} else {
					if (processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY) != null) {
						userId = processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY).toString();
					}
				}
				if (applicationIdKeyInProcess != null && !applicationIdKeyInProcess.equals("")
						&& processVariables.get(applicationIdKeyInProcess) != null) {
					appId = processVariables.get(applicationIdKeyInProcess).toString();
				} else {
					if (processVariables.get(Constants.APPLICATION_ID_KEY) != null) {
						appId = processVariables.get(Constants.APPLICATION_ID_KEY).toString();
					} else {
						appId = losId;
					}
				}
			}


			JSONObject requestPayload = buildCreditEngineAssessmentRequestPayload(processVariables, processIds);
			//CustomLogger.log(userId, processInstanceId, LogType.OUTBOUND_REQUEST_BODY, JobType.OUTBOUND_API, appId,
			//    losId, requestPayload);
			HTTPResponse response = fireCreditEngine(requestPayload, httpMethod, protocol, url, port, suburl, token);
			//  CustomLogger.log(userId, processInstanceId, LogType.OUTBOUND_RESPONSE_BODY, JobType.OUTBOUND_API, appId,
			//      losId, response);
			JSONObject jsonResponse = new JSONObject(response.getResponse().toString());
			Map<String, Object> processVariablesToSave = parseCreditAssessmentResponse(jsonResponse);
			runtimeService.setVariables(processInstanceId, processVariablesToSave);

		}

	}

	public JSONObject buildCreditEngineAssessmentRequestPayload(Map<String, Object> processVariables,
			List<Integer> processIds) {
		Map<String, Object> requestParams = new HashMap<String, Object>();
		requestParams.put("productIds", processIds);
		Map<String, Object> inputData = new HashMap<String, Object>();
		for (Map.Entry<String, Object> entry : processVariables.entrySet()) {
			Map<String, Object> variableMap = CommonHelperFunctions.getVariableType(entry);
			inputData.put(variableMap.get("key").toString(), variableMap.get("value"));
		}
		requestParams.put("inputData", inputData);
		JSONObject requestPayload = new JSONObject(requestParams);
		return requestPayload;
	}

	public HTTPResponse fireCreditEngine(JSONObject requestData, HttpMethod httpMethod, String protocol, String url,
			Integer port, String suburl, String token) throws Exception {
		String requestBody = requestData.toString();
		HTTPResponse response = null;
		try {
			response = HTTPConnection.send(protocol, url, port, suburl, requestBody, "POST", token);
		} catch (Exception e) {
			response = new HTTPResponse(Constants.FAILURE_CODE, e.toString());
		}
		return response;

	}

	public Map<String, Object> parseCreditAssessmentResponse(JSONObject jsonResponse)
			throws IOException, InterruptedException, Exception {
		LOGGER.info("parseCreditAssessmentResponse jsonResponse : " + jsonResponse);
		LOGGER.info("parseCreditAssessmentResponse jsonResponse.getInt(\"status\") : " + jsonResponse.getInt("status"));
		Map<String, Object> processVariables = new HashMap<>();
		if (jsonResponse.getInt("status") == Constants.SUCCESS_CODE) {
			JSONArray dataArray = jsonResponse.getJSONArray("data");
			LOGGER.info("parseCreditAssessmentResponse dataArray.getJSONObject(0).getInt(\"status\") : " + dataArray.getJSONObject(0).getInt("status"));
			if (dataArray.getJSONObject(0).getInt("status") == 200) {
				JSONObject data = dataArray.getJSONObject(0).getJSONObject("data");
				JSONObject output = data.getJSONObject("output");
				Iterator<String> outputKeys = output.keys();
				while (outputKeys.hasNext()) {
					String key = outputKeys.next();
					LOGGER.debug("Key in OutPut "+key);
					if (output.get(key) instanceof JSONObject) {
						JSONObject results = output.getJSONObject(key);
						Iterator<String> resultKeys = results.keys();
						while (resultKeys.hasNext()) {
							String resultKey = resultKeys.next();
							LOGGER.debug("Key In JSON "+resultKey);
							LOGGER.debug("Key "+resultKey+"_CE"+" value "+results.get(resultKey));
							processVariables.put(resultKey+"_CE", results.get(resultKey));
						}
					} else {
						processVariables.put(key+"_CE", output.get(key));
						LOGGER.debug(" Key: "+key+"_CE"+" value:- "+output.get(key));
					}
					if (output.get(key) instanceof String || output.get(key) instanceof Boolean
							|| output.get(key) instanceof Double) {
						processVariables.put(Constants.CREDIT_ASSESSMENT_DECISION_KEY,
								CommonHelperFunctions.getStringValue(output.get(key).toString().toLowerCase()));
					}
				}
				processVariables.put(Constants.CREDIT_ASSESSMENT_INPUT_KEY, data.getJSONObject("input").toString());
				processVariables.put(Constants.CREDIT_ASSESSMENT_OUTPUT_KEY, data.getJSONObject("output").toString());
				processVariables.put(Constants.CREDIT_ASSESSMENT_STATUS_KEY, "true");
			} else {
				throw new InterruptedException("Error while evaluating Credit Assessment");
			}
		} else {
			throw new InterruptedException("Error response from Credit Assessment");
		}
		return processVariables;
	}

}
