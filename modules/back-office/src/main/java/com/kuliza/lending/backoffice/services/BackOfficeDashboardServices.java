package com.kuliza.lending.backoffice.services;

import static com.kuliza.lending.backoffice.utils.BackOfficeConstants.JOURNEY_TYPE;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kuliza.lending.backoffice.config.BackOfficeJourneyConfig;
import com.kuliza.lending.backoffice.dao.BucketsDao;
import com.kuliza.lending.backoffice.dao.BucketsVariablesDao;
import com.kuliza.lending.backoffice.dao.CardsDao;
import com.kuliza.lending.backoffice.dao.CardsVariablesDao;
import com.kuliza.lending.backoffice.dao.HeadersDao;
import com.kuliza.lending.backoffice.dao.HeadersVariablesDao;
import com.kuliza.lending.backoffice.dao.OptionsDao;
import com.kuliza.lending.backoffice.dao.OutcomesDao;
import com.kuliza.lending.backoffice.dao.RolesDao;
import com.kuliza.lending.backoffice.dao.TabsDao;
import com.kuliza.lending.backoffice.dao.TabsVariablesDao;
import com.kuliza.lending.backoffice.dao.VariablesDao;
import com.kuliza.lending.backoffice.exceptions.BucketNotMappedException;
import com.kuliza.lending.backoffice.exceptions.CommentIsNullException;
import com.kuliza.lending.backoffice.exceptions.CommentsNotEnabledForRoleException;
import com.kuliza.lending.backoffice.exceptions.DashboardNotConfiguredForRoleException;
import com.kuliza.lending.backoffice.exceptions.InvalidApplicationIdException;
import com.kuliza.lending.backoffice.exceptions.InvalidOutcomeException;
import com.kuliza.lending.backoffice.exceptions.TabNotConfiguredForRoleException;
import com.kuliza.lending.backoffice.exceptions.UserNotInvolvedInApplicationException;
import com.kuliza.lending.backoffice.exceptions.VariableNotFoundException;
import com.kuliza.lending.backoffice.models.Buckets;
import com.kuliza.lending.backoffice.models.BucketsVariablesMapping;
import com.kuliza.lending.backoffice.models.Cards;
import com.kuliza.lending.backoffice.models.CardsVariablesMapping;
import com.kuliza.lending.backoffice.models.Headers;
import com.kuliza.lending.backoffice.models.HeadersVariablesMapping;
import com.kuliza.lending.backoffice.models.Options;
import com.kuliza.lending.backoffice.models.Outcomes;
import com.kuliza.lending.backoffice.models.Roles;
import com.kuliza.lending.backoffice.models.Tabs;
import com.kuliza.lending.backoffice.models.TabsVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;
import com.kuliza.lending.backoffice.pojo.dashboard.CommentData;
import com.kuliza.lending.backoffice.pojo.dashboard.CommentFilter;
import com.kuliza.lending.backoffice.pojo.dashboard.GetCommentsResponse;
import com.kuliza.lending.backoffice.pojo.dashboard.SaveApplicationData;
import com.kuliza.lending.backoffice.pojo.dashboard.SubmitApplication;
import com.kuliza.lending.backoffice.pojo.dashboard.SubmitCommentRequest;
import com.kuliza.lending.backoffice.pojo.dashboard.TabApplicationDataResponse;
import com.kuliza.lending.backoffice.pojo.dashboard.config.AppConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.config.AppVariablesConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.config.ApplicationDataConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.config.BucketVariablesConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.config.OptionsConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.BucketConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.CardConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.DashboardConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.HeaderConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.OutcomeConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.RoleConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.TabConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.VariableConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.VariableOptionListConfig;
import com.kuliza.lending.backoffice.utils.BackOfficeConstants;
import com.kuliza.lending.backoffice.utils.BackOfficeUtils;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.flowable.task.service.history.NativeHistoricTaskInstanceQuery;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class BackOfficeDashboardServices {

	private static final Logger logger = LoggerFactory.getLogger(BackOfficeDashboardServices.class);

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private BackOfficeJourneyConfig backOfficeJourneyConfig;

	@Autowired
	private RolesDao rolesDao;

	@Autowired
	private TabsDao tabsDao;

	@Autowired
	private VariablesDao variablesDao;

	@Autowired
	private OutcomesDao outcomeDao;

	@Autowired
	private BucketsVariablesDao bucketsVariablesDao;

	@Autowired
	private BucketsDao bucketsDao;

	@Autowired
	private TabsVariablesDao tabsVariablesDao;

	@Autowired
	private CardsVariablesDao cardsVariablesDao;

	@Autowired
	private CardsDao cardsDao;

	@Autowired
	private HeadersDao headersDao;

	@Autowired
	private HeadersVariablesDao headersVariablesDao;

	@Autowired
	private OptionsDao optionsDao;

	public Set<String> getSetOfCommaSeperatedVariables(String[] listOfVariables) {
		Set<String> variables = new LinkedHashSet<>();
		for (int i = 0; i < listOfVariables.length; i++) {
			if (listOfVariables[i] != null && !listOfVariables[i].isEmpty()) {
				variables.add(listOfVariables[i].trim());
			}
		}
		return variables;
	}
	

	/**
	 * This method is used to get list of Assigned cases for role ad bucket
	 *
	 * @param request   to get role name form the request
	 * @param username
	 * @param bucketKey for which bucket of role, Assigned cases are to be returned
	 * @param filters   A map of filters to get results based on these
	 * @return List of Assigned cases for a given role,bucket and filters(if any)
	 * @throws Exception
	 */
	// TODO : filter on losApplicationId
	public ApiResponse getListOfAssignedCases(HttpServletRequest request, String username, String bucketKey,
			Map<String, String> filters) throws Exception {
		logger.info("--> Entered get List of Assigned cases()");
		ApiResponse response = null;
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			Map<String, Object> processVariables = null;
			Set<BucketsVariablesMapping> bucketVariableMapping = null;
			List<String> variables = new ArrayList<>();
			List<Map<String, Object>> taskList = new ArrayList<>();
			Object[] filterKeys = filters.keySet().toArray();
			Map<String, Object> responseObject = new HashMap<>();
			Buckets bucket = bucketsDao.findByBucketKeyAndRoleAndIsDeleted(bucketKey, role, false);
			if (bucket != null) {
				logger.debug("Found bucket: "+ bucket.getLabel() +" for role");
				bucketVariableMapping = bucketsVariablesDao
						.findByBucketAndIsDeletedOrderByBucketVariableOrderAsc(bucket, false);
				variables = new ArrayList<>();
				for (BucketsVariablesMapping buckVarMaping : bucketVariableMapping) {
					AppVariablesConfig bucketsConfig = new AppVariablesConfig(buckVarMaping.getVariable());
					variables.add(bucketsConfig.getKey());
				}
				for (int i = 0; i < filterKeys.length; i++) {
					String filterKey = (String) filterKeys[i];
					if ((!filterKey.equals(BackOfficeConstants.FILTER_OFFSET_KEY))
							&& (!filterKey.equals(BackOfficeConstants.FILTER_PAGE_WIDTH_KEY))
							&& !(variables.contains(filterKey))) {
						logger.error(BackOfficeConstants.getVariableNotMappedForRoleMessage(roleName));
						throw new VariableNotFoundException(
								BackOfficeConstants.getVariableNotMappedForRoleMessage(roleName));
					}
				}
				HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery().unfinished()
						.taskAssignee(username).orderByTaskCreateTime().desc();
				query = query.processVariableValueEquals(BackOfficeConstants.STATUS_VARIABLE_KEY, bucketKey);

				List<HistoricTaskInstance> tasks = query.list();
				Set<String> addedApplicationsList = new HashSet<>();
				for (HistoricTaskInstance task : tasks) {
					if (task.getProcessInstanceId() != null) {
						HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
								.processInstanceId(task.getProcessInstanceId()).includeProcessVariables()
								.singleResult();
						String businessKey = processInstance.getBusinessKey();
						if (!addedApplicationsList.contains(businessKey)) {
							Map<String, Object> taskMap = new HashMap<>();
							processVariables = processInstance.getProcessVariables();
							boolean taskFlag = false;
							boolean nextFilterFlag = false;
							for (int i = 0; i < filterKeys.length && !taskFlag; i++) {
								nextFilterFlag = false;
								if (BackOfficeUtils.isNotEmpty(filters.get(filterKeys[i]))
										&& (!filterKeys[i].equals(BackOfficeConstants.FILTER_OFFSET_KEY))
										&& (!filterKeys[i].equals(BackOfficeConstants.FILTER_PAGE_WIDTH_KEY))) {
									nextFilterFlag = true;
									if (!String.valueOf(processVariables.get(filterKeys[i])).toLowerCase()
											.contains(filters.get(filterKeys[i]).toLowerCase())) {
										taskFlag = true;
										continue;
									}
									if (i == filterKeys.length - 1)
										nextFilterFlag = false;
								}

								if (!taskFlag && filterKeys.length == 2) {
									for (int j = 0; j < variables.size(); j++) {
										taskMap.put(variables.get(j), processVariables.get(variables.get(j)));
									}
								} else if (!taskFlag && !nextFilterFlag
										&& (!filterKeys[i].equals(BackOfficeConstants.FILTER_OFFSET_KEY))
										&& (!filterKeys[i].equals(BackOfficeConstants.FILTER_PAGE_WIDTH_KEY))) {
									for (int j = 0; j < variables.size(); j++) {
										taskMap.put(variables.get(j), processVariables.get(variables.get(j)));
									}
								}
							}
							if (!taskMap.isEmpty()) {
								taskMap.put("losApplicationId", businessKey);
								taskMap.put(JOURNEY_TYPE,
										JOURNEY_TYPE().getOrDefault(processInstance.getProcessDefinitionKey(),
												processInstance.getProcessDefinitionKey()));
								taskList.add(taskMap);
							}
							addedApplicationsList.add(businessKey);
						}
					}
				}
				if (BackOfficeUtils.isNotEmpty(filters.get(BackOfficeConstants.FILTER_OFFSET_KEY))) {
					if (taskList.size() > Integer.parseInt(filters.get(BackOfficeConstants.FILTER_OFFSET_KEY))) {
						taskList = taskList.subList(
								Integer.parseInt(filters.get(BackOfficeConstants.FILTER_OFFSET_KEY)), taskList.size());
					}
				}
				if (BackOfficeUtils.isNotEmpty(filters.get(BackOfficeConstants.FILTER_PAGE_WIDTH_KEY))) {
					if (taskList.size() > Integer.parseInt(filters.get(BackOfficeConstants.FILTER_PAGE_WIDTH_KEY))) {
						taskList = taskList.subList(0,
								Integer.parseInt(filters.get(BackOfficeConstants.FILTER_PAGE_WIDTH_KEY)));
					}
				}
				Map<String, Object> metaMap = new HashMap<>();
				metaMap.put("totalCount", taskList.size());
				metaMap.put("showPagination", true);
				responseObject.put("meta", metaMap);
				responseObject.put(bucketKey, taskList);
				response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), responseObject);
			} else {
				logger.error(BackOfficeConstants.getBucketNotConfiguredForRoleMessage(roleName));
				throw new BucketNotMappedException(BackOfficeConstants.getBucketNotConfiguredForRoleMessage(roleName));
			}
		} else {
			logger.error(BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<-- Exiting get List of Assigned cases() ");
		return response;
	}


  /**
   * This method is used to get list of Assigned cases for role and bucket
   *
   * @param request   to get role name form the request
   * @param username
   * @return List of Assigned cases for a given role,bucket and filters(if any)
   * @throws Exception
   */
  // TODO : filter on losApplicationId
  public ApiResponse getAssignedListOfMobileDashboard(HttpServletRequest request, String username) throws Exception {
    logger.info("--> Entered get List of Assigned cases()");
    ApiResponse response = new ApiResponse();
    Map<String, Object> processVariables = null;
    Map<String, Object> responseObject = new HashMap<>();
    Map<String, Object> metaMap = new HashMap<>();
    List<Map<String, Object>> taskList = new ArrayList<>();

    NativeHistoricTaskInstanceQuery query = historyService.createNativeHistoricTaskInstanceQuery().sql("select * from ACT_HI_TASKINST where ASSIGNEE_ = \"" + username
			+ "\"AND SCOPE_TYPE_ is NULL and END_TIME_ is NULL order by START_TIME_ DESC ;");

    List<HistoricTaskInstance> tasks = query.list();
    Set<String> addedApplicationsList = new HashSet<>();
    for (HistoricTaskInstance task : tasks) {
      HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
          .processInstanceId(task.getProcessInstanceId()).includeProcessVariables().singleResult();
      String businessKey = processInstance.getBusinessKey();
      processVariables = processInstance.getProcessVariables();
      Map<String, Object> coulumnMap = new HashMap<>();
      String listViewColumns = CommonHelperFunctions.getStringValue(processVariables.get("listViewColumns"));
      if (!addedApplicationsList.contains(businessKey) && !listViewColumns.isEmpty()) {
        String[] coulmnList = listViewColumns.split(",");
        for (String columnName: coulmnList) {
          coulumnMap.put(columnName, processVariables.get(columnName));
        }
        taskList.add(coulumnMap);
      }
    }
    metaMap.put("showPagination", true);
    metaMap.put("totalCount", taskList.size());
    responseObject.put("meta", metaMap);
    responseObject.put("taskList", taskList);
    response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseObject);
    logger.info("<-- Exiting get List of Assigned cases() ");
    return response;
    }


	// TODO : to be removed. only for demo
	public ApiResponse deleteAllBOConfig(String roleName) {
		logger.info("--> Entered Delete Back-office configuration()");
		if (roleName == null) {
			logger.warn("--> Deleting all configuration in database as no roleName specified");
			rolesDao.deleteAll();
		} else {
			logger.debug("--> Deleting all configuration for role :" + roleName);
			Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
			if (role != null) {
				rolesDao.delete(role);
			} else {
				logger.error(BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
				throw new DashboardNotConfiguredForRoleException(
						BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
			}
		}
		logger.debug("Successfully deleted configurations");
		logger.info("<--Exiting Delete Back-office configuration()");
		return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());

	}

	public void configureRoleInDatabase(RoleConfig newRole) throws JsonProcessingException {
		logger.info("--> Entering Configure role in Database()");
		Roles role = rolesDao.findByRoleNameAndIsDeleted(newRole.getRoleName(), false);
		if (role == null) {
			role = new Roles(newRole.getRoleName(), newRole.isEnableComments());
			rolesDao.save(role);
			logger.debug("created role in database");
		}
		List<OutcomeConfig> outcomes = newRole.getOutcomes();
		int i = 1;
		boolean flag = false;
		for (OutcomeConfig outcome : outcomes) {
			Outcomes otcm = outcomeDao.findByOutcomeKeyAndRoleAndIsDeleted(outcome.getKey(), role, false);
			if (otcm == null) {
				otcm = new Outcomes(outcome.getKey(), i, outcome.getLabel(), outcome.isCommentRequired(), role);
				outcomeDao.save(otcm);
				flag = true;
			}
			i++;
		}
		if (flag) {
			flag = false;
			logger.debug("saved outcomes for role");
		} else {
			logger.warn("no outcomes mapped with role");
		}

		List<VariableConfig> variables = newRole.getVariables();
		for (int indx = 0; indx < variables.size(); indx++) {

			Variables v = variablesDao.findByMappingAndRoleAndIsDeleted(variables.get(indx).getKey(), role, false);
			if (v == null) {
				v = new Variables(variables.get(indx).getLabel(), variables.get(indx).getKey(),
						variables.get(indx).isEditable(), variables.get(indx).isWritable(),
						variables.get(indx).getMeta(), variables.get(indx).getType(), role);
				variablesDao.save(v);
				flag = true;
			}
			if (v.getType().equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN)) {
				VariableOptionListConfig var = (VariableOptionListConfig) variables.get(indx);

				List<OptionsConfig> options = var.getOptions();
				int ordr = 1;
				for (OptionsConfig optn : options) {

					Options option = optionsDao.findByOptionKeyAndVariableAndIsDeleted(optn.getKey(), v, false);
					if (option == null) {
						option = new Options(optn.getLabel(), optn.getKey(), ordr++, v);
						optionsDao.save(option);

					}

				}

			}

		}
		if (flag) {
			flag = false;
			logger.debug("saved variables for role");
		} else {
			logger.warn("no variables mapped with role");
		}
		List<BucketConfig> buckets = newRole.getBuckets();
		i = 1;
		for (BucketConfig bucket : buckets) {
			Buckets bkt = bucketsDao.findByBucketKeyAndRoleAndIsDeleted(bucket.getKey(), role, false);
			if (bkt == null) {
				bkt = new Buckets(bucket.getLabel(), bucket.getKey(), i, role);
				bucketsDao.save(bkt);
			} else if (bkt.getBucketOrder() != i) {
				bkt.setBucketOrder(i);
				bucketsDao.save(bkt);
			}

			Set<String> variableKeys = bucket.getVariables();
			int j = 1;
			for (String variable : variableKeys) {
				Variables v = variablesDao.findByMappingAndRoleAndIsDeleted(variable, role, false);
				if (v == null) {
					logger.error(variable + " not found for role : " + role.getRoleName() + " in bucket : "
							+ bucket.getLabel());
					throw new VariableNotFoundException(variable + " not found for role : " + role.getRoleName()
							+ " in bucket : " + bucket.getLabel());
				} else {
					BucketsVariablesMapping bvmap = bucketsVariablesDao.findByBucketAndVariableAndIsDeleted(bkt, v,
							false);
					if (bvmap == null) {
						bvmap = new BucketsVariablesMapping(bkt, v, j);
						bucketsVariablesDao.save(bvmap);
					} else if (bvmap.getBucketVariableOrder() != j) {
						bvmap.setBucketVariableOrder(j);
						bucketsVariablesDao.save(bvmap);
					}
				}
				j++;
			}
			i++;
		}
		List<TabConfig> tabs = newRole.getTabs();
		i = 1;
		for (TabConfig tab : tabs) {
			Tabs tb = tabsDao.findByTabKeyAndRoleAndIsDeleted(tab.getKey(), role, false);
			if (tb == null) {
				tb = new Tabs(tab.getLabel(), i, tab.getKey(), role);
				tabsDao.save(tb);
			} else if (tb.getTabOrder() != i) {
				tb.setTabOrder(i);
				tabsDao.save(tb);
			}
			Set<String> tabVariableKeys = tab.getVariables();
			int j = 1;
			for (String variable : tabVariableKeys) {
				Variables v = variablesDao.findByMappingAndRoleAndIsDeleted(variable, role, false);
				if (v == null) {
					throw new VariableNotFoundException(
							variable + " not found for role : " + role.getRoleName() + " in tab : " + tab.getLabel());
				} else {
					TabsVariablesMapping tvmap = tabsVariablesDao.findByTabAndVariableAndIsDeleted(tb, v, false);
					if (tvmap == null) {
						tvmap = new TabsVariablesMapping(j, tb, v);
						tabsVariablesDao.save(tvmap);
					} else if (tvmap.getTabVariableOrder() != j) {
						tvmap.setTabVariableOrder(j);
						tabsVariablesDao.save(tvmap);
					}
				}
				j++;
			}
			List<CardConfig> cards = tab.getCards();
			j = 1;
			for (CardConfig card : cards) {
				Cards crd = cardsDao.findByCardsKeyAndTabAndIsDeleted(card.getKey(), tb, false);
				if (crd == null) {
					crd = new Cards(card.getLabel(), card.getKey(), j, tb);
					cardsDao.save(crd);
				} else if (crd.getCardsOrder() != j) {
					crd.setCardsOrder(j);
					cardsDao.save(crd);
				}
				Set<String> cardsVariableKeys = card.getVariables();
				int k = 1;
				for (String variable : cardsVariableKeys) {
					Variables v = variablesDao.findByMappingAndRoleAndIsDeleted(variable, role, false);
					if (v == null) {
						throw new VariableNotFoundException(variable + " not found for role : " + role.getRoleName()
								+ " in card : " + card.getLabel());
					} else {
						CardsVariablesMapping cvmap = cardsVariablesDao.findByCardAndVariableAndIsDeleted(crd, v,
								false);
						if (cvmap == null) {
							cvmap = new CardsVariablesMapping(k, crd, v);
							cardsVariablesDao.save(cvmap);
						} else if (cvmap.getCardVariableOrder() != k) {
							cvmap.setCardVariableOrder(k);
							cardsVariablesDao.save(cvmap);
						}
					}
					k++;
				}
				List<HeaderConfig> headers = card.getHeaders();
				k = 1;
				for (HeaderConfig header : headers) {
					Headers hdr = headersDao.findByHeaderKeyAndCardAndIsDeleted(header.getKey(), crd, false);
					if (hdr == null) {
						hdr = new Headers(header.getLabel(), header.getKey(), crd, k);
						headersDao.save(hdr);
					} else if (hdr.getHeaderOrder() != k) {
						hdr.setHeaderOrder(k);
						headersDao.save(hdr);
					}
					Set<String> headerVariableKeys = header.getVariables();
					int m = 1;
					for (String variable : headerVariableKeys) {
						Variables v = variablesDao.findByMappingAndRoleAndIsDeleted(variable, role, false);
						if (v == null) {
							throw new VariableNotFoundException(variable + " not found for role : " + role.getRoleName()
									+ " in header : " + header.getLabel());
						} else {
							HeadersVariablesMapping hvmap = headersVariablesDao.findByHeaderAndVariableAndIsDeleted(hdr,
									v, false);
							if (hvmap == null) {
								hvmap = new HeadersVariablesMapping(m, hdr, v);
								headersVariablesDao.save(hvmap);
							} else if (hvmap.getHeaderVariableOrder() != m) {
								hvmap.setHeaderVariableOrder(m);
								headersVariablesDao.save(hvmap);
							}
						}
						m++;
					}
					k++;
				}
				j++;
			}
			i++;
		}
		logger.info("<-- Exiting Configure role in Database()");
	}

	public RoleConfig parseWorkbook(Workbook workbook, List<String> errors)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("--> Entering parse workbook()");
		Sheet sheet = workbook.getSheetAt(0);
		if (sheet.getPhysicalNumberOfRows() < 2) {
			errors.add("No of rows in role sheet is less than 2");
			return null;
		}
		RoleConfig roleConfig = new RoleConfig();
		Row row1 = sheet.getRow(0);
		Row row2 = sheet.getRow(1);
		if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
				&& row1.getCell(0).getStringCellValue().equals("roleName")) {
			if (row2.getCell(0).getCellTypeEnum() == CellType.STRING) {
				roleConfig.setRoleName(row2.getCell(0).getStringCellValue().trim());
			} else {
				errors.add("Row 2 Cell 1 of Role Sheet is not string");
				return null;
			}
		} else {
			errors.add("Row 1 Cell 1 of Role Sheet is not string or role name");
			return null;
		}

		if (row1.getCell(1).getCellTypeEnum() == CellType.STRING
				&& row1.getCell(1).getStringCellValue().equals("enableComments")) {
			if (row2.getCell(1).getCellTypeEnum() == CellType.BOOLEAN) {
				roleConfig.setEnableComments(row2.getCell(1).getBooleanCellValue());
			} else {
				errors.add("Row 2 Cell 2 of Role Sheet is not boolean");
				return null;
			}
		} else {
			errors.add("Row 1 Cell 2 of Role Sheet is not string or role name");
			return null;
		}
		roleConfig.setBuckets(parseBucketsSheet(workbook.getSheetAt(6), errors));
		roleConfig.setOutcomes(parseOutcomesSheet(workbook.getSheetAt(1), errors));
		roleConfig.setVariables(parseVariableSheet(workbook.getSheetAt(2), errors));
		roleConfig.setTabs(parseTabsSheet(workbook, errors));
		logger.info(" <--Exiting parse workbook()");
		return roleConfig;
	}

	public List<OutcomeConfig> parseOutcomesSheet(Sheet sheet, List<String> errors) {
		logger.info("--> Entering Parse Outcomes Sheet()");
		List<OutcomeConfig> outcomes = new ArrayList<>();
		if (sheet.getPhysicalNumberOfRows() < 1) {
			errors.add("No of rows in outcomes sheet is less than 1");

		} else {
			Row row1 = sheet.getRow(0);

			if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(1).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(2).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(0).getStringCellValue().equals("label")
					&& row1.getCell(1).getStringCellValue().equals("key")
					&& row1.getCell(2).getStringCellValue().equals("commentRequired")) {
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
					Row row = sheet.getRow(i);
					if (row != null) {
						OutcomeConfig otcm = new OutcomeConfig();
						if (row.getCell(0).getCellTypeEnum() == CellType.STRING) {
							otcm.setLabel(row.getCell(0).getStringCellValue().trim());
						} else {
							errors.add("Row " + ((i + 1)) + " Cell 1 of outcomes Sheet is not string");
						}
						if (row.getCell(1).getCellTypeEnum() == CellType.STRING) {
							otcm.setKey(row.getCell(1).getStringCellValue().trim());
						} else {
							errors.add("Row " + ((i + 1)) + " Cell 2 of outcomes Sheet is not string");
						}
						if (row.getCell(2).getCellTypeEnum() == CellType.BOOLEAN) {
							otcm.setCommentRequired(row.getCell(2).getBooleanCellValue());
						} else {
							errors.add("Row " + ((i + 1)) + " Cell 3 of outcomes Sheet is not boolean");
						}
						outcomes.add(otcm);
					}
				}
			} else {
				logger.debug("Invalid columns in outcomes sheets");
			}
		}
		logger.info("<-- Exiting Parse Outcomes Sheet()");
		return outcomes;
	}

	public List<VariableConfig> parseVariableSheet(Sheet sheet, List<String> errors)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("--> Entering Parse variables Sheet()");
		List<VariableConfig> variables = new ArrayList<>();
		if (sheet.getPhysicalNumberOfRows() < 1) {
			errors.add("No of rows in variables sheet is less than 1");
		} else {
			Row row1 = sheet.getRow(0);
			if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(1).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(2).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(3).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(4).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(5).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(6).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(0).getStringCellValue().equals("label")
					&& row1.getCell(1).getStringCellValue().equals("key")
					&& row1.getCell(2).getStringCellValue().equals("editable")
					&& row1.getCell(3).getStringCellValue().equals("writable")
					&& row1.getCell(4).getStringCellValue().equals("meta")
					&& row1.getCell(5).getStringCellValue().equalsIgnoreCase("type")
					&& row1.getCell(6).getStringCellValue().equalsIgnoreCase("options")) {
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
					Row row = sheet.getRow(i);
					String label = "";
					String key = "";
					boolean isEditable = false;
					boolean isWritable = false;
					String meta = "";
					String type = "";

					if (row != null) {
						VariableConfig vrbls = new VariableConfig();
						VariableOptionListConfig vrblsOptns = new VariableOptionListConfig();
						if (row.getCell(5).getStringCellValue().trim()
								.equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN))
							vrblsOptns = new VariableOptionListConfig();
						if (row.getCell(0).getCellTypeEnum() == CellType.STRING) {
							label = row.getCell(0).getStringCellValue().trim();
						} else {
							errors.add("Row " + (i + 1) + " Cell 1 of variables Sheet is not string");
						}
						if (row.getCell(1).getCellTypeEnum() == CellType.STRING) {
							key = row.getCell(1).getStringCellValue().trim();
						} else {
							errors.add("Row " + (i + 1) + " Cell 2 of variables Sheet is not string");

						}
						if (row.getCell(2).getCellTypeEnum() == CellType.BOOLEAN) {
							isEditable = row.getCell(2).getBooleanCellValue();
						} else {
							errors.add("Row " + (i + 1) + " Cell 3 of variables Sheet is not boolean");
						}

						if (row.getCell(3).getCellTypeEnum() == CellType.BOOLEAN) {
							isWritable = row.getCell(3).getBooleanCellValue();
						} else {
							errors.add("Row " + (i + 1) + " Cell 4 of variables Sheet is not boolean");
						}

						if (row.getCell(4).getCellTypeEnum() == CellType.STRING) {
							meta = row.getCell(4).getStringCellValue().trim();
						} else {
							errors.add("Row " + (i + 1) + " Cell 5 of variables Sheet is not string");
						}

						if (row.getCell(5).getCellTypeEnum() == CellType.STRING) {
							type = row.getCell(5).getStringCellValue().trim();
							if (row.getCell(5).getStringCellValue().trim()
									.equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN)) {
								JsonNode optionsNode = mapper.readTree(row.getCell(6).getStringCellValue().trim());
								String optionString = mapper.writeValueAsString(optionsNode);
								List<OptionsConfig> optnList = mapper.readValue(optionString, mapper.getTypeFactory()
										.constructCollectionType(List.class, OptionsConfig.class));
								vrblsOptns = new VariableOptionListConfig(label, key, isEditable, isWritable, meta,
										type);
								vrblsOptns.setOptions(optnList);
								variables.add(vrblsOptns);

							} else {

								vrbls = new VariableConfig(label, key, isEditable, isWritable, meta, type);
								variables.add(vrbls);
							}
						} else {
							errors.add("Row " + (i + 1) + " Cell " + (i + 1) + " of variables Sheet is not string");
						}

					}
				}
			} else {
				errors.add("Invalid columns in variable sheets");
			}
		}
		logger.info("<-- Exiting Parse variables Sheet()");
		return variables;
	}

	public List<TabConfig> parseTabsSheet(Workbook workbook, List<String> errors) {
		logger.info("--> Entering Parse Tabs Sheet()");
		List<TabConfig> tabs = new ArrayList<>();
		Sheet sheet = workbook.getSheetAt(3);
		if (sheet.getPhysicalNumberOfRows() < 1) {
			errors.add("No of rows in Tabs sheet is less than 1");
		} else {
			Row row1 = sheet.getRow(0);
			if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(1).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(2).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(0).getStringCellValue().equals("label")
					&& row1.getCell(1).getStringCellValue().equals("key")
					&& row1.getCell(2).getStringCellValue().equals("variables")) {
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
					Row row = sheet.getRow(i);
					if (row != null) {
						TabConfig tbsCfg = new TabConfig();
						if (row.getCell(0).getCellTypeEnum() == CellType.STRING) {
							tbsCfg.setLabel(row.getCell(0).getStringCellValue().trim());
						} else {
							errors.add("Row " + (i + 1) + " Cell 1 of Tabs Sheet is not string");
						}
						if (row.getCell(1).getCellTypeEnum() == CellType.STRING) {
							tbsCfg.setKey(row.getCell(1).getStringCellValue().trim());
						} else {
							errors.add("Row " + (i + 1) + " Cell 2 of Tabs Sheet is not string");
						}
						if (row.getCell(2).getCellTypeEnum() == CellType.STRING
								|| row.getCell(2).getCellTypeEnum() == CellType.BLANK) {
							tbsCfg.setVariables(
									getSetOfCommaSeperatedVariables(row.getCell(2).getStringCellValue().split(",")));
						} else {
							errors.add("Row " + (i + 1) + " Cell 3 of Tabs Sheet is not String");
						}
						tbsCfg.setCards(parseCardsSheet(workbook, tbsCfg.getKey(), errors));
						tabs.add(tbsCfg);
					}
				}
			} else {
				errors.add("Invalid columns in Tabs sheets");
			}
		}
		logger.info("<--Exiting Parse Tabs Sheet()");
		return tabs;
	}

	public List<CardConfig> parseCardsSheet(Workbook workbook, String tabKey, List<String> errors) {
		logger.info("--> Entering Parse Cards Sheet()");
		Sheet sheet = workbook.getSheetAt(4);
		List<CardConfig> cards = new ArrayList<>();
		if (sheet.getPhysicalNumberOfRows() < 1) {
			errors.add("No of rows in Cards sheet is less than 1");
		} else {
			Row row1 = sheet.getRow(0);
			if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(1).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(2).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(3).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(0).getStringCellValue().equals("tabKey")
					&& row1.getCell(1).getStringCellValue().equals("label")
					&& row1.getCell(2).getStringCellValue().equals("key")
					&& row1.getCell(3).getStringCellValue().equals("variables")) {
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
					Row row = sheet.getRow(i);
					if (row != null) {
						CardConfig crdCfg = new CardConfig();
						if (row.getCell(0).getCellTypeEnum() == CellType.STRING
								&& row.getCell(0).getStringCellValue().equals(tabKey)) {
							if (row.getCell(1).getCellTypeEnum() == CellType.STRING) {
								crdCfg.setLabel(row.getCell(1).getStringCellValue().trim());
							} else {
								errors.add("Row " + (i + 1) + " Cell 2 of Cards Sheet is not string");
							}
							if (row.getCell(2).getCellTypeEnum() == CellType.STRING) {
								crdCfg.setKey(row.getCell(2).getStringCellValue().trim());
							} else {
								errors.add("Row " + (i + 1) + " Cell 3 of Cards Sheet is not string");
							}
							if (row.getCell(3).getCellTypeEnum() == CellType.STRING
									|| row.getCell(2).getCellTypeEnum() == CellType.BLANK) {
								crdCfg.setVariables(getSetOfCommaSeperatedVariables(
										row.getCell(3).getStringCellValue().split(",")));
							} else {
								errors.add("Row " + (i + 1) + " Cell 4 of Cards Sheet is not String");
							}
							crdCfg.setHeaders(parseHeadersSheet(workbook.getSheetAt(5), crdCfg.getKey(), errors));
							cards.add(crdCfg);
						}
					}
				}
			} else {
				errors.add("Invalid columns in Cards sheets");
			}
		}
		logger.info("<-- Exiting Parse Cards Sheet()");
		return cards;
	}

	public List<HeaderConfig> parseHeadersSheet(Sheet sheet, String cardKey, List<String> errors) {
		logger.info("--> Entering Parse Headers Sheet()");
		List<HeaderConfig> headers = new ArrayList<>();
		if (sheet.getPhysicalNumberOfRows() < 1) {
			errors.add("No of rows in Headers sheet is less than 1");
		} else {
			Row row1 = sheet.getRow(0);
			if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(1).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(2).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(3).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(0).getStringCellValue().equals("cardKey")
					&& row1.getCell(1).getStringCellValue().equals("label")
					&& row1.getCell(2).getStringCellValue().equals("key")
					&& row1.getCell(3).getStringCellValue().equals("variables")) {
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
					Row row = sheet.getRow(i);
					if (row != null) {
						HeaderConfig hdrCfg = new HeaderConfig();
						if (row.getCell(0).getCellTypeEnum() == CellType.STRING
								&& row.getCell(0).getStringCellValue().equals(cardKey)) {
							if (row.getCell(1).getCellTypeEnum() == CellType.STRING) {
								hdrCfg.setLabel(row.getCell(1).getStringCellValue().trim());
							} else {
								errors.add("Row " + ((i + 1)) + " Cell 2 of Headers Sheet is not string");
							}
							if (row.getCell(2).getCellTypeEnum() == CellType.STRING) {
								hdrCfg.setKey(row.getCell(2).getStringCellValue().trim());
							} else {
								errors.add("Row " + (i + 1) + " Cell 3 of Headers Sheet is not string");
							}
							if (row.getCell(3).getCellTypeEnum() == CellType.STRING
									|| row.getCell(2).getCellTypeEnum() == CellType.BLANK) {
								hdrCfg.setVariables(getSetOfCommaSeperatedVariables(
										row.getCell(3).getStringCellValue().split(",")));
							} else {
								errors.add("Row " + (i + 1) + " Cell 4 of Headers Sheet is not String");
							}
							headers.add(hdrCfg);
						}
					}
				}
			} else {
				errors.add("Invalid columns in Headers sheets");
			}
		}
		logger.info("<-- Exiting Parse Headers Sheet()");
		return headers;
	}

	public List<BucketConfig> parseBucketsSheet(Sheet sheet, List<String> errors) {
		logger.info("--> Entering Parse Buckets Sheet()");
		List<BucketConfig> buckets = new ArrayList<>();
		if (sheet.getPhysicalNumberOfRows() < 1) {
			errors.add("No of rows in buckets sheet is less than 1");

		} else {
			for (int i = 0; i < sheet.getPhysicalNumberOfRows() - 1; i++) {
				buckets.add(new BucketConfig());
			}
			Row row1 = sheet.getRow(0);
			if (row1.getCell(0).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(1).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(2).getCellTypeEnum() == CellType.STRING
					&& row1.getCell(0).getStringCellValue().equals("label")
					&& row1.getCell(1).getStringCellValue().equals("key")
					&& row1.getCell(2).getStringCellValue().equals("variables")) {
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
					Row row = sheet.getRow(i);
					if (row.getCell(0).getCellTypeEnum() == CellType.STRING) {
						buckets.get(i - 1).setLabel(row.getCell(0).getStringCellValue().trim());
					} else {
						errors.add("Row " + (i + 1) + " Cell 1 of Buckets Sheet is not string");
					}
					if (row.getCell(1).getCellTypeEnum() == CellType.STRING) {
						buckets.get(i - 1).setKey(row.getCell(1).getStringCellValue().trim());
					} else {
						errors.add("Row " + (i + 1) + " Cell 2 of Buckets Sheet is not string");

					}
					if (row.getCell(2).getCellTypeEnum() == CellType.STRING
							|| row.getCell(2).getCellTypeEnum() == CellType.BLANK) {
						buckets.get(i - 1).setVariables(
								getSetOfCommaSeperatedVariables(row.getCell(2).getStringCellValue().split(",")));
					} else {
						errors.add("Row " + (i + 1) + " Cell 3 of Buckets Sheet is not String");
					}
				}
			} else {
				errors.add("Invalid columns in buckets sheets");
			}
		}
		logger.info("<-- Exiting  Parse Buckets Sheet()");
		return buckets;
	}

	public ApiResponse configureDashboardService(HttpServletRequest request, String username, DashboardConfig data)
			throws JsonProcessingException {
		List<RoleConfig> roles = data.getRoles();
		for (RoleConfig newRole : roles) {
			configureRoleInDatabase(newRole);
		}
		return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());

	}

	public ApiResponse configureDashboardUsingExcel(MultipartFile configFile, boolean saveToDatabase)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info("--> Entering configure dashbord using Excel()");
		ApiResponse response = null;
		Calendar calendar = Calendar.getInstance();
		File file1 = new File("/tmp/" + calendar.getTimeInMillis() + "-" + configFile.getOriginalFilename());
		OutputStream outputStream;
		Workbook workbook = null;
		try {
			outputStream = new FileOutputStream(file1);
			IOUtils.copy(configFile.getInputStream(), outputStream);
			outputStream.close();
			workbook = new XSSFWorkbook(file1);
		} catch (FileNotFoundException e) {
			logger.error("File not found ", e);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		if (workbook.getNumberOfSheets() != 7) {
			logger.debug("No of sheets in template is not 7");
			response = new ApiResponse(HttpStatus.BAD_REQUEST, "No of sheets in template is not 7");
		} else {
			List<String> errors = new ArrayList<>();
			RoleConfig roleConfig = parseWorkbook(workbook, errors);
			if (errors.isEmpty()) {
				if (saveToDatabase) {
					configureRoleInDatabase(roleConfig);
					logger.debug("Successfully configured in database");
					response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), roleConfig);
				} else {
					logger.debug("parsing successful but not configured in database");
					response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), roleConfig);
				}
			} else {
				logger.debug("Error in excel Sheet");
				response = new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(), errors);
			}
		}
		try {
			logger.debug("Closing workbook");
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("<-- Exiting configure dashbord using Excel()");
		return response;
	}

	public ApiResponse getApplicationDataConfig(HttpServletRequest request, String username) {
		ApiResponse response = null;
		logger.info(" --> Entering get Application data config()");
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			Set<Tabs> tabs = role.getTabs();
			Set<Outcomes> outcomes = role.getOutcomes();
			response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(),
					new ApplicationDataConfig(role, tabs, outcomes));
		} else {
			logger.error("DashBoard not configured for role :"+roleName);
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<-- Exiting get Application data config()");
		return response;
	}

	public ApiResponse getTabDataForApplication(HttpServletRequest request, String username, String applicationNumber,
			String tabKey) {
		logger.info("--> Entering get tab data()");
		ApiResponse response = null;
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			Tabs tab = tabsDao.findByTabKeyAndRoleAndIsDeleted(tabKey, role, false);
			if (tab != null) {
				logger.debug("Getting process variables for app id : " + applicationNumber + " journey : "
						+ backOfficeJourneyConfig.getJourneyName());
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceBusinessKey(applicationNumber)
						.involvedUser(username)
						.includeProcessVariables().singleResult();
				if (processInstance != null) {
					response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(),
							new TabApplicationDataResponse(tab, processInstance.getProcessVariables()));
				} else {
					logger.error("Invalid los app id :" + applicationNumber);
					throw new InvalidApplicationIdException(
							BackOfficeConstants.getInvalidLOSApplicationIdMessage(applicationNumber));
				}
			} else {
				logger.error(tab != null ? tab.getName() : null + " tab not configured for role " + roleName);
				throw new TabNotConfiguredForRoleException(
						BackOfficeConstants.getTabNotConfiguredForRoleMessage(roleName));
			}
		} else {
			logger.error("Dash board not configured fot role " + roleName);
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<-- Exiting get tab data()");
		return response;
	}

	public ApiResponse saveApplicationData(HttpServletRequest request, String username, String applicationNumber,
			String tabKey, SaveApplicationData data) {
		logger.info("--> Entering save app data()");
		ApiResponse response = null;
		Map<String, String> errorMap = new HashMap<>();
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			Tabs tab = tabsDao.findByTabKeyAndRoleAndIsDeleted(tabKey, role, false);
			if (tab != null) {
				ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
						.processInstanceBusinessKey(applicationNumber)
						.active().includeProcessVariables().includeProcessVariables().singleResult();
				if (processInstance != null) {
					List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getId())
							.taskAssignee(username).active().list();
					if (tasks != null && !tasks.isEmpty()) {
						Map<String, Object> variablesToSave = data.getData();
						Set<String> keys = variablesToSave.keySet();
						Map<String, Object> processVariables = processInstance.getProcessVariables();
						for (String key : keys) {
							Variables variable = variablesDao.findByMappingAndRoleAndIsDeleted(key, role, false);
							if (variable != null){
								logger.debug("edited variable is:- "+variable.getMapping()+" "+variable.getType());
								if (((!processVariables.containsKey(key) || processVariables.get(key) == null)
										&& variable.isWritable()) || variable.isEditable()) {
									if (variable.getType().equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN)) {
										Options option = optionsDao.findByOptionKeyAndVariableAndIsDeleted(
												variablesToSave.get(key).toString(), variable, false);
										if (option != null) {
											processVariables.put(key, variablesToSave.get(key));
										} else {
											errorMap.put(variable.getMapping(),
													BackOfficeConstants.ERROR_OPTION_FOR_DROPDOWN);
										}
									} else if (variable.getType()
											.equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_UPLOAD)) {
										try {
											List<Object> currentDocuments = new ArrayList<>();
											if (processVariables.get(key) != null
													&& !processVariables.get(key).toString().equals("")) {
												JSONArray currentDocumentsArray = new JSONArray(
														processVariables.get(key).toString());
												if (currentDocumentsArray != null) {
													for (int i = 0; i < currentDocumentsArray.length(); i++) {
														currentDocuments.add(currentDocumentsArray.get(i));
													}
												}
											} else {
												currentDocuments = new ArrayList<>();
											}
											List<Object> newDocuments = new ArrayList<>();
											Gson gson = new GsonBuilder().disableHtmlEscaping().create();
											JSONArray newDocumentsArray = new JSONArray(CommonHelperFunctions
													.getStringValue(gson.toJson(variablesToSave.get(key))));
											if (newDocumentsArray != null) {
												for (int i = 0; i < newDocumentsArray.length(); i++) {
													newDocuments.add(newDocumentsArray.get(i));
												}
											}
											currentDocuments.addAll(newDocuments);
											JSONArray allDocuments = new JSONArray(currentDocuments);
											processVariables.put(key, allDocuments.toString());
										} catch (JSONException e) {
											logger.debug(CommonHelperFunctions.getStackTrace(e));
											errorMap.put(variable.getMapping(), BackOfficeConstants.ERROR_FOR_UPLOAD);
										}
									} else if (variable.getType()
											.equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_NUMBER)) {
										try {
											processVariables.put(key,
													Double.parseDouble(variablesToSave.get(key).toString()));
										} catch (NumberFormatException e) {
											logger.debug(CommonHelperFunctions.getStackTrace(e));
											errorMap.put(variable.getMapping(), BackOfficeConstants.ERROR_FOR_NUMBER);
										}
									}
									else if (variable.getType().equalsIgnoreCase("long")) {
										try {
											processVariables.put(key,
													Integer.parseInt(variablesToSave.get(key).toString()));
										} catch (NumberFormatException e) {
											logger.debug(CommonHelperFunctions.getStackTrace(e));
											errorMap.put(variable.getMapping(), BackOfficeConstants.ERROR_FOR_NUMBER);
										}
									}
									else {
										logger.debug("modified value for variable ::"+variable.getMapping()+" is: "+variablesToSave.get(key));
										processVariables.put(key, variablesToSave.get(key));
									}
								} else {
									logger.debug(key + " variable is not editable/writable for logged in role.");
								}
							}else {
								logger.debug(key + " variable is not available for logged in role");
							}
						}
						if (errorMap.isEmpty()) {
							runtimeService.setVariables(processInstance.getId(), processVariables);
							response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(),
									new TabApplicationDataResponse(tab, processVariables));
						} else {
							response = new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase(),
									errorMap);
						}
					} else {
						throw new UserNotInvolvedInApplicationException(
								BackOfficeConstants.getUserNotInvolvedInApplicationMessage(applicationNumber));
					}
				} else {
					throw new InvalidApplicationIdException(
							BackOfficeConstants.getInvalidLOSApplicationIdMessage(applicationNumber));
				}
			} else {
				throw new TabNotConfiguredForRoleException(
						BackOfficeConstants.getTabNotConfiguredForRoleMessage(roleName));
			}
		} else {
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<-- Exiting save app data()");
		return response;
	}


	public ApiResponse submitApplication(HttpServletRequest request, String username, String applicationNumber,
			SubmitApplication submitData) {
		logger.info("--> Entering Submit application() ");
		ApiResponse response = null;
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			ProcessInstance process = runtimeService.createProcessInstanceQuery()
					.processInstanceBusinessKey(applicationNumber)
					.singleResult();
			if (process != null) {
				List<Task> tasks = taskService.createTaskQuery().processInstanceId(process.getId())
						.taskAssignee(username).active().orderByTaskCreateTime().desc().list();
				if (tasks != null && !tasks.isEmpty()) {
					Task task = tasks.get(0);
					Outcomes outcome = outcomeDao.findByOutcomeKeyAndRoleAndIsDeleted(submitData.getOutcome(), role,
							false);
					if (outcome != null) {
						if (outcome.isCommentRequired()
								&& (submitData.getComment() == null || submitData.getComment().trim().equals(""))) {
							throw new CommentIsNullException(
									"Comment is mandatory for outcome : " + outcome.getOutcomeKey());
						} else {
							Map<String, Object> dataToSave = new HashMap<>();
							dataToSave.put(BackOfficeConstants.STATUS_VARIABLE_KEY, submitData.getOutcome());
							dataToSave.put(BackOfficeConstants.STATUS_MESSAGE_VARIABLE_KEY,
									outcome.getLabel() + " by " + role.getRoleName());
							dataToSave.put("backoffice-" + role.getRoleName() + "-outcome", submitData.getOutcome());
							runtimeService.setVariables(process.getId(), dataToSave);
							if (submitData.getComment() != null && !submitData.getComment().trim().equals("")) {
								taskService.addComment(null, process.getId(), "", submitData.getComment());
							}
							taskService.complete(task.getId());
							response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
						}
					} else {
						throw new InvalidOutcomeException(
								BackOfficeConstants.getInvalidOutcomeMessage(submitData.getOutcome()));
					}
				} else {
					throw new UserNotInvolvedInApplicationException(
							BackOfficeConstants.getUserNotInvolvedInApplicationMessage(applicationNumber));
				}
			} else {
				throw new InvalidApplicationIdException(
						BackOfficeConstants.getInvalidLOSApplicationIdMessage(applicationNumber));
			}
		} else {
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<--Exiting Submit application()");
		return response;
	}

	public ApiResponse getComment(HttpServletRequest request, String username, String applicationNumber,
			CommentFilter filters) {
		logger.info("--> Entering get comments()");
		ApiResponse response = null;
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			if (role.isEnableComments()) {
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceBusinessKey(applicationNumber)
						.involvedUser(username)
						.includeProcessVariables().singleResult();
				if (processInstance != null) {
					response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(),
							new GetCommentsResponse(taskService.getProcessInstanceComments(processInstance.getId())));
				} else {
					throw new InvalidApplicationIdException(
							BackOfficeConstants.getInvalidLOSApplicationIdMessage(applicationNumber));
				}
			} else {
				throw new CommentsNotEnabledForRoleException(BackOfficeConstants.getCommentNotEnabledMessage(roleName));
			}
		} else {
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<-- Exiting get comments()");
		return response;
	}

	public ApiResponse submitComment(HttpServletRequest request, String username, String applicationNumber,
			SubmitCommentRequest comment) {
		logger.info("--> Entering Submit comments for application()");
		ApiResponse response = null;
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		if (role != null) {
			if (role.isEnableComments()) {
				ProcessInstance process = runtimeService.createProcessInstanceQuery()
						.processInstanceBusinessKey(applicationNumber)
						.singleResult();
				if (process != null) {
					List<Task> task = taskService.createTaskQuery().processInstanceId(process.getId())
							.taskAssignee(username).active().list();
					if (task != null && !task.isEmpty()) {
						response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), new CommentData(
								taskService.addComment(null, process.getId(), "", comment.getMessage())));
					} else {
						throw new UserNotInvolvedInApplicationException(
								BackOfficeConstants.getUserNotInvolvedInApplicationMessage(applicationNumber));
					}
				} else {
					throw new InvalidApplicationIdException(
							BackOfficeConstants.getInvalidLOSApplicationIdMessage(applicationNumber));
				}
			} else {
				throw new CommentsNotEnabledForRoleException(BackOfficeConstants.getCommentNotEnabledMessage(roleName));
			}
		} else {
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<--Exiting comments for application()");
		return response;
	}

	/**
	 * This method is used for getting bucket variables configuration for given role
	 * 
	 * @param request
	 * @param name
	 * @return
	 */
	public ApiResponse getBucketsConfiguration(HttpServletRequest request, String name) {
		ApiResponse response = null;
		logger.info("--> Entering get buckets configuration()");
		String roleName = BackOfficeUtils.checkRoleInRequest(request);
		Roles role = rolesDao.findByRoleNameAndIsDeleted(roleName, false);
		Set<BucketVariablesConfig> bucketVariablesConfig = new LinkedHashSet<>();
		if (role != null) {
			Set<Buckets> buckets = role.getBuckets();
			Set<AppVariablesConfig> variables = new LinkedHashSet<>();
			bucketVariablesConfig = new LinkedHashSet<>();
			for (Buckets bucket : buckets) {
				Set<BucketsVariablesMapping> bucketVariableMapping = bucketsVariablesDao
						.findByBucketAndIsDeletedOrderByBucketVariableOrderAsc(bucket, false);
				variables = new LinkedHashSet<>();
				for (BucketsVariablesMapping buckVarMaping : bucketVariableMapping) {
					AppVariablesConfig bucketsConfig = new AppVariablesConfig(buckVarMaping.getVariable());
					variables.add(bucketsConfig);
				}
				bucketVariablesConfig.add(new BucketVariablesConfig(bucket, variables));
			}
			response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(),
					new AppConfig(bucketVariablesConfig));
		} else {
			throw new DashboardNotConfiguredForRoleException(
					BackOfficeConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}
		logger.info("<--Exiting get buckets configuration()");
		return response;
	}

  /**
   * This method is used for getting bucket variables configuration for given role
   *
   * @param request
   * @param username
   * @return
   */
  public ApiResponse getMobileBucketsConfiguration(HttpServletRequest request, String username) {
		logger.info("--> Entered get List of Assigned cases()");
		ApiResponse response = new ApiResponse();
		Map<String, Object> responseObject = new HashMap<>();
		Map<String, Object> bucketMap = new HashMap<>();
		List<Map<String, Object>> headerList = new ArrayList<>();
		List<Map<String, Object>> bucketList = new ArrayList<>();
		NativeHistoricTaskInstanceQuery query = historyService.createNativeHistoricTaskInstanceQuery().sql("select * from ACT_HI_TASKINST where ASSIGNEE_ = \"" + username
				+ "\" AND SCOPE_TYPE_ is NULL and END_TIME_ IS NULL order by START_TIME_ DESC ;");

		List<HistoricTaskInstance> tasks = query.list();
		Set<String> addedApplicationsList = new HashSet<>();
		if (tasks.size() > 0) {
			HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(tasks.get(0).getProcessInstanceId()).includeProcessVariables().singleResult();
			String businessKey = processInstance.getBusinessKey();
			Map<String, Object> processVariables = processInstance.getProcessVariables();
			String listViewColumns = CommonHelperFunctions.getStringValue(processVariables.get("listViewColumnsToDisplay"));
			String listViewColumnsLabel = CommonHelperFunctions.getStringValue(processVariables.get("listViewColumnsHeader"));
			if (!listViewColumns.isEmpty() && !listViewColumnsLabel.isEmpty()) {
				String[] coulmnList = listViewColumns.split(",");
				String[] coulmnLabelList = listViewColumnsLabel.split(",");
				for (int i=0; i < coulmnLabelList.length; i++) {
					Map<String, Object> coulumnMap = new HashMap<>();
					coulumnMap.put("key", coulmnList[i]);
					coulumnMap.put("label", coulmnLabelList[i]);
					headerList.add(coulumnMap);
				}
				bucketMap.put("id", "taskList");
				bucketMap.put("label", "Task List");
				bucketMap.put("variables", headerList);
			}
		}
		bucketList.add(bucketMap);
		responseObject.put("buckets", bucketList);
		response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseObject);
		logger.info("<-- Exiting get bucket variable configuration ");
		return response;
	}
}
