package com.kuliza.lending.backoffice.utils;

import static com.kuliza.lending.backoffice.utils.BackOfficeConstants.KEYCLOAK_ROLE_NAME_INDEX_FROM_END;
import static com.kuliza.lending.backoffice.utils.BackOfficeConstants.KEYCLOAK_ROLE_NAME_STARTING_INDEX;

import com.kuliza.lending.backoffice.exceptions.RoleNotAvailableException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackOfficeUtils {

	private static final Logger logger = LoggerFactory.getLogger(BackOfficeUtils.class);

	public static String checkRoleInRequest(HttpServletRequest request) {
		String roleName = "";
		if (request.getAttribute("userRole") != null) {
			roleName = request.getAttribute("userRole").toString();
			if (roleName.startsWith("[ROLE_")) {
				roleName = roleName.substring(KEYCLOAK_ROLE_NAME_STARTING_INDEX, roleName.length()-KEYCLOAK_ROLE_NAME_INDEX_FROM_END);
			}
		} else {
			throw new RoleNotAvailableException("Role not available in request object");
		}
		logger.info("ROLE NAME "+roleName);
		return roleName;
	}

	/**
	 * This is an utility method used to check if a given string is not null and
	 * not empty
	 * 
	 * @param value
	 * @return true if String is not empty else false
	 */
	public static boolean isNotEmpty(String value) {
		return (value != null) && (!value.trim().equals(""));
	}

}
