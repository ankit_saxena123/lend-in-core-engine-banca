package com.kuliza.lending.backoffice;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.kuliza" })
@EnableJpaRepositories(basePackages = "com.kuliza")
@EntityScan(basePackages = "com.kuliza")
@EnableConfigurationProperties(LiquibaseProperties.class)
public class BackOfficeApplication {

	@Autowired
	private LiquibaseProperties properties;
	@Autowired
	private DataSource dataSource;

	public static void main(String[] args) {
		SpringApplication.run(BackOfficeApplication.class, args);

	}
	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog(this.properties.getChangeLog());
		liquibase.setContexts(this.properties.getContexts());
		liquibase.setDefaultSchema(this.properties.getDefaultSchema());
		liquibase.setDropFirst(this.properties.isDropFirst());
		liquibase.setShouldRun(this.properties.isEnabled());
		liquibase.setLabels(this.properties.getLabels());
		liquibase.setChangeLogParameters(this.properties.getParameters());
		liquibase.setRollbackFile(this.properties.getRollbackFile());
		liquibase.setDatabaseChangeLogLockTable("BO_DATABASECHANGELOGLOCK");
		liquibase.setDatabaseChangeLogTable("BO_DATABASECHANGELOG");
		return liquibase;

	}
}
