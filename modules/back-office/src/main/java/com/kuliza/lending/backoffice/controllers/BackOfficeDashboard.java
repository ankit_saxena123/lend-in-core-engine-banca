package com.kuliza.lending.backoffice.controllers;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.kuliza.lending.backoffice.config.BackOfficeJourneyConfig;
import com.kuliza.lending.backoffice.pojo.dashboard.CommentFilter;
import com.kuliza.lending.backoffice.pojo.dashboard.SaveApplicationData;
import com.kuliza.lending.backoffice.pojo.dashboard.SubmitApplication;
import com.kuliza.lending.backoffice.pojo.dashboard.SubmitCommentRequest;
import com.kuliza.lending.backoffice.pojo.dashboard.configure.DashboardConfig;
import com.kuliza.lending.backoffice.services.BackOfficeDashboardServices;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;

@RestController
@RequestMapping("/lending/backoffice/dashboard")
public class BackOfficeDashboard {

	@Autowired
	private BackOfficeDashboardServices backOfficeDashboardServices;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private BackOfficeJourneyConfig backOfficeJourneyConfig;

	// this is a mock controller for testing purpose
	@GetMapping("/initiate")
	private ApiResponse initiateProcess() {
		JSONObject processVariable = new JSONObject(
				"{\"mobileNumber\":\"9890989090\",\"borrowerFullName\":\"Maninder Singh\",\"gender\":\"Male\",\"loanAmount\":\"20000000\",\"permanentDistrict\":\"Srinagar\",\"loanAgreementId\":\"20180820-420\"}");
		JSONArray keys = processVariable.names();
		Map<String, Object> pv = new HashMap<>();
		for (int i = 0; i < keys.length(); i++) {
			pv.put(keys.getString(i), processVariable.get(keys.getString(i)).toString());
		}
		String appId = Long.toString((long) (Math.random() * 1000));
		ProcessInstance proc = runtimeService.startProcessInstanceByKey(backOfficeJourneyConfig.getJourneyName(), appId,
				pv);
		return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), proc.getId() + "#####" + appId);
	}

	// this is a mock controller for demo purpose only
	@GetMapping("/delete-config")
	private ResponseEntity<Object> deleteAllBOConfig(
			@RequestParam(required = false, name = "roleName") String roleName) {
		return CommonHelperFunctions.buildResponseEntity(backOfficeDashboardServices.deleteAllBOConfig(roleName));
	}

	@PostMapping("/configure")
	private ResponseEntity<Object> configureDashboard(HttpServletRequest request, Principal principal,
			@RequestBody DashboardConfig data) throws IOException {
		return CommonHelperFunctions.buildResponseEntity(
				backOfficeDashboardServices.configureDashboardService(request, principal.getName(), data));
	}

	@GetMapping("/assignedList/{bucketKey}")
	private ResponseEntity<Object> getListOfAssignedCases(HttpServletRequest request, Principal principal,
			@PathVariable(name = "bucketKey") String bucketKey, @RequestParam Map<String, String> map)
			throws Exception {
		return CommonHelperFunctions.buildResponseEntity(
				backOfficeDashboardServices.getListOfAssignedCases(request, principal.getName(), bucketKey, map));
	}

	@PostMapping(value = "/upload-config-xls", consumes = "multipart/form-data")
	public ResponseEntity<Object> uploadConfigXlsFile(Principal principal, @RequestParam("file") MultipartFile xlsFile,
			@RequestParam(required = true, name = "saveToDatabase") boolean saveToDatabase)
			throws JsonParseException, JsonMappingException, IOException {
		return CommonHelperFunctions
				.buildResponseEntity(backOfficeDashboardServices.configureDashboardUsingExcel(xlsFile, saveToDatabase));

	}

	@GetMapping("/appData/config")
	private ResponseEntity<Object> getApplicationDataConfig(HttpServletRequest request, Principal principal) {
		return CommonHelperFunctions.buildResponseEntity(
				backOfficeDashboardServices.getApplicationDataConfig(request, principal.getName()));
	}

	@GetMapping("/{LOSApplicationNumber:^[0-9]+$}/{tab}")
	private ResponseEntity<Object> getApplicationData(HttpServletRequest request, Principal principal,
			@PathVariable(name = "LOSApplicationNumber") String LOSApplicationNumber,
			@PathVariable(name = "tab") String tab) {
		return CommonHelperFunctions.buildResponseEntity(backOfficeDashboardServices.getTabDataForApplication(request,
				principal.getName(), LOSApplicationNumber, tab));
	}

	@PostMapping("/{LOSApplicationNumber:^[0-9]+$}/{tab}/save")
	private ResponseEntity<Object> saveApplicationData(HttpServletRequest request, Principal principal,
			@PathVariable(name = "LOSApplicationNumber") String LOSApplicationNumber,
			@PathVariable(name = "tab") String tab, @Valid @RequestBody SaveApplicationData data) {
		return CommonHelperFunctions.buildResponseEntity(backOfficeDashboardServices.saveApplicationData(request,
				principal.getName(), LOSApplicationNumber, tab, data));
	}

	@PostMapping("/{LOSApplicationNumber:^[0-9]+$}/submit")
	private ResponseEntity<Object> submitApplication(HttpServletRequest request, Principal principal,
			@PathVariable(name = "LOSApplicationNumber") String LOSApplicationNumber,
			@Valid @RequestBody SubmitApplication outcome) {
		return CommonHelperFunctions.buildResponseEntity(backOfficeDashboardServices.submitApplication(request,
				principal.getName(), LOSApplicationNumber, outcome));
	}

	@GetMapping("/{LOSApplicationNumber:^[0-9]+$}/comment")
	private ResponseEntity<Object> getApplicationComments(HttpServletRequest request, Principal principal,
			@PathVariable(name = "LOSApplicationNumber") String LOSApplicationNumber,
			@RequestParam(required = false) CommentFilter filters) {
		return CommonHelperFunctions.buildResponseEntity(
				backOfficeDashboardServices.getComment(request, principal.getName(), LOSApplicationNumber, filters));
	}

	@PostMapping("/{LOSApplicationNumber:^[0-9]+$}/comment")
	private ResponseEntity<Object> submitApplicationComment(HttpServletRequest request, Principal principal,
			@PathVariable(name = "LOSApplicationNumber") String LOSApplicationNumber,
			@Valid @RequestBody SubmitCommentRequest comment) {
		return CommonHelperFunctions.buildResponseEntity(
				backOfficeDashboardServices.submitComment(request, principal.getName(), LOSApplicationNumber, comment));
	}

	@GetMapping("/buckets/configuration")
	private ResponseEntity<Object> getBucketsConfiguration(HttpServletRequest request, Principal principal) {
		return CommonHelperFunctions
				.buildResponseEntity(backOfficeDashboardServices.getBucketsConfiguration(request, principal.getName()));
	}


	@GetMapping("/mobile/buckets/configuration")
	private ResponseEntity<Object> getMobileBucketsConfiguration(HttpServletRequest request, Principal principal) {
		return CommonHelperFunctions
				.buildResponseEntity(backOfficeDashboardServices.getMobileBucketsConfiguration(request, principal.getName()));
	}

	/*
	 * FETCH DASHBOARD ASSIGNED LIST IN MOBILE APP
	 * It will fetch all assigned cases for the given user(will be fetched from task).
	 * */
	@GetMapping("/mobile/assignedList")
	private ResponseEntity<Object> getAssignedUserList(HttpServletRequest request, Principal principal) throws Exception{
		return CommonHelperFunctions
				.buildResponseEntity(backOfficeDashboardServices.getAssignedListOfMobileDashboard(request, principal.getName()));
	}

}
