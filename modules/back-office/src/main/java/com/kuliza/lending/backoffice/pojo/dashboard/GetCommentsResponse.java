package com.kuliza.lending.backoffice.pojo.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.flowable.engine.task.Comment;

public class GetCommentsResponse {

	private List<CommentData> comments;

	public GetCommentsResponse() {
		super();
	}

	public GetCommentsResponse(List<Comment> comments) {
		super();
		this.comments = new ArrayList<>();
		for (Comment comment : comments) {
			this.comments.add(new CommentData(comment));
		}
	}

	public List<CommentData> getComments() {
		return comments;
	}

	public void setComments(List<CommentData> comments) {
		this.comments = comments;
	}

}
