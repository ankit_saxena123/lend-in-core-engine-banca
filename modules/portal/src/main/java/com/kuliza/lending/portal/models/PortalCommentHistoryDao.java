package com.kuliza.lending.portal.models;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PortalCommentHistoryDao extends CrudRepository<PortalCommentHistory, Long> {

  public List<PortalCommentHistory> findByPortalUserCaseInstanceId(String caseInstanceId);

  public List<PortalCommentHistory> findByTaskKeyAndPortalUserCaseInstanceId(String tabKey, String caseInstanceId);

}
