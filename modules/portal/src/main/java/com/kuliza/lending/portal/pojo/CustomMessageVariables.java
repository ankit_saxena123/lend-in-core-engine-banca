package com.kuliza.lending.portal.pojo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.kuliza.lending.portal.utils.EnumConstants.Events;

@JsonSubTypes({ @Type(value = Map.class), @Type(value = List.class), })
public class CustomMessageVariables implements Serializable {
	private static final long serialVersionUID = 1L;
	private String requestId;
	private String transactionId;
	private Events event;
	private Map<String, Object> functionArguments;

	public Events getEvent() {
		return event;
	}

	public void setEvent(Events event) {
		this.event = event;
	}

	public Map<String, Object> getFunctionArguments() {
		return functionArguments;
	}

	public void setFunctionArguments(Map<String, Object> functionArguments) {
		this.functionArguments = functionArguments;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public CustomMessageVariables() {
		super();
	}

	public CustomMessageVariables(String requestId, Events event, Map<String, Object> functionArguments) {
		super();
		this.requestId = requestId;
		this.event = event;
		this.functionArguments = functionArguments;
	}

	public CustomMessageVariables(String requestId, String transactionId, Events event, Map<String, Object> functionArguments) {
		super();
		this.requestId = requestId;
		this.transactionId = transactionId;
		this.event = event;
		this.functionArguments = functionArguments;
	}

	@Override
	public String toString() {
		return "CustomMessageVariables{" +
				"requestId='" + requestId + '\'' +
				", transactionId='" + transactionId + '\'' +
				", event=" + event +
				", functionArguments=" + functionArguments +
				'}';
	}
}
