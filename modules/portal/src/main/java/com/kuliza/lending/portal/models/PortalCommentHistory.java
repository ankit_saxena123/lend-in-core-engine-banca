package com.kuliza.lending.portal.models;

import static com.kuliza.lending.common.utils.CommonHelperFunctions.getDateStringValue;
import static com.kuliza.lending.portal.utils.PortalConstants.COMMENT_HISTORY_DATE_FORMATE;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.common.model.BaseModel;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class PortalCommentHistory extends BaseModel {

	@ApiModelProperty(required = false, hidden = true)
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(referencedColumnName = "id")
	private PortalUser portalUser;

	@Column
	@ApiModelProperty(required = false, hidden = true)
	private String userId;

	@Column
	@ApiModelProperty(required = false, hidden = true)
	private String roleName;

	@Column
	@ApiModelProperty(required = false, hidden = true)
	private String taskKey;

	@Column
//	@NotNull(message = "message can not be null")
//	@NotEmpty(message = "message can not be empty")
	private String message;

	@Column(columnDefinition = "varchar(22) default 'portalComment'")
	private String commentType;

	public PortalUser getPortalUser() {
		return portalUser;
	}

	public void setPortalUser(PortalUser portalUser) {
		this.portalUser = portalUser;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getTaskKey() {
		return taskKey;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCommentType() {
		return commentType;
	}

	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}

	public String getCreatedTime() {
		return this.getCreated() == null ? null : getDateStringValue(this.getCreated(), COMMENT_HISTORY_DATE_FORMATE);
	}

	public String getModifiedTime() {
		return this.getModified() == null ? null : getDateStringValue(this.getModified(), COMMENT_HISTORY_DATE_FORMATE);
	}

	public PortalCommentHistory() {
		super();
		this.setIsDeleted(false);
	}

	public PortalCommentHistory(PortalUser portalUser, String message, String userId, String roleName, String taskKey) {
		this.portalUser = portalUser;
		this.message = message;
		this.userId = userId;
		this.roleName = roleName;
		this.taskKey = taskKey;
		this.setIsDeleted(false);
	}

	public PortalCommentHistory(PortalUser portalUser, String message, String userId, String roleName, String taskKey,
			String commentType) {
		this.portalUser = portalUser;
		this.message = message;
		this.userId = userId;
		this.roleName = roleName;
		this.taskKey = taskKey;
		this.commentType = commentType;
		this.setIsDeleted(false);
	}
}
