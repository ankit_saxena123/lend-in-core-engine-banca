package com.kuliza.lending.portal.aspects;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.BrokerConfigs;
import com.kuliza.lending.portal.broker.CommonListenerService;
import com.kuliza.lending.portal.broker.MessageProducerJMS;
import com.kuliza.lending.portal.broker.MessageProducerKafka;
import com.kuliza.lending.portal.broker.RabbitCustomMessageProducer;
import com.kuliza.lending.portal.utils.EnumConstants.Events;

@Aspect
@Component
@ConditionalOnExpression("${portal.audit.service.init.enabled:true}")
public class AuditBeforeAdviceAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditBeforeAdviceAspect.class);

	@Autowired
	private MessageProducerJMS messageProducerJMS;

	@Autowired
	private RabbitCustomMessageProducer rabbitCustomProducer;
	
	@Autowired
	private MessageProducerKafka kafkaMessageProducer;
	
	@Autowired
	private CommonListenerService commonListenerService;

	@Before(value = "com.kuliza.lending.portal.aspects.AuditPointcuts.combinedAuditAPIs()")
	public void logBefore(JoinPoint joinPoint) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		LOGGER.debug("Entering in Method : " + joinPoint.getSignature());
		LOGGER.debug("Class Name : " + joinPoint.getSignature().getDeclaringTypeName());
		LOGGER.debug("Target class : " + joinPoint.getTarget().getClass().getName());
		Object[] args = joinPoint.getArgs();
		MethodSignature methodSignature = (MethodSignature) joinPoint.getStaticPart().getSignature();
		Method method = methodSignature.getMethod();

		Annotation[][] parameterAnnotations = method.getParameterAnnotations();
		if (null != request) {
			if (CommonHelperFunctions.getStringValue(request.getAttribute("requestId")).isEmpty()) {
				String jsonStr = "";
				for (int argIndex = 0; argIndex < args.length; argIndex++) {
					for (Annotation annotation : parameterAnnotations[argIndex]) {
						if (!(annotation instanceof RequestParam) && !(annotation instanceof RequestBody))
							continue;
						if (annotation instanceof RequestParam) {
							RequestParam requestParam = (RequestParam) annotation;
							if ("requestId".equals(requestParam.value())
									&& (args[argIndex] != null && !args[argIndex].equals(""))) {
								LOGGER.info("Skipping aspect for requestId as it exists in parameter");
								return;
							}
							if ("Authorization".equals(requestParam.value()))
			                    continue;
						}
						if (annotation instanceof RequestBody) {
							ObjectMapper Obj = new ObjectMapper();
							try {
								jsonStr = Obj.writeValueAsString(args[argIndex]);
							} catch (JsonProcessingException | JSONException e) {
								LOGGER.error(CommonHelperFunctions.getStackTrace(e));
							}
						}
					}
				}
				request.setAttribute("requestId", UUID.randomUUID().toString());
				try {
					if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
						messageProducerJMS.publishAuditEvent(Events.IncomingRequestEvent, request, jsonStr);
					}  else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
						kafkaMessageProducer.publishAuditEvent(Events.IncomingRequestEvent, request, jsonStr);
					} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")){
						rabbitCustomProducer.publishAuditEvent(Events.IncomingRequestEvent, request, jsonStr);
					} else {
					  commonListenerService.saveAuditEntrySync(Events.IncomingRequestEvent, request, jsonStr);
					}
				} catch (Exception e) {// IOException | TimeoutException |
					LOGGER.error("Error in publishing the event");
					LOGGER.error(CommonHelperFunctions.getStackTrace(e));
				}
			} else {
				LOGGER.info("Skipping aspect for requestId as it exists in attribute");
			}
		}
	}
}
