package com.kuliza.lending.portal.controllers;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.models.CustomURLMapper;
import com.kuliza.lending.engine_common.utils.EngineCommonConstants;
import com.kuliza.lending.portal.misc.AllocationServiceTask;
import com.kuliza.lending.portal.models.PortalCommentHistory;
import com.kuliza.lending.portal.models.PortalConfigurationModel;
import com.kuliza.lending.portal.pojo.BucketConfiguration;
import com.kuliza.lending.portal.pojo.ClaimUnclaimInput;
import com.kuliza.lending.portal.service.PortalService;
import com.kuliza.lending.portal.utils.PortalConstants;
import com.kuliza.lending.portal.utils.SwaggerConstants;
import io.swagger.annotations.*;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import static com.kuliza.lending.portal.utils.PortalConstants.PORTAL_API_URL;

@RestController
@RequestMapping(PORTAL_API_URL)
@Api(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PortalController {

	@Autowired
	private PortalService portalService;

	@Autowired
	private AllocationServiceTask allocationServiceTask;

	@ApiOperation(value = SwaggerConstants.PC_PORTAL_CONFIGURE, response = PortalConfigurationModel.class )
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_UPDATE_PRODUCT_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = "/configurator/add-product-config")
	public ResponseEntity<Object> configurePortal(Principal principal,
			@Valid @RequestBody PortalConfigurationModel portalConfigurationModel) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(
				portalService.configurePortal(principal.getName(), portalConfigurationModel));
	}

	@ApiOperation(value = SwaggerConstants.PC_UPDATE_PORTAL_CONFIGURE)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_UPDATE_PRODUCT_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.PUT, value = "/configurator/update-product-config")
	public ResponseEntity<Object> updateConfigurePortal(Principal principal,
			 @Valid @RequestBody PortalConfigurationModel portalConfigurationModel) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(
				portalService.updateConfigurePortal(principal.getName(), portalConfigurationModel));
	}

	@ApiOperation(value = SwaggerConstants.PC_GET_PORTAL_CONFIGURE)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_PORTAL_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "/configurator/{roleName}/{productType}")
	public ResponseEntity<Object> getProductConfig(ServletRequest request, @ApiParam(value = SwaggerConstants.PC_PRODUCT_GET_PORTAL ,
			required = true, example = "kulizaPersonalLoan") @PathVariable(name = "productType") @NotEmpty String productType,
			@ApiParam(value = SwaggerConstants.PC_ROLENAME_GET_PORTAL,required = true
			, example = "cmmnCreditAnalystPOCAdmin")
			@PathVariable(name = "roleName") @NotEmpty String roleName) throws Exception {
		portalService.validateRequestResponse(request, roleName, null);
		return CommonHelperFunctions.buildResponseEntity(portalService.getConfigurePortal(roleName, productType));
	}

	@ApiOperation(value = SwaggerConstants.PC_DEL_PORTAL_CONFIGURE)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_DELETE_PORTAL_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.DELETE, value = "/configurator/{roleName}/{productType}")
	public ResponseEntity<Object> deleteConfigurePortal(ServletRequest request, @ApiParam(value = SwaggerConstants.PC_PRODUCT_DEL_PORTAL ,
			required = true, example = "personalLoan") @PathVariable(name = "productType") @NotEmpty String productType,
			@ApiParam(value = SwaggerConstants.PC_ROLENAME_DEL_PORTAL, required = true
					, example = "cmmnCreditAnalystPOCAdmin") @PathVariable(name = "roleName") @NotEmpty String roleName) throws Exception {
		portalService.validateRequestResponse(request, roleName, null);
		return CommonHelperFunctions.buildResponseEntity(portalService.deleteConfigurePortal(roleName, productType));
	}

	@ApiOperation(value = SwaggerConstants.PC_ADD_BUCKET_CONFIG_DESC)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_ADD_BUCKET_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = "/configurator/{roleName}/{productType}/add-bucket-config")
	public ResponseEntity<Object> addBucketConfig(Principal principal, ServletRequest request,
		  @ApiParam(value = SwaggerConstants.PC_ROLENAME_ADD_BUCKET,required = true
				  , example = "cmmnCreditAnalystPOCAdmin") @PathVariable(name = "roleName" , required = true) @NotEmpty String roleName,
			@ApiParam(value = SwaggerConstants.PC_PRODUCT_ADD_BUCKET ,
			required = true, example = "personalLoan") @PathVariable(name = "productType" , required = true) @NotEmpty String productType,
			@Valid @RequestBody List<BucketConfiguration> bucketConfigurationList) throws Exception {
		portalService.validateRequestResponse(request, roleName, null);
		return CommonHelperFunctions.buildResponseEntity(
				portalService.addBucketConfig(roleName, principal.getName(), productType, bucketConfigurationList));
	}

	/*@ApiOperation(value = Constants.PC_GET_BUCKET_CONFIG , response = BucketConfiguration.class, responseContainer="List")
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	 @RequestMapping(method = RequestMethod.GET, value = "/configurator/{roleName}/{productType}/get-bucket-config")
	public ResponseEntity<Object> getBucketConfigList(@ApiParam(value = Constants.PC_ROLENAME_GET_BUCKET,required = true
			, example = "cmmnCreditAnalystPOCAdmin") @PathVariable(name = "roleName") @NotEmpty String roleName,
	 @ApiParam(value = Constants.PC_PRODUCT_GET_BUCKET ,
			required = true, example = "personalLoan") @PathVariable(name = "productType") @NotEmpty String productType) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(portalService.getBucketConfigList(
	 roleName, productType));
	}*/

	@ApiOperation(value = SwaggerConstants.PC_GET_ASSIGNED_LIST_DESC)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_ASSIGNED_LIST, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "/assignedList/{roleName}/{groupId}/{productType}/{bucketKey}")
	public ResponseEntity<Object> getListOfAssignedCases(Principal principal, ServletRequest request,
		  @ApiParam(value="Role name",required = true, example = "cmmnCreditAnalystPOCAdmin")
			@PathVariable(name = "roleName") @NotEmpty String roleName,
			@ApiParam(value = "Group id", required = true, example = "2214e328-6789-43de-9229-3142414d80ef")
			@PathVariable(name = "groupId") @NotEmpty String groupId,
		  @ApiParam(value="Product type", required = true, example = "personalLoan")
		  @PathVariable(name = "productType") @NotEmpty String productType,
			@ApiParam(value = "Bucket Key", required = true, example = "pending")
			@PathVariable(name = "bucketKey") String bucketKey,
			@ApiParam(value = "Map" , required = true, example = "offset=1&pageWidth=2")
			@RequestParam Map<String, String> map) throws Exception {
		portalService.validateRequestResponse(request, roleName, groupId);
		return CommonHelperFunctions.buildResponseEntity(
				portalService.getAssignedCaseList(roleName, principal.getName(), groupId, productType,
						bucketKey, map));
	}

	@ApiOperation(value = SwaggerConstants.PC_GET_PROD_CONFIG)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_PRODUCT_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "/get-product-config/{roleName}")
	public ResponseEntity<Object> getProductConfig(ServletRequest request, @ApiParam(value = SwaggerConstants.PC_ROLENAME_GET_PROD_CONFIG, required = true
			, example = "cmmnCreditAnalystPOCAdmin")@PathVariable(name = "roleName") @NotEmpty String roleName) throws Exception {
		portalService.validateRequestResponse(request, roleName, null);
		return CommonHelperFunctions.buildResponseEntity(portalService.getProductConfig(roleName));
	}

	@ApiOperation(value = SwaggerConstants.PC_GET_TAB_CONFIG_DESC)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_TAB_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = "/{caseInstanceId}/get-tab-config")
	public ResponseEntity<Object> getTabConfiguration(@ApiParam(value = "Case Instance id" , required = true,
			example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @PathVariable(name = "caseInstanceId") String caseInstanceId,
			@ApiParam(value = "Assignee" , required = true , example = "cmmnuser@kuliza.com") @RequestParam(required = true, value = "assignee") String assignee,
													  @ApiParam(value = "Assigned RoleName" , required = true , example = "cmmnCreditAnalystPOC") @RequestParam(required = true, value = "assignedRoleName") String roleName) throws Exception {
		return CommonHelperFunctions.buildResponseEntity(portalService.getTabConfig(assignee, caseInstanceId, roleName));
	}

	@ApiOperation(value = SwaggerConstants.PC_GET_CARD_CONFIG_DESC)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_CARD_CONFIG, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.GET, value = "/{roleName}/{bucket}/{caseInstanceId}/{tabKey}/get-card-config")
  public ResponseEntity<Object> getCardConfiguration(HttpServletRequest request, @ApiParam(value = SwaggerConstants.PC_ROLENAME_GET_PROD_CONFIG, required = true
			, example = "cmmnCreditAnalystPOCAdmin") @PathVariable(name = "roleName", required = true) String roleName, @ApiParam(value = SwaggerConstants.PC_GET_BUCKET, required = true
			, example = "pending") @PathVariable(name = "bucket", required = true) String bucket, @ApiParam(value = "Tab Key" , required = true,example = "personalDetailScreen") @PathVariable(name = "tabKey") String tabKey, @ApiParam(value = "Case Instance id" , required = true,
		  example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @PathVariable(name = "caseInstanceId") String caseInstanceId,
		@ApiParam(value = "Map", required = true)
		@RequestParam Map<String, String> map) throws Exception{
    return CommonHelperFunctions
        .buildResponseEntity(portalService.getCardConfig(request, roleName, bucket, tabKey, caseInstanceId, map));
  }

  @ApiOperation(value = SwaggerConstants.PC_SET_COMMENT + " and with tab key")
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_SET_COMMENT_RESPONSE, code = 200)})
  @RequestMapping(method = RequestMethod.POST, value = "/{roleName}/{caseInstanceId}/{tabKey}/comment")
  public ResponseEntity<Object> setComment(HttpServletRequest request, Principal principal, @ApiParam(value = "Role Name" , example = "creditAnalystAdmin") @PathVariable(name = "roleName", required = false) String roleName, @ApiParam(value = "Tab Key" , example = "personalDetailScreen") @PathVariable(name = "tabKey", required = false) String tabKey, @ApiParam(value = "Case Instance id - Sets Comment" , required = true,
		  example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @PathVariable(name = "caseInstanceId") String caseInstanceId, @Valid @RequestBody
      PortalCommentHistory portalCommentHistory) throws Exception{
    return CommonHelperFunctions
        .buildResponseEntity(portalService.setComment(request, roleName, principal.getName(), tabKey, caseInstanceId, portalCommentHistory));
  }

  @ApiOperation(value = SwaggerConstants.PC_SET_COMMENT + " and without tab key")
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_SET_COMMENT_RESPONSE_NULL_TAB_KEY, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.POST, value = "/{caseInstanceId}/comment")
  public ResponseEntity<Object> setTabComment(HttpServletRequest request, Principal principal, @ApiParam(value = "Case Instance id" , required = true,
		  example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @PathVariable(name = "caseInstanceId") String caseInstanceId, @Valid @RequestBody
      PortalCommentHistory portalCommentHistory) throws Exception{
    return CommonHelperFunctions
        .buildResponseEntity(portalService.setComment(request,null, principal.getName(), null, caseInstanceId, portalCommentHistory));
  }

  @ApiOperation(value= SwaggerConstants.PC_GET_COMMENT + " and tab key")
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_COMMENT_RESPONSE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.GET, value = "/{caseInstanceId}/{tabKey}/comment")
  public ResponseEntity<Object> getTabComment(@ApiParam(value = "Tab Key - Fetch Comments" ,example = "personalDetailScreen")@PathVariable(name = "tabKey", required = false) String tabKey,@ApiParam(value = "Case Instance id-Fetch Comments" , required = true,
		  example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @PathVariable(name = "caseInstanceId") String caseInstanceId) throws Exception{
    return CommonHelperFunctions
        .buildResponseEntity(portalService.getComment(tabKey, caseInstanceId));
  }

  @ApiOperation(value = SwaggerConstants.PC_GET_COMMENT)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_COMMENT_RESPONSE_NULL_TAB_KEY, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.GET, value = "/{caseInstanceId}/comment")
  public ResponseEntity<Object> getComment(@ApiParam(value = "Case Instance - Fetch Comments" , required = true,
		  example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @PathVariable(name = "caseInstanceId") String caseInstanceId) throws Exception{
    return CommonHelperFunctions
        .buildResponseEntity(portalService.getComment(null, caseInstanceId));
  }

	@ApiOperation(value = SwaggerConstants.PC_CREATE_CUSTOM_ACTION)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_CREATE_CUSTOM_RESPONSE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.POST , value = "/create-custom-url-mapping")
	public ResponseEntity<Object> saveCustomUrlMapper(Principal principal, @Valid @RequestBody List<CustomURLMapper> customUrlMapperList) throws Exception{
		return CommonHelperFunctions.buildResponseEntity(portalService.saveCustomUrlMapper(customUrlMapperList, principal.getName()));
  }

  @ApiOperation(value = SwaggerConstants.PC_DELETE_CUSTOM_ACTION)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_DELETE_CUSTOM_RESPONSE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.DELETE, value = "/delete-custom-url-mapping")
	public ResponseEntity<Object> deleteCustomUrlMapper(@ApiParam(value = "slug" , required = true, example = "triggerCibil") @NotNull @RequestParam String slug){
		return CommonHelperFunctions.buildResponseEntity(portalService.deleteCustomUrl(slug));
  }

  @ApiOperation(value = SwaggerConstants.PC_DELETE_ALL_CUSTOM_ACTION)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.SUCCESS_RESPONSE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.DELETE, value = "/delete-all-custom-url-mapping")
	public ResponseEntity<Object> deleteAllCustomURL(){
		return CommonHelperFunctions.buildResponseEntity(portalService.deleteAllCustomURL());
  }

  @ApiOperation(value = SwaggerConstants.PC_UPDATE_CUSTOM_ACTION)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_UPDATE_CUSTOM_RESPONSE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.PUT, value = "/update-custom-url-mapping")
	public ResponseEntity<Object> updateCustomURLMapper(@ApiParam(value = "Slug" , required = true, example = "triggerCibil")@NotNull @RequestParam String slug,
														@NotNull @Valid @RequestBody CustomURLMapper customUrlMapper){
		return CommonHelperFunctions.buildResponseEntity(portalService.updateCustomURLMapper(slug, customUrlMapper));
  }

  @ApiOperation(value = SwaggerConstants.PC_GET_CUSTOM_ACTION)
  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_GET_CUSTOM_RESPOSNE, code = 200)})
  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.GET, value = "/custom-url-mapping")
	public ResponseEntity<Object> getCustomURL(@ApiParam(value = "slug" , required = true, example = "triggerCibil") @NotNull @RequestParam String slug){
		return CommonHelperFunctions.buildResponseEntity(portalService.getCustomURL(slug));
  }

//  @ApiOperation(value = SwaggerConstants.PC_FIRE_CUSTOM_ACTION)
//  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_FIRE_CUSTOM_RESPONSE, code = 200)})
//  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
//		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
//  @RequestMapping(method = RequestMethod.POST, value = PortalConstants.CUSTOM_ACTION_URL + "{slug}")
//	public ResponseEntity<Object> fireCustomAction(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerCibil") @PathVariable(value = "slug") String slug,
//											   @ApiParam(value="Role name",required = true,
//													   example = "cmmnCreditAnalystPOCAdmin") @NotNull @RequestParam String roleName,
//											   @ApiParam(value = "Case Instance id" , required = true,
//													   example = "5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @NotNull @RequestParam String caseInstanceId,
//											   @ApiParam(value = "Tab Key" , required = true,
//													   example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
//										 @Valid @RequestBody CustomActionURLBody customActionUrlBody,
//											@ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
//										 @RequestHeader(value = "Authorization") String token) throws Exception{
//	  return CommonHelperFunctions.buildResponseEntity(portalService.fireCustomAction(request, slug, customActionUrlBody, contentType,
//			  token, caseInstanceId, roleName, principal.getName()));
//  }

//  @ApiOperation(value = SwaggerConstants.PC_FIRE_BULK_ACTION)
//  @ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_FIRE_BULK_ACTION_RESPONSE, code = 200)})
//  @ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
//		  required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
//  @RequestMapping(method = RequestMethod.POST, value = PortalConstants.BULK_ACTION_URL +"{slug}")
//	public ResponseEntity<Object> initiateBulkUrl(HttpServletRequest request, Principal principal, @ApiParam(value = "Slug" , required = true, example = "triggerId") @PathVariable(value = "slug") String slug,
//												  @ApiParam(value="Role name",required = true,
//														  example = "cmmnCreditAnalystPOCAdmin") @NotNull @RequestParam String roleName,
//												  @ApiParam(value = "Tab Key" , required = true,
//														  example = "personalDetailScreen") @NotNull @RequestParam String tabKey,
//												  @Valid @RequestBody List<CustomBulkActionURLBody> customBulkActionURLBodyList,
//												  @ApiParam(example = "Application/json") @RequestHeader(value = "Content-Type") String contentType,
//												  @RequestHeader(value = "Authorization") String token){
//		return CommonHelperFunctions.buildResponseEntity(portalService.fireBulkCustomActions(request, slug, customBulkActionURLBodyList,
//				contentType, token, roleName, principal.getName()));
//  }

	@ApiOperation(value = SwaggerConstants.PC_CLAIM)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.SUCCESS_RESPONSE , code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = PortalConstants.CLAIM_API_URL)
	public ResponseEntity<Object> claimCase(Principal principal, @Valid @RequestBody ClaimUnclaimInput claimUnclaimInput){
		return CommonHelperFunctions.buildResponseEntity(portalService.claimCase(claimUnclaimInput, principal.getName()));
	}

	@ApiOperation(value = SwaggerConstants.PC_UNCLAIM)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.SUCCESS_RESPONSE , code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = PortalConstants.UNCLAIM_API_URL)
	public ResponseEntity<Object> unclaimCase(Principal principal, @Valid @RequestBody ClaimUnclaimInput claimUnclaimInput){
		return CommonHelperFunctions.buildResponseEntity(portalService.unclaimCase(claimUnclaimInput));
	}

	@ApiOperation(value = SwaggerConstants.PC_REASSIGN)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.SUCCESS_RESPONSE, code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = PortalConstants.REASSIGN_API_URL)
	public ResponseEntity<Object> reassignCase(Principal principal, @Valid @RequestBody ClaimUnclaimInput claimUnclaimInput){
		return CommonHelperFunctions.buildResponseEntity(portalService.reAssignCase(claimUnclaimInput, principal.getName()));
	}

	@ApiOperation(value = SwaggerConstants.PC_SUBMIT_PARENT_CASE_VARIABLES)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_SUBMIT_CASE_VARIABLES_RESPONSE,
			code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = "/submit-parentcaseinstance-variables")
	public ResponseEntity<Object> submitParentCaseInstanceVariables(Principal principal,
		@ApiParam(value="Case Instance Id", required=true, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa")
		@RequestParam(required=true, value="caseInstanceId") String caseInstanceId,
		@Valid @NotNull @NotEmpty @RequestBody Map<String, Object> caseVariables){
		return CommonHelperFunctions.buildResponseEntity(portalService.submitParentCaseInstanceVariables(principal.getName(), caseInstanceId, caseVariables));
	}

	@ApiOperation(value = SwaggerConstants.PC_SUBMIT_CHILD_CASE_VARIABLES)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_SUBMIT_CASE_VARIABLES_RESPONSE,
			code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.POST, value = "/submit-childcaseinstance-variables")
	public ResponseEntity<Object> submitChildCaseInstanceVariables(Principal principal,
		@ApiParam(value="Case Instance Id", required=true, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa")
		@RequestParam(required=true, value="caseInstanceId") String caseInstanceId,
		@Valid @NotNull @NotEmpty @RequestBody Map<String, Object> caseVariables) {
		return CommonHelperFunctions.buildResponseEntity(portalService
				.submitChildCaseInstanceVariables(principal.getName(), caseInstanceId, caseVariables));
	}

	@ApiOperation(value = SwaggerConstants.PC_ALLOCATION_LOGIC)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_ALLOCATION_LOGIC_RESPONSE,
			code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
  @RequestMapping(method = RequestMethod.GET, value = "/allocation")
	public String allocation(@RequestParam @ApiParam(value = "Case ID") String caseId,
			@RequestParam @ApiParam(value = "Portal Type") String portalType,
			@RequestParam @ApiParam(value = "Allocation Type") String allocationType,
			@RequestParam @ApiParam(value = "Group Names") String groupNames,
			@RequestParam @ApiParam(value = "Roles") String roles
			) throws IOException, InterruptedException, Exception {
		return allocationServiceTask.allocationLogic(caseId, portalType, allocationType, groupNames, roles);
	}


	@ApiOperation(value = SwaggerConstants.EXPORT_CUSTOM_ACTION_CONFIGURATION)
	@ApiResponses(value = {@ApiResponse(message = SwaggerConstants.PC_ALLOCATION_LOGIC_RESPONSE,
			code = 200)})
	@ApiImplicitParam(name = Constants.AUTH, value = Constants.AUTH_TOKEN,
			required = true, dataType = "string", paramType = "header", example = Constants.BEARER_TOKEN)
	@RequestMapping(method = RequestMethod.GET, value = PortalConstants.EXPORT_CUSTOM_ACTION_API_URL)
	public ResponseEntity<Object> exportCustomActionConfiguration(@ApiParam(value="Custom Action Method", required=false, example="triggerCibil")
	@RequestParam(required=false, value="slug") String slug) {
		return CommonHelperFunctions
				.buildResponseEntity(portalService.exportCustomActionConfiguration(slug));
	}

	@ApiImplicitParam(name= Constants.AUTH, value= Constants.AUTH_TOKEN,
			required=true, dataType="string", paramType="header", example= Constants.BEARER_TOKEN)
	@ApiOperation(value= SwaggerConstants.IMPORT_CUSTOM_ACTION_CONFIGURATION)
	@ApiResponses(value={@ApiResponse(message= EngineCommonConstants.ECC_UPLOAD_FILE_RESPONSE, code=200)})
	@RequestMapping(method = RequestMethod.POST, value = PortalConstants.IMPORT_CUSTOM_ACTION_API_URL, consumes = "multipart/form-data")
	public ResponseEntity<Object> importCustomActionConfiguration(@RequestParam("file") MultipartFile file
											 ) throws IOException{
		return CommonHelperFunctions.buildResponseEntity(portalService.importCustomActionConfiguration(file));
	}


}
