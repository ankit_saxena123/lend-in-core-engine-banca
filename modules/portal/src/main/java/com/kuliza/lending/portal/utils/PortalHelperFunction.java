package com.kuliza.lending.portal.utils;

import com.kuliza.lending.common.pojo.ExtendedApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.models.CustomURLMapper;
import com.kuliza.lending.pojo.GroupDetailWithMembers;
import com.kuliza.lending.portal.PortalURLConfiguratorComponent;
import com.kuliza.lending.portal.models.PortalCommentHistory;
import com.kuliza.lending.portal.pojo.ActionConfiguration;
import com.kuliza.lending.portal.pojo.BucketConfiguration;
import com.kuliza.lending.portal.pojo.BucketvariableConfiguration;
import com.kuliza.lending.portal.pojo.CustomActionHeaders;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.flowable.form.model.FormField;
import org.flowable.form.model.Option;
import org.flowable.form.model.OptionFormField;
import org.flowable.task.api.Task;
import org.json.JSONArray;
import org.json.JSONObject;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.*;

import static com.kuliza.lending.portal.utils.PortalConstants.*;

public class PortalHelperFunction extends CommonHelperFunctions {

  private static final Logger LOGGER = LoggerFactory.getLogger(PortalHelperFunction.class);

  public static String replaceServiceNameWithUrl(String url) {
		/*// Integration Broker URL Replacement
		url = url.replace(
				StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
						+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
								.getIntegrationBrokerServiceName()
						+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getEndServiceNameSymbol(),
				StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMockIntegrationBrokerIpAddress() + ":"
						+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMockIntegrationBrokerPort());*/

    // DMS URL Replacement
    url = url
        .replace(
            StaticContextAccessor.getBean(PortalURLConfiguratorComponent.class)
                .getStartServiceNameSymbol()
                + StaticContextAccessor.getBean(PortalURLConfiguratorComponent.class)
                .getDMSServiceName()
                + StaticContextAccessor.getBean(PortalURLConfiguratorComponent.class)
                .getEndServiceNameSymbol(),

            StaticContextAccessor.getBean(PortalURLConfiguratorComponent.class).getDmsIpAddress() + ":"
                + StaticContextAccessor.getBean(PortalURLConfiguratorComponent.class).getDmsPort());

		/*// Masters URL Replacement
		url = url
				.replace(
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMastersServiceName()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
										.getEndServiceNameSymbol(),
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMastersIpAddress() + ":"
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMastersPort());*/

    // Backend URL Replacement
		/*url = url
				.replace(
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getBackendServiceName()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
										.getEndServiceNameSymbol(),
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getBackendIpAddress() + ":"
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getBackendPort());*/
    return url;
  }

  public static Map<String, Object> getPresentScreenInfo(Map<String, FormField> formFields)
      throws Exception {
    Map<String, Object> formData = new HashMap<>();
    for (Map.Entry<String, FormField> entry : formFields.entrySet()) {
      FormField property = entry.getValue();
      Map<String, Object> tempMap = new HashMap<>();
      LOGGER.info("=====  LOGGER  ==== getPresentScreenInfo   ======== entry.getKey() : " + entry.getKey() +
          ", === Name : " + entry.getValue().getName() +
          ", === Type : " + entry.getValue().getType() +
          ", === ID : " + entry.getValue().getId() +
          ", === Value : " + entry.getValue().getValue());
      if (property.getType().equals("dropdown") || property.getType().equals("radio-buttons")) {
        OptionFormField property2 = (OptionFormField) property;
        List<Option> options = property2.getOptions();
        if (options.get(0).getId() == null) {
          options.remove(0);
        }
        tempMap.put("enumValues", options);
      } else {
        tempMap.put("enumValues", null);
      }
      tempMap.put("name", property.getName());
      tempMap.put("id", property.getId());
      tempMap.put("isWritable", !property.isReadOnly());
      tempMap.put("isRequired", property.isRequired());
      tempMap.put("value", property.getValue());
      tempMap.put("type", property.getType());
      Map<String, Object> params = property.getParams();
      Map<String, Object> metaData = (Map<String, Object>) params
          .getOrDefault("meta", new HashMap<>());
      if ((CommonHelperFunctions.getStringValue(metaData.get("type")).equals("rest")
          || CommonHelperFunctions.getStringValue(metaData.get("form_type"))
          .equals("rest-dropdown")
          || CommonHelperFunctions.getStringValue(metaData.get("form_type")).equals("upload")
          || CommonHelperFunctions.getStringValue(metaData.get("type")).equals("upload"))
          && metaData.get("api") instanceof Map) {
        Map<String, Object> apiData = (Map<String, Object>) metaData
            .getOrDefault("api", new HashMap<>());
        apiData.put("url",
            replaceServiceNameWithUrl(CommonHelperFunctions.getStringValue(apiData.get("url"))));
        metaData.put("api", apiData);
      } else if ((CommonHelperFunctions.getStringValue(metaData.get("type")).equals("rest")
          || CommonHelperFunctions.getStringValue(metaData.get("form_type"))
          .equals("rest-dropdown")
          || CommonHelperFunctions.getStringValue(metaData.get("form_type")).equals("upload")
          || CommonHelperFunctions.getStringValue(metaData.get("type")).equals("upload"))
          && metaData.get("api") instanceof String
          && metaData.get("api").toString().length() > 0) {
        JSONArray apisData = new JSONArray(metaData.get("api").toString());
        JSONArray newApisData = new JSONArray();
        for (int i = 0; i < apisData.length(); i++) {
          JSONObject apiData = apisData.getJSONObject(i);
          apiData.put("url",
              replaceServiceNameWithUrl(
                  CommonHelperFunctions.getStringValue(apiData.get("url"))));
          newApisData.put(apiData);
        }
        metaData.put("api", newApisData.toString());
      }
      params.put("meta", metaData);
      tempMap.put("params", params);
      formData.put(property.getId(), tempMap);
    }
    return formData;
  }

  public static ExtendedApiResponse makeResponse(HttpStatus status, String message,
      String caseInstanceId,
      Task task, Map<String, Object> data, Map<String, FormField> formFields) throws Exception {
    String taskDefinitionKey = task.getTaskDefinitionKey();
    String taskId = task.getId();
    Map<String, Object> formData = getPresentScreenInfo(formFields);
    Map<String, Object> responseData = new HashMap<>();
    responseData.put("taskId", taskId);
    responseData.put("caseInstanceId", caseInstanceId);
    responseData.put("data", data);
    responseData.put(taskDefinitionKey, formData);
    responseData.put("stepperData", task.getDescription());
    return new ExtendedApiResponse(status, message, responseData, taskDefinitionKey,
        task.getName(), "");
  }

  public static ExtendedApiResponse makePortalResponse(HttpStatus status, String message,
      Task task, Map<String, Object> data, Map<String, FormField> formFields, Map<String, FormField> globalFormFields, List<PortalCommentHistory> commentData) throws Exception {
    String taskDefinitionKey = task.getTaskDefinitionKey();
    String taskId = task.getId();
    Map<String, Object> formData = new HashMap<>();
    formData.put("assignedList", data.getOrDefault("assignedList", null));
    formData.put("bucketConfiguration", data.getOrDefault("bucketConfiguration", null));
    if(!taskDefinitionKey.contains(PortalConstants.PORTFOLIO_SUFFIX)) {
      formData = getPresentScreenInfo(formFields);
    }
    Map<String, Object> globalFormData = getPresentScreenInfo(globalFormFields);
    Map<String, Object> responseData = new HashMap<>();
    responseData.put("taskId", taskId);
    responseData.put("caseInstanceId", data.get("caseInstanceId"));
    responseData.put(taskDefinitionKey, formData);
    responseData.put("globalVariables", globalFormData);
    responseData.put("comment", commentData);
    responseData.put("commentConfiguration", data.get("commentConfiguration"));
    responseData.put("stepperData", task.getDescription());
    responseData.put(PORTAL_VISTED_TAB, data.get(PORTAL_VISTED_TAB));
    return new ExtendedApiResponse(status, message, responseData, taskDefinitionKey,
        task.getName(), "");
  }

  public static ArrayList<BucketConfiguration> getBucketConfigList(Object bucketConfigStringListObject) {
    ArrayList<BucketConfiguration> response = new ArrayList<>();
    if (bucketConfigStringListObject != null) {
      JSONArray bucketConfigStringList = new JSONArray(CommonHelperFunctions.getStringValue(bucketConfigStringListObject));
      for (int i=0; i < bucketConfigStringList.length(); i++) {
        if (bucketConfigStringList.get(i) != null) {
          BucketConfiguration bucketConfiguration = new BucketConfiguration();
          Map<String, Object> bucketConfigurationMap = CommonHelperFunctions.fromJson(bucketConfigStringList.getJSONObject(i));
          bucketConfiguration.setBucketActions(getActionConfigurationList(
              bucketConfigurationMap.getOrDefault("bucketActions", null)));
          bucketConfiguration.setVariables(getBucketVariableConfiguration(
              bucketConfigurationMap.getOrDefault("variables", null)));
          bucketConfiguration.setLabel(CommonHelperFunctions
              .getStringValue(bucketConfigurationMap.getOrDefault("label", null)));
          bucketConfiguration.setId(CommonHelperFunctions
              .getStringValue(bucketConfigurationMap.getOrDefault("id", null)));
          bucketConfiguration.setEditable(CommonHelperFunctions
              .getBooleanValue(bucketConfigurationMap.getOrDefault("editable", null)));
          bucketConfiguration.setComment(CommonHelperFunctions
              .getIntegerValue(bucketConfigurationMap.getOrDefault("comment", null)));
					bucketConfiguration.setRedirectToDetails(CommonHelperFunctions
							.getBooleanValue(bucketConfigurationMap.getOrDefault("redirectToDetails", true)));
          response.add(bucketConfiguration);
        }
      }
    }
    return response;
  }

  public static ArrayList<ActionConfiguration> getActionConfigurationListFromString(Object actionConfigurationListString) {
    ArrayList<ActionConfiguration> response = new ArrayList<>();
    if (actionConfigurationListString != null) {
      JSONArray actionConfigStringList = new JSONArray(CommonHelperFunctions.getStringValue(actionConfigurationListString));
      for (int i=0; i < actionConfigStringList.length(); i++) {
        if (actionConfigStringList.get(i) != null) {
          ActionConfiguration actionConfiguration = new ActionConfiguration();
          Map<String, Object> actionConfigurationMap = CommonHelperFunctions.fromJson(actionConfigStringList.getJSONObject(i));
          actionConfiguration.setId(
              CommonHelperFunctions.getStringValue(actionConfigurationMap.getOrDefault("id", null)));
          actionConfiguration.setLabel(
              CommonHelperFunctions.getStringValue(actionConfigurationMap.getOrDefault("label", null)));
          actionConfiguration
              .setParams((Map<String, Object>) actionConfigurationMap.getOrDefault("params", null));
          actionConfiguration.setIconKey(
              CommonHelperFunctions.getStringValue(actionConfigurationMap.getOrDefault("iconKey", null)));
          actionConfiguration.setValue(
              CommonHelperFunctions.getBooleanValue(actionConfigurationMap.getOrDefault("value", null)));
          response.add(actionConfiguration);
        }
      }
    }
    return response;
  }

  public static ArrayList<CustomActionHeaders> getCustomActionHeadersList(Object customActionHeadersListObject) {
    ArrayList<CustomActionHeaders> response = new ArrayList<>();
    if (customActionHeadersListObject != null) {
      JSONArray customActionHeaderStringList = null;
      if (customActionHeadersListObject instanceof ArrayList) {
        customActionHeaderStringList = new JSONArray((List<Map<String, Object>>) customActionHeadersListObject);
      } else {
        customActionHeaderStringList = new JSONArray(CommonHelperFunctions.getStringValue(customActionHeadersListObject));
      }
      for (int i=0; i < customActionHeaderStringList.length(); i++) {
        if (customActionHeaderStringList.get(i) != null) {
          CustomActionHeaders customActionHeadersConfiguration = new CustomActionHeaders();
          Map<String, Object> customActionHeadersConfigurationMap = CommonHelperFunctions.fromJson(customActionHeaderStringList.getJSONObject(i));
          customActionHeadersConfiguration.setKey(CommonHelperFunctions
                  .getStringValue(customActionHeadersConfigurationMap.getOrDefault("key", null)));
          customActionHeadersConfiguration.setValue(CommonHelperFunctions
                  .getStringValue(customActionHeadersConfigurationMap.getOrDefault("value", null)));
          response.add(customActionHeadersConfiguration);
        }
      }
    }
    return response;
  }


	public static ArrayList<BucketvariableConfiguration> getBucketVariableConfiguration(
			Object bucketVariableConfigList) {
		ArrayList<BucketvariableConfiguration> response = new ArrayList<>();
		if (bucketVariableConfigList != null) {
			List<Map<String, Object>> bucketVariableMapList = (List<Map<String, Object>>) bucketVariableConfigList;
			for (Map<String, Object> bucketVariableMap : bucketVariableMapList) {
				if (bucketVariableMap != null) {
					BucketvariableConfiguration bucketvariableConfiguration = new BucketvariableConfiguration();
					bucketvariableConfiguration
							.setId(CommonHelperFunctions.getStringValue(bucketVariableMap.getOrDefault("id", null)));
					bucketvariableConfiguration.setLabel(
							CommonHelperFunctions.getStringValue(bucketVariableMap.getOrDefault("label", null)));
					bucketvariableConfiguration.setType(
							CommonHelperFunctions.getStringValue(bucketVariableMap.getOrDefault("type", null)));
					bucketvariableConfiguration.setActionConfigurationList(getActionConfigurationList(
							bucketVariableMap.getOrDefault("actionConfigurationList", null)));
					response.add(bucketvariableConfiguration);
				}
			}
		}
		return response;
	}

	public static ArrayList<ActionConfiguration> getActionConfigurationList(Object actionConfigurationList) {
		ArrayList<ActionConfiguration> response = new ArrayList<>();
		if (actionConfigurationList != null) {
			List<Map<String, Object>> actionConfigurationMapList = (List<Map<String, Object>>) actionConfigurationList;
			for (Map<String, Object> actionConfigurationMap : actionConfigurationMapList) {
				if (actionConfigurationMap != null) {
					ActionConfiguration actionConfiguration = new ActionConfiguration();
					actionConfiguration.setId(
							CommonHelperFunctions.getStringValue(actionConfigurationMap.getOrDefault("id", null)));
					actionConfiguration.setLabel(
							CommonHelperFunctions.getStringValue(actionConfigurationMap.getOrDefault("label", null)));
					actionConfiguration
							.setParams((Map<String, Object>) actionConfigurationMap.getOrDefault("params", null));
					actionConfiguration.setIconKey(
							CommonHelperFunctions.getStringValue(actionConfigurationMap.getOrDefault("iconKey", null)));
					actionConfiguration.setValue(
							CommonHelperFunctions.getBooleanValue(actionConfigurationMap.getOrDefault("value", null)));
					response.add(actionConfiguration);
				}
			}
		}
		return response;
	}

  public static Map<String, Object> getRoleMembersList(List<GroupDetailWithMembers> groupDetailWithMembersList, String groupId, String emailId) {
    Map<String, Object> response = new HashMap<>();
    for (GroupDetailWithMembers groupDetailWithMembers: groupDetailWithMembersList){
      Set<String> memberList = new HashSet<>();
      for (UserRepresentation userRepresentation: groupDetailWithMembers.getMemberList()){
        if (userRepresentation.getEmail() != null && !userRepresentation.getEmail().isEmpty()){
          if (!groupDetailWithMembers.getId().equals(groupId) || userRepresentation.getEmail().equals(emailId))
            memberList.add(userRepresentation.getEmail());
        }
      }
      if (!memberList.isEmpty()){
        memberList.addAll((Set<String>) response.getOrDefault(groupDetailWithMembers.getRealmRoles().get(0), new HashSet<>()));
        memberList.add(emailId);
        response.put(groupDetailWithMembers.getRealmRoles().get(0), memberList);
      }
    }
    return response;
  }

  public static String setPathVariables(String url, Map<String, String> pathVariables){
    String pathVariablesUrl = url;
    if(pathVariables != null && !pathVariables.isEmpty() && pathVariables.size() > 0){
      for(String key : pathVariables.keySet()){
        pathVariablesUrl = pathVariablesUrl.replace("/"+key+"/",
                "/" + pathVariables.get(key) + "/");
        if((pathVariablesUrl.indexOf("/"+key) + ("/"+key).length() ) == pathVariablesUrl.length()){
          pathVariablesUrl = pathVariablesUrl.replace("/"+key, "/" + pathVariables.get(key));
        }
      }
    }
    return pathVariablesUrl;
  }

  public static String setRequestParams(String url, Map<String, String> requestParams){
    String requestParamUrl = url;
    if(requestParams != null && !requestParams.isEmpty() && requestParams.size() > 0){
      requestParamUrl = requestParamUrl + "?";
      int mapSize = requestParams.size(), count = 1;
      for(String key: requestParams.keySet()) {
        requestParamUrl = requestParamUrl + key + "=" + requestParams.get(key);
        if(count < mapSize)
        {
          requestParamUrl = requestParamUrl + "&";
        }
        count++;
      }
    }
    return requestParamUrl;
  }

  public static Map<String, Object> getVisitedTab(Map<String, Object> processVariables, String roleName, String assignee, String tabKey){
    Map<String, Object> response = new HashMap<>();
    Map<String, Object> visitedTabsMap = (Map<String, Object>) processVariables.getOrDefault(PORTAL_VISTED_TAB, new HashMap<>());
    Set<String> oldVistedTabs = new HashSet<>();
    try{
      oldVistedTabs = (Set<String>) visitedTabsMap.getOrDefault(roleName +"-"+ assignee, new HashSet<>());
    } catch (Exception e) {
     oldVistedTabs = new HashSet<String>((List<String>) visitedTabsMap
          .getOrDefault(roleName + "-" + assignee, new HashSet<>()));
    }
    Set<String> newVistedTabs = new HashSet<>();
    newVistedTabs.addAll(oldVistedTabs);
    newVistedTabs.add(tabKey);
    visitedTabsMap.put(roleName +"-"+ assignee, newVistedTabs);
    response.put("oldVistedTabs", oldVistedTabs);
    response.put("newVistedTabs", newVistedTabs);
    response.put("newVistedTabsMap", visitedTabsMap);
    return response;
  }

  public static ArrayList<CustomURLMapper> getCustomURLMapperList(Object customURLMapperList) {
    ArrayList<CustomURLMapper> response = new ArrayList<>();
    if (customURLMapperList != null) {
      List<Map<String, Object>> customURLMapperMapList = (List<Map<String, Object>>) customURLMapperList;
      for (Map<String, Object> customURLMapperMap : customURLMapperMapList) {
        if (customURLMapperMap != null) {
          CustomURLMapper customURLMapper = new CustomURLMapper();
          customURLMapper.setSlug(
              CommonHelperFunctions.getStringValue(customURLMapperMap.getOrDefault("slug", null)));
          customURLMapper.setUserId(
              CommonHelperFunctions.getStringValue(customURLMapperMap.getOrDefault("userId", null)));
          customURLMapper.setOpenApi(
              CommonHelperFunctions.getBooleanValue(customURLMapperMap.getOrDefault("openApi", null)));
          customURLMapper.setUrl(
              CommonHelperFunctions.getStringValue(customURLMapperMap.getOrDefault("url", null)));
          customURLMapper.setPortalType(
              CommonHelperFunctions.getStringValue(customURLMapperMap.getOrDefault("portalType", null)));
//          customURLMapper.setHeaders(getCustomURLMapperList(customURLMapperMap.getOrDefault("headers", null)));
          customURLMapper.setExternalApi(
              CommonHelperFunctions.getBooleanValue(customURLMapperMap.getOrDefault("externalApi", null)));
//          customURLMapper.setRequestType(
//              REQUEST_TYPE.valueOf(CommonHelperFunctions.getStringValue(customURLMapperMap.getOrDefault("requestType", null))));
          response.add(customURLMapper);
        }
      }
    }
    return response;
  }

  public static String portalFetchingCaseQuery(Map<String, Set<String>> roleMembersMap, Map<String, String> filters, String productType, String bucketKey, List<String> bucketVariables, String databaseType) {
    String queryFilterJoin = "";
    String queryFilterWhereClause = "";
    String queryFilterBaseQuery = "";
    if (filters.keySet().size() > 2) {
      String queryFilterKey = getFilterKey(filters);
      queryFilterJoin = portalFetchQueryMap(databaseType).get(FILTER_JOIN_SYNTAX);
      queryFilterWhereClause = portalFetchQueryMap(databaseType).get(FILTER_WHERE_CLAUSE);
      queryFilterBaseQuery = portalFetchQueryMap(databaseType).get(ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY);
      if (queryFilterKey.equals(GLOBAL_SEARCH_KEY)) {
        queryFilterWhereClause = portalFetchQueryMap(databaseType).get(GLOBAL_FILTER_WHERE_CLAUSE);
        queryFilterBaseQuery = portalFetchQueryMap(databaseType).get(GLOBAL_ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY);
      }
      queryFilterWhereClause = queryFilterWhereClause.replace("queryFilterKey", queryFilterKey);
      queryFilterBaseQuery = queryFilterBaseQuery.replace("queryFilterKey", queryFilterKey);
      queryFilterWhereClause = queryFilterWhereClause.replace("queryFilterValue", filters.get(queryFilterKey));
      queryFilterBaseQuery = queryFilterBaseQuery.replace("queryFilterValue", filters.get(queryFilterKey));
    }

    String portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
    if (productType.equals(ALL_PRODUCT_TYPE) && bucketKey.equals(ALL_BUCKET_KEY)) {
      portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(ALL_PRODUCT_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
    } else if (productType.equals(ALL_PRODUCT_TYPE)) {
      portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(ALL_PRODUCT_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
    } else if (bucketKey.equals(ALL_BUCKET_KEY)) {
      portalFetchingCaseQuery = portalFetchQueryMap(databaseType).get(ALL_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY);
    }
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryBucketVariables", "'".concat(String.join("','", bucketVariables)).concat("'"));
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryBucketKey", bucketKey);
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryProductType", productType);
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryFilterWhereClause", queryFilterWhereClause);
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryFilterJoin", queryFilterJoin);
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryFilterBaseQuery", queryFilterBaseQuery);
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryRoleNameStatusList", "'".concat(String.join("Status','", roleMembersMap.keySet())).concat("Status'"));
    portalFetchingCaseQuery = portalFetchingCaseQuery.replace("queryTaskNameAssigneeList", getTaskNameAssigneeListQuery(roleMembersMap, bucketKey, databaseType));

//    portalFetchingCaseQuery = "select  concat('{\"caseInstanceId\":\"',act_ru_task.scope_id_,'\"}') as caseInstanceId, concat('{',string_agg(concat('\"',TRANSLATE('created',  '\"', '\"'),'\":\"', TRANSLATE(cast(act_ru_task.create_time_ as text), '\"', '\"'),'\"'), ','),'}') as asdf, concat('{',string_agg(concat('\"',TRANSLATE(act_hi_varinst.name_,  '\"', '\"'),'\":\"', TRANSLATE(act_hi_varinst.text_, '\"', '\"'),'\"'), ','),'}') as variables from act_ru_task, act_hi_varinst where act_ru_task.scope_type_ = 'cmmn' and (act_ru_task.task_def_key_ in ('RcuHunterSampler-claim') ) and act_ru_task.scope_id_ in (select scope_id_ from act_hi_varinst where act_hi_varinst.name_ in ('RcuHunterSamplerStatus') and act_hi_varinst.text_ = 'claim')  and act_hi_varinst.name_ in ('firstname','applicantMobileNumber','claimbulk','claim') and act_ru_task.scope_id_ = act_hi_varinst.scope_id_ group by act_ru_task.scope_id_ order by  max(act_ru_task.create_time_) desc;";
    LOGGER.info("portalFetchingCaseQuery ::::::::::: " + portalFetchingCaseQuery);
    return portalFetchingCaseQuery;
  }

  public static String getTaskNameAssigneeListQuery(Map<String, Set<String>> roleMembersMap, String bucketKey, String databaseType) {
    String taskNameAssigneeListCompleteQuery = "";
    List<String> taskAssigneeQueryList = new ArrayList<>();
    String taskNameSuffix = LIST_VIEW_KEY;
    if (bucketKey.equals(CLAIM_BUCKET_KEY)) {
      taskNameSuffix = CLAIM_VIEW_KEY;
    } else if (bucketKey.equals(HISTORY_BUCKET_KEY)) {
      taskNameSuffix = HISTORY_VIEW_KEY;
    }
    if (bucketKey.equals(CLAIM_BUCKET_KEY)) {
      taskNameAssigneeListCompleteQuery = portalFetchQueryMap(databaseType).get(CLAIM_TASK_ASSIGNEE_BASE_QUERY);
      taskNameAssigneeListCompleteQuery = taskNameAssigneeListCompleteQuery.replace("queryClaimTaskNameList", "'".concat(String.join(taskNameSuffix.concat("','"), roleMembersMap.keySet())).concat(taskNameSuffix).concat("'"));
    } else {
      for (String roleName : roleMembersMap.keySet()) {
        String taskAssigneeBaseQuery = portalFetchQueryMap(databaseType).get(TASK_ASSIGNEE_BASE_QUERY);
        if (bucketKey.equals(ALL_BUCKET_KEY)) {
          taskAssigneeBaseQuery = portalFetchQueryMap(databaseType).get(ALL_BUCKET_TASK_ASSIGNEE_BASE_QUERY);
          taskAssigneeBaseQuery = taskAssigneeBaseQuery.replace("queryAssignedTaskName", "'".
                  concat(roleName).concat(LIST_VIEW_KEY).concat("', '").
                  concat(roleName).concat(HISTORY_BUCKET_KEY).concat("', '").
                  concat(roleName).concat(HISTORY_BUCKET_KEY).concat("'"));
        }
        taskAssigneeBaseQuery = taskAssigneeBaseQuery.replace("queryAssignedTaskName", "'".concat(roleName).concat(taskNameSuffix).concat("'"));
        taskAssigneeBaseQuery = taskAssigneeBaseQuery.replace("queryAssigneeList", "'".concat(String.join("','", roleMembersMap.get(roleName))).concat("'"));
        taskAssigneeQueryList.add(taskAssigneeBaseQuery);
      }
      taskNameAssigneeListCompleteQuery = String.join(" OR ", taskAssigneeQueryList);
    }

    System.out.println("taskNameAssigneeListCompleteQuery ::::::::::: " + taskNameAssigneeListCompleteQuery);
    return taskNameAssigneeListCompleteQuery;
  }

  public static List<Map<String, Object>> assignedCasesFromResultSet(List<Object[]> resultSet, String roleName, String bucketKey, List<String> bucketVariables, String parentProcessInstanceId, Boolean redirectToDetails) {
    ArrayList<Map<String, Object>> assignedCasesList = new ArrayList<Map<String, Object>>();
    Map<String, Object> bucketVariablesMap = new HashMap<>();
    bucketVariablesMap.putAll(CommonHelperFunctions.getMapFromKeySet(bucketVariables, null));
    for (Object[] row: resultSet) {
      Map<String, Object> assignedCase = new HashMap<>();
      //TODO: need to validate for dashboard view action value
//      assignedCase.putAll(bucketVariablesMap);
      assignedCase.put("parentProcessInstanceId", parentProcessInstanceId);
      for (Object object: row) {
        assignedCase.putAll(CommonHelperFunctions.getHashMapFromJsonString(CommonHelperFunctions.getStringValue(object)));
      }
      if (!redirectToDetails && assignedCase.containsKey("assignee")) {
        assignedCase.put("assignee", null);
      }
      assignedCase.put("roleName", roleName);
      assignedCase.put("assignedRoleName", CommonHelperFunctions.getStringValue(assignedCase.get("taskName")).split("-")[0]);
      assignedCase.put("bucketKey", bucketKey);
      assignedCasesList.add(assignedCase);
    }
    return assignedCasesList;
  }

  public static String getFilterKey(Map<String, String> filters){
    Map<String, Object> tempMap = new HashMap<>();
    tempMap.putAll(filters);
    tempMap.remove(FILTER_OFFSET_KEY);
    tempMap.remove(FILTER_PAGE_WIDTH_KEY);
    return tempMap.keySet().iterator().next();
  }

  public static String getStageInstanceId(List<PlanItemInstance> planItemInstanceList, String stageName) {
//		List<PlanItemInstance> planItemInstanceList = cmmnRuntimeService.createPlanItemInstanceQuery().caseInstanceId(caseInstanceId).planItemInstanceStateActive().list();
    String stageInstanceId = null;
    for (PlanItemInstance planItemInstance: planItemInstanceList){
      if (planItemInstance.isStage() && planItemInstance.getPlanItemDefinitionId().equals(stageName)) {
        stageInstanceId = planItemInstance.getId();
        break;
      }
    }
    return stageInstanceId;
  }

  public static List<String> getPlanItemInstanceDefinitionIdByStage(List<PlanItemInstance> planItemInstanceList, String stageInstanceId) {
    List<String> planItemInstanceSet = new ArrayList<>();
    for (PlanItemInstance planItemInstance: planItemInstanceList){
      if (planItemInstance.getPlanItemDefinitionType().equals("humantask") && planItemInstance.getStageInstanceId().equals(stageInstanceId)) {
        planItemInstanceSet.add(planItemInstance.getPlanItemDefinitionId());
      }
    }
    return planItemInstanceSet;
  }

  public static List<PlanItemInstance> getPlanItemInstanceByStage(List<PlanItemInstance> planItemInstanceList, String stageInstanceId) {
    List<PlanItemInstance> planItemInstanceSet = new ArrayList<>();
    for (PlanItemInstance planItemInstance: planItemInstanceList){
      if (!planItemInstance.isStage() && planItemInstance.getStageInstanceId().equals(stageInstanceId)) {
        planItemInstanceSet.add(planItemInstance);
      }
    }
    return planItemInstanceSet;
  }

}
