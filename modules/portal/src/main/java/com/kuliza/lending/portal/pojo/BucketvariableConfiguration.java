package com.kuliza.lending.portal.pojo;

import java.util.Arrays;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class BucketvariableConfiguration {

  @NotNull(message = "id is a required key")
  @NotEmpty(message = "id cannot be empty")
  String id;

  @NotNull(message = "label is a required key")
  @NotEmpty(message = "label cannot be empty")
  String label;

  @NotNull(message = "type is a required key")
  @NotEmpty(message = "type cannot be empty")
  String type;

  List<ActionConfiguration> actionConfigurationList;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<ActionConfiguration> getActionConfigurationList() {
    return actionConfigurationList;
  }

  public void setActionConfigurationList(
      List<ActionConfiguration> actionConfigurationList) {
    this.actionConfigurationList = actionConfigurationList;
  }

  public void addActionConfigurationList(ActionConfiguration actionConfiguration) {
    if (this.actionConfigurationList != null){
      this.actionConfigurationList.add(actionConfiguration);
    } else {
      this.actionConfigurationList = Arrays.asList(actionConfiguration);
    }
  }
}
