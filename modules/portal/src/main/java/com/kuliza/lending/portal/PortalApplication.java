package com.kuliza.lending.portal;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.kuliza.lending.portal.filter.CustomAuditFilters;
import com.kuliza.lending.portal.utils.PortalConstants;

@SpringBootApplication
@EnableAsync
@ComponentScan(basePackages = {"com.kuliza"})
@EnableScheduling
@EnableRetry
@EnableConfigurationProperties(LiquibaseProperties.class)
public class PortalApplication extends SpringBootServletInitializer {

  @Value(value = "${portal.audit.filter.enabled}")
  private Boolean enableFilter;

  @Autowired
  private LiquibaseProperties properties;
  @Autowired
  private DataSource dataSource;

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(PortalApplication.class);
  }

  public static void main(String[] args) {
    SpringApplication.run(PortalApplication.class, args);
  }

  @Bean
  public FilterRegistrationBean CustomAuditFilters() {
    FilterRegistrationBean registrationBean = new FilterRegistrationBean();
    registrationBean.setFilter(new CustomAuditFilters());
    registrationBean.setEnabled(enableFilter);
    registrationBean.addUrlPatterns(PortalConstants.CUSTOM_API_URL);
    registrationBean.addUrlPatterns(PortalConstants.SET_VARIABLE_API_URL);
    registrationBean.addUrlPatterns(PortalConstants.SUBMIT_FORM_API_URL);
    registrationBean.addUrlPatterns(PortalConstants.BULK_SUBMIT_API);
    return registrationBean;
  }

  @Bean
  public SpringLiquibase liquibase() {
    SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setDataSource(dataSource);
    liquibase.setChangeLog(this.properties.getChangeLog());
    liquibase.setContexts(this.properties.getContexts());
    liquibase.setDefaultSchema(this.properties.getDefaultSchema());
    liquibase.setDropFirst(this.properties.isDropFirst());
    liquibase.setShouldRun(this.properties.isEnabled());
    liquibase.setLabels(this.properties.getLabels());
    liquibase.setChangeLogParameters(this.properties.getParameters());
    liquibase.setRollbackFile(this.properties.getRollbackFile());
    liquibase.setDatabaseChangeLogLockTable("PORTAL_DATABASECHANGELOGLOCK");
    liquibase.setDatabaseChangeLogTable("PORTAL_DATABASECHANGELOG");
    return liquibase;

  }

}
