package com.kuliza.lending.portal.pojo;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.portal.service.PortalService;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.Callable;

public class CallableBulkActionUrlThread implements Callable<ApiResponse> {
    private String slug;
    private PortalService portalService;
    private CustomBulkActionURLBody customBulkActionURLBody;
    private String contentType;
    private String token;
    private RequestAttributes context;
    private String roleName;
    private String userId;
    private HttpServletRequest request;

    public CallableBulkActionUrlThread(HttpServletRequest request, String slug, PortalService portalService, CustomBulkActionURLBody customBulkActionURLBody,
                                       String contentType, String token, RequestAttributes context, String roleName, String userId) {
        this.slug = slug;
        this.portalService = portalService;
        this.customBulkActionURLBody = customBulkActionURLBody;
        this.contentType = contentType;
        this.token = token;
        this.context = context;
        this.roleName = roleName;
        this.userId = userId;
        this.request = request;
    }

    public ApiResponse call() throws Exception {
        ApiResponse response = null;
        if (context != null) {
            RequestContextHolder.setRequestAttributes(context);
        }
        response = this.portalService.fireCustomAction(request, slug, customBulkActionURLBody.getCustomActionURLBody(),
            contentType, token, customBulkActionURLBody.getCaseInstanceId(), roleName, userId);
        return response;
    }
}
