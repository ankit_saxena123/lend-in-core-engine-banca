package com.kuliza.lending.portal.utils;

public class EnumConstants {
	public enum Events {
		IncomingRequestEvent, OutgoingRequestEvent, IncomingResponseEvent, OutgoingResponseEvent, SetVariablesEvent, SubmitFormEvent;
	}

}
