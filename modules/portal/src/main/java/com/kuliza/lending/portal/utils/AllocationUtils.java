package com.kuliza.lending.portal.utils;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.portal.models.AllocationDao;
import com.kuliza.lending.portal.models.AllocationModel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.keycloak.representations.idm.UserRepresentation;

public class AllocationUtils {
	

	public static String getGroupIdFromName(String groupName) {
		return groupName;
	}

	public static void saveToAllocation(String portalType, List<String> groupIds, AllocationDao allocationDao, KeyCloakService keyCloakService) {
		
		for (String groupId : groupIds) {
			Set<String> keyCloakCopyUsers = new HashSet<String>();
			Set<String> keyCloakUsers = new HashSet<String>();
			Set<String> allocationUsers = new HashSet<String>();
			for (UserRepresentation userRepresentation : keyCloakService.groupMembers(groupId)) {
				if (userRepresentation.getEmail() != null && !userRepresentation.getEmail().isEmpty()) {
					keyCloakUsers.add(userRepresentation.getEmail());
				}
			}
			List<AllocationModel> allocationUserObjects = allocationDao
					.findByPortalTypeAndGroupIdAndIsDeletedOrderByCreatedAsc(portalType, groupId, false);
			for (AllocationModel allocationUserObject : allocationUserObjects) {
				allocationUsers.add(allocationUserObject.getEmailId());
			}
			keyCloakCopyUsers.addAll(keyCloakUsers);
			keyCloakUsers.removeAll(allocationUsers);

			for (String newUser : keyCloakUsers) {
				AllocationModel allocationObject = new AllocationModel();
				allocationObject.setEmailId(newUser);
				allocationObject.setGroupId(groupId);
				allocationObject.setPortalType(portalType);
				allocationDao.save(allocationObject);
			}
			allocationUsers.removeAll(keyCloakCopyUsers);
			String[] removeUsers = allocationUsers.toArray(new String[allocationUsers.size()]);
			List<AllocationModel> removeUserObjs = allocationDao
					.findByPortalTypeAndGroupIdAndEmailIdInAndIsDeleted(portalType, groupId, removeUsers, false);
			for (AllocationModel removeUser : removeUserObjs) {
				removeUser.setIsDeleted(true);
				allocationDao.save(removeUser);
			}
		}
		
	}

	public static String getUserByRoundRobin(String portalType, List<String> groupIds, AllocationDao allocationDao) {
		AllocationModel user = null;
		user = allocationDao.findFirstByPortalTypeAndGroupIdInAndIsOccupiedAndIsDeletedOrderByCreatedDesc(portalType,
				groupIds, false, false);
		if (user == null) {
			List<AllocationModel> occupiedUsers = allocationDao
					.findByPortalTypeAndGroupIdInAndIsDeletedOrderByCreatedAsc(portalType, groupIds, false);
			for (AllocationModel occupiedUser : occupiedUsers) {
				occupiedUser.setIsOccupied(false);
				user = allocationDao.save(occupiedUser);
			}
		}
		user.setIsOccupied(true);
		user = allocationDao.save(user);
		return user.getEmailId();
	}

}
