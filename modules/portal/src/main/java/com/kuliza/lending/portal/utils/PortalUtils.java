package com.kuliza.lending.portal.utils;

import com.kuliza.lending.portal.pojo.ActionConfiguration;
import com.kuliza.lending.portal.pojo.BucketConfiguration;
import com.kuliza.lending.portal.pojo.BucketvariableConfiguration;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.engine.runtime.ProcessInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.kuliza.lending.portal.utils.PortalConstants.GLOBAL_SEARCH_KEY;
import static com.kuliza.lending.portal.utils.PortalConstants.PARENT_CASE_INSTANCE_ID;

public class PortalUtils {

	/**
	 * This is an utility method used to check if a given string is not null and not
	 * empty
	 * 
	 * @param value
	 * @return true if String is not empty else false
	 */
	public static boolean isNotEmpty(String value) {
		return (value != null) && (!value.trim().equals(""));
	}

	/**
	 * Checks if filter key is one from the bucket variables
	 *
	 * @param filters
	 * @param bucketVariables
	 *
	 * @return boolean
	 *
	 * */
	public static boolean checkFilterKeys(Map<String, String> filters, List<String> bucketVariables) {
		Object[] filterKeys = filters.keySet().toArray();
		// to skip the filtering check whether it is present in bucket or not, add those variables to bucket variables.
		bucketVariables.add(GLOBAL_SEARCH_KEY);
		bucketVariables.add(PARENT_CASE_INSTANCE_ID);
		for (int i = 0; i < filterKeys.length; i++) {
			String filterKey = (String) filterKeys[i];
			if ((!filterKey.equals(PortalConstants.FILTER_OFFSET_KEY))
					&& (!filterKey.equals(PortalConstants.FILTER_PAGE_WIDTH_KEY))
					&& !(bucketVariables.contains(filterKey)))
				return false;
		}
		return true;
	}

	/**
	 * This method is used to get a bucket variables for backOffice
	 * List of Bucket Configurations
	 *
	 * @return list of actions
	 *
	 */
	public static List<String> getBucketActions(BucketConfiguration bucketConfiguration) {
		List<String> actions = new ArrayList<>();
		List<BucketvariableConfiguration> bucketVariablesConfig = bucketConfiguration.getVariables();
		for (BucketvariableConfiguration bucketVariable : bucketVariablesConfig) {
			if (bucketVariable.getType().equals(PortalConstants.PORTAL_ACTION_VARIABLE_TYPE))
				actions.add(bucketVariable.getId());
		}
		return actions;
	}

	/**
	 * This method is used to get a bucket variables for backOffice
	 *
	 * @return list of variables
	 *
	 */
	public static List<String> getBucketVariables(BucketConfiguration bucketConfiguration, Set<String> productVariables) {
		List<String> variables = new ArrayList<>();
		List<BucketvariableConfiguration> bucketVariablesConfig = bucketConfiguration.getVariables();
		List<ActionConfiguration> actionConfigurationList = new ArrayList<>();
		actionConfigurationList.addAll(bucketConfiguration.getBucketActions());
		for (BucketvariableConfiguration bucketVariable : bucketVariablesConfig) {
			
			if (!bucketVariable.getType().equals(PortalConstants.PORTAL_ACTION_VARIABLE_TYPE))
				variables.add(bucketVariable.getId());
			else{
				actionConfigurationList.addAll(bucketVariable.getActionConfigurationList());
			}
		}
		for (ActionConfiguration actionConfiguration: actionConfigurationList){
			variables.add(actionConfiguration.getId());
		}
		variables.addAll(productVariables);
		variables.add(GLOBAL_SEARCH_KEY);
		return variables;
	}


	/**
	 * Get process definition key from list of case Instances
	 *
	 * 
	 * @return ProcessDeifinitionKey
	 * 
	 * **/

	public static String getCaseDefinitionKey(List<CaseInstance> caseInstances) {
		String caseDefinitionKey = "";
		if (!caseInstances.isEmpty()) {
			CaseInstance caseInstance = caseInstances.get(0);
			String caseDefinitionId = caseInstance.getCaseDefinitionId();
			String[] caseDefinitionIdArr = caseDefinitionId.split(":", 2);
			if (caseDefinitionIdArr.length >= 2) {
				caseDefinitionKey = caseDefinitionIdArr[0];
			}
		}
		return caseDefinitionKey;
	}
	
	/**
	 * Get process definition key from list of processInstances of BPMN
	 *
	 * 
	 * @return ProcessDeifinitionKey
	 * 
	 * **/

	public static String getBPMNDefinitionKey(List<ProcessInstance> processInstances) {
		String processDefinitionKey = "";
		if (!processInstances.isEmpty()) {
			ProcessInstance processInstance = processInstances.get(0);
			processDefinitionKey = processInstance.getProcessDefinitionKey();
		}
		return processDefinitionKey;
	}
	
	/**
	 * This function returns a map of processVariables for the first case instance
	 * from the list
	 * 
	 * @param caseInstances:
	 *            List of caseinstances
	 * 
	 * @return map of processVariables
	 * 
	 **/

	public static Map<String, Object> getProcessVariables(List<CaseInstance> caseInstances) {
		Map<String, Object> caseVariables = null;
		if (!caseInstances.isEmpty()) {
			CaseInstance caseInstance = caseInstances.get(0);
			caseVariables = caseInstance.getCaseVariables();
		}
		return caseVariables;
	}

	public static Map<String, String> getFilters(Map<String, String> filters){
		Map<String, String> newFilters = filters;
		if(newFilters.get(PortalConstants.GROUP_ID) != null){
			newFilters.remove(PortalConstants.GROUP_ID);
		}
		if(newFilters.get(PortalConstants.ASSIGNEE) != null){
			newFilters.remove(PortalConstants.ASSIGNEE);
		}
		return newFilters;
	}

}
