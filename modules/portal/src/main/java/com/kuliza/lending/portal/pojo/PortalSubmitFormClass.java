package com.kuliza.lending.portal.pojo;

import java.util.Map;

public class PortalSubmitFormClass {

	private String taskId;
	private String caseInstanceId;
	private Map<String, Object> formProperties;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public Map<String, Object> getFormProperties() {

		return formProperties;
	}

	public void setFormProperties(Map<String, Object> formProperties) {
		this.formProperties = formProperties;
	}

	public PortalSubmitFormClass(String taskId, String caseInstanceId, Map<String, Object> formProperties) {
		super();
		this.taskId = taskId;
		this.formProperties = formProperties;
		this.caseInstanceId = caseInstanceId;
	}

	public PortalSubmitFormClass() {
	}

	@Override
	public String toString() {
		return "PortalSubmitFormClass [taskId=" + taskId + ", caseInstanceId=" + caseInstanceId + ", formProperties="
				+ formProperties + "]";
	}
}
