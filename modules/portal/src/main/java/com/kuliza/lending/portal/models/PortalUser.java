package com.kuliza.lending.portal.models;

import com.kuliza.lending.common.model.BaseModel;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "portal_user")
public class PortalUser extends BaseModel {

	@Column(nullable = true)
	private String caseInstanceId;

	@Column
	private String applicationNumber;

	// TODO: add enum for portal types, destination
	@Column
	private String portalType;

	// new instantiation one, processName for portals
	@Column
	private String caseName;

	public String getCaseInstanceId() {
		return caseInstanceId;
	}

	public void setCaseInstanceId(String caseInstanceId) {
		this.caseInstanceId = caseInstanceId;
	}

	public String getPortalType() {
		return portalType;
	}

	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}


	public PortalUser(String applicationNumber, String caseInstanceId, String caseName,
			String portalType) {
		this.applicationNumber = applicationNumber;
		this.caseInstanceId = caseInstanceId;
		this.caseName = caseName;
		this.portalType = portalType;
		this.setIsDeleted(false);
	}

	public PortalUser() {
		super();
		this.setIsDeleted(false);
	}
}
