package com.kuliza.lending.portal.models;

import com.kuliza.lending.common.model.BaseModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "allocation")
public class AllocationModel extends BaseModel {

	// TODO: add enum for portal types, destination
	@Column
	private String portalType;

	@Column(nullable = true)
	private String groupId;

	@Column
	private String emailId;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable=false)
	private Boolean isOccupied = false;

	public AllocationModel() {
		super();
		// TODO Auto-generated constructor stub
		this.setIsOccupied(false);
		this.setIsDeleted(false);
	}

	public AllocationModel(String portalType, String groupId, String emailId, Boolean isOccupied) {
		super();
		this.portalType = portalType;
		this.groupId = groupId;
		this.emailId = emailId;
		this.setIsOccupied(false);
		this.setIsDeleted(false);
	}

	public String getPortalType() {
		return portalType;
	}

	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Boolean getIsOccupied() {
		return isOccupied;
	}

	public void setIsOccupied(Boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

}
