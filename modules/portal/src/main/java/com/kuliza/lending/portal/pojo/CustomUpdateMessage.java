package com.kuliza.lending.portal.pojo;

import java.io.Serializable;

import com.kuliza.lending.portal.utils.EnumConstants.Events;

public class CustomUpdateMessage implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public CustomUpdateMessage() {
    super();
    // TODO Auto-generated constructor stub
  }

  private String transactionId;
	private String requestId;
	private String data;
	private Events event;

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Events getEvent() {
    return event;
  }

  public void setEvent(Events event) {
    this.event = event;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public CustomUpdateMessage(String requestId, String data, Events event) {
    super();
    this.requestId = requestId;
    this.data = data;
    this.event = event;
  }

  public CustomUpdateMessage(String requestId, String data, String transactionId, Events event) {
    super();
    this.requestId = requestId;
    this.data = data;
    this.transactionId = transactionId;
    this.event = event;
  }

  @Override
  public String toString() {
    return "CustomUpdateMessage{" +
        "transactionId='" + transactionId + '\'' +
        ", requestId='" + requestId + '\'' +
        ", data='" + data + '\'' +
        ", event=" + event +
        '}';
  }
}
