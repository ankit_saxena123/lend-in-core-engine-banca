package com.kuliza.lending.portal.models;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.portal.converter.ActionConfiguratorConverter;
import com.kuliza.lending.portal.converter.BucketConfiguratorConverter;
import com.kuliza.lending.portal.pojo.ActionConfiguration;
import com.kuliza.lending.portal.pojo.BucketConfiguration;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "portal_configuration")
public class PortalConfigurationModel extends BaseModel {

  @NotNull(message = "Role name can not be null")
  @Column(nullable = false)
  private String roleName;

  @Column(nullable = false)
  private String roleLabel;

  @Column
  private String productType;

  @Column
  private String productLabel;

  @Column(columnDefinition="LONGTEXT")
  @Convert(converter = BucketConfiguratorConverter.class)
  private List<BucketConfiguration> bucketList;

  @Column(columnDefinition="LONGTEXT")
  @Convert(converter = ActionConfiguratorConverter.class)
  private List<ActionConfiguration> productActionList;

  @Transient
  @ApiModelProperty(required = false, hidden = true)
  private Object productVariableList;

  @Column
  @ApiModelProperty(hidden = true, required = false)
  private String userId;

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleLabel() {
    return roleLabel;
  }

  public void setRoleLabel(String roleLabel) {
    this.roleLabel = roleLabel;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public String getProductLabel() {
    return productLabel;
  }

  public void setProductLabel(String productLabel) {
    this.productLabel = productLabel;
  }

  public List<BucketConfiguration> getBucketList() {
    return bucketList;
  }

  public void setBucketList(List<BucketConfiguration> bucketList) {
    this.bucketList = bucketList;
  }

  public List<ActionConfiguration> getProductActionList() {
    return productActionList;
  }

  public void setProductActionList(
      List<ActionConfiguration> productActionList) {
    this.productActionList = productActionList;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void addBucketConfig(BucketConfiguration bucketConfiguration){
    if (this.bucketList != null){
      this.bucketList.add(bucketConfiguration);
    } else {
      this.bucketList = Arrays.asList(bucketConfiguration);
    }
  }

  public Object getProductVariableList() {
    return productVariableList;
  }

  public void setProductVariableList(Object productVariableList) {
    this.productVariableList = productVariableList;
  }

  public PortalConfigurationModel(String roleName, String roleLabel, String productType, String productLabel, List<BucketConfiguration> bucketList, List<ActionConfiguration> productActionList) {
    this.bucketList = bucketList;
    this.productType = productType;
    this.productLabel = productLabel;
    this.roleName = roleName;
    this.roleLabel = roleLabel;
    this.productActionList = productActionList;
    this.setIsDeleted(false);
  }

  public PortalConfigurationModel(){
    super();
    this.setIsDeleted(false);
  }


@Override
public String toString() {
	return "PortalConfigurationModel [roleName=" + roleName + ", roleLabel=" + roleLabel + ", productType=" + productType + ", productLabel="
			+ productLabel + ", bucketList=" + bucketList + ", userId=" + userId + "]";
}
}
