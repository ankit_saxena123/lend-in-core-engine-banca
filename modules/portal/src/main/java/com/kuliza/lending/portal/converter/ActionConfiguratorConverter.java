package com.kuliza.lending.portal.converter;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.portal.pojo.ActionConfiguration;
import com.kuliza.lending.portal.utils.PortalHelperFunction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActionConfiguratorConverter implements
    AttributeConverter<List<ActionConfiguration>, String> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ActionConfiguratorConverter.class);

  @Override
  public String convertToDatabaseColumn(List<ActionConfiguration> actionConfigurationList) {
    ArrayList<String> actionList = new ArrayList<>();
    try {
      for (ActionConfiguration actionConfiguration: actionConfigurationList){
        if (actionConfiguration != null){
          Map<String, Object> actionConfigurationMap = new HashMap<>();
          actionConfigurationMap.put("id", actionConfiguration.getId());
          actionConfigurationMap.put("label", actionConfiguration.getLabel());
          actionConfigurationMap.put("params", actionConfiguration.getParams());
          actionConfigurationMap.put("iconKey", actionConfiguration.getIconKey());
          actionConfigurationMap.put("value", actionConfiguration.getValue());


          actionList.add(CommonHelperFunctions
              .getJsonString(actionConfigurationMap));
        }
      }
    } catch (Exception exception) {
      LOGGER.error("convertToDatabaseColumn excepotion : " + exception);
      exception.printStackTrace();
    }
    return CommonHelperFunctions.getStringValue(actionList);
  }

  @Override
  public List<ActionConfiguration> convertToEntityAttribute(String actionConfigurationList) {
    ArrayList<ActionConfiguration> response = new ArrayList<>();
    try {
      response = PortalHelperFunction.getActionConfigurationListFromString(actionConfigurationList);
    } catch (Exception exception) {
      LOGGER.error("convertToEntityAttribute excepotion : " + exception);
      exception.printStackTrace();
    }
//    LOGGER.info("convertToEntityAttribute response : " + response);
    return response;
  }

}
