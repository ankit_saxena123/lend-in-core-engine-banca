package com.kuliza.lending.portal.broker;

import com.kuliza.lending.portal.filter.WrappedRequest;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.ThreadContext;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.portal.misc.AuditEventHandler;
import com.kuliza.lending.portal.pojo.CustomMessageDetails;
import com.kuliza.lending.portal.pojo.CustomMessageVariables;
import com.kuliza.lending.portal.pojo.CustomUpdateMessage;
import com.kuliza.lending.portal.service.AuditService;
import com.kuliza.lending.portal.utils.EnumConstants.Events;

@Service
public class CommonListenerService {

  @Autowired
  private AuditEventHandler eventHandler;

  @Autowired
  private AuditService auditService;
  

  private static final Logger LOGGER = LoggerFactory.getLogger(CommonListenerService.class);

  public void forwardMessageToService(String message) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      JSONObject object = new JSONObject(message);
      if(object.has("transactionId"))
        ThreadContext.put("transactionId",object.getString("transactionId"));
      if (object.has("event") && object.get("event").equals(Events.IncomingRequestEvent.name())) {
        CustomMessageDetails customMessageDetails =
            mapper.readValue(message, CustomMessageDetails.class);
        eventHandler.handler(customMessageDetails);
      } else if (object.has("event") && (object.get("event").equals(Events.SetVariablesEvent.name())
          || object.get("event").equals(Events.SubmitFormEvent.name()))) {
        CustomMessageVariables customMessageVariables =
            mapper.readValue(message, CustomMessageVariables.class);
        eventHandler.handler(customMessageVariables);
      } else {
        CustomUpdateMessage customUpdateMessage =
            mapper.readValue(message, CustomUpdateMessage.class);
        eventHandler.handler(customUpdateMessage);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    LOGGER.info("Processed message as specific class: {}", message.toString());
  }

  public void updateAuditEntrySync(Events event, String dataToUpdate, String requestId) {
    CustomUpdateMessage customUpdateMessage =
        new CustomUpdateMessage(requestId, dataToUpdate, event);
    if (customUpdateMessage.getEvent() != null) {
      LOGGER.info("publish UpdateAuditEvent " + customUpdateMessage);
      auditService.updateAuditEntry(customUpdateMessage);
    }
  }

  public void saveAuditEntrySync(Events event, HttpServletRequest request, String jsonRequestBody) {
    CustomMessageDetails customMessage =
        auditService.getCustomAuditMessageDetails(event, request, jsonRequestBody);
    if (customMessage != null && customMessage.getEvent() != null) {
      LOGGER.info("publish IncomingRequestEvent" + customMessage);
      auditService.saveAuditEntry(customMessage);
    }
  }

  public void saveAuditEntryFilterSync(Events event, WrappedRequest request) {
    CustomMessageDetails customMessage = auditService.getCustomAuditMessageDetailsFilter(event, request);
    if (customMessage != null && customMessage.getEvent() != null) {
      LOGGER.info("publish IncomingRequestEvent Filter" + customMessage);
      auditService.saveAuditEntry(customMessage);
    }
  }

  public void updateOrCreateAuditEntrySync(Events event, String requestId,
      Map<String, Object> functionParametersAndValues) {
    CustomMessageVariables customMessageVariables =
        new CustomMessageVariables(requestId, event, functionParametersAndValues);
    if (customMessageVariables.getEvent() != null) {
      LOGGER.info("publish VariableUpdate " + customMessageVariables);
      auditService.updateOrCreateAuditEntry(customMessageVariables);
    }
  }
}
