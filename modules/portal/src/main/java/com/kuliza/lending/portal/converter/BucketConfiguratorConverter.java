package com.kuliza.lending.portal.converter;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.portal.pojo.BucketConfiguration;
import com.kuliza.lending.portal.utils.PortalHelperFunction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BucketConfiguratorConverter implements
    AttributeConverter<List<BucketConfiguration>, String> {

  private static final Logger LOGGER = LoggerFactory.getLogger(BucketConfiguratorConverter.class);

  @Override
  public String convertToDatabaseColumn(List<BucketConfiguration> bucketConfigurationList) {
    ArrayList<String> bucketList = new ArrayList<>();
    try {
      for (BucketConfiguration bucketConfiguration: bucketConfigurationList){
        if (bucketConfiguration != null){
          Map<String, Object> bucketConfigurationMap = new HashMap<>();
          bucketConfigurationMap.put("id", bucketConfiguration.getId());
          bucketConfigurationMap.put("label", bucketConfiguration.getLabel());
          bucketConfigurationMap.put("editable", bucketConfiguration.getEditable());
          bucketConfigurationMap.put("comment", bucketConfiguration.getComment());
          bucketConfigurationMap.put("redirectToDetails", bucketConfiguration.getRedirectToDetails());
          bucketConfigurationMap.put("bucketActions", (Object) bucketConfiguration.getBucketActions());
          bucketConfigurationMap.put("variables", (Object) bucketConfiguration.getVariables());


          bucketList.add(CommonHelperFunctions
            .getJsonString(bucketConfigurationMap));
        }
      }
    } catch (Exception exception) {
      LOGGER.error("convertToDatabaseColumn excepotion : " + exception);
      exception.printStackTrace();
    }
//    LOGGER.info("convertToDatabaseColumn response : " + bucketList);
    return CommonHelperFunctions.getStringValue(bucketList);
  }

  @Override
  public List<BucketConfiguration> convertToEntityAttribute(String bucketConfigurationList) {
    ArrayList<BucketConfiguration> response = new ArrayList<>();
    try {
      response = PortalHelperFunction.getBucketConfigList(bucketConfigurationList);
    } catch (Exception exception) {
      LOGGER.error("convertToEntityAttribute excepotion : " + exception);
      exception.printStackTrace();
    }
//    LOGGER.info("convertToEntityAttribute response : " + response);
    return response;
    }

}
