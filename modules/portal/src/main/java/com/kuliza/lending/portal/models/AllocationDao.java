package com.kuliza.lending.portal.models;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.portal.models.AllocationModel;

@Repository
public interface AllocationDao extends CrudRepository<AllocationModel, Long> {

	public AllocationModel findFirstByPortalTypeAndGroupIdInAndIsOccupiedAndIsDeletedOrderByCreatedDesc(String portalType,
			List<String> groupIds, Boolean isOccupied, Boolean isDeleted);

	public List<AllocationModel> findByPortalTypeAndGroupIdAndIsDeletedOrderByCreatedAsc(String portalType,
			String groupId, Boolean isDeleted);

	public List<AllocationModel> findByPortalTypeAndGroupIdAndEmailIdInAndIsDeleted(String portalType, String groupId,
			String[] emailIds, Boolean isDeleted);
	
	public List<AllocationModel> findByPortalTypeAndGroupIdInAndIsDeletedOrderByCreatedAsc(String portalType,
			List<String> groupIds, Boolean isDeleted);

}
