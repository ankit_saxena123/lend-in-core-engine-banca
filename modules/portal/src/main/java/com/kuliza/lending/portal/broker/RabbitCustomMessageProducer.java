package com.kuliza.lending.portal.broker;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.ThreadContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.portal.filter.WrappedRequest;
import com.kuliza.lending.portal.pojo.CustomMessageDetails;
import com.kuliza.lending.portal.pojo.CustomMessageVariables;
import com.kuliza.lending.portal.pojo.CustomUpdateMessage;
import com.kuliza.lending.portal.service.AuditService;
import com.kuliza.lending.portal.utils.EnumConstants;
import com.kuliza.lending.portal.utils.PortalConstants;

@Service
//@ConditionalOnExpression("${broker.name.rabbit:true}")
public class RabbitCustomMessageProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitCustomMessageProducer.class);
	@Autowired
	RabbitTemplate template;

	@Autowired
	AuditService auditService;

	public void publishAuditEvent(EnumConstants.Events event, HttpServletRequest request, String jsonRequestBody)
			throws JsonProcessingException, JSONException {
		LOGGER.info(event.name() + " received");
		CustomMessageDetails message = auditService.getCustomAuditMessageDetails(event, request, jsonRequestBody);
		ObjectMapper mapper = new ObjectMapper();
		String publishMessage = mapper.writeValueAsString(message);
		template.convertAndSend(PortalConstants.EXCHANGE_NAME, PortalConstants.AUDIT_ROUTING_KEY, publishMessage);
	}

	public void publishAuditEvent(EnumConstants.Events event, String response, String requestId)
			throws JsonProcessingException {
		LOGGER.info(event.name() + " received");
		CustomUpdateMessage message = null;
		Map<String, String> threadContext = ThreadContext.getContext();
		if(threadContext!=null && threadContext.containsKey("transactionId"))
			message = new CustomUpdateMessage(requestId, response, threadContext.get("transactionId"), event);
		else
			message = new CustomUpdateMessage(requestId, response, event);
		ObjectMapper mapper = new ObjectMapper();
		String publishMessage = mapper.writeValueAsString(message);
		template.convertAndSend(PortalConstants.EXCHANGE_NAME, PortalConstants.AUDIT_ROUTING_KEY, publishMessage);
	}

	public void publishAuditEventFilter(EnumConstants.Events event, WrappedRequest request)
			throws JsonProcessingException, JSONException {
		LOGGER.info(event.name() + " received using filter");
		CustomMessageDetails message = auditService.getCustomAuditMessageDetailsFilter(event, request);
		ObjectMapper mapper = new ObjectMapper();
		String publishMessage = mapper.writeValueAsString(message);
		template.convertAndSend(PortalConstants.EXCHANGE_NAME, PortalConstants.AUDIT_ROUTING_KEY, publishMessage);
	}
	
	public void publishAuditEvent(EnumConstants.Events event, String requestId,
			Map<String, Object> functionParametersAndValues) throws JsonProcessingException {
		LOGGER.info(event.name() + " received using method");
		ObjectMapper mapper = new ObjectMapper();
		CustomMessageVariables message = null;
		Map<String, String> threadContext = ThreadContext.getContext();
		if(threadContext!=null && threadContext.containsKey("transactionId"))
			message =  new CustomMessageVariables(requestId, threadContext.get("transactionId"), event, functionParametersAndValues);
		else
			message = new CustomMessageVariables(requestId, event, functionParametersAndValues);
		String publishMessage = mapper.writeValueAsString(message);
		template.convertAndSend(PortalConstants.EXCHANGE_NAME, PortalConstants.AUDIT_ROUTING_KEY, publishMessage);
	}
}
