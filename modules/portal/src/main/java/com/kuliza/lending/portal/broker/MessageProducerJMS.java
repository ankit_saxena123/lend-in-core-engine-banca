package com.kuliza.lending.portal.broker;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.ThreadContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.portal.filter.WrappedRequest;
import com.kuliza.lending.portal.pojo.CustomMessageDetails;
import com.kuliza.lending.portal.pojo.CustomMessageVariables;
import com.kuliza.lending.portal.pojo.CustomUpdateMessage;
import com.kuliza.lending.portal.service.AuditService;
import com.kuliza.lending.portal.utils.EnumConstants;
import com.kuliza.lending.portal.utils.PortalConstants;

@Service
//@ConditionalOnExpression("${broker.name.rabbit:false}")
public class MessageProducerJMS {
	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	AuditService auditService;

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageProducerJMS.class);

	public MessageCreator getJMSMessage(String message) {
		return new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(message);
			}
		};
	}

	public void publishAuditEvent(EnumConstants.Events event, HttpServletRequest request, String jsonStr)
			throws JsonProcessingException, JSONException {
		LOGGER.info(event.name() + " received");
		final CustomMessageDetails message = auditService.getCustomAuditMessageDetails(event, request, jsonStr);
		jmsTemplate.convertAndSend(PortalConstants.JMS_AUDIT_ROUTING_KEY, message);
	}

	public void publishAuditEvent(EnumConstants.Events event, String response, String requestId)
			throws JsonProcessingException {
		LOGGER.info(event.name() + " received");
		CustomUpdateMessage message = null;
		Map<String, String> threadContext = ThreadContext.getContext();
		if(threadContext!=null && threadContext.containsKey("transactionId"))
			message = new CustomUpdateMessage(requestId, response, threadContext.get("transactionId"), event);
		else
			message = new CustomUpdateMessage(requestId, response, event);
		jmsTemplate.convertAndSend(PortalConstants.JMS_AUDIT_ROUTING_KEY, message);
	}

	public void publishAuditEventFilter(EnumConstants.Events event, WrappedRequest request)
			throws JsonProcessingException, JSONException {
		LOGGER.info(event.name() + " received using filter");
		CustomMessageDetails message = auditService.getCustomAuditMessageDetailsFilter(event, request);
		jmsTemplate.convertAndSend(PortalConstants.JMS_AUDIT_ROUTING_KEY, message);
	}

	public void publishAuditEvent(EnumConstants.Events event, String requestId,
			Map<String, Object> functionParametersAndValues) throws JsonProcessingException {
		LOGGER.info(event.name() + " received using method");
		CustomMessageVariables message = null;
		Map<String, String> threadContext = ThreadContext.getContext();
		if(threadContext!=null && threadContext.containsKey("transactionId"))
			message =  new CustomMessageVariables(requestId, threadContext.get("transactionId"), event, functionParametersAndValues);
		else
			message = new CustomMessageVariables(requestId, event, functionParametersAndValues);
		jmsTemplate.convertAndSend(PortalConstants.JMS_AUDIT_ROUTING_KEY, message);
	}

}
