package com.kuliza.lending.portal.filter;

import com.kuliza.lending.portal.broker.CommonListenerService;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.BrokerConfigs;
import com.kuliza.lending.portal.broker.MessageProducerJMS;
import com.kuliza.lending.portal.broker.MessageProducerKafka;
import com.kuliza.lending.portal.broker.RabbitCustomMessageProducer;
import com.kuliza.lending.portal.utils.EnumConstants.Events;

@Order(Ordered.LOWEST_PRECEDENCE)
public class CustomAuditFilters implements Filter {

	@Autowired
	RabbitCustomMessageProducer rabbitMessageProducer;
	
	@Autowired
	MessageProducerJMS messageProducerJMS;
	
	@Autowired
	MessageProducerKafka kafkaMessageProducer;

	@Autowired
	CommonListenerService commonListenerService;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest requestContext, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (CommonHelperFunctions.getStringValue(requestContext.getAttribute("requestId")).isEmpty()) {
			final HttpServletRequest request = (HttpServletRequest) requestContext;
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			WrappedRequest multiReadRequest = new WrappedRequest(request);
			ContentCachingResponseWrapper wrappedResponse = wrapResponse(httpResponse);
			ServletContext servletContext = requestContext.getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			if (messageProducerJMS == null) {
				messageProducerJMS = webApplicationContext.getBean(MessageProducerJMS.class);
			}
			if (rabbitMessageProducer == null) {
				rabbitMessageProducer = webApplicationContext.getBean(RabbitCustomMessageProducer.class);
			}
			if(kafkaMessageProducer==null) {
				kafkaMessageProducer = webApplicationContext.getBean(MessageProducerKafka.class);
			}
			if(commonListenerService==null) {
				commonListenerService = webApplicationContext.getBean(CommonListenerService.class);
			}
			if (CommonHelperFunctions.getStringValue(multiReadRequest.getParameter("requestId")).isEmpty()) {
				requestContext.setAttribute("requestId", UUID.randomUUID().toString());
				try {
					if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
						messageProducerJMS.publishAuditEventFilter(Events.IncomingRequestEvent, multiReadRequest);
					}else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
						kafkaMessageProducer.publishAuditEventFilter(Events.IncomingRequestEvent, multiReadRequest);
					} else if(StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")) {
						rabbitMessageProducer.publishAuditEventFilter(Events.IncomingRequestEvent, multiReadRequest);
					}
					else{
						commonListenerService.saveAuditEntryFilterSync(Events.IncomingRequestEvent, multiReadRequest);
					}
					//rabbitMessageProducer.publishAuditEventFilter(Events.IncomingRequestEvent, multiReadRequest);
					chain.doFilter(multiReadRequest, wrappedResponse);
					String requestId = (String) requestContext.getAttribute("requestId");
					byte[] content = wrappedResponse.getContentAsByteArray();
					String responseContent = new String(content, wrappedResponse.getCharacterEncoding());
					if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
						messageProducerJMS.publishAuditEvent(Events.OutgoingResponseEvent, responseContent, requestId);
					} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
						kafkaMessageProducer.publishAuditEvent(Events.OutgoingResponseEvent, responseContent, requestId);
					} else if(StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")){
						rabbitMessageProducer.publishAuditEvent(Events.OutgoingResponseEvent, responseContent, requestId);
					}else{
						commonListenerService.updateAuditEntrySync(Events.OutgoingResponseEvent, responseContent, requestId);
					}
					wrappedResponse.copyBodyToResponse();
				} catch (Exception e) {
					e.printStackTrace();
					chain.doFilter(multiReadRequest, wrappedResponse);
				}
			} else {
				chain.doFilter(multiReadRequest, wrappedResponse);
			}
		} else {
			chain.doFilter(requestContext, response);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	static ContentCachingResponseWrapper wrapResponse(HttpServletResponse response) {
		if (response instanceof ContentCachingResponseWrapper) {
			return (ContentCachingResponseWrapper) response;
		} else {
			return new ContentCachingResponseWrapper(response);
		}
	}

}
