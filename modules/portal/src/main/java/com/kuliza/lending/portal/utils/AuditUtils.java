package com.kuliza.lending.portal.utils;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.utils.CommonHelperFunctions;

@Service
public class AuditUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditUtils.class);

	public static Map<String, Object> getAttributes(HttpServletRequest request) {
		/**
		 * Utility function that takes HttpReqeuest as input and returns the
		 * PathVariables and RequestId from the attributeMap of request
		 **/
		Map<String, Object> attributeMap = new HashMap<String, Object>();
		Enumeration<String> attrs = request.getAttributeNames();
		while (attrs.hasMoreElements()) {
			String name = attrs.nextElement();
			if (name.equals(PortalConstants.REQUEST_PATH_VARIABLES) || name.equals(PortalConstants.REQUEST_ID_KEY)) {
				Object value = request.getAttribute(name);
				attributeMap.put(name, value);
			}
		}
		return attributeMap;
	}

	/**
	 * This function assumes that the last part of request URL will be a path
	 * variable representing role name
	 */
	public static String getMapperUrlFilter(HttpServletRequest request) {
		StringBuffer uri = request.getRequestURL();
		String customUrlId = uri.substring(uri.lastIndexOf("/") + 1, uri.length());
		return customUrlId;
	}

	public static String getPathVariable(Map<String, Object> attributeMap, String key) {
		String value = "";
		Object pathVariableMap = attributeMap.get(PortalConstants.REQUEST_PATH_VARIABLES);
		if (pathVariableMap == null) {
			return value;
		}
		if (pathVariableMap instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, String> pathVariables = (Map<String, String>) pathVariableMap;
			return CommonHelperFunctions.getStringValue(pathVariables.getOrDefault(key, ""));
		}
		return value;
	}

	public static Map<String, Object> getParameters(HttpServletRequest request) {
		/**
		 * Utility function that takes HttpReqeuest as input and returns the map of
		 * params and their values
		 **/
		Map<String, Object> attributeMap = new HashMap<String, Object>();
		Enumeration<String> attrs = request.getParameterNames();
		while (attrs.hasMoreElements()) {
			String name = attrs.nextElement();
			Object value = request.getParameter(name);
			attributeMap.put(name, value);
		}
		return attributeMap;
	}

	public static JSONObject getRequestBody(HttpServletRequest request) {
		/**
		 * Utility function that takes HttpReqeuest as input reads the requestbody using
		 * inputstream then converts it to json object to return
		 **/
		StringBuffer stringbuffer = new StringBuffer();
		String line = null;
		try {
			InputStream s = request.getInputStream();
			line = IOUtils.toString(s, Charsets.UTF_8);
			stringbuffer.append(line);
			// BufferedReader reader = request.getReader();
			// while ((line = reader.readLine()) != null)
			// stringbuffer.append(line);
		} catch (Exception e) { /* report an error */
			LOGGER.warn("Reader threw error");
			LOGGER.warn(e.getMessage());
			e.printStackTrace();
		}

		JSONObject jsonObject = null;
		try {
			jsonObject = HTTP.toJSONObject(stringbuffer.toString());
		} catch (JSONException e) {
			// crash and burn
			LOGGER.warn("Error parsing JSON request string");
		}
		return jsonObject;
	}
	
	public String getValue(Object result) {
		String returnValue = null;
		if (null != result) {
			if (result.toString().endsWith("@" + Integer.toHexString(result.hashCode()))) {
				returnValue = ReflectionToStringBuilder.toString(result);
			} else {
				returnValue = result.toString();
			}
		}
		return returnValue;
	}

}
