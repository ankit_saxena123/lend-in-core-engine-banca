package com.kuliza.lending.portal.exceptions;

public class AuditNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AuditNotFoundException() {
		super();
	}

	public AuditNotFoundException(String message) {
		super(message);
	}

}
