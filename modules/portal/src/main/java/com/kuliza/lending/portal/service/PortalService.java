package com.kuliza.lending.portal.service;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.connection.UnirestConnection;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.CommonConfig;
import com.kuliza.lending.engine_common.configs.PortalConfig;
import com.kuliza.lending.engine_common.models.CustomURLMapper;
import com.kuliza.lending.engine_common.models.CustomURLMapperDao;
import com.kuliza.lending.engine_common.pojo.CustomActionHeaders;
import com.kuliza.lending.engine_common.portal.PortalJourneyServiceTask;
import com.kuliza.lending.portal.exceptions.BucketNotMappedException;
import com.kuliza.lending.portal.exceptions.DashboardNotConfiguredForRoleException;
import com.kuliza.lending.portal.exceptions.VariableNotFoundException;
import com.kuliza.lending.portal.models.*;
import com.kuliza.lending.portal.pojo.*;
import com.kuliza.lending.portal.utils.PortalConstants;
import com.kuliza.lending.portal.utils.PortalConstants.*;
import com.kuliza.lending.portal.utils.PortalHelperFunction;
import com.kuliza.lending.portal.utils.PortalUtils;
import com.mashape.unirest.http.HttpResponse;
import org.apache.commons.io.IOUtils;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.CmmnTaskService;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.CaseInstanceQuery;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.runtime.ProcessInstanceQuery;
import org.flowable.form.api.FormService;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskQuery;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.json.JSONArray;
import org.json.JSONObject;
import org.keycloak.representations.idm.GroupRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.*;

import static com.kuliza.lending.engine_common.utils.MetaReadingUtils.getErrorActionProcessVariables;
import static com.kuliza.lending.engine_common.utils.MetaReadingUtils.getSuccessActionProcessVariables;
import static com.kuliza.lending.portal.utils.PortalConstants.*;

@Service
public class PortalService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PortalService.class);

	@Resource
	PortalService portalService;

	@Autowired
	private CmmnTaskService cmmnTaskService;

	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	private CMMNService cmmnService;

	@Autowired
	private PortalConfigurationDao portalConfigurationDao;

	@Autowired
	private PortalCommentHistoryDao portalCommentHistoryDao;

	@Autowired
	private PortalUserDao portalUserDao;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private KeyCloakService keyCloakService;

	@Autowired
	private AllocationDao allocationDao;

	@Autowired
	private CustomURLMapperDao customUrlMapperDao;
	
	@Autowired
	private AuditService auditService;

	@Autowired
	private PortalJourneyServiceTask portalJourneyServiceTask;

	@Autowired
	private HistoryService historyService;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private FormService formService;

	@Autowired
	private CommonConfig commonConfig;

	public ApiResponse configurePortal(String userName, PortalConfigurationModel portalConfigurationModel)
			throws Exception {
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_ALREADY_EXISTS);

		String roleName = portalConfigurationModel.getRoleName();

		PortalConfigurationModel portalConfiguration = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(),
				portalConfigurationModel.getProductType());

		if (portalConfiguration == null) {
			PortalConfigurationModel portalConfigurationModelnew = new PortalConfigurationModel(portalConfigurationModel.getRoleName(),
					portalConfigurationModel.getRoleLabel(), portalConfigurationModel.getProductType(), portalConfigurationModel.getProductLabel(),
					portalConfigurationModel.getBucketList(), portalConfigurationModel.getProductActionList());
			portalConfigurationModelnew.setUserId(userName.trim());
			portalConfigurationModelnew.setRoleName(roleName.trim());
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					portalConfigurationDao.save(portalConfigurationModelnew));
		}
		return response;
	}

	public ApiResponse updateConfigurePortal(String userName,
			PortalConfigurationModel portalConfigurationModel) throws Exception {

		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_NOT_EXISTS);

		String roleName = portalConfigurationModel.getRoleName();

		PortalConfigurationModel portalConfiguration = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(), portalConfigurationModel.getProductType());

		if (portalConfiguration != null) {
			portalConfiguration.setUserId(userName.trim());
			portalConfiguration.setBucketList(portalConfigurationModel.getBucketList());
			portalConfiguration.setProductActionList(portalConfigurationModel.getProductActionList());
			portalConfiguration.setProductLabel(portalConfigurationModel.getProductLabel());
			portalConfiguration.setRoleLabel(portalConfigurationModel.getRoleLabel());
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					portalConfigurationDao.save(portalConfiguration));
		}
		return response;
	}

	public ApiResponse deleteConfigurePortal(String roleName, String productType) throws Exception {

		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_NOT_EXISTS);

		PortalConfigurationModel portalConfiguration = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(), productType.trim());

		if (portalConfiguration != null) {
			portalConfigurationDao.delete(portalConfiguration);
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		}
		return response;
	}

	public ApiResponse getConfigurePortal(String roleName, String productType) {

		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_NOT_EXISTS);

		PortalConfigurationModel portalConfiguration = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(), productType.trim());

		if (portalConfiguration != null) {
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, portalConfiguration);
		}
		return response;
	}

	public ApiResponse addBucketConfig(String roleName, String userName, String productType,
			List<BucketConfiguration> bucketConfigurationList) {

		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_NOT_EXISTS);

		PortalConfigurationModel portalConfiguration = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(), productType.trim());

		if (portalConfiguration != null) {
			portalConfiguration.setUserId(userName.trim());
			for (BucketConfiguration bucketConfiguration : bucketConfigurationList) {
				portalConfiguration.addBucketConfig(bucketConfiguration);
			}
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					portalConfigurationDao.save(portalConfiguration));
		}
		return response;
	}

	public ApiResponse getBucketConfigList(String roleName, String productType) {

		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_NOT_EXISTS);

		PortalConfigurationModel portalConfiguration = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(),
				productType.trim());

		if (portalConfiguration != null) {
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, portalConfiguration.getBucketList());
		}
		return response;
	}

	public ApiResponse getProductConfig(String roleName) {
		Set<PortalConfigurationModel> portalConfigurationModelList = portalConfigurationDao.findByRoleName(roleName.trim());
		List<PortalConfigurationModel> responsePortalConfigurationModelList = new ArrayList<>();
		for (PortalConfigurationModel portalConfigurationModel: portalConfigurationModelList){
			portalConfigurationModel.setProductVariableList(getConfigurationProductVariable(roleName));
			responsePortalConfigurationModelList.add(portalConfigurationModel);
		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responsePortalConfigurationModelList);
	}

	public Map<String, FormField> getConfigurationProductVariable(String roleName) {
		Map<String, FormField> productVariables = new HashMap<>();
		try {
			productVariables = cmmnService.getSimpleFormModel(roleName, null).allFieldsAsMap();
		} catch (Exception e) {
			LOGGER.info("Get Product Config getCompletePortalConfigurationModel is not present or internal server error: " + e);
		}
		return productVariables;
	}

	/**
	 * This method is used to get a bucket Config for backOffice
	 *
	 * @param roleName
	 * @param productType
	 * @param bucketKey
	 *
	 * @return bucketConfigurationlist
	 */

	public BucketConfiguration getBucketConfig(String roleName, String productType, String bucketKey) {

		List<BucketConfiguration> bucketConfigurationList = null;
		BucketConfiguration bucketConfigurationDefault = null;
		PortalConfigurationModel portalConfigurations = portalConfigurationDao.findByRoleNameAndProductType(
				roleName.trim(),
				productType.trim());
		if (portalConfigurations != null) {
			bucketConfigurationList = portalConfigurations.getBucketList();
		}
		for (BucketConfiguration bucketConfiguration : bucketConfigurationList) {
			if (bucketConfiguration.getId().equals(bucketKey)) {
				return bucketConfiguration;
			}
		}
		return bucketConfigurationDefault;
	}

	/**
	 * Returns a taskquery formed for different tabs. Query returns tasks for user
	 * tasks with given task_def_key and have given bucketKey
	 *
	 * @param bucketKey
	 *            bucket for which we need to fetch cases
	 * @param roleName
	 *            to get name of task for the user
	 *
	 * @return TaskQuery instance to get tasks for the user
	 */

	public TaskQuery getPortalTaskQuery(String bucketKey, String roleName, Set<String> assigneeList) {
		String bucketTaskName = roleName.concat("-").concat(bucketKey);
		TaskQuery query = cmmnTaskService.createTaskQuery().scopeType("cmmn").active();
		if (!bucketKey.equals(PortalConstants.HISTORY_BUCKET_KEY) && !bucketKey.equals(PortalConstants.CLAIM_BUCKET_KEY)) {
			bucketTaskName = roleName.concat(PortalConstants.LIST_VIEW_KEY);
		}
		query = query.taskDefinitionKey(bucketTaskName);
		if (!bucketKey.equals(PortalConstants.CLAIM_BUCKET_KEY)) {
			query = query.taskAssigneeIds(new ArrayList<String>(assigneeList));
		}
		query = query.orderByTaskCreateTime().desc();
		return query;
	}

	/**
	 * Returns a taskquery formed for different tabs. Query returns tasks for user
	 * tasks with given task_def_key and have given bucketKey
	 *
	 * @param bucketKey
	 *            bucket for which we need to fetch cases
	 * @param roleName
	 *            to get name of task for the user
	 * @return TaskQuery instance to get tasks for the user
	 */

	public CaseInstance getPortalCaseInstance(Task task, String roleName, String bucketKey, String productType){
		CaseInstance caseInstance = null;
		CaseInstanceQuery caseInstanceQuery = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(task.getScopeId());
		if (!bucketKey.equals(ALL_BUCKET_KEY)){
			caseInstanceQuery = caseInstanceQuery.includeCaseVariables().variableValueEquals(roleName.concat(STATUS_VARIABLE_KEY), bucketKey);;
		}
		if (!productType.equals(ALL_PRODUCT_TYPE)){
			caseInstanceQuery = caseInstanceQuery.variableValueEquals(JOURNEY_TYPE, productType);
		}
		List<CaseInstance> caseInstanceList = caseInstanceQuery.list();
		if (!caseInstanceList.isEmpty())
			caseInstance = caseInstanceList.get(0);
		return caseInstance;
	}


	/**
	 * Returns a taskquery formed for different tabs. Query returns tasks for user
	 * tasks with given task_def_key and have given bucketKey
	 *
	 * @param bucketKey
	 *            bucket for which we need to fetch cases
	 * @param roleName
	 *            to get name of task for the user
	 * @return TaskQuery instance to get tasks for the user
	 */

	public CaseInstanceTaskMap getPortalCaseInstanceList(List<Task> taskList, String roleName, String bucketKey,
														 String productType, Map<String, String> filters,
														 String parentCaseInstanceId){
		CaseInstanceTaskMap caseInstanceTaskMap = new CaseInstanceTaskMap();
		List<CaseInstance> caseInstanceList = new ArrayList<>();
		Map<String, TaskDetails> taskMapList = new HashMap<>();
		for (Task task: taskList) {
			TaskDetails taskDetails =  new TaskDetails();
			taskDetails.setAssignee(task.getAssignee());
			taskDetails.setTaskName(task.getTaskDefinitionKey());
			taskMapList.put(task.getScopeId(), taskDetails);
		}
		if (taskMapList.size() > 0) {
			CaseInstanceQuery caseInstanceQuery = cmmnRuntimeService.createCaseInstanceQuery()
					.caseInstanceIds(taskMapList.keySet());
			if(parentCaseInstanceId != null && !parentCaseInstanceId.isEmpty()){
				caseInstanceQuery = caseInstanceQuery.caseInstanceParentId(parentCaseInstanceId);
			}
			if (!bucketKey.equals(ALL_BUCKET_KEY)) {
				caseInstanceQuery = caseInstanceQuery.includeCaseVariables()
						.variableValueEquals(roleName.concat(STATUS_VARIABLE_KEY), bucketKey);
			}
			if (!productType.equals(ALL_PRODUCT_TYPE)) {
				caseInstanceQuery = caseInstanceQuery.variableValueEquals(JOURNEY_TYPE, productType);
			}
			caseInstanceList = caseInstanceQuery.orderByStartTime().desc().list();
		}
    caseInstanceTaskMap.setCaseInstanceList(caseInstanceList);
		caseInstanceTaskMap.setTaskMap(taskMapList);
		return caseInstanceTaskMap;
	}

	/**
	 * Returns List of assigned tasks for user after filters
	 *
	 * @param tasks
	 *            list of tasks which can be shown
	 * @param bucketVariables
	 *            a list of variables for which we need to get data from
	 *            processVariables
	 *
	 * @return list of cases assigned to the user
	 */
	public List<Map<String, Object>> getAssignedCaseList(List<HistoricTaskInstance> tasks,
			List<String> bucketVariables, String bucketKey, Boolean redirectToDetails, String productType,
														 String parentCaseInstanceId) {
		Map<String, Object> processVariables = new HashMap<>();
		List<Map<String, Object>> taskList = new ArrayList<>();
		for (HistoricTaskInstance task: tasks) {
			String caseInstanceId = task.getScopeId();
			processVariables = cmmnRuntimeService.getVariables(caseInstanceId);
			Map<String, Object> taskMap = new HashMap<>();
			for (int j = 0; j < bucketVariables.size(); j++) {
				taskMap.put(bucketVariables.get(j), processVariables.get(bucketVariables.get(j)));
			}
			if (!taskMap.isEmpty()) {
				String taskAssignee = null;
				if (redirectToDetails) {
					taskAssignee = task.getAssignee();
				}
				taskMap.put("caseInstanceId", caseInstanceId);
//					taskMap.put("portalId", businessKey);
				taskMap.put("assignee", taskAssignee);
				taskMap.put("bucket", bucketKey);
				taskMap.put("roleName", task.getTaskDefinitionKey().split("-")[0]);
				taskMap.put("productType", productType);
				taskMap.put("taskName", task.getTaskDefinitionKey());
				taskList.add(taskMap);
			}
			}
		return taskList;
	}

	/**
	 * This method is used to get list of Assigned cases for role ad bucket
	 *
	 * @param username
	 * @param productType
	 *            for the type of product
	 * @param bucketKey
	 *            for which bucket of role, Assigned cases are to be returned
	 * @param filters
	 *            A map of filters to get results based on these
	 * @return List of Assigned cases for a given role, bucket, product and
	 *         filters(if any)
	 * @throws Exception
	 */
	@LogMethodDetails(userId = "username")
	public ApiResponse getAssignedCaseList(String roleName, String username, String groupId, String productType, String bucketKey,
										   Map<String, String> filters) throws Exception {
		ApiResponse response = null;
		String parentCaseInstanceId = null;

		BucketConfiguration bucketConfiguration = getBucketConfig(roleName, productType, bucketKey);
		if (bucketConfiguration == null) {
			LOGGER.warn(LOG_EXCEPTION_PORTAL_USER, username, PortalConstants.getDashboardNotConfiguredForRoleMessage(roleName));
			throw new DashboardNotConfiguredForRoleException(
					PortalConstants.getDashboardNotConfiguredForRoleMessage(roleName));
		}

		List<String> bucketVariables = PortalUtils.getBucketVariables(bucketConfiguration, getConfigurationProductVariable(roleName).keySet());
		LOGGER.info("BucketVariables for userID {} are {}", username, bucketVariables);
		if (!bucketVariables.isEmpty()) {
			boolean filterCheck = PortalUtils.checkFilterKeys(filters, bucketVariables);
			if (!filterCheck) {
				LOGGER.warn(LOG_EXCEPTION_PORTAL_USER, username, PortalConstants.getVariableNotMappedForRoleMessage(roleName));
				throw new VariableNotFoundException(PortalConstants.getVariableNotMappedForRoleMessage(roleName));
			}
			Map<String, Set<String>> roleMembersMap = getSubRolesAndUsers(groupId, username);
			List<Map<String, Object>> taskList = new ArrayList<>();
			Query query = entityManager.createNativeQuery(PortalHelperFunction.portalFetchingCaseQuery(
					roleMembersMap, filters, productType, bucketKey, bucketVariables, commonConfig.getDatabaseType()));
			List<Object[]> tasks = query.getResultList();

			LOGGER.info("Portal userId {} got query tasks", username);
			Integer totalCount = tasks.size();
			if (PortalUtils.isNotEmpty(filters.get(PortalConstants.FILTER_OFFSET_KEY)) &&
					PortalUtils.isNotEmpty(filters.get(PortalConstants.FILTER_PAGE_WIDTH_KEY))) {
				if (tasks.size() > (Integer.parseInt(filters.get(PortalConstants.FILTER_OFFSET_KEY)) *
						Integer.parseInt(filters.get(PortalConstants.FILTER_PAGE_WIDTH_KEY)))) {
					tasks = tasks.subList((Integer.parseInt(filters.get(PortalConstants.FILTER_OFFSET_KEY)) *
									Integer.parseInt(filters.get(PortalConstants.FILTER_PAGE_WIDTH_KEY))),
							tasks.size());
					if (tasks.size() > Integer.parseInt(filters.get(PortalConstants.FILTER_PAGE_WIDTH_KEY))) {
						tasks = tasks.subList(0, Integer.parseInt(filters.get(PortalConstants.FILTER_PAGE_WIDTH_KEY)));
					}
				} else {
					tasks = new ArrayList<>();
				}
			}
			taskList.addAll(PortalHelperFunction.assignedCasesFromResultSet(tasks, roleName, bucketKey, bucketVariables, parentCaseInstanceId, bucketConfiguration.getRedirectToDetails()));
			Map<String, Object> metaMap = new HashMap<>();
			Map<String, Object> responseObject = new HashMap<>();
			metaMap.put("totalCount", totalCount);
			metaMap.put("showPagination", true);
			responseObject.put("meta", metaMap);
			responseObject.put(bucketKey, taskList);
			response = new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), responseObject);
		} else {
			LOGGER.warn(LOG_EXCEPTION_PORTAL_USER, username, PortalConstants.getBucketNotConfiguredForRoleMessage(roleName));
			throw new BucketNotMappedException(PortalConstants.getBucketNotConfiguredForRoleMessage(roleName));
		}
		return response;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given case instance id.
	 *
	 * @param userId
	 * @param caseInstanceId
	 * @return ApiResponse
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse getTabConfig(String userId, String caseInstanceId, String roleName) throws Exception {
		LOGGER.info("getTabConfig userId : " + userId);
		LOGGER.info("getTabConfig caseInstanceId : " + caseInstanceId);
		ApiResponse response;
		ArrayList<TabConfig> tabConfigList = new ArrayList<>();
		List<String> taskIdList = getTaskIdListByRoleName(caseInstanceId, roleName);
		List<Task> activeTaskList = cmmnTaskService.createTaskQuery().scopeType("cmmn").caseInstanceId(caseInstanceId)
				.taskAssignee(userId).active().orderByTaskPriority().asc().list();
		if (activeTaskList.size() < 1) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.PORTAL_TAB_NOT_EXISTS);
		} else {
			ArrayList<String> parentTabList = new ArrayList<>();
			for (Task task : activeTaskList) {
				if (taskIdList.contains(task.getTaskDefinitionKey())) {
					String taskLabel = task.getName().trim();
					String taskId = task.getTaskDefinitionKey().trim();

					TabConfig tabConfig = new TabConfig();
					tabConfig.setId(taskId);
					tabConfig.setLabel(taskLabel);

					if (!taskLabel.equals(PORTAL_DASHBORD_TASK_POSTFIX)
							&& !taskLabel.equals(PORTAL_HISTORY_TASK_POSTFIX)
							&& !taskLabel.equals(PORTAL_CLAIM_TASK_POSTFIX)
							&& !taskLabel.contains(PORTAL_GLOBAL_TASK_POSTFIX)) {

						String[] taskLabelList = taskLabel.trim().split("-");
						if (taskLabelList.length == 2) {

							String parentTaskLabel = taskLabelList[0].trim();
							String parentTaskId = CommonHelperFunctions.getCamelCase(parentTaskLabel).replaceAll(" ", "");
							String childTabTaskLabel = taskLabelList[1].trim();

							TabConfig childTabConfig = new TabConfig();
							childTabConfig.setLabel(childTabTaskLabel);
							childTabConfig.setId(taskId);

							if (parentTabList.contains(parentTaskLabel)) {
								for (TabConfig tempTabConfig : tabConfigList) {
									if (tempTabConfig.getLabel().equals(parentTaskLabel)) {
										tempTabConfig.addChildTab(childTabConfig);
									}
								}
							} else {
								tabConfig.setId(parentTaskId);
								tabConfig.setLabel(parentTaskLabel);
								tabConfig.setChildPresent(true);
								tabConfig.addChildTab(childTabConfig);
								parentTabList.add(parentTaskLabel);
								tabConfigList.add(tabConfig);
							}
						} else {
							tabConfigList.add(tabConfig);
						}
					}
				}
			}
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, tabConfigList);
		}
		LOGGER.info("getTabConfig response : " + response.getData());
		return response;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given case instance id.
	 *
	 * @param caseInstanceId
	 * @return ApiResponse
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse getCardConfig(HttpServletRequest request, String roleName, String bucket, String tabKey, String caseInstanceId, Map<String, String> filters)
			throws Exception {
		ApiResponse response;
		String groupId = filters.getOrDefault(PortalConstants.GROUP_ID, null);;
		String userId = filters.getOrDefault(PortalConstants.ASSIGNEE, null);
		Map<String, Object> processVariables = cmmnRuntimeService.getVariables(caseInstanceId);
		validateRequestResponse(request, roleName, groupId, userId, processVariables);
		filters = PortalUtils.getFilters(filters);

        Task currentTasktab = cmmnTaskService.createTaskQuery().scopeType("cmmn").active().caseInstanceId(caseInstanceId).taskAssignee(userId)
                .taskDefinitionKey(tabKey).singleResult();
		if (currentTasktab == null) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.PORTAL_TAB_NOT_EXISTS);
		} else {
			Integer commentConfiguration = 0;
			Map<String, Object> extendedData = new HashMap<>();
			SimpleFormModel currentFormModel = null;
			Map<String, FormField> currentFormFields = new HashMap<>();
			if(tabKey.contains(PortalConstants.PORTFOLIO_SUFFIX)){
				filters.put(PortalConstants.PARENT_CASE_INSTANCE_ID, caseInstanceId);
				ApiResponse portfolioApiResponse = getAssignedCaseList(roleName, userId, groupId, ALL_PRODUCT_TYPE, ALL_BUCKET_KEY, filters);
				Object portfolioResponseObject = portfolioApiResponse.getData();
				BucketConfiguration portfolioBucketConfiguration = getBucketConfig(roleName, ALL_PRODUCT_TYPE, ALL_BUCKET_KEY);
				commentConfiguration = portfolioBucketConfiguration.getComment();
				extendedData.put("assignedList", portfolioResponseObject);
				extendedData.put("bucketConfiguration", portfolioBucketConfiguration);
			}else{
				commentConfiguration = commentConfiguration(roleName, bucket, CommonHelperFunctions.getStringValue(processVariables.get(JOURNEY_TYPE)));
				currentFormModel = cmmnService.getSimpleFormModel(currentTasktab, processVariables);
				currentFormFields = currentFormModel.allFieldsAsMap();
			}
//			List<PortalCommentHistory> commentHistoryList = null;
			Task globalTaskTab = cmmnTaskService.createTaskQuery().scopeType("cmmn").active().caseInstanceId(caseInstanceId).taskAssignee(userId)
					.taskNameLike(PORTAL_GLOBAL_TASK_LIKE).taskDefinitionKeyLike(roleName+"%").singleResult();
			SimpleFormModel globalFormModel = cmmnService.getSimpleFormModel(globalTaskTab, processVariables);
			Map<String, Object> visitedTabsMap = PortalHelperFunction.getVisitedTab(processVariables, roleName, userId, tabKey);
      		Set<String> oldVisitedTabs = (Set<String>) visitedTabsMap.get("oldVistedTabs");
      		cmmnRuntimeService.setVariable(caseInstanceId, PORTAL_VISTED_TAB, visitedTabsMap.get("newVistedTabsMap"));
			//TODO: we can remove if UI sends bucketKey.
//			if (commentConfiguration > 0 && commentConfiguration < 3) {
//				commentHistoryList = portalCommentHistoryDao.findByPortalUserCaseInstanceId(caseInstanceId);
//			}
			Map<String, FormField> globalFormFields = globalFormModel.allFieldsAsMap();
			extendedData.put("caseInstanceId", caseInstanceId);
			extendedData.put("commentConfiguration", commentConfiguration);
			extendedData.put(PORTAL_VISTED_TAB, oldVisitedTabs);
			response = PortalHelperFunction.makePortalResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					currentTasktab, extendedData, currentFormFields, globalFormFields, null);
		}
		LOGGER.info("getCardConfig response : " + response);
		return response;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given case instance id.
	 *
	 * @param userId
	 * @param caseInstanceId
	 * @return ApiResponse
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse setComment(HttpServletRequest request, String roleName, String userId, String tabKey, String caseInstanceId,
			PortalCommentHistory portalCommentHistory) throws Exception {
		validateRequestResponse(request, roleName, null, userId, runtimeService.getVariables(caseInstanceId));
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_NOT_EXISTS);
		PortalUser portalUser = portalUserDao.findByCaseInstanceIdAndIsDeleted(caseInstanceId, false);
		if (portalUser != null) {
			portalCommentHistory.setRoleName(roleName);
			portalCommentHistory.setUserId(userId);
			portalCommentHistory.setPortalUser(portalUser);
			portalCommentHistory.setTaskKey(tabKey);
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, portalCommentHistoryDao.save(portalCommentHistory));
		}
		LOGGER.info("setComment response : " + response);
		return response;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given case instance id.
	 *
	 * @param caseInstanceId
	 * @return ApiResponse
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse getComment(String tabKey, String caseInstanceId) throws Exception {

		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PORTAL_USER_NOT_EXISTS);
		PortalUser portalUser = portalUserDao.findByCaseInstanceIdAndIsDeleted(caseInstanceId, false);
		if (portalUser != null) {
			if (tabKey == null || tabKey.isEmpty()) {
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						portalCommentHistoryDao.findByPortalUserCaseInstanceId(caseInstanceId));
			} else {
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						portalCommentHistoryDao.findByTaskKeyAndPortalUserCaseInstanceId(tabKey, caseInstanceId));
			}
		}
		LOGGER.info("getComment response : " + response);
		return response;
	}

	/**
	 * This function returns the instance of InitiatePortalData which acts as part of request body for sending the case to portal
	 *
	 * @param sourceKey
	 * @param sourceInstanceId
	 *
	 * @return InitiatePortalData
	 *
	 * **/

	public InitiatePortalData getRequestBody(String sourceKey, String sourceInstanceId, String destinationInstanceId) {
		CaseInstanceQuery query = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(sourceInstanceId).desc();
		List<CaseInstance> caseInstances = query.list();
		Map<String, Object> variablesToSave = PortalUtils.getProcessVariables(caseInstances);
		String caseDefinitionKey = PortalUtils.getCaseDefinitionKey(caseInstances);
		if (caseDefinitionKey.isEmpty() || variablesToSave == null)
			return null;

		InitiatePortalData initiatePortal = new InitiatePortalData();
		initiatePortal.setPortalType(sourceKey);
		initiatePortal.setSourceId(sourceInstanceId);
		initiatePortal.setSourceType(caseDefinitionKey);
		initiatePortal.setVariablesToSave(variablesToSave);
		return initiatePortal;
	}

	/**
	 * This function prepares the request body for returning process back to journey
	 *
	 * @param sourceKey Parameter contains the name of the portal/process
	 * @param sourceInstanceId
	 * @param destinationInstanceId
	 *
	 * @return JSONObject of requestBody
	 *
	 * **/

	public JSONObject getJourneyRequestBody(String sourceKey, String sourceInstanceId, String destinationInstanceId) {
		JSONObject requestBody = new JSONObject();
		CaseInstanceQuery query = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(sourceInstanceId).desc();
		List<CaseInstance> caseInstances = query.list();
		Map<String, Object> variablesToSave = PortalUtils.getProcessVariables(caseInstances);
		String caseDefinitionKey = PortalUtils.getCaseDefinitionKey(caseInstances);
		if (caseDefinitionKey.isEmpty() || variablesToSave == null)
			return null;
		String applicationNumber = CommonHelperFunctions
				.getStringValue(variablesToSave.get(Constants.LOAN_APPLICATION_ID_KEY));
		String processName = getDestinationProcessName(destinationInstanceId, false);
		requestBody.put(Constants.LOAN_APPLICATION_ID_KEY, applicationNumber);
		requestBody.put("journeyType", processName);
		requestBody.put("sourceType", sourceKey); // or should we send name of portal
		requestBody.put("variablesToSave", variablesToSave);
		requestBody.put("closeTask", false); // This should be configured in the journey itself, so should be read
												// instead of code
		return requestBody;
	}

	/**
	 * Get process definition key from processInstanceId or case InstanceId
	 *
	 * @param destinationInstanceId
	 * @param isCaseModel
	 *
	 * @return ProcessDeifinitionKey
	 *
	 * **/

	public String getDestinationProcessName(String destinationInstanceId, boolean isCaseModel) {
		String processDefinitionKey = "";
		if (isCaseModel) {
			CaseInstanceQuery query = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(destinationInstanceId)
					.desc();
			List<CaseInstance> caseInstances = query.list();
			processDefinitionKey = PortalUtils.getCaseDefinitionKey(caseInstances);
		} else {
			ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery()
					.processInstanceId(destinationInstanceId).desc();
			List<ProcessInstance> processInstances = query.list();
			processDefinitionKey = PortalUtils.getBPMNDefinitionKey(processInstances);
		}

		return processDefinitionKey;
	}

	/**
	 * TODO
	 * **/

	public ApiResponse copyProcessVariables(String sourceKey, String sourceInstanceId, String destinationInstanceId,
			String desinationKey) throws Exception {
		Processes sourceProcess;
		Processes destinationProcess;
		try {
			sourceProcess = Processes.valueOf(sourceKey);
			destinationProcess = Processes.valueOf(desinationKey);
		} catch (IllegalArgumentException ex) {
			return new ApiResponse(HttpStatus.BAD_REQUEST, "Invalid source key or destination key");
		}

		switch (destinationProcess) {
		case JOURNEY:
			JSONObject requestBodyJson = getJourneyRequestBody(sourceKey, sourceInstanceId, destinationInstanceId);
			// TODO: call API for journey
			break;
		case BACKOFFICE:
		case COLLATERAL:
		case COLLECTIONS:
		case LMS:
			InitiatePortalData requestBody = getRequestBody(sourceKey, sourceInstanceId, destinationInstanceId);
			Map<String, Object> requestVariables = requestBody.getVariablesToSave();
			String applicationNumber = CommonHelperFunctions
					.getStringValue(requestVariables.get(Constants.LOAN_APPLICATION_ID_KEY));
			// TODO: figure out how to get user, could be some other table
			String userId = CommonHelperFunctions.getStringValue(Constants.PROCESS_TASKS_ASSIGNEE_KEY);
			String caseName = getDestinationProcessName(destinationInstanceId, true);
			cmmnService.startOrResumeCase(caseName, applicationNumber, userId, requestBody, Constants.PORTFOLIO_FLAG_VALUE);
			// TODO: call initiate for portal
			break;
		default:
			break;
		}

		// LOGGER.info("getComment response : " + response);
		return new ApiResponse(HttpStatus.ACCEPTED, "Invalid source key or destination key");
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given case instance id.
	 *
	 *
	 * @return ApiResponse
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	public Integer commentConfiguration(String roleName, String bucketKey, String productType) throws Exception {
		Integer commentConfiguration = 0;
		BucketConfiguration bucketConfiguration = getBucketConfig(roleName, productType, bucketKey);
		if (bucketConfiguration != null) {
			commentConfiguration = bucketConfiguration.getComment();
		}
		return commentConfiguration;
	}

	public Map<String, Set<String>> getSubRolesAndUsers(String groupId, String emailId){
		Map<String, Set<String>> response = new HashMap<>();
		Map<String, Object> roleMemberMap = PortalHelperFunction.getRoleMembersList(keyCloakService.subGroupDetailsWithMembers(groupId), groupId, emailId);
		for (String key: roleMemberMap.keySet())
			response.put(key, (HashSet<String>) roleMemberMap.get(key));
		return response;
	}

	public ApiResponse saveCustomUrlMapper(List<CustomURLMapper> customUrlMapperList, String userId){
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		List<CustomURLMapper> finalCustomUrlMapperList = new ArrayList<>();
		if(customUrlMapperList.size() > 0) {
			for (CustomURLMapper customUrlMapper : customUrlMapperList) {
				customUrlMapper.setUserId(userId);
				finalCustomUrlMapperList.add(customUrlMapperDao.save(customUrlMapper));
				customUrlMapper.setBulkActionUrl();
				customUrlMapper.setCustomActionUrl();
			}
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, finalCustomUrlMapperList);
		}
		return response;
	}

	public ApiResponse deleteCustomUrl(String slug){
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		CustomURLMapper mapper = customUrlMapperDao.findBySlugAndIsDeleted(slug, false);
		if(mapper != null)
		{
			mapper.setIsDeleted(true);
			customUrlMapperDao.save(mapper);
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		}else{
			response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.URL_MAPPER_NOT_EXIST);
		}
		return response;
	}

	public ApiResponse deleteAllCustomURL(){
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		for(CustomURLMapper customUrlMapper : customUrlMapperDao.findAll())
		{
			CustomURLMapper mapper = customUrlMapper;
			mapper.setIsDeleted(true);
			customUrlMapperDao.save(mapper);
		}
		response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		return response;
	}

	public ApiResponse updateCustomURLMapper(String slug, CustomURLMapper customUrlMapper){
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.CUSTOM_URL_NOT_EXISTS);
		CustomURLMapper mapper = customUrlMapperDao.findBySlugAndIsDeleted(slug, false);
		if(mapper != null && customUrlMapper != null)
		{
			if(customUrlMapper.getSlug() != null)
				mapper.setSlug(customUrlMapper.getSlug());
			if(customUrlMapper.getPortalType() != null)
				mapper.setPortalType(customUrlMapper.getPortalType());
			if(customUrlMapper.getHeaders() != null)
				mapper.setHeaders(customUrlMapper.getHeaders());
			mapper.setRequestType(customUrlMapper.getRequestType());
			mapper.setUrl(customUrlMapper.getUrl());
			mapper.setCustomActionUrl();
			mapper.setBulkActionUrl();
			customUrlMapperDao.save(mapper);
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, mapper);
		}
		return response;
	}

	public ApiResponse getCustomURL(String slug){
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.CUSTOM_URL_NOT_EXISTS);
		CustomURLMapper customUrlMapper = customUrlMapperDao.findBySlugAndIsDeleted(slug, false);
		if(customUrlMapper != null)
		{
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, customUrlMapper);
		}
		return response;
	}

	private String setPathVariables(String url, Map<String, String> pathVariables) {
		String pathVariablesUrl = url;
		if(pathVariables != null && !pathVariables.isEmpty() && pathVariables.size() > 0){
			for(String key : pathVariables.keySet()){
				pathVariablesUrl = pathVariablesUrl.replace("/"+key+"/",
						"/" + pathVariables.get(key) + "/");
				if((pathVariablesUrl.indexOf("/"+key) + ("/"+key).length() ) == pathVariablesUrl.length()){
					pathVariablesUrl = pathVariablesUrl.replace("/"+key, "/" + pathVariables.get(key));
				}
			}
		}
		return pathVariablesUrl;
	}

	private String setRequestParams(String url, Map<String, String> requestParams) {
		String requestParamUrl = url;
		if(requestParams != null && !requestParams.isEmpty() && requestParams.size() > 0){
			requestParamUrl = requestParamUrl + "?";
			int mapSize = requestParams.size(), count = 1;
			for (String key : requestParams.keySet()) {
				requestParamUrl = requestParamUrl + key + "=" + requestParams.get(key);
				if (count < mapSize) {
					requestParamUrl = requestParamUrl + "&";
				}
				count++;
			}
		}
		return requestParamUrl;
	}

	@SuppressWarnings("unchecked")
	@LogMethodDetails
	public HttpResponse getHttpResponse(CustomURLMapper customUrlMapper, CustomActionURLBody customActionUrlBody,
			Map<String, String> headers, String customUrl) {
		HttpResponse<String> httpResponse = null;
		Map<String, Object> requestBody = new HashMap<>();
		JSONArray requestArrayBody = new JSONArray();
		RequestAttributes attribs = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = null;
		if (attribs instanceof NativeWebRequest) {
			request = (HttpServletRequest) ((NativeWebRequest) attribs)
					.getNativeRequest();
		}
			String requestId = "";
		if (request != null && !CommonHelperFunctions.getStringValue(request.getAttribute("requestId")).isEmpty()) {
			requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		}
		if (customActionUrlBody.getRequestBody() != null && customActionUrlBody.getRequestBody() instanceof Map) {
			requestBody = (Map<String, Object>) customActionUrlBody.getRequestBody();
		}else if(customActionUrlBody.getRequestBody() != null && customActionUrlBody.getRequestBody() instanceof JSONArray
						|| customActionUrlBody.getRequestBody() instanceof ArrayList){
			requestArrayBody = CommonHelperFunctions.toJson((List<Object>) customActionUrlBody.getRequestBody());
		}if (!requestId.isEmpty()) {
			if (customUrl.contains(PortalConstants.SET_VARIABLE_API_URL)) {
				customUrl = customUrl + "&requestId=" + requestId;
			} else if (customUrl.contains(PortalConstants.SUBMIT_FORM_API_URL)) {
				customUrl = customUrl + "?requestId=" + requestId;
			}
		}
		switch (customUrlMapper.getRequestType()){
			case GET:
				httpResponse = UnirestConnection.sendGET(headers, customUrl);
				break;
			case DELETE:
				httpResponse = UnirestConnection.sendDELETE(requestBody, headers, customUrl);
				break;
			case POST:
				if(customActionUrlBody.getRequestBody() instanceof Map) {
					httpResponse = UnirestConnection.sendPOST(requestBody, headers, customUrl);
				}
				else if(customActionUrlBody.getRequestBody() != null && customActionUrlBody.getRequestBody() instanceof JSONArray
						|| customActionUrlBody.getRequestBody() instanceof ArrayList){
					httpResponse = UnirestConnection.sendPOST(requestArrayBody, headers, customUrl);
				}
				break;
			case PUT:
				if(customActionUrlBody.getRequestBody() instanceof Map) {
					httpResponse = UnirestConnection.sendPUT(requestBody, headers, customUrl);
				}
				else{
					httpResponse = UnirestConnection.sendPUT(requestArrayBody, headers, customUrl);
				}
				break;
		}
		return httpResponse;
	}

	@SuppressWarnings("unchecked")
	public ApiResponse getApiResponse(CustomURLMapper customUrlMapper, CustomActionURLBody customActionUrlBody,
			Map<String, String> headers, String customUrl) {
		HttpResponse<String> httpResponse = null;
		if (StaticContextAccessor.getBean(PortalConfig.class).isAuditServiceEnabled()) {
			httpResponse = portalService.getHttpResponse(customUrlMapper,
					customActionUrlBody, headers, customUrl);
		} else {
			httpResponse = getHttpResponse(customUrlMapper, customActionUrlBody, headers, customUrl);
		}
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		if (httpResponse != null) {
			if(httpResponse.getStatus() == 200) {
			try {
				Object responseObject = CommonHelperFunctions.getHashMapFromJsonString(CommonHelperFunctions.
						getStringValue(httpResponse.getBody()));
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseObject);
			} catch (Exception e) {
//				TODO : Check for JSONArray as response
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, httpResponse.getBody());
			} }else {
			try {
				Object responseObject = CommonHelperFunctions
						.getHashMapFromJsonString(CommonHelperFunctions.
								getStringValue(httpResponse.getBody()));
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.SOMETHING_WRONG_MESSAGE, responseObject);
			} catch (Exception e) {
//				TODO : Check for JSONArray as response
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.SOMETHING_WRONG_MESSAGE,
						httpResponse.getBody());
			}
		}
		}
		return response;
	}

	@LogMethodDetails(caseInstanceId="caseInstanceId", userId="userId")
	public ApiResponse fireCustomAction(HttpServletRequest request, String slug, CustomActionURLBody customActionUrlBody, String contentType, String token,
			String caseInstanceId, String roleName, String userId) throws Exception{
		Map<String, Object> processVariables = cmmnRuntimeService.getVariables(caseInstanceId);
		validateRequestResponse(request, roleName, null, userId, processVariables);
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.CUSTOM_URL_NOT_EXISTS);
		CustomURLMapper customUrlMapper = customUrlMapperDao.findBySlugAndIsDeleted(slug, false);
		if(customUrlMapper != null){
			String url = PortalHelperFunction.setPathVariables(customUrlMapper.getUrl(), customActionUrlBody.getPathVariables());
			url = PortalHelperFunction.setRequestParams(url, customActionUrlBody.getRequestParams());
			Map<String, String> headers =  new HashMap<>();
			if(customUrlMapper.getId() < 0 && !customUrlMapper.isExternalApi()){
				url = StaticContextAccessor.getBean(PortalConfig.class).getProtocol() + "://"
						+ StaticContextAccessor.getBean(PortalConfig.class).getHost() + ":"
						+ StaticContextAccessor.getBean(PortalConfig.class).getPort()
						+ StaticContextAccessor.getBean(PortalConfig.class).getSubURL() + url;
				headers.put("Content-Type", contentType);
				if (!customUrlMapper.isOpenApi()) {
					headers.put("Authorization", token);
				}
			} else {
				for (CustomActionHeaders customActionHeader : customUrlMapper.getHeaders()) {
					headers.put(customActionHeader.getKey(), customActionHeader.getValue());
				}
				if(!customUrlMapper.isOpenApi()) {
					headers.put("Authorization", "Bearer " + portalJourneyServiceTask.getAdminAccessToken());
				}
			}
			response = getApiResponse(customUrlMapper, customActionUrlBody, headers, url);
		}
		Integer actionCount = 0;
		Map<String, Object> responseData = new HashMap<>();
		if(customActionUrlBody.getKey() != null && !customActionUrlBody.getKey().isEmpty()){
			actionCount = CommonHelperFunctions.getIntegerValue(processVariables.get(
					customActionUrlBody.getKey() + CUSTOM_ACTION_COUNT_SUFFIX));
		}
		responseData.put("actionValue", false);
		responseData.put("data", response.getData());
		if(response.getStatus() == 200){
			responseData.put("actionValue", true);
			actionCount += 1;
			if(customActionUrlBody.getKey() != null && !customActionUrlBody.getKey().isEmpty()){
				processVariables.putAll(getSuccessActionProcessVariables(responseData, customActionUrlBody.getActionMeta()));
				processVariables.put(customActionUrlBody.getKey(), true);
				processVariables.put(customActionUrlBody.getKey() + CUSTOM_ACTION_COUNT_SUFFIX, actionCount);
				processVariables.put(customActionUrlBody.getKey() + CUSTOM_ACTION_TIMESTAMP_SUFFIX, CommonHelperFunctions.getCurrentDateInFormat(COMMENT_HISTORY_DATE_FORMATE));
				processVariables.put(customActionUrlBody.getKey() + CUSTOM_ACTION_COMMENT_SUFFIX, customActionUrlBody.getMessage());
				cmmnRuntimeService.setVariables(caseInstanceId, processVariables);
			}
		} else {
			if (customActionUrlBody.getActionMeta() != null) {
				processVariables.putAll(getErrorActionProcessVariables(responseData, customActionUrlBody.getActionMeta()));
			}
		}
		responseData.put("actionCount", actionCount);
		response.setData(responseData);
		return response;
	}

@LogMethodDetails
public ApiResponse fireBulkCustomActions(HttpServletRequest request, String slug, List<CustomBulkActionURLBody> customBulkActionURLBodyList, String contentType,
		String token, String roleName, String userId)  {
	ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.ERROR_BUILDING_THREAD);
	LOGGER.info("Starting Bulk Api with slug = " + slug);
		RequestAttributes attribs = RequestContextHolder.getRequestAttributes();
		String requestId = "";
		if (request != null && !CommonHelperFunctions.getStringValue(request.getAttribute("requestId")).isEmpty()) {
			requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		}
		if (customBulkActionURLBodyList != null && customBulkActionURLBodyList.size() > 0) {
			int threadPoolSize = PortalConstants.THREAD_POOL_SIZE;
			int customBulkActionURLBodylistSize = customBulkActionURLBodyList.size();
			ExecutorService threadPool = Executors.newFixedThreadPool(threadPoolSize);
			LOGGER.info("size of custom actionUrl body = " + customBulkActionURLBodylistSize);
			CallableBulkActionUrlThread[] callableBulkActionUrlThreads = new CallableBulkActionUrlThread[customBulkActionURLBodylistSize];
			Map<String, ApiResponse> result = new HashMap<>();
			Future<ApiResponse>[] futures = new Future[customBulkActionURLBodylistSize];
			for (int i = 0; i < customBulkActionURLBodylistSize; ++i) {
				LOGGER.info("Custom action url body case instance id = " + customBulkActionURLBodyList.get(i).getCaseInstanceId());
				LOGGER.info("Custom action url body request body = " + customBulkActionURLBodyList.get(i).getCustomActionURLBody().getRequestBody());LOGGER.info("Custom action url body case instance id = " + customBulkActionURLBodyList.get(i).getCaseInstanceId());
				LOGGER.info("Custom action url body request body = " + customBulkActionURLBodyList.get(i).getCustomActionURLBody().getRequestBody());
				RequestAttributes context =
		                RequestContextHolder.currentRequestAttributes();
				callableBulkActionUrlThreads[i] = new CallableBulkActionUrlThread(request, slug, this,
						customBulkActionURLBodyList.get(i), contentType, token, context, roleName, userId);
				futures[i] = threadPool.submit(callableBulkActionUrlThreads[i]);
			}
			for (int i = 0; i < customBulkActionURLBodylistSize; i++) {
				try {
					LOGGER.info("future at index = " + i);
					LOGGER.info("timeout = " + StaticContextAccessor.getBean(PortalConfig.class).getTimeout());
					LOGGER.info("future at = " + futures[i].get().getMessage());
					result.put(customBulkActionURLBodyList.get(i).getCaseInstanceId(), futures[i].get(
							StaticContextAccessor.getBean(PortalConfig.class).getTimeout(), TimeUnit.MILLISECONDS));
				} catch (CompletionException|InterruptedException| ExecutionException| TimeoutException e) {
					e.printStackTrace();
					LOGGER.warn("exception = " + e.toString());
					result.put(customBulkActionURLBodyList.get(i).getCaseInstanceId(),
							new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, PortalConstants.THREAD_ID_FAILED + " : "
									+ customBulkActionURLBodyList.get(i).getCaseInstanceId()));
					continue;
				}
			}
			threadPool.shutdown();
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, result);
		}
		return response;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given case instance id.
	 *
	 * @param userId
	 * @param claimUnclaimInput
	 * @return ApiResponse
	 *
	 * @author Mandip Gothadiya
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse claimCase(ClaimUnclaimInput claimUnclaimInput, String userId) {
		LOGGER.info("claimCase claimUnclaimInput : " + claimUnclaimInput.toString());
		LOGGER.info("claimCase claimUnclaimInput : " + claimUnclaimInput.getRoleName() + CLAIM_VIEW_KEY);
		ApiResponse response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		try {
			String bucketKey = claimUnclaimInput.getRoleName() + "Status";
			if (!CommonHelperFunctions.getStringValue(cmmnRuntimeService.getVariable(claimUnclaimInput.getCaseInstanceId(), bucketKey)).equals(CLAIM_BUCKET_KEY)) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, ALREADY_CLAIM);
			} else {
				List<Task> activeTaskList = getTaskListListByRoleName(claimUnclaimInput.getCaseInstanceId(), claimUnclaimInput.getRoleName(), claimUnclaimInput.getRoleName());
				Map<String, Object> variablesToSave = new HashMap<>();
				if (claimUnclaimInput.getNewAssignee() == null || claimUnclaimInput.getNewAssignee()
						.isEmpty()) {
					claimUnclaimInput.setNewAssignee(userId);
				}
				if (claimUnclaimInput.getBucketKey() != null && !claimUnclaimInput.getBucketKey().isEmpty()) {
					variablesToSave
							.put(bucketKey, claimUnclaimInput.getBucketKey());
				}
				if (claimUnclaimInput.getNewAssignee() != null && !claimUnclaimInput.getNewAssignee()
						.isEmpty()) {
					variablesToSave
							.put(claimUnclaimInput.getRoleName() + ROLE_ASSIGNEE_VALUE, claimUnclaimInput.getNewAssignee());
				}
				cmmnRuntimeService.setVariables(claimUnclaimInput.getCaseInstanceId(), variablesToSave);
				for (Task task : activeTaskList) {
					cmmnTaskService.setAssignee(task.getId(), claimUnclaimInput.getNewAssignee());
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.warn("Exception in claimCase : " + exception);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		}
		LOGGER.info("claimCase response : " + response);
		return response;
	}

	@SuppressWarnings("unchecked")
	public ApiResponse unclaimCase(ClaimUnclaimInput claimUnclaimInput) {
		LOGGER.info("unclaimCase claimUnclaimInput : " + claimUnclaimInput.toString());
		LOGGER.info("unclaimCase claimUnclaimInput : " + claimUnclaimInput.getRoleName() + PortalConstants.LIST_VIEW_KEY);
		ApiResponse response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		String bucketKey = claimUnclaimInput.getRoleName() + "Status";
		if (!CommonHelperFunctions.getStringValue(cmmnRuntimeService.getVariable(claimUnclaimInput.getCaseInstanceId(), bucketKey)).equals(PortalConstants.CLAIM_BUCKET_KEY)) {
			Task listTask = cmmnTaskService.createTaskQuery().scopeType("cmmn")
					.caseInstanceId(claimUnclaimInput.getCaseInstanceId())
					.taskDefinitionKey(claimUnclaimInput.getRoleName() + PortalConstants.LIST_VIEW_KEY)
					.active().singleResult();
			claimUnclaimInput.setCurrentAssignee(listTask.getAssignee());

			List<Task> activeTaskList = getTaskListListByRoleName(claimUnclaimInput.getCaseInstanceId(), claimUnclaimInput.getRoleName(), claimUnclaimInput.getCurrentAssignee());

			Map<String, Object> variablesToSave = new HashMap<>();
			variablesToSave.put(bucketKey, PortalConstants.CLAIM_BUCKET_KEY);
			cmmnRuntimeService.setVariables(claimUnclaimInput.getCaseInstanceId(), variablesToSave);


			for (Task task : activeTaskList) {
				cmmnTaskService.setAssignee(task.getId(), claimUnclaimInput.getRoleName());
			}

		} else  {
			LOGGER.warn("This application was already unclaimed or moved to history by CMMN diagram exit condition : " +
					claimUnclaimInput.getRoleName() + "-list ======caseInstanceId : " + claimUnclaimInput
					.getCaseInstanceId());
			response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.ALREADY_UNCLAIM);
		}

		LOGGER.info("claimCase response : " + response);
		return response;
	}

	public ApiResponse reAssignCase(ClaimUnclaimInput claimUnclaimInput, String userId) {
		LOGGER.info("reassignCase claimUnclaimInput : " + claimUnclaimInput.toString());
		ApiResponse response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		if(claimUnclaimInput.getNewAssignee() != null && !claimUnclaimInput.getNewAssignee().isEmpty()){
			if(claimUnclaimInput.getCurrentAssignee() == null || claimUnclaimInput.getCurrentAssignee().isEmpty()){
				claimUnclaimInput.setCurrentAssignee(userId);
			}
			List<Task> activeTaskList = getTaskListListByRoleName(claimUnclaimInput.getCaseInstanceId(), claimUnclaimInput.getRoleName(), claimUnclaimInput.getCurrentAssignee());
			if(activeTaskList != null && activeTaskList.size() > 0){
				for (Task task : activeTaskList) {
					cmmnTaskService.setAssignee(task.getId(), claimUnclaimInput.getNewAssignee());
				}
			}else{
				response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.UNABLE_TO_REASSIGN);
			}
		}else{
			response = new ApiResponse(HttpStatus.BAD_REQUEST, PortalConstants.NEW_ASSIGNEE_NOT_EXISTS);
		}
		return response;
	}

	public List<CaseInstance> getChildCaseInstances(CaseInstance parentCaseInstance){
		List<CaseInstance> childCaseInstanceList = new ArrayList<>();
		if(parentCaseInstance != null) {
			 childCaseInstanceList = cmmnRuntimeService.createCaseInstanceQuery().
					caseInstanceParentId(parentCaseInstance.getId()).list();
		}
		return childCaseInstanceList;
	}

	public List<String> getChildCaseInstancesString(CaseInstance parentCaseInstance){
		List<CaseInstance> childCaseInstanceList = getChildCaseInstances(parentCaseInstance);
		List<String> childCaseInstanceIds = new ArrayList<>();
		if(!childCaseInstanceList.isEmpty()) {
			for (CaseInstance childCaseInstance : childCaseInstanceList) {
				childCaseInstanceIds.add(childCaseInstance.getId());
			}
		}
		return childCaseInstanceIds;
	}

	public CaseInstance getParentCaseInstance(CaseInstance childCaseInstance){
		CaseInstance parentCaseInstance = null;
		if(childCaseInstance != null) {
			String parentCaseInstanceId = childCaseInstance.getParentId();
			if (parentCaseInstanceId != null || !parentCaseInstanceId.isEmpty()) {
				parentCaseInstance = cmmnRuntimeService.createCaseInstanceQuery().
						caseInstanceId(parentCaseInstanceId).singleResult();
			}
		}
		return parentCaseInstance;
	}

	public String getParentCaseInstanceId(CaseInstance childCaseInstance){
		CaseInstance parentCaseInstance = getParentCaseInstance(childCaseInstance);
		String parentCaseInstanceId = "";
		if(parentCaseInstance != null) {
			parentCaseInstanceId = parentCaseInstance.getId();
		}
		return parentCaseInstanceId;
	}

	public ApiResponse submitParentCaseInstanceVariables(String userId, String caseInstanceId, Map<String, Object> caseVariables){
		ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		LOGGER.info("Submit Parent Case Instance");
		try {
			CaseInstance parentCaseInstance = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(caseInstanceId)
					.singleResult();
			LOGGER.info("CaseInstance Id = " + parentCaseInstance.getId());
			List<String> childCaseInstanceIdList = getChildCaseInstancesString(parentCaseInstance);
			childCaseInstanceIdList.add(caseInstanceId);
			if(!childCaseInstanceIdList.isEmpty()) {
				for (String childCaseInstanceId : childCaseInstanceIdList) {
					LOGGER.info("childCaseinstance Id = " + childCaseInstanceId);
					ApiResponse apiResponsechild = cmmnService.submitOrGetCaseVariables(userId, childCaseInstanceId, caseVariables, null);
					if (apiResponsechild.getStatus() != Constants.SUCCESS_CODE) {
						throw new Exception(apiResponsechild.getMessage());
					}
					apiResponse = apiResponsechild;
				}
			}
		}catch (Exception e){
			LOGGER.warn("Error = " + e.getMessage());
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
			e.printStackTrace();
		}
		return apiResponse;
	}

	public ApiResponse submitChildCaseInstanceVariables(String userId, String caseInstanceId, Map<String, Object> caseVariables){
		LOGGER.info("Submit child Case Instance");
		ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		try{
			CaseInstance childCaseInstance = cmmnRuntimeService.createCaseInstanceQuery().caseInstanceId(caseInstanceId).
					singleResult();
			LOGGER.info("CaseInstance Id = " + childCaseInstance.getId());
			String parentCaseInstanceId = getParentCaseInstanceId(childCaseInstance);
			if(childCaseInstance != null) {
				ApiResponse childApiResponse = cmmnService.submitOrGetCaseVariables(userId, caseInstanceId, caseVariables, null);
				if (childApiResponse.getStatus() != Constants.SUCCESS_CODE) {
					throw new Exception(childApiResponse.getMessage());
				}
				apiResponse = childApiResponse;
				LOGGER.info("Parent CaseInstance Id = " + parentCaseInstanceId);
				if (!parentCaseInstanceId.equals("")) {
					ApiResponse parentApiResponse = cmmnService.submitOrGetCaseVariables(userId, parentCaseInstanceId, caseVariables, null);
					if (parentApiResponse.getStatus() != Constants.SUCCESS_CODE) {
						throw new Exception(parentApiResponse.getMessage());
					}
					apiResponse = parentApiResponse;
				}
			}
		}catch (Exception e){
			LOGGER.warn("Error = " + e.getMessage());
			e.printStackTrace();
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return apiResponse;
	}


	public ApiResponse exportCustomActionConfiguration(String slug) {
		List<CustomURLMapper> customURLMapperList = new ArrayList<>();
		if (slug != null && !slug.isEmpty()) {
			customURLMapperList = customUrlMapperDao.findBySlug(slug, false);
		} else {
			customURLMapperList = customUrlMapperDao.findByIsDeleted(false);
		}
		ApiResponse response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
				customURLMapperList);

		return response;

	}



	public ApiResponse importCustomActionConfiguration(MultipartFile importFile) throws IOException {
		ApiResponse response = new ApiResponse(HttpStatus.OK,
				Constants.SUCCESS_MESSAGE);
		try {
			String importDataJsonString = IOUtils
					.toString(new InputStreamReader(importFile.getInputStream()));
			List<CustomURLMapper> customURLMapperList = PortalHelperFunction.getCustomURLMapperList(CommonHelperFunctions
					.getHashMapFromJsonString(importDataJsonString).get("data"));
			if (customURLMapperList.size() > 500) {
					customUrlMapperDao.save(customURLMapperList);

			}

		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		}
		return response;
	}

	/**
	 * This functions validates the object level user access.
	 *
	 * @param userId
	 * @param roleName
	 * @param processVariables
	 * @return void
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	public Boolean isValidUser(String userId, String roleName, Map<String, Object> processVariables) throws Exception {
		Boolean isValidUser = true;
		String assignedUser = CommonHelperFunctions.getStringValue(processVariables.get(roleName + ROLE_ASSIGNEE_VALUE));
		String roleNameStatus = CommonHelperFunctions.getStringValue(processVariables.get(roleName + STATUS_VARIABLE_KEY));
		if (!roleNameStatus.equals(CLAIM_BUCKET_KEY) && !userId.equals(assignedUser)){
			isValidUser = false;
		}
		return isValidUser;
	}

	/**
	 * This functions validates the object level user access for group id & roleName.
	 *
	 * @param request
	 * @param roleName
	 * @param groupId
	 * @param assignee
	 * @param processVariable
	 * @return void
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	public void validateRequestResponse(ServletRequest request, String roleName, String groupId, String assignee, Map<String, Object> processVariable) throws Exception {
		Map<String, Object> keyCloakResponse = (Map<String, Object>) keyCloakService.userGroupsWithUserNAme(request).getData();
		List<GroupRepresentation> groupList = (List<GroupRepresentation>) keyCloakResponse.get("groups");
		String userId = CommonHelperFunctions.getStringValue(keyCloakResponse.get("username"));
		Boolean isValidGroup = false;
		Boolean isValidRoleName = false;
		Boolean isValidAssignee = false;
		String  ownerRoleName;

		//check for null only as empty groupId & userId are not allowed.
		if (groupId == null) {
			isValidGroup = true;
		}
		if (assignee == null){
			isValidAssignee = true;
		}
		for (GroupRepresentation groupRepresentation: groupList) {
			ownerRoleName = groupRepresentation.getRealmRoles().get(0);
			if (!isValidGroup && groupId.equals(groupRepresentation.getId())) {
				isValidGroup = true;
				if (!ownerRoleName.equals(roleName)) {
					throw new Exception(PORTAL_USER_NOT_AUTHENTICATED);
				}
				isValidRoleName = true;
			} else if (ownerRoleName.equals(roleName)) {
				isValidRoleName = true;
			}
			if (!isValidAssignee && isValidUser(userId, roleName, processVariable)){
				isValidAssignee = true;
			}
		}
		if (!isValidRoleName || !isValidGroup || !isValidAssignee) {
			throw new Exception(PORTAL_USER_NOT_AUTHENTICATED);
		}
	}


	/**
	 * This functions validates the object level user access for group id & roleName.
	 *
	 * @param groupId
	 * @return void
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	public void validateRequestResponse(ServletRequest request, String roleName, String groupId) throws Exception {
		validateRequestResponse(request, roleName, groupId, null, null);
	}

	/*
	* To get All task id by based on roleName - for tab configuration
	* @param caseInstanceId
	* @param roleName
	* @return list of humantask's id
	*
	* @author Mandip Gothadiya
	*
	* */

	public List<String> getTaskIdListByRoleName(String caseInstanceId, String roleName) {
		List<PlanItemInstance> planItemInstanceList = cmmnRuntimeService.createPlanItemInstanceQuery().caseInstanceId(caseInstanceId).planItemInstanceStateActive().list();
		String stageInstanceId = PortalHelperFunction.getStageInstanceId(planItemInstanceList, roleName);
		return PortalHelperFunction.getPlanItemInstanceDefinitionIdByStage(planItemInstanceList, stageInstanceId);
	}

	/*
	 * To get All task id by based on roleName - for tab configuration
	 * @param caseInstanceId
	 * @param roleName
	 * @return list of humantask's id
	 *
	 * @author Mandip Gothadiya
	 *
	 * */
	public List<Task> getTaskListListByRoleName(String caseInstanceId, String roleName, String userId) {
		List<String> taskIdList = getTaskIdListByRoleName(caseInstanceId, roleName);
		List<Task> activeTaskList = cmmnTaskService.createTaskQuery().scopeType("cmmn").caseInstanceId(caseInstanceId)
				.taskAssignee(userId).active().orderByTaskPriority().asc().list();
		for (Task activeTask: activeTaskList) {
			if (!taskIdList.contains(activeTask.getTaskDefinitionKey())) {
				activeTaskList.remove(activeTask);
			}
		}
		return activeTaskList;
	}


	
//	public String allocationLogic(String portalType, String groupId) {
//		AllocationUtils.saveToAllocation(portalType, groupId, keyCloakService.groupMembers(groupId), allocationDao);
//		return AllocationUtils.getUserByRoundRobin(portalType, groupId, allocationDao);
//	}
}
