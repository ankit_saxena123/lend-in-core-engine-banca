package com.kuliza.lending.portal.aspects;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.BrokerConfigs;
import com.kuliza.lending.portal.broker.CommonListenerService;
import com.kuliza.lending.portal.broker.MessageProducerJMS;
import com.kuliza.lending.portal.broker.MessageProducerKafka;
import com.kuliza.lending.portal.broker.RabbitCustomMessageProducer;
import com.kuliza.lending.portal.utils.AuditUtils;
import com.kuliza.lending.portal.utils.EnumConstants.Events;
import com.mashape.unirest.http.HttpResponse;

@Aspect
@Component
@ConditionalOnExpression("${portal.audit.service.enabled:true}")
public class AuditAfterAdviceMethodAspect {
	@Autowired
	private AuditUtils auditUtils;

	@Autowired
	private MessageProducerJMS messageProducerJMS;

	@Autowired
	private RabbitCustomMessageProducer rabbitCustomProducer;
	
	@Autowired
	private MessageProducerKafka kafkaMessageProducer;
	
	@Autowired
	private CommonListenerService commonListenerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditBeforeAdviceAspect.class);

	@AfterReturning(value = "com.kuliza.lending.portal.aspects.AuditPointcuts.auditPortalServiceCustomMethod()", returning = "result")
	public void logAfterIntermediateRequest(JoinPoint joinPoint, Object result) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		try {
			String finalResponse = "";
			if (result != null) {
				@SuppressWarnings("unchecked")
				HttpResponse<String> response = (HttpResponse<String>) result;
				finalResponse = response.getBody();
			}
			if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
				messageProducerJMS.publishAuditEvent(Events.IncomingResponseEvent, finalResponse, requestId);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
				kafkaMessageProducer.publishAuditEvent(Events.IncomingResponseEvent, finalResponse, requestId);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")){
				rabbitCustomProducer.publishAuditEvent(Events.IncomingResponseEvent, finalResponse, requestId);
			} else {
			  commonListenerService.updateAuditEntrySync(Events.IncomingResponseEvent, finalResponse, requestId);
			}
		} catch (JsonProcessingException e) {
			String returnValue = auditUtils.getValue(result);
			LOGGER.error(" Check for after return " + returnValue);
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));

		}
	}

	@AfterThrowing(pointcut = "com.kuliza.lending.portal.aspects.AuditPointcuts.portalRestControllers()"
			+ "&& com.kuliza.lending.portal.aspects.AuditPointcuts.customAPI()", throwing = "exception")
	public void afterThrowing(JoinPoint joinPoint, Throwable exception) {
		LOGGER.error(" Check for after throw " + joinPoint.getSignature().getName());
		LOGGER.error(" Allowed execution for {}" + exception.getCause());
	}
}
