package com.kuliza.lending.portal.pojo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class TabConfig {

  String id;
  String label;
  boolean childPresent;
  LinkedHashSet<TabConfig> children;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public boolean isChildPresent() {
    return childPresent;
  }

  public void setChildPresent(boolean childPresent) {
    this.childPresent = childPresent;
  }

  public Set<TabConfig> getChildren() {
    return children;
  }

  public void setChildren(LinkedHashSet<TabConfig> children) {
    this.children = children;
  }

  public void addChildTab(TabConfig tabListConfig){
    if (this.children != null){
      this.children.add(tabListConfig);
    } else {
      this.children = new LinkedHashSet<>(Arrays.asList(tabListConfig));
    }
  }

  public TabConfig(){
    this.childPresent = false;
  }

@Override
public String toString() {
	return "TabConfig [id=" + id + ", label=" + label + ", childPresent=" + childPresent + ", children=" + children
			+ "]";
}
}
