package com.kuliza.lending.portal.aspects;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.BrokerConfigs;
import com.kuliza.lending.portal.broker.CommonListenerService;
import com.kuliza.lending.portal.broker.MessageProducerJMS;
import com.kuliza.lending.portal.broker.MessageProducerKafka;
import com.kuliza.lending.portal.broker.RabbitCustomMessageProducer;
import com.kuliza.lending.portal.utils.EnumConstants.Events;

@Aspect
@Component
@ConditionalOnExpression("${portal.audit.service.enabled:true}")
public class AuditBeforeAdviceMethodAspect {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuditBeforeAdviceAspect.class);

	@Autowired
	private MessageProducerJMS messageProducerJMS;

	@Autowired
	private RabbitCustomMessageProducer rabbitCustomProducer;
	
	@Autowired
	private MessageProducerKafka kafkaMessageProducer;
	
	@Autowired
	private CommonListenerService commonListenerService;

	@Before(value = "com.kuliza.lending.portal.aspects.AuditPointcuts.auditPortalServiceCustomMethod()")
	public void logBeforeIntermediateRequest(JoinPoint joinPoint) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		ObjectMapper Obj = new ObjectMapper();
		try {
			String jsonStr = Obj.writeValueAsString(joinPoint.getArgs());
			JSONArray jsonArray = new JSONArray(jsonStr);
			for(int i=0;i<jsonArray.length();i++){
				if(jsonArray.get(i) instanceof  JSONObject) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					if (jsonObject.has("Authorization"))
						jsonObject.remove("Authorization");
				}
			}
			String jsonAuditStr = jsonArray.toString();
			if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
				messageProducerJMS.publishAuditEvent(Events.OutgoingRequestEvent, jsonAuditStr, requestId);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
				kafkaMessageProducer.publishAuditEvent(Events.OutgoingRequestEvent, jsonAuditStr, requestId);
			} else if(StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")) {
				rabbitCustomProducer.publishAuditEvent(Events.OutgoingRequestEvent, jsonAuditStr, requestId);
			} else {
			  commonListenerService.updateAuditEntrySync(Events.OutgoingRequestEvent, jsonAuditStr, requestId);
			}
		} catch (JsonProcessingException e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}

	@Before(value = "com.kuliza.lending.portal.aspects.AuditPointcuts.setVariablesServiceCMMN()")
	public void logBeforeSetVariablesCMMN(JoinPoint joinPoint) {
		Object[] args = joinPoint.getArgs();
		MethodSignature methodSignature = (MethodSignature) joinPoint.getStaticPart().getSignature();
		String[] parameterNames = methodSignature.getParameterNames();
		Map<String, Object> functionParametersAndValues = new HashMap<String, Object>();
		for (int argIndex = 0; argIndex < args.length; argIndex++) {
			if(args[argIndex] instanceof HttpServletRequest || parameterNames[argIndex].equals("token"))
				continue;
			functionParametersAndValues.put(parameterNames[argIndex], args[argIndex]);
		}
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		if (requestId.isEmpty()) {
			requestId = CommonHelperFunctions.getStringValue(functionParametersAndValues.get("requestId"));
		}
		LOGGER.info("requestId from function params for SetVariables is " + requestId);
		try {
			if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
				messageProducerJMS.publishAuditEvent(Events.SetVariablesEvent, requestId, functionParametersAndValues);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
				kafkaMessageProducer.publishAuditEvent(Events.SetVariablesEvent, requestId,
						functionParametersAndValues);
			} else if(StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")){
				rabbitCustomProducer.publishAuditEvent(Events.SetVariablesEvent, requestId,
						functionParametersAndValues);
			} else {
			  commonListenerService.updateOrCreateAuditEntrySync(Events.SetVariablesEvent, requestId, functionParametersAndValues);
			}

		} catch (JsonProcessingException e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}

	@Before(value = "com.kuliza.lending.portal.aspects.AuditPointcuts.submitFormService()")
	public void logBeforeFormSubmitCMMN(JoinPoint joinPoint) {
		Object[] args = joinPoint.getArgs();
		MethodSignature methodSignature = (MethodSignature) joinPoint.getStaticPart().getSignature();
		String[] parameterNames = methodSignature.getParameterNames();
		Map<String, Object> functionParametersAndValues = new HashMap<String, Object>();
		for (int argIndex = 0; argIndex < args.length; argIndex++) {
			if(args[argIndex] instanceof HttpServletRequest || parameterNames[argIndex].equals("token"))
				continue;
			functionParametersAndValues.put(parameterNames[argIndex], args[argIndex]);
		}
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		if (requestId.isEmpty()) {
			requestId = CommonHelperFunctions.getStringValue(functionParametersAndValues.get("requestId"));
		}
		LOGGER.info("requestId from function params for SetVariables is " + requestId);
		try {
			if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
				messageProducerJMS.publishAuditEvent(Events.SubmitFormEvent, requestId, functionParametersAndValues);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
				kafkaMessageProducer.publishAuditEvent(Events.SubmitFormEvent, requestId, functionParametersAndValues);
			} else if(StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")){
				rabbitCustomProducer.publishAuditEvent(Events.SubmitFormEvent, requestId, functionParametersAndValues);
			} else {
			  commonListenerService.updateOrCreateAuditEntrySync(Events.SubmitFormEvent, requestId, functionParametersAndValues);
			}
		} catch (JsonProcessingException e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}
}
