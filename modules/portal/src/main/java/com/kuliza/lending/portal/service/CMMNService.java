package com.kuliza.lending.portal.service;

import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.portal.models.PortalUser;
import com.kuliza.lending.portal.models.PortalUserDao;
import com.kuliza.lending.portal.models.PortalUserSource;
import com.kuliza.lending.portal.models.PortalUserSourceDao;
import com.kuliza.lending.portal.pojo.InitiatePortalData;
import com.kuliza.lending.portal.pojo.PortalSubmitFormClass;
import com.kuliza.lending.portal.utils.PortalConstants;
import com.kuliza.lending.portal.utils.PortalHelperFunction;
import com.kuliza.lending.portal.utils.PortalValidation;
import org.flowable.cmmn.api.CmmnRepositoryService;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.CmmnTaskService;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.CaseInstanceBuilder;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.form.api.FormService;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.kuliza.lending.common.utils.Constants.*;
import static com.kuliza.lending.portal.utils.PortalConstants.PARENT_CASE_INSTANCE_ID;

@Service
public class CMMNService {

  private static final Logger LOGGER = LoggerFactory.getLogger(CMMNService.class);

  @Autowired
  private CmmnRepositoryService cmmnRepositoryService;

  @Autowired
  private CmmnRuntimeService cmmnRuntimeService;

  @Autowired
  private CmmnTaskService cmmnTaskService;

  @Autowired
  private PortalUserDao portalUserDao;

  @Autowired
  private PortalUserSourceDao portalUserSourceDao;

  @Autowired
  private FormService formService;

	/**
	 * @param caseName
	 * 
	 * @return boolean
	 * 
	 *         Checks if casename is valid case process
	 * 
	 **/
	public boolean isCaseDeployed(String caseName) {
		return cmmnRepositoryService.createCaseDefinitionQuery().caseDefinitionKey(caseName).count() > 0 ? true : false;
	}

	/**
	 * @param caseName:
	 *            Process name to instantiate
	 * @param applicationNumber:
	 *            applicationNumber will be same as per source application
	 * @param initiatePortalData:
	 *            InitiatePortalData instance that contains variables for Process
	 * 
	 * @return CaseInstance
	 * 
	 *         This method checks if caseInstance exists and then creates or updates
	 *         caseInstance if initiatePortalData is not empty
	 */
	public CaseInstance createOrUpdateCaseInstance(String caseName, String applicationNumber,
			InitiatePortalData initiatePortalData, boolean portfolioFlag) throws Exception{
		List<CaseInstance> caseInstancesList = cmmnRuntimeService.createCaseInstanceQuery().caseDefinitionKey(caseName)
				.caseInstanceBusinessKey(applicationNumber).includeCaseVariables().orderByStartTime().list();
		CaseInstance caseInstance = null;
		CaseInstance portfolioInstance = null;
		Map<String, Object> variableToSave = new HashMap<String, Object>();
		variableToSave.putAll(initiatePortalData.getVariablesToSave());
		String applicationJourneyType = CommonHelperFunctions.getStringValue(variableToSave.get(JOURNEY_TYPE));
		variableToSave.put(APPLICATION_ID_KEY, applicationNumber);
		// check whether case exists or not.
		if (caseInstancesList.isEmpty()) {
			if(portfolioFlag){
				Map<String, Object> portfolioVariables = new HashMap<String, Object>();
				if (initiatePortalData.getPortfolioVariablesToSave() != null && !initiatePortalData.getPortfolioVariablesToSave().isEmpty()){
					portfolioVariables.putAll(initiatePortalData.getPortfolioVariablesToSave());
				} else
					portfolioVariables.putAll(initiatePortalData.getVariablesToSave());
				portfolioVariables.put(Constants.JOURNEY_TYPE, "portfolio");
				initiatePortalData.setVariablesToSave(portfolioVariables);
	//			initiatePortalDataPortfolio.setVariablesToSave(portfolioVariables);
				String portfolioKey = CommonHelperFunctions.getStringValue(variableToSave.getOrDefault("portalPortfolioKey", "customerId"));
				String portfolioKeyValue = CommonHelperFunctions.getStringValue(variableToSave.get(portfolioKey));
				portfolioInstance =  createOrUpdateCaseInstance(
						caseName, portfolioKeyValue, initiatePortalData, false);
				if(portfolioInstance == null){
					throw new Exception("Exception - Portfolio instance not created");
				}
				LOGGER.info("Creating portal user for portfolio with business key - " + portfolioInstance.getBusinessKey());
				savePortalUser(caseName, portfolioKeyValue, initiatePortalData, portfolioInstance);
			}
			variableToSave.put(JOURNEY_TYPE, applicationJourneyType);
			// no existing case, create a new case for the given applicationId & caseName.
			if (portfolioFlag) {
				variableToSave.put(PORTFOLIO_ID, portfolioInstance.getBusinessKey());
				variableToSave.put(PARENT_CASE_INSTANCE_ID, portfolioInstance.getId());
			}
			CaseInstanceBuilder caseInstanceBuilder = cmmnRuntimeService.createCaseInstanceBuilder().caseDefinitionKey(caseName)
					.businessKey(applicationNumber).variables(variableToSave);
			if(portfolioFlag){
				caseInstanceBuilder.parentId(portfolioInstance.getId());
			}
			caseInstance = caseInstanceBuilder.start();
			cmmnRuntimeService.setVariable(caseInstance.getId(), CASE_INSTANCE_ID_KEY, caseInstance.getId());
			cmmnRuntimeService.setVariable(caseInstance.getId(), CASE_DEFINITION_ID_KEY, caseInstance.getCaseDefinitionId());
			LOGGER.info("creating new instance " + caseInstance.getBusinessKey());
		} else {
			// If application with same businesskey exists the just update the variables
			// else create new one
			caseInstance = caseInstancesList.get(0);
			if (!variableToSave.isEmpty()) {
				cmmnRuntimeService.setVariables(caseInstance.getId(), variableToSave);
				LOGGER.info("updating variables in existing instance " + caseInstance.getBusinessKey());
			} else {
				LOGGER.info("returning existing instance " + caseInstance.getBusinessKey());
			}
		}
		return caseInstance;
	}

	/**
	 * @param caseName:
	 *            Process name to check for user
	 * @param applicationNumber:
	 *            applicationNumber used to check for user
	 * @param initiatePortalData:
	 *            InitiatePortalData instance that contains variables for Process
	 * @param caseInstance:
	 *            caseInstance initiated for this user
	 * 
	 * @return
	 * 
	 * 		This method saves the data in database
	 * 
	 **/

	public void savePortalUser(String caseName, String applicationNumber, InitiatePortalData initiatePortalData,
			CaseInstance caseInstance) {
		PortalUser portalUser = portalUserDao.findByApplicationNumberAndCaseName(
				applicationNumber, caseName);
		if (portalUser == null) {
			portalUser = new PortalUser(applicationNumber, caseInstance.getId(),
					caseName, initiatePortalData.getPortalType());
			portalUserDao.save(portalUser);
			cmmnRuntimeService.setVariable(caseInstance.getId(), portalUser.getPortalType() + "PortalId", portalUser.getId());
			LOGGER.info("Saved the portalUser");
		}

		PortalUserSource portalUserSource = portalUserSourceDao
				.findByPortalUserAndSourceTypeAndSourceId(portalUser, initiatePortalData.getSourceType(), initiatePortalData.getSourceId());
		if (portalUserSource == null) {
			LOGGER.info("savePortalUser initiatePortalData.getSourceProcessName() : " + initiatePortalData.getSourceProcessName());
			portalUserSource = new PortalUserSource(portalUser, initiatePortalData.getSourceProcessName(), initiatePortalData.getSourceType(), initiatePortalData.getSourceId());
			portalUserSourceDao.save(portalUserSource);
		}
	}

	/**
	 * @param caseName:
	 *            Process name to instantiate
	 * @param applicationNumber:
	 *            applicationNumber will be same as per source application
	 * @param userId
	 * @param initiatePortalData:
	 *            InitiatePortalData instance that contains variables for Process
	 * 
	 * @return ApiResponse
	 * 
	 *         Checks if caseName is valid ProcessName and then checks if PortalUser
	 *         is there. Then calls methods to create or update portal user and
	 *         caseInstance
	 * 
	 **/
	@LogMethodDetails(businessKey = "applicationNumber", userId = "userId")
	public ApiResponse startOrResumeCase(String caseName, String applicationNumber, String userId,
			InitiatePortalData initiatePortalData, boolean portfolioFlag) {
		LOGGER.info("startOrResumeCase initiatePortalData.getSourceProcessName()" + initiatePortalData.getSourceProcessName());
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
				Constants.NO_CASE_DEPLOYED_FOR_GIVEN_KEY_MESSAGE);
		CaseInstance caseInstance = null;
		Map<String, Object> dataToReturn = new HashMap<>();
		try {
			LOGGER.info("User with Id {}, applicationNumber {}, caseName {}", userId, applicationNumber, caseName) ;
			// Checking whether if any case with the given caseName has been deployed or
			// not.
			if (isCaseDeployed(caseName)) {
				// Case is deployed based on the given caseDefinition key.
				// fetching the case instance for the given user details with the given
				// caseName.
				caseInstance = createOrUpdateCaseInstance(caseName, applicationNumber, initiatePortalData, portfolioFlag);
				savePortalUser(caseName, applicationNumber, initiatePortalData, caseInstance);
				LOGGER.info("returning caseInstance " + caseInstance.getBusinessKey());

				dataToReturn.put("caseInstanceId", caseInstance.getId());
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, dataToReturn);
				if (userId != null) {
					response = getTaskData(userId, caseInstance.getId(), HttpStatus.BAD_REQUEST,
							Constants.NO_TASKS_MESSAGE);
				}
			}
			LOGGER.info(PortalConstants.LOG_RESPONSE_USER_CASEINSTANCE, userId, caseInstance, response);
		} catch (Exception e) {
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
			LOGGER.warn(PortalConstants.LOG_EXCEPTION_USER_APPLICATION, userId, applicationNumber, e.getMessage());
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

  /**
   * This functions return the current task data For the given user associated
   * with given case instance id.
   *
   * @param userId
   * @param caseInstanceId
   * @param errorcode
   * @param errorMessage
   * @return ApiResponse
   * @throws Exception
   *
   * @author Mandip Gothadiya
   */
  @SuppressWarnings("unchecked")
  public ApiResponse getTaskData(String userId, String caseInstanceId, HttpStatus errorcode,
      String errorMessage) throws Exception {

    ApiResponse response;

    List<Task> activeTaskList = cmmnTaskService.createTaskQuery().caseInstanceId(caseInstanceId).
        taskAssignee(userId).active().orderByTaskCreateTime().desc().list();
    if (activeTaskList.size() < 1) {
      response = new ApiResponse(errorcode, errorMessage);
    } else {
        // TODO: optimize this as much as possible
      CaseInstance caseInstance = cmmnRuntimeService.createCaseInstanceQuery().
          caseInstanceId(caseInstanceId).singleResult();

      Map<String, Object> data = new HashMap<>();
      data.put(Constants.LOAN_APPLICATION_ID_KEY, caseInstance.getBusinessKey());

        SimpleFormModel formModel = (SimpleFormModel) cmmnTaskService.getTaskFormModel(activeTaskList.get(0).getId())
            .getFormModel();
        Map<String, FormField> formFields = formModel.allFieldsAsMap();
        response = PortalHelperFunction.makeResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
            caseInstance.getId(), activeTaskList.get(0), data, formFields);
      }
    return response;
  }

  @LogMethodDetails(userId = "userId")
  public ApiResponse submitFormData(String userId, PortalSubmitFormClass input, boolean isPartialSubmit) {
    ApiResponse response;
    try {
      Task task = cmmnTaskService.createTaskQuery().taskAssignee(userId).active().taskId(input.getTaskId())
          .singleResult();
      if (task == null) {
        response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
      } else {
        if (!task.getScopeId().equals(input.getCaseInstanceId())) {
          response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
        } else {
          SimpleFormModel formModel = (SimpleFormModel) cmmnTaskService.getTaskFormModel(task.getId())
              .getFormModel();
          List<FormField> listOfFormProp = formModel.getFields();
          Map<String, Object> formPropertiesMap = (!input.getFormProperties().isEmpty()
              ? input.getFormProperties()
              : new HashMap<String, Object>());
          Map<Object, Object> validationResult = PortalValidation.submitValidation(listOfFormProp,
              formPropertiesMap, true);
          if (!(boolean) validationResult.get("status")) {
            response = new ApiResponse(HttpStatus.BAD_REQUEST,
                validationResult.get(Constants.MESSAGE_KEY).toString());
          } else {
            cmmnRuntimeService.setVariables(task.getScopeId(), formPropertiesMap);
            if (!isPartialSubmit) {
              cmmnTaskService.complete(input.getTaskId());
            }
            response = getTaskData(userId, input.getCaseInstanceId(), HttpStatus.BAD_REQUEST,
                Constants.NO_TASKS_MESSAGE);
          }
        }
      }
			LOGGER.info(PortalConstants.LOG_RESPONSE_USER_CASEINSTANCE, userId, input.getCaseInstanceId(), response);
    } catch (Exception e) {
			LOGGER.warn(PortalConstants.LOG_EXCEPTION_USER_CASEINSTANCE, userId, input.getCaseInstanceId(), e.getMessage());
      response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
    }
    return response;

  }


  @LogMethodDetails(caseInstanceId = "caseInstanceId", userId = "userId")
  public ApiResponse submitOrGetCaseVariables(String userId, String caseInstanceId, Map<String, Object> caseVariables, String requestId) {
    ApiResponse response;
    try {
      Object responseObject = caseVariables;
      if (caseVariables == null) {
        responseObject = cmmnRuntimeService.getVariables(caseInstanceId);
      } else {
        cmmnRuntimeService.setVariables(caseInstanceId, caseVariables);
      }
      response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseObject);
      LOGGER.info(PortalConstants.LOG_RESPONSE_USER_CASEINSTANCE,userId, caseInstanceId,response);
    } catch (Exception e) {
      LOGGER.warn(PortalConstants.LOG_EXCEPTION_USER_CASEINSTANCE, userId, caseInstanceId, e.getMessage());
      response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e.getMessage());
    }
    return response;

  }

//  public ApiResponse clearData(String userId, List<ClearDataInput> caseInstanceList) {
//    ApiResponse response;
//    try {
//      try {
//        CustomLogger.log(userId, null, LogType.INBOUND_REQUEST_BODY,
//            JobType.INBOUND_API, null, null, caseInstanceList);
//      } catch (JsonProcessingException e) {
//        CustomLogger.logException(userId, null, LogType.INBOUND_REQUEST_BODY,
//            JobType.INBOUND_API, null, null, e, caseInstanceList);
//      }
//
//      Object responseObject = caseVariables;
//      if (caseVariables == null) {
//        responseObject = cmmnRuntimeService.getVariables(caseInstanceId);
//      } else {
//        cmmnRuntimeService..setVariables(caseInstanceId, caseVariables);
//      }
//      response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseObject);
//
//      try {
//        CustomLogger.log(userId, null, LogType.INBOUND_RESPONSE_BODY,
//            JobType.INBOUND_API, null, null, response);
//      } catch (JsonProcessingException e) {
//        CustomLogger.logException(userId, null, LogType.INBOUND_RESPONSE_BODY,
//            JobType.INBOUND_API, null, null, e, response);
//      }
//    } catch (Exception e) {
//      CustomLogger.logException(userId, caseInstanceId, LogType.EXCEPTION, JobType.INBOUND_API,
//          null, null, e, caseVariables);
//      response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
//    }
//    return response;
//
//  }
  
  public Execution sendApplicationToPortal(Execution execution) {
	  return execution;
	  
  }
  
  public Execution sendApplicationToJourney(Execution execution) {
	  return execution;
	  
  }

  public SimpleFormModel getSimpleFormModel(String key, Map<String, Object> processVariables) {
  	return ((SimpleFormModel) formService.getFormModelWithVariablesByKey(key, "", processVariables).getFormModel());
  }

  public SimpleFormModel getSimpleFormModel(Task task, Map<String, Object> processVariables) {
  	SimpleFormModel simpleFormModel = null;
  	if (task.getFormKey() != null && !task.getFormKey().isEmpty()) {
		simpleFormModel = getSimpleFormModel(task.getFormKey(), processVariables);
  	} else {
		simpleFormModel = (SimpleFormModel) cmmnTaskService
				.getTaskFormModel(task.getId()).getFormModel();
	}
  	return simpleFormModel;
  }


}
