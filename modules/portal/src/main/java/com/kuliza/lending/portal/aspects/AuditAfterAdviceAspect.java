package com.kuliza.lending.portal.aspects;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.engine_common.configs.BrokerConfigs;
import com.kuliza.lending.portal.broker.CommonListenerService;
import com.kuliza.lending.portal.broker.MessageProducerJMS;
import com.kuliza.lending.portal.broker.MessageProducerKafka;
import com.kuliza.lending.portal.broker.RabbitCustomMessageProducer;
import com.kuliza.lending.portal.utils.AuditUtils;
import com.kuliza.lending.portal.utils.EnumConstants.Events;

@Aspect
@Component
@ConditionalOnExpression("${portal.audit.service.init.enabled:true}")
public class AuditAfterAdviceAspect {
	@Autowired
	private AuditUtils auditUtils;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MessageProducerJMS messageProducerJMS;

	@Autowired
	private RabbitCustomMessageProducer rabbitCustomProducer;
	
	@Autowired
	private MessageProducerKafka kafkaMessageProducer;
	
	@Autowired
	private CommonListenerService commonListenerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditBeforeAdviceAspect.class);

	@AfterReturning(pointcut = "com.kuliza.lending.portal.aspects.AuditPointcuts.combinedAuditAPIs()", returning = "result")
	public void afterReturning(JoinPoint joinPoint, Object result) {
		LOGGER.debug("Exiting in Method : " + joinPoint.getSignature());
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String requestId = CommonHelperFunctions.getStringValue(request.getAttribute("requestId"));
		if (requestId.isEmpty()) {
			return;
		}
		try {
			String finalResponse = objectMapper.writeValueAsString(result);
			if (StaticContextAccessor.getBean(BrokerConfigs.class).isJmsEnable()) {
				messageProducerJMS.publishAuditEvent(Events.OutgoingResponseEvent, finalResponse, requestId);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("KAFKA")) {
				kafkaMessageProducer.publishAuditEvent(Events.OutgoingResponseEvent, finalResponse, requestId);
			} else if (StaticContextAccessor.getBean(BrokerConfigs.class).getBrokerName().equals("RABBITMQ")) {
				rabbitCustomProducer.publishAuditEvent(Events.OutgoingResponseEvent, finalResponse, requestId);
			} else {
			  commonListenerService.updateAuditEntrySync(Events.OutgoingResponseEvent, finalResponse, requestId);
			}
		} catch (JsonProcessingException e) {
			String returnValue = auditUtils.getValue(result);
			LOGGER.error(" Check for after return " + returnValue);
			LOGGER.error(CommonHelperFunctions.getStackTrace(e));
		}
	}

	@AfterThrowing(pointcut = "(com.kuliza.lending.portal.aspects.AuditPointcuts.customAPI() || "
			+ "com.kuliza.lending.portal.aspects.AuditPointcuts.submitVariablesAPI())", throwing = "exception")
	public void afterThrowing(JoinPoint joinPoint, Throwable exception) {
		LOGGER.error(" Check for after throw " + joinPoint.getSignature().getName());
		LOGGER.error(" Allowed execution for {}" + exception.getCause());
	}

}
