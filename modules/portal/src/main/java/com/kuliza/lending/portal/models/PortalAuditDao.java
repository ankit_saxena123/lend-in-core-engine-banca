package com.kuliza.lending.portal.models;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public interface PortalAuditDao extends JpaRepository<PortalAudit, Long>{
	PortalAudit findByRequestId(String requestId);

}
