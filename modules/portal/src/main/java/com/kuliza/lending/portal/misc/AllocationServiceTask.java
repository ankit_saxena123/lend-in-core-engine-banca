package com.kuliza.lending.portal.misc;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.portal.models.AllocationDao;
import com.kuliza.lending.portal.utils.AllocationUtils;
import com.kuliza.lending.portal.utils.PortalConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("AllocationServiceTask")
public class AllocationServiceTask {

	@Autowired
	private KeyCloakService keyCloakService;

	@Autowired
	private AllocationDao allocationDao;

	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;


	public String allocationLogic(String caseId, String portalType, String allocationType, String groupNames,
			String rolesToAssign) throws IOException, InterruptedException, Exception {
		Map<String, Object> processVariables = cmmnRuntimeService.getVariables(caseId);
		try {
			String assignee = null;
			// TODO: Take this variable from applications.properties
			Boolean isCache = false;
			if (isCache) {
				// TODO: Cache Implementation
			} else {
				if (!CommonHelperFunctions.getStringValue(processVariables.get(PortalConstants.ROLE_PROCESS_VARIABLE_KEY)).equals(
				CommonHelperFunctions.getStringValue(processVariables.get(PortalConstants.LAST_ROLE_PROCESS_VARIABLE_KEY))
				)) {  
					if (groupNames == null || "".equals(groupNames)) {
						groupNames = CommonHelperFunctions.getStringValue(processVariables.get(PortalConstants.GROUP_NAME_PROCESS_VARIABLE_KEY));
					}
					if (rolesToAssign == null || "".equals(rolesToAssign)) {
						rolesToAssign = CommonHelperFunctions
								.getStringValue(processVariables.get(PortalConstants.ROLE_PROCESS_VARIABLE_KEY));
					}
					String[] groupNameList = groupNames.split(",");
					List<String> groupIds = new ArrayList<String>();
					for (String groupName : groupNameList) {
						GroupRepresentation group = keyCloakService.getGroupByName(groupName);
						if (group != null) {
							groupIds.add(group.getId());
						} else {
							throw new InterruptedException(
									"Group Id not found in Allocation Logic for group: " + groupName);
						}
					}

					AllocationUtils.saveToAllocation(portalType, groupIds, allocationDao, keyCloakService);
					switch (allocationType) {
					case "manual":
						// TODO: Logic for manual allocation
						break;
					default:
						assignee = AllocationUtils.getUserByRoundRobin(portalType, groupIds, allocationDao);
						break;
					}
				}
			}
			if (rolesToAssign != null && !"".equals(rolesToAssign)) {
				for (String role : rolesToAssign.split(",")) {
					cmmnRuntimeService.setVariable(caseId, role + PortalConstants.ROLE_ASSIGNEE_VALUE, assignee);
				}
			}
			return assignee;

		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedException("Error in Allocation Logic.");
		}
	}

}
