package com.kuliza.lending.portal.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.ThreadContext;
import javax.transaction.Transactional;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.models.CustomURLMapper;
import com.kuliza.lending.engine_common.models.CustomURLMapperDao;
import com.kuliza.lending.portal.exceptions.AuditNotFoundException;
import com.kuliza.lending.portal.filter.WrappedRequest;
import com.kuliza.lending.portal.models.*;
import com.kuliza.lending.portal.pojo.CustomMessageDetails;
import com.kuliza.lending.portal.pojo.CustomMessageVariables;
import com.kuliza.lending.portal.pojo.CustomUpdateMessage;
import com.kuliza.lending.portal.pojo.PortalSubmitFormClass;
import com.kuliza.lending.portal.utils.AuditUtils;
import com.kuliza.lending.portal.utils.EnumConstants.Events;
import com.kuliza.lending.portal.utils.PortalConstants;

@Service
public class AuditService {

	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	CustomURLMapperDao customUrlMapperDao;

	@Autowired
	PortalCommentHistoryDao portalCommentHistoryDao;

	@Autowired
	PortalUserDao portalUserDao;

	@Autowired
	PortalAuditDao portalAuditDao;

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

	public CustomURLMapper getURLMapperUsingId(String slug) {
		if(slug==null || slug.isEmpty())
			return null;
		CustomURLMapper url = customUrlMapperDao.findBySlugAndIsDeleted(slug, false);
		return url;
	}

	public PortalUser getPortalUserUsingCase(String caseInstanceId) {
		PortalUser portalUser = portalUserDao.findByCaseInstanceIdAndIsDeleted(caseInstanceId, false);
		return portalUser;
	}

	public PortalCommentHistory createPortalComment(String requestBody, PortalUser portalUser, String userId,
			String roleName, String tabKey) throws JSONException{
		PortalCommentHistory portalComment = null;
		if (requestBody != null && !requestBody.isEmpty()) {
			JSONObject requestJson = null;
			JSONArray jsonArray = null;
			JSONObject commentBody = null;
			if(requestBody.startsWith("["))
				jsonArray = new JSONArray(requestBody);
			else
				requestJson = new JSONObject(requestBody);
			if (requestJson!= null && requestJson.has(PortalConstants.COMMENT_MESSAGE_KEY)) {
				 commentBody = requestJson;
			}
			else if(jsonArray!=null && jsonArray.getJSONObject(0).has(PortalConstants.COMMENT_MESSAGE_KEY
			)) {
				commentBody = jsonArray.getJSONObject(0);
			}
			if(commentBody!=null){
				if(commentBody.has(PortalConstants.COMMENT_MESSAGE_KEY)) {
					String message = commentBody.optString(PortalConstants.COMMENT_MESSAGE_KEY, "");
					String commentType = PortalConstants.AUDIT_COMMENT_IDENTIFIER;
					portalComment = new PortalCommentHistory(portalUser, message, userId, roleName, tabKey, commentType);
					portalComment = portalCommentHistoryDao.save(portalComment);
				}
			}
		}
		return portalComment;
	}

	/**
	 * This function is called for the first time entry to Audit table for a
	 * particular request
	 **/
	@LogMethodDetails
	public PortalAudit saveAuditEntry(CustomMessageDetails incomingRequestDetails) throws JSONException{
		LOGGER.info("publish event execution for " + incomingRequestDetails.getRequestId());
		String requestBody = incomingRequestDetails.getRequestBody();
		String parameters = incomingRequestDetails.getParameters();
		String attributes = incomingRequestDetails.getAttributes();

		String requestId = incomingRequestDetails.getRequestId();
		String roleName = incomingRequestDetails.getRoleName();
		String userId = incomingRequestDetails.getUserId();
		String requestURI = incomingRequestDetails.getUri();
		String caseInstanceId = incomingRequestDetails.getCaseInstanceId();
		String tabKey = incomingRequestDetails.getTabKey();
		CustomURLMapper url = getURLMapperUsingId(incomingRequestDetails.getSlug());

		PortalUser portalUser = getPortalUserUsingCase(caseInstanceId);

		PortalCommentHistory portalComment = createPortalComment(requestBody, portalUser, userId, roleName, tabKey);

		PortalAudit portalAudit = new PortalAudit(caseInstanceId, tabKey, userId, roleName, url, requestURI,
				portalComment, requestBody, requestId, parameters, attributes, portalUser);
		portalAuditDao.save(portalAudit);
		return portalAudit;
	}


	String getIntermediateDataToSave(String data, String intermediateData){
		int len = 0;
		JSONObject jsonObject = new JSONObject();
		if(intermediateData!=null && !intermediateData.isEmpty()){
			jsonObject = new JSONObject(intermediateData);
			len = jsonObject.keySet().size();
		}
		jsonObject.put(CommonHelperFunctions.getStringValue(len+1), data);
		return jsonObject.toString();
	}

	/***
	 * This function is called to update the Audit Table with response
	 * 
	 * @throws AuditNotFoundException
	 **/
	@Transactional
	@LogMethodDetails
	public PortalAudit updateAuditEntry(CustomUpdateMessage customMessage) throws AuditNotFoundException {
		String requestId = customMessage.getRequestId();
		String data = customMessage.getData();
		Events event = customMessage.getEvent();
		PortalAudit portalAudit = portalAuditDao.findByRequestId(requestId);
		if (portalAudit == null) {
			LOGGER.error(
					"Audit instance not found for requestId: " + requestId + " event::::: " + customMessage.getEvent());
			throw new AuditNotFoundException("Reque as Audit instance not found");
		}
		entityManager.refresh(portalAudit);
		LOGGER.info("publish update event execution for " + requestId);
		switch (event) {
		case OutgoingRequestEvent:
			portalAudit.setIntermediateRequestBody(getIntermediateDataToSave(data,
					portalAudit.getIntermediateRequestBody()));
			portalAudit.setStatus(2);
			break;
		case IncomingResponseEvent:
			portalAudit.setIntermediateResponseBody(getIntermediateDataToSave(data,
					portalAudit.getIntermediateResponseBody()));
			portalAudit.setStatus(3);
			break;
		case OutgoingResponseEvent:
			portalAudit.setResponseBody(data);
			portalAudit.setStatus(4);
			break;
		default:
			break;
		}
		portalAuditDao.save(portalAudit);
		return portalAudit;
	}

	public static String getValueFromParameters(JSONObject parameters, String key) {
		String value = parameters.optString(key, "");
		if (value != null && !value.isEmpty()) {
			value = value.substring(2, value.length() - 2);
		}
		return value;
	}

	/**
	 * Fetches the data from the request and Returns instance of
	 * CustomMessageDetails for Audit Purposes
	 * 
	 * @param event
	 *            Event that we are trying to log
	 * @param request
	 *            Incoming HttpRequest
	 * @param jsonRequestStr
	 *            RequestBody in case of Post Request
	 * 
	 **/
	public CustomMessageDetails getCustomAuditMessageDetails(Events event, HttpServletRequest request,
			String jsonRequestStr) throws JSONException{
		String userId = request.getUserPrincipal().getName();
		String url = request.getServletPath();
		String requestMethod = request.getMethod();
		String uri = request.getRequestURI();

		Map<String, String[]> parameterMap = request.getParameterMap();
		JSONObject parameters = CommonHelperFunctions.mapToJson(parameterMap);
		Map<String, Object> attributeMap = AuditUtils.getAttributes(request);
		JSONObject attributes = CommonHelperFunctions.toJson(attributeMap);
		String roleName = AuditUtils.getPathVariable(attributeMap, PortalConstants.ROLE_NAME_KEY);
		String customUrlSlug = AuditUtils.getPathVariable(attributeMap, "slug");
		if (parameterMap != null && roleName.isEmpty() && parameterMap.containsKey(PortalConstants.ROLE_NAME_KEY)) {
			roleName = parameterMap.get(PortalConstants.ROLE_NAME_KEY)[0];
		}

		String caseInstanceId = "";
		String tabKey = "";
		String transactionId="";
		Map<String, String> threadContext = ThreadContext.getContext();
		if(threadContext!=null && threadContext.containsKey("transactionId"))
			transactionId = ThreadContext.get("transactionId");
		if (parameters != null) {
			caseInstanceId = getValueFromParameters(parameters, PortalConstants.CASE_INSTANCE_ID);
			tabKey = getValueFromParameters(parameters, PortalConstants.TAB_KEY);
		}

		if(caseInstanceId.isEmpty() && jsonRequestStr!=null && !jsonRequestStr.isEmpty()){
			caseInstanceId = getCaseInstanceId(jsonRequestStr);
		}

		String requestId = attributes.optString(PortalConstants.REQUEST_ID_KEY, "");

		final CustomMessageDetails customMessageDetails = new CustomMessageDetails(event,
				CommonHelperFunctions.getStringValue(attributes), CommonHelperFunctions.getStringValue(parameters),
				roleName, userId, uri, url, requestMethod, caseInstanceId, tabKey,
				jsonRequestStr, requestId, customUrlSlug, transactionId);

		return customMessageDetails;
	}


	private String getCaseInstanceId(String jsonRequestStr){
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		String caseInstanceId = "";
		if(jsonRequestStr.startsWith("["))
			jsonArray = new JSONArray(jsonRequestStr);
		else
			jsonObject = new JSONObject(jsonRequestStr);
		if (jsonObject != null && jsonObject.has(PortalConstants.CASE_INSTANCE_ID)) {
			caseInstanceId = jsonObject.getString(PortalConstants.CASE_INSTANCE_ID);
		}
		else if(jsonArray!=null){
			int jsonLength = jsonArray.length();
			for(int i=0;i<jsonLength;i++) {
				JSONObject jsonRequestObj = new JSONObject(jsonArray.getJSONObject(i));
				caseInstanceId = caseInstanceId + ", " + jsonRequestObj.getString(PortalConstants.CASE_INSTANCE_ID);
			}
		}
		return caseInstanceId;
	}

	/**
	 * Fetches the data from the request and Returns instance of
	 * CustomMessageDetails for Audit Purposes
	 **/
	public CustomMessageDetails getCustomAuditMessageDetailsFilter(Events event, WrappedRequest request) throws JSONException {
		String jsonRequestStr = "";
		if (request.getMethod().equalsIgnoreCase("POST")) {
			jsonRequestStr = request.getBody();
		}
		String userId = request.getUserPrincipal().getName();
		String url = request.getServletPath();
		String requestMethod = request.getMethod();
		String uri = request.getRequestURI();

		Map<String, Object> attributeMap = AuditUtils.getAttributes(request);
		JSONObject attributes = CommonHelperFunctions.toJson(attributeMap);
		String roleName = AuditUtils.getPathVariable(attributeMap, PortalConstants.ROLE_NAME_KEY);
		String customUrlSlug = AuditUtils.getPathVariable(attributeMap, "slug");

		Map<String, Object> parameterMap = AuditUtils.getParameters(request);
		if ((roleName == null || roleName.isEmpty()) && parameterMap != null) {
			roleName = (String) parameterMap.get(PortalConstants.ROLE_NAME_KEY);
		}
		if (customUrlSlug == null || customUrlSlug.isEmpty()) {
			customUrlSlug = AuditUtils.getMapperUrlFilter(request);
		}

		JSONObject parameters = CommonHelperFunctions.toJson(parameterMap);

		String caseInstanceId = "";
		String tabKey = "";
		String transactionId = "";
		CustomMessageVariables message = null;
		Map<String, String> threadContext = ThreadContext.getContext();
		if(threadContext!=null && threadContext.containsKey("transactionId"))
			transactionId = ThreadContext.get("transactionId");
		if (parameters != null) {
			caseInstanceId = parameters.optString(PortalConstants.CASE_INSTANCE_ID);
			tabKey = parameters.optString(PortalConstants.TAB_KEY, "");
		}
		if (caseInstanceId.isEmpty()) {
			caseInstanceId = getCaseInstanceId(jsonRequestStr);
		}
		String requestId = attributes.optString(PortalConstants.REQUEST_ID_KEY);

		final CustomMessageDetails customMessageDetails = new CustomMessageDetails(event,
				CommonHelperFunctions.getStringValue(attributes), CommonHelperFunctions.getStringValue(parameters),
				roleName, userId, uri, url, requestMethod, caseInstanceId, tabKey,
				jsonRequestStr, requestId, customUrlSlug, transactionId);

		return customMessageDetails;
	}

	/**
	 * Adds entry in portal audit table for oldMap and newMap of variables.
	 * 
	 * @param oldMap
	 *            Map<String, Object> contains variables which already existed and
	 *            now has been updated
	 * @param newVariableMap
	 *            Map<String, Object> that contains new Variables added
	 * @throws AuditNotFoundException
	 **/
	public PortalAudit saveVariablesAudit(Map<String, Object> oldMap, Map<String, Object> newVariableMap, String requestId)
			throws AuditNotFoundException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonFromOldMap = "";
		String jsonFromNewMap = "";
		try {
			if (oldMap != null && !oldMap.isEmpty())
				jsonFromOldMap = mapper.writeValueAsString(oldMap);
			if (newVariableMap != null && !newVariableMap.isEmpty())
				jsonFromNewMap = mapper.writeValueAsString(newVariableMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		PortalAudit portalAudit = portalAuditDao.findByRequestId(requestId);
		if (portalAudit == null) {
			LOGGER.error("Audit instance not found for requestId: " + requestId);
			throw new AuditNotFoundException("Reque as Audit instance not found");
		}
		portalAudit.setOldProcessVariables(jsonFromOldMap);
		portalAudit.setNewProcessVariables(jsonFromNewMap);
		portalAuditDao.save(portalAudit);
		return portalAudit;
	}

	/**
	 * Checks which variables have been updated or added in case of submit variables
	 * 
	 * @param caseInstanceId
	 *            CaseInstanceId used to fetch the existing variables
	 * @param newVariableMap
	 *            Map<String, Object> that contains new Variables added
	 * @throws AuditNotFoundException
	 **/
	public PortalAudit getAndSaveChangedVariables(String caseInstanceId, Map<String, Object> newVariableMap, String requestId)
			throws AuditNotFoundException {
		if (newVariableMap != null && !newVariableMap.isEmpty()) {
			Map<String, Object> existingVariables = cmmnRuntimeService.getVariables(caseInstanceId);
			Map<String, Object> oldMap = new HashMap<>();
			for (Entry<String, Object> entry : newVariableMap.entrySet()) {
				String key = entry.getKey();
				Object oldValue = existingVariables.getOrDefault(key, null);
				oldMap.put(key, oldValue);
			}
			return saveVariablesAudit(oldMap, newVariableMap, requestId);
		}
		return null;
	}

	/**
	 * Intercepted code for submit or partial submit, that saves the process
	 * variable changes in audit
	 *
	 * @param caseInstanceId
	 *            CaseInstanceId of application for which submitForm has been called
	 * @param newVariables
	 *            Form submitted by user in case of partial submit or submit
	 * @throws AuditNotFoundException
	 **/
	public void getAndSaveFormVariables(String caseInstanceId, PortalSubmitFormClass newVariables, String requestId)
			throws AuditNotFoundException {
		if (newVariables != null) {
			Map<String, Object> newVariableMap = newVariables.getFormProperties();
			getAndSaveChangedVariables(caseInstanceId, newVariableMap, requestId);
		}
	}

	@LogMethodDetails
	@SuppressWarnings("unchecked")
	public PortalAudit updateOrCreateAuditEntry(CustomMessageVariables customMessage) throws AuditNotFoundException {
		String requestId = customMessage.getRequestId();
		Map<String, Object> functionArguments = customMessage.getFunctionArguments();
		if (functionArguments != null && !functionArguments.isEmpty()) {
			if (Events.SetVariablesEvent.equals(customMessage.getEvent())) {
				String caseInstanceId = CommonHelperFunctions
						.getStringValue(functionArguments.getOrDefault(PortalConstants.CASE_INSTANCE_ID, ""));
				Map<String, Object> data = (Map<String, Object>) functionArguments.getOrDefault("caseVariables",
						new HashMap<>());
				return getAndSaveChangedVariables(caseInstanceId, data, requestId);
			} else {
				// SubmitFormEvent
				Object formObject = functionArguments.get("input");
				if (formObject != null) {
					if (formObject instanceof Map) {
						Map<String, Object> data = (Map<String, Object>) formObject;
						Map<String, Object> variables = (Map<String, Object>) data.get("formProperties");
						String caseInstanceId = CommonHelperFunctions
								.getStringValue(data.get(PortalConstants.CASE_INSTANCE_ID));
						return getAndSaveChangedVariables(caseInstanceId, variables, requestId);
					}
				}
			}

		} else {
			LOGGER.error("Error in invocation of Submit");
		}
		return null;
	}
}
