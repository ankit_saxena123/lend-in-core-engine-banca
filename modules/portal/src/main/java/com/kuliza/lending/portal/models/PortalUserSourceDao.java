package com.kuliza.lending.portal.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PortalUserSourceDao extends CrudRepository<PortalUserSource, Long> {

  public PortalUserSource findByPortalUserAndSourceTypeAndSourceId(PortalUser portalUser,
      String sourceType, String sourceId);

  public PortalUserSource findBySourceProcessNameAndSourceTypeAndPortalUserCaseInstanceId(String sourceProcessName,
      String sourceType, String sourceId);
}
