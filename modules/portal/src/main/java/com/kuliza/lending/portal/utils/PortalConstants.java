package com.kuliza.lending.portal.utils;

import java.util.HashMap;
import java.util.Map;

public class PortalConstants {
	// **************** Master URLs *************************
	public static final String PORTAL_API_URL = "/lending/portal";
	public static final String CASE_API_URL = "/lending/case";
	public static final String CASE_INITIATE = "/initiate";
	public static final String MASTER_DATA_STORAGE = "/data-storage";
	public static final String SLUG_URL = "/{slug}";
	public static final String ID_URL = "/{id}";

	// *********** Portal Error Constants ***********
	public static final String PORTAL_ALREADY_EXISTS = "Portal Configuration already exists";
	public static final String PORTAL_NOT_EXISTS = "Portal Configuration does not exists";
	public static final String PORTAL_USER_NOT_EXISTS = "Portal user does not exists";
	public static final String PORTAL_TAB_NOT_EXISTS = "Portal Tab does not exists";
	public static final String PORTAL_USER_NOT_AUTHENTICATED = "Invalid Portal User";

	
	public static final String PORTAL_DASHBORD_TASK_POSTFIX = "List";
	public static final String PORTAL_HISTORY_TASK_POSTFIX = "History";
	public static final String PORTAL_GLOBAL_TASK_POSTFIX = "Global";
	public static final String PORTAL_GLOBAL_TASK_LIKE = "%Global%";
	public static final String PORTAL_CLAIM_TASK_POSTFIX = "Claim";
	public static final String PORTAL_GLOBAL_TASK_POSTFIX_LIKE = "%-global";
	public static final String PORTAL_ACTION_VARIABLE_TYPE = "listOfAction";
	public static final String PORTAL_BUCKET_ACTION = "bucketActions";

	// ************* To start the Portal **************
	public static final String PORTAL_VISTED_TAB = "portalVisitedTabs";

	public static final String HISTORY_BUCKET_KEY = "history";
	public static final String CLAIM_BUCKET_KEY = "claim";
	public static final String FILTER_OFFSET_KEY = "offset";
	public static final String FILTER_PAGE_WIDTH_KEY = "pageWidth";
	public static final String DASHBOARD_TASK_VARIABLE_KEY = "dashboard";
	public static final String STATUS_VARIABLE_KEY = "Status";
	public static final String HISTORY_VIEW_KEY = "-history";
	public static final String LIST_VIEW_KEY = "-list";
	public static final String CLAIM_VIEW_KEY = "-claim";
	public static final String ALL_PRODUCT_TYPE = "all";
	public static final String ALL_BUCKET_KEY = "all";
	public static final String PORTFOLIO_SUFFIX = "-portfolio";
	public static final String PRODUCT_TYPE_KEY = "productType";
	public static final String GLOBAL_SEARCH_KEY = "globalSearch";

	public static final String CUSTOM_URL_NOT_EXISTS = "Custom url with this id does not exist";
	public static final String JOURNEY_TYPE = "journeyType";
	public static final int THREAD_POOL_SIZE = 3;
	public static final String CUSTOM_ACTION_COUNT_SUFFIX = "Count";
	public static final String CUSTOM_ACTION_TIMESTAMP_SUFFIX = "Timestamp";
	public static final String CUSTOM_ACTION_COMMENT_SUFFIX = "Comment";
	public static final String THREAD_ID_FAILED = "Thread with this caseInstanceId failed to get response due to timeout error";
	public static final String ERROR_BUILDING_THREAD = "Unable to build thread";

	public static final String BULK_ACTION_URL = "/fire-bulk-action/";
	public static final String CUSTOM_ACTION_URL = "/fire-custom-action/";

	public static final String IMPORT_CUSTOM_ACTION_API_URL = "/custom-action/import";
	public static final String EXPORT_CUSTOM_ACTION_API_URL = "/custom-action/export";
	public static final String CLAIM_API_URL = "/claim";
	public static final String UNCLAIM_API_URL = "/unclaim";
	public static final String REASSIGN_API_URL = "/reassign";
	public static final String UNABLE_TO_REASSIGN = "Unable to reassign";

	// Constants for CustomURLMapper
	public static final String URL_MAPPER_NOT_EXIST = "CustomURLMapper with this id does not exist";

	public static final String VARIABLE_NOT_MAPPED_FOR_ROLE_MESSAGE = "Variable not mapped for role : ";
	public static final String DASHBOARD_NOT_CONFIGURED_FOR_ROLE_MESSAGE = "Dashboard not configured for role : ";
	public static final String BUCKET_NOT_CONFIGURED_FOR_ROLE_MESSAGE = "Bucket not configured for role : ";
	public static final String ALREADY_CLAIM = "Already claimed";
	public static final String ALREADY_UNCLAIM = "Already unclaimed";
	public static final String NEW_ASSIGNEE_NOT_EXISTS = "Please provide new assignee";
	public static final String PARENT_CASE_INSTANCE_ID = "parentCaseInstanceId";
	public static final String GROUP_ID = "groupId";
	public static final String ASSIGNEE = "assignee";

	public static String getBucketNotConfiguredForRoleMessage(String role) {
		return BUCKET_NOT_CONFIGURED_FOR_ROLE_MESSAGE + role;
	}

	public static String getDashboardNotConfiguredForRoleMessage(String role) {
		return DASHBOARD_NOT_CONFIGURED_FOR_ROLE_MESSAGE + role;
	}

	public static String getVariableNotMappedForRoleMessage(String role) {
		return VARIABLE_NOT_MAPPED_FOR_ROLE_MESSAGE + role;
	}

	public static Map<String, String> JOURNEY_TYPE() {
		/*
		 * Kindly change your Journey Type Label with Journey Type Key
		 */
		final Map<String, String> journeyMap = new HashMap<>();
		journeyMap.put("SCB_PoC", "Business Loan");
		journeyMap.put("Auto_Loan_SCB", "Auto Loan");
		return journeyMap;
	}
	
	public enum Processes{
		JOURNEY, LMS, BACKOFFICE, COLLECTIONS, COLLATERAL;
	}

	// Constants for RabbitMQ configuration
	public static final String EXCHANGE_NAME = "exchange";
	public static final String QUEUE_GENERIC_NAME = "generic-queue";
	public static final String AUDIT_ROUTING_KEY = "route.audit";
	public static final String AUDIT_QUEUE_NAME = "audit-queue";
	public static final String AUDIT_UPDATE_QUEUE_NAME = "";
	public static final String ROUTING_KEY = "route.generic";
	
	//
	public static final String JMS_AUDIT_ROUTING_KEY = "route.jms.auditQueue";

	// Constants for Portal Audit
	public static final String REQUEST_PATH_VARIABLES = "org.springframework.web.servlet.View.pathVariables";
	public static final String ROLE_NAME_KEY = "roleName";
	public static final String REQUEST_ID_KEY = "requestId";
	public static final String PATH_VARIABLE_KEY = "pathVariables";
	public static final String CASE_INSTANCE_ID = "caseInstanceId";
	public static final String TAB_KEY = "tabKey";
	public static final String COMMENT_MESSAGE_KEY = "message";
	public static final String AUDIT_COMMENT_IDENTIFIER = "auditActionComment";
	public static final String CUSTOM_API_URL = "/lending/portal/fire-custom-action/*";
	public static final String SET_VARIABLE_API_URL = "/lending/case/submit-variables";
	public static final String SUBMIT_FORM_API_URL = "/lending/case/submit-form";
	public static final String BULK_SUBMIT_API = "/lending/portal/fire-bulk-action/*";

	public static final String ROLE_PROCESS_VARIABLE_KEY = "roles";
	public static final String LAST_ROLE_PROCESS_VARIABLE_KEY = "lastRole";
	public static final String GROUP_NAME_PROCESS_VARIABLE_KEY = "groupNames";
	public static final String ROLE_ASSIGNEE_VALUE = "Assignee";

	public static final String COMMENT_HISTORY_DATE_FORMATE = "MM-dd-yyyy hh:mm:ss";

	public static final boolean AUDIT_WITH_FILTER_ENABLED=false;

	/************************* Portal Native Query ************************/

	public static final String FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY = "FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY";
	public static final String ALL_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY = "ALL_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY";
	public static final String ALL_PRODUCT_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY = "ALL_PRODUCT_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY";
	public static final String ALL_PRODUCT_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY = "ALL_PRODUCT_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY";
	public static final String TASK_ASSIGNEE_BASE_QUERY = "TASK_ASSIGNEE_BASE_QUERY";
	public static final String ALL_BUCKET_TASK_ASSIGNEE_BASE_QUERY = "ALL_BUCKET_TASK_ASSIGNEE_BASE_QUERY";
	public static final String CLAIM_TASK_ASSIGNEE_BASE_QUERY = "CLAIM_TASK_ASSIGNEE_BASE_QUERY";
	public static final String FILTER_WHERE_CLAUSE = "FILTER_WHERE_CLAUSE";
	public static final String GLOBAL_FILTER_WHERE_CLAUSE = "GLOBAL_FILTER_WHERE_CLAUSE";
	public static final String FILTER_JOIN_SYNTAX = "FILTER_JOIN_SYNTAX";
	public static final String ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY = "ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY";
	public static final String GLOBAL_ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY = "GLOBAL_ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY";

//	public static  Map<String, Map<String, String>> portalFetchQueryMap = new HashMap<String, Map<String, String>>() {{
//		put("mysql", mySQLSubQueyMap);
//		put("postgres", postgresSubQueyMap);
////		put(null, mySQLSubQueyMap);
//	}};

	public static Map<String, String> portalFetchQueryMap(String databaseType){
		Map<String, Map<String, String>> queryMap = new HashMap<>();
		queryMap.put("mysql", mySQLSubQueyMap);
		queryMap.put("postgres", postgresSubQueyMap);
		queryMap.put(null, mySQLSubQueyMap);
		return queryMap.get(databaseType);
	};

	public static final Map<String, String> mySQLSubQueyMap = new HashMap<String, String>() {{
		put(FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY, "select  CONCAT('{\\\"caseInstanceId\\\":\\\"',ACT_RU_TASK.SCOPE_ID_,'\\\"}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('created',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.CREATE_TIME_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('assignee',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.ASSIGNEE_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('taskName',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.TASK_DEF_KEY_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE(ACT_HI_VARINST.NAME_,  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_HI_VARINST.TEXT_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}') as variables from ACT_RU_TASK, ACT_HI_VARINST where ACT_RU_TASK.SCOPE_TYPE_ = 'cmmn' and (queryTaskNameAssigneeList) and ACT_RU_TASK.SCOPE_ID_ IN (select V1.SCOPE_ID_ FROM ACT_HI_VARINST AS V1 JOIN ACT_HI_VARINST AS V2 ON V1.SCOPE_ID_ = V2.SCOPE_ID_ queryFilterJoin WHERE V1.NAME_ IN (queryRoleNameStatusList) and V1.TEXT_ = 'queryBucketKey' AND V2.NAME_ = 'journeyType' AND V2.TEXT_= 'queryProductType' queryFilterWhereClause)  and ACT_HI_VARINST.NAME_ in (queryBucketVariables) AND ACT_RU_TASK.SCOPE_ID_ = ACT_HI_VARINST.SCOPE_ID_ group by ACT_RU_TASK.SCOPE_ID_ ORDER BY  MAX(ACT_RU_TASK.CREATE_TIME_) desc");
		put(ALL_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY,"select  CONCAT('{\\\"caseInstanceId\\\":\\\"',ACT_RU_TASK.SCOPE_ID_,'\\\"}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('created',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.CREATE_TIME_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('assignee',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.ASSIGNEE_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('taskName',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.TASK_DEF_KEY_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE(ACT_HI_VARINST.NAME_,  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_HI_VARINST.TEXT_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}') as variables from ACT_RU_TASK, ACT_HI_VARINST where ACT_RU_TASK.SCOPE_TYPE_ = 'cmmn' and (queryTaskNameAssigneeList) and ACT_RU_TASK.SCOPE_ID_ IN (select V1.SCOPE_ID_ FROM ACT_HI_VARINST AS V1 queryFilterJoin WHERE V1.NAME_ = 'journeyType' AND V1.TEXT_= 'queryProductType' queryFilterWhereClause )  and ACT_HI_VARINST.NAME_ in (queryBucketVariables) AND ACT_RU_TASK.SCOPE_ID_ = ACT_HI_VARINST.SCOPE_ID_ group by ACT_RU_TASK.SCOPE_ID_ ORDER BY  MAX(ACT_RU_TASK.CREATE_TIME_) desc");
		put(ALL_PRODUCT_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY, "select  CONCAT('{\\\"caseInstanceId\\\":\\\"',ACT_RU_TASK.SCOPE_ID_,'\\\"}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('created',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.CREATE_TIME_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('assignee',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.ASSIGNEE_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('taskName',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.TASK_DEF_KEY_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE(ACT_HI_VARINST.NAME_,  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_HI_VARINST.TEXT_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}') as variables from ACT_RU_TASK, ACT_HI_VARINST where ACT_RU_TASK.SCOPE_TYPE_ = 'cmmn' and (queryTaskNameAssigneeList) and ACT_RU_TASK.SCOPE_ID_ IN (select V1.SCOPE_ID_ FROM ACT_HI_VARINST AS V1 queryFilterJoin WHERE V1.NAME_ IN (queryRoleNameStatusList) AND V1.TEXT_= 'queryBucketKey' queryFilterWhereClause )  and ACT_HI_VARINST.NAME_ in (queryBucketVariables) AND ACT_RU_TASK.SCOPE_ID_ = ACT_HI_VARINST.SCOPE_ID_ group by ACT_RU_TASK.SCOPE_ID_ ORDER BY  MAX(ACT_RU_TASK.CREATE_TIME_) desc");
		put(ALL_PRODUCT_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY, "select  CONCAT('{\\\"caseInstanceId\\\":\\\"',ACT_RU_TASK.SCOPE_ID_,'\\\"}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('created',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.CREATE_TIME_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('assignee',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.ASSIGNEE_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE('taskName',  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_RU_TASK.TASK_DEF_KEY_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}'), CONCAT('{',GROUP_CONCAT('\\\"',REPLACE(ACT_HI_VARINST.NAME_,  '\\\"', '\\\\\\\\\\\"'),'\\\":\\\"', REPLACE(ACT_HI_VARINST.TEXT_, '\\\"', '\\\\\\\\\\\"'),'\\\"'),'}') as variables from ACT_RU_TASK, ACT_HI_VARINST where ACT_RU_TASK.SCOPE_TYPE_ = 'cmmn' and (queryTaskNameAssigneeList) queryFilterBaseQuery and ACT_HI_VARINST.NAME_ in (queryBucketVariables) AND ACT_RU_TASK.SCOPE_ID_ = ACT_HI_VARINST.SCOPE_ID_ group by ACT_RU_TASK.SCOPE_ID_ ORDER BY  MAX(ACT_RU_TASK.CREATE_TIME_) desc");
		put(TASK_ASSIGNEE_BASE_QUERY, "(ACT_RU_TASK.TASK_DEF_KEY_ = queryAssignedTaskName AND ACT_RU_TASK.ASSIGNEE_  IN (queryAssigneeList))");
		put(ALL_BUCKET_TASK_ASSIGNEE_BASE_QUERY, "(ACT_RU_TASK.TASK_DEF_KEY_ in (queryAssignedTaskName) AND ACT_RU_TASK.ASSIGNEE_  IN (queryAssigneeList))");
		put(CLAIM_TASK_ASSIGNEE_BASE_QUERY, "ACT_RU_TASK.TASK_DEF_KEY_ in (queryClaimTaskNameList) ");
		put(FILTER_WHERE_CLAUSE, " AND V3.NAME_ = 'queryFilterKey' AND UPPER(V3.TEXT_) like UPPER('%queryFilterValue%') ");
		put(GLOBAL_FILTER_WHERE_CLAUSE, " AND UPPER(V3.TEXT_) like UPPER('%queryFilterValue%') ");
		put(FILTER_JOIN_SYNTAX, "JOIN ACT_HI_VARINST AS V3 ON V1.SCOPE_ID_ = V3.SCOPE_ID_ ");
		put(ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY, " and ACT_RU_TASK.SCOPE_ID_ IN (select V1.SCOPE_ID_ FROM ACT_HI_VARINST AS V1 WHERE V1.NAME_ = 'queryFilterKey' AND UPPER(V1.TEXT_) like UPPER('%queryFilterValue%') ) ");
		put(GLOBAL_ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY, " and ACT_RU_TASK.SCOPE_ID_ IN (select V1.SCOPE_ID_ FROM ACT_HI_VARINST AS V1 WHERE UPPER(V1.TEXT_) like UPPER('%queryFilterValue%') ) ");
	}} ;

	public static final Map<String, String> postgresSubQueyMap = new HashMap<String, String>() {{
		put(FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY, "select  concat('{\"caseInstanceId\":\"',act_ru_task.scope_id_,'\"}') as caseInstanceId, concat('{',string_agg(concat('\"',TRANSLATE('assignee',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.assignee_, '\"', '\"'),'\"'), ','),'}') as assignee, concat('{',string_agg(concat('\"',TRANSLATE('taskName',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.task_def_key_, '\"', '\"'),'\"'), ','),'}') as taskName, concat('{',string_agg(concat('\"',TRANSLATE('created',  '\"', '\"'),'\":\"', TRANSLATE(cast(act_ru_task.create_time_ as text), '\"', '\"'),'\"'), ','),'}') as created, concat('{',string_agg(concat('\"',TRANSLATE(act_hi_varinst.name_,  '\"', '\"'),'\":\"', TRANSLATE(act_hi_varinst.text_, '\"', '\"'),'\"'), ','),'}') as variables from act_ru_task, act_hi_varinst where act_ru_task.scope_type_ = 'cmmn' and (queryTaskNameAssigneeList) and act_ru_task.scope_id_ in (select v1.scope_id_ from act_hi_varinst as v1 join act_hi_varinst as v2 on v1.scope_id_ = v2.scope_id_ queryFilterJoin where v1.name_ in (queryRoleNameStatusList) and v1.text_ = 'queryBucketKey' and v2.name_ = 'journeyType' and v2.text_= 'queryProductType' queryFilterWhereClause)  and act_hi_varinst.name_ in (queryBucketVariables) and act_ru_task.scope_id_ = act_hi_varinst.scope_id_ group by act_ru_task.scope_id_ order by  max(act_ru_task.create_time_) desc");
		put(ALL_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY,"select  concat('{\"caseInstanceId\":\"',act_ru_task.scope_id_,'\"}') as caseInstanceId, concat('{',string_agg(concat('\"',TRANSLATE('assignee',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.assignee_, '\"', '\"'),'\"'), ','),'}') as assignee, concat('{',string_agg(concat('\"',TRANSLATE('taskName',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.task_def_key_, '\"', '\"'),'\"'), ','),'}') as taskName, concat('{',string_agg(concat('\"',TRANSLATE('created',  '\"', '\"'),'\":\"', TRANSLATE(cast(act_ru_task.create_time_ as text), '\"', '\"'),'\"'), ','),'}') as created, concat('{',string_agg(concat('\"',TRANSLATE(act_hi_varinst.name_,  '\"', '\"'),'\":\"', TRANSLATE(act_hi_varinst.text_, '\"', '\"'),'\"'), ','),'}') as variables from act_ru_task, act_hi_varinst where act_ru_task.scope_type_ = 'cmmn' and (queryTaskNameAssigneeList) and ACT_RU_TASK.SCOPE_ID_ IN (select V1.SCOPE_ID_ FROM ACT_HI_VARINST AS V1 queryFilterJoin WHERE V1.NAME_ = 'journeyType' AND V1.TEXT_= 'queryProductType' queryFilterWhereClause )  and ACT_HI_VARINST.NAME_ in (queryBucketVariables) AND ACT_RU_TASK.SCOPE_ID_ = ACT_HI_VARINST.SCOPE_ID_ group by ACT_RU_TASK.SCOPE_ID_ ORDER BY  MAX(ACT_RU_TASK.CREATE_TIME_) desc");
		put(ALL_PRODUCT_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY, "select  concat('{\"caseInstanceId\":\"',act_ru_task.scope_id_,'\"}') as caseInstanceId, concat('{',string_agg(concat('\"',TRANSLATE('assignee',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.assignee_, '\"', '\"'),'\"'), ','),'}') as assignee, concat('{',string_agg(concat('\"',TRANSLATE('taskName',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.task_def_key_, '\"', '\"'),'\"'), ','),'}') as taskName, concat('{',string_agg(concat('\"',TRANSLATE('created',  '\"', '\"'),'\":\"', TRANSLATE(cast(act_ru_task.create_time_ as text), '\"', '\"'),'\"'), ','),'}') as created, concat('{',string_agg(concat('\"',TRANSLATE(act_hi_varinst.name_,  '\"', '\"'),'\":\"', TRANSLATE(act_hi_varinst.text_, '\"', '\"'),'\"'), ','),'}') as variables from act_ru_task, act_hi_varinst where act_ru_task.scope_type_ = 'cmmn' and (queryTaskNameAssigneeList) and act_ru_task.scope_id_ in (select v1.scope_id_ from act_hi_varinst as v1 queryFilterJoin where v1.name_ in (queryRoleNameStatusList) and v1.text_= 'queryBucketKey' queryFilterWhereClause )  and act_hi_varinst.name_ in (queryBucketVariables) and act_ru_task.scope_id_ = act_hi_varinst.scope_id_ group by act_ru_task.scope_id_ order by  max(act_ru_task.create_time_) desc");
		put(ALL_PRODUCT_BUCKET_FETCH_TASK_NAME_ASSIGNEE_LIST_BASE_QUERY, "select  concat('{\"caseInstanceId\":\"',act_ru_task.scope_id_,'\"}') as caseInstanceId, concat('{',string_agg(concat('\"',TRANSLATE('assignee',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.assignee_, '\"', '\"'),'\"'), ','),'}') as assignee, concat('{',string_agg(concat('\"',TRANSLATE('taskName',  '\"', '\"'),'\":\"', TRANSLATE(act_ru_task.task_def_key_, '\"', '\"'),'\"'), ','),'}') as taskName, concat('{',string_agg(concat('\"',TRANSLATE('created',  '\"', '\"'),'\":\"', TRANSLATE(cast(act_ru_task.create_time_ as text), '\"', '\"'),'\"'), ','),'}') as created, concat('{',string_agg(concat('\"',TRANSLATE(act_hi_varinst.name_,  '\"', '\"'),'\":\"', TRANSLATE(act_hi_varinst.text_, '\"', '\"'),'\"'), ','),'}') as variables from act_ru_task, act_hi_varinst where act_ru_task.scope_type_ = 'cmmn' and (queryTaskNameAssigneeList) queryFilterBaseQuery and act_hi_varinst.name_ in (queryBucketVariables) and act_ru_task.scope_id_ = act_hi_varinst.scope_id_ group by act_ru_task.scope_id_ order by  max(act_ru_task.create_time_) desc");
		put(TASK_ASSIGNEE_BASE_QUERY, "(act_ru_task.task_def_key_ = queryAssignedTaskName and act_ru_task.assignee_  in (queryAssigneeList))");
		put(ALL_BUCKET_TASK_ASSIGNEE_BASE_QUERY, "(act_ru_task.task_def_key_ in (queryAssignedTaskName) and act_ru_task.assignee_  in (queryAssigneeList))");
		put(CLAIM_TASK_ASSIGNEE_BASE_QUERY, "act_ru_task.task_def_key_ in (queryClaimTaskNameList) ");
		put(FILTER_WHERE_CLAUSE, " and v3.name_ = 'queryFilterKey' and UPPER(v3.text_) like UPPER('%queryFilterValue%') ");
		put(GLOBAL_FILTER_WHERE_CLAUSE, " and UPPER(v3.text_) like UPPER('%queryFilterValue%') ");
		put(FILTER_JOIN_SYNTAX, "join act_hi_varinst as v3 on v1.scope_id_ = v3.scope_id_ ");
		put(ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY, " and act_ru_task.scope_id_ in (select v1.scope_id_ from act_hi_varinst as v1 where v1.name_ = 'queryFilterKey' and UPPER(v1.text_) like UPPER('%queryFilterValue%') ) ");
		put(GLOBAL_ALL_PRODUCT_BUCKET_FILTER_BASE_QUERY, " and act_ru_task.scope_id_ in (select v1.scope_id_ from act_hi_varinst as v1 where UPPER(v1.text_) like UPPER('%queryFilterValue%') ) ");
	}} ;

	// Logging Constants for CMMNService

	public static String LOG_RESPONSE_USER_CASEINSTANCE = "Response for userId {}, caseInstanceId {} is {}";
	public static String LOG_EXCEPTION_USER_CASEINSTANCE = "Exception raised for userId {}, caseInstanceId {} is: {}";
	public static String LOG_EXCEPTION_USER_APPLICATION = "Exception raised for userId {}, applicationNumber {} is: {}";

	// Logging Constants for PortalService
	public static String LOG_EXCEPTION_PORTAL_USER = "Exception raised for Portal user {} is {}";
}
