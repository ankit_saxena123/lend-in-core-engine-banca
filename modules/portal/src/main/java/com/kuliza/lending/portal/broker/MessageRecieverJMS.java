package com.kuliza.lending.portal.broker;

import java.io.Serializable;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.kuliza.lending.engine_common.utils.HelperFunctions;
import com.kuliza.lending.portal.utils.PortalConstants;

@Component
@ConditionalOnExpression("${broker.name.jms:true}")
public class MessageRecieverJMS implements MessageListener {
	@Autowired
	private CommonListenerService commonListenerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMessageListener.class);

	@JmsListener(destination = PortalConstants.JMS_AUDIT_ROUTING_KEY, containerFactory = "jmsFactory")
	@Override
	public void onMessage(Message message) {
		LOGGER.info("Received message JMS");
		try {
			if (message instanceof TextMessage) {
				String textMessage = HelperFunctions.extractStringFromMessage((TextMessage) message);
				commonListenerService.forwardMessageToService(textMessage);
			} else if (message instanceof BytesMessage) {
				byte[] byteArray = HelperFunctions.extractByteArrayFromMessage((BytesMessage) message);
			} else if (message instanceof MapMessage) {
				Map<String, Object> mapMessage = HelperFunctions.extractMapFromMessage((MapMessage) message);
			} else if (message instanceof ObjectMessage) {
				Serializable object = HelperFunctions.extractSerializableFromMessage((ObjectMessage) message);
			} else {
				LOGGER.info("Received: " + message);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				message.acknowledge();
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
}