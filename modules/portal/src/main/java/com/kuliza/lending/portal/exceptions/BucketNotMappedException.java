package com.kuliza.lending.portal.exceptions;

public class BucketNotMappedException extends RuntimeException {

	public BucketNotMappedException() {
		super();
	}

	public BucketNotMappedException(String message) {
		super(message);
	}

}
