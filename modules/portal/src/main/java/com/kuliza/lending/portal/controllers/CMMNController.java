package com.kuliza.lending.portal.controllers;

import static com.kuliza.lending.portal.utils.PortalConstants.CASE_API_URL;
import static com.kuliza.lending.portal.utils.PortalConstants.CASE_INITIATE;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.portal.pojo.InitiatePortalData;
import com.kuliza.lending.portal.pojo.PortalSubmitFormClass;
import com.kuliza.lending.portal.service.CMMNService;
import java.security.Principal;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.kuliza.lending.portal.utils.SwaggerConstants;
import io.swagger.annotations.*;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(CASE_API_URL)
@Api(consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CMMNController {

	@Autowired
	private CMMNService cmmnService;

	@ApiOperation(value=SwaggerConstants.CC_START_OR_RESUME)
    @ApiResponses(value={@ApiResponse(message=SwaggerConstants.CC_GET_FORM, code=200)})
    @ApiImplicitParam(name=Constants.AUTH, value=Constants.AUTH_TOKEN,
        required=true, dataType="string", paramType="header", example=Constants.BEARER_TOKEN)
	@PostMapping(CASE_INITIATE)
	public ResponseEntity<Object> initiateProcess(Principal principal,
			@ApiParam(value="Case Name", required=true, example="portalPOCV2") @RequestParam(required=true, value="caseName") String caseName,
			@ApiParam(value="Application Number", required=true, example="725") @RequestParam(required=true, value="applicationNumber") String applicationNumber,
			@ApiParam(value = "PortFolio Flag" , required = false, example = "true")
            @RequestParam(required = false, value = Constants.PORTFOLIO_FLAG) boolean portfolioFlag,
			@RequestBody InitiatePortalData initiatePortalData) {
		return CommonHelperFunctions.buildResponseEntity(
				cmmnService.startOrResumeCase(caseName, applicationNumber, principal.getName(), initiatePortalData, portfolioFlag));
	}

  @ApiOperation(value=SwaggerConstants.CC_SUBMIT_FORM)
  @ApiImplicitParam(name=Constants.AUTH, value=Constants.AUTH_TOKEN,
          required=true, dataType="string", paramType="header", example=Constants.BEARER_TOKEN)
  @ApiResponses(value={@ApiResponse(message=SwaggerConstants.CC_GET_FORM, code=200)})
  @RequestMapping(method=RequestMethod.POST, value="/submit-form")
  public ResponseEntity<Object> submitForm(Principal principal, 
		  @ApiParam(value="request Id", required=false, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @RequestParam(required=false, value="requestId") String requestId,
		  @RequestBody PortalSubmitFormClass input) {
    return CommonHelperFunctions
        .buildResponseEntity(cmmnService.submitFormData(principal.getName(), input, false));
  }

   @ApiImplicitParam(name=Constants.AUTH, value=Constants.AUTH_TOKEN,
            required=true, dataType="string", paramType="header", example=Constants.BEARER_TOKEN)
   @ApiOperation(value=SwaggerConstants.CC_POST_PARTIAL_SUBMIT_FORM)
   @ApiResponses(value={@ApiResponse(message=SwaggerConstants.CC_GET_FORM, code=200)})
  @RequestMapping(method=RequestMethod.POST, value="/partial-submit-form")
  public ResponseEntity<Object> partialSubmitForm(Principal principal, @RequestBody PortalSubmitFormClass input) {
    return CommonHelperFunctions
        .buildResponseEntity(cmmnService.submitFormData(principal.getName(), input, true));
  }

  @ApiImplicitParam(name=Constants.AUTH, value=Constants.AUTH_TOKEN,
          required=true, dataType="string", paramType="header", example=Constants.BEARER_TOKEN)
  @ApiOperation(value=SwaggerConstants.CC_GET_PARTIAL_SUBMIT_FORM)
  @ApiResponses(value={@ApiResponse(message=SwaggerConstants.CC_GET_FORM, code=200)})
  @RequestMapping(method=RequestMethod.GET, value="/get-form")
  public ResponseEntity<Object> partialSubmitForm(Principal principal,
         @ApiParam(value="Case Instance Id", required=true, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @RequestParam(required=true, value="caseInstanceId") String caseInstanceId) throws Exception {
    return CommonHelperFunctions
        .buildResponseEntity(cmmnService.getTaskData(principal.getName(), caseInstanceId, HttpStatus.BAD_REQUEST,
            Constants.NO_TASKS_MESSAGE));
  }

  @ApiImplicitParam(name=Constants.AUTH, value=Constants.AUTH_TOKEN,
          required=true, dataType="string", paramType="header", example=Constants.BEARER_TOKEN)
  @ApiOperation(value=SwaggerConstants.CC_SUBMIT_VARIABLES_DESC)
  @ApiResponses(value={@ApiResponse(message=SwaggerConstants.CC_SUBMIT_VARIABLES, code=200)})
  @RequestMapping(method=RequestMethod.POST, value="/submit-variables")
  public ResponseEntity<Object> submitVariables(Principal principal,
         @ApiParam(value="Case Instance Id", required=true, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @RequestParam(required=true, value="caseInstanceId") String caseInstanceId,
         @ApiParam(value="Role Name", required=false, example="cmmnCreditAnalystPOCAdminRole") @RequestParam(required=false, value="roleName") String roleName,
         @ApiParam(value="tab key", required=false, example="personalAddressDetail") @RequestParam(required=false, value="tabKey") String tabKey,
         @ApiParam(value="request Id", required=false, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @RequestParam(required=false, value="requestId") String requestId,
         @ApiParam(value=SwaggerConstants.CC_CASE_VARAIBLES) @Valid @NotNull @NotEmpty @RequestBody Map<String, Object> caseVariables) {
    	  return CommonHelperFunctions.buildResponseEntity(
				cmmnService.submitOrGetCaseVariables(principal.getName(), caseInstanceId, caseVariables, requestId));
	}

  @ApiImplicitParam(name=Constants.AUTH, value=Constants.AUTH_TOKEN,
          required=true, dataType="string", paramType="header", example=Constants.BEARER_TOKEN)
  @ApiOperation(value=SwaggerConstants.CC_GET_VARIABLES_DESC)
  @ApiResponses(value={@ApiResponse(message=SwaggerConstants.CC_GET_VARIABLES, code=200)})
  @RequestMapping(method=RequestMethod.GET, value="/get-variables")
  public ResponseEntity<Object> getVariables(Principal principal,
         @ApiParam(value="Case Instance Id", required=true, example="5cfd4e36-5a18-11e9-880c-02426ed2fcfa") @RequestParam(required=true, value="caseInstanceId") String caseInstanceId) {
    return CommonHelperFunctions
        .buildResponseEntity(cmmnService.submitOrGetCaseVariables(principal.getName(), caseInstanceId, null, null));
  }

//  @RequestMapping(method=RequestMethod.POST, value="/clear-data")
//  public ResponseEntity<Object> submitVariables(Principal principal, @RequestBody List<ClearDataInput> caseInstanceList) {
//    return CommonHelperFunctions
//        .buildResponseEntity(cmmnService.clearData(principal.getName(), caseInstanceList));
//  }

}
