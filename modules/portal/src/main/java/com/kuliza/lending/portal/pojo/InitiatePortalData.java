package com.kuliza.lending.portal.pojo;

import java.util.Map;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class InitiatePortalData {

	String sourceId;

	@NotEmpty(message = "sourceType cannot be empty")
	String sourceType;

	String sourceProcessName;

	@NotNull(message = "portalType cannot be null")
	@NotEmpty(message = "portalType cannot be empty")
	String portalType;

	Map<String, Object> variablesToSave;
	

	Map<String, Object> portfolioVariablesToSave;

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getPortalType() {
		return portalType;
	}

	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}

	public Map<String, Object> getVariablesToSave() {
		return variablesToSave;
	}

	public void setVariablesToSave(Map<String, Object> variablesToSave) {
		this.variablesToSave = variablesToSave;
	}

	public String getSourceProcessName() {
		return sourceProcessName;
	}

	public void setSourceProcessName(String sourceProcessName) {
		this.sourceProcessName = sourceProcessName;
	}

	public Map<String, Object> getPortfolioVariablesToSave() {
		return portfolioVariablesToSave;
	}

	public void setPortfolioVariablesToSave(Map<String, Object> portfolioVariablesToSave) {
		this.portfolioVariablesToSave = portfolioVariablesToSave;
	}
	
	
}
