package com.kuliza.lending.decision_table.pojo;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

public class DecisionTableExecuteRepresentation implements Serializable {

	@NotNull
	private String decisionKey;
	private Map<String, Object> inputVariables;
	private String caseModelId;
	private String processInstanceId;

	public String getDecisionKey() {
		return decisionKey;
	}

	public void setDecisionKey(String decisionKey) {
		this.decisionKey = decisionKey;
	}

	public Map<String, Object> getInputVariables() {
		return inputVariables;
	}

	public void setInputVariables(Map<String, Object> inputVariables) {
		this.inputVariables = inputVariables;
	}

	public String getCaseModelId() {
		return caseModelId;
	}

	public void setCaseModelId(String caseModelId) {
		this.caseModelId = caseModelId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

}
