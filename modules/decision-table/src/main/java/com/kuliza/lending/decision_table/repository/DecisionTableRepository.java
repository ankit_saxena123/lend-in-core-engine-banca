package com.kuliza.lending.decision_table.repository;

import com.kuliza.lending.decision_table.models.DecisionTableModel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


//This is the repository for decision table, to interact with the model based on the functions defined in it.
@Repository
public interface DecisionTableRepository extends JpaRepository<DecisionTableModel, Long> {



    //This is the function which returns the first entry for the provided nameKey and isDeleted ordered by version in descending order
    DecisionTableModel findFirstByNameKeyAndIsDeletedOrderByVersionDesc(String nameKey, boolean isDeleted);

    //This is the function which returns the first entry for the provided nameKey ordered by version in descending order
    DecisionTableModel findFirstByNameKeyOrderByVersionDesc(String nameKey);

    //This is the function which returns the decision table for provided nameKey and version
    DecisionTableModel findByNameKeyAndVersionAndIsDeleted(String nameKey, int version, boolean isDeleted);
    
	// This is the function which returns the decision table for provided nameKey
	// and version
	@Query(value = "select model_editor_json as modelEditorJson, name as name, model_key as nameKey, version as version, description as description, last_updated as lastUpdated, id as id from ACT_DE_MODEL where model_key = ?1 and model_type = 4 limit 1 ;", nativeQuery = true)
	List<Object[]> findByNameKeyAndModelTypeFromACTDEMODEL(String nameKey);



}
