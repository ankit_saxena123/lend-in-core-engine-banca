package com.kuliza.lending.decision_table.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.kuliza.lending.decision_table.utils.DecisionTableConstants;
import com.kuliza.lending.decision_table.models.DecisionTableModel;
import com.kuliza.lending.decision_table.pojo.DecisionTableExecuteRepresentation;
import com.kuliza.lending.decision_table.pojo.DecisionTableModelRepresentation;
import com.kuliza.lending.decision_table.pojo.DecisionTableRequestRepresentation;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(DecisionTableConstants.DECISION_TABLE_URL)
public class DecisionTableController {

	@Autowired
	private DecisionTableService decisionTableService;

	@PostMapping(DecisionTableConstants.CREATE_DECISION_TABLE_URL)
	public ResponseEntity<DecisionTableModel> createAndDeployDecisionTable(
			@Valid @RequestBody DecisionTableModelRepresentation decisionTableModelRepresentation) throws Exception {
		return new ResponseEntity<DecisionTableModel>(
				decisionTableService.addDecisionTable(decisionTableModelRepresentation), HttpStatus.CREATED);
	}

	@PostMapping(DecisionTableConstants.DEPLOY_DECISION_TABLE_URL)
	public ResponseEntity<DecisionTableModel> deployDecisionTable(
			@Valid @RequestBody DecisionTableRequestRepresentation decisionTableRequestRepresentation)
			throws Exception {
		return new ResponseEntity<DecisionTableModel>(
				decisionTableService.deployDecisionTable(decisionTableRequestRepresentation.getDecisionKey(),
						decisionTableRequestRepresentation.getVersion()),
				HttpStatus.OK);
	}

	@GetMapping(DecisionTableConstants.FETCH_DECISION_TABLE_URL)
	public ResponseEntity<DecisionTableModelRepresentation> fetchDecisionTable(@Valid @RequestParam String decisionKey,
			@RequestParam(required = false) Integer version) throws Exception {
		return new ResponseEntity<DecisionTableModelRepresentation>(
				decisionTableService.fetchDecisionTableDefinition(decisionKey, version), HttpStatus.OK);
	}

	@DeleteMapping(DecisionTableConstants.DELETE_DECISION_TABLE_URL)
	public ResponseEntity<DecisionTableModel> deleteDecisionTable(
			@Valid @RequestBody DecisionTableRequestRepresentation decisionTableRequestRepresentation)
			throws Exception {
		return new ResponseEntity<DecisionTableModel>(
				decisionTableService.deleteDecisionTable(decisionTableRequestRepresentation.getDecisionKey(),
						decisionTableRequestRepresentation.getVersion()),
				HttpStatus.OK);
	}

	@PostMapping(DecisionTableConstants.EXECUTE_DECISION_TABLE_URL)
	public ResponseEntity<Map<String, Object>> executeDecisionTable(
			@Valid @RequestBody DecisionTableExecuteRepresentation decisionTableExecuteRepresentation)
			throws Exception {
		Map<String, Object> outputVariables = decisionTableService.executeDecisionTable(
				decisionTableExecuteRepresentation.getDecisionKey(),
				decisionTableService.getInputVariabes(decisionTableExecuteRepresentation));
		if (outputVariables == null) {
			Map<String, Object> error = new HashMap<String, Object>();
			error.put("error", DecisionTableConstants.EXECUTE_API_ERROR_MSG);
			return new ResponseEntity<Map<String, Object>>(error, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, Object>>(outputVariables, HttpStatus.OK);
	}

}
