package com.kuliza.lending.decision_table.pojo;

import com.kuliza.lending.decision_table.models.DecisionTableModel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/*
    * This is the model for accepting input for the creation/updation of a decision table
    * The user can create a new decision table(version 1), update the current version or add a new version of
      the existing decision table.
*/
public class DecisionTableModelRepresentation implements Serializable {

    @NotNull(message = "Name cannot be null")
    private String name;
    @NotNull(message = "Key cannot be null")
    private String key;

    private String description;
    private Date lastUpdated;
    private String comment;
    private boolean newVersion;

    @Valid
    private DecisionTableDefinitionRepresentation decisionTableDefinition;



    public DecisionTableModelRepresentation() {
    }

    public DecisionTableModelRepresentation(DecisionTableModel model){
        this.name = model.getName();
        this.key = model.getNameKey();
        this.description = model.getDescription();
        this.comment = model.getComment();
        this.lastUpdated = model.getModified();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public boolean isNewVersion() {
        return newVersion;
    }

    public void setNewVersion(boolean newVersion) {
        this.newVersion = newVersion;
    }


    public DecisionTableDefinitionRepresentation getDecisionTableDefinition() {
        return decisionTableDefinition;
    }

    public void setDecisionTableDefinition(DecisionTableDefinitionRepresentation decisionTableDefinition) {
        this.decisionTableDefinition = decisionTableDefinition;
    }


}
