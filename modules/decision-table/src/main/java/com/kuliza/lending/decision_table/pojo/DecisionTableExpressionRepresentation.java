package com.kuliza.lending.decision_table.pojo;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

//This is the model that contains the expressions of the DMN definition
public class DecisionTableExpressionRepresentation implements Serializable {

    @NotNull(message = "Variable Id cannot be null")
    private String id;
    @NotNull(message = "Variable Id cannot be null")
    private String variableId;
    private String variableType;
    private String type;
    private String label;
    private List<String> entries;
    private boolean newVariable;
    private boolean complexExpression;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVariableId() {
        return variableId;
    }

    public void setVariableId(String variableId) {
        this.variableId = variableId;
    }

    public String getVariableType() {
        return variableType;
    }

    public void setVariableType(String variableType) {
        this.variableType = variableType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getEntries() {
        return entries;
    }

    public void setEntries(List<String> entries) {
        this.entries = entries;
    }

    public boolean isNewVariable() {
        return newVariable;
    }

    public void setNewVariable(boolean newVariable) {
        this.newVariable = newVariable;
    }

    public boolean isComplexExpression() {
        return complexExpression;
    }

    public void setComplexExpression(boolean complexExpression) {
        this.complexExpression = complexExpression;
    }
}
