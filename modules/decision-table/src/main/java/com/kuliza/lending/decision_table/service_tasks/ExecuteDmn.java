package com.kuliza.lending.decision_table.service_tasks;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.decision_table.repository.DecisionTableRepository;
import com.kuliza.lending.decision_table.services.DecisionTableService;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.dmn.api.DmnDecisionTable;
import org.flowable.dmn.api.DmnRepositoryService;
import org.flowable.dmn.model.DmnDefinition;
import org.flowable.editor.dmn.converter.DmnJsonConverter;
import org.flowable.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ExecuteDmn")
public class ExecuteDmn {
	
	@Autowired
	private DecisionTableRepository decisionTableRepository;
	
	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;
	
	@Autowired
	private DmnRepositoryService dmnRepositoryService;
	
	@Autowired
	protected ObjectMapper objectMapper;
	
	@Autowired
	private DecisionTableService decisionTableService;
	
	@Autowired
	private RuntimeService runtimeService;
	
	
	public Map<String, Object> executethroughCMMN(String caseId, String decisionKey)
			throws IOException, InterruptedException, Exception {
		Map<String, Object> processVariables = cmmnRuntimeService.getVariables(caseId);
		try {
			Object[] decisionTable = null;
			List<Object[]> decisionTables = decisionTableRepository.findByNameKeyAndModelTypeFromACTDEMODEL("textExtract");
			if (decisionTables.size() > 0) {
				decisionTable = decisionTables.get(0);
			} else {
				throw new InterruptedException("No DMN Found for this key.");
			}
			DmnDecisionTable deployed = dmnRepositoryService.createDecisionTableQuery().decisionTableKey(CommonHelperFunctions.getStringValue(decisionTable[2])).
					decisionTableVersion(CommonHelperFunctions.getIntegerValue(decisionTable[3])).singleResult();
			if (deployed != null && deployed.getDeploymentId() != null) {
				
			} else {
				JsonNode decisionTableNode = objectMapper.readTree(CommonHelperFunctions.getStringValue(decisionTable[0]));
				DmnDefinition dmnDefinition = new DmnJsonConverter().convertToDmn(decisionTableNode,
						CommonHelperFunctions.getStringValue(decisionTable[6]), CommonHelperFunctions.getIntegerValue(decisionTable[3]),
						(Date) decisionTable[5]);
				String uniqueName = CommonHelperFunctions.getStringValue(decisionTable[2]) + "_" + CommonHelperFunctions.getIntegerValue(decisionTable[3]);
				dmnRepositoryService.createDeployment().name("deployment_" + uniqueName)
						.addDmnModel("dmn_" + uniqueName + ".dmn", dmnDefinition).deploy();
			}
			Map<String, Object> outputVariables = decisionTableService.executeDecisionTable(decisionKey, processVariables);
			cmmnRuntimeService.setVariables(caseId, outputVariables);
			return outputVariables;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedException("Error in Executing DMN: " + e.getMessage());
		}
	}
	
	public Map<String, Object> executethroughBPMN(String procInstId, String decisionKey)
			throws IOException, InterruptedException, Exception {
		Map<String, Object> processVariables = runtimeService.getVariables(procInstId);
		try {
			Object[] decisionTable = null;
			List<Object[]> decisionTables = decisionTableRepository.findByNameKeyAndModelTypeFromACTDEMODEL(decisionKey);
			if (decisionTables.size() > 0) {
				decisionTable = decisionTables.get(0);
			} else {
				throw new InterruptedException("No DMN Found for this key.");
			}
			DmnDecisionTable deployed = dmnRepositoryService.createDecisionTableQuery().decisionTableKey(CommonHelperFunctions.getStringValue(decisionTable[2])).
					decisionTableVersion(CommonHelperFunctions.getIntegerValue(decisionTable[3])).singleResult();
			if (deployed != null && deployed.getDeploymentId() != null) {
				
			} else {
				JsonNode decisionTableNode = objectMapper.readTree(CommonHelperFunctions.getStringValue(decisionTable[0]));
				DmnDefinition dmnDefinition = new DmnJsonConverter().convertToDmn(decisionTableNode,
						CommonHelperFunctions.getStringValue(decisionTable[6]), CommonHelperFunctions.getIntegerValue(decisionTable[3]),
						(Date) decisionTable[5]);
				String uniqueName = CommonHelperFunctions.getStringValue(decisionTable[2]) + "_" + CommonHelperFunctions.getIntegerValue(decisionTable[3]);
				dmnRepositoryService.createDeployment().name("deployment_" + uniqueName)
						.addDmnModel("dmn_" + uniqueName + ".dmn", dmnDefinition).deploy();
			}
			Map<String, Object> outputVariables = decisionTableService.executeDecisionTable(decisionKey, processVariables);
			runtimeService.setVariables(procInstId, outputVariables);
			return outputVariables;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedException("Error in Executing DMN: " + e.getMessage());
		}
	}

}
