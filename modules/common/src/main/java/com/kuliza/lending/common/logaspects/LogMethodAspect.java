package com.kuliza.lending.common.logaspects;

import com.kuliza.lending.common.pojo.HTTPResponse;
import javax.servlet.http.HttpServletResponse;
import org.apache.activemq.transport.nio.SelectorSelection;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.LogType;

@Aspect
@Component
public class LogMethodAspect {
	@Around("com.kuliza.lending.common.logaspects.LogPointcuts.logAnnotationPointcut()")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
		final long start = System.currentTimeMillis();
		final Object proceed = joinPoint.proceed();
		final long executionTime = System.currentTimeMillis() - start;
		String str = joinPoint.getSignature() + " executed in " + executionTime + "ms";
		LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.EXITING_METHOD, str);
		return proceed;
	}

	@Before("com.kuliza.lending.common.logaspects.LogPointcuts.logAnnotationPointcut()")
	public void logMethodArguments(JoinPoint joinPoint) throws Throwable {
		LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.ENTERING_METHOD, null);
	}

	@AfterReturning(value = "com.kuliza.lending.common.logaspects.LogPointcuts.logAnnotationPointcut()", returning = "result")
	public void logMethodResult(JoinPoint joinPoint, Object result) throws Throwable {
		ObjectMapper objectMapper = new ObjectMapper();
		String finalResponse ="";
		if(result!=null && !(result instanceof com.mashape.unirest.http.HttpResponse)
				&& !(result instanceof HttpServletResponse)) {
			finalResponse = objectMapper.writeValueAsString(result);
		}
		LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.EXITING_METHOD, finalResponse);
	}

	@AfterThrowing(value = "com.kuliza.lending.common.logaspects.LogPointcuts.logAnnotationPointcut()", throwing = "exception")
	public void logMethodException(JoinPoint joinPoint, Throwable exception) throws Throwable {
		LogAspectUtils.fetchDetailsFromJoinPoint(joinPoint, LogType.EXCEPTION, exception);
	}
}
