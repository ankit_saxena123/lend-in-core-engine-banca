package com.kuliza.lending.common.logaspects;

import org.aspectj.lang.annotation.Pointcut;

public class LogPointcuts {
     @Pointcut(value="@annotation(com.kuliza.lending.common.annotations.LogMethodDetails)")
     public void logAnnotationPointcut() {
    	 
     }
     
     // Breaks when building project for keycloak
//     @Pointcut(value="execution(* com.kuliza.lending..*.*(..))")
//     public void logEverywherePointcut() {
//    	 
//     }
//     
//     @Pointcut(value="logAnnotationPointcut() || logEverywherePointcut()")
//     public void logPointcut() {
//    	 
//     }
}
