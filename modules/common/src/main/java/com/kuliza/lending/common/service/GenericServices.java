package com.kuliza.lending.common.service;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.common.utils.LogType;
import com.kuliza.lending.common.utils.CommonHelperFunctions;

@Service
public class GenericServices {

	@Autowired
	private ObjectMapper objectMapper;

	public String generateBPMToken(String userId, String password) throws Exception {
		String temp = userId + ":" + password;
		return Base64.getEncoder().encodeToString(temp.getBytes("utf-8"));
	}

	public ApiResponse checkErrors(BindingResult result) throws Exception {
		ApiResponse response = null;
		if (result.hasErrors()) {
			Map<String, Object> data = new HashMap<>();
			data.put(Constants.DATA_ERRORS_KEY,
					CommonHelperFunctions.generateErrorResponseData(result.getFieldErrors()));
			response = new ApiResponse(HttpStatus.UNPROCESSABLE_ENTITY, Constants.FAILURE_MESSAGE, data);
		}
		return response;
	}
}
