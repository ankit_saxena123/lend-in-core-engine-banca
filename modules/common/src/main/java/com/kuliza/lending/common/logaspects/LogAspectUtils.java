package com.kuliza.lending.common.logaspects;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.LogType;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.logging.utils.LoggingMarkers;

public class LogAspectUtils {
	private static final Logger logger = LoggerFactory.getLogger(LogAspectUtils.class);

	public static void putDataInMDC(String userId, String processInstanceId, String caseInstanceId, LogType type, String appId, String method) {
		MDC.put("userId", userId != null ? userId : "");
		MDC.put("processInstanceId", processInstanceId != null ? processInstanceId : "");
		MDC.put("caseInstanceId", caseInstanceId != null ? caseInstanceId.toString() : "");
		MDC.put("type", type != null ? type.toString() : "");
		MDC.put("appId", appId != null ? appId : "");
		MDC.put("method", method != null ? method : "");
	}

	public static void log(String userId, String processInstanceId, String caseInstanceId, LogType type, String appId, String method,
			Object msg) throws JsonProcessingException {
		if (logger.isInfoEnabled()) {
			putDataInMDC(userId, processInstanceId, caseInstanceId, type, appId, method);
			logger.info(LoggingMarkers.JSON,StaticContextAccessor.getBean(ObjectMapper.class).writeValueAsString(msg));
		}
	}

	public static void logException(String userId, String processInstanceId, String caseInstanceId, LogType type, String appId,
			String method, Exception e) {
		if (logger.isErrorEnabled()) {
			try {
				putDataInMDC(userId, processInstanceId, caseInstanceId, type, appId, method);
				logger.error(StaticContextAccessor.getBean(ObjectMapper.class).writeValueAsString(e.getMessage()), e);
			} catch (Exception jsonParseException) {
				logger.error(e.getMessage(), jsonParseException);
			}
		}
	}
	
	public static void fetchDetailsFromJoinPoint(JoinPoint joinPoint, LogType event, Object result) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String methodName = signature.toString();
		Method method = signature.getMethod();
		LogMethodDetails myAnnotation = method.getAnnotation(LogMethodDetails.class);
		String caseInstanceId = myAnnotation.caseInstanceId();
		String processInstanceId = myAnnotation.processInstanceId();
		String userId = myAnnotation.userId();
		String businessKey = myAnnotation.businessKey();
		Object[] args = joinPoint.getArgs();
		String[] parameterNames = signature.getParameterNames();
		Map<String, Object> functionParametersAndValues = new HashMap<String, Object>();
		for (int argIndex = 0; argIndex < args.length; argIndex++) {
			if (parameterNames[argIndex].equalsIgnoreCase("token") || parameterNames[argIndex]
					.equalsIgnoreCase("otp")) {
				continue;
			}
			if (parameterNames[argIndex].equals(CommonHelperFunctions.getStringValue(myAnnotation.caseInstanceId()))) {
				caseInstanceId = CommonHelperFunctions.getStringValue(args[argIndex]);
			} else if (parameterNames[argIndex].equals(CommonHelperFunctions.getStringValue(myAnnotation.userId()))) {
				userId = CommonHelperFunctions.getStringValue(args[argIndex]);
			} else if (parameterNames[argIndex]
					.equals(CommonHelperFunctions.getStringValue(myAnnotation.businessKey()))) {
				businessKey = CommonHelperFunctions.getStringValue(args[argIndex]);
			} else if (parameterNames[argIndex]
					.equals(CommonHelperFunctions.getStringValue(myAnnotation.processInstanceId()))) {
				processInstanceId = CommonHelperFunctions.getStringValue(args[argIndex]);
			}
			if(args[argIndex] instanceof HttpServletRequest)
				continue;
			functionParametersAndValues.put(parameterNames[argIndex], args[argIndex]);
		}
		try {
			if(event.equals(LogType.ENTERING_METHOD))
				LogAspectUtils.log(userId, processInstanceId, caseInstanceId, event, businessKey, methodName,
						functionParametersAndValues);
			else if(event.equals(LogType.EXITING_METHOD)) {
				LogAspectUtils.log(userId, processInstanceId, caseInstanceId, event, businessKey, methodName,
						result);
			}
			else if(event.equals(LogType.EXCEPTION)) {
				LogAspectUtils.logException(userId, processInstanceId, caseInstanceId, event, businessKey, methodName,
						(Exception)result);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
