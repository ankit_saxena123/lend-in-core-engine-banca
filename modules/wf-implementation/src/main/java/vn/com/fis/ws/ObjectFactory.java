
package vn.com.fis.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the vn.com.fis.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PrepareCertificateForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "prepareCertificateForSignCloud");
    private final static QName _GetCertificateDetailForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "getCertificateDetailForSignCloud");
    private final static QName _GetSignedFileForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "getSignedFileForSignCloudResponse");
    private final static QName _ApproveCertificateForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "approveCertificateForSignCloud");
    private final static QName _ChangePasscodeForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "changePasscodeForSignCloudResponse");
    private final static QName _SetCertificateDetailForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "setCertificateDetailForSignCloudResponse");
    private final static QName _ForgetPasscodeForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "forgetPasscodeForSignCloud");
    private final static QName _GetSignedFileForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "getSignedFileForSignCloud");
    private final static QName _ApproveCertificateForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "approveCertificateForSignCloudResponse");
    private final static QName _AuthorizeCounterSigningForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "authorizeCounterSigningForSignCloudResponse");
    private final static QName _ChangePasscodeForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "changePasscodeForSignCloud");
    private final static QName _PrepareCertificateForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "prepareCertificateForSignCloudResponse");
    private final static QName _ForgetPasscodeForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "forgetPasscodeForSignCloudResponse");
    private final static QName _PrepareFileForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "prepareFileForSignCloudResponse");
    private final static QName _GetCertificateDetailForSignCloudResponse_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "getCertificateDetailForSignCloudResponse");
    private final static QName _AuthorizeCounterSigningForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "authorizeCounterSigningForSignCloud");
    private final static QName _SetCertificateDetailForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "setCertificateDetailForSignCloud");
    private final static QName _PrepareFileForSignCloud_QNAME = new QName("http://api.esigncloud.mobileid.vn/", "prepareFileForSignCloud");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: vn.com.fis.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SignCloudMetaData }
     * 
     */
    public SignCloudMetaData createSignCloudMetaData() {
        return new SignCloudMetaData();
    }

    /**
     * Create an instance of {@link SignCloudMetaData.SingletonSigning }
     * 
     */
    public SignCloudMetaData.SingletonSigning createSignCloudMetaDataSingletonSigning() {
        return new SignCloudMetaData.SingletonSigning();
    }

    /**
     * Create an instance of {@link SignCloudMetaData.CounterSigning }
     * 
     */
    public SignCloudMetaData.CounterSigning createSignCloudMetaDataCounterSigning() {
        return new SignCloudMetaData.CounterSigning();
    }

    /**
     * Create an instance of {@link ChangePasscodeForSignCloudResponse }
     * 
     */
    public ChangePasscodeForSignCloudResponse createChangePasscodeForSignCloudResponse() {
        return new ChangePasscodeForSignCloudResponse();
    }

    /**
     * Create an instance of {@link ApproveCertificateForSignCloud }
     * 
     */
    public ApproveCertificateForSignCloud createApproveCertificateForSignCloud() {
        return new ApproveCertificateForSignCloud();
    }

    /**
     * Create an instance of {@link GetSignedFileForSignCloudResponse }
     * 
     */
    public GetSignedFileForSignCloudResponse createGetSignedFileForSignCloudResponse() {
        return new GetSignedFileForSignCloudResponse();
    }

    /**
     * Create an instance of {@link GetCertificateDetailForSignCloud }
     * 
     */
    public GetCertificateDetailForSignCloud createGetCertificateDetailForSignCloud() {
        return new GetCertificateDetailForSignCloud();
    }

    /**
     * Create an instance of {@link PrepareCertificateForSignCloud }
     * 
     */
    public PrepareCertificateForSignCloud createPrepareCertificateForSignCloud() {
        return new PrepareCertificateForSignCloud();
    }

    /**
     * Create an instance of {@link ApproveCertificateForSignCloudResponse }
     * 
     */
    public ApproveCertificateForSignCloudResponse createApproveCertificateForSignCloudResponse() {
        return new ApproveCertificateForSignCloudResponse();
    }

    /**
     * Create an instance of {@link GetSignedFileForSignCloud }
     * 
     */
    public GetSignedFileForSignCloud createGetSignedFileForSignCloud() {
        return new GetSignedFileForSignCloud();
    }

    /**
     * Create an instance of {@link ForgetPasscodeForSignCloud }
     * 
     */
    public ForgetPasscodeForSignCloud createForgetPasscodeForSignCloud() {
        return new ForgetPasscodeForSignCloud();
    }

    /**
     * Create an instance of {@link SetCertificateDetailForSignCloudResponse }
     * 
     */
    public SetCertificateDetailForSignCloudResponse createSetCertificateDetailForSignCloudResponse() {
        return new SetCertificateDetailForSignCloudResponse();
    }

    /**
     * Create an instance of {@link GetCertificateDetailForSignCloudResponse }
     * 
     */
    public GetCertificateDetailForSignCloudResponse createGetCertificateDetailForSignCloudResponse() {
        return new GetCertificateDetailForSignCloudResponse();
    }

    /**
     * Create an instance of {@link PrepareFileForSignCloudResponse }
     * 
     */
    public PrepareFileForSignCloudResponse createPrepareFileForSignCloudResponse() {
        return new PrepareFileForSignCloudResponse();
    }

    /**
     * Create an instance of {@link ForgetPasscodeForSignCloudResponse }
     * 
     */
    public ForgetPasscodeForSignCloudResponse createForgetPasscodeForSignCloudResponse() {
        return new ForgetPasscodeForSignCloudResponse();
    }

    /**
     * Create an instance of {@link ChangePasscodeForSignCloud }
     * 
     */
    public ChangePasscodeForSignCloud createChangePasscodeForSignCloud() {
        return new ChangePasscodeForSignCloud();
    }

    /**
     * Create an instance of {@link PrepareCertificateForSignCloudResponse }
     * 
     */
    public PrepareCertificateForSignCloudResponse createPrepareCertificateForSignCloudResponse() {
        return new PrepareCertificateForSignCloudResponse();
    }

    /**
     * Create an instance of {@link AuthorizeCounterSigningForSignCloudResponse }
     * 
     */
    public AuthorizeCounterSigningForSignCloudResponse createAuthorizeCounterSigningForSignCloudResponse() {
        return new AuthorizeCounterSigningForSignCloudResponse();
    }

    /**
     * Create an instance of {@link PrepareFileForSignCloud }
     * 
     */
    public PrepareFileForSignCloud createPrepareFileForSignCloud() {
        return new PrepareFileForSignCloud();
    }

    /**
     * Create an instance of {@link AuthorizeCounterSigningForSignCloud }
     * 
     */
    public AuthorizeCounterSigningForSignCloud createAuthorizeCounterSigningForSignCloud() {
        return new AuthorizeCounterSigningForSignCloud();
    }

    /**
     * Create an instance of {@link SetCertificateDetailForSignCloud }
     * 
     */
    public SetCertificateDetailForSignCloud createSetCertificateDetailForSignCloud() {
        return new SetCertificateDetailForSignCloud();
    }

    /**
     * Create an instance of {@link SignCloudReq }
     * 
     */
    public SignCloudReq createSignCloudReq() {
        return new SignCloudReq();
    }

    /**
     * Create an instance of {@link AgreementDetails }
     * 
     */
    public AgreementDetails createAgreementDetails() {
        return new AgreementDetails();
    }

    /**
     * Create an instance of {@link CredentialData }
     * 
     */
    public CredentialData createCredentialData() {
        return new CredentialData();
    }

    /**
     * Create an instance of {@link SignCloudResp }
     * 
     */
    public SignCloudResp createSignCloudResp() {
        return new SignCloudResp();
    }

    /**
     * Create an instance of {@link SignCloudMetaData.SingletonSigning.Entry }
     * 
     */
    public SignCloudMetaData.SingletonSigning.Entry createSignCloudMetaDataSingletonSigningEntry() {
        return new SignCloudMetaData.SingletonSigning.Entry();
    }

    /**
     * Create an instance of {@link SignCloudMetaData.CounterSigning.Entry }
     * 
     */
    public SignCloudMetaData.CounterSigning.Entry createSignCloudMetaDataCounterSigningEntry() {
        return new SignCloudMetaData.CounterSigning.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareCertificateForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "prepareCertificateForSignCloud")
    public JAXBElement<PrepareCertificateForSignCloud> createPrepareCertificateForSignCloud(PrepareCertificateForSignCloud value) {
        return new JAXBElement<PrepareCertificateForSignCloud>(_PrepareCertificateForSignCloud_QNAME, PrepareCertificateForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCertificateDetailForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "getCertificateDetailForSignCloud")
    public JAXBElement<GetCertificateDetailForSignCloud> createGetCertificateDetailForSignCloud(GetCertificateDetailForSignCloud value) {
        return new JAXBElement<GetCertificateDetailForSignCloud>(_GetCertificateDetailForSignCloud_QNAME, GetCertificateDetailForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSignedFileForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "getSignedFileForSignCloudResponse")
    public JAXBElement<GetSignedFileForSignCloudResponse> createGetSignedFileForSignCloudResponse(GetSignedFileForSignCloudResponse value) {
        return new JAXBElement<GetSignedFileForSignCloudResponse>(_GetSignedFileForSignCloudResponse_QNAME, GetSignedFileForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApproveCertificateForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "approveCertificateForSignCloud")
    public JAXBElement<ApproveCertificateForSignCloud> createApproveCertificateForSignCloud(ApproveCertificateForSignCloud value) {
        return new JAXBElement<ApproveCertificateForSignCloud>(_ApproveCertificateForSignCloud_QNAME, ApproveCertificateForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePasscodeForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "changePasscodeForSignCloudResponse")
    public JAXBElement<ChangePasscodeForSignCloudResponse> createChangePasscodeForSignCloudResponse(ChangePasscodeForSignCloudResponse value) {
        return new JAXBElement<ChangePasscodeForSignCloudResponse>(_ChangePasscodeForSignCloudResponse_QNAME, ChangePasscodeForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCertificateDetailForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "setCertificateDetailForSignCloudResponse")
    public JAXBElement<SetCertificateDetailForSignCloudResponse> createSetCertificateDetailForSignCloudResponse(SetCertificateDetailForSignCloudResponse value) {
        return new JAXBElement<SetCertificateDetailForSignCloudResponse>(_SetCertificateDetailForSignCloudResponse_QNAME, SetCertificateDetailForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForgetPasscodeForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "forgetPasscodeForSignCloud")
    public JAXBElement<ForgetPasscodeForSignCloud> createForgetPasscodeForSignCloud(ForgetPasscodeForSignCloud value) {
        return new JAXBElement<ForgetPasscodeForSignCloud>(_ForgetPasscodeForSignCloud_QNAME, ForgetPasscodeForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSignedFileForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "getSignedFileForSignCloud")
    public JAXBElement<GetSignedFileForSignCloud> createGetSignedFileForSignCloud(GetSignedFileForSignCloud value) {
        return new JAXBElement<GetSignedFileForSignCloud>(_GetSignedFileForSignCloud_QNAME, GetSignedFileForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApproveCertificateForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "approveCertificateForSignCloudResponse")
    public JAXBElement<ApproveCertificateForSignCloudResponse> createApproveCertificateForSignCloudResponse(ApproveCertificateForSignCloudResponse value) {
        return new JAXBElement<ApproveCertificateForSignCloudResponse>(_ApproveCertificateForSignCloudResponse_QNAME, ApproveCertificateForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeCounterSigningForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "authorizeCounterSigningForSignCloudResponse")
    public JAXBElement<AuthorizeCounterSigningForSignCloudResponse> createAuthorizeCounterSigningForSignCloudResponse(AuthorizeCounterSigningForSignCloudResponse value) {
        return new JAXBElement<AuthorizeCounterSigningForSignCloudResponse>(_AuthorizeCounterSigningForSignCloudResponse_QNAME, AuthorizeCounterSigningForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePasscodeForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "changePasscodeForSignCloud")
    public JAXBElement<ChangePasscodeForSignCloud> createChangePasscodeForSignCloud(ChangePasscodeForSignCloud value) {
        return new JAXBElement<ChangePasscodeForSignCloud>(_ChangePasscodeForSignCloud_QNAME, ChangePasscodeForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareCertificateForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "prepareCertificateForSignCloudResponse")
    public JAXBElement<PrepareCertificateForSignCloudResponse> createPrepareCertificateForSignCloudResponse(PrepareCertificateForSignCloudResponse value) {
        return new JAXBElement<PrepareCertificateForSignCloudResponse>(_PrepareCertificateForSignCloudResponse_QNAME, PrepareCertificateForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForgetPasscodeForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "forgetPasscodeForSignCloudResponse")
    public JAXBElement<ForgetPasscodeForSignCloudResponse> createForgetPasscodeForSignCloudResponse(ForgetPasscodeForSignCloudResponse value) {
        return new JAXBElement<ForgetPasscodeForSignCloudResponse>(_ForgetPasscodeForSignCloudResponse_QNAME, ForgetPasscodeForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareFileForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "prepareFileForSignCloudResponse")
    public JAXBElement<PrepareFileForSignCloudResponse> createPrepareFileForSignCloudResponse(PrepareFileForSignCloudResponse value) {
        return new JAXBElement<PrepareFileForSignCloudResponse>(_PrepareFileForSignCloudResponse_QNAME, PrepareFileForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCertificateDetailForSignCloudResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "getCertificateDetailForSignCloudResponse")
    public JAXBElement<GetCertificateDetailForSignCloudResponse> createGetCertificateDetailForSignCloudResponse(GetCertificateDetailForSignCloudResponse value) {
        return new JAXBElement<GetCertificateDetailForSignCloudResponse>(_GetCertificateDetailForSignCloudResponse_QNAME, GetCertificateDetailForSignCloudResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeCounterSigningForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "authorizeCounterSigningForSignCloud")
    public JAXBElement<AuthorizeCounterSigningForSignCloud> createAuthorizeCounterSigningForSignCloud(AuthorizeCounterSigningForSignCloud value) {
        return new JAXBElement<AuthorizeCounterSigningForSignCloud>(_AuthorizeCounterSigningForSignCloud_QNAME, AuthorizeCounterSigningForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCertificateDetailForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "setCertificateDetailForSignCloud")
    public JAXBElement<SetCertificateDetailForSignCloud> createSetCertificateDetailForSignCloud(SetCertificateDetailForSignCloud value) {
        return new JAXBElement<SetCertificateDetailForSignCloud>(_SetCertificateDetailForSignCloud_QNAME, SetCertificateDetailForSignCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareFileForSignCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.esigncloud.mobileid.vn/", name = "prepareFileForSignCloud")
    public JAXBElement<PrepareFileForSignCloud> createPrepareFileForSignCloud(PrepareFileForSignCloud value) {
        return new JAXBElement<PrepareFileForSignCloud>(_PrepareFileForSignCloud_QNAME, PrepareFileForSignCloud.class, null, value);
    }

}
