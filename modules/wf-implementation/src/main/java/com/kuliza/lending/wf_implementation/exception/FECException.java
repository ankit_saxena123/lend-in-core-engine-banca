package com.kuliza.lending.wf_implementation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kuliza.lending.wf_implementation.common.ErrorId;
import com.kuliza.lending.wf_implementation.common.ErrorIdList;

@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class FECException extends Exception{
	
	private static final long serialVersionUID = 676852299677539062L;
	
	public FECException(){
		super();
	}
	
	@JsonIgnore
	@Override
	public StackTraceElement[] getStackTrace() {
		return super.getStackTrace();
	}
	
	public FECException(String message){
		super(message);
	}
	
	public FECException(ErrorId errorId){
		super(new ErrorIdList(errorId).convertToJsonString());
	}


}
