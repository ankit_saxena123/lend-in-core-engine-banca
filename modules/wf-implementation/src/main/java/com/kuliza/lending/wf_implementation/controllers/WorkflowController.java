package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.BackToJourneyInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;
import com.kuliza.lending.wf_implementation.services.WorkflowService;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@RestController
@RequestMapping(WfImplConstants.WORKFLOW_API_ENDPOINT)
public class WorkflowController {

	@Autowired
	private WorkflowService workflowService;

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_INITIATE_API_ENDPOINT)
	public ResponseEntity<Object> initiateProcess(Principal principal,
			@RequestBody @Valid WorkflowInitiateRequest wfInitiateRequest) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.startOrResumeProcess(principal.getName(), wfInitiateRequest));
	}
	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_INITIATE_BY_ASSIGNEE_API_ENDPOINT)
	public ResponseEntity<Object> initiateProcessByAssignee(Principal principal,
			@RequestBody @Valid WorkflowInitiateRequest wfInitiateRequest) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.startOrResumeProcess(wfInitiateRequest.getAssignee(), wfInitiateRequest));
	}

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_SUBMIT_FORM_API_ENDPOINT)
	public ResponseEntity<Object> allSubmit(Principal principal, @RequestBody SubmitFormClass input) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.submitFormData(principal.getName(), input));
	}

	@RequestMapping(method = RequestMethod.GET, value = WfImplConstants.WORKFLOW_SUBMIT_FORM_API_ENDPOINT)
	public ResponseEntity<Object> getPresentState(Principal principal, @RequestParam String processInstanceId) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.getFormData(principal.getName(), processInstanceId));
	}

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_BACK_API_ENDPOINT)
	public ResponseEntity<Object> moveBack(Principal principal, @Valid @RequestBody BackTaskInput input,
			BindingResult result) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.changeProcessState(principal.getName(), input, result));
	}

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_BACK_TO_JOURNEY_API_ENDPOINT)
	public ResponseEntity<Object> backToJourney(@RequestBody @Valid BackToJourneyInput backToJourneyInput) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.setUserData(backToJourneyInput));
	}	
	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.WORKFLOW_UPLOAD_NID_ENDPOINT, consumes = "multipart/form-data")
	public ResponseEntity<Object> uploadFile(Principal principal,
		@RequestParam(required = true, name = "type") String type,
		@RequestParam(required = true, name = "image") MultipartFile submission,
		@RequestParam(required = true, name = "id") String procInstId) {
		return CommonHelperFunctions.buildResponseEntity(workflowService.uploadAll(submission, type, procInstId));
	}

}
