package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

import com.kuliza.lending.common.utils.Constants.DeviceType;

public class DeviceInfoInpuForm {

	@NotNull(message = "AppbuildNumber cannot be null")
	private String appbuildNumber;
	@NotNull(message = "AppbundleId cannot be null")
	private String appbundleId;
	@NotNull(message = "BrandName cannot be null")
	private String brandName;
	@NotNull(message = "Carrier cannot be null")
	private String carrier;
	@NotNull(message = "DeviceId cannot be null")
	private String deviceId;
	@NotNull(message = "DeviceLocale cannot be null")
	private String deviceLocale;
	@NotNull(message = "DeviceName cannot be null")
	private String deviceName;
	@NotNull(message = "DeviceNumberId cannot be null")
	private String deviceNumberId;
	@NotNull(message = "DeviceType cannot be null")
	private DeviceType deviceType;
	@NotNull(message = "Imei cannot be null")
	private String imei;
	@NotNull(message = "Ip cannot be null")
	private String ip;
	@NotNull(message = "MacAddress cannot be null")
	private String macAddress;
	@NotNull(message = "Model cannot be null")
	private String model;
	@NotNull(message = "PhoneNumber cannot be null")
	private String phoneNumber;
	@NotNull(message = "Version cannot be null")
	private String version;
	public String getAppbuildNumber() {
		return appbuildNumber;
	}
	public void setAppbuildNumber(String appbuildNumber) {
		this.appbuildNumber = appbuildNumber;
	}
	public String getAppbundleId() {
		return appbundleId;
	}
	public void setAppbundleId(String appbundleId) {
		this.appbundleId = appbundleId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceLocale() {
		return deviceLocale;
	}
	public void setDeviceLocale(String deviceLocale) {
		this.deviceLocale = deviceLocale;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDeviceNumberId() {
		return deviceNumberId;
	}
	public void setDeviceNumberId(String deviceNumberId) {
		this.deviceNumberId = deviceNumberId;
	}
	public DeviceType getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
}
