package com.kuliza.lending.wf_implementation.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.TwPremiumInput;
import com.kuliza.lending.wf_implementation.services.TwoWheelerPremimumService;

@RestController
@RequestMapping("/tw")
public class TwoWheelerPremiumCalculation {
	
	@Autowired
	private TwoWheelerPremimumService twoWheelerPremimumService;
	
	private static final Logger logger = LoggerFactory.getLogger(TwoWheelerPremiumCalculation.class);
	
	@RequestMapping(method = RequestMethod.POST, value = "/getTwPremium")
	public ResponseEntity<Object> getTwPremium(
			@RequestBody @Valid TwPremiumInput twInputForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(twoWheelerPremimumService.calPremium(twInputForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

}
