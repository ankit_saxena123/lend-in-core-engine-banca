package com.kuliza.lending.wf_implementation.notification;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.wf_implementation.dao.TopicArnDao;
import com.kuliza.lending.wf_implementation.dao.TopicMessagesDao;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.TopicArn;
import com.kuliza.lending.wf_implementation.models.TopicMessages;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.notification.SNSConfiguration.Platform;
import com.kuliza.lending.wf_implementation.services.IPATPaymentConfirmationService;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class SNSEventSubscription extends SNSClient{ 
	
		@Autowired
		public TopicMessagesDao topicMessagesDao;
		
		@Autowired
		public UserTopicsDao userTopicsDao;
		
		@Autowired
		public WorkflowUserDao workflowUserDao;
		
		@Autowired
		public TopicArnDao topicArnDao;

		@Autowired
		public SNSParamInit snsParamInit;
		
	 	private static final Logger logger = LoggerFactory
			.getLogger(SNSEventSubscription.class);
		 	 	
	 	public String subscribeToTopic(String topicArn, String subscriberProtocol, String endDeviceArn){
		   
			try{
				
				final SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, subscriberProtocol, endDeviceArn);
				AmazonSNSClient snsClient = getSNSClient();
				String subscriberArn = snsClient.subscribe(subscribeRequest).getSubscriptionArn();
				logger.info("<==subscribetopicrequestid==>"+snsClient.getCachedResponseMetadata(subscribeRequest).getRequestId());
				return subscriberArn;
			
		    }catch(Exception ex){
		    	logger.error("exception occured while publish message to endpoint " + ex);
		    	return null;
		    }

    	}
	 	
	 	public boolean unSubscribeFromTopic(String contactNumber, String topic){
			   
			try{
				WorkFlowUser workflowUser =workflowUserDao.findByMobileNumberAndIsDeleted(contactNumber, false);
				String userName = workflowUser.getIdmUserName();
				logger.info("<==unsubscribe username==>"+workflowUser.getUsername());
				logger.info("<==unsubscribe topic==>"+topic);
				String topicArn = snsParamInit.getTopicsMap().get(topic);
				UserTopics userTopic = userTopicsDao.findByUserNameAndTopicAndIsDeleted(userName, topic, false);
				String subscriberArn = userTopic.getSubscriberArn();
				logger.info("<==unsubscribe subscriberArn==>"+subscriberArn);
				AmazonSNSClient snsClient = getSNSClient();
				snsClient.unsubscribe(subscriberArn);
				logger.info("<==unsubscribe user ==>"+userName+"<=from Topic==>"+topic);
				userTopicsDao.unSubscibeUserFromTopic(userName, topic);
				return true;
		    }catch(Exception ex){
		    	logger.error("exception occured while unsubscribe user " + ex);
		    	return false;
		    }
    
	 	}
	 	
	 	public boolean publishMessageToTopic(String topic, String title, String body){
			   
			try{
				logger.info("<==subscribetopicrequestid topic==>"+topic);

				String topicArn = snsParamInit.getTopicsMap().get(topic);
				logger.info("<==subscribetopicrequestid topicArn==>"+topicArn);
				logger.info("<==subscribetopicrequestid title==>"+title);
				logger.info("<==subscribetopicrequestid body==>"+body);

				TopicMessages topicMessages = new TopicMessages();
				topicMessages.setMessage(body);
				topicMessages.setTopic(topic);
				topicMessagesDao.save(topicMessages);
				SNSMessagePublish  snsMessagePublish = new SNSMessagePublish();
				snsMessagePublish.publishMessageToEndpoint(body,title, topicArn,
										DeviceType.ios);
				
				return true;
			
		    }catch(Exception ex){
		    	logger.error("exception occured while publish message to endpoint " + ex);
		    	return false;
		    }

    	}
	 	
	 	public String createTopic(String topic){
	 		try{
	 			final CreateTopicRequest createTopicRequest = new CreateTopicRequest(topic);
	 			AmazonSNSClient snsClient = getSNSClient();
	 			final CreateTopicResult createTopicResponse = snsClient.createTopic(createTopicRequest);

	 			String arn = createTopicResponse.getTopicArn();
	 			TopicArn topicArn = new TopicArn();
	 			topicArn.setArn(arn);
	 			topicArn.setTopic(topic);
	 			topicArnDao.save(topicArn);
	 			// Print the topic ARN.
	 			logger.info("<==TopicArn:==>" + createTopicResponse.getTopicArn());
	 			    
				return createTopicResponse.getTopicArn();

	 		}catch(Exception ex){
		    	logger.error("exception occured while create topic " + ex);

		    	return "false";

	 		}
	 		
	 	}	
}
