package com.kuliza.lending.wf_implementation.pojo;

public class PlanPremiums {

	Integer sumInsuredAmount;
	Integer ppdAmount;
	Integer criticalIllenessAmount;
	Integer protectionAmount;
	
	Integer additionalAmount;
	
	public Integer getProtectionAmount() {
		return protectionAmount;
	}

	public void setProtectionAmount(Integer protectionAmount) {
		this.protectionAmount = protectionAmount;
	}

	public PlanPremiums(Integer sumInsuredAmount, Integer ppdAmount, Integer criticalIllenessAmount,Integer protectionAmount,Integer additionalAmount) {
		super();
		this.sumInsuredAmount = sumInsuredAmount;
		this.ppdAmount = ppdAmount;
		this.criticalIllenessAmount = criticalIllenessAmount;
		this.protectionAmount = protectionAmount;
		this.additionalAmount = additionalAmount;
		
	}

	public Integer getSumInsuredAmount() {
		return sumInsuredAmount;
	}

	public Integer getPpdAmount() {
		return ppdAmount;
	}

	public Integer getCriticalIllenessAmount() {
		return criticalIllenessAmount;
	}

	public void setSumInsuredAmount(Integer sumInsuredAmount) {
		this.sumInsuredAmount = sumInsuredAmount;
	}

	public void setPpdAmount(Integer ppdAmount) {
		this.ppdAmount = ppdAmount;
	}

	public void setCriticalIllenessAmount(Integer criticalIllenessAmount) {
		this.criticalIllenessAmount = criticalIllenessAmount;
	}

	@Override
	public String toString() {
		return "PlanPremiums [sumInsuredAmount=" + sumInsuredAmount + ", ppdAmount=" + ppdAmount
				+ ", criticalIllenessAmount=" + criticalIllenessAmount + "]";
	}

	public Integer getAdditionalAmount() {
		return additionalAmount;
	}

	public void setAdditionalAmount(Integer additionalAmount) {
		this.additionalAmount = additionalAmount;
	}
}
