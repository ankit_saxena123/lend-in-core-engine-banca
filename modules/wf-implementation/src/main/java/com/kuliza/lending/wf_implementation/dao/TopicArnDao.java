package com.kuliza.lending.wf_implementation.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.TopicArn;

@Repository
public interface TopicArnDao extends CrudRepository<TopicArn, Long> {

	public TopicArn findById(long id);

	public TopicArn findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<TopicArn> findByTopicAndIsDeleted(String topic, boolean isDeleted);

	public List<TopicArn> findByTopic(String topic);

}
