package com.kuliza.lending.wf_implementation.common;


public interface ErrorCodes {
	
	ErrorId INTERNAL_ERROR = new ErrorId("E1000", "Something went wrong. Please try again...");
	
	ErrorId INVALID_FPT_STATUS = new ErrorId("E1001", "Invalid Fpt status");
	
	ErrorId FPT_FILE_NOT_UPLOAD = new ErrorId("E1002", "FPT files are not uploaded yet");

	ErrorId INVALID_PREMIUM_AMOUNT = new ErrorId("E1003", "Invalid Premium amount");
	
	ErrorId INVALID_PROTECTION_AMOUNT = new ErrorId("E1004", "Invalid Production Premium amount");
	ErrorId INVALID_BASIC_PREMIUM_AMOUNT = new ErrorId("E1005", "Invalid Basic Premium amount");
	
	ErrorId INVALID_PRODUCT_TYPE = new ErrorId("E1006", "Invalid Product Type");
	
	ErrorId INVALID_PREMIUM_TYPE = new ErrorId("E1007", "Invalid Premium Type");
	ErrorId INVALID_QUOTE_DETAILS = new ErrorId("E1008", "Invalid Quote Details");
	
	ErrorId INVALID_POLICY_PDF = new ErrorId("E1009", "Invalid Policy pdf link");
	
	ErrorId DMS_UPLOAD_ERROR = new ErrorId("E1010", "Error while upload file in DMS");
	
	ErrorId EMPTY_MASTER_DATA = new ErrorId("E1011", "Empty master data from master");
	ErrorId INVALID_MASTER_STATUS = new ErrorId("E1012", "Invalid master status");
	
	ErrorId INVALID_DOC_ID = new ErrorId("E1013", "Invalid Doc Id");
	
	ErrorId INVALID_PROC_ID = new ErrorId("E1014", "Invalid Process Instance Id");
	
	ErrorId INVALID_WORKFLOW_APP = new ErrorId("E1015", "Invalid Workflow application");
	ErrorId INVALID_CMS_INTEGRATION = new ErrorId("E1017", "Invalid cms integration");

	ErrorId PROCESS_INSTANCE_ID_NULL = new ErrorId("E1018", "Process Instance Id Null");
	
	ErrorId INVALID_PAYMENT_STATUS = new ErrorId("E1019", "Invalid Payment status");
	
	ErrorId INVALID_USER = new ErrorId("E1020", "User not found in DB");
	
	ErrorId INVALID_MOBILE_NUMBER = new ErrorId("E1021", "Invalid mobile number");


}
