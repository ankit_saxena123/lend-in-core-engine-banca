package com.kuliza.lending.wf_implementation.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.pojo.FamilyPremiumPojo;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class FamilyHealthPremiumService {

	private static final Logger logger = LoggerFactory.getLogger(FamilyHealthPremiumService.class);
	@Autowired
	private MasterApiIntegration master;

	public ApiResponse FamilyPremium(String slugg, FamilyPremiumPojo input) throws JSONException {
		logger.info("inside get family premium*************************** :" + slugg);

		List<Map<String, String>> familyDetailsList = input.getFamilyDetails();

		try {

			List<String> familyMemberAgeList = new ArrayList<String>();
			for (Map<String, String> familyDetails : familyDetailsList) {
				if (familyDetails.containsKey("dob_hyperverge")) {
					String calculatedAge = Utils
							.getStringValue(Utils.getAgeFromDOB((familyDetails.get(WfImplConstants.DOB_HYPERVERGE))));
					familyMemberAgeList.add(calculatedAge);
				} else {
					String calculatedAge = Utils
							.getStringValue(Utils.getAgeFromDOB((familyDetails.get(WfImplConstants.HYPERVERGE_DOB))));
					familyMemberAgeList.add(calculatedAge);
				}
			}

			String language = input.getLanguage();
			logger.info("inside get from master AGE***************************" + familyMemberAgeList);
			List<Map<String, Object>> data = getDataFromMaster(familyMemberAgeList, slugg, language);

			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
		} catch (Exception e) {
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}
	}

	public List<Map<String, Object>> getDataFromMaster(List<String> familyMemberAgeList, String slugg, String language)
			throws Exception {
		logger.info("inside get from master***************************");

		JSONArray premiumList = new JSONArray();
		JSONObject premiumMasterResponse;
		Long basicTotal = (long) 0;
		Long advanceTotal = (long) 0;
		Long superiorTotal = (long) 0;
		List<String> memberPremiumBasic=new ArrayList<String>();
		List<String> memberPremiumAdvance=new ArrayList<String>();
		List<String> memberPremiumSuperior=new ArrayList<String>();
		try {
			premiumMasterResponse = master.getDataFromService(WfImplConstants.BANCA_GET_FH_PREMIUM);

			if (premiumMasterResponse != null
					&& premiumMasterResponse.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				premiumList = master.parseResponseFromMaster(premiumMasterResponse);

				for (int j = 0; j < familyMemberAgeList.size(); j++) {
					int age = Utils.getIntegerValue(familyMemberAgeList.get(j));

					for (int i = 0; i < premiumList.length(); i++) {
						JSONObject premium = premiumList.getJSONObject(i);

						if (age >= Utils.getIntegerValue(premium.getString(Constants.AGE_FROM))
								&& age <= Utils.getIntegerValue(premium.getString(Constants.AGE_TO))) {

							if (premium.getString(Constants.PLAN_ID_FAMILY).equals(Constants.PLAN_ID_BASIC)) {
								memberPremiumBasic.add(Utils.getStringValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT)));
								basicTotal = basicTotal
										+ Utils.getIntegerValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT));

							} else if (premium.getString(Constants.PLAN_ID_FAMILY).equals(Constants.PLAN_ID_ADVANCED)) {
								memberPremiumAdvance.add(Utils.getStringValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT)));
								advanceTotal = advanceTotal
										+ Utils.getIntegerValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT));
							} else {
								memberPremiumSuperior.add(Utils.getStringValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT)));
								superiorTotal = superiorTotal
										+ (Utils.getIntegerValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT)));
							}
						}
					}
				}
			}
			
			logger.info(" basic **********************************  " + basicTotal + superiorTotal + advanceTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return calculation(basicTotal, advanceTotal, superiorTotal, familyMemberAgeList.size(), slugg, language,memberPremiumBasic,memberPremiumAdvance,memberPremiumSuperior);

	}

	public List<Map<String, Object>> calculation(Long basic, Long advance, Long superior, int member, String slugg,
			String language, List<String>memberPremiumBasic,List<String>memberPremiumAdvance,List<String>memberPremiumSuperior) throws Exception {
		Map<String, Long> allPlanAmountList = new HashMap<String, Long>();
		double discFive = Constants.DISCOUNT_FIVE;
		double discTen = Constants.DISCOUNT_TEN;
		allPlanAmountList.put(Constants.Plans.Basic.name(), basic);
		allPlanAmountList.put(Constants.Plans.Advance.name(), advance);
		allPlanAmountList.put(Constants.Plans.Superior.name(), superior);

		if (member <= 2) {
			logger.info("inside total premium*****inside 2  " + member);
			return discountPremium(0.0, allPlanAmountList, member, slugg, language,memberPremiumBasic,memberPremiumAdvance,memberPremiumSuperior);
		} else if (member == 3) {
			logger.info("3 members***********");
			return discountPremium(discFive, allPlanAmountList, member, slugg, language,memberPremiumBasic,memberPremiumAdvance,memberPremiumSuperior);
		} else {
			logger.info("more then 3 members************** " + member);
			return discountPremium(discTen, allPlanAmountList, member, slugg, language,memberPremiumBasic,memberPremiumAdvance,memberPremiumSuperior);
		}
	}

	public List<Map<String, Object>> discountPremium(double discPer, Map<String, Long> planAmountMap, int member,
			String slugg, String language, List<String>memberPremiumBasic,List<String>memberPremiumAdvance,List<String>memberPremiumSuperior) throws Exception {
		logger.info(" inside calculation***************** ");

		List<Long> allPlansTotalAmountList = new ArrayList<>();
		Map<String, Long> discountedPlanValues = new HashMap<>();
		for (Map.Entry<String, Long> entry : planAmountMap.entrySet()) {
			logger.info("totap premium**  " + entry.getKey());
			double dicsp = discPer * entry.getValue();
			double totalCost = entry.getValue() - dicsp;
			logger.info("total cost ***************  " + totalCost);
			Long ceil = (long) (Math.round(totalCost));
			discountedPlanValues.put(entry.getKey(), ceil);
			// allPlansTotalAmountList.add(ceil);
		}
		logger.info("total discount*************  " + allPlansTotalAmountList);
		switch (slugg) {
		// To do
		case (Constants.FH_QUICK_QUOTE):
			return quickQuote(discountedPlanValues, language,memberPremiumBasic,memberPremiumAdvance,memberPremiumSuperior);
		default:
			return quoteDetails(discountedPlanValues, member, language,memberPremiumBasic,memberPremiumAdvance,memberPremiumSuperior);
		}
	}

	public List<Map<String, Object>> quickQuote(Map<String, Long> discountedPlanValues, String language,List<String>memberPremiumBasic,List<String>memberPremiumAdvance,List<String>memberPremiumSuperior) {
		String slug = WfImplConstants.BANCA_GET_FH_QUICK_QUOTE;

		JSONObject fhQuickQuoteResponse;
		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();
		List<Object> arrayList = new ArrayList<>();
		JSONArray fhQuickQuoteList = new JSONArray();

		try {
			fhQuickQuoteResponse = master.getDataFromService(slug);

			fhQuickQuoteList = master.parseResponseFromMaster(fhQuickQuoteResponse);
			for (int i = 0; i < fhQuickQuoteList.length(); i++) {
				String planValues = null;
				Long finalAmount = null;
				String planShortValues = null;
				JSONObject quickQuoteDetails = fhQuickQuoteList.getJSONObject(i);
				if (quickQuoteDetails.get(Constants.FH_LANGUAGE).equals(language)) {

					if (quickQuoteDetails.getString(Constants.PLAN_TYPE).equals(Constants.BASIC)
							|| quickQuoteDetails.getString(Constants.PLAN_TYPE).equals(Constants.BASIC_VI_QQ)) {
						planShortValues = quickQuoteDetails.getString(Constants.PLAN_SHORT_VALUES);

						logger.info("============= total disccoounntt  : "
								+ discountedPlanValues.get(Constants.Plans.Basic.name()) + "  ---->"
								+ Utils.convertToViantnumCurrencyTrieu(
										discountedPlanValues.get(Constants.Plans.Basic.name())));

						planShortValues = getPlanShortValue(planShortValues,
								discountedPlanValues.get(Constants.Plans.Basic.name()));
						quickQuoteDetails.put(Constants.PLAN_SHORT_VALUES, planShortValues);

						planValues = quickQuoteDetails.getString(Constants.PLAN_VALUES);
						planValues = planValues.replace(Constants.CONSTANT_X, Utils
								.convertToViantnumCurrencyTrieu(discountedPlanValues.get(Constants.Plans.Basic.name()))
								.toString());
						finalAmount = discountedPlanValues.get(Constants.Plans.Basic.name());
						quickQuoteDetails.put(WfImplConstants.MEMBER_PREMIUM, Utils.getStringValue(memberPremiumBasic).replaceAll("(^\\[|\\]$)", ""));
					} else if (quickQuoteDetails.getString(Constants.PLAN_TYPE).equals(Constants.ADVANCE)
							|| quickQuoteDetails.getString(Constants.PLAN_TYPE).equals(Constants.ADVANCE_VI_QQ)) {
						planShortValues = quickQuoteDetails.getString(Constants.PLAN_SHORT_VALUES);
						logger.info("============= total disccoounntt  : " + discountedPlanValues
								.get(Constants.Plans.Advance.name() + "  ---->" + Utils.convertToViantnumCurrencyTrieu(
										discountedPlanValues.get(Constants.Plans.Advance.name()))));
						planShortValues = getPlanShortValue(planShortValues,
								discountedPlanValues.get(Constants.Plans.Advance.name()));
						quickQuoteDetails.put(Constants.PLAN_SHORT_VALUES, planShortValues);

						planValues = quickQuoteDetails.getString(Constants.PLAN_VALUES);
						planValues = planValues
								.replace(
										Constants.CONSTANT_X, Utils
												.convertToViantnumCurrencyTrieu(
														discountedPlanValues.get(Constants.Plans.Advance.name()))
												.toString());
						finalAmount = discountedPlanValues.get(Constants.Plans.Advance.name());
						quickQuoteDetails.put(WfImplConstants.MEMBER_PREMIUM, Utils.getStringValue(memberPremiumAdvance).replaceAll("(^\\[|\\]$)", ""));
					} else {
						planShortValues = quickQuoteDetails.getString(Constants.PLAN_SHORT_VALUES);
						logger.info("============= total disccoounntt  : "
								+ discountedPlanValues.get(Constants.Plans.Superior.name()) + "  ---->"
								+ Utils.convertToViantnumCurrencyTrieu(
										discountedPlanValues.get(Constants.Plans.Superior.name())));

						planShortValues = getPlanShortValue(planShortValues,
								discountedPlanValues.get(Constants.Plans.Superior.name()));

						planValues = quickQuoteDetails.getString(Constants.PLAN_VALUES);
						planValues = planValues
								.replace(
										Constants.CONSTANT_X, Utils
												.convertToViantnumCurrencyTrieu(
														discountedPlanValues.get(Constants.Plans.Superior.name()))
												.toString());
						finalAmount = discountedPlanValues.get(Constants.Plans.Superior.name());
						quickQuoteDetails.put(WfImplConstants.MEMBER_PREMIUM, Utils.getStringValue(memberPremiumSuperior).replaceAll("(^\\[|\\]$)", ""));

					}
					
					quickQuoteDetails.put(Constants.PLAN_SHORT_VALUES, planShortValues);
					quickQuoteDetails.put(Constants.PLAN_VALUES, planValues);
					quickQuoteDetails.put(Constants.BASIC_PREMIUM, finalAmount);
					arrayList.add(quickQuoteDetails);
				}
			}
			finalMap = new ObjectMapper().readValue(arrayList.toString(),
					new TypeReference<List<Map<String, Object>>>() {
					});
			logger.info("quick finale array list**==>>*****  :  " + arrayList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return finalMap;
	}

	private String getPlanShortValue(String planShortValues, Long amount) {
		String[] splitStr = Utils.convertToViantnumCurrencyTrieu(amount).toString().split(Constants.WHITE_SPACE_SPLIT);

		planShortValues = planShortValues.replace(Constants.CONSTANT_X, splitStr[0]);
		return planShortValues;
	}

	public List<Map<String, Object>> quoteDetails(Map<String, Long> discountedPlanValues, int member, String language,List<String>memberPremiumBasic,List<String>memberPremiumAdvance,List<String>memberPremiumSuperior)
			throws Exception {
		logger.info("inside quote details *******  ->>>>>>");
		String slug = WfImplConstants.BANCA_GET_FH_QUOTE_DETAILS;
		JSONObject dataFromMaster;
		int family = member - 1;
		String quote;
		if (language.equals("en-US")) {
			quote = Constants.PROTECTS_YOU + family + Constants.LOVED_ONES;
		} else {
			quote = Constants.PROTECTS_YOU_VI + family + Constants.LOVED_ONES_VI;
		}
		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();
		List<Object> arrayList = new ArrayList<>();
		JSONArray masterParseArray = new JSONArray();
		try {
			dataFromMaster = master.getDataFromService(slug);
			masterParseArray = master.parseResponseFromMaster(dataFromMaster);
			for (int i = 0; i < masterParseArray.length(); i++) {
				String planShortValues = null;
				String protectionLabel = null;
				Long finalAmount = null;
				JSONObject arrayObject = masterParseArray.getJSONObject(i);
				if (arrayObject.get(Constants.FH_LANGUAGE).equals(language)) {
					if (arrayObject.getString(Constants.PLAN_TYPE).equals(Constants.BASIC)
							|| arrayObject.getString(Constants.PLAN_TYPE).equals(Constants.BASIC_VI_QD)) {
						logger.info("************ language*********** :" + arrayObject.get("language") + "     "
								+ language);
						planShortValues = arrayObject.getString(Constants.TOP_SECTION_VALUES);
						protectionLabel = arrayObject.getString(Constants.PROTECTION_LABEL);
						planShortValues = planShortValues.replace(Constants.CONSTANT_X, Utils
								.convertToViantnumCurrencyTrieu(discountedPlanValues.get(Constants.Plans.Basic.name()))
								.toString());
						protectionLabel = protectionLabel.replace(Constants.CONSTANT_Y, quote);
						finalAmount = discountedPlanValues.get(Constants.Plans.Basic.name());
						arrayObject.put(WfImplConstants.MEMBER_PREMIUM, Utils.getStringValue(memberPremiumBasic).replaceAll("(^\\[|\\]$)", ""));
					} else if (arrayObject.getString(Constants.PLAN_TYPE).equals(Constants.ADVANCE)
							|| arrayObject.getString(Constants.PLAN_TYPE).equals(Constants.ADVANCE_VI_QD)) {
						planShortValues = arrayObject.getString(Constants.TOP_SECTION_VALUES);
						protectionLabel = arrayObject.getString(Constants.PROTECTION_LABEL);
						planShortValues = planShortValues
								.replace(
										Constants.CONSTANT_X, Utils
												.convertToViantnumCurrencyTrieu(
														discountedPlanValues.get(Constants.Plans.Advance.name()))
												.toString());
						protectionLabel = protectionLabel.replace(Constants.CONSTANT_Y, quote);
						finalAmount = discountedPlanValues.get(Constants.Plans.Advance.name());
						arrayObject.put(WfImplConstants.MEMBER_PREMIUM, Utils.getStringValue(memberPremiumAdvance).replaceAll("(^\\[|\\]$)", ""));
					} else if (arrayObject.getString(Constants.PLAN_TYPE).equals(Constants.SUPERIOR)
							|| arrayObject.getString(Constants.PLAN_TYPE).equals(Constants.SUPERIOR_VI)) {
						planShortValues = arrayObject.getString(Constants.TOP_SECTION_VALUES);
						protectionLabel = arrayObject.getString(Constants.PROTECTION_LABEL);
						planShortValues = planShortValues
								.replace(
										Constants.CONSTANT_X, Utils
												.convertToViantnumCurrencyTrieu(
														discountedPlanValues.get(Constants.Plans.Superior.name()))
												.toString());
						protectionLabel = protectionLabel.replace(Constants.CONSTANT_Y, quote);
						finalAmount = discountedPlanValues.get(Constants.Plans.Superior.name());
						arrayObject.put(WfImplConstants.MEMBER_PREMIUM, Utils.getStringValue(memberPremiumSuperior).replaceAll("(^\\[|\\]$)", ""));
					}
					arrayObject.put(Constants.TOP_SECTION_VALUES, planShortValues);
					arrayObject.put(Constants.PROTECTION_LABEL, protectionLabel);
					arrayObject.put(Constants.BASIC_PREMIUM, finalAmount);
					arrayList.add(arrayObject);

				}
			}
			finalMap = new ObjectMapper().readValue(arrayList.toString(),
					new TypeReference<List<Map<String, Object>>>() {
					});
			logger.info("quick finale array list**==>>*****  :  " + arrayList);
		} catch (Exception e) {
			logger.error("Error Inside quick quote", e);
			throw new FECException(ErrorCodes.INVALID_QUOTE_DETAILS);

		}
		return finalMap;
	}
}
