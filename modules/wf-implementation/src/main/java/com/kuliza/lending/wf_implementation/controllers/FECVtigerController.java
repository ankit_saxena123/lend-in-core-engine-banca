package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.pojo.GenericDropOffRequest;
import com.kuliza.lending.wf_implementation.services.FECVTigerService;
import com.kuliza.lending.wf_implementation.services.GenericDropOffService;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;

@RestController
@RequestMapping("/fec-vtiger")
public class FECVtigerController {
	
	@Autowired
	private FECVTigerService fECVTigerService;
	
	@Autowired
	private GenericDropOffService genericDropOffService;
	
	private static final Logger logger = LoggerFactory.getLogger(FECVtigerController.class);
	
	@RequestMapping(method = RequestMethod.POST, value = "/mark-drop-off")
	public ApiResponse markDropOffs() throws Exception {
		try {
			return  fECVTigerService.markDropOffs();
		} catch (Exception e) {
			logger.error(Utils.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/vtiger-update-drop-off")
	public ApiResponse vtigerUpdateDropOffs() throws Exception {
		try {
			return  fECVTigerService.vtigerUpdateDropOffs();
		} catch (Exception e) {
			logger.error(Utils.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/mark-generic-drop-off")
	public ApiResponse vTigerMarkGenericDropOff(Principal principal, @RequestBody @Valid GenericDropOffRequest genericDropOffRequest) throws Exception {
		try {
			return  genericDropOffService.markGenericDropOffs(principal,genericDropOffRequest);
		} catch (Exception e) {
			logger.error(Utils.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/update-generic-drop-off")
	public ApiResponse vtigerUpdateGenericDropOff() throws Exception {
		try {
			return  genericDropOffService.vtigerUpdateGenericDropOffs();
		} catch (Exception e) {
			logger.error(Utils.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
	}
	
}
