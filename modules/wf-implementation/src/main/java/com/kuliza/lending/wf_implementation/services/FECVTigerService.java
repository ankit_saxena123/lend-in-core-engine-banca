package com.kuliza.lending.wf_implementation.services;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.UserNotificationDao;
import com.kuliza.lending.wf_implementation.dao.VtigerDropOffDao;
import com.kuliza.lending.wf_implementation.dao.WfUserDeviceRegDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECVtigerIntegration;
import com.kuliza.lending.wf_implementation.models.UserNotificationModel;
import com.kuliza.lending.wf_implementation.models.VtigerDropoffModel;
import com.kuliza.lending.wf_implementation.models.WfUserDeviceRegister;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.notification.SNSMessagePublish;
import com.kuliza.lending.wf_implementation.notification.SNSNotificationMessages;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class FECVTigerService {

	private static final Logger logger = LoggerFactory
	.getLogger(FECVTigerService.class);
	
	@Autowired
	private WfImplConfig wfImplConfig;
	@Autowired
	private TaskService taskService;
	@Autowired
	private VtigerDropOffDao vtigerDropOffDao;
	@Autowired
	private UserNotificationDao userNotificationDao;
	@Autowired
	private WorkflowUserDao workFlowUserDao;
	@Autowired
	private WfUserDeviceRegDao wfUserDeviceRegDao;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private FECVtigerIntegration fecVtigerIntegration;
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private SNSMessagePublish snsMessagePublish;
	@Autowired
	private GenericDropOffService genericDropOffService;

	/**
	* marks the task as drop off in vtiger table.
	*
	* @return ApiResponse
	*/
	public ApiResponse markDropOffs() {
		ApiResponse response;
		try {
			logger.info(">>>>> Inside vtiger markDropOffs() ><<<<<<");
			List<Task> taskList = getDropOffTaskList();
			for (Task task : taskList) {
				String taskName = task.getName();
				String currentJourneyStage = null;
				String productName = null;
				String processInstanceId = task.getProcessInstanceId();
				logger.info(">>>>> vtigerdropoff taskname><<<<<<" + taskName + "><<<vtigerdropoff processInstanceId<<<<<<<"
				+ processInstanceId);
				Map<String, Object> processVariables = runtimeService
						.getVariables(processInstanceId);
				if (processVariables == null) {
					continue;
				}
				String nationalId = Utils.getStringValue(processVariables
						.get(WfImplConstants.NATIONAL_ID_HYPERVERGE));
				String userName = Utils
						.getStringValue(processVariables
								.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
				logger.info("vtiger markup task username=="+userName);
				productName = Utils.getStringValue(processVariables
						.get(WfImplConstants.JOURNEY_PRODUCT_NAME));
				logger.info("vtiger markup task productname=="+productName);
				
				VtigerDropoffModel vtigerModel = vtigerDropOffDao
						.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
								userName, processInstanceId,
								Constants.VTIGER_ENROLMENT_DROP_OFF_TYPE, false);
				currentJourneyStage = Utils.getStringValue(processVariables
						.get(WfImplConstants.APPLICATION_STATUS));
				logger.info("vtiger markup task currentJourneyStage<==>"+currentJourneyStage);
				String mobile="";
				if(processVariables.containsKey(WfImplConstants.APPLICATION_MOBILE_NUMBER)){
					mobile=Utils.getStringValue(processVariables.get(WfImplConstants.APPLICATION_MOBILE_NUMBER));
					if (mobile.startsWith("0")) {
						mobile = mobile.substring(1, mobile.length());
					}
				}
	
				productName = Utils.getStringValue(processVariables
						.get(WfImplConstants.PRODUCT_NAME));
				if (vtigerModel == null) {
					vtigerModel = new VtigerDropoffModel();
					vtigerModel.setStatus(Constants.VTIGER_DROP_OFF);
					vtigerModel.setNationalId(nationalId);
					vtigerModel
					.setDropOffType(Constants.VTIGER_ENROLMENT_DROP_OFF_TYPE);
					vtigerModel.setUserName(userName);
					vtigerModel.setCurrentJourneyStage(currentJourneyStage);
					if (mobile.startsWith("+"))
						vtigerModel.setContactNumber(mobile);
					else
						vtigerModel.setContactNumber("+84"+mobile);
					vtigerModel.setProcessInstanceId(processInstanceId);
					vtigerModel.setUpdatedInVtiger(false);
					vtigerModel.setPushNotificationCount(0);
					VtigerDropoffModel vtigerModelList = vtigerDropOffDao.getTopParentCrmIdOfUser(userName);
					if(vtigerModelList!=null){
						String parentCRMId = vtigerModelList.getvTigerParentCrmId();
						vtigerModel.setvTigerParentCrmId(parentCRMId);
						logger.info("set parent crmid in enrollment markup 1"+parentCRMId);		
					}
					 
					logger.info("<<<<<<<<<<<<<<inserting new Record for processInstanceId : "
							+ processInstanceId);
					logger.info("<==sentnotificationcount==>"+vtigerModel.getPushNotificationCount());
					vtigerDropOffDao.save(vtigerModel);
				}else {
					if (!vtigerModel.getCurrentJourneyStage()
							.equalsIgnoreCase(currentJourneyStage)
							&& vtigerModel.getvTigerContactId() != null) {
						vtigerModel.setPushNotificationCount(0);
						vtigerModel.setUpdatedInVtiger(false);
						if (mobile.startsWith("+"))
							vtigerModel.setContactNumber(mobile);
						else
							vtigerModel.setContactNumber("+84"+mobile);
						vtigerModel.setCurrentJourneyStage(currentJourneyStage);
						vtigerModel.setProcessInstanceId(processInstanceId);
						logger.info("<<<<<<<<<<<updating the Record for processInstanceId : "
								+ processInstanceId);
						VtigerDropoffModel vtigerModelList = vtigerDropOffDao.getTopParentCrmIdOfUser(userName);
						if(vtigerModelList!=null){
							String parentCRMId = vtigerModelList.getvTigerParentCrmId();
							vtigerModel.setvTigerParentCrmId(parentCRMId);
							logger.info("set parent crmid set parent crmid in enrollment markup 2"+parentCRMId);
						}
						vtigerDropOffDao.save(vtigerModel);
						logger.info("<==sentnotificationcount==>"+vtigerModel.getPushNotificationCount());

	
					}
				}
	
			}
	
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.error("Error in markDropOff :" + e.getMessage(), e);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
				Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}
	
	/**
	* gets the list of task which are dropOff
	*
	* @return List<Task>
	*/
	public List<Task> getDropOffTaskList() {
		logger.info("<======>Inside getDropOffTaskList<=========>");
	
		List<Task> dropOffTaskList = new ArrayList<Task>();
		String millSec = wfImplConfig.getDropOffMinTime();
		
		//String millSec = "300000";
		
		//String millSec = System.getenv("VTIGER_TRIGGER_TIME");
		logger.info("<====>VTIGER_MIN TRIGGER_TIME<===>" + millSec);
		long millSeclong = Long.valueOf(millSec);
		Date minDropOff = new Date(System.currentTimeMillis()
				- (millSeclong));
		/*Date maxDropOff = new Date(System.currentTimeMillis()
		- (millSeclong * 480));
	
		Date minDropOffDate = new Date(System.currentTimeMillis() - (60 * 30 * 1000));
		Date maxDropOffDate = new Date(System.currentTimeMillis() - (60 * 30 * 1000 * 96));*/
		String maxSec=wfImplConfig.getDropOffMaxTime();
		
		//String maxSec= "25500000";
		
		logger.info("<====>VTIGER_TRIGGER_MAX_TIME<===>" + maxSec);
		long maxSeclong = Long.valueOf(maxSec);
		Date maxDropOffDate = new Date(System.currentTimeMillis()
				- (maxSeclong));
		List<String> processDefList = Arrays.asList(
				WfImplConstants.FEC_SINGLE_HEALTH_INSURANCEFINAL,
				WfImplConstants.FEC_BANCA_TWOWHEELER,
				WfImplConstants.FEC_BANCA_CC,
				WfImplConstants.FEC_BANCA_PA,
				WfImplConstants.FEC_BANCA_TL,
				WfImplConstants.FEC_BANCA_FAMILY_HEALTH);
	
		List<Task> tasks = taskService.createTaskQuery()
				.taskCreatedBefore(minDropOff)
				.taskCreatedAfter(maxDropOffDate).orderByTaskCreateTime()
				.desc().list();
		if (tasks != null)
			dropOffTaskList = tasks
			.stream()
			.filter(task -> {
				String taskDefKey = task.getTaskDefinitionKey();
				return !(taskDefKey
						.equals(Constants.SH_CONGRATULATION_SCREEN)
						|| taskDefKey
						.equals(Constants.SH_UPLOAD_ERROR_SCREEN)
						|| taskDefKey
						.equals(Constants.SH_CHECKOUT_SCREEN) || taskDefKey
						.equals(Constants.TW_SUCCESS));
			}).collect(Collectors.toList());
		logger.info("<=============>dropOffTaskList size:<===>" + dropOffTaskList.size());
		return dropOffTaskList;
	}
	
	public void updateExpireProductJourneyStageInVtiger(Map<String, Object> processVariable, VtigerDropoffModel dropoff) {
	
		try {
	
			JSONObject requestPayload = fecVtigerIntegration
					.buildRequestPayLoad(processVariable, dropoff,
							true, dropoff.getCurrentJourneyStage());
	
			JSONObject response = null;
			JSONObject uresponse = null;
			response = fecVtigerIntegration
					.getDataFromServiceForCreate(requestPayload);
	
			Map<String, Object> responseMap = fecVtigerIntegration
					.parseResponse(response);
			logger.info("<=============>Inside Vtiger updateExpireProductJourneyStageInVtiger responseforcreate status:<==>"+responseMap.get("status") +"for pid <==>"+dropoff.getProcessInstanceId()
					);
			logger.info("<<<<<<<<<<<<<ResponseMap :" + responseMap);
			if ((boolean) responseMap.get("status")) {
				String contactId = (String) responseMap
						.get("contactId");
				String vtigerCrmId = (String) responseMap
						.get("vtigerCrmId");
				if (contactId != null && vtigerCrmId != null) {
					dropoff.setvTigerContactId(contactId);
					dropoff.setvTigerOpportunityId(vtigerCrmId);
					/*if(dropoff.getvTigerParentCrmId()==null){
						logger.info("set parent crmid"+vtigerCrmId);
	    				dropoff.setvTigerParentCrmId(vtigerCrmId);

					}*/
					if(dropoff.getvTigerParentCrmId()==null){
						VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
						if(vtigerModel!=null){
							if(vtigerModel.getvTigerParentCrmId()!=null){
								logger.info(" VTiger Enrollment set parent crmid 3"+vtigerModel.getvTigerParentCrmId());
								dropoff.setvTigerParentCrmId(vtigerModel.getvTigerParentCrmId());
							}else{
								logger.info(" VTiger Enrollment set parent crmid 4"+vtigerModel.getvTigerParentCrmId());
								dropoff.setvTigerParentCrmId(vtigerCrmId);
							}
						}else{
							logger.info("VTiger Enrollment else set parent crmid 5"+vtigerCrmId);
		    				dropoff.setvTigerParentCrmId(vtigerCrmId);
						}
					}
					dropoff.setUpdatedInVtiger(true);
					dropoff.setInActive(true);
	    	    	logger.info("<=============>Inside Vtiger updateExpireProductJourneyStageInVtiger getvTigerParentCrmId:<==>"+dropoff.getvTigerParentCrmId());

					logger.info("<=============>Vtiger updateExpireProductJourneyStageInVtiger contactid <====>  :"+contactId+"for pid <===>"+dropoff.getProcessInstanceId()
							);
					logger.info("<=============>Vtiger updateExpireProductJourneyStageInVtiger crmid <====>  :"+vtigerCrmId
							);
					logger.info("<=============>Vtiger updateExpireProductJourneyStageInVtiger  setInActive<====>  :"+true
							);
					logger.info("<=============>Vtiger updateExpireProductJourneyStageInVtiger  setUpdatedInVtiger<====>  :"+true
							);
				}else{
					logger.info("<=============>Vtiger updateExpireProductJourneyStageInVtiger contactid and crm id is null for pid <====>  :"+dropoff.getProcessInstanceId()
							);
				}
			}
	
			String userName = Utils
					.getStringValue(processVariable
							.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			logger.info("<=============>Vtiger updateExpireProductJourneyStageInVtiger userName <====>  :"+userName
					);
			List<VtigerDropoffModel> userDropoffJourney1  = vtigerDropOffDao.findByUserNameAndIsUpdatedInVtigerAndDropOffTypeAndIsDeleted(
					userName, true,
					Constants.VTIGER_ENROLMENT_DROP_OFF_TYPE, false);
			logger.info("<=============>Vtiger expire product  user dropoff journey list size<====>  :"
					+ userDropoffJourney1.size()+"for user <==>"+userName);
	
			List<VtigerDropoffModel> userDropoffJourney  = vtigerDropOffDao.findByUserNameAndIsUpdatedInVtigerAndDropOffTypeAndIsDeletedAndProcessInstanceIdNot(
					userName, true,
					Constants.VTIGER_ENROLMENT_DROP_OFF_TYPE, false,dropoff.getProcessInstanceId());
			logger.info("<=============>Vtiger expire product  user dropoff journey list size<====>  :"
					+ userDropoffJourney.size()+"for user <==>"+userName);
	
			vtigerDropOffDao.save(dropoff);
			VtigerDropoffModel vtigerModel1 = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
			if(vtigerModel1!=null)
					logger.info(" VTiger Enrollment set parent crmid 6"+vtigerModel1.getvTigerParentCrmId());
				
	
			for (VtigerDropoffModel userDropoff : userDropoffJourney) {
	
				String upid = userDropoff.getProcessInstanceId();
				ProcessInstance uprocessInstance = runtimeService
						.createProcessInstanceQuery().processInstanceId(upid)
						.includeProcessVariables().singleResult();
	
				if (uprocessInstance == null) {
					userDropoff.setUpdatedInVtiger(true);
					vtigerDropOffDao.save(userDropoff);
					continue;
				}
	
				Map<String, Object> uprocessVariables = uprocessInstance
						.getProcessVariables();
	
				JSONObject urequestPayload = fecVtigerIntegration
						.buildRequestPayLoad(uprocessVariables, userDropoff,
								false, userDropoff.getCurrentJourneyStage());
	
				uresponse = fecVtigerIntegration
						.getDataFromService(urequestPayload);
	
				Map<String, Object> uresponseMap = fecVtigerIntegration
						.parseResponse(uresponse);
				logger.info("<=============>Inside Vtiger updateExpireProduct userJourney response status:<==>"+uresponseMap.get("status") +"for pid <==>"+userDropoff.getProcessInstanceId()
						);
				logger.info("<<<<<<<<<<<<<ResponseMap :" + uresponseMap);
				if ((boolean) uresponseMap.get("status")) {
					String contactId = (String) uresponseMap
							.get("contactId");
					String vtigerCrmId = (String) uresponseMap
							.get("vtigerCrmId");
					if (contactId != null && vtigerCrmId != null) {
						userDropoff.setvTigerContactId(contactId);
						userDropoff.setvTigerOpportunityId(vtigerCrmId);
						userDropoff.setUpdatedInVtiger(true);
						userDropoff.setInActive(false);
						/*if(userDropoff.getvTigerParentCrmId()==null){
							logger.info("set parent crmid"+vtigerCrmId);
							userDropoff.setvTigerParentCrmId(vtigerCrmId);
						}*/
						if(userDropoff.getvTigerParentCrmId()==null){
							VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(userDropoff.getProcessInstanceId());
							if(vtigerModel!=null){
								if(vtigerModel.getvTigerParentCrmId()!=null){
									logger.info(" VTiger Enrollment set parent crmid 7"+vtigerModel.getvTigerParentCrmId());
									userDropoff.setvTigerParentCrmId(vtigerModel.getvTigerParentCrmId());
								}else{
									logger.info(" VTiger Enrollment set parent crmid 8"+vtigerModel.getvTigerParentCrmId());
									userDropoff.setvTigerParentCrmId(vtigerCrmId);
								}
							}else{
								logger.info("VTiger Enrollment else set parent crmid 9"+vtigerCrmId);
								userDropoff.setvTigerParentCrmId(vtigerCrmId);
							}
						}

						vtigerDropOffDao.save(userDropoff);
						VtigerDropoffModel vtigerModel2 = vtigerDropOffDao.findByProcessInstanceId(userDropoff.getProcessInstanceId());
						if(vtigerModel2!=null)
								logger.info(" VTiger Enrollment set parent crmid 10"+vtigerModel2.getvTigerParentCrmId());
						
		    	    	logger.info("<=============>Inside Vtiger updateExpireProductJourneyStageInVtiger1 getvTigerParentCrmId:<==>"+userDropoff.getvTigerParentCrmId());

						logger.info("<=============>Vtiger updateExpireProduct userJourneyStageInVtiger contactid <====>  :"+contactId+"<===for user journey pid===>"+userDropoff.getProcessInstanceId()
								);
						logger.info("<=============>Vtiger updateExpireProduct userJourneyStageInVtiger crmid <====>  :"+vtigerCrmId
								);
						logger.info("<=============>Vtiger updateExpireProduct userJourneyStageInVtiger  setInActive<====>  :"+false
								);
						logger.info("<=============>Vtiger updateExpireProduct userJourneyStageInVtiger  setUpdatedInVtiger<====>  :"+true
								);
					}else{
						logger.info("<=============>Vtiger updateExpireProduct userJourneyStageInVtiger contactid and crm id is null for pid <====>  :"+userDropoff.getProcessInstanceId()
								);
					}
				}
	
			}
		}catch(Exception ex){
			logger.error("Error in updateExpireProductJourneyStageInVtiger for pid <==>:"+dropoff.getProcessInstanceId()+"<=========>" + ex);
	
		}
	
	}
	
	   
	public void updateActiveProductJourneyStageInVtiger(Map<String, Object> processVariable, VtigerDropoffModel dropoff) throws Exception{
	    try{
	   
	    	logger.info("<=============>Inside Vtiger updateActiveProductJourneyStageInVtiger   for pid:<==>"+dropoff.getProcessInstanceId()
	    			);
	    	JSONObject requestPayload = fecVtigerIntegration
	    			.buildRequestPayLoad(processVariable, dropoff,
	    					true, dropoff.getCurrentJourneyStage());
	    	JSONObject response = null;
	
	    	response = fecVtigerIntegration
	    			.getDataFromService(requestPayload);
	    	Map<String, Object> responseMap = fecVtigerIntegration
	    			.parseResponse(response);
	    	logger.info("<=============>Inside Vtiger updateActiveProductJourneyStageInVtiger response status:<==>"+responseMap.get("status") +"for pid <==>"+dropoff.getProcessInstanceId()
	    			);
	    	if ((boolean) responseMap.get("status")) {
	    		String contactId = (String) responseMap
	    				.get("contactId");
	    		String vtigerCrmId = (String) responseMap
	    				.get("vtigerCrmId");
	    		if (contactId != null && vtigerCrmId != null) {
	    			dropoff.setvTigerContactId(contactId);
	    			dropoff.setvTigerOpportunityId(vtigerCrmId);
	    			/*if(dropoff.getvTigerParentCrmId()==null){
						logger.info("set parent crmid"+vtigerCrmId);
	    				dropoff.setvTigerParentCrmId(vtigerCrmId);
	    			}*/
	    			if(dropoff.getvTigerParentCrmId()==null){
						VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
						if(vtigerModel!=null){
							if(vtigerModel.getvTigerParentCrmId()!=null){
								logger.info(" VTiger Enrollment set parent crmid11"+vtigerModel.getvTigerParentCrmId());
								dropoff.setvTigerParentCrmId(vtigerModel.getvTigerParentCrmId());
							}else{
								logger.info(" VTiger Enrollment set parent crmid12"+vtigerModel.getvTigerParentCrmId());
								dropoff.setvTigerParentCrmId(vtigerCrmId);
							}
						}else{
							logger.info("VTiger Enrollment else set parent crmid13"+vtigerCrmId);
		    				dropoff.setvTigerParentCrmId(vtigerCrmId);
						}
					}
	    			dropoff.setUpdatedInVtiger(true);
	    			dropoff.setInActive(true);
	    			vtigerDropOffDao.save(dropoff);
	    			VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
					if(vtigerModel!=null)
						logger.info(" VTiger Enrollment set parent crmid14"+vtigerModel.getvTigerParentCrmId());

	    	    	logger.info("<=============>Inside Vtiger updateActiveProductJourneyStageInVtiger getvTigerParentCrmId:<==>"+dropoff.getvTigerParentCrmId());

	    			logger.info("<=============>Vtiger updateActiveProductJourneyStageInVtiger contactid <====>  :"+contactId+"for pid <===>"+dropoff.getProcessInstanceId()
	    					);
	    			logger.info("<=============>Vtiger updateActiveProductJourneyStageInVtiger crmid <====>  :"+vtigerCrmId
	    					);
	    			logger.info("<=============>Vtiger updateActiveProductJourneyStageInVtiger  setInActive<====>  :"+true
	    					);
	    			logger.info("<=============>Vtiger updateActiveProductJourneyStageInVtiger  setUpdatedInVtiger<====>  :"+true
	    					);
	    		}else{
	    			logger.info("<=============>Inside Vtiger updateActiveProductJourneyStageInVtiger   for pid:<==>"+dropoff.getProcessInstanceId() +"contact id and crm id is null"
	    					);
	    		}
	
	    	}
	    }catch(Exception e){
	    	logger.error("Error in updateActiveProductJourneyStageInVtiger for pid:<===>"+dropoff.getProcessInstanceId() + e.getMessage(), e);
	    }
	   
	}
	
	public ApiResponse vtigerUpdateDropOffs() {
		ApiResponse response = null;
	
		try {
			logger.info("<=============>Vtiger Dropoff vtigerUpdateDropOffs <====>  :"
					);
	
			List<VtigerDropoffModel> dropoffList = vtigerDropOffDao
					.findByDropOffTypeAndIsUpdatedInVtigerAndIsDeleted(
							Constants.VTIGER_ENROLMENT_DROP_OFF_TYPE, false, false);
	
			logger.info("<=============>Vtiger Dropoff vtigerUpdateDropOffs list size<====>  :"
					+ dropoffList.size());
			for (VtigerDropoffModel dropoff : dropoffList) {
				logger.info("<=============>Vtiger Dropoff VtigerDropoffModel list <====>  :");
	
				String pid = dropoff.getProcessInstanceId();
				ProcessInstance processInstance = runtimeService
						.createProcessInstanceQuery().processInstanceId(pid)
						.includeProcessVariables().singleResult();
				logger.info("<=============>Vtiger Dropoff Process Inst<====>  :"
						+ pid);
				if (processInstance == null) {
					dropoff.setUpdatedInVtiger(true);
					vtigerDropOffDao.save(dropoff);
					continue;
				}
	
				logger.info("<=============>pushNotification count <==>:"
						+ dropoff.getPushNotificationCount());
				logger.info("<=============>dropoff pid :<==>"
						+ dropoff.getProcessInstanceId());
				Map<String, Object> processVariables = processInstance
						.getProcessVariables();
				logger.info("<=============>pushNotification count :<==>"
						+ dropoff.getPushNotificationCount());
	
				if (dropoff.getPushNotificationCount() < 2) {
					sendPushNotification(dropoff, processVariables);
				} else {
	
					Date modified = dropoff.getModified();
					Date currentDate = new Date();
					long diff = currentDate.getTime() - modified.getTime();
					long diffInMillSec = TimeUnit.MILLISECONDS.toMillis(diff);
					String millSec ="";
					String ref_code = Utils.getStringValue(processVariables.get(Constants.REF_CODE));

					if("".equals(ref_code)){
						millSec = wfImplConfig.getvTigerTriggerTime();
						logger.info("vtiger trigger time"+millSec);
					}else{
						millSec = wfImplConfig.getvTigerRefCodeTriggerTime();
						logger.info("vtiger refcode trigger time"+millSec);
					}	
					//String millSec = "300000";
					long millSeclong = Long.valueOf(millSec);
					logger.info("<=======>diffInMillSec : " + diffInMillSec
							+ "<=====>millSeclong :" + millSeclong);
	
					boolean callVtiger=false;
					// call vtiger immediately when there is a change in the state, ie will updating vtiger dropOff
					/*if(dropoff.getvTigerContactId() != null){
					callVtiger=true;
					}
					else{
					if (diffInMillSec > millSeclong) {
					callVtiger=true;
					}
					}*/
					if (diffInMillSec > millSeclong) {
						callVtiger=true;
					}
					if (callVtiger) {
						List<VtigerDropoffModel> VtigerGenericDropoffModelList= vtigerDropOffDao.findByUserNameAndIsUpdatedInVtigerAndDropOffTypeAndIsDeleted(dropoff.getUserName(), true, Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
						for(VtigerDropoffModel vtigerDropoffModel:VtigerGenericDropoffModelList) {
							logger.info("<=============>disableGenericDropoffStatusAtVtiger for user<====>  :"+vtigerDropoffModel.getUserName());

							genericDropOffService.disableGenericDropoffStatusAtVtiger(vtigerDropoffModel);
						}
						logger.info("<=============>call vtiger true for pid<====>  :"+pid
								);
						if (dropoff.getvTigerContactId() != null && dropoff.getvTigerOpportunityId() != null) {
							logger.info("<=============>Vtiger Dropoff  Not Null Contact and Not Null Opportunity with pid<====>  :"+pid
									);
	
	
							if(dropoff.isInActive()==true){
								logger.info("<=============>Vtiger Dropoff  Not Null Contact and Not Nul Opportunity  and Active true with pid<====>  :"+pid
										);
	
								updateActiveProductJourneyStageInVtiger(processVariables, dropoff
										);
	
							}else{
								logger.info("<=============>Vtiger Dropoff  Not Null Contact and Not Nul Opportunity  and Active false with pid<====>  :"+pid
										);
								modified = dropoff.getModified();
								diff = currentDate.getTime() - modified.getTime();
								diffInMillSec = TimeUnit.MILLISECONDS.toMillis(diff);
								//millSec = wfImplConfig.getvTigerExpireTime();
								millSec = "4200000";
								millSeclong = Long.valueOf(millSec);
								logger.info("<=======>diffInMillSec : " + diffInMillSec
										+ "<=====>millSeclong :" + millSeclong);
								VtigerDropoffModel userDropoffJourney  = vtigerDropOffDao.findByProcessInstanceIdAndIsDeleted(
										pid, true );
	
								if (userDropoffJourney!=null) {
									logger.info("<=============>Vtiger Dropoff  Not Null Contact and Not NULL Opportunity  and expiry true for pid<====>  :"+pid
											);
	
									updateExpireProductJourneyStageInVtiger(processVariables, dropoff
											);
								}else {
									logger.info("<=============>Vtiger Dropoff  Not Null Contact and Not Null Opportunity  and Active false and expiry false for pid<====>  :"+pid
											);
	
									updateActiveProductJourneyStageInVtiger(processVariables, dropoff
											);
								}
							}
	
	
						} else {
							logger.info("<=============>Vtiger Dropoff Inside  Null Contact and  Null Opportunity ID for pid<====>  :"+pid
									);
							updateExpireProductJourneyStageInVtiger(processVariables, dropoff
									);
	
	
						}
					}
	
				}
			}
	
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.error("Error in vtigerUpdateDropOffs :" + e.getMessage(), e);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.SOMETHING_WRONG_MESSAGE);
		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
	
	
	}
	
	private String chooseProductAndStage(String productName,String journey){
	
		String product ="";
		String stage ="";
		switch(productName){
	
			case "Single Health Protection":
				product = "SH";
				logger.info("<==***Single Health Protection product***==>");
				stage = chooseSHStage(journey);
				logger.info("<==***Single Health Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Motor Protection":
				product = "TW";
				logger.info("<==***TW  Protection product***==>");
				stage = chooseTWStage(journey);
				logger.info("<==***TW Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Term Life Protection":
				product = "TL";
				logger.info("<==***TL  Protection product***==>");
				stage = chooseTLStage(journey);
				logger.info("<==***TL Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Personal Accident Protection":
				product = "PA";
				logger.info("<==***Personal Accident Protection product***==>");
				stage = choosePAStage(journey);
				logger.info("<==***Personal Accident Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Cancer Care":
				product = "CC";
				logger.info("<==***Cancer Care  Protection product***==>");
				stage = chooseCCStage(journey);
				logger.info("<==***Cancer Care Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Family Health":
				product = "FH";
				logger.info("<==***Family Health  Protection product***==>");
				stage = chooseFHStage(journey);
				logger.info("<==***Family Health Protection stage***==>"+stage+"<==***==>");
			break;
			
			case "Bao hiem suc khoe ca nhan":
				product = "SH";
				logger.info("<==***Single Health Protection product***==>");
				stage = chooseSHStage(journey);
				logger.info("<==***Single Health Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Bao hiem xe may":
				product = "TW";
				logger.info("<==***TW  Protection product***==>");
				stage = chooseTWStage(journey);
				logger.info("<==***TW Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "An tam tron ven":
				product = "TL";
				logger.info("<==***TL  Protection product***==>");
				stage = chooseTLStage(journey);
				logger.info("<==***TL Protection stage***==>"+stage+"<==***==>");
			break;
			
			
			case "Bao hiem tai nan":
				product = "PA";
				logger.info("<==***Personal Accident Protection product***==>");
				stage = choosePAStage(journey);
				logger.info("<==***Personal Accident Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Bao hiem ung thu":
				product = "CC";
				logger.info("<==***Cancer Care  Protection product***==>");
				stage = chooseCCStage(journey);
				logger.info("<==***Cancer Care Protection stage***==>"+stage+"<==***==>");
			break;
	
			case "Bao hiem suc khoe gia đinh":
				product = "FH";
				logger.info("<==***Family Health  Protection product***==>");
				stage = chooseFHStage(journey);
				logger.info("<==***Family Health Protection stage***==>"+stage+"<==***==>");
			break;
			default :
				product = "";
				logger.info("<==***No Product and No stage***==>");
				stage = "";
			break;
	
		}
		if(""==stage){
			logger.info("<==***No stage***==>");

			return  "";
		}
		return product+stage;
	}
	
	private String choosePAStage(String journey){
	
		String stage ="";
	
		switch(journey){
	   
			case "Selection NID":
				stage = "S1";
			break;
	
			case "Review":
				stage = "S2";
			break;
	
			case "Splash":
				stage = "S2";
			break;
	
			case "Application":
				stage = "S3";
			break;
	
			case "Payment":
				stage = "S4";
			break;
	
			case "completed":
				stage = "S5";
			break;
	
			default :
				stage = "";
			break;
	
		}
	
	return stage;
	}
	
	private String chooseCCStage(String journey){
	
		String stage ="";
	
		switch(journey){
	
		case "Selection NID":
			stage = "S1";
		break;

		case "Review":
			stage = "S2";
		break;

		case "Splash":
			stage = "S2";
		break;

		case "Question":
			stage = "S3";
		break;
		
		case "Application":
			stage = "S4";
		break;

		case "Payment":
			stage = "S5";
		break;

		case "completed":
			stage = "S6";
		break;
		
		default :
			stage = "";
		break;
	
		}
	
	return stage;
	}
	
	private String chooseFHStage(String journey){
	
		String stage ="";
	
		switch(journey){
	
		case "Selection NID":
			stage = "S1";
		break;

		case "Review":
			stage = "S2";
		break;

		case "Splash":
			stage = "S2";
		break;

		case "Question":
			stage = "S3";
		break;
		
		case "Application":
			stage = "S4";
		break;

		case "Payment":
			stage = "S5";
		break;

		case "completed":
			stage = "S6";
		break;
		
		default :
			stage = "";
		break;
	
	
		}
	
	return stage;
	}
	
	private String chooseSHStage(String journey){
		
		String stage ="";
	
		switch(journey){
	   
			case "Selection NID":
				stage = "S1";
			break;
	
			case "Review":
				stage = "S2";
			break;
	
			case "Splash":
				stage = "S2";
			break;
	
			case "Question":
				stage = "S3";
			break;
	
			case "recheck":
				stage = "S3";
			break;
	
			case "Application":
				stage = "S4";
			break;
	
			case "Payment":
				stage = "S5";
			break;
	
			case "completed":
				stage = "S6";
			break;
	
			default :
				stage = "";
			break;
	
		}
	
	return stage;
	}
	
	private String chooseTWStage(String journey){
	
		String stage ="";
	
		switch(journey){
	
			case "Selection NID":
				stage = "S1";
				break;
	
			case "Questionaire  Review":
				stage = "S2";
				break;
	
			case "Questionaire Review":
				stage = "S2";
				break;
	
			case "Questionaire Screen":
				stage = "S2";
				break;
	
			case "Questionaire  Screen":
				stage = "S2";
				break;
	
			case "Static Selection Screen":
				stage = "S3";
				break;
	
			case "Photo Splash Screen":
				stage = "S3";
				break;
	
			case "Review NID":
				stage = "S4";
				break;
	
			case "Upload Vehicle Screen":
				stage = "S5";
				break;
	
			case "Application Review Screen":
				stage = "S6";
				break;
	
			case "Payment Screen":
				stage = "S7";
				break;
	
			case "completed":
				stage = "S8";
				break;
	
			default :
				stage = "";
				break;
	
		}
	
	return stage;
	}
	
	private String chooseTLStage(String journey){
	
		String stage ="";
	
		switch(journey){
	
			case "Selection NID":
				stage = "S1";
				break;
	
			case "Review":
				stage = "S2";
				break;
	
			case "Beneficiary Splash":
				stage = "S2";
				break;
	
			case "Beneficiary  Splash":
				stage = "S2";
				break;
	
			case "Beneficiary":
				stage = "S2";
				break;
	
			case "Splash":
				stage = "S2";
				break;
	
			case "Question":
				stage = "S3";
				break;
	
			case "Application":
				stage = "S4";
				break;
	
			case "Payment":
				stage = "S5";
				break;
	
			case "recheck":
				stage = "S5";
				break;
	
			case "Paymentcompleted":
				stage = "S6";
				break;
	
			default :
				stage = "";
				break;
	
		}
	
	return stage;
	}
	
	private String decode(String str) {
		String decoded = null;
		try{
	       String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
	       Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
	       decoded=pattern.matcher(nfdNormalizedString).replaceAll("");
		}catch(Exception e)   {
			logger.error(
					"<========>Exception while decode push notiofication: "
								+ e.getMessage(), e);
		}
	
		return decoded;
	}
	
	private void sendPushNotification(VtigerDropoffModel dropoff,Map<String, Object> processVariables) {
		try {
			Date modified = dropoff.getModified();
			Date currentDate = new Date();
			long diff = currentDate.getTime() - modified.getTime();
			long diffInMillSec = TimeUnit.MILLISECONDS.toMillis(diff);
			//String millSec = wfImplConfig.getPushNotificationTime();
			String millSec = "";
			if(dropoff.getPushNotificationCount() == 0)
				millSec = wfImplConfig.getPushFirstNotificationTime();
			else 
				millSec = wfImplConfig.getPushSecondNotificationTime();
			
			//String millSec = "300000";
			int notifyCount = 0;
			long millSeclong = Long.valueOf(millSec);
			logger.info("<=======>diffInMillSec : " + diffInMillSec
					+ "<=====>millSeclong :" + millSeclong);
			boolean sendPushNotification=false;
	
			if (diffInMillSec > millSeclong){
				if(dropoff.getPushNotificationCount()==0){
					sendPushNotification=true;
					notifyCount = 1;
				}else{
					sendPushNotification=true;
					notifyCount = 2;
				}
					
			}
			
			if (sendPushNotification) {
				logger.info("<============>sendPushNotification is true<============>");
				WorkFlowUser workFlowUser = workFlowUserDao
						.findByIdmUserNameAndIsDeleted(dropoff.getUserName(),
								false);
				if (workFlowUser != null) {
					String arnEndPoint = workFlowUser.getArnEndPoint();
					logger.info("<========>arnEndPoint :" + arnEndPoint);
					logger.info("<========>mobileNo:"
							+ workFlowUser.getMobileNumber());
					logger.info("<========>customer name:"
							+ workFlowUser.getUsername());
					String language = Utils.getStringValue(processVariables.get(Constants.LANGUAGE_KEY));
					
					logger.info("<========>Process Var language :" + language);
					if( Constants.BANCA_DEFAULT_LANGUAGE.equals(language.trim()))
						language="VN";
					else if(Constants.LANGUAGE_VALUE.equals(language.trim()))
						language="EN";
					else
						language="VN";
					logger.info("<========>language :" + language);
	
					List<WfUserDeviceRegister> workFlowDeviceList = wfUserDeviceRegDao
							.findByWfUserAndIsDeleted(workFlowUser, false);
					if (workFlowDeviceList.size() > 0) {
						WfUserDeviceRegister wfRegister = workFlowDeviceList
								.get(0);
						String customer_name = workFlowUser.getUsername();
						String productName=Utils.getStringValue(processVariables.get(WfImplConstants.JOURNEY_PRODUCT_NAME));
						logger.info("<==========>Befpdecode:"+productName);
						String dproductName=Utils.getStringValue(decode(productName));
						logger.info("<==========>pdecode:"+dproductName);
						String curJourney = dropoff.getCurrentJourneyStage();
						logger.info("<==========>Befjourney:"+curJourney);
						logger.info("<==========>Aftjourney:"+Utils.getStringValue(decode(curJourney)));
						logger.info("<==========>current mobile number :"+dropoff.getContactNumber());
						String notifyProductAndStage = chooseProductAndStage(dproductName.trim(),curJourney.trim());
						logger.info("<==========>notifyProductAndStage:"+notifyProductAndStage+"<=====>");

						String notifyMsgKey = notifyProductAndStage+language+"N"+notifyCount;
						logger.info("<========>notifyMsgKey :" + notifyMsgKey);
						String notifyMsgEn="";String notifyMsgVn="";String title="";
						if("VN"==language){
							title=Constants.DROP_OFF_PUSH_NOTIFICATION_TITLE_VN;
						}else {
							title=Constants.DROP_OFF_PUSH_NOTIFICATION_TITLE_EN;
						}
	
						String notifyMsg = SNSNotificationMessages.notificationMsgs.get(notifyMsgKey);
						logger.info("<========>notifyMsg :" + notifyMsg);
	
						if(notifyMsg == null){
							if("EN"==language){
								if(notifyCount==1){
									notifyMsgKey="DHENN1";
									notifyMsg = SNSNotificationMessages.notificationMsgs.get("DHENN1");
								}
								else{
									notifyMsgKey="DHENN2";
									notifyMsg = SNSNotificationMessages.notificationMsgs.get("DHENN2");
								}	
								title=Constants.DROP_OFF_PUSH_NOTIFICATION_TITLE_EN;
							}else{
								if(notifyCount==1){
									notifyMsgKey="DHVNN1";
									notifyMsg = SNSNotificationMessages.notificationMsgs.get("DHVNN1");
								}else{
									notifyMsgKey="DHVNN2";
									notifyMsg =  SNSNotificationMessages.notificationMsgs.get("DHVNN2"); 
								}	
								title=Constants.DROP_OFF_PUSH_NOTIFICATION_TITLE_VN;
	
							}
						}
						logger.info("<===========>push notification body :"+notifyMsg);
						logger.info("<===========>vtiger genericpush notification customer_name :"+customer_name);
						logger.info("<===========>vtiger genericpush notification productName :"+productName);
						//body=body.replace("${policy_name}", productName);
						//String body=Constants.DROP_OFF_PUSH_NOTIFICATION_BODY;
						if(customer_name==null){
							notifyMsg=notifyMsg.replace("<customer_name>","");
						}else{
							notifyMsg=notifyMsg.replace("<customer_name>",customer_name);
						}
						if(productName==null){
							notifyMsg=notifyMsg.replace("<product_name>", "");
						}else{
							notifyMsg=notifyMsg.replace("<product_name>", productName);
						}
						logger.info("<===========>push notification body :"+notifyMsg);
						UserNotificationModel userNotificationModel = new UserNotificationModel();
						userNotificationModel.setNotificationMessage(notifyMsg);
						userNotificationModel.setNotificationDetailedMessage(notifyMsg);
						userNotificationModel.setContactNumber(workFlowUser.getMobileNumber());
						userNotificationModel.setNationalId(workFlowUser.getNationalId());
						userNotificationModel.setProcessInstanceId(dropoff.getProcessInstanceId());
						userNotificationModel.setUserName(dropoff.getUserName());
						
						userNotificationDao.save(userNotificationModel);
						String notificationMessage = "";
						logger.info("<===========>notificationMessage:"
								+ notificationMessage);
						if(workFlowUser.isPushNotification())
							notificationMessage = snsMessagePublish
								.publishMessageToEndpoint(notifyMsg,title, arnEndPoint,
										wfRegister.getDeviceType());
						
						int count = dropoff.getPushNotificationCount();
						dropoff.setPushNotificationCount(count + 1);
						vtigerDropOffDao.save(dropoff);
					}
	
				}
			}
		}catch (Exception e) {
			logger.error(
					"<========>Exception while sending push notiofication: "
							+ e.getMessage(), e);
		}
	
	}
}