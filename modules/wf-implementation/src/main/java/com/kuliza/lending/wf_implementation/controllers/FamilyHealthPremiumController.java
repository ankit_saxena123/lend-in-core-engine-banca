package com.kuliza.lending.wf_implementation.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.FamilyPremiumPojo;
import com.kuliza.lending.wf_implementation.services.FamilyHealthPremiumService;

@RestController
@RequestMapping("/fh")
public class FamilyHealthPremiumController {

	@Autowired
	private FamilyHealthPremiumService FHSercive;

	private static final Logger logger = LoggerFactory.getLogger(FamilyHealthPremiumController.class);

	@PostMapping("/premium/{slug}")
	public ResponseEntity<Object> getmaster(@Valid @PathVariable("slug") String slug,
			@RequestBody FamilyPremiumPojo input) {
		try {

			logger.info("******************FHService : + " + input.getLanguage());

			return CommonHelperFunctions.buildResponseEntity(FHSercive.FamilyPremium(slug, input));

		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}

}
