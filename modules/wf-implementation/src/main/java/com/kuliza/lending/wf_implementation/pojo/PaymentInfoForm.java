package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;


public class PaymentInfoForm {

	@NotNull
	String mobileNumber;
	
	String crmId;
	
	@NotNull
	String nationalId;
	
	@NotNull
	String premium;
	
	@NotNull
	String customerName;
	
	@NotNull
	String dueDate;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCrmId() {
		return crmId;
	}

    public PaymentInfoForm(){

    	super();

    }
    
	public PaymentInfoForm(String mobileNumber, String crmId, String nationalId, String premium, String customerName,
			String dueDate) {
		super();
		this.mobileNumber = mobileNumber;
		this.crmId = crmId;
		this.nationalId = nationalId;
		this.premium = premium;
		this.customerName = customerName;
		this.dueDate = dueDate;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	
	
}
