package com.kuliza.lending.wf_implementation.pojo;

public class GenericDropOffRequest {

	private String userName;
	private String contactNumber;
	private String currentJourneyScreenName;
	private String language;
	private String productSelection;

	public String getUserName() {
		return userName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public String getCurrentJourneyScreenName() {
		return currentJourneyScreenName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public void setCurrentJourneyScreenName(String currentJourneyScreenName) {
		this.currentJourneyScreenName = currentJourneyScreenName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getProductSelection() {
		return productSelection;
	}
	public void setProductSelection(String productSelection) {
		this.productSelection = productSelection;
	}

	@Override
	public String toString() {
		return "GenericDropOffRequest [userName=" + userName + ", contactNumber=" + contactNumber
				+ ", currentJourneyScreenName=" + currentJourneyScreenName + ", language=" + language
				+ ", productSelection=" + productSelection + "]";
	}
}
