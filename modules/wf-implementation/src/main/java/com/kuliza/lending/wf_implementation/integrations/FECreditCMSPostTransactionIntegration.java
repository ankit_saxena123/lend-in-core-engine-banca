package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditCMSPostTransactionIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(FECreditCMSPostTransactionIntegration.class);

	public JSONObject requestPayload(Map<String, Object> data, String accNo, String tenor, String premiumAmount,
			String tranxNo, String refNo) throws Exception {
		String productId = Utils.getStringValue(data.get(WfImplConstants.JOURNEY_PRODUCT_ID));
		String plan_id = Utils.getStringValue(data.get(WfImplConstants.JOURNEY_PLAN_ID));
		String ippFlag = Constants.POST_TRANSACTION_IPP_FLAG;
		String actionCode = WfImplConstants.CMS_ACTION_CODE_MAP.get(getProductAndPlanId(productId, plan_id));
		logger.info("<--- actionCode  ---> " + actionCode);

		JSONObject requestPayload = new JSONObject();
		JSONObject requestBody = new JSONObject();
		logger.info("<--- Inside_Post_Transaction_Request_Payload --->");
		requestBody.put("cms:PostTransaction.Tenor", tenor);
		requestBody.put("cms:PostTransaction.Account", accNo);
		if (tenor.equals("0")) {
			ippFlag = "false";
		}
		requestBody.put("cms:PostTransaction.IPP_FLAG", ippFlag);
		logger.info("<=========>ProductId :" + productId + "<=================>" + plan_id);
		logger.info("cms:PostTransaction.ActionCode ---> " + actionCode);
		requestBody.put("cms:PostTransaction.ActionCode", actionCode);
		// requestBody.put("cms:PostTransaction.InsProduct",
		// Constants.POST_TRANSACTION_INS_PRODUCT);
		requestBody.put("cms:PostTransaction.ReferenceNo", refNo); // remove
		requestBody.put("cms:PostTransaction.Sys.TransID", tranxNo);
		requestBody.put("cms:PostTransaction.Sys.DateTime", Utils.getCurrentDateTime());
		requestBody.put("cms:PostTransaction.PremiumAmount", premiumAmount);
		requestBody.put("cms:PostTransaction.Sys.RequestorID", Constants.FEC_REQUESTOR_ID);

		requestPayload.put("body", requestBody);
		logger.info("requestPayload ---> " + requestPayload);
		return requestPayload;
	}

	private String getProductAndPlanId(String productId, String plan_id) {
		String concatPlanAndProductId = (new StringBuilder()).append(productId).append("-").append(plan_id).toString();
		logger.info("concatPlanAndProductId ---> " + concatPlanAndProductId);
		return concatPlanAndProductId;
	}

	public JSONObject getDataFromService(JSONObject requestPayload) throws Exception {
		logger.info("--------->>>>>>>>>>>>> inside PT getDataFromService******** ");
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();

			headersMap.put("Content-Type", Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.CMS_CHECK_ELIGIBILITY_SOAP_SLUG_KEY
					+ Constants.CMS_CHECK_ELIGIBILITY_SLUG + "/" + Constants.CMS_POST_TRANSACTION_KEY + "/"
					+ wfImplConfig.getIbCompany() + "/" + Constants.CMS_CHECK_ELIGIBILITY_HASH + "/?env="
					+ wfImplConfig.getIbEnv() + Constants.CMS_CHECK_ELIGIBILITY_INTEGRATION_SOAP_KEY;
			logger.info("--------->>>>>>>>>>>>> " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call CMS api :" + e.getMessage(), e);
		}
		logger.info("<-- ***************Exiting PT getDataFromService()**************");
		return responseJson;
	}

	public Map<String, Object> parseResponse(JSONObject responseFromIB) throws Exception {
		logger.info("*********** Innside PT Parse Response  ***************");
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (responseFromIB != null && responseFromIB.has(Constants.POST_TRANSACTION_POST_TRANX_RESPONSE)) {
			map.put(Constants.STATUS_MAP_KEY, true);

			JSONObject details = new JSONObject();
			details = responseFromIB.getJSONObject(Constants.POST_TRANSACTION_POST_TRANX_RESPONSE)
					.getJSONObject(Constants.POST_TRANSACTION_SYS);
			map.put(Constants.POST_TRANSACTION_DESCRIPTION, details.get(Constants.POST_TRANSACTION_DESCRIPTION));
			map.put(Constants.POST_TRANSACTION_CODE, details.get(Constants.POST_TRANSACTION_CODE));
			logger.info("*********** EXITING PT Parse Response map ***************");
			return map;
		} else {
			map.put(Constants.STATUS_MAP_KEY, false);
			map.put(Constants.CMS_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
			return map;
		}

	}


}

