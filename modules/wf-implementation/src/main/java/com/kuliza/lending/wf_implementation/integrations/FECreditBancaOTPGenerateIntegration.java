package com.kuliza.lending.wf_implementation.integrations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.OTPConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditBancaOTPGenerateIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;
	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaOTPGenerateIntegration.class);

	public JSONObject buildRequestPayload(Map<String, Object> data) throws JSONException {
		JSONObject requestPayload = new JSONObject();
		JSONObject sysRequest = new JSONObject();
		sysRequest.put("srv:GenerateOTP.sys.TransID", Utils.generateUUIDTransanctionId());
		sysRequest.put("srv:GenerateOTP.sys.RequestorID", Constants.FEC_REQUESTOR_ID_OTP);
		String mobile = Utils.getStringValue(data.get(Constants.MOBILE_NUMBER_KEY));
		if (mobile.startsWith("0")) {
			mobile = mobile.substring(1, mobile.length());
		}
		logger.info("<========>mobile No : <==============>"+mobile);
		sysRequest.put("srv:GenerateOTP.phone", "+84" + mobile);
		sysRequest.put("srv:GenerateOTP.otpTTL", Constants.FEC_OTP_TTL);
		sysRequest.put("srv:GenerateOTP.otpType", Constants.FEC_OTP_TYPE);
		sysRequest.put("srv:GenerateOTP.otpLength", Constants.FEC_OTP_LENGTH);
		sysRequest.put("srv:GenerateOTP.otpAttempts", Constants.FEC_OTP_ATTEMPTS);
		sysRequest.put("srv:GenerateOTP.textSuf", Constants.FEC_TEXT_SUF);
		sysRequest.put("srv:GenerateOTP.textPref", Constants.FEC_TEXT_PREF);
		sysRequest.put("srv:GenerateOTP.sys.DateTime", Utils.getCurrentDateTime());
		requestPayload.put("body", sysRequest);
		return requestPayload;
	}

	@Override
	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put("Content-Type", Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.GENERATE_VALIDATE_OTP_INTEGRATION_SLUG + "/" + Constants.GENERATE_OTP_SLUG
				+ "/" + wfImplConfig.getIbCompany() + "/" + Constants.IB_HASH_VALUE
				+ "/?env="+wfImplConfig.getIbEnv()+Constants.OTP_INTEGRATION_SOAP_KEY;
		logger.info("HashGenerationURL ::>>>>>>>>>>>>>>>>>>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		return resposeAsJson;
	}

	@Override
	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		Map<String, Object> processVariablesFromSMSIntegration = new HashMap<>();
		List<String> successCodes = new ArrayList<String>();
		successCodes.add(Constants.FE_CREDIT_OTP_GENERATION_SUCCESS_CODE_KEY);
		if (processVariablesFromSMSIntegration.isEmpty()) {
			if (responseFromIntegrationBroker.has("NS1:GenerateOTP_Response")
					&& responseFromIntegrationBroker.get("NS1:GenerateOTP_Response") instanceof JSONObject) {
				JSONObject response = responseFromIntegrationBroker.getJSONObject("NS1:GenerateOTP_Response");
				if (response.has("status") && Utils.getIntegerValue(response.get("status")) == 0) {
					logger.debug("Success Response for FEC SMS");
					String successDate = Utils.getCurrentDateTime().toString();
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_OTP_GENERATION_SUCCESS_STATUS_KEY,
							"true");
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_OTP_GENERATION_LAST_SUCCESS_DATE_KEY,
							successDate);
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_SMS_STATUS_KEY, "SENT");
				} else {
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_SMS_STATUS_KEY,
							"Error generating OTP");
				}
			} else {
				throw new Exception("No Response Key In Response for FEC SMS");
			}
		} else {
			throw new Exception(responseFromIntegrationBroker.toString());
		}
		return processVariablesFromSMSIntegration;
	}
}
