package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.kuliza.lending.common.utils.Constants.DeviceType;

public class UserMPINInputForm {

	@NotNull(message = "MPIN cannot be null")
	@Size(min = 4, max = 6)
	@Pattern(regexp = "[0-9]+")
	String mpin;
	
	@NotNull(message = "Device Id cannot be null")
	@Size(min = 1)
	String deviceId;
	
	@NotNull(message = "Device type cannot be null")
	DeviceType deviceType;
	
//	@NotNull(message = "Device Info cannot be null")
	String deviceInfo;
	String appsFlyerData;
	
	
	public UserMPINInputForm() {
		super();
	}

	public UserMPINInputForm(String mpin, String deviceId, DeviceType deviceType, String deviceInfo,
			String appsFlyerData) {
		super();
		this.mpin = mpin;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.deviceInfo = deviceInfo;
		this.appsFlyerData = appsFlyerData;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getAppsFlyerData() {
		return appsFlyerData;
	}

	public void setAppsFlyerData(String appsFlyerData) {
		this.appsFlyerData = appsFlyerData;
	}

	@Override
	public String toString() {
		return "UserMPINInputForm [mpin=" + mpin + ", deviceId=" + deviceId + ", deviceType=" + deviceType
				+ ", deviceInfo=" + deviceInfo + ", appsFlyerData=" + appsFlyerData + "]";
	}
}
