package com.kuliza.lending.wf_implementation.controllers;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.base.DefaultOTPService;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.UserMPINInputForm;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.services.FECreditOTPService;
import com.kuliza.lending.wf_implementation.services.UserValidationService;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@RestController
@RequestMapping(WfImplConstants.VALIDATION_API_ENDPOINT)
public class UserValidationController {

	@Autowired
	private UserValidationService userValidationService;

	@Autowired
	private DefaultOTPService defaultOTPService;
	
	@Autowired
	private FECreditOTPService fecOtpService;

	private static final Logger logger = LoggerFactory.getLogger(UserValidationController.class);

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.GENERATE_PASSWORD_API_ENDPOINT)
	public ResponseEntity<Object> generateOtpController(
			@RequestBody @Valid UserPasswordGenerationForm userForm, HttpServletRequest request) {
		try {
			// Note: In case, implementation wants to have their own implementation, then write
			// implementations in UserValidationService, and use its reference variable here
			return CommonHelperFunctions.buildResponseEntity(defaultOTPService.generateUserPassword(request, userForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.VALIDATE_USER_API_ENDPOINT)
	public ResponseEntity<Object> validateOtpController(
			@RequestBody @Valid UserValidationinputForm userValidationForm, HttpServletRequest request) {
		try {
			// Note: In case, implemetation wants to have their own implementation, then write
			// implementations in UserValidationService, and use its reference variable here
			return CommonHelperFunctions.buildResponseEntity(defaultOTPService.validateAndRegisterUser(request, userValidationForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.VALIDATE_OTP)
	public ResponseEntity<Object> validateOtp(
			@RequestBody @Valid UserValidationinputForm userValidationForm, HttpServletRequest request) {
		try {
			// Note: In case, implemetation wants to have their own implementation, then write
			// implementations in UserValidationService, and use its reference variable here
			return CommonHelperFunctions.buildResponseEntity(defaultOTPService.validateOtp(request, userValidationForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.SET_MPIN_API_ENDPOINT)
	public ResponseEntity<Object> setMpin(Principal principal,
			@RequestBody @Valid UserMPINInputForm mpinForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(userValidationService.setMpin(principal.getName(), mpinForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.VALIDATE_MPIN_API_ENDPOINT)
	public ResponseEntity<Object> validateMpin(Principal principal,
			@RequestBody @Valid UserMPINInputForm mpinForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(userValidationService.validateMpin(principal.getName(), mpinForm));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

}
