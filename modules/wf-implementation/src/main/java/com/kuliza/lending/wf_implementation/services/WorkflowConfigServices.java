package com.kuliza.lending.wf_implementation.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WorkflowConfigDao;
import com.kuliza.lending.wf_implementation.models.WorkFlowConfig;
import com.kuliza.lending.wf_implementation.pojo.AppUpdateInfo;
import com.kuliza.lending.wf_implementation.pojo.ConfigInput;
import com.kuliza.lending.wf_implementation.pojo.ConfigInputList;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class WorkflowConfigServices {

	@Autowired
	private WorkflowConfigDao wfConfigDao;

	public ApiResponse updateConfig(ConfigInputList configInputList) {
		for (ConfigInput configInput : configInputList.getConfigInputList()) {
			WorkFlowConfig currentRecord = wfConfigDao.findByNameAndIsDeleted(configInput.getName(), false);
			if (currentRecord == null) {
				WorkFlowConfig losConfig = new WorkFlowConfig(configInput.getName(), configInput.getValue());
				wfConfigDao.save(losConfig);
			} else {
				currentRecord.setName(configInput.getName());
				currentRecord.setValue(configInput.getValue());
				wfConfigDao.save(currentRecord);
			}
		}
		return new ApiResponse(Constants.SUCCESS_CODE, Constants.SUCCESS_MESSAGE, "");
	}

	public ApiResponse getConfig(String name) {
		WorkFlowConfig currentRecord = wfConfigDao.findByNameAndIsDeleted(name, false);
		if (currentRecord == null) {
			return new ApiResponse(Constants.FAILURE_CODE, Constants.FAILURE_MESSAGE, Constants.DATA_NOT_FOUND);
		} else {
			return new ApiResponse(Constants.SUCCESS_CODE, Constants.SUCCESS_MESSAGE, currentRecord);
		}
	}

	public ApiResponse getUpdateInfo(AppUpdateInfo body) {
		ApiResponse response;
		try {
			Map<String, Object> data = new HashMap<>();
			String leastSupportedVersion = "";
			String currentVersion = body.getVersion();
			String latestVersion = "";
			String iosLeastVersionKey = WfImplConstants.FE_CREDIT_IOS_LEAST_SUPPORTED_VERSION_KEY;
			String iosLatestVersionKey = WfImplConstants.FE_CREDIT_IOS_LATEST_VERSION_KEY;
			String androidLeastVersionKey = WfImplConstants.FE_CREDIT_ANDROID_LEAST_SUPPORTED_VERSION_KEY;
			String androidLatestVersionKey = WfImplConstants.FE_CREDIT_ANDROID_LATEST_VERSION_KEY;

			if (body.getPlatform().toLowerCase().equals(Constants.IOS_KEY)) {
				leastSupportedVersion = Utils
						.getStringValue(wfConfigDao.findByNameAndIsDeleted(iosLeastVersionKey, false).getValue());
				latestVersion = Utils
						.getStringValue(wfConfigDao.findByNameAndIsDeleted(iosLatestVersionKey, false).getValue());
			} else {
				leastSupportedVersion = Utils
						.getStringValue(wfConfigDao.findByNameAndIsDeleted(androidLeastVersionKey, false).getValue());
				latestVersion = Utils
						.getStringValue(wfConfigDao.findByNameAndIsDeleted(androidLatestVersionKey, false).getValue());
			}
			data.put(Constants.LATEST_VERSION_MAP_KEY, latestVersion);
			if (versionCompare(currentVersion, leastSupportedVersion)) {
				data.put(Constants.FORCE_UPDATE_MAP_KEY, false);
			} else {
				data.put(Constants.FORCE_UPDATE_MAP_KEY, true);
			}
			if (versionCompare(currentVersion, latestVersion)) {
				data.put(Constants.UPDATE_AVAILABLE_MAP_KEY, false);
			} else {
				data.put(Constants.UPDATE_AVAILABLE_MAP_KEY, true);
			}
			response = new ApiResponse(Constants.SUCCESS_CODE, Constants.SUCCESS_MESSAGE, data);
		} catch (Exception e) {
			response = new ApiResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.SOMETHING_WRONG_MESSAGE,
					e.getMessage());
		}
		return response;
	}

	boolean versionCompare(String v1, String v2) {
		int vnum1 = 0, vnum2 = 0;
		for (int i = 0, j = 0; (i < v1.length() || j < v2.length());) {
			while (i < v1.length() && v1.charAt(i) != '.') {
				vnum1 = vnum1 * 10 + (Utils.getIntegerValue(v1.charAt(i)) - 0);
				i++;
			}
			while (j < v2.length() && v2.charAt(j) != '.') {
				vnum2 = vnum2 * 10 + (Utils.getIntegerValue(v2.charAt(j)) - 0);
				j++;
			}
			if (vnum1 > vnum2) {
				return true;
			}
			if (vnum2 > vnum1) {
				return false;
			}
			vnum1 = vnum2 = 0;
			i++;
			j++;
		}
		return true;
	}
}
