package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.pojo.UpdateNidData;
import com.kuliza.lending.wf_implementation.services.UpdateNidDataService;

@RestController
@RequestMapping("/update-nid")
public class UpdateNidDataController {

	@Autowired
	private UpdateNidDataService updateNidDataService;

	@RequestMapping(method = RequestMethod.POST, value = "/updateNidData")
	public ApiResponse updateNidDetails(Principal principal, @RequestBody UpdateNidData updateNidData) {
		return updateNidDataService.updateNidData(principal.getName(), updateNidData);
	}
}
