package com.kuliza.lending.wf_implementation.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.MobileNumberUpdateHistoryDao;
import com.kuliza.lending.wf_implementation.dao.TopicMessagesDao;
import com.kuliza.lending.wf_implementation.dao.UserNotificationDao;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WfUserDeviceRegDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowApplicationDocsDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.models.MobileNumberUpdateHistory;
import com.kuliza.lending.wf_implementation.models.TopicMessages;
import com.kuliza.lending.wf_implementation.models.UserNotificationModel;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WfUserDeviceRegister;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplicationDocs;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.notification.SNSMessagePublish;
import com.kuliza.lending.wf_implementation.pojo.DeviceInfoInpuForm;
import com.kuliza.lending.wf_implementation.pojo.PolicyDetails;
import com.kuliza.lending.wf_implementation.pojo.ProductRequest;
import com.kuliza.lending.wf_implementation.pojo.UpdateDmsRequest;
import com.kuliza.lending.wf_implementation.pojo.UpdateNotification;
import com.kuliza.lending.wf_implementation.pojo.ViewPdfForm;
import com.kuliza.lending.wf_implementation.pojo.WFUserRequest;
import com.kuliza.lending.wf_implementation.pojo.WorkFlowUserSms;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class WorkFlowUserService {

	@Autowired
	private SNSMessagePublish snsMessagePublish;
	
	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private WfUserDeviceRegDao wfUserDeviceRegDao;

	@Autowired
	private WorkFlowAddressDao workflowAdressDao;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private MasterApiIntegration masterApiIntegration;
	@Autowired
	private FECDMSService fecdmsService;
	
	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;
	
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	
	@Autowired
	private WorkFlowApplicationDocsDao workflowApplicationDocsDao;
	
	@Autowired
	private HistoryService historyService;

	@Autowired
	private UserNotificationDao userNotificationDao;
	
	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	private TopicMessagesDao topicMessagesDao; 

	@Autowired
	private UserTopicsDao userTopicsDao;
	
	@Autowired
	private MobileNumberUpdateHistoryDao mobileNumberUpdateHistoryDao;
	
	@Autowired
	private FECreditBancaNotifyUserServices notifyUserServices;
	
	private static final Logger logger = LoggerFactory
			.getLogger(WorkFlowUserService.class);

	public ApiResponse getUserNotifications(String userName) {
		logger.info("-->Entering WorkFlowUserService.getUserNotifications()");
		WorkFlowUser workFlowUser = workFlowUserDao
				.findByIdmUserNameAndIsDeleted(userName, false);
		if(workFlowUser!=null){
			try{
				List<UserNotificationModel> userNotificationList = userNotificationDao.findByUserNameAndIsDeleted(userName, false);
				List<Map<String, String>> userNotifications = new ArrayList<Map<String,String>>();
	
				List<UserTopics> usertopics = userTopicsDao.findByUserNameAndIsDeleted(workFlowUser.getIdmUserName(), false);
	
				if (workFlowUser.isPushNotification()) {
					for(UserNotificationModel userNotification: userNotificationList){
						Map<String, String> userNotificationMap = new HashMap<String,String>();
						userNotificationMap.put("NotificationMessage",userNotification.getNotificationMessage());
						userNotificationMap.put("NotificationDetailMessage",userNotification.getNotificationDetailedMessage());
						userNotificationMap.put("CreationTime", userNotification.getCreated().toString());
						userNotifications.add(userNotificationMap);
					}
				}
				
				if (workFlowUser.isPromotionPushNotification()){
					for(UserTopics usertopic: usertopics){
		                logger.info("<==primotional notification user name==>" + usertopic.getUserName());  
		                logger.info("<==primotional notification user created==>" + usertopic.getCreated());  

						DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");  
		                String strDate = dateFormat.format(usertopic.getCreated());  
		                	
		                logger.info("<==primotional notification start date: ==>" + strDate);  
		                logger.info("<==primotional notification start date: ==>" + usertopic.getCreated().toString());					  
						
		                List<TopicMessages> topicMsgs = topicMessagesDao.findMessagesForTopic(usertopic.getTopic(), usertopic.getCreated().toString(), false);
		                for(TopicMessages topicMsg: topicMsgs){
		                	
							Map<String, String> userNotificationMap = new HashMap<String,String>();
							userNotificationMap.put("NotificationMessage",topicMsg.getMessage());
							userNotificationMap.put("NotificationDetailMessage",topicMsg.getMessage());
							userNotificationMap.put("CreationTime", topicMsg.getCreated().toString());
							userNotifications.add(userNotificationMap);
						}
					}
				}
		
			
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						userNotifications);
			}catch(Exception ex){
                logger.info("<==exception occured while get notifications for user  ==>" + ex);  

				return new ApiResponse(HttpStatus.UNAUTHORIZED,
						Constants.USER_DOES_NOT_EXIST_MESSAGE);
			}
			
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED,
					Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}	

	public ApiResponse getNotificationThroughProcessInst(String process_id) {
		logger.info("-->Entering WorkFlowUserService.getNotificationThroughProcessInst()");
		
				WorkFlowUserApplication workFlowUser = workflowUserApplicationDao.findByProcInstIdAndIsDeleted(process_id, false);
				if(workFlowUser!=null){
					List<UserNotificationModel> userNotificationList = userNotificationDao.findByUserNameAndIsDeleted(workFlowUser.getUserIdentifier(), false);
					List<Map<String, String>> userNotifications = new ArrayList<Map<String,String>>();

					List<UserTopics> usertopics = userTopicsDao.findByUserNameAndIsDeleted(workFlowUser.getUserIdentifier(), false);

						for(UserNotificationModel userNotification: userNotificationList){
							Map<String, String> userNotificationMap = new HashMap<String,String>();
							userNotificationMap.put("NotificationMessage",userNotification.getNotificationMessage());
							userNotificationMap.put("NotificationDetailMessage",userNotification.getNotificationDetailedMessage());
							userNotificationMap.put("CreationTime", userNotification.getCreated().toString());
							userNotifications.add(userNotificationMap);
						}
					
					
						for(UserTopics usertopic: usertopics){
							DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");  
			                String strDate = dateFormat.format(usertopic.getCreated());  
			                logger.info("<==primotional notification start date: ==>" + strDate);  
							
			                List<TopicMessages> topicMsgs = topicMessagesDao.findMessagesForTopic(usertopic.getTopic(), strDate, false);
							for(TopicMessages topicMsg: topicMsgs){
								Map<String, String> userNotificationMap = new HashMap<String,String>();
								userNotificationMap.put("NotificationMessage",topicMsg.getMessage());
								userNotificationMap.put("NotificationDetailMessage",topicMsg.getMessage());
								userNotificationMap.put("CreationTime", topicMsg.getCreated().toString());
								userNotifications.add(userNotificationMap);
							}
						}
					
				
					
					return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
								userNotifications);
					
				} else {
					return new ApiResponse(HttpStatus.UNAUTHORIZED,
							Constants.USER_DOES_NOT_EXIST_MESSAGE);
				}		
			
	}	

	public ApiResponse getUserInfoDetails(String userName) {
		logger.info("-->Entering WorkFlowUserService.getUserInfoDetails()");
		WorkFlowUser workFlowUser = workFlowUserDao
				.findByIdmUserNameAndIsDeleted(userName, false);
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		if (workFlowUser != null) {
			workFlowUserMap.put(Constants.MOBILE_NUMBER_KEY,
					Utils.getStringValue(workFlowUser.getMobileNumber()));
			workFlowUserMap.put(Constants.USER_NAME_KEY,
					Utils.getStringValue(workFlowUser.getUsername()));
			workFlowUserMap.put(Constants.NOTIFICATION_PREFERENCE, Utils
					.getStringValue(workFlowUser.isNotificationPreference()));
			workFlowUserMap.put(Constants.TOUCH_ID_KEY,
					Utils.getStringValue(workFlowUser.isTouchIdEnabled()));
			workFlowUserMap.put(Constants.PUSH_NOTIFICATION_KEY,
					Utils.getStringValue(workFlowUser.isPushNotification()));
			workFlowUserMap.put(Constants.EMAIL_NOTIFICATION,
					Utils.getStringValue(workFlowUser.isEmailNotification()));
			workFlowUserMap.put(Constants.SMS_NOTIFICATION,
					Utils.getStringValue(workFlowUser.isSmsNotification()));
			workFlowUserMap
					.put(Constants.PROMOTION_PUSH_NOTIFICATION, Utils
							.getStringValue(workFlowUser
									.isPromotionPushNotification()));
			workFlowUserMap.put(Constants.PROMOTION_SMS_NOTIFICATION, Utils
					.getStringValue(workFlowUser.isPromotionSmsNotification()));
			workFlowUserMap.put(Constants.PROMOTION_EMAIL_NOTIFICATION,
					Utils.getStringValue(workFlowUser
							.isPromotionEmailNotification()));
			workFlowUserMap.put(Constants.BANCA_NATIONAL_ID_KEY,
					Utils.getStringValue(workFlowUser.getNationalId()));
			logger.info("<-- Exiting WorkFlowUserService.getUserInfoDetails()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					workFlowUserMap);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED,
					Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse updateUserInfo(String userName, WFUserRequest request) {
		WorkFlowUser workFlowUser = workFlowUserDao
				.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser != null) {
			String date = null;
			try {
				date = Utils
						.formatDate(request.getDob(),
								Constants.DATE_FORMAT_SLASH,
								Constants.DATE_FORMAT_DASH);
			} catch (Exception e) {
				return new ApiResponse(HttpStatus.BAD_REQUEST,
						"Data Format is Wrong");
			}
			workFlowUser.setDob(date);
			workFlowUser.setGender(Utils.convertVietnamGenderToEnglish(request
					.getGender()));
			workFlowUser.setUsername(request.getUserName());
			workFlowUserDao.save(workFlowUser);
			return new ApiResponse(HttpStatus.OK,
					Constants.USER_UPDATED_SUCCESS_MESSAGE);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED,
					Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse addDeviceInfo(DeviceInfoInpuForm deviceInfoInputForm) {
		logger.info("-->Entering WorkFlowUserService.addDeviceInfo()");
		WorkFlowUser workFlowUser = workFlowUserDao
				.findByMobileNumberAndIsDeleted(
						deviceInfoInputForm.getPhoneNumber(), false);
		if (workFlowUser != null) {
			List<WfUserDeviceRegister> wfDeviceList = wfUserDeviceRegDao
					.findByWfUserAndIsDeleted(workFlowUser, false);
			WfUserDeviceRegister wfDeviceRegister = null;
			if (wfDeviceList != null && !wfDeviceList.isEmpty())
				wfDeviceRegister = wfDeviceList.get(0);
			else
				wfDeviceRegister = new WfUserDeviceRegister();
			wfDeviceRegister.setWfUser(workFlowUser);
			wfDeviceRegister.setDeviceId(deviceInfoInputForm.getDeviceId());
			wfDeviceRegister.setDeviceType(deviceInfoInputForm.getDeviceType());
			wfDeviceRegister.setAppbuildNumber(deviceInfoInputForm
					.getAppbuildNumber());
			wfDeviceRegister.setAppbundleId(deviceInfoInputForm
					.getAppbundleId());
			wfDeviceRegister.setBrandName(deviceInfoInputForm.getBrandName());
			wfDeviceRegister.setCarrier(deviceInfoInputForm.getCarrier());
			wfDeviceRegister.setDeviceLocale(deviceInfoInputForm
					.getDeviceLocale());
			wfDeviceRegister.setDeivceName(deviceInfoInputForm.getDeviceName());
			wfDeviceRegister.setDeviceNumberId(deviceInfoInputForm
					.getDeviceNumberId());
			wfDeviceRegister.setImei(deviceInfoInputForm.getImei());
			wfDeviceRegister.setIp(deviceInfoInputForm.getIp());
			wfDeviceRegister.setMacAddress(deviceInfoInputForm.getMacAddress());
			wfDeviceRegister.setModel(deviceInfoInputForm.getModel());
			wfDeviceRegister.setPhoneNumber(deviceInfoInputForm
					.getPhoneNumber());
			wfDeviceRegister.setVersion(deviceInfoInputForm.getVersion());
			wfUserDeviceRegDao.save(wfDeviceRegister);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					Constants.DEVICE_INFO_ADDED_MESSAGE);
		} else {
			logger.info("<-- Exiting WorkFlowUserService.addDeviceInfo()");
			return new ApiResponse(HttpStatus.UNAUTHORIZED,
					Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse getDataFromMasters(ProductRequest productRequest,
			String slug) throws Exception {
		logger.info("-->Entering WorkFlowUserService.getListOfProducts()");
		List<HashMap<String, Object>> filterList = productRequest.getKeyList();
		JSONObject getRequestPayloadForMaster = null;
		JSONObject getDataFromMasterApi = null;
		
		if (slug != null && !slug.isEmpty()) {
			JSONArray masterRequestList = getMasterRequest(filterList);
			getRequestPayloadForMaster = masterApiIntegration
					.buildRequestPayload(masterRequestList);
			getDataFromMasterApi = masterApiIntegration.getDataFromService(
					getRequestPayloadForMaster, slug);
			if (getDataFromMasterApi != null
					&& getDataFromMasterApi.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				JSONArray DataArray = masterApiIntegration
						.parseResponseFromMaster(getDataFromMasterApi);
				return new ApiResponse(HttpStatus.OK,
						Constants.SUCCESS_MESSAGE,
						new ObjectMapper().readValue(DataArray.toString(),
								new TypeReference<List<Map<String, Object>>>() {
								}));
			} else {
				return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						Constants.FAILURE_MESSAGE,
						getDataFromMasterApi
								.getString(Constants.MASTER_ERROR_MESSAGE));
			}
		} else {
			return new ApiResponse(HttpStatus.BAD_REQUEST,
					Constants.FAILURE_MESSAGE,
					Constants.SLUG_KEY_MASTER_MISSING);
		}
	}

	public ApiResponse getDataFromMasters(String slug) throws Exception {
		logger.info("-->Entering WorkFlowUserService.getListOfProducts()");
		JSONObject getDataFromMasterApi = null;
		if (slug != null && !slug.isEmpty()) {
			getDataFromMasterApi = masterApiIntegration
					.getDataFromService(slug);
			if (getDataFromMasterApi != null
					&& getDataFromMasterApi.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				JSONArray dataArray = masterApiIntegration
						.parseResponseFromMaster(getDataFromMasterApi);
				return new ApiResponse(HttpStatus.OK,
						Constants.SUCCESS_MESSAGE,
						new ObjectMapper().readValue(dataArray.toString(),
								new TypeReference<List<Map<String, Object>>>() {
								}));
			} else {
				return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						Constants.FAILURE_MESSAGE,
						getDataFromMasterApi
								.getString(Constants.MASTER_ERROR_MESSAGE));
			}
		} else {
			return new ApiResponse(HttpStatus.BAD_REQUEST,
					Constants.FAILURE_MESSAGE,
					Constants.SLUG_KEY_MASTER_MISSING);
		}
	}

	public JSONArray getMasterRequest(List<HashMap<String, Object>> filterMapList)
			throws JSONException {
		JSONArray masterRequestList = new JSONArray();
		if (filterMapList != null) {
			for (HashMap<String, Object> filterMap : filterMapList) {
				JSONObject masterRequest = new JSONObject();
				// adding all key, value to master request
				for (Entry<String, Object> filter : filterMap.entrySet()) {
					masterRequest.put(filter.getKey(),filter.getValue());
				}
				masterRequestList.put(masterRequest);
				
//				if (filterMap.containsKey(Constants.KEY)
//						&& filterMap.containsKey(Constants.VALUE)) {
//					
//					keyListJsonObject.put(Constants.KEY,
//							filterMap.get(Constants.KEY));
//					keyListJsonObject.put(Constants.VALUE,
//							filterMap.get(Constants.VALUE));
//					finalJsonArrayRequest.put(keyListJsonObject);
//				}
			}
		}
		logger.info("masterRequestList -------->"+ masterRequestList);
		return masterRequestList;

	}

	public ApiResponse getPolicyDetails(Principal principal,
			PolicyDetails policyDetails) throws Exception {
		logger.info("-->Entering WorkFlowUserService.getPolicyDetails()");
		JSONObject masterFinalJsonResponse = null;
		JSONObject getRequestPayloadForMaster = null;
		JSONObject masterFinalJsonResponseAsJson = null;
		logger.info("<==========> principal.getName() :: "+principal.getName());	
		/*WorkFlowUserApplication workflowUserApplication = workflowUserApplicationDao
				.findByUserIdentifierAndProcInstIdAndIsDeleted(
						principal.getName(),
						policyDetails.getProcessInstanceId(), false);
						*/
		WorkFlowUserApplication workflowUserApplication = workflowUserApplicationDao
				.findByUserIdentifierAndProcInstId(
						principal.getName(),
						policyDetails.getProcessInstanceId());
		if (workflowUserApplication != null) {
			WorkFlowApplication workFlowApplication = workflowUserApplication
					.getWfApplication();
			if (workFlowApplication != null) {
				String productId = Utils.getStringValue(workFlowApplication
						.getProductId());
				logger.info("<==============> productId :: "+productId);
				String planId = Utils.getStringValue(workFlowApplication
						.getPlanId());
				String categoryId = Utils.getStringValue(workFlowApplication
						.getCategoryId());
				logger.info("<==============> categoryId :: "+categoryId);
				String productName = Utils.getStringValue(workFlowApplication
						.getProductName());
				String policyNumber = Utils.getStringValue(workFlowApplication
						.getPolicyNumber());
				String nextPaymentDue = Utils
						.getStringValue(workFlowApplication.getNextPaymentDue());
				String planExpiringDate = Utils
						.getStringValue(workFlowApplication
								.getPlanExpiringDate());
				String insurer = Utils.getStringValue(workFlowApplication
						.getInsurer());
				String montlyPayment = Utils.getStringValue(workFlowApplication
						.getMontlyPayment());
				String protectionAmount = Utils
						.getStringValue(workFlowApplication
								.getProtectionAmount());
				String paymentDate = Utils.getStringValue(workFlowApplication
						.getApplicationDate());
				String insurerDocId = Utils.getStringValue(workFlowApplication.getDmsDocId());
				String fhMemberDocId = Utils.getStringValue(workFlowApplication.getDocIdForExcel());
				String language = Utils.getStringValue(policyDetails
						.getLanguage());
				if (language.isEmpty())
					language = Constants.LANGUAGE_VALUE;
				logger.info("<======>productId:" + productId + " planId :"
						+ planId + " categoryId :" + categoryId
						+ " productName :" + productName
						+ " protectionAmount :" + protectionAmount
						+ " paymentDate :" + paymentDate + " language: "
						+ language);
				logger.info("<=======>planId is :" + planId + "<==========>");
				if (planId.contains(",")) {
					
					logger.info("Plan Id with comma present ---->"+planId);
					
					List<String> planIdList = Arrays.asList(planId
							.split("\\s*,\\s*"));
					if (planIdList.contains(WfImplConstants.TW_MOTOR_LOSS_PLAN_ID)) {
						planId = WfImplConstants.TW_MOTOR_LOSS_PLAN_ID;
					}else {
						planId = Utils.getMainPlanId(planId);
					}
				}
				logger.info("<=======>planId is :" + planId + "<==========>");
				JSONArray data = constructFilterJsonArray(productId, planId,
						categoryId, language);
				getRequestPayloadForMaster = masterApiIntegration
						.buildRequestPayload(data);
				masterFinalJsonResponse = masterApiIntegration
						.getDataFromService(getRequestPayloadForMaster,
								getMasterDataFromProductId(productId));
				masterFinalJsonResponseAsJson = new JSONObject();
				
				if (masterFinalJsonResponse != null
						&& masterFinalJsonResponse
								.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
					
					JSONArray masterFinalResponseAsJsonArray = masterApiIntegration
							.parseResponseFromMaster(masterFinalJsonResponse);
					
					if (masterFinalResponseAsJsonArray.length() != 0) {
						masterFinalJsonResponseAsJson = (JSONObject) masterFinalResponseAsJsonArray
								.get(0);
					}else {
						logger.error("Getting Empty data from master");
						throw new FECException(ErrorCodes.EMPTY_MASTER_DATA);
					}
				}else {
					logger.error("Invalid status from master");
					throw new FECException(ErrorCodes.INVALID_MASTER_STATUS);
				
				}
				
				masterFinalJsonResponseAsJson.put(Constants.PRODUCT_NAME,
						productName);
				masterFinalJsonResponseAsJson.put(Constants.POLICY_NUMBER,
						policyNumber);
				masterFinalJsonResponseAsJson.put(Constants.NEXT_PAYMENT_DUE,
						nextPaymentDue);
				masterFinalJsonResponseAsJson.put(Constants.PLAN_EXPIRING_DATE,
						planExpiringDate);
				masterFinalJsonResponseAsJson.put(Constants.INSURER, insurer);
				masterFinalJsonResponseAsJson.put(Constants.MONTHLY_PAYMENT,
						montlyPayment);
				masterFinalJsonResponseAsJson.put(Constants.PROTECTION_AMOUNT,
						protectionAmount);
				masterFinalJsonResponseAsJson.put(Constants.PURCHASE_DATE,
						paymentDate);
				masterFinalJsonResponseAsJson.put("policyDetails",
						getPolicyInfo(workFlowApplication));
				masterFinalJsonResponseAsJson.put(WfImplConstants.INSURER_PDF_DOC_ID, insurerDocId);
				masterFinalJsonResponseAsJson.put(WfImplConstants.FH_MEMBER_DOC_ID, fhMemberDocId);
				
				
//				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
//						.processInstanceId(policyDetails.getProcessInstanceId()).includeProcessVariables().singleResult();
//				
//				Map<String, Object> variables = processInstance.getProcessVariables();
				
				// e-sign docs ids added
				List<String> docList = getFptDocList(workFlowApplication);
				
				return new ApiResponse(HttpStatus.OK,
						Constants.SUCCESS_MESSAGE, constructResponseMap(
								masterFinalJsonResponseAsJson, productName,
								policyNumber, nextPaymentDue, planExpiringDate,
								insurer, montlyPayment, protectionAmount,docList));
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST,
						Constants.DATA_NOT_FOUND);
			}
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED,
					Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}

	}
	
	private List<String> getFptDocList(WorkFlowApplication workFlowApplication) {

		List<String> docList = new ArrayList<String>();

		List<WorkFlowApplicationDocs> workFlowApplicationDocsList = workflowApplicationDocsDao
				.findListByWorkFlowApplicationAndDocType(workFlowApplication,
						Constants.DocType.ESIGN_FPT.getDocTypeId());

		if (workFlowApplicationDocsList == null || workFlowApplicationDocsList.isEmpty()) {
			return docList;
		}

		for (WorkFlowApplicationDocs workFlowApplicationDocs : workFlowApplicationDocsList) {
			docList.add(workFlowApplicationDocs.getDocId());
		}

		return docList;
	}
	
	private List<Map<String, String>> getPolicyInfo(
			WorkFlowApplication wfApplication) {
		List<Map<String, String>> policyList=new ArrayList<Map<String,String>>();
		List<WorkFlowPolicy> workflowPolicyList = workFlowPolicyDAO.findByWorkFlowApplicationAndIsDeleted(wfApplication, false);
		for(WorkFlowPolicy workFlowPolicy:workflowPolicyList){
			String planId=workFlowPolicy.getPlanId();
			if(planId.equals(WfImplConstants.TW_PERSONAL_ACCIDENT_PLAN_ID)){
				continue;
			}
			Map<String, String> policiesMap = new HashMap<String, String>();
			policiesMap.put(WfImplConstants.INSURER_PDF_DOC_ID, workFlowPolicy.getDmsDocId());
			policiesMap.put(WfImplConstants.POLICY_NUMBER, workFlowPolicy.getPolicyNumber());
			policiesMap.put(WfImplConstants.PLAN_ID, workFlowPolicy.getPlanId());
			policiesMap.put(WfImplConstants.PLAN_NAME, workFlowPolicy.getPlanName());
			policiesMap.put(WfImplConstants.PLAN_NAME_VI, Constants.PLAN_NAME_VI);
			policiesMap.put(WfImplConstants.MONTHLY_PAYMENT, workFlowPolicy.getMontlyPayment());
			policiesMap.put(WfImplConstants.PROTECTION_AMOUNT, workFlowPolicy.getProtectionAmount());
			policyList.add(policiesMap);
		}
		
		return policyList;
	}

	public JSONArray constructFilterJsonArray(String productId, String planId,
			String categoryId, String languageValue) throws Exception {
		JSONArray constructJsonArray = new JSONArray();
		constructJsonArray.put(getFilterJsonObject(Constants.PRODUCT_ID_KEY,
				productId));
		constructJsonArray.put(getFilterJsonObject(Constants.PLAN_ID_KEY,
				planId));
		constructJsonArray.put(getFilterJsonObject(Constants.CATEGORY_ID_KEY,
				categoryId));
		constructJsonArray.put(getFilterJsonObject(Constants.LANGUAGE_KEY,
				languageValue));
		return constructJsonArray;
	}

	public JSONObject getFilterJsonObject(String key, String value)
			throws JSONException {
		JSONObject filterJson = new JSONObject();
		filterJson.put(Constants.KEY, key);
		filterJson.put(Constants.VALUE, value);
		return filterJson;
	}

	public Map<String, Object> constructResponseMap(JSONObject jsonObj,
			String productName, String policyNumber, String nextPaymentDue,
			String planExpiringDate, String insurer, String montlyPayment,
			String protectionAmount,List<String> docList) throws Exception {
		Map<String, Object> masterFinalJsonResponse = new HashMap<String, Object>();
		jsonObj.put(Constants.PRODUCT_NAME, productName);
		jsonObj.put(Constants.POLICY_NUMBER, policyNumber);
		jsonObj.put(Constants.NEXT_PAYMENT_DUE, nextPaymentDue);
		jsonObj.put(Constants.PLAN_EXPIRING_DATE, planExpiringDate);
		jsonObj.put(Constants.INSURER, insurer);
		jsonObj.put(Constants.MONTHLY_PAYMENT, montlyPayment);
		jsonObj.put(Constants.PROTECTION_AMOUNT, protectionAmount);
		
		jsonObj.put(WfImplConstants.E_SIGN_DOC_LIST,docList);
		
		masterFinalJsonResponse = new ObjectMapper().readValue(
				jsonObj.toString(), new TypeReference<Map<String, Object>>() {
				});
		return masterFinalJsonResponse;
	}

	public ApiResponse updateNotification(String userName,
			UpdateNotification updateNotification) {
		logger.info("-->Entering WorkFlowUserService.updateNotification()");
		WorkFlowUser workFlowUser = workFlowUserDao
				.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser != null) {
			if (updateNotification.getIsPushNotification() != null) {
				workFlowUser.setPushNotification(updateNotification
						.getIsPushNotification());
			}
			if (updateNotification.getIsEmailNotification() != null) {
				workFlowUser.setEmailNotification(updateNotification
						.getIsEmailNotification());
			}
			if (updateNotification.getIsSmsNotification() != null) {
				workFlowUser.setSmsNotification(updateNotification
						.getIsSmsNotification());
			}
			if (updateNotification.getTouchIdEnabled() != null) {
				workFlowUser.setTouchIdEnabled(updateNotification
						.getTouchIdEnabled());
			}
			if (updateNotification.getIsPromotionPushNotification() != null) {
				workFlowUser.setPromotionPushNotification(updateNotification
						.getIsPromotionPushNotification());
			}
			if (updateNotification.getIsPromotionSmsNotification() != null) {
				workFlowUser.setPromotionSmsNotification(updateNotification
						.getIsPromotionSmsNotification());
			}
			if (updateNotification.getIsPromotionEmailNotification() != null) {
				workFlowUser.setPromotionEmailNotification(updateNotification
						.getIsPromotionEmailNotification());
			}
			workFlowUserDao.save(workFlowUser);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					Constants.NOTIFICATION_UPDATED_SUCCESS_MESSAGE);
		} else {
			logger.info("<-- Exiting WorkFlowUserService.updateNotification()");
			return new ApiResponse(HttpStatus.UNAUTHORIZED,
					Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse pushNotification(String userName,  String notificationTitle,
			 String notificationMessage) {
				logger.info("-->Entering WorkFlowUserService.pushNotification()");
				WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
				if (workFlowUser != null) {
					UserNotificationModel userNotificationModel = new UserNotificationModel();
					userNotificationModel.setNotificationMessage(notificationMessage);
					userNotificationModel.setNotificationDetailedMessage(notificationMessage);
					userNotificationModel.setContactNumber(workFlowUser.getMobileNumber());
					userNotificationModel.setUserName(userName);
					userNotificationDao.save(userNotificationModel);
					if(workFlowUser.isPushNotification())
						  snsMessagePublish
							.publishMessageToEndpoint(notificationMessage,notificationTitle, workFlowUser.getArnEndPoint(),
									DeviceType.android);
					
					return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
							Constants.NOTIFICATION_UPDATED_SUCCESS_MESSAGE);
				} else {
					logger.info("<-- Exiting WorkFlowUserService.updateNotification()");
					return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
				}
	}
	
	public DelegateExecution updateWorkFlowUserTableData(
			DelegateExecution execution) throws Exception {
		logger.info("-->Entering WorkFlowUserService.updateWorkFlowUserTableData()");
		Map<String, Object> variables = execution.getVariables();

		String userFullName = variables.getOrDefault(
				WfImplConstants.USER_FULL_NAME, "").toString();
		String userGender = variables.getOrDefault(WfImplConstants.USER_GENDER,
				"").toString();
		userGender = Utils.convertVietnamGenderToEnglish(userGender);
		String userDateOfBirth = variables.getOrDefault(
				WfImplConstants.USER_DOB, "").toString();
		String fullNameHyperVerge = variables.getOrDefault(
				WfImplConstants.FULLNAME_HYPERVERGE, "").toString();
		String genderHyperVerge = variables.getOrDefault(
				WfImplConstants.GENDER_HYPERVERGE, "").toString();
		genderHyperVerge = Utils
				.convertVietnamGenderToEnglish(genderHyperVerge);
		String dateOfBirthHyperVerge = variables.getOrDefault(
				WfImplConstants.DOB_HYPERVERGE, "").toString();
		String nationalIdHyperVerge = variables.getOrDefault(
				WfImplConstants.NATIONAL_ID_HYPERVERGE, "").toString();
		String nationalIdFront = variables.getOrDefault(
				WfImplConstants.NID_FRONT, "").toString();
		String nationalIdBack = variables.getOrDefault(
				WfImplConstants.NID_BACK, "").toString();
		String hyperVergeFrontResponse = variables.getOrDefault(
				WfImplConstants.HYPERVERGE_FRONT_RESPONSE, "").toString();
		String hyperVergeBackResponse = variables.getOrDefault(
				WfImplConstants.HYPERVERGE_BACK_RESPONSE, "").toString();
		String cifResponse = variables.getOrDefault(
				WfImplConstants.CIF_RESPONSE, "").toString();
		String userName = variables
				.getOrDefault(
						com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY,
						"").toString();
		String userEmail = null;
		logger.info("applicantEmail_user : " + variables.getOrDefault(WfImplConstants.APPLICANT_EMAIL_USER, ""));
		if (variables.getOrDefault(WfImplConstants.APPLICANT_EMAIL_USER, "") != null) {
			userEmail = variables.getOrDefault(WfImplConstants.APPLICANT_EMAIL_USER, "").toString();
		}
		
		
		String natioanlity = variables.getOrDefault(
				WfImplConstants.NATIONALITY_HYPERVERGE, "").toString();
		natioanlity = Utils.convertVietnamNationalityToEnglish(natioanlity);
		logger.info("userName is : " + userName);
		logger.info("natioanlity :" + natioanlity);

		if (!userName.isEmpty()) {
			WorkFlowUser workFlowUser = workFlowUserDao
					.findByIdmUserNameAndIsDeleted(userName, false);
			if (workFlowUser != null) {
				workFlowUser.setUsername(userFullName);
				workFlowUser.setGender(userGender);
				workFlowUser.setDob(dateOfBirthHyperVerge);
				workFlowUser.setNid_user_name(fullNameHyperVerge);
				workFlowUser.setNid_gender(genderHyperVerge);
				workFlowUser.setNid_dob(dateOfBirthHyperVerge);
				workFlowUser.setNationalId(nationalIdHyperVerge);
				workFlowUser.setEmail(userEmail);
				if (!nationalIdFront.isEmpty())
					workFlowUser.setNationalIdFrontLink(nationalIdFront);
				if (!nationalIdBack.isEmpty())
					workFlowUser.setNationalIdBackLink(nationalIdBack);
				if (!hyperVergeFrontResponse.isEmpty())
					workFlowUser
							.setHyperVergeFrontResponse(hyperVergeFrontResponse);
				if (!hyperVergeBackResponse.isEmpty())
					workFlowUser
							.setHyperVergeBackResponse(hyperVergeBackResponse);
				workFlowUser.setNationality(natioanlity);
				workFlowUser.setCIFResponse(cifResponse);
				saveUserAddress(workFlowUser, variables);
				workFlowUserDao.save(workFlowUser);
				logger.info("user updated Successfully");
			} else {
				logger.info("user does not exist:" + userName);
			}
			logger.info("<-- Exiting WorkFlowUserService.updateWorkFlowUserTableData()");
		}
		return execution;
	}

	private void saveUserAddress(WorkFlowUser workFlowUser,
			Map<String, Object> variables) {
		logger.info("-->Entering WorkFlowUserService.saveUserAddress()");
		String proviance = variables.getOrDefault(
				WfImplConstants.PLACEOFORIGIN_HYPERVERGE, "").toString();
		String address = variables.getOrDefault(
				WfImplConstants.ADDRESS_HYPERVERGE, "").toString();
		String deliveryAddressLine = variables.getOrDefault(
				WfImplConstants.DELIVERY_ADDRESS_LINE, "").toString();
		String deliveryCity = variables.getOrDefault(
				WfImplConstants.DELIVERY_CITY, "").toString();
		String deliveryProviance = variables.getOrDefault(
				WfImplConstants.DELIVERY_PROVINCE, "").toString();
		logger.info("<=========>proviance :" + proviance + " address :"
				+ address + " deliveryAddressLine :" + deliveryAddressLine
				+ " deliveryCity :" + deliveryCity + " deliveryProviance :"
				+ deliveryProviance + "<============>");
		List<WorkFlowUserAddress> workFlowAddressList = workflowAdressDao
				.findByWfUser(workFlowUser);
		WorkFlowUserAddress workFlowUserNIDAddress = null;
		WorkFlowUserAddress workFlowUserDeliveryAddress = null;
		if (workFlowAddressList != null && !workFlowAddressList.isEmpty()) {
			for (WorkFlowUserAddress workFlowUserAddress : workFlowAddressList) {
				String addressType = workFlowUserAddress.getAddressType();
				if (addressType.equalsIgnoreCase(Constants.NID_ADDRESS_KEY)) {
					workFlowUserNIDAddress = workFlowUserAddress;
				} else if (addressType
						.equalsIgnoreCase(Constants.DELIVERY_ADDRESS_KEY)) {
					workFlowUserDeliveryAddress = workFlowUserAddress;

				}

			}
		}
		if (workFlowUserNIDAddress == null) {
			workFlowUserNIDAddress = new WorkFlowUserAddress();
			workFlowUserNIDAddress.setWfUser(workFlowUser);
			workFlowUserNIDAddress.setAddressType(Constants.NID_ADDRESS_KEY);
		}
		if (workFlowUserDeliveryAddress == null
				&& !deliveryAddressLine.isEmpty()) {
			workFlowUserDeliveryAddress = new WorkFlowUserAddress();
			workFlowUserDeliveryAddress.setWfUser(workFlowUser);
			workFlowUserDeliveryAddress
					.setAddressType(Constants.DELIVERY_ADDRESS_KEY);
		}

		workFlowUserNIDAddress.setAddressLine1(address);
		workFlowUserNIDAddress.setCityDistrict(proviance);
		workFlowUserNIDAddress.setProvince(proviance);
		workflowAdressDao.save(workFlowUserNIDAddress);
		logger.info("<===> NID Address is saved<=========>");
		if (workFlowUserDeliveryAddress != null
				&& !deliveryAddressLine.isEmpty()) {
			workFlowUserDeliveryAddress.setAddressLine1(deliveryAddressLine);
			workFlowUserDeliveryAddress.setCityDistrict(deliveryCity);
			workFlowUserDeliveryAddress.setProvince(deliveryProviance);
			workflowAdressDao.save(workFlowUserDeliveryAddress);
			logger.info("<===> Delivery Address is saved<=========>");
		}

	}

	public ApiResponse viewPdf(ViewPdfForm viewPdf) throws Exception {
		logger.info("<===> viewPdf =========>");
		try {
			String dmsDocId = viewPdf.getDmsDocId();
			logger.info("<=======>DocId is : " + dmsDocId);
			if (dmsDocId.isEmpty())
				throw new Exception("DMS DocId is Empty");
			File file = fecdmsService
					.downloadDocumentAsFile(dmsDocId, "", true);
			byte[] base64EncodedData = Base64.encodeBase64(
					loadFileAsBytesArray(file), true);
			String base64pdf = new String(base64EncodedData);
			base64pdf = base64pdf.replaceAll("\r\n", "");
			if (file.exists()) {
				file.delete();
			}
			logger.info("<========>View Pdf is SuccessFull<=============>");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					base64pdf);
		} catch (Exception e) {
			logger.error("<=====>ViewPDf failed :" + e.getMessage(), e);
			e.printStackTrace();
			throw e;
		}

	}
	
	private String getMasterDataFromProductId(String productId) {
		logger.info("<=========> productId :: " + productId);
		Map<String, String> mapOfProductId = Constants.productIdMap;
		String slugName = null;
		logger.info("<==========>mapOfProductId.get(productId) :: "+mapOfProductId);
		if(mapOfProductId.containsKey(productId)) {
			slugName = mapOfProductId.get(productId);
			logger.info("<========> slugName :: "+ slugName);
		}else {
			logger.error("<================> ProductId Not Found <================>");
		}
		return slugName;
	}

	public static byte[] loadFileAsBytesArray(File file) throws Exception {

		int length = (int) file.length();
		BufferedInputStream reader = new BufferedInputStream(
				new FileInputStream(file));
		byte[] bytes = new byte[length];
		reader.read(bytes, 0, length);
		reader.close();
		return bytes;

	}

	public ApiResponse updateDmsDocForAppIdAndPdfLink(UpdateDmsRequest updateDmsRequest) {

		ObjectMapper map = new ObjectMapper();
		try {
			logger.info("updateDmsDoc Request -->" + map.writeValueAsString(updateDmsRequest));
		} catch (Exception e) {
			logger.error("Error while print request", e);
		}

		ApiResponse validateApiResponse = validateUpdateDmsReq(updateDmsRequest);
		if (validateApiResponse != null) {
			return validateApiResponse;
		}

		Map<String, Object> dmsUpdateRes = new HashMap<String, Object>();
		try {
			WorkFlowApplication workFlowApp = workflowApplicationDao
					.findByProcessInstanceIdAndIsDeleted(updateDmsRequest.getProcessInstanceId(), false);
			if (workFlowApp == null)
				throw new Exception(
						"No Application is runnging for processInstanceId :" + updateDmsRequest.getProcessInstanceId());

			String docId = fecdmsService.uploadPDftoDMS(workFlowApp.getApplicationId(), updateDmsRequest.getPdfUrl());
			logger.info("docId :" + docId);
			if (docId == null) {
				return new ApiResponse(HttpStatus.BAD_REQUEST, "Invalid doc id");
			}

			workFlowApp.setDmsDocId(docId);
			workflowApplicationDao.save(workFlowApp);

			updateWFPolicy(workFlowApp, docId);

			dmsUpdateRes.put("docId", docId);
			dmsUpdateRes.put("applicationId", workFlowApp.getApplicationId());
		} catch (Exception e) {
			logger.error("Error while update updateDms", e);
			return new ApiResponse(HttpStatus.BAD_REQUEST, "Error while update update Dms Doc");
		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, dmsUpdateRes);
	}

	public void updateWFPolicy(WorkFlowApplication workFlowApp, String docId) {
		
		WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO.findByWorkFlowApplicationAndPlanIdAndIsDeleted(workFlowApp, workFlowApp.getPlanId(), false);
		
		if (workFlowPolicy != null) {
			workFlowPolicy.setDmsDocId(docId);
			workFlowPolicyDAO.save(workFlowPolicy);
		} else {
			logger.error("workFlowPolicy is NULL");

		}
	}

	public ApiResponse validateUpdateDmsReq(UpdateDmsRequest updateDmsRequest) {
		if (updateDmsRequest == null) {
			return new ApiResponse(HttpStatus.BAD_REQUEST, "Invalid Update Dms Request");
		}

		if (updateDmsRequest.getProcessInstanceId() == null || updateDmsRequest.getProcessInstanceId().isEmpty()) {
			return new ApiResponse(HttpStatus.BAD_REQUEST, "Invalid ProcessInstance Id");
		}

		if (updateDmsRequest.getPdfUrl() == null || updateDmsRequest.getPdfUrl().isEmpty()) {
			return new ApiResponse(HttpStatus.BAD_REQUEST, "Invalid pdf url");
		}

		return null;
	}

	public ApiResponse updateMobile(String userName, String newMobile) throws Exception {

		logger.info("Mobile number update called for User : " + userName + " new Mobile " + newMobile);
		if (newMobile == null || newMobile.isEmpty()) {
			throw new FECException(ErrorCodes.INVALID_MOBILE_NUMBER);
		}
		WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser == null) {
			throw new FECException(ErrorCodes.INVALID_USER);
		}
		List<WorkFlowUserApplication> workflowAppUserList = workflowUserApplicationDao
				.findByUserIdentifierAndIsDeleted(userName, false);

		if (workflowAppUserList != null) {
			updateMobileNoInWorkFlow(newMobile, workflowAppUserList);
		}
		MobileNumberUpdateHistory newMobileNumberUpdateHis = new MobileNumberUpdateHistory();
		newMobileNumberUpdateHis.setWorkFlowUser(workFlowUser);
		newMobileNumberUpdateHis.setNewNumber(newMobile);
		newMobileNumberUpdateHis.setOldNumber(workFlowUser.getMobileNumber());
		mobileNumberUpdateHistoryDao.save(newMobileNumberUpdateHis);
		
		sentNotification(workFlowUser.getMobileNumber(),newMobile);
		workFlowUser.setMobileNumber(newMobile);
		workFlowUserDao.save(workFlowUser);
		logger.info("mobile number updated successfully for " + userName);
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
	}

	public void updateMobileNoInWorkFlow(String newMobile, List<WorkFlowUserApplication> workflowAppUserList) {

		for (WorkFlowUserApplication workflowUserApp : workflowAppUserList) {
			WorkFlowApplication wfApplication = workflowUserApp.getWfApplication();

			if (wfApplication == null) {
				continue;
			}
			String stage = wfApplication.getCurrentJourneyStage();
			logger.info("update Mobile Number currentJourneyStage :" + stage + " procId : "
					+ wfApplication.getProcessInstanceId() + " " + newMobile);

			if (stage != null && stage.equalsIgnoreCase(WfImplConstants.APP_STATUS_COMPLETED)) {
				continue;
			}

			try {
				logger.info("started update mobile in workflow for :" + wfApplication.getProcessInstanceId());
				runtimeService.setVariable(wfApplication.getProcessInstanceId(),
						WfImplConstants.APPLICATION_MOBILE_NUMBER, "+84" + newMobile);
			} catch (Exception e) {
				logger.error("Error while update number", e);
				logger.error("Error while update number in workflow " + wfApplication.getProcessInstanceId()
						+ " stage : " + stage);
			}
		}
	}
	
	public void sentNotification( String contactNumber, String newMobile) throws Exception {
		
		WorkFlowUserSms workFlowUserSms = new WorkFlowUserSms();
		
		String message = Constants.MOBILE_CHANGE_SMS_CONTENT.replace(Constants.MOBILE_CHANGE_MOBILE_KEY, newMobile);
		
		workFlowUserSms.setMessage(message);
		workFlowUserSms.setMobileNumber(contactNumber);
		logger.info("sentNotification init for : "+contactNumber);
		logger.info("sentNotification message : "+message);
		notifyUserServices.sendSmsNotification(workFlowUserSms);
		logger.info("sentNotification completed for : "+contactNumber);
	}

}
