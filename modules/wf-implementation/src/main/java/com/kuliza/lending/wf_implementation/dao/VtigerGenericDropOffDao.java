package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.VtigerDropoffModel;
import com.kuliza.lending.wf_implementation.models.VtigerGenericDropOffModel;

@Repository
@Transactional
public interface VtigerGenericDropOffDao extends CrudRepository<VtigerGenericDropOffModel, Long> {

	public List<VtigerGenericDropOffModel> findByUserNameAndDropOffTypeAndIsDeleted(String userName, String dropOffType,
			boolean isDeleted);

	public List<VtigerGenericDropOffModel> findByStatusAndIsUpdatedInVtigerAndIsDeleted(String status,
			boolean isUpdatedInVtiger, boolean isDeleted);

	public List<VtigerGenericDropOffModel> findByUserNameAndIsUpdatedInVtigerAndDropOffTypeAndIsDeleted(String userName,
			boolean isUpdate, String dropOffType, boolean isDeleted);

	public VtigerGenericDropOffModel findByUserNameAndIsDeleted(String userName, boolean isDeleted);

}
