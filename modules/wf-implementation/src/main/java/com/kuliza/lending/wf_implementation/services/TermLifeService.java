package com.kuliza.lending.wf_implementation.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.models.PremiumCalculateRequest;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.pojo.ProductRequest;
import com.kuliza.lending.wf_implementation.pojo.TlPremiumInput;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("termLifeService")
public class TermLifeService {

	@Autowired
	private MasterApiIntegration masterApiIntegration;

	@Autowired
	private WorkflowUserDao workFlowUserDao;
	
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	
	@Autowired
	RuntimeService runtimeService;
	
	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	private static final Logger logger = LoggerFactory.getLogger(TermLifeService.class);

	public ApiResponse getDataFromQuickQuote(String userName, ProductRequest productRequest, String slug) {
		logger.info("-->Entering TermLifeService.getDobAndMasterData()");
		try {
			int age = getAgeFromDb(userName);
			logger.info(" <========= age =========> " + age);

			List<Map<String, Object>> data = buildQuickQuoteWithPremiumAmount(age, productRequest, slug);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
		} catch (Exception e) {
			e.printStackTrace();
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}
	}

	public List<Map<String, Object>> buildQuickQuoteWithPremiumAmount(int age, ProductRequest productRequest,
			String slug) {
		logger.info("-->Entering TermLifeService.buildQuickQuoteWithPremiumAmount()");
		JSONObject quickQuotemasterJsonData = null;
		JSONObject getRequestPayloadForMaster = null;
		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();
		try {
			List<HashMap<String, Object>> filterList = productRequest.getKeyList();
			JSONArray data = Utils.filterJsonArray(filterList);
			getRequestPayloadForMaster = masterApiIntegration.buildRequestPayload(data);
			quickQuotemasterJsonData = masterApiIntegration.getDataFromService(getRequestPayloadForMaster,
					Constants.MASTERS_BANCA_GET_TERM_LIFE_QUICK_QUOTE);
			JSONArray arrayOfPlans = quickQuotemasterJsonData.getJSONArray(Constants.JSON_ARRAY_DATA_KEY);
			if (arrayOfPlans != null) {
				for (int i = 0; i < arrayOfPlans.length(); i++) {
					JSONObject jsonObjectData = arrayOfPlans.getJSONObject(i);
					String plan_id = jsonObjectData.getString(Constants.PLAN_ID_KEY);
					PremiumCalculateRequest premiumCalculateRequest = new PremiumCalculateRequest();
					premiumCalculateRequest.setPlanId(plan_id);
					premiumCalculateRequest.setAge(age);
					premiumCalculateRequest.setPremiumType(Constants.PremiumType.BASIC_PREMIUM.name());
					premiumCalculateRequest.setProductType(Constants.Products.TERM_LIFE.name());
					jsonObjectData.put(Constants.BASIC_PREMIUM, Utils.getPremiumAmount(premiumCalculateRequest));
					premiumCalculateRequest.setPremiumType(Constants.PremiumType.ADDITIONAL_CRITICAL_PREMIUM.name());
					jsonObjectData.put(Constants.ADDITIONAL_CRITICAL_AMOUNT,
							Utils.getPremiumAmount(premiumCalculateRequest));
				}
				finalMap = new ObjectMapper().readValue(arrayOfPlans.toString(),
						new TypeReference<List<Map<String, Object>>>() {
						});

			}
			logger.info("<-- Exiting TermLifeService.buildQuickQuoteWithPremiumAmount()");
			return finalMap;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Internal Error : ", e.getMessage(), e);
			return finalMap;
		}
	}

	public ApiResponse quoteDetailsMasterModification(String userName, TlPremiumInput tlPremiumInput) {
		JSONArray keyListArray = new JSONArray();
		JSONObject quoteDetailsMasterData = null;
		JSONObject requestPayload;
		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();
		try {
			String languageValue = Utils.getStringValue(tlPremiumInput.getLanguage());
			JSONObject languageAsJson = masterApiIntegration.getFilterJsonObject(Constants.LANGUAGE_KEY, languageValue);
			keyListArray.put(languageAsJson);
			logger.info("<============> languageAsJson <============>" + languageAsJson);
			requestPayload = masterApiIntegration.buildRequestPayload(keyListArray);
			logger.info("<============> requestPayload <============> :: " + requestPayload);
			
			quoteDetailsMasterData = masterApiIntegration.getDataFromService(requestPayload,
					Constants.MASTERS_BANCA_GET_TERM_LIFE_QUOTE_DETAILS);
			JSONArray quoteDetailsJsonArray = quoteDetailsMasterData.getJSONArray(Constants.JSON_ARRAY_DATA_KEY);
			if (quoteDetailsJsonArray != null) {
				for (int j = 0; j < quoteDetailsJsonArray.length(); j++) {
					JSONObject quoteDetailsJsonObject = quoteDetailsJsonArray.getJSONObject(j);
					String qd_plan_id = quoteDetailsJsonObject.getString(Constants.PLAN_ID_KEY);
					PremiumCalculateRequest premiumCalculateRequest = new PremiumCalculateRequest();
					premiumCalculateRequest.setPlanId(qd_plan_id);
					premiumCalculateRequest.setAge(getAgeFromDb(userName));
					
					premiumCalculateRequest.setProductType(Constants.Products.TERM_LIFE.name());

					if (tlPremiumInput.getIsAdditionalKey()) {
						premiumCalculateRequest.setPremiumType(Constants.PremiumType.ADDITIONAL_CRITICAL_PREMIUM.name());
						premiumCalculateRequest.setAdditionalAmount(Utils.getPremiumAmount(premiumCalculateRequest));
						
//						premiumCalculateRequest.setPremiumType(Constants.PremiumType.PROTECTION_PREMIUM.name());
//						premiumCalculateRequest.setProtectionAmount((Utils.getPremiumAmount(premiumCalculateRequest)));
						
//						premiumCalculateRequest.setPremiumType(Constants.PremiumType.PROTECTION_PREMIUM.name());
//						quoteDetailsJsonObject.put(Constants.PROTECTION_AMOUNT,	Utils.getPremiumAmount(premiumCalculateRequest));
//						
					}else {
						removeCriticalIllenessText(languageValue,quoteDetailsJsonObject);
					}
					// get basic premium
					premiumCalculateRequest.setPremiumType(Constants.PremiumType.BASIC_PREMIUM.name());
					Integer basicPremiumAmount = Utils.getPremiumAmount(premiumCalculateRequest);

					if (basicPremiumAmount == null) {
						throw new FECException(ErrorCodes.INVALID_BASIC_PREMIUM_AMOUNT);
					}
					quoteDetailsJsonObject.put(Constants.BASIC_PREMIUM,basicPremiumAmount);
					
					// get protection premium
					premiumCalculateRequest.setPremiumType(Constants.PremiumType.PROTECTION_PREMIUM.name());
					Integer protectionAmount = 	Utils.convertToMillion(Utils.getPremiumAmount(premiumCalculateRequest));
					
					if( protectionAmount == null) {
						throw new FECException(ErrorCodes.INVALID_PROTECTION_AMOUNT);
					}
					quoteDetailsJsonObject.put(Constants.PROTECTION_AMOUNT,protectionAmount);
				}
			}
			finalMap = new ObjectMapper().readValue(quoteDetailsJsonArray.toString(),
					new TypeReference<List<Map<String, Object>>>() {
					});
			logger.info("finalMap ----> " +finalMap );
			logger.info("<-- Exiting TermLifeService.quoteDetailsMasterModification()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, finalMap);
		} catch (Exception e) {
			logger.error("<============> Internal Error <============> " + e.getMessage(), e);
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}
	}
	
	

	private void updatePremiumAmount(String processInstanceId, String basicPremiumAmount) throws Exception {
		runtimeService.setVariable(processInstanceId, WfImplConstants.JOURNEY_MONTHLY_PAYMENT, basicPremiumAmount);
		
		WorkFlowApplication workFlowApplication = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if(workFlowApplication == null) {
			throw new FECException(ErrorCodes.INVALID_WORKFLOW_APP);
		}
		workFlowApplication.setMontlyPayment(basicPremiumAmount);
		workflowApplicationDao.save(workFlowApplication);
		
		List<WorkFlowPolicy> workFlowPolicyList = workFlowPolicyDAO.findByWorkFlowApplicationAndIsDeleted(workFlowApplication, false);
		
		if(workFlowPolicyList == null || workFlowPolicyList.isEmpty()) {
			logger.error("workFlowPolicy list is null for appId "+workFlowApplication.getId());
			throw new Exception("Invalid workFlow Policy for appId "+workFlowApplication.getId());
		}
		
		for (WorkFlowPolicy workFlowPolicy : workFlowPolicyList) {
			workFlowPolicy.setMontlyPayment(basicPremiumAmount);
			workFlowPolicyDAO.save(workFlowPolicy);
		}
		
	}

	private void removeCriticalIllenessText(String languageValue,JSONObject quoteDetailsJsonObject) throws JSONException {
		if(languageValue.equals(Constants.LANGUAGE_VALUE)) {
		String middle_section_listing_title=quoteDetailsJsonObject.getString(Constants.MIDDLE_SECTION_LISTING_TITLE);
		middle_section_listing_title=middle_section_listing_title.replaceAll(Constants.CRITICAL_ILLNESS,"");
		quoteDetailsJsonObject.put(Constants.MIDDLE_SECTION_LISTING_TITLE, middle_section_listing_title);
		logger.info("quoteDetailsJsonObject ----> " +quoteDetailsJsonObject );
		}
		else {
			String middle_section_listing_title=quoteDetailsJsonObject.getString(Constants.MIDDLE_SECTION_LISTING_TITLE);
		middle_section_listing_title=middle_section_listing_title.replaceAll(Constants.CRITICAL_ILLNESS_VI,"");
		quoteDetailsJsonObject.put(Constants.MIDDLE_SECTION_LISTING_TITLE, middle_section_listing_title);
		}
		
	}

	private Integer getAgeFromDb(String userName) throws Exception {
		WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		int age = 0;
		if (workFlowUser != null) {
			logger.info("user ------>>  " + userName);
			String dateOfBirth = Utils.getStringValue(workFlowUser.getDob());
			logger.info(" <========= dateOfBirth =========> " + dateOfBirth);
			age = Utils.getAge(dateOfBirth);
			return age;
		} else {
			throw new Exception("User Does Not Exist");
		}
	}

	public ApiResponse updateTLPremium(String name, TlPremiumInput tlPremiumInput) throws Exception {
		
		logger.info("Update premium called for procId : "+tlPremiumInput.getProcessInstanceId()+",premium : "+ tlPremiumInput.getPremiumAmount());
		
		if(tlPremiumInput.getProcessInstanceId() == null || tlPremiumInput.getProcessInstanceId().isEmpty()) {
			throw new FECException(ErrorCodes.PROCESS_INSTANCE_ID_NULL);
		}
		
		if(tlPremiumInput.getPremiumAmount() == null || tlPremiumInput.getPremiumAmount().isEmpty()) {
			throw new FECException(ErrorCodes.INVALID_BASIC_PREMIUM_AMOUNT);
		}
		
		updatePremiumAmount(tlPremiumInput.getProcessInstanceId(),tlPremiumInput.getPremiumAmount());
		
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, null);
	}
}
