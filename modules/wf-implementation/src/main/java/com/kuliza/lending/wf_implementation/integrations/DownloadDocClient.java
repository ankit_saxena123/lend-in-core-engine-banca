package com.kuliza.lending.wf_implementation.integrations;

import javax.xml.soap.AttachmentPart;

import omnidocsapi.DownloadDoc;
import omnidocsapi.DownloadDocResponse;
import omnidocsapi.SysRequestType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.wf_implementation.WfImplConfig;

@Component
public class DownloadDocClient extends WebServiceGatewaySupport {
	
	@Autowired
	private WfImplConfig wfImplConfig;

	public AttachmentPart getDownloadedDoc(String docId, String volumeId, boolean toPdf, SysRequestType sys) {

		try {
			DownloadDoc request = new DownloadDoc();
			request.setDocID(docId);
			request.setVolumeId(volumeId);
			request.setToPDF(toPdf);
			request.setSys(sys);
			CustomLogger.log(Thread.currentThread().getStackTrace()[1].getMethodName(), null, null,
					JobType.OUTBOUND_API, docId, null,
					"getDownloadedDoc request: " + request.toString());
			WebServiceTemplate template = getWebServiceTemplate();
			DownloadDocResponse response = new DownloadDocResponse();
			final SoapActionCallback soapActionCallback = new SoapActionCallback("http://OmnidocsAPI/downloadDoc");
			response = (DownloadDocResponse) template
					.marshalSendAndReceive(wfImplConfig.getOmnidocsEndPoint(), request, soapActionCallback);
			CustomLogger.log(Thread.currentThread().getStackTrace()[1].getMethodName(), null, null,
					JobType.OUTBOUND_API, docId, null,
					"getDownloadedDoc response: " + response.toString());
			if (1 == response.getSys().getCode()) {
				ClientInterceptor[] inters = template.getInterceptors();
				for (ClientInterceptor inte : inters) {
					if (inte instanceof FileDownloadInterceptor) {
						return ((FileDownloadInterceptor) inte).getAttachment();
					}
				}

			}
			return null;
		} catch (Exception e) {
			CustomLogger.logException(Thread.currentThread().getStackTrace()[1].getMethodName(), null, null,
					JobType.OUTBOUND_API, docId, null, e,
					"DownloadDocClient returned Exception");
			return null;
		}
	}

}