package com.kuliza.lending.wf_implementation.models;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "confirmation_token")
public class ConfirmationToken extends BaseModel {

	@Column(name = "confirmation_token")
	private String confirmationToken;

	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "email_id")
	private String emailId;
	
	@Column(nullable = false, name="is_mail_sent")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isMailSent = false;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false, name = "workflow_application_id")
	private WorkFlowApplication workFlowApplication;

	public ConfirmationToken() {
	}

	public boolean isMailSent() {
		return isMailSent;
	}

	public void setMailSent(boolean isMailSent) {
		this.isMailSent = isMailSent;
	}

	public ConfirmationToken(WorkFlowApplication workFlowApplication) {
		this.workFlowApplication = workFlowApplication;
		confirmationToken = UUID.randomUUID().toString();
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public WorkFlowApplication getWorkFlowApplication() {
		return workFlowApplication;
	}

	public void setWorkFlowApplication(WorkFlowApplication workFlowApplication) {
		this.workFlowApplication = workFlowApplication;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
