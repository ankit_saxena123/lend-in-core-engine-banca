package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "topic_messages")
public class TopicMessages extends BaseModel {

	@Column(nullable = true, name = "topic_name")
	private String topic;

	@Column(nullable = true, name = "message")
	private String message;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
