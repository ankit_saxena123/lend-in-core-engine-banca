package com.kuliza.lending.wf_implementation.pojo;

public class PaymentConfirmForm {

	String refNum;
	
	String contractNumber;
	
	String contactNumber;

	String customerName;

	String description;

	String paymentDate;

	

	String paymentAmount;

	String customerBankAccount;

	public PaymentConfirmForm() {
		super();
	}

	public PaymentConfirmForm(String refNum, String contactNumber,
			String customerName, String description, String paymentDate,
			String paymentAmount, String customerBankAccount) {
		super();
		this.refNum = refNum;
		this.contactNumber = contactNumber;
		this.customerName = customerName;
		this.description = description;
		this.paymentDate = paymentDate;
		this.paymentAmount = paymentAmount;
		this.customerBankAccount = customerBankAccount;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getCustomerBankAccount() {
		return customerBankAccount;
	}

	public void setCustomerBankAccount(String customerBankAccount) {
		this.customerBankAccount = customerBankAccount;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	@Override
	public String toString() {
		return "PaymentConfirmForm [refNum=" + refNum + ", contractNumber=" + contractNumber + ", contactNumber="
				+ contactNumber + ", customerName=" + customerName + ", description=" + description + ", paymentDate="
				+ paymentDate + ", paymentAmount=" + paymentAmount + ", customerBankAccount=" + customerBankAccount
				+ "]";
	}

}
