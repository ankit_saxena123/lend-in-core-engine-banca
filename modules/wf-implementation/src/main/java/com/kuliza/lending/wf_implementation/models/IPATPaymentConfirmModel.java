package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "ipat_payment_confirm")
public class IPATPaymentConfirmModel extends BaseModel {

	@Column(nullable = true, name = "ref_num")
	private String refNum;
	
	@Column(nullable = true, name = "crm_id")
	private String crmId;

	@Column(nullable = true, name = "contact_number")
	private String contactNumber;

	@Column(nullable = true, name = "customer_name")
	private String customerName;
	
	@Column(nullable = true, name = "description")
	private String description;
	
	@Column(nullable = true, name = "payment_date")
	private String paymentDate;
	
	@Column(nullable = true, name = "payment_amount")
	private String paymentAmount;
	
	@Column(nullable = true, name = "customer_bank_account")
	private String customerBankAccount;

	@Column(nullable = true, name = "process_flag")
	private String processFlag;
	
	@Column(name="process_instance_id",nullable = true)
	private String processInstanceId;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable = false, name = "payment_info_status")
	private boolean paymentInfoStatus=false;
	
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable = false, name = "payment_confirm_status")
	private boolean paymentConfirmStatus=false;
	
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable = false, name = "insurer_status")
	private boolean insurerStatus=false;
	
	
	@Column(name = "ipat_response", columnDefinition = "LONGTEXT")
	private String IPATResponse;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false, name="wf_application_id")
	private WorkFlowApplication wfApplication;
	
	@Column(nullable = true, name = "payoo_transaction_id")
	private String payooTransactionId;
	
	@Column(nullable = true, name = "partial_payment")
	private String partialPayment;
	
	@Column(nullable = true, name = "access_payment")
	private String accessPayment;
	
	public IPATPaymentConfirmModel(String refNum, String contactNumber, String customerName, String description,
			String paymentDate, String paymentAmount, String customerBankAccount) {
		super();
		this.refNum = refNum;
		this.contactNumber = contactNumber;
		this.customerName = customerName;
		this.description = description;
		this.paymentDate = paymentDate;
		this.paymentAmount = paymentAmount;
		this.customerBankAccount = customerBankAccount;
	}
	
	public IPATPaymentConfirmModel(){
		super();
	}
	
	public String getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getCustomerBankAccount() {
		return customerBankAccount;
	}

	public void setCustomerBankAccount(String customerBankAccount) {
		this.customerBankAccount = customerBankAccount;
	}

	public WorkFlowApplication getWfApplication() {
		return wfApplication;
	}

	public void setWfApplication(WorkFlowApplication wfApplication) {
		this.wfApplication = wfApplication;
	}

	public String getIPATResponse() {
		return IPATResponse;
	}

	public void setIPATResponse(String iPATResponse) {
		IPATResponse = iPATResponse;
	}

	public boolean isPaymentInfoStatus() {
		return paymentInfoStatus;
	}

	public void setPaymentInfoStatus(boolean paymentInfoStatus) {
		this.paymentInfoStatus = paymentInfoStatus;
	}

	public boolean isPaymentConfirmStatus() {
		return paymentConfirmStatus;
	}

	public void setPaymentConfirmStatus(boolean paymentConfirmStatus) {
		this.paymentConfirmStatus = paymentConfirmStatus;
	}

	public boolean isInsurerStatus() {
		return insurerStatus;
	}

	public void setInsurerStatus(boolean insurerStatus) {
		this.insurerStatus = insurerStatus;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getPayooTransactionId() {
		return payooTransactionId;
	}

	public void setPayooTransactionId(String payooTransactionId) {
		this.payooTransactionId = payooTransactionId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	
	
	public String getPartialPayment() {
		return partialPayment;
	}
	public void setPartialPayment(String partialPayment) {
		this.partialPayment = partialPayment;
	}
	public String getAccessPayment() {
		return accessPayment;
	}
	public void setAccessPayment(String accessPayment) {
		this.accessPayment = accessPayment;
	}

	@Override
	public String toString() {
		return "IPATPaymentConfirmModel [refNum=" + refNum + ", crmId=" + crmId + ", contactNumber=" + contactNumber
				+ ", customerName=" + customerName + ", description=" + description + ", paymentDate=" + paymentDate
				+ ", paymentAmount=" + paymentAmount + ", customerBankAccount=" + customerBankAccount + ", processFlag="
				+ processFlag + ", processInstanceId=" + processInstanceId + ", paymentInfoStatus=" + paymentInfoStatus
				+ ", paymentConfirmStatus=" + paymentConfirmStatus + ", insurerStatus=" + insurerStatus
				+ ", IPATResponse=" + IPATResponse + ", wfApplication=" + wfApplication + ", payooTransactionId="
				+ payooTransactionId + ", partialPayment=" + partialPayment + ", accessPayment=" + accessPayment + "]";
	}

	
	
	
	
	
}
