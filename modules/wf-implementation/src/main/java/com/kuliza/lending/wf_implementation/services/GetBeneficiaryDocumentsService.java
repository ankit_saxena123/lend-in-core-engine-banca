package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import omnidocsapi.UploadDocResponse;

import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.GenerateApplicationIdIntegration;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service("getBeneficiaryDocumentsService")
public class GetBeneficiaryDocumentsService {

	@Autowired
	private MasterApiIntegration master;

	private static final Logger logger = LoggerFactory.getLogger(GetBeneficiaryDocumentsService.class);

	/**
	 * this is a service task
	 * @param deletegateExecution
	 * @return
	 */
	public DelegateExecution getBeneficiaryDocuments(DelegateExecution delegateExecution) {
		try{
			logger.info("-->Delegate execution GetBeneficiaryDocumentsService.getBeneficiaryDocuments()");
			
			Map<String, Object> variables = delegateExecution.getVariables();
			String language = Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY));
			
			ArrayList<String> ipBCList = new ArrayList<String>();
			List<Map<String, String>> beneficiary_dcos_list = null;
			
			@SuppressWarnings("unchecked")
			List<Map<String, String>> bcip = (List<Map<String, String>>) variables
					.get(WfImplConstants.CLAIMS_BENIFICIARY_CODES);
			
			logger.info("-->Delegate execution GetBeneficiaryDocumentsService.language--"+language);
		
			for (Map<String, String> memberMap : bcip) {
				logger.info("-->Delegate execution GetBeneficiary Code <--"+(WfImplConstants.BENIFICIARY_CODE));
				ipBCList.add(memberMap.get(WfImplConstants.BENIFICIARY_CODE));
			}
					 
			beneficiary_dcos_list = getDataFromMaster(ipBCList,language);
		
			for(int i=0;i<beneficiary_dcos_list.size();i++){
				logger.info("-->Delegate execution Beneficiary OP Docs List Title--> --"+i+"--"+beneficiary_dcos_list.get(i).get(WfImplConstants.BENIFICIARY_TITLE));
				logger.info("-->Delegate execution Beneficiary OP Docs Beneficiary Code--> --"+i+"--"+beneficiary_dcos_list.get(i).get(WfImplConstants.BENIFICIARY_MASTER_CODE));

			}
			
			delegateExecution.setVariable(WfImplConstants.HOSPITAL_BILL_SYSTEM, beneficiary_dcos_list);
		}
		catch(Exception e){
			logger.error("<=======>unable to get getBeneficiaryDocuments :"+e.getMessage());
		}
		
		return delegateExecution;
	}
	
	public List<Map<String, String>> getDataFromMaster(ArrayList<String> ipBCList,String language){

		JSONArray beneficiaryCodesMaster = new JSONArray();
		JSONObject beneficiaryCodeMasterResponses;
		List<Map<String, String>> beneficiary_docs = new ArrayList<Map<String, String>>();

		try{
			beneficiaryCodeMasterResponses = master.getDataFromService(WfImplConstants.BANCA_GET_BENIFICIARY_DOC);

			if (beneficiaryCodeMasterResponses != null
					&& beneficiaryCodeMasterResponses.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				beneficiaryCodesMaster = master.parseResponseFromMaster(beneficiaryCodeMasterResponses);
				HashSet<String> set = new HashSet<String>();

				for(int i=0;i<beneficiaryCodesMaster.length();i++){
					JSONObject jo = beneficiaryCodesMaster.getJSONObject(i);
					logger.info("-->Delegate execution beneficiaryCodesMaster response -->"+jo);
					
					for(int ii=0;ii<ipBCList.size();ii++){
						String bf_code = ipBCList.get(ii);
					
						if(jo.getString(WfImplConstants.BENIFICIARY_MASTER_CODE).equals(bf_code) && jo.getString(Constants.LANGUAGE_KEY).equals(language)){
							boolean bd = set.add(jo.getString("description"));
							boolean bt = set.add(jo.getString("title"));
				        
							if(bt == true ){
								Map<String,String> map = new HashMap<String,String>();
								map.put("title", jo.getString(WfImplConstants.BENIFICIARY_TITLE));
								map.put("description", jo.getString(WfImplConstants.BENIFICIARY_DESCRIPTION));
								map.put("language", jo.getString(Constants.LANGUAGE_KEY));
								map.put("benefitcode",jo.getString(WfImplConstants.BENIFICIARY_MASTER_CODE));
								beneficiary_docs.add(map);
							}
						}else
							continue;
						
					}
				
				
				}
			}
			
		}catch(Exception e){
			
		}
		return beneficiary_docs;
	}
	
	public JSONArray objectToJSONArray(Object object){

	    JSONArray jsonArray = null;

		try{
		    Object json = null;
	
	
		    try {
	
		        json = new JSONTokener(object.toString()).nextValue();
	
		    } catch (JSONException e) {
	
		        e.printStackTrace();
	
		    }
	
		    if (json instanceof JSONArray) {
	
		        jsonArray = (JSONArray) json;
	
		    }
		}catch(Exception e)   {
			logger.error("<-- Unable to convert to json array in delegate execution getBeneficiaryDocuments :"+e.getMessage());

		}
	    return jsonArray;
	}
}
