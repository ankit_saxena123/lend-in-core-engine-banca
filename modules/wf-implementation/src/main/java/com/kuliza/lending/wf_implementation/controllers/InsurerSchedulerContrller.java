package com.kuliza.lending.wf_implementation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.services.InsurerScheduleService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping("/insurer")
public class InsurerSchedulerContrller {

	
	private final Logger logger = LoggerFactory.getLogger(InsurerSchedulerContrller.class);
	
	@Autowired
	private InsurerScheduleService SchedularService;
	
	
	@GetMapping("/scheduler")
	public ResponseEntity<Object> insurerScheduller() {
		ApiResponse apirespo=null;
		try {
			if(SchedularService.scheduler()) {;
			apirespo=new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
			}
			else {
				apirespo=new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE);

		} 
		}catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}
		return  CommonHelperFunctions.buildResponseEntity(apirespo);

	}
	
	
	
	
	
}
