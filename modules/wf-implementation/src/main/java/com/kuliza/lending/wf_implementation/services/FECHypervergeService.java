package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaCIFCustomerInfoBasicIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaCIFCustomerSearchListIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditHypervergeIntegration;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service("fECHypervergeService")
public class FECHypervergeService {


	@Autowired
	private WorkflowUserDao workflowuserDao;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private FECDMSService fecDmsService;

	@Autowired
	private FECreditHypervergeIntegration hypervergeIntegration;

	@Autowired
	private FECreditBancaCIFCustomerSearchListIntegration cifCustomerSearchIntegration;

	@Autowired
	private FECreditBancaCIFCustomerInfoBasicIntegration cifCustomerBasicInfoIntegration;

	private static final Logger logger = LoggerFactory
			.getLogger(FECHypervergeService.class);
	
/**
 * gets the docId from the processVariables and calls hypervege and cif apis.
 * @param execution
 * @return DelegateExecution
 */
	public DelegateExecution readNID(DelegateExecution execution) {
		Map<String, Object> processVariables = new HashMap<String, Object>();
		try {
			execution.setVariable(WfImplConstants.NID_ERROR, false);
			String dmsNidFrontDocId = Utils.getStringValue(execution
					.getVariable(WfImplConstants.NID_FRONT));
			String dmsNidBackId = Utils.getStringValue(execution
					.getVariable(WfImplConstants.NID_BACK));
			String mobileNumber = Utils.getStringValue(execution
					.getVariable(WfImplConstants.APPLICATION_MOBILE_NUMBER));
			
			logger.info("dmsNidFrontDocId :" + dmsNidFrontDocId);
			logger.info("dmsNidBackId :" + dmsNidBackId);
			logger.info("mobileNumber :" + mobileNumber);
			String nationalId = null;
			Map<String, Object> cifDataMap = new HashMap<String, Object>();
			Map<String, Object> hypervergeFrontData = new HashMap<String, Object>();
			Map<String, Object> hypervergeBackData = new HashMap<String, Object>();
			CloseableHttpResponse closeableHttpResponse = callHyperVerge(dmsNidFrontDocId);
			int statusCode = closeableHttpResponse.getStatusLine()
					.getStatusCode();
			String hypervergeFrontResponse = EntityUtils
					.toString(closeableHttpResponse.getEntity());
			logger.info("hyperverge Front Response : "
					+ hypervergeFrontResponse);
			execution.setVariable(WfImplConstants.HYPERVERGE_FRONT_RESPONSE, hypervergeFrontResponse);
			closeableHttpResponse.close();

			if (statusCode != HttpStatus.OK.value()) {
				String error = new JSONObject(hypervergeFrontResponse)
						.getString(WfImplConstants.CIF_ERROR);
				logger.info("hyperverge call failed : " + error);
				processVariables.put(WfImplConstants.NID_ERROR, true);
				processVariables.put(WfImplConstants.NID_ERROR_MESSAGE,
						"error in nid front :" + error);
			}
			else{
			hypervergeFrontData = hypervergeIntegration
					.parseNationalIdFrontData(hypervergeFrontResponse);

			nationalId = (String) hypervergeFrontData.getOrDefault(
					WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY, "");
			}

			// calling hyperverge to get nid back details
			closeableHttpResponse = callHyperVerge(dmsNidBackId);
			statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
			String hypervergeBackResponse = EntityUtils
					.toString(closeableHttpResponse.getEntity());
			logger.info("hyperverge back Response : " + hypervergeBackResponse);
			closeableHttpResponse.close();
			execution.setVariable(WfImplConstants.HYPERVERGE_BACK_RESPONSE, hypervergeBackResponse);
			if (statusCode != HttpStatus.OK.value()) {
				String error = new JSONObject(hypervergeFrontResponse)
						.getString(WfImplConstants.CIF_ERROR);
				logger.info("hyperverge call failed : " + error);
				processVariables.put(WfImplConstants.NID_ERROR, true);
				processVariables.put(WfImplConstants.NID_ERROR_MESSAGE,
						" error in nid back :" + error);

			}
			else{
			hypervergeBackData = hypervergeIntegration
					.parseNationalIdBackData(hypervergeBackResponse);
			}
			logger.info("nationalId is : " + nationalId);
			// calling cif apis
			cifDataMap = getCIFDataMap(nationalId, mobileNumber, execution);
			processVariables = addProcessVariables(hypervergeFrontData,
					hypervergeBackData, cifDataMap, nationalId, execution);
			if(processVariables.isEmpty())
			{
				logger.info("user data not found in both hyperverge and cif");
				execution.setVariable(WfImplConstants.NID_ERROR, true);
				execution.setVariable(WfImplConstants.NID_ERROR_MESSAGE,  "user data not found in both hyperverge and cif");
			}
			execution.setVariables(processVariables);

		} catch (Exception e) {
			execution.setVariable(WfImplConstants.NID_ERROR, true);
			execution.setVariable(WfImplConstants.NID_ERROR_MESSAGE,  e.getMessage());
			logger.error("failed to get nid details : " + e.getMessage(), e);
			
		}
		return execution;

	}
/**
 * calls the cif api and gets the cif data in map
 * @param nationalId
 * @param mobileNumber
 * @param execution
 * @return Map<String, Object>
 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getCIFDataMap(String nationalId,
			String mobileNumber, DelegateExecution execution)  {
		Map<String, Object> customerBasicInfoResponse = new HashMap<String, Object>();
		if(nationalId==null || nationalId.isEmpty()){
			return customerBasicInfoResponse;
		}
		try {
			JSONObject requestPayload = cifCustomerSearchIntegration
					.buildRequestPayload(nationalId, mobileNumber);
			HttpResponse<JsonNode> responseFromIntegrationBroker = cifCustomerSearchIntegration
					.getDataFromService(requestPayload);
			logger.info("response::>>>>>>>>>>>"
					+ cifCustomerSearchIntegration.toString());
			Map<String, Object> getCustomerListResponseMapObject = new HashMap<String, Object>();
			Map<String, Object> customersMapObject = new HashMap<String, Object>();
			Map<String, Object> customerMapObject = new HashMap<String, Object>();
			Map<String, Object> parseResponseMap = cifCustomerSearchIntegration
					.parseResponse(responseFromIntegrationBroker.getBody()
							.getObject());
			if ((Boolean) parseResponseMap.get(Constants.STATUS_MAP_KEY) == true) {
				customerBasicInfoResponse.put(Constants.STATUS_MAP_KEY, true);
				logger.info("parseJsonObject::>>>>>>>>>>>>>>>>::::::"
						+ parseResponseMap);
				getCustomerListResponseMapObject = (Map<String, Object>) parseResponseMap
						.get(WfImplConstants.NS1_GET_CUSTOMER_LIST_RESPONSE);
				customersMapObject = (Map<String, Object>) getCustomerListResponseMapObject
						.get(WfImplConstants.CIF_CUSTOMERS);
				logger.info("customersMapObject::>>>>>>>>>>>>>>>>>>>>>>>>>>>"
						+ customersMapObject);
				if (customersMapObject.get(WfImplConstants.CIF_CUSTOMER) instanceof Map<?, ?>) {
					customerMapObject = (Map<String, Object>) customersMapObject
							.get(WfImplConstants.CIF_CUSTOMER);
				} else if (customersMapObject.get(WfImplConstants.CIF_CUSTOMER) instanceof List) {
					List<Map<String, Object>> customersList = (List<Map<String, Object>>) customersMapObject
							.get(WfImplConstants.CIF_CUSTOMER);
					customerMapObject = customersList.get(0);
				} else {
					logger.info("Customer object is type : "
							+ customersMapObject.get(WfImplConstants.CIF_CUSTOMER).getClass());
				}
				String cifNumber = customerMapObject.get(WfImplConstants.CIF_NUMBER)
						.toString();
				logger.info("cifNumber:" + cifNumber);
				customerBasicInfoResponse = getCustomerBasicInfo(cifNumber);
				logger.info("customerBasicInfoResponse :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
						+ customerBasicInfoResponse);
				execution.setVariable(WfImplConstants.CIF_RESPONSE, customerBasicInfoResponse);

			} else {
				customerBasicInfoResponse.put(Constants.STATUS_MAP_KEY, false);
			}
		} catch (Exception e) {
			logger.error("failed to getCIFData  " + e.getMessage(), e);
			customerBasicInfoResponse.put("status", false);
		}
		return customerBasicInfoResponse;
	}

	public CloseableHttpResponse callHyperVerge(String nidDocId)
			throws Exception {
		File file =null;
		try{
		file= fecDmsService.downloadDocumentAsFile(nidDocId, "", true);
		CloseableHttpResponse hypervergeResponse = hypervergeIntegration
				.getDataFromService(file);
		return hypervergeResponse;
		}
		finally{
			if(file!=null)
			file.deleteOnExit();
		}
		

	}

	public Map<String, Object> addProcessVariables(
			Map<String, Object> hypervergeFrontData,
			Map<String, Object> hypervergeBackData,
			Map<String, Object> cifDataMap, String nationalId, DelegateExecution execution) {
		
		Map<String, Object> processVariableMap = new HashMap<String, Object>();
		Map<String, Object> variables = execution.getVariables();
		String language = Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY));
		boolean cifStatus=(boolean)cifDataMap.getOrDefault(Constants.STATUS_MAP_KEY, false);
		if (cifStatus ) {
			logger.info("cif data is available");
			processVariableMap.put(WfImplConstants.NATIONAL_ID_HYPERVERGE, nationalId);
			processVariableMap.put(WfImplConstants.FULLNAME_HYPERVERGE,
					Utils.getStringValue(cifDataMap
							.get(Constants.CIF_FULL_NAME_KEY)));
			String gender=(String)cifDataMap.getOrDefault(Constants.CIF_GENDER_KEY, "");
			gender=cifCustomerBasicInfoIntegration.getGender(gender);
			if(gender.isEmpty())
				gender=Utils.getStringValue(execution.getVariable(WfImplConstants.USER_GENDER));
			if(language!=null && language.equals(Constants.BANCA_DEFAULT_LANGUAGE) && gender.equals(Constants.NID_FEMALE)) {
				gender=Constants.NID_FEMALE_VI;
			}else if(language!=null && language.equals(Constants.BANCA_DEFAULT_LANGUAGE) && gender.equals(Constants.NID_MALE)) {
				gender=Constants.NID_MALE_VI;
			}
			processVariableMap.put(WfImplConstants.GENDER_HYPERVERGE, gender);
			processVariableMap
					.put(WfImplConstants.DOB_HYPERVERGE, Utils.getStringValue(cifDataMap
							.get(Constants.CIF_BIRTHDAY_KEY)));
			processVariableMap.put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE, Utils
					.getStringValue(cifDataMap
							.get(Constants.CIF_COUNTRY_NAME_KEY)));
			String addressNumber = Utils.getStringValue(cifDataMap
					.get(Constants.CIF_ADDRESS_NUMBER_KEY));
			String street = Utils.getStringValue(cifDataMap
					.get(Constants.CIF_STREET_KEY));
			processVariableMap.put(WfImplConstants.ADDRESS_HYPERVERGE, addressNumber+" "+street);
			processVariableMap.put(WfImplConstants.NATIONALITY_HYPERVERGE, WfImplConstants.VIETNAMESE);
		} else {
			boolean hypervergeFrontStatus= (boolean)hypervergeFrontData.getOrDefault(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
			boolean hypervergeBackStatus= (boolean)hypervergeBackData.getOrDefault(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, false);
			logger.info("hypervergeFrontStatus : "+hypervergeFrontStatus);
			logger.info("hypervergeBackStatus : "+hypervergeBackStatus);
			if(hypervergeFrontStatus && hypervergeBackStatus){
				logger.info("hyperverge data is available");
			String nidName = (String) hypervergeFrontData.getOrDefault(
					WfImplConstants.BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY, "");
			String nidGender = (String) hypervergeFrontData.getOrDefault(
					WfImplConstants.GENDER_FROM_HYPERVERGE_KEY, "");
			nidGender=hypervergeIntegration.getGender(nidGender);
			if(nidGender.isEmpty())
				nidGender=Utils.getStringValue(execution.getVariable(WfImplConstants.USER_GENDER));
			String nidDob = (String) hypervergeFrontData.getOrDefault(
					WfImplConstants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY, "");
			processVariableMap
					.put(WfImplConstants.NATIONAL_ID_HYPERVERGE,
							Utils.getStringValue(hypervergeFrontData
									.get(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY)));
			processVariableMap.put(WfImplConstants.FULLNAME_HYPERVERGE, nidName);
			processVariableMap.put(WfImplConstants.GENDER_HYPERVERGE, nidGender);
			processVariableMap.put(WfImplConstants.DOB_HYPERVERGE, nidDob);
			processVariableMap
					.put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
							Utils.getStringValue(hypervergeFrontData
									.get(WfImplConstants.PROVINCE_FROM_HYPERVERGE_KEY)));
			processVariableMap.put(WfImplConstants.ADDRESS_HYPERVERGE, Utils
					.getStringValue(hypervergeFrontData
							.get(WfImplConstants.ADDRESS_FROM_HYPERVERGE_KEY)));
			processVariableMap.put(WfImplConstants.NATIONALITY_HYPERVERGE, WfImplConstants.VIETNAMESE);
		
			}
			}
		
		return processVariableMap;

	}

	/**
	 * This Method takes cifNumber and return parseResponse of Map
	 * 
	 * 
	 * @param cifNumber
	 * @return parseResponseMap
	 * @throws Exception
	 */
	public Map<String, Object> getCustomerBasicInfo(String cifNumber)
			throws Exception {
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();

		JSONObject getRequestPayloadForCif = cifCustomerBasicInfoIntegration
				.buildRequestPayload(cifNumber);
		getResponseFromIntegrationBroker = cifCustomerBasicInfoIntegration
				.getDataFromService(getRequestPayloadForCif);
		JSONObject parsedResponseForCif = getResponseFromIntegrationBroker
				.getBody().getObject();
		parseResponseMap = cifCustomerBasicInfoIntegration
				.parseResponse(parsedResponseForCif);

		return parseResponseMap;
	}
}
