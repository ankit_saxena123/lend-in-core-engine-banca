package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "vtiger_dropoff_status")
public class VtigerDropoffModel extends BaseModel {
	
	@Column(name="user_name", nullable = true)
	private String userName;
	@Column(name="national_id", nullable = true)
	private String nationalId;

	@Column(name="contact_number",nullable = true)
	private String contactNumber;
	
	@Column(name="application_id",nullable = true)
	private String applicationId;

	@Column(name="process_instance_id",nullable = true)
	private String processInstanceId;

	@Column(name="vtiger_contact_id",nullable = true)
	private String vTigerContactId;

	@Column(name="vtiger_opportunity_id",nullable = true)
	private String vTigerOpportunityId;

	@Column(name="vtiger_parent_crm_id",nullable = true)
	private String vTigerParentCrmId;
	
	@Column(name="status",nullable = true)
	private String status;
	
	@Column(name="drop_off_type",nullable = true)
	private String dropOffType;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(name="is_updated_invtiger",nullable = false)
	private boolean isUpdatedInVtiger;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(name="inactive", nullable=false)
	private boolean inActive;
	@Column(name="current_journey_stage",nullable = true)
	private String currentJourneyStage;
	@Column(name="push_notification_count",nullable = false)
	private int pushNotificationCount=0;
	@Column(name="language",nullable = true)
	private String language;
	@Column(name="product_selection",nullable = true)
	private String productSelection;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getProductSelection() {
		return productSelection;
	}

	public void setProductSelection(String productSelection) {
		this.productSelection = productSelection;
	}

	public VtigerDropoffModel() {
		super();
	}
	
	public VtigerDropoffModel(String nationalId, String contactNumber, String applicationId, String processInstanceId,
			String vTigerContactId, String vTigerOpportunityId, String status, boolean isUpdatedInVtiger,String vTigerParentCrmId) {
		super();
		this.nationalId = nationalId;
		this.contactNumber = contactNumber;
		this.applicationId = applicationId;
		this.processInstanceId = processInstanceId;
		this.vTigerContactId = vTigerContactId;
		this.vTigerOpportunityId = vTigerOpportunityId;
		this.status = status;
		this.isUpdatedInVtiger = isUpdatedInVtiger;
		this.vTigerParentCrmId = vTigerParentCrmId;
	}
	
	public String getvTigerParentCrmId() {
		return vTigerParentCrmId;
	}

	public void setvTigerParentCrmId(String vTigerParentCrmId) {
		this.vTigerParentCrmId = vTigerParentCrmId;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getvTigerContactId() {
		return vTigerContactId;
	}

	public void setvTigerContactId(String vTigerContactId) {
		this.vTigerContactId = vTigerContactId;
	}

	public String getvTigerOpportunityId() {
		return vTigerOpportunityId;
	}

	public void setvTigerOpportunityId(String vTigerOpportunityId) {
		this.vTigerOpportunityId = vTigerOpportunityId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isUpdatedInVtiger() {
		return isUpdatedInVtiger;
	}

	public void setUpdatedInVtiger(boolean isUpdatedInVtiger) {
		this.isUpdatedInVtiger = isUpdatedInVtiger;
	}


	public String getDropOffType() {
		return dropOffType;
	}

	public void setDropOffType(String dropOffType) {
		this.dropOffType = dropOffType;
	}

	public boolean isInActive() {
		return inActive;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public String getCurrentJourneyStage() {
		return currentJourneyStage;
	}

	public void setCurrentJourneyStage(String currentJourneyStage) {
		this.currentJourneyStage = currentJourneyStage;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPushNotificationCount() {
		return pushNotificationCount;
	}

	public void setPushNotificationCount(int pushNotificationCount) {
		this.pushNotificationCount = pushNotificationCount;
	}
	
	
	
	
}
