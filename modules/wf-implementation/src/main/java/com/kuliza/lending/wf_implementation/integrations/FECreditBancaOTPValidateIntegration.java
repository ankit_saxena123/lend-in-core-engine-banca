package com.kuliza.lending.wf_implementation.integrations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.OTPConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditBancaOTPValidateIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;
	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaOTPValidateIntegration.class);

	public JSONObject buildRequestPayload(Map<String, Object> data) throws JSONException {
		JSONObject requestPayload = new JSONObject();
		JSONObject sysRequest = new JSONObject();
		sysRequest.put("srv:ValidateOTP.sys.TransID", Utils.generateUUIDTransanctionId());
		sysRequest.put("srv:ValidateOTP.sys.RequestorID", Constants.FEC_REQUESTOR_ID_OTP);
		sysRequest.put("srv:ValidateOTP.sys.DateTime", Utils.getCurrentDateTime());
		String mobile = Utils.getStringValue(data.get(Constants.MOBILE_NUMBER_KEY));
		if (mobile.startsWith("0")) {
			mobile = mobile.substring(1, mobile.length());
		}
		logger.info("<=========>mobile :"+mobile+"<=============>");
		sysRequest.put("srv:ValidateOTP.phone", "+84" + mobile);
		sysRequest.put("srv:ValidateOTP.otp", Utils.getStringValue(data.get(Constants.FE_CREDIT_MVP_OTP_KEY)));
		requestPayload.put("body", sysRequest);
		return requestPayload;
	}

	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put("Content-Type", Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.GENERATE_VALIDATE_OTP_INTEGRATION_SLUG + "/" + Constants.VALIDATE_OTP_SLUG
				+ "/" + wfImplConfig.getIbCompany()+ "/" + Constants.IB_HASH_VALUE
				+"/?env="+wfImplConfig.getIbEnv()+ Constants.OTP_INTEGRATION_SOAP_KEY;
		logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		return resposeAsJson;
	}

	// Here you have to return the Map which needs to be stored in process
	// variables.
	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		Map<String, Object> processVariablesFromSMSIntegration = new HashMap<>();
		List<String> successCodes = new ArrayList<String>();
		successCodes.add(Constants.FE_CREDIT_OTP_VALIDATION_SUCCESS_CODE_KEY);
		if (processVariablesFromSMSIntegration.isEmpty()) {
			if (responseFromIntegrationBroker.has("NS1:ValidateOTP_Response")
					&& responseFromIntegrationBroker.get("NS1:ValidateOTP_Response") instanceof JSONObject) {
				JSONObject actualResponse = responseFromIntegrationBroker.getJSONObject("NS1:ValidateOTP_Response");
				if (actualResponse.has("status")) {
					logger.debug("Success Response for FEC OTP Validation");
					String successDate = Utils.getCurrentDateTime().toString();
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_OTP_VALIDATION_SUCCESS_STATUS_KEY,
							"true");
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_OTP_VALIDATION_LAST_SUCCESS_DATE_KEY,
							successDate);
					if (actualResponse.has("status") && actualResponse.get("status") instanceof Integer) {
						processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_OTP_VALIDATED_STATUS_KEY,
								actualResponse.getInt("status"));
					} else if (actualResponse.has("status") && actualResponse.get("status") instanceof String) {
						if (actualResponse.getString("status").equals("0")) {
							processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_OTP_VALIDATED_STATUS_KEY, 0);
						} else {
							if (Utils.getIntegerValue(actualResponse.getString("status")) == 1) {
								processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_OTP_VALIDATED_STATUS_KEY,
										-1);
							} else {
								processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_OTP_VALIDATED_STATUS_KEY,
										Utils.getIntegerValue(actualResponse.getString("status")));
							}
						}
					} else {
						processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_OTP_VALIDATED_STATUS_KEY, -1);
					}
					processVariablesFromSMSIntegration.put(Constants.FE_CREDIT_MVP_OTP_VALIDATED_KEY,
							Utils.getStringValue(actualResponse.get("description")));
				} else {
					throw new Exception("No Status Key in Actual Response for FEC OTP Validation");
				}
			}
		} else {
			throw new Exception("Error Response for FEC OTP Validation");
		}
		return processVariablesFromSMSIntegration;
	}
}