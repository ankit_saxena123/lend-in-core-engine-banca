package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.kuliza.lending.wf_implementation.models.IPATPaymentInfoModel;

@Repository
public interface IPATPaymentInfoDao extends CrudRepository<IPATPaymentInfoModel, Long> {

	public IPATPaymentInfoModel findById(long id);

}
