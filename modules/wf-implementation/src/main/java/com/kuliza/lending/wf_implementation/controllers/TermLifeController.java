package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.pojo.ProductRequest;
import com.kuliza.lending.wf_implementation.pojo.TlPremiumInput;
import com.kuliza.lending.wf_implementation.services.TermLifeService;

@RestController
@RequestMapping("/term-life")
public class TermLifeController {

	@Autowired
	private TermLifeService termLifeService;

	private static final Logger logger = LoggerFactory.getLogger(TermLifeController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/quick-quote/{slug}")
	public ResponseEntity<Object> getDataFromQuickQuoteMaster(Principal principal,
			@RequestBody ProductRequest productRequest, @PathVariable(value = "slug") String slug) {
		logger.info("<=============== TermLifeController.getDataFromQuickQuoteMaster ===============> ");
		try {
			return CommonHelperFunctions.buildResponseEntity(
					termLifeService.getDataFromQuickQuote(principal.getName(), productRequest, slug));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/quote-details")
	public ResponseEntity<Object> getDataFromQuoteDetailsMaster(Principal principal,
			@RequestBody @Valid TlPremiumInput tlPremiumInput) {
		logger.info("<=============== TermLifeController.getDataFromQuoteDetailsMaster ===============> ");
		try {
			return CommonHelperFunctions.buildResponseEntity(
					termLifeService.quoteDetailsMasterModification(principal.getName(), tlPremiumInput));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@RequestMapping(method = RequestMethod.POST,value="/update-premium")
	public ResponseEntity<Object> updateTLPremium(Principal principal,
			@RequestBody TlPremiumInput productRequest) throws Exception {
		return CommonHelperFunctions
				.buildResponseEntity(termLifeService.updateTLPremium(principal.getName(), productRequest));
	}
}
