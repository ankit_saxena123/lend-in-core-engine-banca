package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditBancaCIFCustomerSearchListIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaCIFCustomerSearchListIntegration.class);

	public JSONObject buildRequestPayload(String nationalId, String phoneNumber) throws JSONException {
		logger.info("-->Entering FECreditBancaCIFCustomerSearchListIntegration.buildRequestPayload()");
		JSONObject requestPayload = new JSONObject();
		JSONObject bodyRequestData = new JSONObject();
		bodyRequestData.put("cif:GetCustomerList.Sys.TransID", Utils.generateUUIDTransanctionId());
		bodyRequestData.put("cif:GetCustomerList.Sys.RequestorID", Constants.FEC_REQUESTOR_ID);
		bodyRequestData.put("cif:GetCustomerList.Sys.DateTime", Utils.getCurrentDateTime());
		bodyRequestData.put("cif:GetCustomerList.Parameters.NationalID", nationalId);
		bodyRequestData.put("cif:GetCustomerList.Parameters.PhoneNumber", phoneNumber);
		requestPayload.put("body", bodyRequestData);
		logger.info("FECreditBanca CIF Customer List Basic Request Payload : " + requestPayload.toString());
		logger.info("<-- Exiting FECreditBancaCIFCustomerSearchListIntegration.buildRequestPayload()");
		return requestPayload;
	}

	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put("Content-Type", Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.CIF_CUSTOMER_SEARCH_LIST_INTEGRATION_SLUG_KEY + "/"
				+ Constants.CIF_CUSTOMER_GET_CUSTOMER_LIST + "/" + wfImplConfig.getIbCompany() + "/"
				+ Constants.IB_HASH_VALUE +"/?env="+wfImplConfig.getIbEnv()+ Constants.CIF_CUSTOMER_SEARCH_LIST_SOAP_KEY;
		logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		logger.info("resposeAsJson>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + resposeAsJson.getBody());
		return resposeAsJson;
	}

	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		logger.info("-->Entering FECreditBancaCIFCustomerSearchListIntegration.parseResponse()");
		Map<String, Object> cifSearchListResponseMap = new HashMap<>();
		String apiResponse = null;
		try {
			logger.debug("Success Response for FEC CIF CustomerSearch");
			apiResponse = responseFromIntegrationBroker.getString("monoResponse");
			logger.info("apiResponse>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + apiResponse);
			JSONObject apiResponseAsJson = new JSONObject(apiResponse);
			JSONObject getCustomerListResponseObject = apiResponseAsJson.getJSONObject("NS1:GetCustomerListResponse");
			JSONObject sysResponse = getCustomerListResponseObject.getJSONObject("Sys");
			if (sysResponse.getString("Description").equals("DATA FOUND")) {
				cifSearchListResponseMap = new ObjectMapper().readValue(apiResponse.toString(),
						new TypeReference<Map<String, Object>>() {
						});
				cifSearchListResponseMap.put(Constants.STATUS_MAP_KEY, true);
			} else {
				logger.debug("Data Not Found In Description for FEC CIF CustomerSearch");
				cifSearchListResponseMap.put(Constants.STATUS_MAP_KEY, false);
				cifSearchListResponseMap.put(Constants.DESCRIPTION_MAP_KEY, sysResponse.getString("Description"));
			}
		} catch (Exception e) {
			cifSearchListResponseMap.put(Constants.STATUS_MAP_KEY, false);
			cifSearchListResponseMap.put(Constants.DESCRIPTION_MAP_KEY, e.getMessage());
		}
		logger.info("<-- Exiting FECreditBancaCIFCustomerSearchListIntegration.parseResponse()");
		return cifSearchListResponseMap;
	}
}