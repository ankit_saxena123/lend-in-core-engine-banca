package com.kuliza.lending.wf_implementation.pojo;

public class TlPremiumInput {

	private String language;
	private Boolean isAdditionalKey;
	private String processInstanceId;
	private String premiumAmount;

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Boolean getIsAdditionalKey() {
		return isAdditionalKey;
	}

	public void setIsAdditionalKey(Boolean isAdditionalKey) {
		this.isAdditionalKey = isAdditionalKey;
	}
	
	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
}
