package com.kuliza.lending.wf_implementation.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.util.Enumeration;
import java.util.List;

import javax.xml.bind.DatatypeConverter;
import javax.xml.namespace.QName;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.pojo.NomineeDetails;
import com.kuliza.lending.wf_implementation.pojo.TLPolicyDetails;

import vn.com.fis.ws.Services_Service;

@Component
public class FPTUtils {
	static Logger logger = LoggerFactory.getLogger(FPTUtils.class);

	@Autowired
	private WfImplConfig wfImplConfig;

	public static String getPKCS1Signature(String data, String relyingPartyKeyStore,
			String relyingPartyKeyStorePassword) throws Exception {

		Security.addProvider(new BouncyCastleProvider());
		KeyStore keystore = KeyStore.getInstance("PKCS12");
		InputStream is = new FileInputStream(relyingPartyKeyStore);
		keystore.load(is, relyingPartyKeyStorePassword.toCharArray());

		Enumeration<String> e = keystore.aliases();
		String aliasName = "";
		while (e.hasMoreElements()) {
			aliasName = e.nextElement();
		}
		PrivateKey key = (PrivateKey) keystore.getKey(aliasName, relyingPartyKeyStorePassword.toCharArray());

		Signature sig = Signature.getInstance("SHA1withRSA");
		sig.initSign(key);
		sig.update(data.getBytes());
		return DatatypeConverter.printBase64Binary(sig.sign());
	}

	public String getData2SignIn(String timestamp) {
		String data2sign = wfImplConfig.getFptRelyingPartyUserName() + wfImplConfig.getFptRelyingPartyPassword()
				+ wfImplConfig.getFptRelyingPartySignature() + timestamp;
		return data2sign;
	}

	public static File generateFilePath(String fptFileDirectory, String url) {

		String fullFile = fptFileDirectory + "FPT-" + System.nanoTime() + Constants.E_SIGN_FILE_EXTENSION;

		logger.info("fullFile ---->: " + fullFile);
//		File file = new File(fullFile);
		File file = new File("/home/aravind.selvam/work/workspace2/ok1.pdf");


		try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
				FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			byte dataBuffer[] = new byte[1024];
			int bytesRead;
			while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
				fileOutputStream.write(dataBuffer, 0, bytesRead);
			}
			logger.info("Files generated for fpt : " + file);
		} catch (Exception e) {
			logger.error("Exception while genrate file, from url : " + url + e);
		}

		/*
		 * try {
		 * 
		 * byte[] data = Base64.getDecoder().decode(base64); file = new
		 * File(Constants.FILE_DIRECTORY + base64.substring(0, 4) + "-" +
		 * System.nanoTime() + Constants.FILE_EXTENSION); OutputStream stream = new
		 * FileOutputStream(file); stream.write(data);
		 * logger.info("File genrated from base64 saved at : " + file); } catch
		 * (Exception e) { logger.error("Exception : " + e); }
		 */
		return file;
	}

	public Services_Service createFPTService() throws MalformedURLException {

		String fptESDLUrl = wfImplConfig.getFptServiceUrl();

		logger.info("SOAP END POINT FPT : " + fptESDLUrl);
		Services_Service services = new Services_Service(new URL(fptESDLUrl),
				new QName("http://api.esigncloud.mobileid.vn/", "Services"));
		logger.info("FPT serive created");
		return services;
	}

	public String getPKCS1Signature(String timestamp) throws Exception {

		String data2sign = getData2SignIn(timestamp);

		String pkcs1Signature = getPKCS1SignatureByDataSign(data2sign);

		return pkcs1Signature;
	}

	private String getPKCS1SignatureByDataSign(String data2sign) throws Exception {

		Security.addProvider(new BouncyCastleProvider());
		KeyStore keystore = KeyStore.getInstance("PKCS12");

		String relyingPartyKeyStore = wfImplConfig.getFptRelyingPartyKeyPath();
		logger.info("relyingPartyKeyStore : "+relyingPartyKeyStore);

		String relyingPartyKeyStorePassword = wfImplConfig.getFptRelyingPartyKeyStorePassword();
				

		InputStream is = new FileInputStream(relyingPartyKeyStore);
		keystore.load(is, relyingPartyKeyStorePassword.toCharArray());

		Enumeration<String> e = keystore.aliases();
		String aliasName = "";
		while (e.hasMoreElements()) {
			aliasName = e.nextElement();
		}
		PrivateKey key = (PrivateKey) keystore.getKey(aliasName, relyingPartyKeyStorePassword.toCharArray());

		Signature sig = Signature.getInstance("SHA1withRSA");
		sig.initSign(key);
		sig.update(data2sign.getBytes());
		return DatatypeConverter.printBase64Binary(sig.sign());

	}
	
	
	private static final String CONTRACT_NUMBER = "{{contract_number}}";
	
	private static final String FULL_NAME = "{{full_name}}";
	private static final String GENDER = "{{gender}}";
	private static final String DOB = "{{dob}}";
	private static final String NID = "{{nid}}";
	private static final String AGE = "{{age}}";

	private static final String ADDRESS = "{{address}}";
	private static final String PHONE_NUMBER = "{{phone_number}}";
	private static final String EMAIL = "{{email}}";
	private static final String PROTECTION_DURATION = "{{protection_duration}}";
	private static final String PAYMENT_OPTION = "{{payment_option}}";
	
	
	
	private static final String ILLUSTRATION_NUMBER = "{{illustration_number}}";
	
	
	private static final String TL_PROTECTION_AMOUNT = "{{tl_protection_amount}}";
	
	private static final String WHAT_YOU_PAY = "{{what_you_pay}}";
	
	
	private static final String TL_PREMIUM = "{{tl_premium}}";
	private static final String PERMANENT_DISABILITY_PROTECTION_AMOUNT = "{{permanent_disability_due_to_accident_protection_amount}}";
	private static final String PERMANENT_DISABILITY_PREMIUM_AMOUNT = "{{permanent_disability_due_to_accident_protection_premium}}";
	
	private static final String CRITICAL_ILLNESS_PROTECTION_AMOUNT = "{{critical_illness_protection_amount}}";
	private static final String CRITICAL_ILLNESS_PREMIUM = "{{critical_illness_premium}}";
	
	private static final String TOTAL_AMOUNT = "{{total_amount}}";
	
	
	private static final String NOMINATED_ROWS = "{{nominated_rows}}";
	
	private static final String NOMINATED_NAME = "{{nominated_name}}";
	private static final String NOMINATED_DOB = "{{nominated_dob}}";
	private static final String NOMINATED_NID = "{{nominated_nid}}";
	private static final String NOMINATED_GENDER = "{{nominated_gender}}";
	private static final String NOMINATED_RELATION = "{{nominated_relation}}";

	private static final String NOMINATED_SHARE = "{{nominated_share}}";

	
	private static final String POLICYHOLDER_NAME = "{{policyholder_name}}";
	private static final String AGENT_CODE = "{{agent_code}}";
	
	private static final String POLICY_SELLER_NAME = "{{policy_seller_name}}";
	
	private static final String POLICY_NUMBER = "{{policy_number}}";
	private static final String OCCUPATION = "{{occupation}}";
	
	private static final String PURCHASED_DATE = "{{purchased_date}}";
	private static final String PLAN_EXPIRING_DATE = "{{plan_expiring_date}}";
	
private static String setNomiantedDetailsList(String fileContent, List<NomineeDetails> nomineeDetailsList) {
	
	String nomineeRowList = "";
	
	nomineeRowList = buildNomineeRows(nomineeDetailsList);
	
	logger.info("nomineeRowList ============>"+nomineeRowList);
	
	fileContent = replace(fileContent,NOMINATED_ROWS, nomineeRowList);
	return fileContent;
	
//	for (NomineeDetails nomiantedDetails : nomineeDetailsList) {
//		fileContent = replace(fileContent,NOMINATED_NAME, nomiantedDetails.getName());
//		fileContent = replace(fileContent,NOMINATED_DOB, nomiantedDetails.getDob());
//		fileContent = replace(fileContent,NOMINATED_NID, nomiantedDetails.getNid());
//		fileContent = replace(fileContent,NOMINATED_GENDER, nomiantedDetails.getGender());
//		fileContent = replace(fileContent,NOMINATED_RELATION, nomiantedDetails.getRelation());
//		fileContent = replace(fileContent,NOMINATED_SHARE, nomiantedDetails.getShare());
//	}
//		return fileContent;
	}

public static String buildNomineeRows(List<NomineeDetails> nomineeDetailsList) {
	
	
	if(nomineeDetailsList == null || nomineeDetailsList.isEmpty()) {
		
		logger.info("nominee details are empty");
		String emptyNomineeRow  = Constants.NOMINEE_ROW;
		
		emptyNomineeRow = replace(emptyNomineeRow,NOMINATED_NAME, "");
		
		emptyNomineeRow = replace(emptyNomineeRow,NOMINATED_DOB, "");
		emptyNomineeRow = replace(emptyNomineeRow,NOMINATED_NID, "");
		emptyNomineeRow = replace(emptyNomineeRow,NOMINATED_GENDER, "");
		emptyNomineeRow = replace(emptyNomineeRow,NOMINATED_RELATION, "");
		emptyNomineeRow = replace(emptyNomineeRow,NOMINATED_SHARE, "");
		
		return emptyNomineeRow;
	}
	
	logger.info("nominee details are found");
	
	String nomineeRowList = "";
	for (NomineeDetails nomiantedDetails : nomineeDetailsList) {
		
		String nomineeRow  = Constants.NOMINEE_ROW;
		nomineeRow = replace(nomineeRow,NOMINATED_NAME, nomiantedDetails.getName());
		
		nomineeRow = replace(nomineeRow,NOMINATED_DOB, nomiantedDetails.getDob());
		nomineeRow = replace(nomineeRow,NOMINATED_NID, nomiantedDetails.getNid());
		nomineeRow = replace(nomineeRow,NOMINATED_GENDER, nomiantedDetails.getGender());
		nomineeRow = replace(nomineeRow,NOMINATED_RELATION, nomiantedDetails.getRelation());
		nomineeRow = replace(nomineeRow,NOMINATED_SHARE, nomiantedDetails.getShare());
		
		nomineeRowList = nomineeRowList + nomineeRow;
	}
	return nomineeRowList;
}
	
	
	
	public static String replacePolicyDetails(String fileContent,TLPolicyDetails tlPolicyDetails) {
		
		fileContent = setNomiantedDetailsList(fileContent,tlPolicyDetails.getNomiantedDetailsList());
		fileContent = replace(fileContent,PURCHASED_DATE, tlPolicyDetails.getPurchasedDate());
		fileContent = replace(fileContent,PLAN_EXPIRING_DATE, tlPolicyDetails.getPlanExpiringDate());
		
		fileContent = replace(fileContent,ILLUSTRATION_NUMBER, tlPolicyDetails.getIllustrationNumber());
		
		
		fileContent = replace(fileContent,CONTRACT_NUMBER, tlPolicyDetails.getContractNumber());

		fileContent = replace(fileContent,FULL_NAME, tlPolicyDetails.getPolicyHolderName());
		fileContent = replace(fileContent,POLICYHOLDER_NAME, tlPolicyDetails.getPolicyHolderName());
		
		fileContent = replace(fileContent,GENDER, tlPolicyDetails.getPolicyHolderGender());
		fileContent = replace(fileContent,DOB, tlPolicyDetails.getPolicyHolderDOB());
		fileContent = replace(fileContent,NID, tlPolicyDetails.getPolicyHolderNID());
		fileContent = replace(fileContent,ADDRESS, tlPolicyDetails.getPolicyHolderAddress());
		fileContent = replace(fileContent,PHONE_NUMBER, tlPolicyDetails.getPolicyHolderPhone());
		fileContent = replace(fileContent,EMAIL, tlPolicyDetails.getPolicyHolderEmail());
		fileContent = replace(fileContent,AGE, tlPolicyDetails.getPolicyHolderAge());
		
		fileContent = replace(fileContent,PROTECTION_DURATION, tlPolicyDetails.getContractNumber());
		fileContent = replace(fileContent,PAYMENT_OPTION, tlPolicyDetails.getContractNumber());
		
		fileContent = replace(fileContent,TL_PROTECTION_AMOUNT, tlPolicyDetails.getTlProductionAmount());
		fileContent = replace(fileContent,TL_PREMIUM, tlPolicyDetails.getTlPremiumAmount());
		
		fileContent = replace(fileContent,WHAT_YOU_PAY, tlPolicyDetails.getWhatYouPayAmount());
		
		fileContent = replace(fileContent,PERMANENT_DISABILITY_PROTECTION_AMOUNT, tlPolicyDetails.getPermanentDisabilityProductionAmount());
		fileContent = replace(fileContent,PERMANENT_DISABILITY_PREMIUM_AMOUNT, tlPolicyDetails.getPermanentDisabilityPremiumAmount());

		fileContent = replace(fileContent,CRITICAL_ILLNESS_PROTECTION_AMOUNT, tlPolicyDetails.getCriticalIllnessProductionAmount());
		fileContent = replace(fileContent,CRITICAL_ILLNESS_PREMIUM, tlPolicyDetails.getCriticalIllnessPremiumAmount());
		
		fileContent = replace(fileContent,CRITICAL_ILLNESS_PREMIUM, tlPolicyDetails.getCriticalIllnessPremiumAmount());
		
		fileContent = replace(fileContent,AGENT_CODE, tlPolicyDetails.getAgentCode());
		fileContent = replace(fileContent,POLICY_SELLER_NAME, tlPolicyDetails.getPolicySellerName());
		
		fileContent = replace(fileContent,TOTAL_AMOUNT, tlPolicyDetails.getTotalAmount());
		fileContent = replace(fileContent,OCCUPATION, tlPolicyDetails.getOccupation());

		fileContent = replace(fileContent,POLICY_NUMBER, tlPolicyDetails.getPolicyNumber());

		return fileContent;
	}

	private static String replace(String fileContent, String key, String value) {
		if(value != null) {
			fileContent = fileContent.replace(key, value);
		}else {
			
			String emptyValue = "";
			if(key.equals(EMAIL)) {
				emptyValue = "-";
			}
			
			fileContent = fileContent.replace(key, emptyValue);
		}
		return fileContent;
	}

}
