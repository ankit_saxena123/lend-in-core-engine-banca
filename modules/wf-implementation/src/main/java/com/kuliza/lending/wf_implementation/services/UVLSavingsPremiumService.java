package com.kuliza.lending.wf_implementation.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.pojo.UVLSavingsPremiumPojo;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("uvlSavingsPremiumService")
public class UVLSavingsPremiumService {

	private static final Logger logger = LoggerFactory.getLogger(UVLSavingsPremiumService.class);
	@Autowired
	private MasterApiIntegration master;

	public ApiResponse uvlPremium(UVLSavingsPremiumPojo details) throws JSONException {
		logger.info("request body instde service" + details);
		Map<String, String> uvlDetails = new HashMap<>();
		uvlDetails.put(WfImplConstants.HYPERVERGE_DOB, details.getDob());
		uvlDetails.put(Constants.BANCA_GENDER_KEY, details.getGender());
		uvlDetails.put(Constants.INVSTMENT_AMOUNT, details.getInvestment_amount());
		uvlDetails.put(Constants.PROTECTION_TERM, details.getProtection_term());
		uvlDetails.put("paymentOptions", details.getPaymentOption());
		try {

			if (uvlDetails.containsKey(WfImplConstants.DOB_HYPERVERGE)) {
				String calculatedAge = Utils
						.getStringValue(Utils.getAgeFromDOB((uvlDetails.get(WfImplConstants.DOB_HYPERVERGE))));
				uvlDetails.put(Constants.AGE, calculatedAge);

			} else {
				String calculatedAge = Utils
						.getStringValue(Utils.getAgeFromDOB((uvlDetails.get(WfImplConstants.HYPERVERGE_DOB))));
				uvlDetails.put(Constants.AGE, calculatedAge);
			}
			Map<String, String> data = getDataFromMaster(uvlDetails);
			logger.info("EXITING uvlPremium ******* ");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
		} catch (ParseException e) {
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}

	}

	public Map<String, String> getDataFromMaster(Map<String, String> uvlDetails) throws JSONException {
		logger.info("inside getDataFromMaster******* " + uvlDetails.get(Constants.AGE));
		JSONArray responseArray = new JSONArray();
		Map<String, String> premiumValue = new HashMap<String, String>();
		try {
			JSONObject premiumMasterResponse;
			premiumMasterResponse = master.getDataFromService(WfImplConstants.UVL_SAVINGS_PREMIUM);
			if (premiumMasterResponse != null
					&& premiumMasterResponse.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				responseArray = master.parseResponseFromMaster(premiumMasterResponse);
			}
			String investmentAmount = protectionAmount(uvlDetails.get(Constants.INVSTMENT_AMOUNT),
					uvlDetails.get("paymentOptions"));
			premiumValue.put(Constants.INVSTMENT_AMOUNT, investmentAmount);
			for (int i = 0; i < responseArray.length(); i++) {

				JSONObject premium = responseArray.getJSONObject(i);
				if (premium.get(Constants.AGE).equals(uvlDetails.get(Constants.AGE))
						&& premium.getString(Constants.BANCA_GENDER_KEY)
								.equals(uvlDetails.get(Constants.BANCA_GENDER_KEY))
						&& premium.getString(Constants.INVSTMENT_AMOUNT)
								.equals(uvlDetails.get(Constants.INVSTMENT_AMOUNT))
						&& premium.getString(Constants.PROTECTION_TERM)
								.equals(uvlDetails.get(Constants.PROTECTION_TERM))) {
					premiumValue.put(Constants.LOYALTI_BONUS, premium.getString(Constants.LOYALTI_BONUS));
					premiumValue.put(Constants.TOTAL_PERMANENT, premium.getString(Constants.TOTAL_PERMANENT));
					premiumValue.put(Constants.TERMINAL_ILLNESS, premium.getString(Constants.TERMINAL_ILLNESS));
					premiumValue.put(Constants.DEATH_BENIFIT, premium.getString(Constants.DEATH_BENIFIT));
					premiumValue.put(Constants.FINAL_INVESTMENT_VALUE,
							premium.getString(Constants.FINAL_INVESTMENT_VALUE));
				}

			}

			logger.info("exiting getDataFromMaster****** mapp** " + premiumValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return premiumValue;
	}

	public String protectionAmount(String investmentAmount, String paymentOption) {
		Integer invAmount = Utils.getIntegerValue(investmentAmount);
		try {
			if (paymentOption.equals(Constants.SEMI_ANNUALLY)) {
				invAmount = invAmount / 2;
			} else if (paymentOption.equals(Constants.QUARTERLY)) {
				invAmount = invAmount / 4;
			} else if (paymentOption.equals(Constants.MONTHLY)) {
				invAmount = invAmount / 12;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return Utils.getStringValue(invAmount);

	}

	public ApiResponse uvlMaster(UVLSavingsPremiumPojo details) {
		logger.info("<--- Entering uvlMaster --->");
		JSONArray responseArray = new JSONArray();
		List<Object> arrayList = new ArrayList<>();
		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();
		try {
			JSONObject premiumMasterResponse;
			premiumMasterResponse = master.getDataFromService(Constants.BANCA_GET_UVL_QUOTE_DETAILS);
			if (premiumMasterResponse != null
					&& premiumMasterResponse.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				responseArray = master.parseResponseFromMaster(premiumMasterResponse);
			}
			List<Object> finalArrayList = replaceValueInMaster(responseArray,arrayList,details);
			finalMap = new ObjectMapper().readValue(finalArrayList.toString(),
						new TypeReference<List<Map<String, Object>>>() {
						});
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());

		}
		logger.info("<--- Existing uvlMaster --->");
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, finalMap);
	}

	private List<Object> replaceValueInMaster(JSONArray responseArray, List<Object> arrayList,
			UVLSavingsPremiumPojo details) throws Exception {
		logger.info(" <--- Entering replaceValueInMaster ---> ");
		String investmentAmount = details.getInvestment_amount();
		String finalInvestmentWithLoyaltyBonus = details.getFinal_investment_value();
		String deathBenefit = details.getDeath_benefit();
		String totalPermanentDisability = details.getTotal_permanant();
		String terminalIllness = details.getTerminal_illness();
		
		String planShortValues = null;
		String middleSectionListingValues = null;
		for (int i = 0; i < responseArray.length(); i++) {
			JSONObject arrayObject = responseArray.getJSONObject(i);
			if (arrayObject.get(Constants.FH_LANGUAGE).equals(details.getLanguage())) {
				planShortValues = arrayObject.getString(Constants.TOP_SECTION_VALUES);
				planShortValues = planShortValues.replace(Constants.CONSTANT_X, investmentAmount);

				middleSectionListingValues = arrayObject.getString(Constants.MIDDLE_SECTION_LISTING_DESC);
				middleSectionListingValues = middleSectionListingValues
						.replace(Constants.FI, finalInvestmentWithLoyaltyBonus)
						.replace(Constants.DB, deathBenefit)
						.replace(Constants.TP, totalPermanentDisability)
						.replace(Constants.TI, terminalIllness);
				arrayObject.put(Constants.TOP_SECTION_VALUES, planShortValues);
				arrayObject.put(Constants.MIDDLE_SECTION_LISTING_DESC, middleSectionListingValues);
				arrayList.add(arrayObject);
			}
			logger.info(" <--- Existing replaceValueInMaster ---> ");
		}
		return arrayList;
	}

}
