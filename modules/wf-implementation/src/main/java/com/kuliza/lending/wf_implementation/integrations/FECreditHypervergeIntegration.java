package com.kuliza.lending.wf_implementation.integrations;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class FECreditHypervergeIntegration {
	
	@Autowired
	private WfImplConfig wfImplConfig;
	
	private static final Logger logger = LoggerFactory
			.getLogger(FECreditHypervergeIntegration.class);

	public CloseableHttpResponse getDataFromService(File file)
			throws ClientProtocolException, IOException {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(
				wfImplConfig.getHypervergeUrl());
		httpPost.setHeader(WfImplConstants.HYPERVERGE_APPID, wfImplConfig.getHypervergeAppId());
		httpPost.setHeader(WfImplConstants.HYPERVERGE_KEY,
				wfImplConfig.getHypervergeKey());
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder = MultipartEntityBuilder.create();
		builder.addPart(WfImplConstants.HYPERVERGE_REQUEST_IMAGE, new FileBody(file));
		HttpEntity multipart = builder.build();
		httpPost.setEntity(multipart);
		CloseableHttpResponse closeableHttpResponse = httpClient
				.execute(httpPost);
		if (file.exists()) {
			file.delete();
		}
		return closeableHttpResponse;

	}

	public Map<String, Object> parseNationalIdFrontData(
			String frontResponseFromHyperverge) {
		Map<String, Object> processVariables = new HashMap<>();
		if (frontResponseFromHyperverge == null
				|| frontResponseFromHyperverge.equalsIgnoreCase("null"))
			return processVariables;
		try {
			JSONObject data = new JSONObject(frontResponseFromHyperverge);
			if (data.has(WfImplConstants.HYPERVERGE_RESULT)
					&& data.get(WfImplConstants.HYPERVERGE_RESULT) instanceof JSONArray) {
				JSONArray result = data.getJSONArray(WfImplConstants.HYPERVERGE_RESULT);
				if (result.length() > 0
						&& result.get(result.length() - 1) instanceof JSONObject) {
					JSONObject result0 = result
							.getJSONObject(result.length() - 1);
					if (result0.has(WfImplConstants.HYPERVERGE_DETAILS)
							&& result0.get(WfImplConstants.HYPERVERGE_DETAILS) instanceof JSONObject) {
						JSONObject details = result0
								.getJSONObject(WfImplConstants.HYPERVERGE_DETAILS);
						processVariables
								.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
										true);
						if (details.has(WfImplConstants.HYPERVERGE_DOB)
								&& details.get(WfImplConstants.HYPERVERGE_DOB) instanceof JSONObject) {
							JSONObject dob = details
									.getJSONObject(WfImplConstants.HYPERVERGE_DOB);
							processVariables
									.put(WfImplConstants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(dob
													.getString(WfImplConstants.HYPERVERGE_VALUE)));
						} else {
							processVariables
									.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
											false);
						}

						if (details.has(WfImplConstants.HYPERVERGE_ID)
								&& details.get(WfImplConstants.HYPERVERGE_ID) instanceof JSONObject) {
							JSONObject id = details
									.getJSONObject(WfImplConstants.HYPERVERGE_ID);
							processVariables
									.put(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(id
													.getString(WfImplConstants.HYPERVERGE_VALUE)));
						} else {
							processVariables
									.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
											false);
						}
						if (details.has(WfImplConstants.HYPERVERGE_NAME)
								&& details.get(WfImplConstants.HYPERVERGE_NAME) instanceof JSONObject) {
							JSONObject name = details
									.getJSONObject(WfImplConstants.HYPERVERGE_NAME);
							processVariables
									.put(WfImplConstants.BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(name
													.getString(WfImplConstants.HYPERVERGE_VALUE)));
							processVariables
									.put(WfImplConstants.NAME_CONFIDENCE_SCORE_FROM_HYPERVERGE_KEY,
											name.getInt(WfImplConstants.HYPERVERGE_CONF));
							processVariables
									.put(WfImplConstants.NAME_TO_BE_REVIEWED_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(name
													.getString(WfImplConstants.HYPERVERGE_TO_BE_REVIEWED)));
						} else {
							processVariables
									.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
											false);
						}
						if (details.has(WfImplConstants.HYPERVERGE_PROVINCE)
								&& details.get(WfImplConstants.HYPERVERGE_PROVINCE) instanceof JSONObject) {
							JSONObject province = details
									.getJSONObject(WfImplConstants.HYPERVERGE_PROVINCE);
							processVariables
									.put(WfImplConstants.PROVINCE_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(province
													.getString(WfImplConstants.HYPERVERGE_VALUE)));
						}
						if (details.has(WfImplConstants.HYPERVERGE_ADDRESS)
								&& details.get(WfImplConstants.HYPERVERGE_ADDRESS) instanceof JSONObject) {
							JSONObject address = details
									.getJSONObject(WfImplConstants.HYPERVERGE_ADDRESS);
							processVariables
									.put(WfImplConstants.ADDRESS_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(address
													.getString(WfImplConstants.HYPERVERGE_VALUE)));
						}
						if (details.has(WfImplConstants.HYPERVERGE_GENDER)
								&& details.get(WfImplConstants.HYPERVERGE_GENDER) instanceof JSONObject) {
							JSONObject gender = details
									.getJSONObject(WfImplConstants.HYPERVERGE_GENDER);
							processVariables.put(
									WfImplConstants.GENDER_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(gender
											.getString(WfImplConstants.HYPERVERGE_VALUE)));
						}
					} else {
						processVariables
								.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
										false);
					}
				} else {
					processVariables
							.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
									false);
				}
			}
		} catch (Exception e) {
			logger.error("failed to hypervergeFrontData  " + e.getMessage(), e);
			processVariables.put(
					WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
					false);
		}
		return processVariables;
	}

	public Map<String, Object> parseNationalIdBackData(
			String backResponseFromHyperverge) {
		Calendar cal = Calendar.getInstance();
		Map<String, Object> processVariables = new HashMap<>();
		if (backResponseFromHyperverge == null
				|| backResponseFromHyperverge.equalsIgnoreCase("null"))
			return processVariables;
		try {
			JSONObject data = new JSONObject(backResponseFromHyperverge);
			if (data.has(WfImplConstants.HYPERVERGE_RESULT)
					&& data.get(WfImplConstants.HYPERVERGE_RESULT) instanceof JSONArray) {
				JSONArray result = data.getJSONArray(WfImplConstants.HYPERVERGE_RESULT);
				if (result.length() > 0
						&& result.get(result.length() - 1) instanceof JSONObject) {
					JSONObject result0 = result
							.getJSONObject(result.length() - 1);
					if (result0.has(WfImplConstants.HYPERVERGE_DETAILS)
							&& result0.get(WfImplConstants.HYPERVERGE_DETAILS) instanceof JSONObject) {
						JSONObject details = result0
								.getJSONObject(WfImplConstants.HYPERVERGE_DETAILS);
						processVariables
								.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG,
										true);
						if (details.has(WfImplConstants.HYPERVERGE_PROVINCE)
								&& details.get(WfImplConstants.HYPERVERGE_PROVINCE) instanceof JSONObject) {
							JSONObject province = details
									.getJSONObject(WfImplConstants.HYPERVERGE_PROVINCE);
							processVariables
									.put(WfImplConstants.PLACE_OF_ISSUE_OF_NATIONAL_ID_FROM_HYPERVERGE_KEY,
											Utils.getStringValue(province
													.getString(WfImplConstants.HYPERVERGE_VALUE)));
						}
					} else {
						processVariables
								.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG,
										false);
					}
				} else {
					processVariables
							.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG,
									false);
				}
			} else {
				processVariables
						.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG,
								false);
			}
		} catch (Exception e) {
			logger.error("failed to hypervergeBackData  " + e.getMessage(), e);
			processVariables.put(
					WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG,
					false);
		}
		return processVariables;
	}

	public int checkFrontBack(String hypervergeNationalIdResponse) {
		if (hypervergeNationalIdResponse == null
				|| hypervergeNationalIdResponse.equalsIgnoreCase("null"))
			return -1;
		try {
			JSONObject data = new JSONObject(hypervergeNationalIdResponse);
			if (data.has(WfImplConstants.HYPERVERGE_STATUS)
					&& data.has(WfImplConstants.HYEPRVERGE_STATUS_CODE)
					&& data.getString(WfImplConstants.HYPERVERGE_STATUS).equals(
							WfImplConstants.HYPERVERGE_SUCCESS)
					&& data.getString(WfImplConstants.HYEPRVERGE_STATUS_CODE).equals("200")) {
				if (data.has(WfImplConstants.HYPERVERGE_RESULT)
						&& data.get(WfImplConstants.HYPERVERGE_RESULT) instanceof JSONArray) {
					JSONArray result = data.getJSONArray(WfImplConstants.HYPERVERGE_RESULT);
					if (result.length() > 0
							&& result.get(result.length() - 1) instanceof JSONObject) {
						JSONObject result0 = result.getJSONObject(result
								.length() - 1);
						if (result0.has(WfImplConstants.HYPERVERGE_TYPE)
								&& result0.get(WfImplConstants.HYPERVERGE_TYPE) instanceof String) {
							if (result0.getString(WfImplConstants.HYPERVERGE_TYPE).equals(
									WfImplConstants.HYPERVERGE_ID_FRONT)
									|| result0.getString(WfImplConstants.HYPERVERGE_TYPE)
											.equals(WfImplConstants.HYPERVERGE_ID_NEW_FRONT)) {
								return 1;
							} else if (result0.getString(WfImplConstants.HYPERVERGE_TYPE)
									.equals(WfImplConstants.HYPERVERGE_ID_BACK)
									|| result0.getString(WfImplConstants.HYPERVERGE_TYPE)
											.equals(WfImplConstants.HYPERVERGE_ID_NEW_BACK)) {
								return 0;
							}
						}
					}
				}
			}
			return -1;
		} catch (Exception e) {
			logger.error("failed to checkFrontBack  " + e.getMessage(), e);
			return -1;
		}
	}

	/**
	 * gets the gender from hyperverge
	 * 
	 * @param gender
	 * @return String
	 */
	public String getGender(String gender) {
		gender = Utils.getStringValue(gender);

		if (gender.equalsIgnoreCase("m")) {
			gender = Constants.NID_MALE;
		} else if (gender.equalsIgnoreCase("f")) {
			gender = Constants.NID_FEMALE;
		}
		logger.info("gender from hyperverge :" + gender);
		return gender;

	}

}
