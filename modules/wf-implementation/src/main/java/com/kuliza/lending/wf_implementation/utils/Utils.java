package com.kuliza.lending.wf_implementation.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.models.PremiumCalculateRequest;
import com.kuliza.lending.wf_implementation.pojo.PlanPremiums;
import com.kuliza.lending.wf_implementation.pojo.TermLifeAgePremiumRates;

public class Utils {

	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	private Utils() {
		throw new UnsupportedOperationException();
	}

	public static String getStringValue(Object s) {
		return s != null ? s.toString() : "";
	}

	public static String getStringValueOrEmptyArrayString(Object s) {
		return s != null ? s.toString() : "[]";
	}

	public static String getStringValueOrEmptyMapString(Object s) {
		return s != null ? s.toString() : "{}";
	}

	public static String getStringValueOrDefault(Object s, String defaultString) {
		return s != null ? s.toString() : defaultString;
	}

	public static Long getLongValue(Object s) {
		try {
			return s != null ? (long) Double.parseDouble(s.toString()) : new Long(0);
		} catch (NumberFormatException e) {
			return new Long(0);
		}
	}

	public static Integer getIntegerValue(Object s) {
		try {
			return s != null ? (int) Double.parseDouble(s.toString()) : 0;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public static Double getDoubleValue(Object s) {
		try {
			return s != null ? Double.parseDouble(s.toString()) : new Double(0);
		} catch (NumberFormatException e) {
			return new Double(0);
		}

	}

	public static BigDecimal getBigDecimalValue(Object s) {
		try {
			return s != null ? new BigDecimal(s.toString()) : new BigDecimal("0");
		} catch (NumberFormatException e) {
			return new BigDecimal("0");
		}
	}

	public static Boolean getBooleanValue(Object s) {
		return s != null && s.toString().toLowerCase().matches("^(true)|(false)$") ? Boolean.parseBoolean(s.toString())
				: false;
	}

	public static final String generateUUIDTransanctionId() {
		return UUID.randomUUID().toString();
	}

	public static final ZonedDateTime getCurrentDateTime() {
		return ZonedDateTime.now(ZoneOffset.UTC);
	}

	public static final String getCurrentDateInFormat(String format) {
		Date date = new Date();
		return convertToZonedDate(date, Constants.VIETNAM_TIME_ZONE, format);
//		SimpleDateFormat formatter = new SimpleDateFormat(format);
//		String strDate = formatter.format(date);
//		return strDate;
	}

	public static final XMLGregorianCalendar getCurrentDateTimeInGregorianCalendar()
			throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeInMillis(System.currentTimeMillis());

		XMLGregorianCalendar xmlGrogerianCalendar = DatatypeFactory.newInstance()
				.newXMLGregorianCalendar(gregorianCalendar);
		xmlGrogerianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		return xmlGrogerianCalendar;

	}

	public static String formatDate(String date, String initDateFormat, String endDateFormat) throws ParseException {

		Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String parsedDate = formatter.format(initDate);

		return parsedDate;
	}

	public static String getDateInString(Date date, String dateFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		String parsedDate = formatter.format(date);
		return parsedDate;
	}

	public static String convertToZonedDate(Date date, String timeZone, String dateFormat) {
		DateFormat df = new SimpleDateFormat(dateFormat);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		return df.format(date);
	}

	public static Boolean isDateInFormat(String date, String initDateFormat) {

		try {
			new SimpleDateFormat(initDateFormat).parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static Date parseDate(String date, String format) throws ParseException {
		DateFormat sourceFormat = new SimpleDateFormat(format);
		// String dateAsString = "25/12/2010";
		return sourceFormat.parse(date);
	}

	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	public static String getErrorResponseString(String api, String error) {
		String errorResponse = "";
		errorResponse.concat("Error From :" + api);
		errorResponse.concat(", Error Description: " + error);
		return errorResponse;
	}

	public static boolean isValidDate(String inDate, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}

	public static Date addDaysToDate(Date inputDate, int numberOfDays) {
		Calendar c = Calendar.getInstance();
		c.setTime(inputDate);
		c.add(Calendar.DATE, numberOfDays);
		return c.getTime();
	}

	public static Date subtractDaysToDate(Date inputDate, int numberOfDays) {
		Calendar c = Calendar.getInstance();
		c.setTime(inputDate);
		c.add(Calendar.DATE, -numberOfDays);
		return c.getTime();
	}

	public static Date addYearsToDate(Date inputDate, int numberOfYears) {
		Calendar c = Calendar.getInstance();
		c.setTime(inputDate);
		c.add(Calendar.YEAR, numberOfYears);
		return c.getTime();
	}

	public static Date addMonthsToDate(Date inputDate, int numberOfMonths) {
		Calendar c = Calendar.getInstance();
		c.setTime(inputDate);
		c.add(Calendar.MONTH, numberOfMonths);
		return c.getTime();
	}

	public static Date getDateFromEpochTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	public static int getMonthsDiffBetweenDates(Date startdate, Date endDate) {
		Calendar startdateCalendar = Calendar.getInstance();
		startdateCalendar.setTime(startdate);
		Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.setTime(endDate);
		int diffYear = endDateCalendar.get(Calendar.YEAR) - startdateCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endDateCalendar.get(Calendar.MONTH) - startdateCalendar.get(Calendar.MONTH);
		return diffMonth;
	}

	public static int getMonthsDiffBetweenDatesDDMMYYYY(String startDate, String endDate) {
		StringBuilder formattedStartDate = new StringBuilder();
		StringBuilder formattedLastDate = new StringBuilder();
		try {
			if (startDate.length() == 8) {
				formattedStartDate = formattedStartDate.append(
						startDate.substring(4, 8) + "-" + startDate.substring(2, 4) + "-" + startDate.substring(0, 2));
			} else {
				formattedStartDate = formattedStartDate.append(startDate);
			}
			if (endDate.length() == 8) {
				formattedLastDate = formattedLastDate.append(
						endDate.substring(4, 8) + "-" + endDate.substring(2, 4) + "-" + endDate.substring(0, 2));
			} else {
				formattedLastDate = formattedLastDate.append(endDate);
			}
			int monthsDiff = Period.between(LocalDate.parse(formattedStartDate), LocalDate.parse(formattedLastDate))
					.getMonths();
			return monthsDiff;
		} catch (Exception e) {
			return 0;
		}
	}
	
	public static Date subtractDaysToDateAddYearsToDate(Date inputDate, int numberOfDays) {
		Calendar c = Calendar.getInstance();
		c.setTime(inputDate);
		c.add(Calendar.DATE, -numberOfDays);
		c.add(Calendar.YEAR, numberOfDays);
		return c.getTime();
	}

	public static String convertCharsToEnglish(String s) {
		String nfdNormalizedString = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("").replaceAll("Đ", "D").replaceAll("đ", "d");
		// return org.apache.commons.lang3.StringUtils.stripAccents(s);
	}

	public static String getDMSDocIdFromDownloadLink(String downloadLink) {
		System.out.println(downloadLink);
		String[] splittedURL = downloadLink.split("/");
		if (splittedURL.length > 0) {
			String lastIndexSplittedURL = splittedURL[splittedURL.length - 1];
			if (lastIndexSplittedURL != null && !lastIndexSplittedURL.equals("")) {
				String[] dmsDocIdLink = lastIndexSplittedURL.split("\\?");
				if (dmsDocIdLink.length > 0) {
					String dmsDocId = dmsDocIdLink[0];
					System.out.println(dmsDocId);
					if (dmsDocId != null && !dmsDocId.equals("")) {
						return dmsDocId;
					}
				}
			}
		}
		return "";
	}

	// Removes any spaces or tabs
	public static String removeSpacesFromString(String s) {
		return s.replaceAll("\\s+", "");
	}

	// Removes all the special characters, spaces, tabs etc. It accepts only
	// alphanumerics
	public static String removeSpecialCharsFromString(String s) {
		return s.replaceAll("[^a-zA-Z0-9]", "");
	}

	public static long getUserIdFromKeykloak(Object user) {
		String str = getStringValue(user);
		user = str.split("_")[1];
		return getLongValue(user);
	}

	public static String getSubStringValue(String s, int n) {
		byte[] sArray = s.getBytes();
		if (sArray.length <= n)
			return s;
		else {
			byte[] slice = Arrays.copyOfRange(sArray, 0, n);
			s = new String(slice);
			if (s.length() > 0 && !Character.isLetter(s.charAt(s.length() - 1))) {
				return s.substring(0, s.length() - 1);
			}
			return s;
		}
	}

	public static String converVietnamNameToEnglish(String name) {
		String EngName = null;
		if (null != name) {
			String nfdNormalizedString = Normalizer.normalize(name, Normalizer.Form.NFD);
			java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
			EngName = pattern.matcher(nfdNormalizedString).replaceAll("");
			if (EngName.contains("~"))
				EngName = EngName.replace("~", "");
			if (EngName.contains("ʼ"))
				EngName = EngName.replace("ʼ", "");
			if (EngName.contains("'"))
				EngName = EngName.replace("'", "");
			if (EngName.contains(","))
				EngName = EngName.replace(",", "");
			if (EngName.contains("."))
				EngName = EngName.replace(".", "");
			if (EngName.contains("ŉ"))
				EngName = EngName.replace("ŉ", "N");
			if (EngName.contains("Đ"))
				EngName = EngName.replace("Đ", "D");
			if (EngName.contains("đ"))
				EngName = EngName.replace("đ", "d");
			if (EngName.contains("Ŀ"))
				EngName = EngName.replace("Ŀ", "L");
			if (EngName.contains("ŀ"))
				EngName = EngName.replace("ŀ", "L");
		}
		return EngName;
	}

	public static String convertVietnamGenderToEnglish(String vietnamGender) {
		if (vietnamGender.equalsIgnoreCase(Constants.NID_MALE_VI)) {
			return Constants.NID_MALE;
		} else if (vietnamGender.equalsIgnoreCase(Constants.NID_FEMALE_VI)) {
			return Constants.NID_FEMALE;
		} else {
			return vietnamGender;
		}
	}

	public static String convertEnglishGenderToVietnam(String gender) {
		if (gender.equalsIgnoreCase(Constants.NID_MALE)) {
			return Constants.NID_MALE_VI;
		} else if (gender.equalsIgnoreCase(Constants.NID_FEMALE)) {
			return Constants.NID_FEMALE_VI;
		} else {
			return gender;
		}
	}

	public static String convertVietnamNationalityToEnglish(String vietnamNationality) {
		vietnamNationality = Utils.getStringValue(vietnamNationality);
		if (vietnamNationality.equalsIgnoreCase(Constants.NATIONALITY_VI)) {
			return Constants.NATIONALITY_EN;
		} else {
			return vietnamNationality;
		}
	}

	public static double PV(double rate, int nper, double pmt) {
		{
			double pv;
			if (rate != 0) {
				pv = (-pmt * ((Math.pow(1 + rate, nper) - 1) / rate)) / Math.pow(1 + rate, nper);
			} else {
				pv = -pmt * nper;
			}
			return Math.abs(pv);
		}
	}

	public static String phoneNumberFormat(String phoneNumber) {
		if (phoneNumber.startsWith("84")) {
			phoneNumber = phoneNumber.replaceFirst("84", "0");
		} else if (phoneNumber.startsWith("+84")) {
			phoneNumber = "0" + phoneNumber.substring(3);
		}
		return phoneNumber;
	}

	public static String getEmbossingName(String name) {
		if (name.length() < 19)
			return name;
		else if (name.split(" ").length <= 2)
			return name;
		else {
			String strArr[] = name.split(" ");
			String firstWord = strArr[0];
			String lastWord = strArr[strArr.length - 1];
			name = firstWord + " "
					+ formatEmbossingName(firstWord.length() + lastWord.length() + 2,
							name.substring(firstWord.length() + 1, name.length() - lastWord.length() - 1))
					+ " " + lastWord;
		}
		return name;
	}

	public static String formatEmbossingName(int firstAndLastWordLength, String middleWord) {

		if (firstAndLastWordLength + middleWord.length() < 19)
			return middleWord;
		else {
			String strArr[] = middleWord.split(" ");
			if (strArr.length == 1)
				middleWord = middleWord.substring(0, 1);
			else if (strArr[strArr.length - 1].length() == 1)
				middleWord = String.join(" ", strArr);
			else {
				int i = 0;
				while (strArr[i].length() == 1 && i < strArr.length - 1) {
					i++;
				}
				strArr[i] = strArr[i].substring(0, 1);
				middleWord = formatEmbossingName(firstAndLastWordLength, String.join(" ", strArr));
			}
		}
		return middleWord;
	}

	public static Map<String, Object> getHVNidFrontErrorMessageFromCodes(List<Integer> errorCodes) {
		Map<String, Object> errorMessage = new HashMap<String, Object>();
		if (errorCodes.contains(4)) {
			errorMessage.put("vi", "Hình CMND không hợp lệ, vui lòng chụp lại.");
			errorMessage.put("en", "Unqualified ID photo, please capture live one again.");
			return errorMessage;
		}
		if (errorCodes.contains(5)) {
			errorMessage.put("vi", "Vui lòng chụp mặt trước CMND rõ hình gương mặt.");
			errorMessage.put("en", "Pls capture ID front with recognizable face photo.");
			return errorMessage;
		}
		if (errorCodes.contains(6)) {
			errorMessage.put("vi", "Không chấp nhận CMND bản photo, Vui lòng chụp bản gốc.");
			errorMessage.put("en", "Do not accept with black&white ID copies, Please capture the original.");
			return errorMessage;
		}
		String enPrefix = "", viPrefix = "";
		if (errorCodes.contains(1)) {
			enPrefix += enPrefix.equals("") ? "National id number" : ", National id number";
			viPrefix += viPrefix.equals("") ? "CMND" : ", CMND";
		}
		if (errorCodes.contains(2)) {
			enPrefix += enPrefix.equals("") ? "Name" : ", Name";
			viPrefix += viPrefix.equals("") ? "tên" : ", tên";
		}
		if (errorCodes.contains(3)) {
			enPrefix += enPrefix.equals("") ? "Date of Birth" : ", Date of Birth";
			viPrefix += viPrefix.equals("") ? "ngày sinh" : ", ngày sinh";
		}
		errorMessage.put("en", enPrefix + " do not match with the details entered.");
		errorMessage.put("vi", viPrefix + " không khớp với các chi tiết được nhập.");
		return errorMessage;
	}

	public static String dobformatCorrection(String dob) {
		if (dob.length() == 4) {
			dob = "01-01-" + dob;
		} else {
			try {
				if (dob.split("-")[0].length() == 4) {
					dob = formatDate(dob, "yyyy-MM-dd", "dd-MM-yyyy");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dob;
	}

	public static Map<String, Object> parseNationalIdFrontData(String frontResponseFromHyperverge) {
		Map<String, Object> processVariables = new HashMap<>();
		if (frontResponseFromHyperverge == null || frontResponseFromHyperverge.equalsIgnoreCase("null"))
			return processVariables;
		try {
			JSONObject data = new JSONObject(frontResponseFromHyperverge);
			if (data.has("result") && data.get("result") instanceof JSONArray) {
				JSONArray result = data.getJSONArray("result");
				if (result.length() > 0 && result.get(result.length() - 1) instanceof JSONObject) {
					JSONObject result0 = result.getJSONObject(result.length() - 1);
					if (result0.has("details") && result0.get("details") instanceof JSONObject) {
						JSONObject details = result0.getJSONObject("details");
						processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, true);
						if (details.has("dob") && details.get("dob") instanceof JSONObject) {
							JSONObject dob = details.getJSONObject("dob");
							processVariables.put(WfImplConstants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(dob.getString("value")));
						} else {
							processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
						}

						if (details.has("id") && details.get("id") instanceof JSONObject) {
							JSONObject id = details.getJSONObject("id");
							processVariables.put(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(id.getString("value")));
						} else {
							processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
						}
						if (details.has("name") && details.get("name") instanceof JSONObject) {
							JSONObject name = details.getJSONObject("name");
							processVariables.put(WfImplConstants.BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(name.getString("value")));
							processVariables.put(WfImplConstants.NAME_CONFIDENCE_SCORE_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(name.getString("conf")));
							processVariables.put(WfImplConstants.NAME_TO_BE_REVIEWED_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(name.getString("to-be-reviewed")));
						} else {
							processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
						}
						if (details.has("province") && details.get("province") instanceof JSONObject) {
							JSONObject province = details.getJSONObject("province");
							processVariables.put(WfImplConstants.PROVINCE_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(province.getString("value")));
						}
						if (details.has("address") && details.get("address") instanceof JSONObject) {
							JSONObject address = details.getJSONObject("address");
							processVariables.put(WfImplConstants.ADDRESS_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(address.getString("value")));
						}
						if (details.has("gender") && details.get("gender") instanceof JSONObject) {
							JSONObject gender = details.getJSONObject("gender");
							processVariables.put(WfImplConstants.GENDER_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(gender.getString("value")));
						}
					} else {
						processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
					}
				} else {
					processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
				}
			}
		} catch (Exception e) {
			processVariables.put(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
		}
		return processVariables;
	}

	public static Map<String, Object> parseNationalIdBackData(String backResponseFromHyperverge) {
		Calendar cal = Calendar.getInstance();
		Map<String, Object> processVariables = new HashMap<>();
		if (backResponseFromHyperverge == null || backResponseFromHyperverge.equalsIgnoreCase("null"))
			return processVariables;
		try {
			JSONObject data = new JSONObject(backResponseFromHyperverge);
			if (data.has("result") && data.get("result") instanceof JSONArray) {
				JSONArray result = data.getJSONArray("result");
				if (result.length() > 0 && result.get(result.length() - 1) instanceof JSONObject) {
					JSONObject result0 = result.getJSONObject(result.length() - 1);
					if (result0.has("details") && result0.get("details") instanceof JSONObject) {
						JSONObject details = result0.getJSONObject("details");
						processVariables.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, true);
						if (details.has("province") && details.get("province") instanceof JSONObject) {
							JSONObject province = details.getJSONObject("province");
							processVariables.put(WfImplConstants.PLACE_OF_ISSUE_OF_NATIONAL_ID_FROM_HYPERVERGE_KEY,
									Utils.getStringValue(province.getString("value")));
						}
					} else {
						processVariables.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, false);
					}
				} else {
					processVariables.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, false);
				}
			} else {
				processVariables.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, false);
			}
		} catch (Exception e) {
			processVariables.put(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, false);
		}
		return processVariables;
	}

	public static int checkFrontBack(String hypervergeNationalIdResponse) {
		if (hypervergeNationalIdResponse == null || hypervergeNationalIdResponse.equalsIgnoreCase("null"))
			return -1;
		try {
			JSONObject data = new JSONObject(hypervergeNationalIdResponse);
			if (data.has("status") && data.has("statusCode") && data.getString("status").equals("success")
					&& data.getString("statusCode").equals("200")) {
				if (data.has("result") && data.get("result") instanceof JSONArray) {
					JSONArray result = data.getJSONArray("result");
					if (result.length() > 0 && result.get(result.length() - 1) instanceof JSONObject) {
						JSONObject result0 = result.getJSONObject(result.length() - 1);
						if (result0.has("type") && result0.get("type") instanceof String) {
							if (result0.getString("type").equals("id_front")
									|| result0.getString("type").equals("id_new_front")) {
								return 1;
							} else if (result0.getString("type").equals("id_back")
									|| result0.getString("type").equals("id_new_back")) {
								return 0;
							}
						}
					}
				}
			}
			return -1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static double convertValueBasedOnCurrency(String text) {
		double textValue = 0;
		String[] textArray = text.split(" ");
		if (textArray[1].trim().equals(WfImplConstants.VIATNUM_TRIEU_CURRENCY)) {
			textValue = Utils.getDoubleValue(textArray[0]) * 1000000;
		} else {
			textValue = Utils.getDoubleValue(textArray[0]) * 1000;
		}
		return textValue;

	}

	public static double divideValueBasedOnCurrency(double value, String text) {
		double textValue = 0;
		if (text.equals(WfImplConstants.VIATNUM_NGAN_CURRENCY)) {
			textValue = value / 1000;
		} else {
			textValue = value / 1000000;
		}
		return textValue;

	}

	public static String convertToViantnumCurrencyNgan(double currentVehicleValue) {
		currentVehicleValue = currentVehicleValue / 1000;
		return currentVehicleValue + WfImplConstants.NGÀN_VNĐ;
	}

	public static String convertToViantnumCurrencyTrieu(double currentVehicleValue) {
		currentVehicleValue = currentVehicleValue / 1000000;
		return currentVehicleValue + WfImplConstants.TRIỆU_VNĐ;
	}

	public static String ConvertViatnumCurrencyToDigits(String amount) {
		amount = replaceComma(amount);
		String digitCurrencyValue = "";
		try {
			if (amount != null && !amount.isEmpty()) {
				String[] split = amount.split(" ");
				if (split.length > 1) {
					String currency = split[1];
					Double parseDouble = Double.parseDouble(split[0]);
					if (currency.equals(WfImplConstants.VIATNUM_TRIEU_CURRENCY)) {
						Long longValue = new Double(parseDouble * 1000000).longValue();
						digitCurrencyValue = String.valueOf((longValue));
					} else if (currency.equals(WfImplConstants.VIATNUM_NGAN_CURRENCY)) {
						Long longValue = new Double(parseDouble * 1000).longValue();
						digitCurrencyValue = String.valueOf((longValue));
					} else {

						Integer parsedInteger = Integer.parseInt(split[0]);
						digitCurrencyValue = String.valueOf((parsedInteger));
					}
				} else {
					return amount;
				}
			}
			return digitCurrencyValue.replace(".", "");
		} catch (Exception e) {
			throw e;
		}

	}

	private static String replaceComma(String amount) {
		amount = amount.replaceAll(",", "");
		return amount;
	}

	public static Month EnumMonth(String month) {
		switch (month.toLowerCase()) {
		case "january":
			return Month.JANUARY;

		case "february":
			return Month.FEBRUARY;

		case "march":
			return Month.MARCH;

		case "april":
			return Month.APRIL;

		case "may":
			return Month.MAY;

		case "june":
			return Month.JUNE;

		case "july":
			return Month.JULY;

		case "august":
			return Month.AUGUST;

		case "september":
			return Month.SEPTEMBER;

		case "october":
			return Month.OCTOBER;

		case "november":
			return Month.NOVEMBER;

		case "december":
			return Month.DECEMBER;

		default:
			break;
		}
		return null;
	}

	public static int getMonthsDiff(String year, String month) {
		if (!year.isEmpty() && !month.isEmpty()) {
			int yearInt = Integer.valueOf(year);
			Month enumMonth = Utils.EnumMonth(month);
			if (enumMonth != null) {
				LocalDate currentDate = LocalDate.now();
				LocalDate registrationDate = LocalDate.of(yearInt, enumMonth, 1);
				int monthsDiff = (int) ChronoUnit.MONTHS.between(registrationDate, currentDate);
				return monthsDiff;
			}
			return -1;
		}
		return -1;
	}

	public static String translateEngToViatnam(String text) {
		text = Utils.getStringValue(text);
		Map<String, String> map = Constants.engToViatnamMap;
		if (map.containsKey(text.trim().toLowerCase())) {
			text = map.get(text.trim().toLowerCase());
		}
		return text;
	}

	public static String translateViatnamToEng(String text) {
		text = Utils.getStringValue(text);
		Map<String, String> map = Constants.viatnamToEngMap;
		if (map.containsKey(text.trim().toLowerCase())) {
			text = map.get(text.trim().toLowerCase());
		}
		return text;
	}

	public static String translateToEngDashBoard(String text) {
		text = Utils.getStringValue(text);
		Map<String, String> map = Constants.engDashBoardMap;
		if (map.containsKey(text.trim().toLowerCase())) {
			text = map.get(text.trim().toLowerCase());
		}
		return text;
	}

	public static int getAgeFromDOB(String dateOfBirth) throws ParseException {

		if (dateOfBirth == null || dateOfBirth.isEmpty()) {
			return 0;
		}

		String convertedDateFormat = Utils.formatDate(dateOfBirth, Constants.DATE_FORMAT_DASH,
				Constants.DATE_FORMAT_DASH_YEAR);
		LocalDate dob = LocalDate.parse(convertedDateFormat);
		LocalDate today = LocalDate.now();
		Period period = Period.between(dob, today);
		return period.getYears();
	}

	public static JSONArray filterJsonArray(List<HashMap<String, Object>> filterList) throws JSONException {
		JSONArray finalJsonArrayRequest = new JSONArray();
		if (filterList != null) {
			for (HashMap<String, Object> filter : filterList) {
				JSONObject keyListJsonObject = new JSONObject();
				if (filter.containsKey(Constants.KEY) && filter.containsKey(Constants.VALUE)) {
					keyListJsonObject.put(Constants.KEY, filter.get(Constants.KEY));
					keyListJsonObject.put(Constants.VALUE, filter.get(Constants.VALUE));
					finalJsonArrayRequest.put(keyListJsonObject);
				}
			}
		}
		return finalJsonArrayRequest;
	}

	public static double convertValue(String text) {
		String[] textArray = text.split("[|]");
		String textString = textArray[0];
		String[] textArrayWithouSpace = textString.trim().split(" ");
		double finalVal = Utils.getDoubleValue(textArrayWithouSpace[1]);
		return finalVal;

	}

	public static String createStringForPlanValues(String[] splitPlanValue, String appender) {
		String str = "";
		for (int i = 0; i < splitPlanValue.length; i++) {
			str = str + splitPlanValue[i] + appender;
		}
		str = str.trim();
		return str;
	}

	public static String stringArrayToString(Object[] splitPlanValue, String appender) {
		String str = "";
		for (int i = 0; i < splitPlanValue.length; i++) {
			str = str + splitPlanValue[i] + appender;
		}
		str = str.trim();
		return str;
	}

	public static String createStringWithAppender(String splitPlanValue, String appender) {
		String str = "";
		str = str + splitPlanValue + appender;
		str = str.trim();
		return str;
	}

	private static DecimalFormat df = new DecimalFormat("0.00");

	public static String convertNumberToMillion(int n) {

		double r = n / 1000000.0;
		System.out.println(r);
		return df.format(r);
	}

	public static Integer getPremiumAmount(PremiumCalculateRequest premiumRequest) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		logger.info("premiumRequest : "+mapper.writeValueAsString(premiumRequest));

		if (premiumRequest.getProductType().equals(Constants.Products.CANCER_CARE.name())) {

			String premiumKey = premiumRequest.getPlanId() + "_" + premiumRequest.getGender() + "_"
					+ premiumRequest.getAge();

//			logger.info("Premium Key : " + premiumKey);
			String premium = Constants.cancer_premium_map.get(premiumKey);

		} else if (premiumRequest.getProductType().equals(Constants.Products.TERM_LIFE.name())) {

			TermLifeAgePremiumRates agePremium = Constants.AGE_PREMIUM_MAP.get(String.valueOf(premiumRequest.getAge()));
			PlanPremiums premiumAmountBasedOnPlanId = Constants.SUM_PPD_CRITICAL_PROTE_AMOUNT
					.get(premiumRequest.getPlanId());

			if (premiumRequest.getPremiumType().equals(Constants.PremiumType.BASIC_PREMIUM.name())) {
				Double basicPremium = getFinalPremiumAmount(premiumAmountBasedOnPlanId, agePremium);
				if (premiumRequest.getAdditionalAmount() != null) {
					basicPremium = basicPremium + premiumRequest.getAdditionalAmount();
				}
				return (int) Math.round(basicPremium);

			} else if (premiumRequest.getPremiumType()
					.equals(Constants.PremiumType.ADDITIONAL_CRITICAL_PREMIUM.name())) {
				Double additionalCriticalPremium = getFinalCriticalAmount(premiumAmountBasedOnPlanId, agePremium);
				return (int) Math.round(additionalCriticalPremium);

			} else if (premiumRequest.getPremiumType().equals(Constants.PremiumType.PROTECTION_PREMIUM.name())) {
				Integer protectionAmount = 0;
				if (premiumRequest.getAdditionalAmount() != null) {
					protectionAmount = (premiumAmountBasedOnPlanId.getProtectionAmount()
							+ premiumAmountBasedOnPlanId.getAdditionalAmount());
					return protectionAmount;
				} else {
					protectionAmount = premiumAmountBasedOnPlanId.getProtectionAmount();
					return protectionAmount;
				}
			} else {
				throw new FECException(ErrorCodes.INVALID_PREMIUM_TYPE);
			}
		} else {
			throw new FECException(ErrorCodes.INVALID_PRODUCT_TYPE);
		}
		return 0;
	}

	static Double getFinalPremiumAmount(PlanPremiums premiumAmountBasedOnPlanId, TermLifeAgePremiumRates agePremium) {

		Double premiumAmount = getTLPremiumsByRate(premiumAmountBasedOnPlanId.getSumInsuredAmount(),
				agePremium.getPremiumRate());

		Double ppdAmount = getTLPremiumsByRate(premiumAmountBasedOnPlanId.getPpdAmount(), agePremium.getPpdRate());

		return (premiumAmount + ppdAmount);
	}

	public static Double getTLPremiumsByRate(Integer amount, Double rate) {
		Double premiumAmount = (Double) ((amount * rate) / 100);
		return premiumAmount;
	}

	static Double getFinalCriticalAmount(PlanPremiums premiumAmountBasedOnPlanId, TermLifeAgePremiumRates agePremium) {
		Double finalCriticalPremiumAmount = getTLPremiumsByRate(premiumAmountBasedOnPlanId.getCriticalIllenessAmount(),
				agePremium.getCriticalRate());
		return finalCriticalPremiumAmount;
	}

	public static Integer convertToMillion(Integer protectionAmount) {
		protectionAmount = protectionAmount * 1000000;
		return protectionAmount;
	}

	public static String dateFormatAddDaysAndYear(String DatetoParse, String isExistOfStartOrExpiry) {
		Calendar cal = Calendar.getInstance();
		Date date;
		try {
			date = new SimpleDateFormat("dd/MM/yyyy").parse(DatetoParse);
			cal.setTime(date);
			if (isExistOfStartOrExpiry.equals("start_date_exist") || isExistOfStartOrExpiry == "start_date_exist") {
				cal.add(Calendar.YEAR, 1);
			} else {
				cal.add(Calendar.DATE, 1);
			}
		} catch (ParseException e) {
			logger.error("Exception while parse date", e);
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
	}

	public static String getMainPlanId(String planId) {
		if (planId == null || planId.isEmpty()) {
			return "";
		}
		String planIds[] = planId.split(",");
		return planIds[0];
	}

	public static String getInterNationalNumber(String amount) {
		if (amount == null || amount.isEmpty()) {
			return "";
		}
		char[] c = amount.toCharArray();

		String convertedAmount = "";
		int n = amount.indexOf(".");

		if (n < 0) {
			n = amount.length();
		}
		for (int i = 1; i <= n; i++) {
			convertedAmount = c[n - i] + convertedAmount;

			if (i % 3 == 0 && i < n) {
				convertedAmount = "," + convertedAmount;
			}
		}

		convertedAmount += amount.substring(n, amount.length());
		return convertedAmount;
	}

	public static int getAge(String dateOfBirth) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_DASH);
		Date d = sdf.parse(dateOfBirth);

		Calendar c = Calendar.getInstance();
		c.setTime(d);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH) + 1;
		int dayOfMonth = c.get(Calendar.DATE);

		LocalDate bday = LocalDate.of(year, month, dayOfMonth);
		LocalDate today = LocalDate.now();
		
		logger.info("DOB date : " + bday.toString());
		logger.info("current date : " + today.toString());
		
		Period period = Period.between(bday, today);
		int years = period.getYears();
		int months = period.getMonths();
		int days = period.getDays();
		int age = 0;

		logger.info("number of years: " + years);
		logger.info("number of months: " + months);
		logger.info("number of dayOfMonth: " + dayOfMonth);
		logger.info("number of days: " + days);

		if (years == 0) {
			if (months < 3) {
				age = 0;

			} else if (months == 3 && days == 0) {
				age = 0;
			} else {
				age = 1;
			}
		} else {
//			if (months == 0 && days == 0) {
//				age = years;
//			} else {
//				age = years + 1;
//			}
			
			age = years + 1;

		}

		return age;

	}

}
