package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditBancaBlackListIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaBlackListIntegration.class);

	public JSONObject buildRequestPayload(String nationalId) throws JSONException {
		logger.info("-->Entering FECreditBancaBlackListIntegration.buildRequestPayload()");
		JSONObject requestPayload = new JSONObject();
		JSONObject bodyRequestData = new JSONObject();
		bodyRequestData.put("aps:CheckBlackList.Sys.TransID", Utils.generateUUIDTransanctionId());
		bodyRequestData.put("aps:CheckBlackList.Sys.RequestorID", Constants.FEC_REQUESTOR_ID);
		bodyRequestData.put("aps:CheckBlackList.Sys.DateTime", Utils.getCurrentDateTime());
		bodyRequestData.put("aps:CheckBlackList.BlackListRequest.Sequence",
				Constants.FEC_CREDIT_BLACK_LIST_SEQUENCE_KEY);
		bodyRequestData.put("aps:CheckBlackList.BlackListRequest.NationalID", nationalId);
		bodyRequestData.put("aps:CheckBlackList.BlackListRequest.PersonType",
				Constants.FEC_CREDIT_BLACK_LIST_PERSON_TYPE);
		requestPayload.put("body", bodyRequestData);
		logger.info("FECreditBancaBlackList Request Payload : " + requestPayload.toString());
		logger.info("<-- Exiting FECreditBancaBlackListIntegration.buildRequestPayload()");
		return requestPayload;
	}

	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.BANCA_BLACKLIST_INTEGRATION_SLUG_KEY + "/" + Constants.BANCA_BLACK_LIST_SLUG_KEY + "/"
				+ wfImplConfig.getIbCompany() + "/" + Constants.IB_HASH_VALUE+"/?env="+wfImplConfig.getIbEnv()
				+ Constants.BANCA_BLACK_LIST_SOAP_KEY;
		logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		logger.info("resposeAsJson>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + resposeAsJson.getBody());
		return resposeAsJson;
	}

	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		logger.info("-->Entering FECreditBancaBlackListIntegration.parseResponse()");
		Map<String, Object> bancaBlackListResponseMap = new HashMap<>();
		JSONObject apiResponse = null;
		try {
			logger.debug("Success Response for FECBancaBlackList");
			apiResponse = responseFromIntegrationBroker.getJSONObject("NS1:CheckBlackListResponse");
			logger.info("apiResponse>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + apiResponse);
			JSONObject sysResponse = apiResponse.getJSONObject("Sys");
			if (sysResponse.getString("Description").equals("DATA_FOUND")) {
				bancaBlackListResponseMap = new ObjectMapper().readValue(apiResponse.toString(),
						new TypeReference<Map<String, Object>>() {
						});
				bancaBlackListResponseMap.put(Constants.STATUS_MAP_KEY, true);
				bancaBlackListResponseMap.put(Constants.IS_BLACK_LISTED_KEY, true);

			} else {
				logger.debug("Data Not Found In Description for FEC BancaBlackList");
				bancaBlackListResponseMap.put(Constants.STATUS_MAP_KEY, false);
				bancaBlackListResponseMap.put(Constants.IS_BLACK_LISTED_KEY, false);
				bancaBlackListResponseMap.put(Constants.DESCRIPTION_MAP_KEY, sysResponse.getString("Description"));
			}
		} catch (Exception e) {
			bancaBlackListResponseMap.put(Constants.STATUS_MAP_KEY, false);
			bancaBlackListResponseMap.put(Constants.DESCRIPTION_MAP_KEY, e.getMessage());
		}
		logger.info("<-- Exiting FECreditBancaBlackListIntegration.parseResponse()");
		return bancaBlackListResponseMap;
	}

}
