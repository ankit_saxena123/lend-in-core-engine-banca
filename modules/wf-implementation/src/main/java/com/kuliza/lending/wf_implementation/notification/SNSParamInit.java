package com.kuliza.lending.wf_implementation.notification;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Component
public class SNSParamInit {

	@Value("${reg.topic.arn}")
	private String regTopicARN;
	
	@Value("${sh.topic.arn}")
	private String shTopicARN;
	
	@Value("${tw.topic.arn}")
	private String twTopicARN;
	
	@Value("${pa.topic.arn}")
	private String paTopicARN;
	
	@Value("${cc.topic.arn}")
	private String ccTopicARN;
	
	@Value("${fh.topic.arn}")
	private String fhTopicARN;
	
	@Value("${tl.topic.arn}")
	private String tlTopicARN;
	
	@Value("${us.topic.arn}")
	private String usTopicARN;
	
	@Value("${uh.topic.arn}")
	private String uhTopicARN;
	
	@Value("${ue.topic.arn}")
	private String ueTopicARN;
	
	private Map<String,String> topicsMap;

    @Autowired
	SNSParamInit(@Value("${reg.topic.arn}") String regTopicARN, @Value("${sh.topic.arn}") String shTopicARN,@Value("${tw.topic.arn}") String twTopicARN,@Value("${pa.topic.arn}") String paTopicARN,@Value("${cc.topic.arn}") String ccTopicARN, @Value("${fh.topic.arn}") String fhTopicARN, @Value("${tl.topic.arn}") String tlTopicARN, @Value("${us.topic.arn}") String usTopicARN, @Value("${uh.topic.arn}") String uhTopicARN, @Value("${ue.topic.arn}") String ueTopicARN){
		
    	this.topicsMap = new HashMap<String,String>();
    	this.topicsMap.put(WfImplConstants.REG_TOPIC, regTopicARN);
    	this.topicsMap.put(WfImplConstants.SH_TOPIC, shTopicARN);
    	this.topicsMap.put(WfImplConstants.TW_TOPIC, twTopicARN);
    	this.topicsMap.put(WfImplConstants.PA_TOPIC, paTopicARN);
    	this.topicsMap.put(WfImplConstants.CC_TOPIC, ccTopicARN);
    	this.topicsMap.put(WfImplConstants.FH_TOPIC, fhTopicARN);
    	this.topicsMap.put(WfImplConstants.TL_TOPIC, tlTopicARN);
    	this.topicsMap.put(WfImplConstants.US_TOPIC, usTopicARN);
    	this.topicsMap.put(WfImplConstants.UH_TOPIC, uhTopicARN);
    	this.topicsMap.put(WfImplConstants.UE_TOPIC, ueTopicARN);
	}
	
	public String getRegTopicARN() {
		return regTopicARN;
	}

	public void setRegTopicARN(String regTopicARN) {
		this.regTopicARN = regTopicARN;
	}

	public String getShTopicARN() {
		return shTopicARN;
	}

	public void setShTopicARN(String shTopicARN) {
		this.shTopicARN = shTopicARN;
	}

	public String getTwTopicARN() {
		return twTopicARN;
	}

	public void setTwTopicARN(String twTopicARN) {
		this.twTopicARN = twTopicARN;
	}

	public String getPaTopicARN() {
		return paTopicARN;
	}

	public void setPaTopicARN(String paTopicARN) {
		this.paTopicARN = paTopicARN;
	}

	public String getCcTopicARN() {
		return ccTopicARN;
	}

	public void setCcTopicARN(String ccTopicARN) {
		this.ccTopicARN = ccTopicARN;
	}

	public String getFhTopicARN() {
		return fhTopicARN;
	}

	public void setFhTopicARN(String fhTopicARN) {
		this.fhTopicARN = fhTopicARN;
	}

	public String getTlTopicARN() {
		return tlTopicARN;
	}

	public void setTlTopicARN(String tlTopicARN) {
		this.tlTopicARN = tlTopicARN;
	}

	public String getUsTopicARN() {
		return usTopicARN;
	}

	public void setUsTopicARN(String usTopicARN) {
		this.usTopicARN = usTopicARN;
	}

	public String getUhTopicARN() {
		return uhTopicARN;
	}

	public void setUhTopicARN(String uhTopicARN) {
		this.uhTopicARN = uhTopicARN;
	}

	public String getUeTopicARN() {
		return ueTopicARN;
	}

	public void setUeTopicARN(String ueTopicARN) {
		this.ueTopicARN = ueTopicARN;
	}
	
	public Map<String, String> getTopicsMap() {
		return topicsMap;
	}

	public void setTopicsMap(Map<String, String> topicsMap) {
		this.topicsMap = topicsMap;
	}
}
