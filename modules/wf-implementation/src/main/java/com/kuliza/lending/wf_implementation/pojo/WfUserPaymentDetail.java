package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class WfUserPaymentDetail {

	@NotNull(message = "AccountHoldersName cannot be null")
	private String accountHoldersName;

	@NotNull(message = "AccountNumber cannot be null")
	private String accountNumber;

	@NotNull(message = "BankName cannot be null")
	private String bankName;

	@NotNull(message = "BranchName cannot be null")
	private String branchName;

	public String getAccountHoldersName() {
		return accountHoldersName;
	}

	public void setAccountHoldersName(String accountHoldersName) {
		this.accountHoldersName = accountHoldersName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

}
