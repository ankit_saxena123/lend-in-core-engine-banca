package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.pojo.UpdatePolicy;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

import omnidocsapi.SysResponseType;
import omnidocsapi.UploadDocResponse;

@Service
public class PolicyUpdateAutomationService {

	private static final Logger logger = LoggerFactory.getLogger(PolicyUpdateAutomationService.class);

	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;

	@Autowired
	private WorkflowApplicationDao workFlowApplicationDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private FECDMSService fecDmsService;
	
	@Autowired
	private WfImplConfig wfImplConfig;

	public static String OUT_BOUND = "/outbound/";
	
	public ApiResponse updatePolicy(UpdatePolicy policyDetails) {

		List<Map<String, String>> policyDetail = policyDetails.getPolicyDetails();
		logger.info("================policyDetail============ " + policyDetail);
		Map<String, Object> variables = new HashMap<String, Object>();
		try {
			for (int i = 0; i < policyDetail.size(); i++) {

				logger.info("================policyDetail============ " + policyDetail.get(i));
				IPATPaymentConfirmModel ipatConfirmPaymentModel = ipatPaymentConfirmDao
						.findByCrmIdAndPaymentConfirmStatusAndIsDeleted(
								policyDetail.get(i).get(Constants.BANCA_CRM_ID_KEY), true, false);
				WorkFlowApplication wfApplication = ipatConfirmPaymentModel.getWfApplication();
				String processInstanceId = wfApplication.getProcessInstanceId();
				logger.info("================processInstanceId============ " + processInstanceId);
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();

				variables = processInstance.getProcessVariables();
				logger.info("================variables============ " + variables);
				String planId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PLAN_ID));
				String sumInsured = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
				String premium = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
				String paymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE));
				String endDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
				String nextPaymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_NEXT_PAYMENT_DUE));
				WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO
						.findByWorkFlowApplicationAndPlanIdAndIsDeleted(wfApplication, planId, false);

				wfApplication.setIs_purchased(true);
				wfApplication.setProtectionAmount(sumInsured);
				wfApplication.setMontlyPayment(premium);
				wfApplication.setPlanExpiringDate(endDate);
				wfApplication.setNextPaymentDue(nextPaymentDate);
				wfApplication.setApplicationDate(paymentDate);
				wfApplication.setPolicyNumber(policyDetail.get(i).get(Constants.POLICY_NUMBER));
				logger.info("<================>appId:" + wfApplication.getApplicationId() + "<============>");
				String docID = uploadDms(wfApplication.getApplicationId(),
						policyDetail.get(i).get(Constants.POLICY_NUMBER).replace("/", "_"));
				logger.info("==========>>>>DMS doc ID===========>>>> : " + docID);
				if (docID != null) {
					wfApplication.setDmsDocId(docID);
				} else {
					logger.error("<=======>DMS doc ID is NULL<=========>");
				}

				workFlowApplicationDao.save(wfApplication);
				if (workFlowPolicy != null) {
					logger.info("--->inside workflow policy" + policyDetail.get(i).get(Constants.POLICY_NUMBER)
							+ "payment date: " + paymentDate + "endDate: " + endDate + "transactionId: "
							+ policyDetail.get(i).get(Constants.BANCA_TRANS_ID) + "pdg name: "
							+ policyDetail.get(i).get(Constants.POLICY_NUMBER).replace("/", "_"));
					workFlowPolicy.setDmsDocId(docID);
					workFlowPolicy.setPolicyNumber(policyDetail.get(i).get(Constants.POLICY_NUMBER));
					workFlowPolicy.setPurchasedDate(paymentDate);
					workFlowPolicy.setPlanExpiringDate(endDate);
					workFlowPolicy.setTransactionId(policyDetail.get(i).get(Constants.BANCA_TRANS_ID));
					workFlowPolicyDAO.save(workFlowPolicy);
					logger.info("--->Saved in workFlowPolicyDAO ******* ");
				} else {
					logger.error("<=======>workFlowPolicy is NULL<=========>");

				}
			}
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, null);

		} catch (Exception e) {
			logger.error("<=======>error in savinng workFlowPolicy<=========>" + e);
			e.printStackTrace();
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, null);
		}
	}

	public String uploadDms(String appId, String policyPdfUrl) throws Exception {
		logger.info("================uploadDms============ " + policyPdfUrl);
		String docId = null;
		File file = null;
		String ftpPath = wfImplConfig.getOfflinePolicyPath();
		
		try {

			file = new File(ftpPath+OUT_BOUND+ policyPdfUrl + ".pdf");
			logger.info("====file path====>>: " + ftpPath+OUT_BOUND+ policyPdfUrl + ".pdf");
			UploadDocResponse uploadDocument = fecDmsService.uploadDocument(Constants.INSURER_CERTIFICATE_DMS_ID, appId,
					"101", true, file);

			SysResponseType sys = uploadDocument.getSys();
			int code = sys.getCode();

			if (code == 1) {
				docId = fecDmsService.getDMSDocIdFromResponse(uploadDocument);
				logger.info(">>>>>>>>>>>>>dmsDocId for Insurer PDF :" + docId);
			} else {
				logger.error(">>>>>>>>>>>Failed to upload pdf to DMS<<<<<<<<<");
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Filed to upload Doc to DMS :" + e.getMessage(), e);

		} finally {
			if (file != null) {
				if (file.exists()) {
					file.delete();
				}
			}
		}
		return docId;

	}

}
