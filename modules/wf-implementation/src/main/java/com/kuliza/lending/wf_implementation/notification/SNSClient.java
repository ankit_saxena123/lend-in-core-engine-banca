package com.kuliza.lending.wf_implementation.notification;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.kuliza.lending.wf_implementation.notification.SNSConfiguration.Platform;

public class SNSClient {
   
	public final static String awsAccount = SNSConfiguration.AWS_ACCESS_KEY;
	public final static String awsSecret = SNSConfiguration.AWS_SECRET_KEY;
	public final static String awsRegion = SNSConfiguration.AWS_REGION_NAME;
	
	AmazonSNSClient awsSnsClient = null;
		
	public static final Map<Platform, Map<String, MessageAttributeValue>> attributesMap = new HashMap<Platform, Map<String, MessageAttributeValue>>();
	static {
		attributesMap.put(Platform.GCM, null);
		attributesMap.put(Platform.APNS, null);
		
	}
	
	public AmazonSNSClient getSNSClient(){
		return new AmazonSNSClient(new BasicAWSCredentials(awsAccount, awsSecret));
	}

	public AmazonSNSClient getAwsSnsClient() {
		return awsSnsClient;
	}

	public void setAwsSnsClient(AmazonSNSClient awsSnsClient) {
		this.awsSnsClient = awsSnsClient;
	}

	public static Map<Platform, Map<String, MessageAttributeValue>> getAttributesmap() {
		return attributesMap;
	}
	
}
