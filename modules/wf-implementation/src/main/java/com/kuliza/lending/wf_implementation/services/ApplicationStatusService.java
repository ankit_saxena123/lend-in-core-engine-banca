package com.kuliza.lending.wf_implementation.services;

import org.flowable.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class ApplicationStatusService {
	

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	
	private static final Logger logger = LoggerFactory
			.getLogger(ApplicationStatusService.class);
	
	public DelegateExecution persistAppliactionStatus(DelegateExecution execution) {
		logger.info("--->persistAppliactionStatus()");
		String applicationStatus = Utils.getStringValue(execution.getVariable(WfImplConstants.APPLICATION_STATUS));
		logger.info("applicationStatus :"+applicationStatus);
		if(!applicationStatus.isEmpty()){
		String processInstanceId = execution.getProcessInstanceId();
		logger.info("processInstanceId :"+processInstanceId);
		WorkFlowApplication wfApp = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if(wfApp!=null ){
			wfApp.setCurrentJourneyStage(applicationStatus);
			workflowApplicationDao.save(wfApp);
			logger.info("applicationStatus is persisted");
		}
		}
		return execution;
	}

}
