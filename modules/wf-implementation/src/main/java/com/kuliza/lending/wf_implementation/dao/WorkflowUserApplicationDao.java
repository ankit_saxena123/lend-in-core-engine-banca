package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;

@Repository
@Transactional
public interface WorkflowUserApplicationDao extends CrudRepository<WorkFlowUserApplication, Long>,JpaRepository<WorkFlowUserApplication, Long> {

	public WorkFlowUserApplication findById(long id);

	public WorkFlowUserApplication findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowUserApplication findByUserIdentifierAndProcInstIdAndIsDeleted(String username, String procInstId, boolean isDeleted);
	
	public WorkFlowUserApplication findByUserIdentifierAndProcInstId(String username, String procInstId);

	public WorkFlowUserApplication findByProcInstIdAndIsDeleted(String procInstId, boolean isDeleted);
	
	public List<WorkFlowUserApplication> findByUserIdentifierAndParentProcInstIdAndProcessNameAndIsDeleted(String username, 
			String parentProcInst, String processname, boolean isDeleted);
	

	public List<WorkFlowUserApplication> findByUserIdentifierAndParentProcInstIdAndIsDeleted(String username, 
			String parentProcInst, boolean isDeleted);
	
	public List<WorkFlowUserApplication> findByUserIdentifier(String username);
	public List<WorkFlowUserApplication> findByUserIdentifierAndProcessName(String username,String processName);
	public List<WorkFlowUserApplication> findByUserIdentifierAndProcessNameNot(String username,String processName);

	
	public List<WorkFlowUserApplication> findByUserIdentifierAndIsDeleted(String username, boolean isDeleted);
	public List<WorkFlowUserApplication> findByUserIdentifierAndIsDeletedAndProcessName(String username, boolean isDeleted,String processName);
	public List<WorkFlowUserApplication> findByUserIdentifierAndIsDeletedAndProcessNameNot(String username, boolean isDeleted,String processName);

	@Modifying
	@Query(value= "update workflow_user_app set is_deleted=1 where date(modified) > ?1 and  is_deleted=0 ",nativeQuery = true)
	public int setIsDeletedForExpiryJourney(String oldJourney); 
	
	@Modifying
	@Query(value= "update workflow_user_app set is_deleted=1 where modified < DATE_SUB(NOW(), INTERVAL 1 HOUR) and  is_deleted=0 ",nativeQuery = true)
	public int setIsDeletedForTimelyExpiryJourney();
	
	@Query(value= "select * from workflow_user_app where wf_application_id=?1 ",nativeQuery = true)
	public WorkFlowUserApplication getUserIdentifier(long wf_app_id );

	
}