package com.kuliza.lending.wf_implementation.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDocsDao;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserDocs;
import com.kuliza.lending.wf_implementation.pojo.FileDownloadInput;
import com.kuliza.lending.wf_implementation.pojo.FileUploadForm;
import com.kuliza.lending.wf_implementation.pojo.FileUploadResponse;

@Service
public abstract class BaseDMSService {

	@Autowired
	private WorkflowUserDocsDao workflowUserDocsDao;
	
	private static final Logger logger = LoggerFactory.getLogger(BaseDMSService.class);
	
	public ApiResponse upload(String userId, FileUploadForm uploadForm) {
		FileUploadResponse fileUploadResponse = uploadFile(userId, uploadForm);
		if (fileUploadResponse != null) {
			WorkFlowUserDocs wfUserDoc = new WorkFlowUserDocs(userId, fileUploadResponse.getDocumentId(), uploadForm.getLabel(), 
					CommonHelperFunctions.getStringValue(uploadForm.getDocumentType()), null, null);
			if (fileUploadResponse.getDocumentName() != null && !"".equals(fileUploadResponse.getDocumentName())) {
				wfUserDoc.setDocName(fileUploadResponse.getDocumentName());
				String[] docNameArray = fileUploadResponse.getDocumentName().split("\\.");
				if (docNameArray.length > 0) {
					wfUserDoc.setDocExtension(docNameArray[docNameArray.length - 1]);
				}
			}
			workflowUserDocsDao.save(wfUserDoc);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, fileUploadResponse);
		} else {
			logger.error("There is some error in uploadig doc.");
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FILE_UPLOAD_ERROR);
		}
	}
	
	protected abstract FileUploadResponse uploadFile(String userId, FileUploadForm uploadForm);

	protected abstract byte[] downloadFile(String userId, FileDownloadInput fileDownloadInput);
	
	public byte[] download(String userId, FileDownloadInput fileDownloadInput) {
		WorkFlowUserDocs wfUserDoc = workflowUserDocsDao.findByDocumentIdAndUserIdAndIsDeleted(
				fileDownloadInput.getDocumentId(), userId, false);
		if (wfUserDoc != null) {
			return downloadFile(userId, fileDownloadInput);
		} else {
			logger.error("User doesnt have permisson to access this file.");
			return null;
		}
	}
}