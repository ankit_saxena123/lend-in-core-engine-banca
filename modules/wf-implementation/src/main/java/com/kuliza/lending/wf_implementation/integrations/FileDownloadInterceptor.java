package com.kuliza.lending.wf_implementation.integrations;

import java.util.Iterator;

import javax.xml.soap.AttachmentPart;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

public class FileDownloadInterceptor implements ClientInterceptor {

	//private AttachmentPart[] attList;

	private AttachmentPart attachment;

	@Override
	public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
		WebServiceMessage ctx = (WebServiceMessage) messageContext.getResponse();
		if (ctx instanceof SaajSoapMessage) {
			SaajSoapMessage msg = (SaajSoapMessage) ctx;
			if (msg.getSaajMessage().countAttachments() > 0) {
				Iterator<AttachmentPart> itr = msg.getSaajMessage().getAttachments();
				setAttachment(itr.next()); //We except only one attachment, so no need to iterate
			}
		}
		return true;
	}

	@Override
	public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {
		// TODO Auto-generated method stub

	}

	public AttachmentPart getAttachment() {
		return attachment;
	}

	public void setAttachment(AttachmentPart attachment) {
		this.attachment = attachment;
	}



}
