package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class PayooConfirm {
	
	@NotNull
	public String crmId;
	@NotNull
	public String transactionId;
	
	public String getCrmId() {
		return crmId;
	}
	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	@Override
	public String toString() {
		return "PayooConfirm [crmId=" + crmId + ", transactionId=" + transactionId + "]";
	}
	
	
	

}
