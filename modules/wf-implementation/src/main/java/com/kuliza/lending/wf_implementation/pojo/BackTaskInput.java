package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.wf_implementation.utils.Constants;


public class BackTaskInput {

	@NotNull(message = "processInstanceId is a required key")
	@NotEmpty(message = "processInstanceId cannot be empty")
	@Pattern(regexp = Constants.NOT_ZERO_NUMBER_REGEX, message = "Invalid Process Instance Id")
	String processInstanceId;

	@NotNull(message = "currentTaskId is a required key")
	@NotEmpty(message = "currentTaskId cannot be empty")
	@Pattern(regexp = Constants.NOT_ZERO_NUMBER_REGEX, message = "Invalid Current Task Id")
	String currentTaskId;

	@NotNull(message = "backTaskDefinitionKey is a required key")
	@NotEmpty(message = "backTaskDefinitionKey cannot be empty")
	String backTaskDefinitionKey;

	public BackTaskInput() {
		super();
	}

	public BackTaskInput(String processInstanceId, String currentTaskId, String backTaskDefinitionKey) {
		super();
		this.processInstanceId = processInstanceId;
		this.currentTaskId = currentTaskId;
		this.backTaskDefinitionKey = backTaskDefinitionKey;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getCurrentTaskId() {
		return currentTaskId;
	}

	public void setCurrentTaskId(String currentTaskId) {
		this.currentTaskId = currentTaskId;
	}

	public String getBackTaskDefinitionKey() {
		return backTaskDefinitionKey;
	}

	public void setBackTaskDefinitionKey(String backTaskDefinitionKey) {
		this.backTaskDefinitionKey = backTaskDefinitionKey;
	}

}
