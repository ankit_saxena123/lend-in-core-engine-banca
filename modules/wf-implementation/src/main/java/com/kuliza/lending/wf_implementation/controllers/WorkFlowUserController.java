package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.pojo.DeviceInfoInpuForm;
import com.kuliza.lending.wf_implementation.pojo.PolicyDetails;
import com.kuliza.lending.wf_implementation.pojo.ProductRequest;
import com.kuliza.lending.wf_implementation.pojo.UpdateDmsRequest;
import com.kuliza.lending.wf_implementation.pojo.UpdateNotification;
import com.kuliza.lending.wf_implementation.pojo.ViewPdfForm;
import com.kuliza.lending.wf_implementation.pojo.WFUserRequest;
import com.kuliza.lending.wf_implementation.services.FECreditBancaCIFCustomerSearchListService;
import com.kuliza.lending.wf_implementation.services.WorkFlowUserService;

@RestController
@RequestMapping("/wf-user")
public class WorkFlowUserController {

	@Autowired
	private WorkFlowUserService workFlowUserService;
	@Autowired
	private WfImplConfig wfImplConfig;

	@Autowired
	private FECreditBancaCIFCustomerSearchListService feCreditBancaCIFCustomerSearchListService;

	@RequestMapping(method = RequestMethod.GET, value = "/getUserInfo")
	public ApiResponse getUserInfo(Principal principal) throws Exception {
		return workFlowUserService.getUserInfoDetails(principal.getName());

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getNotification")
	public ApiResponse getUserNotifications(Principal principal) throws Exception {
		return workFlowUserService.getUserNotifications(principal.getName());

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getNotificationThroughProcessInst")
	public ApiResponse getNotificationThroughProcessInst(String process_id) throws Exception {
		return workFlowUserService.getNotificationThroughProcessInst(process_id);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/updateUserInfo")
	public ResponseEntity<Object> updateUserInfo(Principal principal,
			@Valid @RequestBody WFUserRequest request, Errors errors) {
		if (errors.hasErrors()) {

			ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST,
					" request is invalid : "
							+ errors.getFieldError().getField() + " "
							+ errors.getFieldError().getDefaultMessage());
			return CommonHelperFunctions.buildResponseEntity(apiResponse);
		}
		return CommonHelperFunctions.buildResponseEntity(workFlowUserService
				.updateUserInfo(principal.getName(), request));

	}

	@RequestMapping(method = RequestMethod.POST, value = "/addDeviceInfo")
	public ResponseEntity<Object> addDeviceInfo(
			@RequestBody DeviceInfoInpuForm deviceInfoInputForm) {
		return CommonHelperFunctions.buildResponseEntity(workFlowUserService
				.addDeviceInfo(deviceInfoInputForm));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getDataFromMaster/{slug}")
	public ResponseEntity<Object> getDataFromMasterGET(
			@PathVariable(value = "slug") String slug) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(workFlowUserService
							.getDataFromMasters(slug));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(
					HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getDataFromMaster/{slug}")
	public ResponseEntity<Object> getDataFromMasterPOST(
			@RequestBody ProductRequest productRequest,
			@PathVariable(value = "slug") String slug) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(workFlowUserService
							.getDataFromMasters(productRequest, slug));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(
					HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/view-pdf")
	public ResponseEntity<Object> viewPdf(
			@Valid @RequestBody ViewPdfForm viewPdf) {
		try {
			return CommonHelperFunctions
					.buildResponseEntity(workFlowUserService
							.viewPdf(viewPdf));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(
					HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/view-pdf-no-auth")
	public ResponseEntity<Object> viewTermsAndCondition() {
		try {
			ViewPdfForm viewPdf=new ViewPdfForm();
			viewPdf.setDmsDocId(wfImplConfig.getTermsAndCondDocId());
			return CommonHelperFunctions
					.buildResponseEntity(workFlowUserService
							.viewPdf(viewPdf));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(
					HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "/getPolicyDetails")
	public ApiResponse getPolicyDetails(Principal principal,
			@RequestBody PolicyDetails policyDetails) throws Exception {
		return workFlowUserService.getPolicyDetails(principal, policyDetails);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateNotification")
	public ResponseEntity<Object> updateNotification(Principal principal,
			@RequestBody UpdateNotification updateNotification) {
		return CommonHelperFunctions.buildResponseEntity(workFlowUserService
				.updateNotification(principal.getName(), updateNotification));

	}

	@RequestMapping(method = RequestMethod.GET, value = "/getCifInfo")
	public ApiResponse getCifBasicInfo(@RequestParam String nationalId,
			@RequestParam String phoneNumber) throws Exception {
		return feCreditBancaCIFCustomerSearchListService.cifInfoBasic(
				nationalId, phoneNumber);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/update-dms")
	public ResponseEntity<Object> updateDmsDocForAppIdAndPdfLink(@Valid @RequestBody UpdateDmsRequest updateDmsRequest) {
		try {
			return CommonHelperFunctions.buildResponseEntity(workFlowUserService.updateDmsDocForAppIdAndPdfLink(updateDmsRequest));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(
					HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/update-mobile")
	public ResponseEntity<Object> updateMobile(Principal principal,@RequestParam String mobile) throws Exception {
			return CommonHelperFunctions.buildResponseEntity(workFlowUserService.updateMobile(principal.getName(),mobile));
	}
}
