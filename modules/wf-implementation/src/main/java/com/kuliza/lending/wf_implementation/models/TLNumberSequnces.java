package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

@Entity
@Table(name="tl_number_sequences")
public class TLNumberSequnces{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@OrderBy(clause = "id ASC")
	private Integer id;
	
	@Column(name="illustration_number")
	private Long illustrationNumber;
	
	@Column(name="application_number")
	private Long applicationNumber;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getIllustrationNumber() {
		return illustrationNumber;
	}

	public void setIllustrationNumber(Long illustrationNumber) {
		this.illustrationNumber = illustrationNumber;
	}

	public Long getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(Long applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

}
