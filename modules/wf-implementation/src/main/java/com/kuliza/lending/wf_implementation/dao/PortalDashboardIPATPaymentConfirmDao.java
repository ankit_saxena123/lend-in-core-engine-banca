/**
 * 
 */
package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;

/**
 * @author garun
 *
 */
@Repository
public interface PortalDashboardIPATPaymentConfirmDao extends PagingAndSortingRepository<IPATPaymentConfirmModel, Long>, CrudRepository<IPATPaymentConfirmModel, Long> {
	//portal dashboard
		public List<IPATPaymentConfirmModel> findAllByIsDeleted(Pageable paging, boolean isDeleted);
		public List<IPATPaymentConfirmModel> findAllByIsDeleted(boolean isDeleted);
		public IPATPaymentConfirmModel findById(long workFlowApp);
		public List<IPATPaymentConfirmModel> findByWfApplicationAndIsDeleted(WorkFlowApplication workFlowApp,  boolean isDeleted);

}
