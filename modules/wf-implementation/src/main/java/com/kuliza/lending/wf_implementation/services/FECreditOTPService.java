package com.kuliza.lending.wf_implementation.services;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import com.kuliza.lending.wf_implementation.integrations.FECreditBancaOTPGenerateIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaOTPValidateIntegration;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service
public class FECreditOTPService {

	@Autowired
	private FECreditBancaOTPGenerateIntegration feCreditBencaOTPGenerateIntegration;

	@Autowired
	private FECreditBancaOTPValidateIntegration feCreditBencaOTPValidationIntegration;

	private static final Logger logger = LoggerFactory
			.getLogger(FECreditOTPService.class);

	private LoadingCache<String, Integer> otpGenerationCache;
	private LoadingCache<String, Integer> otpValidationCache;
	private LoadingCache<String, Boolean> otpAlreadySendCache;
	private static final Integer EXPIRE_MINS = 30;
	private static final Integer MAX_COUNT = 10;

	public FECreditOTPService() {
		super();
		otpGenerationCache = CacheBuilder.newBuilder()
				.expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES)
				.build(new CacheLoader<String, Integer>() {
					public Integer load(String key) {
						return 0;
					}
				});

		otpValidationCache = CacheBuilder.newBuilder()
				.expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES)
				.build(new CacheLoader<String, Integer>() {
					public Integer load(String key) {
						return 0;
					}
				});

		otpAlreadySendCache = CacheBuilder.newBuilder()
				.expireAfterWrite(60, TimeUnit.SECONDS)
				.build(new CacheLoader<String, Boolean>() {
					public Boolean load(String key) {
						return false;
					}
				});

	}

	public String generateOTP(UserPasswordGenerationForm userForm) {
		try {
			// IOS app review purpose
			if (userForm.getMobile().equals(Constants.NUMBER)) {
				return Constants.SUCCESS_MESSAGE;
			}
			Map<String, Object> requestMap = new HashMap<String, Object>();
			requestMap.put(Constants.MOBILE_NUMBER_KEY, userForm.getMobile());
			JSONObject requestPayload = feCreditBencaOTPGenerateIntegration
					.buildRequestPayload(requestMap);

			logger.info("Request Payload" + requestPayload);
			HttpResponse<JsonNode> response = feCreditBencaOTPGenerateIntegration
					.getDataFromService(requestPayload);
			logger.info("code" + response.getStatus() + " "
					+ response.getBody());
			if (response != null
					&& response.getStatus() == Constants.SUCCESS_CODE) {
				JSONObject jsonResponse = new JSONObject(response.getBody()
						.toString());
				Map<String, Object> data = feCreditBencaOTPGenerateIntegration
						.parseResponse(jsonResponse);
				if (data.containsKey(Constants.FE_CREDIT_OTP_GENERATION_SUCCESS_STATUS_KEY)
						&& Utils.getBooleanValue(data
								.get(Constants.FE_CREDIT_OTP_GENERATION_SUCCESS_STATUS_KEY))) {
					return Constants.SUCCESS_MESSAGE;
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}

	}

	public boolean validateOTP(String mobile, String otp) throws Exception {
		Map<String, Object> requestMap = new HashMap<String, Object>();
		// IOS app review purpose
		if (mobile.equals(Constants.NUMBER)) {
			return true;
		}
		requestMap.put(Constants.MOBILE_NUMBER_KEY, mobile);
		requestMap.put(Constants.FE_CREDIT_MVP_OTP_KEY, otp);
		JSONObject requestPayload = feCreditBencaOTPValidationIntegration
				.buildRequestPayload(requestMap);
		logger.info("Validate OTP payload : " + requestPayload);
		HttpResponse<JsonNode> response = feCreditBencaOTPValidationIntegration
				.getDataFromService(requestPayload);
		logger.info("Validate OTP response : " + response.getStatus() + "   "
				+ response.getBody());
		if (response != null && response.getStatus() == Constants.SUCCESS_CODE) {
			JSONObject jsonResponse = response.getBody().getObject();
			if (jsonResponse.has("NS1:ValidateOTP_Response")) {
				logger.info("<====Inside NS1:ValidateOTP_Response =========>");
				JSONObject json = jsonResponse
						.getJSONObject("NS1:ValidateOTP_Response");
				int status = json.getInt("status");
				if (status == 0) {
					logger.info("<=======>Success<========>");
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public boolean isOtpAlreadyRequested(LoadingCache<String, Boolean> cache,
			HttpServletRequest request, String contactNumber, String type) {
		Boolean flag = false;
		try {
			flag = cache.get("0000" + "#" + type + contactNumber);
		} catch (ExecutionException e) {
			flag = false;
		}

		if (flag == false) {
			cache.put("0000" + "#" + type + contactNumber, true);
			return false;
		}
		return true;
	}

}