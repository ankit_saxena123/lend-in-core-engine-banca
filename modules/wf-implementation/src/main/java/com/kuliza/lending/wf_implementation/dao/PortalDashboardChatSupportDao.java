/**
 * 
 */
package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.CustomerQuerySupport;

/**
 * @author Garun Mishra
 *
 */
@Repository
public interface PortalDashboardChatSupportDao extends PagingAndSortingRepository<CustomerQuerySupport, Long>, CrudRepository<CustomerQuerySupport, Long> {
	
	public List<CustomerQuerySupport> findAllByIsDeleted(Pageable paging, boolean isDeleted);

	public List<CustomerQuerySupport> findAllByticketNumberStatusContainingIgnoreCase(Pageable paging, String status);
//	public List<CustomerQuerySupport> findAllByIsDeleted(Pageable paging, boolean isDeleted);
	public List<CustomerQuerySupport> findAllByIsDeleted(boolean isDeleted);
	public List<CustomerQuerySupport> findAllByticketNumberStatusContainingIgnoreCase(String status);
	//Search Option
	public List<CustomerQuerySupport> findByUsernameContainingIgnoreCase(String searchInputValue);
	public List<CustomerQuerySupport> findByCustomerMobileNumberContainingIgnoreCase(String searchInputValue);
	public List<CustomerQuerySupport> findByTicketNumberContainingIgnoreCase(String searchInputValue);
	public List<CustomerQuerySupport> findByCustomerEmailContainingIgnoreCase(String searchInputValue);
	public List<CustomerQuerySupport> findByticketNumberStatusContainingIgnoreCase(String searchInputValue);
	public List<CustomerQuerySupport> findByCustomerQueryTopicDescriptionContainingIgnoreCase(String searchInputValue);
	public List<CustomerQuerySupport> findByticketNumber(String ticket_number);
	public List<CustomerQuerySupport> findByticketNumberAndTicketNumberStatusContainingIgnoreCase(String ticket_number, String status);
	
}
