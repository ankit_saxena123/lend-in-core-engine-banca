package com.kuliza.lending.wf_implementation.pojo;

import java.util.List;

import javax.validation.constraints.NotNull;

public class ConfigInputList {
	
	@NotNull
	List<ConfigInput> configInputList;
	
	public ConfigInputList() {
		super();
	}
	
	public ConfigInputList(List<ConfigInput> configInputList) {
		super();
		this.configInputList = configInputList;
	}
	
	public List<ConfigInput> getConfigInputList() {
		return this.configInputList;
	}

	public void setConfigInputList(List<ConfigInput> configInputList) {
		this.configInputList = configInputList;
	}
}
