package com.kuliza.lending.wf_implementation.pojo;

public class NotificationParamForm {

	String topic;
	
	String title;
	
	String body;
		
	String subscriberArn;

	public String getSubscriberArn() {
		return subscriberArn;
	}

	public void setSubscriberArn(String subscriberArn) {
		this.subscriberArn = subscriberArn;
	}

	public NotificationParamForm() {
		super();
	}

	public NotificationParamForm(String topic, String title,
			String body, String subscriberArn) {
		super();
		this.topic = topic;
		this.title = title;
		this.body = body;
		this.subscriberArn = subscriberArn;
		
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
