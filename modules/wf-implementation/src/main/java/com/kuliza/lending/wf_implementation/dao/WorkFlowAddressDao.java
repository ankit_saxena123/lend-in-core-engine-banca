package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;

@Repository
public interface WorkFlowAddressDao extends CrudRepository<WorkFlowUserAddress, Long>{
	
	public List<WorkFlowUserAddress> findByWfUser(WorkFlowUser user);

}
