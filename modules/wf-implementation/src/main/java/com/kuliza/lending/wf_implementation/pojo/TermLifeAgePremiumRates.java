package com.kuliza.lending.wf_implementation.pojo;

public class TermLifeAgePremiumRates {
	
	private Double premiumRate;
	private Double ppdRate;
	private Double criticalRate;
	
	public TermLifeAgePremiumRates(Double premiumRate, Double ppdRate, Double criticalRate) {
		super();
		this.premiumRate = premiumRate;
		this.ppdRate = ppdRate;
		this.criticalRate = criticalRate;
	}
	public Double getPremiumRate() {
		return premiumRate;
	}
	public Double getPpdRate() {
		return ppdRate;
	}
	public Double getCriticalRate() {
		return criticalRate;
	}
	public void setPremiumRate(Double premiumRate) {
		this.premiumRate = premiumRate;
	}
	public void setPpdRate(Double ppdRate) {
		this.ppdRate = ppdRate;
	}
	public void setCriticalRate(Double criticalRate) {
		this.criticalRate = criticalRate;
	}
	@Override
	public String toString() {
		return "AgePremium [premiumRate=" + premiumRate + ", ppdRate=" + ppdRate + ", criticalRate=" + criticalRate
				+ "]";
	}
	
	

}
