/**
 * 
 */
package com.kuliza.lending.wf_implementation.services;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakConfig;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.config.keycloak.OpenIdService;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.PortalClaimInfoDao;
import com.kuliza.lending.wf_implementation.dao.PortalDashboardChatSupportDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.CustomerQuerySupport;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.PortalDashboardAuthenticationConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;

/**
 * @author Garun Mishra
 * @Description This service contains all services which is need to Chat box
 *              support w.r.t customer representative.
 *
 */

@Service
public class PortalDashboardChatSupportService {

	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private WorkFlowAddressDao workFlowAddressDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	PortalClaimInfoDao portalClaimInfoDao;

	@Autowired
	WorkflowUserDao workflowUserDao;

	@Autowired
	PortalDashboardChatSupportDao portalDashboardChatSupportDao;

	@Autowired
	KeyCloakService keyCloakService;

	private UsersResource usersResource;

	private static final Logger logger = LoggerFactory.getLogger(PortalDashboardChatSupportService.class);

	private KeyCloakConfig keyCloakConfig;
	private Keycloak keycloak;
	private OpenIdService openIdService;
	private ClientRepresentation clientRepresentation;
	private RealmResource realmResource;
	private GroupsResource groupsResource;
	private ClientsResource clientsResource;
//	private UsersResource usersResource;

	// Initialize the keyclock config
	public PortalDashboardChatSupportService(KeyCloakConfig keyCloakConfig) {
		this.keyCloakConfig = keyCloakConfig;
		this.buildKeyCloak();
		this.buildOpenIdService();
		this.buildResources();
		this.buildClientRepresentation();
	}

	private void buildKeyCloak() {
		this.keycloak = KeycloakBuilder.builder().serverUrl(keyCloakConfig.getAuthEndpoint())
				.realm(keyCloakConfig.getRealm()).grantType(OAuth2Constants.PASSWORD)
				.clientId(keyCloakConfig.getClientId()).clientSecret(keyCloakConfig.getClientSecret())
				.username(keyCloakConfig.getAdminUsername()).password(keyCloakConfig.getAdminPassword()).build();
	}

	private void buildOpenIdService() {
		this.openIdService = this.keycloak.proxy(OpenIdService.class,
				URI.create(this.keyCloakConfig.getAuthEndpoint()));
	}

	private void buildResources() {
		this.realmResource = this.keycloak.realm(this.keyCloakConfig.getRealm());
		this.groupsResource = this.realmResource.groups();
		this.clientsResource = this.realmResource.clients();
		this.usersResource = this.realmResource.users();
	}

	private void buildClientRepresentation() {
		this.clientRepresentation = this.clientsResource.findByClientId(this.keyCloakConfig.getClientId()).get(0);
	}

	public ApiResponse getAllNewAndOpenTickets(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardChatSupportService.getAllNewAndOpenTickets()");
		Set<Map<String, Object>> response = new HashSet<>();
		List<CustomerQuerySupport> countRecords = portalDashboardChatSupportDao.findAllByIsDeleted(false);
		Map<String, Object> totalRecords = new HashMap<String, Object>();
		int countResponse = 0;
		boolean processContnue = true;
		boolean flag = false;
		while (processContnue) {
			if (countResponse >= 0 && countResponse < 10) {
				Pageable paging = new PageRequest(pageNo, pageSize);
				List<CustomerQuerySupport> allTickets = portalDashboardChatSupportDao.findAllByIsDeleted(paging, false);
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
				if (allTickets.size() > 0) {
					for (CustomerQuerySupport allTicket : allTickets) {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						String newTicket = Utils.getStringValue(allTicket.getTicketNumberStatus());
						if ("NEW".equals(newTicket) || "OPEN".equals(newTicket)) {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TICKET_NUMBER,
									Utils.getStringValue(allTicket.getTicketNumber()));
							String customerAction = "getTicketConversationDetails/"
									+ Utils.getStringValue(allTicket.getTicketNumber());
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
									Utils.getStringValue(allTicket.getUsername()));
							workFlowUserMap.put(
									PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO,
									Utils.getStringValue(allTicket.getCustomerMobileNumber()));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_EMAIL,
									Utils.getStringValue(allTicket.getCustomerEmail()));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TOPIC,
									Utils.getStringValue(allTicket.getCustomerQueryTopicDescription()));
							workFlowUserMap.put(
									PortalDashboardAuthenticationConstants.CUSTOMERCUSTOMER_SUPPORT_TICKET_STATUS,
									Utils.getStringValue(allTicket.getTicketNumberStatus()));

							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_LAST_UPDATE,
									Utils.getStringValue(""));
							workFlowUserMap.put(Constants.ACTION, customerAction);
							response.add(workFlowUserMap);
						}
					}
				}else {
					processContnue = false;
					flag = true;
				}
				countResponse = response.size();
				pageNo = pageNo + 1;
				continue;
			} else {
				flag = true;
				logger.info("Custom pagination is successful implemented");
				break;
			}
		}
		if(flag) {
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(countRecords.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardChatSupportService.getAllNewAndOpenTickets()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}	
	}

	public ApiResponse getAllClosedTickets(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardChatSupportService.getAllNewAndOpenTickets()");
		Set<Map<String, Object>> response = new HashSet<>();
		List<CustomerQuerySupport> countRecords = portalDashboardChatSupportDao.findAllByticketNumberStatusContainingIgnoreCase("CLOSED");
		Map<String, Object> totalRecords = new HashMap<String, Object>();
		int countResponse = 0;
		boolean processContnue = true;
		boolean flag = false;
		while (processContnue) {
			if (countResponse >= 0 && countResponse < 10) {
				Pageable paging = new PageRequest(pageNo, pageSize);
				List<CustomerQuerySupport> allTickets = portalDashboardChatSupportDao.findAllByIsDeleted(paging, false);
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
				if (allTickets.size() > 0) {
					for (CustomerQuerySupport allTicket : allTickets) {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						String newTicket = Utils.getStringValue(allTicket.getTicketNumberStatus());

						if ("CLOSED".equals(newTicket)) {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TICKET_NUMBER,
									Utils.getStringValue(allTicket.getTicketNumber()));
							String customerAction = "getTicketConversationDetails/"
									+ Utils.getStringValue(allTicket.getTicketNumber());
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
									Utils.getStringValue(allTicket.getUsername()));
							workFlowUserMap.put(
									PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO,
									Utils.getStringValue(allTicket.getCustomerMobileNumber()));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_EMAIL,
									Utils.getStringValue(allTicket.getCustomerEmail()));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TOPIC,
									Utils.getStringValue(allTicket.getCustomerQueryTopicDescription()));
							workFlowUserMap.put(
									PortalDashboardAuthenticationConstants.CUSTOMERCUSTOMER_SUPPORT_TICKET_STATUS,
									Utils.getStringValue(allTicket.getTicketNumberStatus()));

							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_LAST_UPDATE,
									Utils.getStringValue(""));
							workFlowUserMap.put(Constants.ACTION, customerAction);
							response.add(workFlowUserMap);

						}
					}
				}else {
					processContnue = false;
					flag = true;
				}

			
				countResponse = response.size();
				
				pageNo = pageNo + 1;
				continue;
			} else {
				flag = true;
				logger.info("Custom pagination is successful implemented");
				break;
			}
		}
		if(flag) {
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(countRecords.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardChatSupportService.getAllNewAndOpenTickets()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}	
	}

	public ApiResponse getAllResolvedTickets(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardChatSupportService.getAllNewAndOpenTickets()");
		Set<Map<String, Object>> response = new HashSet<>();
		List<CustomerQuerySupport> countRecords = portalDashboardChatSupportDao.findAllByticketNumberStatusContainingIgnoreCase("RESOLVED");
		Map<String, Object> totalRecords = new HashMap<String, Object>();
		int countResponse = 0;
		boolean processContnue = true;
		boolean flag = false;
		while (processContnue) {
			if (countResponse >= 0 && countResponse < 10) {
				Pageable paging = new PageRequest(pageNo, pageSize);
				List<CustomerQuerySupport> allTickets = portalDashboardChatSupportDao.findAllByIsDeleted(paging, false);
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
				if (allTickets.size() > 0) {

					for (CustomerQuerySupport allTicket : allTickets) {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						String newTicket = Utils.getStringValue(allTicket.getTicketNumberStatus());

						if ("RESOLVED".equals(newTicket)) {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TICKET_NUMBER,
									Utils.getStringValue(allTicket.getTicketNumber()));
							String customerAction = "getTicketConversationDetails/"
									+ Utils.getStringValue(allTicket.getTicketNumber());
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
									Utils.getStringValue(allTicket.getUsername()));
							workFlowUserMap.put(
									PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO,
									Utils.getStringValue(allTicket.getCustomerMobileNumber()));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_EMAIL,
									Utils.getStringValue(allTicket.getCustomerEmail()));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TOPIC,
									Utils.getStringValue(allTicket.getCustomerQueryTopicDescription()));
							workFlowUserMap.put(
									PortalDashboardAuthenticationConstants.CUSTOMERCUSTOMER_SUPPORT_TICKET_STATUS,
									Utils.getStringValue(allTicket.getTicketNumberStatus()));

							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_LAST_UPDATE,
									Utils.getStringValue(""));
							workFlowUserMap.put(Constants.ACTION, customerAction);
							response.add(workFlowUserMap);

						}
					}
				}else {
					processContnue = false;
					flag = true;
				}
				countResponse = response.size();
				pageNo = pageNo + 1;
				continue;
			} else {
				flag = true;
				logger.info("Custom pagination is successful implemented");
				break;
			}
		}
		if(flag) {
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(countRecords.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardChatSupportService.getAllNewAndOpenTickets()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}	
	}

	public ApiResponse saveCSRResponse(String ticket_number, String csr_response, String ticket_status,
			String csruser_email) {
		logger.info("-->Entering PortalDashboardChatSupportService.saveCSRResponse()");
		boolean responseSaveFlag = false;
		try {
			Date currentDatTime = new Date();
			List<UserRepresentation> users = usersResource.search(csruser_email);
			String csrUserName = "";
			String csrUserRole = "";
			String portalMessageThread = "";
			String topicDesc = "";
			long topicId = 0;
			if (users.size() > 0) {
				for (UserRepresentation user : users) {
					csrUserName = Utils.getStringValue(user.getFirstName()) + " "
							+ Utils.getStringValue(user.getLastName());
				}
			}
			// portal message thread:
			List<CustomerQuerySupport> messageThreads = portalDashboardChatSupportDao.findByticketNumber(ticket_number);
			if (messageThreads.size() > 0) {
				for (CustomerQuerySupport messageThread : messageThreads) {
					if (messageThread.getCustomerRole().equals("CM_QUERY")) {
						String addMessage = "" + messageThread.getUsername() + " : "
								+ messageThread.getCustomerQueryMessage() + " \n ";
						portalMessageThread = portalMessageThread + addMessage;
					} else {
						String addMessage = "" + csrUserName + " : " + messageThread.getPortalQueryMessage() + " \n ";
						portalMessageThread = portalMessageThread + addMessage;
					}
					topicDesc = messageThread.getCustomerQueryTopicDescription();
					topicId = messageThread.getCustomerQueryTopicId();
				}
				// add the latest repliy to thread
				portalMessageThread = portalMessageThread + " \n  " + csrUserName + " : " + csr_response;
			} else {
				portalMessageThread = csrUserName + " : " + csr_response;
			}

			responseSaveFlag = savePortalResponse(topicId, topicDesc, ticket_number, csr_response, portalMessageThread,
					ticket_status, csrUserName, csruser_email);

		} catch (Exception ex) {
			String errorMessage = "Error while saving CSR response for ticket =>" + ticket_number;
			logger.error(errorMessage, ex);
			return new ApiResponse(HttpStatus.UNAUTHORIZED, "CSR_MESSAGE_SAVE_FAILED",
					PortalDashboardAuthenticationConstants.CHAT_SUPPORT_CSR_MESSAGE_SAVE_FAILED);
		}
		if (responseSaveFlag == true) {
			logger.info("<-- Exiting PortalDashboardService.saveCSRResponse()");
			return ViewTicketDetails(ticket_number);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, "CSR_MESSAGE_SAVE_FAILED",
					PortalDashboardAuthenticationConstants.CHAT_SUPPORT_CSR_MESSAGE_SAVE_FAILED);
		}
	}

	private boolean savePortalResponse(long topicId, String topicDesc, String ticket_number, String csr_response,
			String portalMessageThread, String ticket_status, String csrUserName, String csruser_email) {
		logger.info("<-- Entering PortalDashboardService.saveCSRResponse().savePortalResponse");
		CustomerQuerySupport customerQuerySupport = new CustomerQuerySupport();
		try {
			Date date = new Date();
			customerQuerySupport.setTicketNumber(ticket_number);
			customerQuerySupport.setTicketNumberStatus(ticket_status);
			customerQuerySupport.setPortalUserName(csrUserName);
			customerQuerySupport.setPortalQueryMessage(csr_response);
			customerQuerySupport.setPortalQueryMessageThread(portalMessageThread.toString());
			customerQuerySupport.setCustomerQueryTopicId(topicId);
			customerQuerySupport.setCustomerQueryTopicDescription(topicDesc);
			customerQuerySupport.setPortalUserRole("CSR_REPLIED");
			customerQuerySupport.setPortalUserEmailId(csruser_email);
			customerQuerySupport.setPortalQueryMessageDateTime(date);
			customerQuerySupport.setCustomerRole("");
			// pass customer related information as blank
			customerQuerySupport.setCustomerMobileNumber("");
			customerQuerySupport.setCustomerEmail("");
			customerQuerySupport.setCustomerMessageDateTime(null);
			customerQuerySupport.setCustomerNationalId("");
			customerQuerySupport.setCustomerQueryMessage("");
			customerQuerySupport.setCustomerQueryMessageThread("");
			customerQuerySupport.setUsername("");
			customerQuerySupport.setIdmUsername("");
			portalDashboardChatSupportDao.save(customerQuerySupport);
			logger.info("<-- Entering PortalDashboardService.saveCSRResponse().savePortalResponse");
			return true;
		} catch (Exception e) {
			String errorMessage = "Error while saving recirds in savePortalResponse";
			logger.error(errorMessage, e);
			return false;
		}
	}

	public ApiResponse ViewTicketDetails(String ticket_number) {
		logger.info("-->Entering PortalDashboardChatSupportService.ViewTicketDetails()");
		List<Map<String, Object>> allMessageDetailsList = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy");
		DateFormat timeFormat = new SimpleDateFormat("hh:mm a");
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		List<CustomerQuerySupport> ticketDetails = portalDashboardChatSupportDao.findByticketNumber(ticket_number);
		String idmUserName = "";
		if (ticketDetails.size() > 0) {
			int messageSequesnceNumber = 1;
			for (CustomerQuerySupport ticketDetail : ticketDetails) {
				Map<String, Object> workFlowUserMapCustomerDetails = new HashMap<String, Object>();
				if (ticketDetail.getIdmUsername() != "") {
					idmUserName = ticketDetail.getIdmUsername();
				}

				String messageIdentifier = ticketDetail.getCustomerRole().trim();

				if (messageIdentifier.equals("CM_QUERY")) {
					Date messageDateandTime = ticketDetail.getCustomerMessageDateTime();
					workFlowUserMapCustomerDetails.put(
							PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_IDENTIFIER,
							Utils.getStringValue(ticketDetail.getCustomerRole()));
					workFlowUserMapCustomerDetails.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE,
							Utils.getStringValue(ticketDetail.getCustomerQueryMessage()));
					workFlowUserMapCustomerDetails.put(
							PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
							Utils.getStringValue(ticketDetail.getUsername()));

					if (messageDateandTime != null) {
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_DATE,
								Utils.getStringValue(dateFormat.format(messageDateandTime)));
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_TIME,
								Utils.getStringValue(timeFormat.format(messageDateandTime)));
					} else {
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_DATE,
								Utils.getStringValue(""));
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_TIME,
								Utils.getStringValue(""));
					}

				} else {

					workFlowUserMapCustomerDetails.put(
							PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_IDENTIFIER,
							Utils.getStringValue(ticketDetail.getCustomerRole()));
					workFlowUserMapCustomerDetails.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE,
							Utils.getStringValue(ticketDetail.getPortalQueryMessage()));
					workFlowUserMapCustomerDetails.put(
							PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
							Utils.getStringValue(ticketDetail.getPortalUserName()));
					Date messageDateandTime = ticketDetail.getPortalQueryMessageDateTime();
					if (messageDateandTime != null) {
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_DATE,
								Utils.getStringValue(dateFormat.format(messageDateandTime)));
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_TIME,
								Utils.getStringValue(timeFormat.format(messageDateandTime)));
					} else {
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_DATE,
								Utils.getStringValue(""));
						workFlowUserMapCustomerDetails.put(
								PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_MESSAGE_TIME,
								Utils.getStringValue(""));
					}

				}
				workFlowUserMapCustomerDetails.put("message_sequence_number", messageSequesnceNumber);
				allMessageDetailsList.add(workFlowUserMapCustomerDetails);
				messageSequesnceNumber += 1;
			}

			WorkFlowUser userDetail = workflowUserDao.findByIdmUserNameAndIsDeleted(idmUserName, false);
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_ID,
					Utils.getStringValue(userDetail.getId()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
					Utils.getStringValue(userDetail.getUsername()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO,
					Utils.getStringValue(userDetail.getMobileNumber()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_EMAIL,
					Utils.getStringValue(userDetail.getEmail()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TICKET_NUMBER,
					Utils.getStringValue(ticket_number));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_ALL_MESSAGE,
					allMessageDetailsList);

			logger.info("-->Entering PortalDashboardChatSupportService.ViewTicketDetails()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}
	/**
	 * 
	 * @param ticket_number
	 * @return ApiResponse
	 * @Description As it's bit dificult to maintain pagination with filter. So
	 *              Putting down logic tomaintains
	 * 
	 */

//	public ApiResponse generateResponseToMaintainPagination(Integer pageNo, Integer pageSize)) {
//		for (CustomerQuerySupport allTicket : allTickets) {
//			Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
//			String newTicket = Utils.getStringValue(allTicket.getTicketNumberStatus());
//
//			if ("NEW".equals(newTicket) || "OPEN".equals(newTicket)) {
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TICKET_NUMBER,
//							Utils.getStringValue(allTicket.getTicketNumber()));
//					String customerAction = "getTicketConversationDetails/"
//							+ Utils.getStringValue(allTicket.getTicketNumber());
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
//							Utils.getStringValue(allTicket.getUsername()));
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO,
//							Utils.getStringValue(allTicket.getCustomerMobileNumber()));
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_EMAIL,
//							Utils.getStringValue(allTicket.getCustomerEmail()));
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TOPIC,
//							Utils.getStringValue(allTicket.getCustomerQueryTopicDescription()));
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMERCUSTOMER_SUPPORT_TICKET_STATUS,
//							Utils.getStringValue(allTicket.getTicketNumberStatus()));
//
//					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_LAST_UPDATE,
//							Utils.getStringValue(""));
//					workFlowUserMap.put(Constants.ACTION, customerAction);
//					response.add(workFlowUserMap);
//				}
//		}
//		
//		
//	}

}
