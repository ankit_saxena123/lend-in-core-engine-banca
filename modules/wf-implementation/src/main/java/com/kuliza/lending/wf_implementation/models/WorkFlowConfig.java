package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "wf_config")
public class WorkFlowConfig extends BaseModel {

	@Column(nullable = false)
	private String name;

	@Column(nullable = true)
	private String value;

	public WorkFlowConfig() {
		this.setIsDeleted(false);
	}
	
	public WorkFlowConfig(String name, String value) {
		this.name = name;
		this.value = value;
		this.setIsDeleted(false);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
