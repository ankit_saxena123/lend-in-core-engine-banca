package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.offline_policy_migration.service.OfflinePolicyDetailService;

@RestController
@RequestMapping(value = "/offline")
public class OfflinePolicyDetailController {

	@Autowired
	OfflinePolicyDetailService offlinePolicyDetailService;

	private final Logger logger = LoggerFactory.getLogger(OfflinePolicyDetailController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/saveOfflinePolicyDetails")
	public ResponseEntity<Object> saveOfflinePolicyDetails() {
		logger.info("saveOfflinePolicyDetails Controller--> : ");
		try {
			return CommonHelperFunctions.buildResponseEntity(
					offlinePolicyDetailService.saveOfflinePolicyDetails());
		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getOfflinePolicyDetails")
	public ResponseEntity<Object> getOfflinePolicyDetails(Principal principal, @RequestParam String mobileNumber) {
		logger.info("mobileNumber--> : "+mobileNumber);
		try {
			return CommonHelperFunctions.buildResponseEntity(
					offlinePolicyDetailService.getOfflinePolicyDetails(principal.getName(), mobileNumber));
		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}

}
