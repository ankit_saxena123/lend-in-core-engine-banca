package com.kuliza.lending.wf_implementation.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationComponent {

	@Value("${otp.validateDefaultOTP}")
	private boolean validateOTP;

	public boolean isValidateOTP() {
		return validateOTP;
	}

	public void setValidateOTP(boolean validateOTP) {
		this.validateOTP = validateOTP;
	}

}
