package com.kuliza.lending.wf_implementation.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.kuliza.lending.wf_implementation.pojo.TermLifeAgePremiumRates;
import com.kuliza.lending.wf_implementation.utils.Constants.Products;
import com.kuliza.lending.wf_implementation.pojo.PlanPremiums;

public class Constants {

	public static final String NUMBER = "9999999999";
	public static final String CONTENT_TYPE_KEY = "Content-Type";
	public static final String CONTENT_TYPE = "application/json";

	public static final String GENERATE_OTP_SLUG = "generateotp";
	public static final String VALIDATE_OTP_SLUG = "validateotp";
	public static final String SEND_SMS_METHOD = "sendsms";
	public static final String GENERATE_VALIDATE_OTP_INTEGRATION_SLUG = "soap_banca_otp_integrations";
	public static final String SMS_INTEGRATION_SLUG = "soap_banca_sms_integration";
	public static final String OTP_INTEGRATION_SOAP_KEY = "&binding=SRV_OTPSOAP";
	public static final String SMS_INTEGRATION_SOAP_KEY = "&binding=SMSHttpBinding";

	public static final String SOAP_SLUG_KEY = "soap/";
	public static final String BANCA_BLACKLIST_INTEGRATION_SLUG_KEY = "banca_blacklist";
	public static final String BANCA_BLACK_LIST_SLUG_KEY = "checkblacklist";
	public static final String BANCA_BLACK_LIST_SOAP_KEY = "&binding=APS_BlackListHttpBinding";
	public static final String FEC_CREDIT_BLACK_LIST_SEQUENCE_KEY = "1";
	public static final String FEC_CREDIT_BLACK_LIST_PERSON_TYPE = "C";
	public static final String FE_CREDIT_MVP_SMS_MOBILE_KEY = "mobile";
	public static final String FE_CREDIT_MVP_SMS_MESSAGE_KEY = "message";
	public static final String FE_CREDIT_MVP_OTP_KEY = "otp";
	public static final String FEC_REQUESTOR_ID = "SHIELD";
	public static final String FEC_REQUESTOR_ID_OTP = "Shield";
	public static final String FEC_SMS_REQUESTOR_ID = "Shield";
	public static final String FEC_CIF_INFO_REQUESTOR_ID = "ROBO";
	public static final String CIF_NUMBER_KEY = "cifNumberKey";
	public static final String CIF_BASIC_INFO_STATUS_KEY = "cifBasicInfoStatusKey";
	public static final String CIF_FULL_NAME_KEY = "cifFullNameKey";
	public static final String CIF_FIRST_NAME_KEY = "cifFirstNameKey";
	public static final String CIF_MIDDLE_NAME_KEY = "cifMiddleNameKey";
	public static final String CIF_LAST_NAME_KEY = "cifLastNameKey";
	public static final String CIF_TITLE_KEY = "cifTitleKey";
	public static final String CIF_BIRTHDAY_KEY = "cifBirthdayKey";
	public static final String CIF_GENDER_KEY = "cifGenderKey";
	public static final String CIF_CITIZENSHIP_KEY = "cifCitizenshipKey";
	public static final String CIF_CUSTOMER_STATUS_KEY = "cifCustomerStatusKey";
	public static final String CIF_SOCIAL_STATUS_KEY = "cifSocialStatusKey";
	public static final String CIF_SHORT_NAME_KEY = "cifShortNameKey";
	public static final String FE_CREDIT_CIF_BASIC_INFO_LAST_SUCCESS_DATE_KEY = "feCreditCustomerInfoLastSuccessDate";
	public static final String FE_CREDIT_CIF_BASIC_INFO_SUCCESS_STATUS_KEY = "feCreditCustomerInfoSuccessStatus";
	public static final String FE_CREDIT_CIF_BASIC_INFO_SUCCESS_CODE_KEY = "1";
	public static final String FEC_OTP_TTL = "240";
	public static final String FEC_OTP_TYPE = "NUMERIC";
	public static final String FEC_OTP_LENGTH = "4";
	public static final String FEC_OTP_ATTEMPTS = "2";
	public static final String FEC_TEXT_SUF = "la ma xac nhan ung dung FE SHIELD cua QK";
	public static final String FEC_TEXT_PREF = "";
	public static final String MOBILE_NUMBER_KEY = "mobileNumber";
	public static final String EMAIL_ADDRESS = "email_address";
	public static final String NOT_ZERO_NUMBER_REGEX = "^[1-9]{1}[0-9]*$";
	public static final String INVALID_PROCESS_INSTANCE_ID_MESSAGE = "INCORRECT PROCESS INSTANCE ID!";
	public static final String INVALID_TASK_ID_MESSAGE = "INCORRECT TASK ID!";
	public static final String TOO_MANY_TASKS = "This User Has Too Many Tasks Assigned";
	public static final String APPLICATION_ID_KEY = "applicationId";
	public static final String APPLICATION_NUMBER = "applicationNumber";
	public static final String INVALID_BACK_TASK_DEFINITION_KEY = "Invalid Back Task Definition Key";

	public static final String CIF_CUSTOMER_SEARCH_LIST_INTEGRATION_SLUG_KEY = "soap_banca_cif_cust_search_integrations";
	public static final String CIF_CUSTOMER_INFO_BASIC_INTEGRATION_SLUG_KEY = "soap_banca_cif_info_integrations";
	public static final String CIF_CUSTOMER_GET_CUSTOMER_LIST = "getcustomerlist";
	public static final String CIF_CUSTOMER_CUSTOMER_GET_INFO_BASIC = "getcustomerinfobasic";
	public static final String COMPANY_SLUG = "banca";
	public static final String IB_HASH_VALUE = "1234";
	public static final String CIF_CUSTOMER_SEARCH_LIST_SOAP_KEY = "&binding=CIF_CustomerSearchHttpBinding";
	public static final String CIF_CUSTOMER_INFO_BASIC__SAOP_KEY = "&binding=CIF_CustomerInfoHttpBinding";

	public static final String CIF_ADDRESS_TYPE_KEY = "cifAddressTypeKey";
	public static final String CIF_ADDRESS_NUMBER_KEY = "cifAddressNumberKey";
	public static final String CIF_ADDRESS_ID_KEY = "cifAddressIdKey";
	public static final String CIF_BUILDING_NUMBER_KEY = "cifBuildingNumberKey";
	public static final String CIF_STREET_KEY = "cifStreetKey";
	public static final String CIF_WARD_KEY = "cifWardKey";
	public static final String CIF_DISTRICT_CODE_KEY = "cifDistrictCodeKey";
	public static final String CIF_CITY_CODE = "cifCityCodeKey";
	public static final String CIF_COUNTRY_NAME_KEY = "cifCountryNameKey";
	public static final String CIF_COUNTRY_CODE_KEY = "cifCountryCodeKey";
	public static final String CIF_ZIP_CODE_KEY = "cifZipCodeKey";
	public static final String CIF_YEARS_KEY = "cifYearsKey";
	public static final String CIF_MONTHS_KEY = "cifMonthsKey";
	public static final String CIF_PROPERTY_STATUS_KEY = "cifPropertyStatusKey";

	public static final String FE_CREDIT_OTP_GENERATION_SUCCESS_CODE_KEY = "0";
	public static final String FE_CREDIT_OTP_GENERATION_SUCCESS_STATUS_KEY = "feCreditOTPGenerationSuccessStatus";
	public static final String FE_CREDIT_OTP_GENERATION_LAST_SUCCESS_DATE_KEY = "feCreditOTPGenerationLastSuccessDate";
	public static final String FE_CREDIT_MVP_SMS_STATUS_KEY = "feCreditmvpSMSStatusKey";
	public static final String FE_CREDIT_MVP_SMS_DESCRIPTION_KEY = "feCreditmvpSMSDescriptionKey";
	public static final String FE_CREDIT_MVP_NO_RESPONSE_MAP_KEY = "feCreditmvpSMSNoResponseKey";
	public static final String FE_CREDIT_MVP_NO_SEND_SMS_RESPONSE_MAP_KEY = "feCreditmvpSMSNoSendSmsResponseKey";
	public static final String FE_CREDIT_MVP_NO_RESPONSE_KEY_MESSAGE = "No Response Key In Response for FEC SMS";
	public static final String FE_CREDIT_MVP_NO_SEND_SMS_RESPONSE_KEY_MESSAGE = "No NS1:SendSMSResponse Key In Response for FEC SMS";
	public static final String FE_CREDIT_EXCEPTION_IN_API = "feCreditExceptionInApi";

	public static final String FE_CREDIT_SMS_SENT_SUCCESS_STATUS_KEY = "feCreditSMSSendingSuccessStatus";

	public static final String FE_CREDIT_OTP_VALIDATION_SUCCESS_CODE_KEY = "0";
	public static final String FE_CREDIT_MVP_OTP_VALIDATED_KEY = "feCreditmvpOTPValidatedKey";
	public static final String FE_CREDIT_OTP_VALIDATION_SUCCESS_STATUS_KEY = "feCreditOTPValidationSuccessStatus";
	public static final String FE_CREDIT_OTP_VALIDATION_LAST_SUCCESS_DATE_KEY = "feCreditOTPVAlidationLastSuccessDate";
	public static final String FE_CREDIT_MVP_OTP_VALIDATED_STATUS_KEY = "feCreditmvpOTPValidatedStatusKey";

	public static final String UNSUCCESS_MESSAGE = "UnSuccessful";
	public static final String EXCEPTION_MESSAGE = "Exception occured";

	public static final String SUCCESS_MESSAGE = "SUCCESS!";
	public static final String TRANSACTION_FAILED = "TRANSACTION FAIL!";
	public static final String FAILURE_MESSAGE = "FAILURE!";
	public static final String DATA_NOT_FOUND = "Data Not Found !";
	public static final String MASTER_DATA_NOT_FOUND = "Master Data Not Found";
	public static final String STATUS_MAP_KEY = "status";
	public static final String STATUS_MESSAGE = "messge";
	public static final String IS_BLACK_LISTED_KEY = "isBlackListed";
	public static final String DESCRIPTION_MAP_KEY = "description";
	public static final int SUCCESS_CODE = 200;
	public static final int FAILURE_CODE = 400;
	public static final int AUTHENTICATION_FAILURE_CODE = 401;
	public static final int INTERNAL_SERVER_ERROR_CODE = 500;
	public static final int INVALID_POST_REQUEST_DATA_ERROR_CODE = 422;
	public static final String SOMETHING_WRONG_MESSAGE = "SOMETHING WENT WRONG!";
	public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error Occured!!!";
	public static final String DEVICE_INFO_ADDED_MESSAGE = "Device Info Added Succesfully";

	public static final String IPAT_SHIELD_INTEGRATIONS = "ipat_shield_integrations";
	public static final String IPAT_UPDATE_LEAD_STATUS = "updateleadstatus";
	public static final String IPAT_SOAP_KEY = "&binding=VT_LeadStatusSOAP";

	public static final String IPAT_PAYMENT_CONFIRMATION_ADDED_MESSAGE = "IPAT Payment Confirmation Record Added Succesfully";
	public static final String IPAT_PAYMENT_INFO_ADDED_MESSAGE = "IPAT Payment Info Record Added Succesfully";
	public static final String USER_DOES_NOT_EXIST_MESSAGE = "User does not Exist";
	public static final String USER_UPDATED_SUCCESS_MESSAGE = "User is updated succesfully";
	public static final String NOTIFICATION_UPDATED_SUCCESS_MESSAGE = "Notification Updated Successfully";
	public static final String PAYMENTS_DETAILS_ADDED_MESSAGE = "Payment Details Added Successfully !";
	public static final String PAYMENT_DETAILS_NOT_FOUND = "Payment Details Not Found !";
	public static final String PAYMENTS_DETAILS_NOT_ADDED_MESSAGE = "Payment Details Not Added !";
	public static final String PAYMENTS_DETAILS_UPDATED_MESSAGE = "Payment Details Updated Successfully !";
	public static final String PAYMENTS_DETAILS_NOT_UPDATED_MESSAGE = "Payment Details Not Updated !";
	public static final String PAYMENT_DETAILS_DELETED_MESSAGE = "Payment Details Deleted Successfully !";
	public static final String PAYMENT_DETAILS_NOT_DELETED_MESSAGE = "Payment Details Doesnot Exist!";
	public static final String ACCOUNT_HOLDERS_NAME_MAP_KEY = "AccountHoldersName";
	public static final String BANK_NAME_MAP_KEY = "BankName";
	public static final String BRANCH_NAME_MAP_KEY = "BranchName";
	public static final String ACCOUNT_NUMBER_MAP_KEY = "AccountNumber";
	public static final String UUID_TRANSACTION_ID_KEY = "TransactionId";

	// Banca Constants
	public static final String BANCA_APP_NAME = "com.fecredit.Bancassurance";
	public static final String BANCA_PRODUCT_CODE_INS_KEY = "schemeId";
	public static final String BANCA_CAMPAIGN_ID = "331";
	public static final String BANCA_IO_STAGE = "Chờ KH thanh toán";
	public static final String BANCA_IO_REASON = "KH chưa thanh toán";
	public static final String BANCA_VT_Instance = "CL_BANCA";
	public static final String BANCA_VT_PRODUCT_ID = "BNCINS";
	public static final String BANCA_LEAD_STATUS = "Chờ KH thanh toán";
	public static final String BANCA_PAYMENT_CHANNEL = "0";
	public static final String BANCA_PAYMENT_DEF_PROCESS_FLAG = "NO";
	public static final String BANCA_BENEFITS_TEXT = "cardBenifits";
	public static final String BANCA_PRODUCT_DESCRIPTION_KEY = "cardType";
	public static final String BANCA_PRODUCT_MAX_SUM_ASSURED_KEY = "maxSumAssured";
	public static final String BANCA_PROODUCT_MIN_SUM_ASSURED_KEY = "minSumAssured";
	public static final String BANCA_PRODUCT_SUM_ASSURED_SLAB_KEY = "sumAssuredSlab";
	public static final String BANCA_PRODUCT_MAX_AGE_KEY = "maxAgeKey";
	public static final String BANCA_PRODUCT_MIN_AGE_KEY = "minAgeKey";
	public static final String BANCA_PRODUCT_MAX_TENOR_KEY = "maxTenorKey";
	public static final String BANCA_PRODUCT_MIN_TENOR_KEY = "minTenorKey";
	public static final String BANCA_PRODUCT_TENOR_SLAB_KEY = "tenorSlab";
	public static final String BANCE_PRODUCT_PREMIUM_TYPE_KEY = "premiumType";
	public static final String BANCA_PRODUCT_QUESTIONAIRE_KEY = "questionaireRequired";
	public static final String BANCA_PRODUCT_PREMIUM_KEY = "premium";
	public static final String BANCA_PRODUCT_URL_KEY = "cardImageURL";
	public static final String BANCA_LINK_LIST_KEY_ = "links";
	public static final String BANCA_PRODUCT_TnC_KEY = "Terms n Conditions";
	public static final String BANCA_LINK_LABEL = "label";
	public static final String BANCA_LINK_KEY = "url";
	public static final String BANCA_PRODUCT_BROCHURE_KEY = "Product Brochure";
	public static final String BANCA_PRODUCT_COMPENSATION_SURGERY_KEY = "Compensation Surgery Link";
	public static final String BANCA_PRODUCT_COMPENSATION_INJURY_KEY = "Compensation Injury Link";
	public static final String BANCA_PRODUCT_TnC_KEY_VN = "Nguyên tắc bảo hiểm";
	public static final String BANCA_PRODUCT_BROCHURE_KEY_VN = "Thông tin sản phẩm chi tiết";
	public static final String BANCA_PRODUCT_COMPENSATION_SURGERY_KEY_VN = "Tỷ lệ chi trả phẫu thuật";
	public static final String BANCA_PRODUCT_COMPENSATION_INJURY_KEY_VN = "Tỷ lệ chi trả thương tật";
	public static final String BANCA_PRODUCT_IMAGE_URL = "http://instabima.com/blogs/wp-content/uploads/2017/11/Health-Ins-Screenshot.png";
	public static final String BANCA_DEFAULT_LANGUAGE = "vi-VN";

	public static final String BANCA_GENDER_MALE = "MALE";
	public static final String BANCA_GENDER_FEMALE = "FEMALE";
	public static final String BANCA_PRODUCT_FINAL_PREMIUM_KEY = "finalPremium";
	public static final String BANCA_PRODUCT_FINAL_PREMIUM_VND_KEY = "finalPremiumVND";
	public static final String BANCA_PRODUCT_FINAL_SUM_ASSURANCE_KEY = "finalSumAssurance";
	public static final String BANCA_PRODUCT_FINAL_SUM_ASSURANCE_VND_KEY = "finalSumAssuranceVND";
	public static final String BANCA_PREMIUM_MALE_25MIL = "premiumMale25Mil";
	public static final String BANCA_PREMIUM_FEMALE_25MIL = "premiumFemale25Mil";
	public static final String BANCA_PREMIUM_MALE_50MIL = "premiumMale50Mil";
	public static final String BANCA_PREMIUM_FEMALE_50MIL = "premiumFemale50Mil";
	public static final String BANCA_PREMIUM_MALE_75MIL = "premiumMale75Mil";
	public static final String BANCA_PREMIUM_FEMALE_75MIL = "premiumFemale75Mil";
	public static final String BANCA_OPTIONAL_PREMIUM_25MIL = "optionalPremium25Mil";
	public static final String BANCA_OPTIONAL_PREMIUM_50MIL = "optionalPremium50Mil";
	public static final String BANCA_OPTIONAL_PREMIUM_75MIL = "optionalPremium75Mil";
	public static final String BANCA_NEW_USER_EMAIL_TEXT = "<html><body><p>Dear valued customer,</p><p></p><p>Thank you for downloading and registering to use SHIELD Application.</p><p></p><p>FE CREDIT&#39;s system has received your information and is processing your request. Within 24 hours, we will send a notification on your phone so that you can start to use the application.</p><p></p><p>If you have any concern, please contact hotline (working hour): (028) 3868 9968 or email: hotrobaohiem@fecredit.com.vn</p><p></p><p>Yours sincerely!</p></body></html>";
	public static final String BANCA_NEW_USER_EMAIL_SUBJECT = "[FE CREDIT] Notice of customer registration on SHIELD Application";
	public static final String BANCA_NEW_USER_EMAIL_SUBJECT_VN = "[FE CREDIT] THÔNG BÁO VỀ VIỆC ĐĂNG KÝ SỬ DỤNG ỨNG DỤNG SHIELD";
	public static final String BANCA_NEW_USER_EMAIL_TEXT_VN = "<html><body><p>Kính gửi Quý khách hàng,</p><p></p><p>Công ty Tài chính TNHH MTV Ngân hàng Việt Nam Thịnh Vượng (FE CREDIT) xin gửi lời cảm ơn chân thành đến Quý khách đã tải và đăng ký sử dụng ứng dụng SHIELD.</p><p></p><p>Hiện tại hệ thống của FE CREDIT đã nhận được thông tin đăng ký của Quý khách và đang tiến hành xử lý. Trong vòng 24 tiếng nữa, chúng tôi sẽ gửi thông báo trên điện thoại để Quý khách bắt đầu sử dụng ứng dụng.</p><p></p><p>Nếu Quý khách có bất kỳ thắc mắc nào, vui lòng liên hệ đến tổng đài của FE CREDIT (giờ hành chính): (028) 3868 9968 hoặc gửi thư đến email: hotrobaohiem@fecredit.com.vn</p><p></p><p>Trân trọng!</p></body></html>";
	public static final String BANCA_LIBERTY_CANCER_KEY = "Liberty_Cancer";
	public static final String BANCA_HEALTH_CARE = "Health Care";
	public static final String BANCA_SUM_ASSURED_KEY_ = "sumAssured";
	public static final String BANCA_SUM_ASSURED_25 = "250000000";
	public static final String BANCA_SUM_ASSURED_50 = "500000000";
	public static final String BANCA_PAYMENT_CODE_KEY = "leadId";
	public static final String BANCA_SUM_ASSURED_75 = "750000000";
	public static final String BANCA_OPTIONAL_RECOMMENDED_PRODUCT_MALE = "optionalRecommendedProductMale";
	public static final String BANCA_OPTIONAL_RECOMMENDED_PRODUCT_FEMALE = "optionalRecommendedProductFemale";

	public static final String BANCA_RECOMMENDED_POLICY_KEY = "recommendedPolicy";
	public static final String BANCA_NATIONAL_ID_KEY = "nationalId";
	public static final String BANCA_CRM_ID_KEY = "crmId";
	public static final String BANCA_TRANS_ID = "transId";
	public static final String IPAT_SUCCESS = "IPAT_SUCCESS";
	public static final String IPAT_FAILED = "IPAT_FAILED";
	public static final String BANCA_VT_REQUESTOR_ID = "VTIGER_CL";
	public static final String BANCA_CUSTOMER_NAME_KEY = "customerName";
	public static final String BANCA_PREMIUM_KEY = "premium";
	public static final String BANCA_DUE_DATE_KEY = "dueDate";
	public static final String BANCA_FULL_NAME_KEY = "fullName";
	public static final String BANCA_MOBILE_NUMBER_KEY = "mobileNumber";
	public static final String BANCA_GENDER_KEY = "gender";
	public static final String BANCA_DATE_OF_BIRTH_KEY = "dateOfBirth";
	public static final String BANCA_DATE_OF_BIRTH_LIBERTY_KEY = "dateOfBirthLiberty";
	public static final String BANCA_DATE_OF_BIRTH_BAO_MINH_KEY = "dateofbirthgeneralhealth";
	public static final String BANCA_EMAIL_ID_KEY = "emailId";
	public static final String BANCA_ADDRESS_KEY = "address";
	public static final String BANCA_BICC_LEAD_KEY = "bicc_lead";
	public static final String BANCA_DISTRICT_KEY = "district";
	public static final String BANCA_CITY_KEY = "city";
	public static final String BANCA_ROBOLENDING_KEY = "robo_lending";
	public static final String BANCA_EMAIL_FLAG_KEY = "email_flag";
	public static final String BANCA_VIBER_KEY = "viber";
	public static final String BANCA_FB_KEY = "fb";
	public static final String BANCA_SMS_KEY = "sms";
	public static final String BANCA_ZALO_KEY = "zalo";
	public static final String BANCA_NEW_CUSTOMER_KEY = "bancaNewCustomer";
	public static final String BANCA_APPLICATION_DATE = "bancaApplicationDate";

	public static final String BANCA_CREDIT_CARD_NUMBER = "creditCardNumber";
	public static final String BANCA_DATE_OF_CONTRACT_KEY = "contractDate";
	public static final String BANCA_AUTHORIZATION_CODE_KEY = "authorizationCode";
	public static final String BANCA_FINAL_PRODUCT_KEY = "product";

	public static final String BANCA_LEAD_ID_VTIGER = "leadId"; // Same for payment code
	public static final String BANCA_EFFECTIVE_DATE = "effectiveDate";
	public static final String BANCA_EXPIRY_DATE = "expiryDate";
	public static final String BANCA_TENTATIVE_DATE = "tenureEffectiveDate";
	public static final String BANCA_VTIGER_ID_BAO_MINH = "140";
	public static final String BANCA_VTIGER_ID_LIBERTY = "139";
	public static final String BANCA_VTIGER_LEAD_SOURCE = "TODO";
	public static final String FINAL_PAYMENT_SOURCE_KEY = "choosingPaymentOption";
	public static final String BANCA_VTIGER_CODE_KEY = "vTigerCode";
	public static final String BANCA_CONTRACT_STATUS_KEY = "bancaContractStatus";
	public static final String BANCA_VTIGER_SUCCESS_KEY = "bancaVTigerSuccess";
	public static final String BANCA_PAYMENT_DATE_KEY = "paymentDate";

	public static final String BANCA_QUESTION_ID_1 = "question1";
	public static final String BANCA_QUESTION_ID_2 = "question2";
	public static final String BANCA_QUESTION_ID_3 = "question3";
	public static final String BANCA_QUESTION_ID_4 = "question4";
	public static final String BANCA_QUESTION_ID_5 = "question5";
	public static final String BANCA_QUESTION_ID_6 = "question6";

	public static final String BANCA_PAID_USER_VEITNAM_SMS = "Cam on Quy khach da thanh toan phi bao hiem va hoan tat thu tuc mua san pham bao hiem qua ung dung SHIELD. GCN bao hiem se duoc gui den qua email dang ky.";
	public static final String BANCA_PAID_USER_ENGLISH_SMS = "Thank you for your payment and completing registering product %s in SHIELD application. The policy will be sent to your registered email";
	public static final String BANCA_PAID_USER_VEITNAM_NOTIFICATION = "Cảm ơn Quý khách đã thanh toán phí bảo hiểm và hoàn tất thủ tục mua sản phẩm %s. Giấy chứng nhận bảo hiểm sẽ được gửi đến email đăng ký của Quý khách.  Nếu có bất kỳ thắc mắc nào, Quý khách vui lòng liên hệ tổng đài của  FE CREDIT (giờ hành chính): (028) 3868 9968 để được giải đáp.";
	public static final String BANCA_PAID_USER_ENGLISH_NOTIFICATION = "Thank you for your payment and complete registering product %s. The policy will be sent to your registered email. If you have any question, please contact FE CREDIT hotline (working hour): (028 ) 3868 9968";
	public static final String BANCA_PAID_USER_VEITNAM_EMAIL_SUBJECT = "[FE CREDIT] XÁC NHẬN VỀ VIỆC KHÁCH HÀNG ĐÃ ĐÓNG PHÍ VÀ HOÀN TẤT THỦ TỤC LÀM HỢP ĐỒNG BẢO HIỂM QUA ỨNG DỤNG SHIELD.";
	public static final String BANCA_PAID_USER_VEITNAM_EMAIL_MESSAGE = "Kính gửi Quý khách hàng,<br>" + "<br>"
			+ "Đầu tiên, Công ty Tài chính TNHH MTV Ngân hàng Việt Nam Thịnh Vượng (FE CREDIT) xin gửi lời cảm ơn chân thành và sâu sắc nhất đến Quý khách đã tin tưởng và sử dụng ứng dụng SHIELD - Ứng dụng Mua bảo hiểm tự động để lựa chọn và đăng ký gói bảo hiểm phù hợp cho các nhu cầu của bản thân.<br>"
			+ "<br>"
			+ "Hiện tại trên hệ thống của FE CREDIT đã ghi nhận Quý khách đã hoàn tất việc đóng phí để mua sản phẩm %s. Quý khách vui lòng kiểm tra email sắp tới về giấy chứng nhận bảo hiểm điện tử của sản phẩm này.<br>"
			+ "<br>"
			+ "Nếu Quý khách có bất kỳ thắc mắc nào, vui lòng liên hệ đến tổng đài  của FE CREDIT (giờ hành chính): (028) 3868 9968 hoặc gửi thư đến email: hotrobaohiem@fecredit.com.vn để được giải đáp.<br>"
			+ "Trân trọng!";
	public static final String BANCA_PAID_USER_ENGLISH_EMAIL_SUBJECT = "[FE CREDIT] CONFIRMATION ABOUT YOUR PAYMENT AND COMPLETION OF REGISTERING PRODUCT VIA SHIELD APPLICATION";
	public static final String BANCA_PAID_USER_ENGLISH_EMAIL_MESSAGE = "Dear Valued Customer,<br>" + "<br>"
			+ "At first, FE CREDIT would like to say thanks for your trusting and using SHIELD application for registering the insurance product.<br>"
			+ "<br>"
			+ "Our system has received your payment for product %s. Please check the following email for the e-policy of the product<br>"
			+ "<br>"
			+ "If you have any question, please contact FE CREDIT hotline (working hour): (028 ) 3868 9968 or via email: hotrobaohiem@fecredit.com.vn<br>"
			+ "<br>" + "Yours sincerely!";

//	public static final String MASTER_API_END_POINT = "http://34.93.14.173:8080/master/api/masters/banca-get-quote-details";

	// Dms Constants

	public static final String DOC_ID = "DocId";
	public static final String DMS_REQUESTOR_ID = "ROBO";

	public static final String NID_MALE = "Male";
	public static final String NID_FEMALE = "Female";

	public static final String NID_MALE_VI = "Nam";
	public static final String NID_FEMALE_VI = "Nữ";

	public static final String NATIONALITY_VI = "Việt Nam";
	public static final String NATIONALITY_EN = "Vietnamese";

	public static final String USER_NAME_KEY = "username";
	public static final String NOTIFICATION_PREFERENCE = "notificationPreference";
	public static final String TOUCH_ID_KEY = "touchIdEnabled";
	public static final String PUSH_NOTIFICATION_KEY = "pushNotification";
	public static final String EMAIL_NOTIFICATION = "emailNotification";
	public static final String SMS_NOTIFICATION = "smsNotification";
	public static final String PROMOTION_PUSH_NOTIFICATION = "promotionPushNotification";
	public static final String PROMOTION_SMS_NOTIFICATION = "promotionSmsNotification";
	public static final String PROMOTION_EMAIL_NOTIFICATION = "promotionEmailNotification";

	public static final String JSON_ARRAY_DATA_KEY = "data";
	public static final String MASTER_DATA_KEY = "data";
	public static final String LANGUAGE_KEY = "language";
	public static final String LANGUAGE_VALUE = "en-US";
	public static final String PRODUCT_ID_KEY = "product_id";
	public static final String CATEGORY_ID_KEY = "category_id";
	public static final String GENDER_ID_KEY = "gender_id";
	public static final String PRODUCT_NAME = "product_name";
	public static final String PLAN_ID_KEY = "plan_id";
	public static final String PLAN_NAME = "plan_name";
	public static final String POLICY_NUMBER = "policy_number";
	public static final String APPLICATION_NUMBER_KEY = "application_number";
	public static final String ILLUSTRATION_NUMBER = "illustration_number";
	
//	public static final String TEMP_ = "temp_";

	public static final String NEXT_PAYMENT_DUE = "next_payment_due";
	public static final String PLAN_EXPIRING_DATE = "plan_expiring_date";
	public static final String INSURER = "insurer";
	public static final String MONTHLY_PAYMENT = "monthly_payment";
	public static final String PROTECTION_AMOUNT = "protection_amount";
	public static final String PURCHASE_DATE = "purchaseDate";
	public static final String KEY = "key";
	public static final String VALUE = "value";
	public static final String KEYLIST = "keyList";

	public static final String VIETNAM_TIME_ZONE = "Asia/Ho_Chi_Minh";
	public static final String DATE_FORMAT_SLASH = "dd/MM/yyyy";
	public static final String DATE_FORMAT_DASH_YEAR = "yyyy-MM-dd";
	public static final String DATE_FORMAT_DASH = "dd-MM-yyyy";
	public static final String DATE_FORMAT_WRONG_MESSAGE = "Date Format is Wrong";
	public static final String DATE_TIME_FORMAT_SLASH = "dd/MM/yyyy hh:mm";

	// Device Update Keys

	public static final String IOS_KEY = "ios";
	public static final String LATEST_VERSION_MAP_KEY = "latest_version";
	public static final String FORCE_UPDATE_MAP_KEY = "force_update";
	public static final String UPDATE_AVAILABLE_MAP_KEY = "update_available";

	public static final String NID_DATA_UPDATED_SUCCESS = "NID Data is updated succesfully";
	public static final String NID_ADDRESS_KEY = "nid-address";
	public static final String DELIVERY_ADDRESS_KEY = "delivery-address";

	public static final String USER_IS_BLACK_LISTED_MESSAGE = "User is BlackListed";
	public static final String USER_IS_NOT_BLACK_LISTED_MESSAGE = "User is Not BlackListed";

	public static final String X_API_KEY = "X-ApiKey";
	public static final String X_API_KEY_VALUE = "d0208bcb84638e52e07a450da8ee6a25cdda13e9";
	public static final String GRANT_TYPE_KEY = "grant_type";
	public static final String GRANT_TYPE_KEY_VALUE = "password";
	public static final String USER_NAME_KEY_VALUE = "fecredit";
	public static final String PASSWORD_KEY = "password";
	public static final String CLIENT_ID = "client_id";
	public static final String CLIENT_ID_KEY_VALUE = "7";
	public static final String CLIENT_SECRET = "client_secret";
	public static final String CLIENT_SECRET_KEY_VALUE = "h1sjfBOq1OUq9FZ2famndV8VYtQY5y3YiG8eUE4N";
	public static final String PASSWORD_KEY_VAUE = "9a20079916a65567e57545cdb946b9be";
	public static final String IB_AUTH_SLUG_KEY = "insurerauth";
	public static final String FAMILY_HEALTH_X_API_KEY = "4b3bb8ac18266473abe9a1c3cc01af68ddcdd9f9";
	public static final String FAMILY_HEALTH_INSURER = "familyhealthinsurer";
	public static final String GET_FAMILY_POLICY = "getfamilypolicy";
	public static final String IB_PARTNER_POLICY_SLUG_KEY = "insurer4";
	public static final String IB_PARTNER_POLICY_KEY = "partner-policy";
	public static final String IB_PARTNER_POLICY_SOAP_KEY = "/?env=banca";
	public static final String IB_AUTH_KEY = "/?env=banca";
	public static final String AUTHORIZATION_KEY = "Authorization";
	public static final String IB_PERSONAL_ACCIDENT_SLUG_KEY = "insurerforpa";
	public static final String IB_PA_INSUREAR_KEY = "painsurerapi";
	public static final String PA_INSUREAR_RESPONSE_STATUS_KEY = "Status";
	public static final String PA_INSUREAR_RESPONSE_MESSAGE = "Message";
	public static final String CC_INSUREAR_RESPONSE_STATUS_KEY = "Status";
	public static final String CC_INSUREAR_RESPONSE_MESSAGE = "Message";
	public static final String IB_CANCER_CARE_SLUG_KEY = "insurerforcancercare";
	public static final String IB_CC_INSUREAR_KEY = "cancercareapi";
	public static final String IB_FH_INSURER_KEY = "familyhealthinsurer";
	public static final String IB_FH_GET_PARTNER_POLICY = "getfamilypolicy";
	public static final String IB_Covid_POLICY = "covid19";
	public static final String IB_COVID19INSURER_POLICY = "covid19insurer";
	// Request BodyPayload MapKeys

	public static final String SUM_INSURED = "sumInsured";
	public static final String PREMIUM_AMOUNT = "premiumAmount";
	public static final String DISCLAIMER_AGREED_TAG = "disclaimerAgreedTag";
	public static final String DISCLAIMER_AGREED_TAG_VALUE = "1";
	public static final String TRANSACTION_ID = "transactionID";
	public static final String REFERENCE_NUMBER = "referenceNumber";
	public static final String AGENT_ID = "agentID";
	public static final String AGENT_ID_VALUE = "FEC007";
	public static final String PAYMENT_DATE = "paymentDate";
	public static final String PROPOSER_DETAILS = "proposerDetails";
	public static final String PRODUCT_DETAILS = "productDetails";
	public static final String DELIVERY_ADDRESS = "deliveryAddress";
	public static final String MEDICAL_QUENTIONNAIRE = "medicalQuestionnaire";
	public static final String MEDICAL_QUENTIONNAIRE1 = "medicalQuestionnaire1";
	public static final String INSURED_DETAILS = "insuredDetails";

	public static final String PROPOSER_NAME_KEY = "proposerName";
	public static final String PROPOSER_GENDER_KEY = "proposerGender";
	public static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
	public static final String PROPOSER_DETAILS_NATIONALITY = "nationality";
	public static final String PROPOSER_DETAILS_NATIONAL_ID = "nationalID";
	public static final String PROPOSER_DETAILS_ADDRESS = "address";
	public static final String PROPOSER_DETAILS_EMAIL_ID = "emailID";
	public static final String PROPOSER_DETAILS_MOBILE_NO = "mobileNo";
	public static final String PROPOSER_DETAILS_PLACE_OF_ORIGIN = "placeOfOrigin";
	
	public static final String CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT = "q6";
	public static final String CLAIM_TREATMENT_VISIT_DATE = "dateOfVisit";
	public static final String PRODUCT_ID = "productID";
	public static final String PLAN_ID = "planID";
	public static final String TYPE_OF_BUSINESS = "typeOfBusiness";
	public static final String TYPE_OF_BUSINESS_VALUE = "1";
	public static final String POLICY_START_DATE = "policyStartDate";
	public static final String POLICY_END_DATE = "policyEndDate";
	public static final String PRODUCT_PACKAGE = "product_package";
	public static final String PACKAGE_PA1 = "PA1";
	public static final String PACKAGE_PA2 = "PA2";
	
	public static final String IS_SAME_CURRENT_KEY = "isSameAsCurrent";
	public static final String IS_SAME_CURRENT_VALUE = "N";
	public static final String ADDRESS_KEY = "address";
	public static final String CITY_KEY = "city";
	public static final String PROVINCE_KEY = "province";

	public static final String MEDICAL_Q1 = "medicalQ1";
	public static final String MEDICAL_Q2 = "medicalQ2";
	public static final String MEDICAL_Q3 = "medicalQ3";
	public static final String MEDICAL_Q4 = "medicalQ4";

	// SingleHealth_Insurer Details Keys
	public static final String INSURED_NAME = "insuredName";
	public static final String INSURED_GENDER = "insuredGender";
	public static final String INSURED_DOB = "insuredDOB";
	public static final String INSURED_NATIONAL_ID = "insuredNationalID";
	public static final String INSURED_RELATIONSHIP = "insuredRelationship";
	public static final String CHU_HOP_DONG = "Chủ hợp đồng";
	public static final String SELF_KEY = "self";
	public static final String INSURED_MOBILE = "insuredMobile";
	public static final String INSURED_EMAIL = "insuredEmail";
	public static final String INSURED_Address = "insuredAddress";

	// PA_INSURER Details Keys
	public static final String PA_INSURED_NAME = "insured1Name";
	public static final String PA_INSURED_GENDER = "insured1Gender";
	public static final String PA_INSURED_DOB = "insured1DOB";
	public static final String PA_INSURED_NATIONAL_ID = "insured1NationalID";
	public static final String PA_INSURED_RELATIONSHIP = "insured1Relationship";
	public static final String PA_CHU_HOP_DONG = "Chủ hợp đồng";
	public static final String PA_SELF_KEY = "self";
	public static final String PA_INSURED_MOBILE = "insured1Mobile";
	public static final String PA_INSURED_EMAIL = "insured1Email";
	public static final String PA_INSURED_Address = "insured1Address";

	// CC_INSURER Details Keys
	public static final String CC_INSURED_NAME = "insured1Name";
	public static final String CC_INSURED_GENDER = "insured1Gender";
	public static final String CC_INSURED_DOB = "insured1DOB";
	public static final String CC_INSURED_NATIONAL_ID = "insured1NationalID";
	public static final String CC_INSURED_RELATIONSHIP = "insured1Relationship";
	public static final String CC_CHU_HOP_DONG = "Chủ hợp đồng";
	public static final String CC_SELF_KEY = "self";
	public static final String CC_INSURED_MOBILE = "insured1Mobile";
	public static final String CC_INSURED_EMAIL = "insured1Email";
	public static final String CC_INSURED_Address = "insured1Address";

	// FH_INSURER Details Keys
	public static final String FH_INSURED_NAME = "insuredName";
	public static final String FH_INSURED_GENDER = "insuredGender";
	public static final String FH_INSURED_DOB = "insuredDOB";
	public static final String FH_INSURED_NATIONAL_ID = "insuredNationalID";
	public static final String FH_INSURED_RELATIONSHIP = "insuredRelationship";
	public static final String FH_CHU_HOP_DONG = "Chủ hợp đồng";
	public static final String FH_SELF_KEY = "self";
	public static final String FH_INSURED_MOBILE = "insuredMobile";
	public static final String FH_INSURED_EMAIL = "insuredEmail";
	public static final String FH_INSURED_Address = "insuredAddress";
	public static final String FH_INSUREAR_RESPONSE_STATUS_KEY = "Status";
	public static final String FH_INSUREAR_RESPONSE_MESSAGE = "Message";

	// insurer api response keyss
	public static final String INSURER_RESPONSE_DATA = "Data";
	public static final String INSURER_RESPONSE_SUCCESS = "Success";
	public static final String INSURER_RESPONSE_POLICYID = "PolicyId";
	public static final String INSURER_RESPONSE_PDFLINK = "pdfLink";
	public static final String INSURER_RESPONSE_EXCEL_LINK = "excelLink";
	public static final String INSURER_RESPONSE_LINK = "Link";
	public static final String INSURER_RESPONSE_FILES = "Files";
	public static final String INSURER_RESPONSE_POLICYNO = "PolicyNo";
	public static final String INSURER_NAME = "Bao Minh";
	public static final String PA_INSURER_NAME = "PetroVietnam Insurance";
	public static final String CC_INSURER_NAME = "PetroVietnam Insurance";
	public static final String INSURER_CERTIFICATE_DMS_ID = "9012";
	public static final String INSURER_DOCUMENT_TYPE = "DocumentType";
	public static final String INSURER_CNBH = "CNBH";
	public static final String INSURER_DSBH = "DSBH";
	public static final String BASIC_PREMIUM = "basic_premium";
	public static final String ADDITIONAL_CRITICAL_AMOUNT = "additionalCriticalAmount";

	public static final String ESIGN_UPLOAD_DMS_ID = "1012";

	// CMS IB
	public static final String CMS_COMPANY_SLUG = "banca";
	public static final String CMS_CHECK_ELIGIBILITY_SLUG = "fecms";
	public static final String CMS_CHECK_ELIGIBILITY_KEY = "checkeligibility";
	public static final String CMS_NOT_ELIGIBLE = "Not Eligible";
	public static final String CMS_CHECK_ELIGIBILITY_INTEGRATION_SOAP_KEY = "&binding=CMS_InsuranceWSHttpBinding";
	public static final String CMS_CHECK_ELIGIBILITY_SOAP_SLUG_KEY = "soap/";
	public static final String CMS_CHECK_ELIGIBILITY_HASH = "1234";
	public static final String CMS_POST_TRANSACTION_KEY = "posttransaction";
	// public static final String
	// PROCESS_VARIABLE_MOB_NO="applicantMobileNumber_user";
	public static final String CMS_CHECK_ELIGIBILITY_DETAILS = "Details";
	public static final String CMS_DETAILS_NOT_FOUND = "Details not found";
	public static final String CMS_CHECK_ELIGIBILITY_ACCOUNT = "Account";
	public static final String CMS_CHECK_ELIGIBILITY_CREDITLIMIT = "CreditLimit";
	public static final String CMS_CHECK_ELIGIBILITY_AVAILABLE_LIMIT = "AvailableLimit";
	public static final String CMS_CHECK_ELIGIBILITY_STATUS = "Eligibility Status";
	public static final String POST_TRANSACTION_ACC_NOT_FOUND = "Account Number Not Found";
	public static final String CMS_MESSAGE = "Message";
	public static final String CMS_PER_MONTH = "perMonth";
	public static final String CMS_PREMIUM_AMOUNT = "premiumAmount";

	public static final String CMS_UAT_PHONE_NO = "841675876982";
	public static final String CMS_UAT_NATIONALID = "331740271";

	// CMS PostTransaction
	public static final String POST_TRANSACTION_TENOR = "1";
	public static final String POST_TRANSACTION_IPP_FLAG = "true";
	public static final String POST_TRANSACTION_ACTION_CODE = "INS2";
	public static final String POST_TRANSACTION_INS_PRODUCT = "1234";

	// CMS PostTransaction response
	public static final String POST_TRANSACTION_POST_TRANX_RESPONSE = "NS1:PostTransactionResponse";
	public static final String CHECK_ELIGIBILITY_RESPONSE = "NS1:CheckEligibilityResponse";
	public static final String POST_TRANSACTION_SYS = "Sys";
	public static final String POST_TRANSACTION_CODE = "Code";
	public static final String POST_TRANSACTION_DESCRIPTION = "Description";
	public static final String CHECK_ELIGIBILITY_BANCA_INSURANCE_ELIGIBILITY = "BancaInsuranceEligibity";

	// Vtiger Constants
	public static final Integer MIN_DROPOFF_MINUTES = 30;
	public static final String SH_CONGRATULATION_SCREEN = "congratulation";
	public static final String SH_UPLOAD_ERROR_SCREEN = "uploadError";
	public static final String SH_CHECKOUT_SCREEN = "checkout";
	public static final String TW_SUCCESS = "successTw";

	// MasterEndPoint
	public static final String MASTERS_BANCA_GET_QUOTE_DETAILS = "banca-get-quote-details";
	public static final String MASTERS_BANCA_GET_PREMIUM_RATE = "banca-get-premium-rate";
	public static final String MASTERS_BANCA_GET_TERM_LIFE_QUICK_QUOTE = "banca-get-tl-quick-quote";
	public static final String MASTERS_BANCA_GET_TERM_LIFE_QUOTE_DETAILS = "banca-get-tl-quote-details";
	public static final String SLUG_KEY_MASTER_MISSING = "Master Slug is Missing";
	public static final String SLUG_KEY = "Slug";
	public static final String MASTER_ERROR_MESSAGE = "errorMessage";

	public static final String MASTERS_BANCA_GET_CANCER_CARE_QUOTE_DETAILS = "banca-get-cc-quote-details";

	public static final String NATIONAL_ID_KEY = "national_Id";
	public static final String NID_GENDER_KEY = "nid_gender_key";
	public static final String NID_USER_NAME_KEY = "nid_username_Key";
	public static final String NID_NATIONALITY_KEY = "nid_nationality_key";
	public static final String NID_DOB = "nid_dob";

	// family premium calculations
	public static final String BASIC = "Basic";
	public static final String ADVANCE = "Advanced";
	public static final String BASIC_VI_QD = "Cơ Bản";
	public static final String ADVANCE_VI_QD = "Nâng Cao";
	public static final String BASIC_VI_QQ = "Gói Cơ Bản";
	public static final String ADVANCE_VI_QQ = "Gói Nâng Cao";
	public static final String PLAN_TYPE = "plan_type";
	public static final String PLAN_VALUES = "plan_values";
	public static final String PLAN_SHORT_VALUES = "plan_short_values";
	public static final String CONSTANT_X = "X";
	public static final String FI = "FI";
	public static final String DB = "DB";
	public static final String TP = "TP";
	public static final String TI = "TI";
	public static final String TOP_SECTION_VALUES = "top_section_values";
	public static final String MIDDLE_SECTION_LISTING_DESC = "middle_section_listing_desc";
	public static final String PROTECTS_YOU = "Protects you & ";
	public static final String LOVED_ONES = " loved ones";
	public static final String AGE_FROM = "age_from";
	public static final String AGE_TO = "age_to";
	public static final String PLAN_ID_FAMILY = "plan_id";
	public static final String PLAN_ID_BASIC = "10003001";
	public static final String PLAN_ID_ADVANCED = "10003002";
	public static final String WHITE_SPACE_SPLIT = "\\s+";
	public static final String PAYABLE_PREMIUM_AMOUNT = "payable_premium_amount";
	public static final Double DISCOUNT_FIVE = 0.05;
	public static final Double DISCOUNT_TEN = 0.1;
	public static final String FH_QUICK_QUOTE = "quick-quote";
	public static final String FH_LANGUAGE = "language";
	public static final String AGE_KEY = "Age";
	public static final String PROTECTS_YOU_VI = "Cho bạn và ";
	public static final String LOVED_ONES_VI = " thành viên trong gia đình";
	public static final String PROTECTION_LABEL = "protection_label";
	public static final String SUPERIOR = "Superior";
	public static final String SUPERIOR_VI = "Toàn Diện";
	public static final String CONSTANT_Y = "Y";

	public static final LinkedHashMap<String, TermLifeAgePremiumRates> AGE_PREMIUM_MAP = new LinkedHashMap<>();
	static {
//		 TermLifeAgePremiumRates(Double premiumRate, Double ppdRate, Double criticalRate)
		AGE_PREMIUM_MAP.put("18", new TermLifeAgePremiumRates(0.360, 0.1698, 0.970));
		AGE_PREMIUM_MAP.put("19", new TermLifeAgePremiumRates(0.360, 0.1698, 0.970));
		AGE_PREMIUM_MAP.put("20", new TermLifeAgePremiumRates(0.360, 0.1698, 0.990));
		AGE_PREMIUM_MAP.put("21", new TermLifeAgePremiumRates(0.360, 0.1698, 0.990));
		AGE_PREMIUM_MAP.put("22", new TermLifeAgePremiumRates(0.360, 0.1698, 0.990));
		AGE_PREMIUM_MAP.put("23", new TermLifeAgePremiumRates(0.360, 0.1698, 1.000));
		AGE_PREMIUM_MAP.put("24", new TermLifeAgePremiumRates(0.360, 0.1698, 1.000));
		AGE_PREMIUM_MAP.put("25", new TermLifeAgePremiumRates(0.360, 0.1698, 1.000));
		AGE_PREMIUM_MAP.put("26", new TermLifeAgePremiumRates(0.360, 0.1698, 1.020));
		AGE_PREMIUM_MAP.put("27", new TermLifeAgePremiumRates(0.360, 0.1698, 1.030));
		AGE_PREMIUM_MAP.put("28", new TermLifeAgePremiumRates(0.360, 0.1698, 1.050));
		AGE_PREMIUM_MAP.put("29", new TermLifeAgePremiumRates(0.360, 0.1698, 1.060));
		AGE_PREMIUM_MAP.put("30", new TermLifeAgePremiumRates(0.360, 0.1698, 1.090));
		AGE_PREMIUM_MAP.put("31", new TermLifeAgePremiumRates(0.360, 0.1698, 1.110));
		AGE_PREMIUM_MAP.put("32", new TermLifeAgePremiumRates(0.360, 0.1698, 1.140));
		AGE_PREMIUM_MAP.put("33", new TermLifeAgePremiumRates(0.360, 0.1698, 1.170));
		AGE_PREMIUM_MAP.put("34", new TermLifeAgePremiumRates(0.360, 0.1698, 1.210));
		AGE_PREMIUM_MAP.put("35", new TermLifeAgePremiumRates(0.360, 0.1698, 1.260));
		AGE_PREMIUM_MAP.put("36", new TermLifeAgePremiumRates(0.500, 0.1698, 1.300));
		AGE_PREMIUM_MAP.put("37", new TermLifeAgePremiumRates(0.500, 0.1698, 1.380));
		AGE_PREMIUM_MAP.put("38", new TermLifeAgePremiumRates(0.500, 0.1698, 1.450));
		AGE_PREMIUM_MAP.put("39", new TermLifeAgePremiumRates(0.500, 0.1698, 1.540));
		AGE_PREMIUM_MAP.put("40", new TermLifeAgePremiumRates(0.500, 0.1698, 1.630));
		AGE_PREMIUM_MAP.put("41", new TermLifeAgePremiumRates(0.630, 0.1698, 1.750));
		AGE_PREMIUM_MAP.put("42", new TermLifeAgePremiumRates(0.630, 0.1698, 1.870));
		AGE_PREMIUM_MAP.put("43", new TermLifeAgePremiumRates(0.630, 0.1698, 2.010));
		AGE_PREMIUM_MAP.put("44", new TermLifeAgePremiumRates(0.630, 0.1698, 2.160));
		AGE_PREMIUM_MAP.put("45", new TermLifeAgePremiumRates(0.630, 0.1698, 2.290));
		AGE_PREMIUM_MAP.put("46", new TermLifeAgePremiumRates(0.700, 0.1698, 2.440));
		AGE_PREMIUM_MAP.put("47", new TermLifeAgePremiumRates(0.900, 0.1698, 2.610));
		AGE_PREMIUM_MAP.put("48", new TermLifeAgePremiumRates(0.900, 0.1698, 2.820));
		AGE_PREMIUM_MAP.put("49", new TermLifeAgePremiumRates(0.900, 0.1698, 3.010));
		AGE_PREMIUM_MAP.put("50", new TermLifeAgePremiumRates(1.150, 0.1698, 3.270));
		AGE_PREMIUM_MAP.put("51", new TermLifeAgePremiumRates(1.150, 0.1698, 3.480));
		AGE_PREMIUM_MAP.put("52", new TermLifeAgePremiumRates(1.150, 0.1698, 3.610));
		AGE_PREMIUM_MAP.put("53", new TermLifeAgePremiumRates(1.400, 0.1698, 3.730));
		AGE_PREMIUM_MAP.put("54", new TermLifeAgePremiumRates(1.400, 0.1698, 3.880));
		AGE_PREMIUM_MAP.put("55", new TermLifeAgePremiumRates(1.400, 0.1698, 4.000));
		AGE_PREMIUM_MAP.put("56", new TermLifeAgePremiumRates(1.700, 0.1698, 4.170));
		AGE_PREMIUM_MAP.put("57", new TermLifeAgePremiumRates(1.700, 0.1698, 4.320));
		AGE_PREMIUM_MAP.put("58", new TermLifeAgePremiumRates(1.700, 0.1698, 4.450));
		AGE_PREMIUM_MAP.put("59", new TermLifeAgePremiumRates(1.700, 0.1698, 4.590));
		AGE_PREMIUM_MAP.put("60", new TermLifeAgePremiumRates(1.700, 0.1698, 4.770));

	}

	public static final LinkedHashMap<String, PlanPremiums> SUM_PPD_CRITICAL_PROTE_AMOUNT = new LinkedHashMap<>();
	static {
		SUM_PPD_CRITICAL_PROTE_AMOUNT.put("10004001", new PlanPremiums(100000000, 100000000, 50000000, 100, 50));
		SUM_PPD_CRITICAL_PROTE_AMOUNT.put("10004002", new PlanPremiums(200000000, 200000000, 100000000, 200, 100));
		SUM_PPD_CRITICAL_PROTE_AMOUNT.put("10004003", new PlanPremiums(300000000, 300000000, 100000000, 300, 100));
	}

	public static final String EMAIL_SENT_SUCCESS = "Email Sent Successfully !";
	public static final String MAIL_SMTP_HOST_KEY = "mail.smtp.host";
	public static final String MAIL_SMTP_PORT_KEY = "mail.smtp.port";
	public static final String MAIL_SMTP_AUTH_KEY = "mail.smtp.auth";
	public static final String MAIL_SMTP_STARTTLS_KEY = "mail.smtp.starttls.enable";
	public static final String EMAIL_SUBJECT_ENGLISH = "Congratulation on your purchase! See your ${policy_name} in details.";
	public static final String EMAIL_SUBJECT_VITENAM = "Thông tin chi tiết về quyền lợi bảo hiểm ${policy_name} của khách hàng.";
	public static final String CUSTOMER_NAME_POLICY_NAME = "Hi Mr/Ms ${customer_name},<br><br>";
	public static final String CUSTOMER_NAME_POLICY_NAME_IN_VI = "Chào khách hàng ${customer_name},<br><br>";
	public static final String POLICY_BODY_MESSAGE_IN_EN = "This is a confirmation that you have successfully purchased ${policy_name}.Please find the details of your policy attached in this email.<br><br>";
	public static final String POLICY_BODY_MESSAGE_IN_VN = "Chúc mừng bạn đã mua thành công bảo hiểm ${policy_name}. Vui lòng xem tài liệu đính kèm trên email này để biết thông tin chi tiết về quyền lợi bảo hiểm của bạn.<br><br>";
	public static final String USER_PREMIUM_TENURE = "userPremiumTenure";
	public static final String POLICY_MESSAGE = "Thanks for choosing us! If you have any further questions, please visit our help center or contact us (028)38689968.<br>";
	public static final String POLICY_MESSAGE_IN_VI = "Cảm ơn khách hàng đã lựa chọn FE CREDIT. Nếu bạn có bất kỳ thắc mắc nào, hãy gọi đến chúng tôi theo số (028)38689968 để được giải đáp.<br>";

	public static final String EMAIL_BODY_CONTENT_TYPE = "text/html;charset=utf-8";
	public static final String CONFIRM_EMAIL_ENDPOINT = "/web/confirm-account?token=";

	// Vtiger Request
	public static final String FEC_VTIGER_REQUESTOR_ID = "SBANCA";
	// VTIGER IB
	public static final String VTIGER_COMPANY_SLUG = "banca";
	public static final String VTIGER_SEND_DROP_OFF_KEY = "senddropoff";
	public static final String VTIGER_CREATE_DROP_OFF_KEY = "senddropoff---create";
	public static final String VTIGER_INTEGRATION_SOAP_KEY = "&binding=Shield_VtigerWSHttpBinding";
	public static final String VTIGER_SOAP_SLUG_KEY = "soap/";
	public static final String VTIGER_HASH = "1234";
	public static final String VTIGER_IB_KEY = "vtiger_shield_integration";

	// vtiger
	public static final String VTIGER_ENROLMENT_DROP_OFF_TYPE = "Enrolment";
	public static final String VTIGER_DROP_OFF = "dropoff";

	public static final Map<String, String> engToViatnamMap;
	static {
		engToViatnamMap = new HashMap<String, String>();
		engToViatnamMap.put("single health protection", "Bảo hiểm Sức Khỏe Cá Nhân");
		engToViatnamMap.put("single health", "Bảo hiểm Sức Khỏe Cá Nhân");
		engToViatnamMap.put("motor protection", "Bảo hiểm Xe Máy");
		engToViatnamMap.put("motor", "Xe Máy");
		engToViatnamMap.put("basic plan", "Gói Cơ Bản");
		engToViatnamMap.put("advanced plan", "Gói Nâng Cao");
		engToViatnamMap.put("superior plan", "Gói Toàn Diện");
		engToViatnamMap.put("male", "Nam");
		engToViatnamMap.put("female", "Nữ");
		engToViatnamMap.put("vietnamese", "Việt Nam");
		engToViatnamMap.put("personal accident protection", "Bảo hiểm Tai Nạn");
		engToViatnamMap.put("cancer care", "Bảo hiểm Ung Thư");
		engToViatnamMap.put("covid cover protection", "Bảo hiểm COVID");
		
		engToViatnamMap.put("Term Life Protection", "Bảo hiểm Tử kỳ");
	}

	public static final Map<String, String> viatnamToEngMap;
	static {
		viatnamToEngMap = new HashMap<String, String>();
		viatnamToEngMap.put("bảo hiểm sức khỏe cá nhân", "Single Health Protection");
		viatnamToEngMap.put("bảo hiểm xe máy", "Motor Protection");
		viatnamToEngMap.put("xe máy", "Motor");
		viatnamToEngMap.put("gói cơ bản", "Basic plan");
		viatnamToEngMap.put("gói nâng cao", "Advanced plan");
		viatnamToEngMap.put("gói toàn diện", "Superior plan");
		viatnamToEngMap.put("nam", "Male");
		viatnamToEngMap.put("nữ", "Female");
		viatnamToEngMap.put("việt nam", "Vietnamese");
		viatnamToEngMap.put("bảo hiểm tai nạn", "Personal Accident Protection");
		viatnamToEngMap.put("bảo hiểm ung thư", "Cancer Care");
		viatnamToEngMap.put("bảo hiểm covid", "Covid Cover Protection");
		
		viatnamToEngMap.put("Bảo hiểm Tử kỳ", "Term Life Protection");
	}

	public static final Map<String, String> engDashBoardMap;
	static {
		engDashBoardMap = new HashMap<String, String>();
		engDashBoardMap.put("single health protection", "Single Health");
		engDashBoardMap.put("motor protection", "Motor");
//		engDashBoardMap.put("personal accident","Personal Accident");
//		engDashBoardMap.put("cancer care","Cancer Care");
	}

	public static final String DMS_APPLICATION_ID_GEN = "dms_application_id_gen";
	public static final String GET_APPLICATION_ID = "getapplicationid";
	public static final String APPLICATION_SOAP_KEY = "&binding=APS_AppIDGenHttpBinding";

	//public static final String SMS_CONTENT = "Chuc mung! QK da dky thanh cong BH ${policy_name} may, so tien duoc BH: ${sum_insured} VND den ${expiry_date}. Chi tiet tai ${Link_to_Shield} hoac LH: 028 38689968";
	
	//public static final String SMS_CONTENT = "Chuc mung! Ban da dang ky thanh cong BH ${policy name}, so tien duoc BH la ${sum insured}VND den {expiry date}. Chi tiet tai SHIELD hoac LH: 02838689968";
	  public static final String SMS_CONTENT = "Chuc mung! QK da dky thanh cong BH ${policy_name}, so tien duoc BH: ${sum_insured}VND den ${expiry_date}. Chi tiet tai SHIELD hoac LH: 028 38689968";
	  public static final String SMS_CASH_PAY_INITIATE = "QK vui long TT phi bao hiem o Cua hang, Vi dien tu Momo, Payoo, Airpay,..hoac tai http://bit.ly/2kyLAdX. Ma Ho so ${crm_id}, Stien: ${sum_insured}d, LH: (028) 3868 9968";
	  public static final String SMS_CASH_PAY_COMPLETE = "Cam on Quy khach da thanh toan ${sum_insured}d cho ma ho so ${crm_id}. Hop dong bao hiem cua Quy khach se som duoc kich hoat trong thoi gian toi. LH: (028) 3868 9968";
	  
	  
	  
	  
	  
	// Request BodyPayload MapKeys for PVIinsurer

	public static final String PVI_PRODUCT_DETAILS_PRODUCT_ID = "productID";

	public static final String PVI_PRODUCT_DETAILS_PLAN_ID = "planId";

	public static final String PVI_PRODUCT_DETAILS_TYPE_OF_BUSINESS = "typeOfBusiness";

	public static final String PVI_PRODUCT_DETAILS_POLICY_START_DATE = "policyStartDate";

	public static final String PVI_PRODUCT_DETAILS_POLICY_END_DATE = "policyEndDate";

	public static final String PVI_PRODUCT_DETAILS_TENNURE = "tennure";

	public static final String PVI_VEHICLE_DETAILS_BRAND = "brand";

	public static final String PVI_VEHICLE_DETAILS_MODEL = "model";

	public static final String PVI_VEHICLE_DETAILS_CC = "cc";

	public static final String PVI_VEHICLE_DETAILS_REGISTRATION_DATE = "registrationDate";

	public static final String PVI_VEHICLE_DETAILS_REGISTRATION_NUMBER = "registrationNumber";

	public static final String PVI_VEHICLE_DETAILS_VALUE_OF_VEHICLE = "valueoftheVehicle";

	public static final String PVI_PROPOSAL_DETAILS_PROPOSER_NAME = "proposerName";

	public static final String PVI_PROPOSAL_DETAILS_PROPOSER_GENDER = "proposerGender";

	public static final String PVI_PROPOSAL_DETAILS_DOB = "dateOfBirth";

	public static final String PVI_PROPOSAL_DETAILS_NATIONALITY = "Nationality";

	public static final String PVI_PROPOSAL_DETAILS_ADDRESS = "Address";

	public static final String PVI_PROPOSAL_DETAILS_NATIONAL_ID = "NationalID";

	public static final String PVI_PROPOSAL_DETAILS_EMAIL = "EmailID";

	public static final String PVI_PROPOSAL_DETAILS_MOBILE_NO = "MobileNo";

	public static final String PVI_PROPOSAL_DETAILS_PLACE_OF_ORIGIN = "PlaceofOrigin";

	public static final String PVI_DELIVERY_ADDRESS_IS_SAME = "isSameAsCurrent";

	public static final String PVI_DELIVERY_ADDRESS = "address";

	public static final String PVI_DELIVERY_ADDRESS_CITY = "city";

	public static final String PVI_DELIVERY_ADDRESS_PROVINCE = "province";

	public static final String PVI_MOTOR_LOSS_PREMIUM = "MotorLossPremium";

	public static final String PVI_DRIVER_LOSS_PREMIUM = "DriverLossPremium";

	public static final String PVI_THIRD_PARTY_LIABILITY_PREMIUM = "ThirdPartyLiabilityPremium";

	public static final String PVI_DISCLAMER_AGREED = "DisclaimerAgreed";

	public static final String PVI_TRANS_ID = "TransactionID";

	public static final String PVI_REF_NUM = "ReferenceNumber";

	public static final String PVI_REF_AGENT_ID = "AgentID";

	// PVIinsurer api response keyss

	public static final String PVIINSURER_RESPONSE_STATUS = "Status";

	public static final String PVIINSURER_RESPONSE_MESSAGE = "Message";

	public static final String PVI_INSURER_NAME = "PVI";

	public static final Integer DISCLAIMER_AGREED_TAG_VALUE_INTEGER = 1;
	public static final Integer TYPE_OF_BUSINESS_VALUE_INTEGER = 1;
	public static final String VEHICLE_DETAILS = "vehicledetails";
	public static final Integer TENURE_INT = 1;

	// moto ib keys

	public static final String IB_PVIInsurer_SLUG_KEY = "moto";
	public static final String IB_PVIInsurer_KEY = "moto";
	public static final String Company_Slug = "banca";
	public static final String Hash_Value = "1234";

	public static final Map<String, String> productIdMap;
	static {
		productIdMap = new HashMap<String, String>();
		productIdMap.put("10001", "banca-get-quote-details");
		productIdMap.put("10005", "banca-get-pa-quote-details");
		productIdMap.put("10002", "banca-get-tw-quote-details");
		productIdMap.put("10004", "banca-get-cc-quote-details");
		productIdMap.put("20001", "banca-get-tl-quote-details");
		productIdMap.put("10003", "banca-get-fh-quote-details");
		productIdMap.put("10010", "banca-get-covid-quote-details");
		productIdMap.put(Products.DENGUE.getProductId(), "banca-get-denque-quote-details");
	}

	public static final String TW_SUB_SECTION1_EN = "Incl. 1.5% of your motor’s value";
	public static final String TW_SUB_SECTION2_PART1_EN = "+ motor value";
	public static final String TW_SUB_SECTION2_PART2_EN = " | + 3rd party liability";
	public static final String TW_SUB_SECTION1_VN = "Dựa trên 1.5% giá trị hiện tại chiếc xe của bạn";
	public static final String TW_SUB_SECTION2_PART1_VN = "+ Bảo hiểm vật chất xe";
	public static final String TW_SUB_SECTION2_PART2_VN = " | + Bảo hiểm trách nhiệm dân sự bắt buộc";
	public static final String PLAN_NAME_VI = "Bảo hiểm vật chất xe máy";

	public static final String AMAZON_SNS_MESSAGE = "{ \"data\": { \"message\": \"Sample message for Android endpoints\" },\"notification\" : {\"body\" : \"${body}\",\"title\": \"${title}\"} }";
	// public static final String DROP_OFF_PUSH_NOTIFICATION_BODY = "Remember that
	// ${policy_name} you wanted? Jump back into FE Shield to finish your progress.
	// Your progress will expire in 3 days otherwise - don't miss out!";
	public static final String DROP_OFF_PUSH_NOTIFICATION_TITLE_EN = "Enrolment progress about to expire";
	public static final String DROP_OFF_PUSH_NOTIFICATION_TITLE_VN = "Phiên đăng ký của bạn sắp hết thời gian";

	public static final String DROP_OFF_FIRST_PUSH_NOTIFICATION_BODY_EN = "Hi <customer_name>, we only require a few more details to get you protected with our <product_name> protection. Click here to provide those final details - it won't take long!";
	public static final String DROP_OFF_FIRST_PUSH_NOTIFICATION_BODY_VN = "Chào khách hàng <customer_name>, chỉ cần cung cấp thêm một vài thông tin nữa là bạn sẽ mua thành công <product_name>. Hãy nhấn vào đây để tiếp tục!";

	public static final String DROP_OFF_SECOND_PUSH_NOTIFICATION_BODY_EN = "Hi <customer_name>, you have been busy? It's okay. Just a few more details and you will be protected with our <product_name> protection. Click here!";
	public static final String DROP_OFF_SECOND_PUSH_NOTIFICATION_BODY_VN = "Có vẻ như khách hàng <customer_name> đang hơi bận rộn đúng không? Chúng tôi vẫn đang chờ đợi bạn hoàn thành thông tin đăng ký <product_name>. Nhấn vào đây ngay để tiếp tục nhé!";

	public static final List<String> phoneNumberList;
	static {
		phoneNumberList = new ArrayList<String>();
		phoneNumberList.add("384722708");
		phoneNumberList.add("772508377");
		phoneNumberList.add("904212743");
		phoneNumberList.add("967763274");
		phoneNumberList.add("795918673");
		phoneNumberList.add("934066968");
		phoneNumberList.add("903510047");
		phoneNumberList.add("934040151");
		phoneNumberList.add("839966527");
		phoneNumberList.add("907220014");
		phoneNumberList.add("766622458");
		phoneNumberList.add("904406101");
		phoneNumberList.add("908301272");
		phoneNumberList.add("9999999999");
		phoneNumberList.add("912244429");
		phoneNumberList.add("988766955");
		phoneNumberList.add("983107266");
		phoneNumberList.add("965033035");
		phoneNumberList.add("934267623");
		phoneNumberList.add("943472227");
		phoneNumberList.add("943703388");
		phoneNumberList.add("915860894");
		phoneNumberList.add("398235742");
		phoneNumberList.add("933481661");
	}
	public static Map<String, String> cancer_premium_map = new HashMap<String, String>();
	public static final Map<String, String> categoryIdMap;
	static {

		categoryIdMap = new HashMap<String, String>();
		categoryIdMap.put("1001", "banca-get-quote-details");
		categoryIdMap.put("1002", "banca-get-pa-quote-details");
		categoryIdMap.put("1003", "banca-get-tw-quote-details");
		categoryIdMap.put("1004", "banca-get-cc-quote-details");
		categoryIdMap.put("1004", "banca-get-tl-quote-details");
		categoryIdMap.put("1001", "banca-get-fh-quote-details");
		categoryIdMap.put("1001", "banca-get-covid-quote-details");

//		cancer_premium_map.put("PLANID_GENDER_AGE", "10000");

		cancer_premium_map.put("10004001_10_16", "57078");
		cancer_premium_map.put("10004001_10_17", "62046");
		cancer_premium_map.put("10004001_10_18", "67721");
		cancer_premium_map.put("10004001_10_19", "74093");
		cancer_premium_map.put("10004001_10_20", "81515");
		cancer_premium_map.put("10004001_10_21", "89782");
		cancer_premium_map.put("10004001_10_22", "98868");
		cancer_premium_map.put("10004001_10_23", "108999");
		cancer_premium_map.put("10004001_10_24", "120651");
		cancer_premium_map.put("10004001_10_25", "133423");
		cancer_premium_map.put("10004001_10_26", "148408");
		cancer_premium_map.put("10004001_10_27", "165069");
		cancer_premium_map.put("10004001_10_28", "184419");
		cancer_premium_map.put("10004001_10_29", "206770");
		cancer_premium_map.put("10004001_10_30", "232431");
		cancer_premium_map.put("10004001_10_31", "262305");
		cancer_premium_map.put("10004001_10_32", "296095");
		cancer_premium_map.put("10004001_10_33", "334069");
		cancer_premium_map.put("10004001_10_34", "375941");
		cancer_premium_map.put("10004001_10_35", "421486");
		cancer_premium_map.put("10004001_10_36", "470322");
		cancer_premium_map.put("10004001_10_37", "522280");
		cancer_premium_map.put("10004001_10_38", "577739");
		cancer_premium_map.put("10004001_10_39", "637305");
		cancer_premium_map.put("10004001_10_40", "701745");
		cancer_premium_map.put("10004001_10_41", "771845");
		cancer_premium_map.put("10004001_10_42", "848295");
		cancer_premium_map.put("10004001_10_43", "931876");
		cancer_premium_map.put("10004001_10_44", "1022046");
		cancer_premium_map.put("10004001_10_45", "1117912");
		cancer_premium_map.put("10004001_10_46", "1218090");
		cancer_premium_map.put("10004001_10_47", "1320702");
		cancer_premium_map.put("10004001_10_48", "1423372");
		cancer_premium_map.put("10004001_10_49", "1523936");
		cancer_premium_map.put("10004001_10_50", "1620501");
		cancer_premium_map.put("10004001_10_51", "1711746");
		cancer_premium_map.put("10004001_10_52", "1798351");
		cancer_premium_map.put("10004001_10_53", "1881530");
		cancer_premium_map.put("10004001_10_54", "1964230");
		cancer_premium_map.put("10004001_10_55", "2050545");
		cancer_premium_map.put("10004001_10_56", "2144656");
		cancer_premium_map.put("10004001_10_57", "2249623");
		cancer_premium_map.put("10004001_10_58", "2367148");
		cancer_premium_map.put("10004001_10_59", "2498283");
		cancer_premium_map.put("10004001_10_60", "2642347");
		cancer_premium_map.put("10004001_10_61", "2797963");
		cancer_premium_map.put("10004001_10_62", "2963810");
		cancer_premium_map.put("10004001_10_63", "3138846");
		cancer_premium_map.put("10004001_10_64", "3322309");
		cancer_premium_map.put("10004001_10_65", "3514279");
		cancer_premium_map.put("10004001_11_16", "68522");
		cancer_premium_map.put("10004001_11_17", "72784");
		cancer_premium_map.put("10004001_11_18", "78197");
		cancer_premium_map.put("10004001_11_19", "84665");
		cancer_premium_map.put("10004001_11_20", "92580");
		cancer_premium_map.put("10004001_11_21", "101615");
		cancer_premium_map.put("10004001_11_22", "111958");
		cancer_premium_map.put("10004001_11_23", "124092");
		cancer_premium_map.put("10004001_11_24", "137735");
		cancer_premium_map.put("10004001_11_25", "153233");
		cancer_premium_map.put("10004001_11_26", "170939");
		cancer_premium_map.put("10004001_11_27", "191017");
		cancer_premium_map.put("10004001_11_28", "213380");
		cancer_premium_map.put("10004001_11_29", "238320");
		cancer_premium_map.put("10004001_11_30", "265457");
		cancer_premium_map.put("10004001_11_31", "294644");
		cancer_premium_map.put("10004001_11_32", "326019");
		cancer_premium_map.put("10004001_11_33", "358937");
		cancer_premium_map.put("10004001_11_34", "393706");
		cancer_premium_map.put("10004001_11_35", "430259");
		cancer_premium_map.put("10004001_11_36", "469357");
		cancer_premium_map.put("10004001_11_37", "511161");
		cancer_premium_map.put("10004001_11_38", "555771");
		cancer_premium_map.put("10004001_11_39", "603495");
		cancer_premium_map.put("10004001_11_40", "653949");
		cancer_premium_map.put("10004001_11_41", "706561");
		cancer_premium_map.put("10004001_11_42", "761326");
		cancer_premium_map.put("10004001_11_43", "817154");
		cancer_premium_map.put("10004001_11_44", "873290");
		cancer_premium_map.put("10004001_11_45", "929410");
		cancer_premium_map.put("10004001_11_46", "984882");
		cancer_premium_map.put("10004001_11_47", "1039178");
		cancer_premium_map.put("10004001_11_48", "1091910");
		cancer_premium_map.put("10004001_11_49", "1142893");
		cancer_premium_map.put("10004001_11_50", "1191801");
		cancer_premium_map.put("10004001_11_51", "1238720");
		cancer_premium_map.put("10004001_11_52", "1283496");
		cancer_premium_map.put("10004001_11_53", "1327176");
		cancer_premium_map.put("10004001_11_54", "1369356");
		cancer_premium_map.put("10004001_11_55", "1410735");
		cancer_premium_map.put("10004001_11_56", "1451454");
		cancer_premium_map.put("10004001_11_57", "1491244");
		cancer_premium_map.put("10004001_11_58", "1529887");
		cancer_premium_map.put("10004001_11_59", "1567250");
		cancer_premium_map.put("10004001_11_60", "1602868");
		cancer_premium_map.put("10004001_11_61", "1637220");
		cancer_premium_map.put("10004001_11_62", "1669392");
		cancer_premium_map.put("10004001_11_63", "1700094");
		cancer_premium_map.put("10004001_11_64", "1729346");
		cancer_premium_map.put("10004001_11_65", "1756774");
		cancer_premium_map.put("10004002_10_16", "114156");
		cancer_premium_map.put("10004002_10_17", "124092");
		cancer_premium_map.put("10004002_10_18", "135442");
		cancer_premium_map.put("10004002_10_19", "148186");
		cancer_premium_map.put("10004002_10_20", "163029");
		cancer_premium_map.put("10004002_10_21", "179563");
		cancer_premium_map.put("10004002_10_22", "197736");
		cancer_premium_map.put("10004002_10_23", "217998");
		cancer_premium_map.put("10004002_10_24", "241302");
		cancer_premium_map.put("10004002_10_25", "266846");
		cancer_premium_map.put("10004002_10_26", "296816");
		cancer_premium_map.put("10004002_10_27", "330137");
		cancer_premium_map.put("10004002_10_28", "368838");
		cancer_premium_map.put("10004002_10_29", "413540");
		cancer_premium_map.put("10004002_10_30", "464862");
		cancer_premium_map.put("10004002_10_31", "524610");
		cancer_premium_map.put("10004002_10_32", "592189");
		cancer_premium_map.put("10004002_10_33", "668138");
		cancer_premium_map.put("10004002_10_34", "751882");
		cancer_premium_map.put("10004002_10_35", "842972");
		cancer_premium_map.put("10004002_10_36", "940644");
		cancer_premium_map.put("10004002_10_37", "1044559");
		cancer_premium_map.put("10004002_10_38", "1155478");
		cancer_premium_map.put("10004002_10_39", "1274609");
		cancer_premium_map.put("10004002_10_40", "1403490");
		cancer_premium_map.put("10004002_10_41", "1543689");
		cancer_premium_map.put("10004002_10_42", "1696590");
		cancer_premium_map.put("10004002_10_43", "1863752");
		cancer_premium_map.put("10004002_10_44", "2044092");
		cancer_premium_map.put("10004002_10_45", "2235824");
		cancer_premium_map.put("10004002_10_46", "2436179");
		cancer_premium_map.put("10004002_10_47", "2641403");
		cancer_premium_map.put("10004002_10_48", "2846743");
		cancer_premium_map.put("10004002_10_49", "3047871");
		cancer_premium_map.put("10004002_10_50", "3241002");
		cancer_premium_map.put("10004002_10_51", "3423492");
		cancer_premium_map.put("10004002_10_52", "3596702");
		cancer_premium_map.put("10004002_10_53", "3763059");
		cancer_premium_map.put("10004002_10_54", "3928459");
		cancer_premium_map.put("10004002_10_55", "4101089");
		cancer_premium_map.put("10004002_10_56", "4289311");
		cancer_premium_map.put("10004002_10_57", "4499246");
		cancer_premium_map.put("10004002_10_58", "4734296");
		cancer_premium_map.put("10004002_10_59", "4996566");
		cancer_premium_map.put("10004002_10_60", "5284693");
		cancer_premium_map.put("10004002_10_61", "5595925");
		cancer_premium_map.put("10004002_10_62", "5927620");
		cancer_premium_map.put("10004002_10_63", "6277692");
		cancer_premium_map.put("10004002_10_64", "6644617");
		cancer_premium_map.put("10004002_10_65", "7028557");
		cancer_premium_map.put("10004002_11_16", "137043");
		cancer_premium_map.put("10004002_11_17", "145567");
		cancer_premium_map.put("10004002_11_18", "156394");
		cancer_premium_map.put("10004002_11_19", "169330");
		cancer_premium_map.put("10004002_11_20", "185159");
		cancer_premium_map.put("10004002_11_21", "203229");
		cancer_premium_map.put("10004002_11_22", "223916");
		cancer_premium_map.put("10004002_11_23", "248184");
		cancer_premium_map.put("10004002_11_24", "275469");
		cancer_premium_map.put("10004002_11_25", "306465");
		cancer_premium_map.put("10004002_11_26", "341877");
		cancer_premium_map.put("10004002_11_27", "382033");
		cancer_premium_map.put("10004002_11_28", "426760");
		cancer_premium_map.put("10004002_11_29", "476639");
		cancer_premium_map.put("10004002_11_30", "530914");
		cancer_premium_map.put("10004002_11_31", "589288");
		cancer_premium_map.put("10004002_11_32", "652038");
		cancer_premium_map.put("10004002_11_33", "717874");
		cancer_premium_map.put("10004002_11_34", "787411");
		cancer_premium_map.put("10004002_11_35", "860517");
		cancer_premium_map.put("10004002_11_36", "938714");
		cancer_premium_map.put("10004002_11_37", "1022321");
		cancer_premium_map.put("10004002_11_38", "1111541");
		cancer_premium_map.put("10004002_11_39", "1206989");
		cancer_premium_map.put("10004002_11_40", "1307898");
		cancer_premium_map.put("10004002_11_41", "1413122");
		cancer_premium_map.put("10004002_11_42", "1522651");
		cancer_premium_map.put("10004002_11_43", "1634308");
		cancer_premium_map.put("10004002_11_44", "1746579");
		cancer_premium_map.put("10004002_11_45", "1858820");
		cancer_premium_map.put("10004002_11_46", "1969763");
		cancer_premium_map.put("10004002_11_47", "2078355");
		cancer_premium_map.put("10004002_11_48", "2183819");
		cancer_premium_map.put("10004002_11_49", "2285786");
		cancer_premium_map.put("10004002_11_50", "2383602");
		cancer_premium_map.put("10004002_11_51", "2477439");
		cancer_premium_map.put("10004002_11_52", "2566991");
		cancer_premium_map.put("10004002_11_53", "2654351");
		cancer_premium_map.put("10004002_11_54", "2738711");
		cancer_premium_map.put("10004002_11_55", "2821469");
		cancer_premium_map.put("10004002_11_56", "2902908");
		cancer_premium_map.put("10004002_11_57", "2982487");
		cancer_premium_map.put("10004002_11_58", "3059773");
		cancer_premium_map.put("10004002_11_59", "3134499");
		cancer_premium_map.put("10004002_11_60", "3205735");
		cancer_premium_map.put("10004002_11_61", "3274440");
		cancer_premium_map.put("10004002_11_62", "3338784");
		cancer_premium_map.put("10004002_11_63", "3400187");
		cancer_premium_map.put("10004002_11_64", "3458691");
		cancer_premium_map.put("10004002_11_65", "3513547");

	}

	public enum ProductsInVi {
		SH("Bao hiem suc khoe ca nhan"), TW("Bao hiem xe may"), TL("An tam tron ven"), PA("Bao hiem tai nan"),
		CC("Bao hiem ung thu"), FH("Bao hiem suc khoe gia đinh");

		String name;

		private ProductsInVi(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	
	public enum Products {
		SINGLE_HEALTH("Single Health Protection", 1, "1001", "10001"),
		TWO_WHEELER("Motor Protection", 2, "1003", "10002"),
		PERSONAL_ACCIDENT("Personal Accident Protection", 3, "1002", "10005"),
		TERM_LIFE("Term Life Protection", 4, "1004", "20001"), CANCER_CARE("Cancer Care", 5, "1001", "10004"),
		FAMILY_HEALTH("Family Health", 6, "1001", "10003"), UVL_SAVINGS("UVL Savings", 7, null, null),
		UVL_HEALTH("UVL Health", 8, null, null),DENGUE("Dengue", 9, "1001", "10011");

		int id;
		String name;
		String categoryId;
		String productId;

		Products(String name, int id, String categoryId, String productId) {
			this.id = id;
			this.name = name;
			this.categoryId = categoryId;
			this.productId = productId;
		}

		public String getProductId() {
			return this.productId;
		}

		public String getName() {
			return this.name;
		}

		public static String getProductName(String productName) {
			for (int i = 0; i < Products.values().length; i++) {
				if (Products.values()[i].getName().equals(productName)) {
					return Products.values()[i].getName();
				}
			}
			return null;
		}

		public static String getCategoryId(String productName) {
			for (int i = 0; i < Products.values().length; i++) {
				if (Products.values()[i].getName().equals(productName)) {
					return Products.values()[i].categoryId;
				}
			}
			return null;
		}

		public static String getProductId(String productName) {
			for (int i = 0; i < Products.values().length; i++) {
				if (Products.values()[i].getName().equals(productName)) {
					return Products.values()[i].productId;
				}
			}
			return null;
		}

	}

	public static final String LOGIN = "logged_in";
	public static final String ONBOARD = "onboarded";
	public static final String ENQUIRY = "initial_enquiry";
	public static final String SH_ENQ = "sh_enquiry";
	public static final String TW_ENQ = "tw_enquiry";
	public static final String TL_ENQ = "tl_enquiry";
	public static final String PA_ENQ = "pa_enquiry";
	public static final String CC_ENQ = "ca_enquiry";
	public static final String FH_SEL = "fh_family_selection";
	public static final String FH_ENQ = "fh_enquiry";
	public static final String FH_FAM_ENQ = "fh_family_declare";

	public static final String GPA2 = "GPA2";
	public static final String GPA1 = "GPA1";
	public static final String GFH2 = "GFH2";
	public static final String GFH1 = "GFH1";
	public static final String GTL2 = "GTL2";
	public static final String GTL1 = "GTL1";
	public static final String GTW2 = "GTW2";
	public static final String GTW1 = "GTW1";
	public static final String GCC2 = "GCC2";
	public static final String GCC1 = "GCC1";
	public static final String GSH3 = "GS3";
	public static final String GSH2 = "GS2";
	public static final String GSH1 = "GS1";
	public static final String S4 = "S4";
	public static final String S3 = "S3";
	public static final String S2 = "S2";
	public static final String S1 = "S1";
	
	public enum PremiumType {
		BASIC_PREMIUM(1), ADDITIONAL_CRITICAL_PREMIUM(2), PROTECTION_PREMIUM(3);

		int id;

		PremiumType(int id) {
			this.id = id;
		}

		public int getPremiumType() {
			return this.id;
		}
	}

	public enum Plans {
		Basic("Cơ Bản", 1), Advance("Nâng Cao", 2), Superior("", 3);

		int id;
		String vietnameName;

		Plans(String vietnameName, int id) {
			this.id = id;
			this.vietnameName = vietnameName;
		}

		public int getPlanId() {
			return this.id;
		}

		public String getVietnameName() {
			return this.vietnameName;
		}
	}

	public enum Gender {
		Male(10,"Nam"), Female(11,"Nữ");
		int id;
		String vietnameName;
		Gender(int id,String vietnameName) {
			this.id = id;
			this.vietnameName = vietnameName;
		}
		public int getGenderId() {
			return this.id;
		}
		public String getVietnameName() {
			return this.vietnameName;
		}
		
		public static String getVietnameName(String engName) {
			for (int i = 0; i < Gender.values().length; i++) {
				Gender gender = Gender.values()[i];
				if(gender.name().equals(engName)) {
					return gender.getVietnameName();
				}
			}
			
			return "";
		}

	}

	public static final String FPT_IS_FILE_UPLOADED = "fpt_isFileUploaded";
	public static final String FPT_ISAUTHORIZE = "fpt_isAuthorize";
	public static final String FPT_RESENT_OPT_SEND = "fpt_isResentOTPSend";
	public static final String FPT_OPT_SEND = "fpt_isOTPSend";
	public static final String FPT_BILL_CODE = "fpt_billcode";
	public static final String FPT_STATE = "fpt_state";
	public static final String FPT_STATUS = "fpt_status";
	public static final String FPT_RESPONSE_MESSAGE = "fpt_response_message";

	public static final String FPT_ESIGN_DOC_PATH_ = "fpt_esign_doc_path_";
	public static final String FPT_ESIGN_DOC_ID_ = "fpt_esign_doc_id_";
	public static final Integer FPT_ESIGN_DOCS_COUNT = 3;
	public static final String IS_FPT_COMPLETED = "is_fpt_completed";
	
	

//	public static final String FPT_REMAINING_COUNTER = "fpt_remaining_counter";

	public static final Integer FPT_SUCCESS_STATUS = 0;
	public static final Integer FPT_AGREMENT_EXISTS_STATUS = 1010;
	public static final Integer FPT_FILE_UPLOAD_SUCCESS_STATUS = 1007;

	public static final Integer FPT_FILE_IS_BEING_PROCESSED_STATUS = 1016;
	public static final Integer FPT_BILLCODE_TIMEOUT_STATUS = 1015;

	public static final String FPT_RETRY_MESSAGE = "Retry";
	public static final String FPT_BILLCODE_TIME_OUT_MESSAGE = "FPT billcode time out";

	public static final Integer FPT_INIT_STATE = 1;
	public static final Integer FPT_GENERATE_FILE_STATE = 2;
	public static final Integer FPT_AUTHORIZE_STATE = 3;
	public static final Integer FPT_RESENT_OTP_STATE = 4;
	public static final Integer FPT_FILE_DOWNLOAD_STATE = 5;

	public static final String FPT_IS_SHOW_VERIFY_OTP = "isShowVerifyOTP";

	public static final String FPT_TL_APPLICAION_FORM = "application-form";
	public static final String FPT_TL_ILLUSTRATION_FORM = "illustration_tl_form";
	public static final String FPT_TL_INSURANCE_FORM = "insurance-form";

	public static final String FPT_AUTH_MESSAGE = "fpt_auth_message";
	public static final String FPT_AUTH_SUBJECT = "fpt_auth_subject";

	public static final String FPT_IS_PROFILE_SUBMITTED_FOR_CERTIFICATE = "fptIsProfileSubmittedForCertificate";
	public static final String FPT_AGREEMENT_UUID = "fptAgreementUuid";
	public static final Boolean FPT_P2P_ENABLED = true;
	public static final String FPT_BILLCODE_UPLOAD = "fptBillcodeUpload";
	public static final String FPT_AUTH_STATUS_CODE = "fptAuthStatusCode";
	public static final String FPT_BILLCODE_AUTHORIZATION = "fptBillcodeAuthorization";
	public static final String FPT_BILLCODE_SIGNED_CONTRACT = "fptBillcodeSignedContract";

	public static final String FPT_DMS_DOC_ID_OF_SIGNED_CONTRACT = "fptDmsDocIdOfSignedContract";
	public static final String FPT_IS_USER_AUTHORIZED = "fptIsUserAuthorized";
	public static final String FPT_IS_SIGNED_CONTRACT_DOWNLOADED = "fptIsSignedContractDownloaded";
	public static final String FPT_SIGNED_CONTRACT_URL = "fptSignedContractUrl";
	public static final String FPT_NUMBER_OF_DOCUMENT_FOR_SIGNATURE = "fptNumberOfDocumentForSignature";
	public static final String FPT_NUMBER_OF_DOCUMENT_AFTER_SIGNATURE = "fptNumberOfDocumentAfterSignature";
	public static final String FPT_DMS_URL = "fptDmsUrl";
	public static final String FPT_DMS_DOC_INDEX = "fptDmsDocIndex";
	public static final String FPT_UPLOAD_STATUS_CODE = "fptUploadStatusCode";
	public static final String FPT_SUBMIT_DATA_MESSAGE = "fptSubmitDataMessage";
	public static final String FPT_SUBMIT_STATUS_CODE = "fptSubmitStatusCode";
	public static final String FPT_DOWNLOAD_STATUS_CODE = "fptDownloadStatusCode";
	public static final String FPT_DOWNLOAD_STATUS_MESSAGE = "fptDownloadStatusMessage";
	public static final String FPT_OTP_MESSAGE = "fptOtpMessage";
	public static final String FPT_OTP = "fptOtp";
	public static final String FPT_REGENERATED_OTP = "fptRegeneratedOtp";
	public static final String FPT_REMAINING_COUNTER = "fpt_remaining_counter";
	public static final String FPT_REGENERATED_OTP_STATUS = "fptRegeneratedOtpStatus";
	public static final String FPT_REGENERATED_OTP_MESSAGE = "fptRegeneratedOtpMessage";
	public static final String FPT_RECEIPT_CODE = "fptReceiptCode";

	public static final String E_SIGN_LOAN_AGREEMENT_KEY = "esignLoanAgreementButton";
	public static final String E_SIGN_LOAN_AGREEMENT_DOCUMENT_LINK_KEY = "signedLoanAgreement";
	public static final String E_SIGN_LOAN_INSURANCE_AGREEMENT_DOCUMENT_LINK_KEY = "signedLoanInsuranceAgreement";
	public static final String E_SIGN_COMPLETE_STATUS_KEY = "eSignCompleteStatus";
	public static final String FE_CREDIT_CARD_EMBOSSING_NAME = "enterName";
	public static final String FE_CREDIT_CARD_EMBOSSING_NAME_IN_VN = "embossingNameInVN";
	public static final String E_SIGN_OTP_KEY = "eSignOtp";
	public static final String E_SIGN_OTP_DATETIME_KEY = "eSignOtpDatetime";

	public static final String FPT_FILE_FILE_ONE = "e_sign_fpt_file";
	public static final String E_SIGN_FILE_EXTENSION = ".pdf";
	public static final String E_SIGN_FILE_DIRECTORY = "/home/aravind.selvam/work/fpt/";

	public static final Object LONGITUDE_KEY = "LONGITUDE_KEY";

	public static final Object LATITUDE_KEY = "LATITUDE_KEY";
	public static final Object CUSTOMER_IP_ADDRESS = "CUSTOMER_IP_ADDRESS";

	public static final String RELATIONSHIP_LABEL = "relationship_label";
	public static final Map<String, String> FAMILY_RELATION = new HashMap<String, String>();
	static {
		FAMILY_RELATION.put("You_Male", "You");
		FAMILY_RELATION.put("my spouse_Female", "Your wife");
		FAMILY_RELATION.put("my spouse_Male", "Your Husband");
		FAMILY_RELATION.put("child_Male", "Your son");
		FAMILY_RELATION.put("child_Female", "Your daughter");
		FAMILY_RELATION.put("parents_Male", "Your Father");
		FAMILY_RELATION.put("parents_Female", "Your mother");
	}
	public static final Map<String, String> FAMILY_RELATION_VI = new HashMap<String, String>();
	static {
		FAMILY_RELATION_VI.put("You_Nam", "Bạn");
		FAMILY_RELATION_VI.put("my spouse_Nữ", "Vợ bạn");
		FAMILY_RELATION_VI.put("my spouse_Nam", "Chồng của bạn");
		FAMILY_RELATION_VI.put("child_Nam", "Con trai bạn");
		FAMILY_RELATION_VI.put("child_Nữ", "Con gái bạn");
		FAMILY_RELATION_VI.put("parents_Nam", "Bố của bạn");
		FAMILY_RELATION_VI.put("parents_Nữ", "Mẹ bạn");
	}

	public static final Object YOU = "You";
	public static final Object DOB = "dob";
	public static final Object YES = "Yes";
	public static final String TL_POLICY_NUMBER_PREFIX = "GCN";
	/**
	 * Portal dashboard
	 * 
	 **/
	// Customer Dashboard

	public static final String CUSTOMER_POLICY_INFO_RESPONSE_IDENTIFIER ="customer_policy_information_response_identifier";
	public static final String ACTION = "action";
	public static final String CUSTOMER_ID = "customer_id";
	public static final String CUSTOMER_EMAIL = "email_id";
	public static final String CUSTOMER_NAME = "customer_name";
	public static final String CUSTOMER_GENDER = "customer_gender";
	public static final String CUSTOMER_DOB = "date_of_birth";
	public static final String CUSTOMER_SEX = "customer_sex";
	public static final String CUSTOMER_LAST_UPDATE_DATE = "last_update_date";
	public static final String CUSTOMER_LAST_UPDATE_TIME = "last_update_time";
	public static final String CUSTOMER_ADDRESS = "customer_address";
	public static final String CUSTOMER_ADDRESS_LINE1 = "customer_address_line1";
	public static final String CUSTOMER_ADDRESS_LINE2 = "customer_address_line2";
	public static final String CUSTOMER_ADDRESS_PROVINCE = "province";
	public static final String CUSTOMER_ADDRESS_CITY = "city";
	public static final String CUSTOMER_ADDRESS_STATE= "state";
	public static final String CUSTOMER_ADDRESS_COUNTRY= "country";
	public static final String CUSTOMER_MOBILE_NUMBER = "mobile_Number";
	public static final String CUSTOMER_NATIONAL_ID = "national_Id";
	public static final String CUSTOMER_DETAILS = "customer_details";
	
	//Policy Dashboard
	public static final String POLICY_DOCUMENT = "policy_document";
	public static final String SERIAL_NO = "sl_no";
	public static final String PORTAL_POLICY_START_DATE = "policy_start_date";
	public static final String PORTAL_POLICY_END_DATE = "policy_end_date";
	public static final String POLICY_RENEWAL_DATE = "policy_renewal_date";
	public static final String POLICY_NEXT_INSTALLMENT_DATE = "next_installment_date";
	public static final String POLICY_INSURER = "policy_insurer";
	public static final String POLICY_SUM_ASSURED = "policy_sum_assured";
	public static final String TOTAL_RECORDS = "total_records";
	public static final String POLICY_PRODUCT_NAME = "policy_product_name";
	public static final String POLICY_PREMIUM_AMOUNT = "policy_premium_amount";
	public static final String POLICY_COVERAGE_AMOUNT = "policy_coverage_amount";
	public static final String POLICY_CUSTOMER_ADDRESS = "policy_customer_address";
	
	//Search Dashboard 		
	public static final String SEARCH_DASHBOARD_CUSTOMER = "search_customer_dashboard";
	public static final String CUSTOMER_SEARCH_BY_NAME = "search_by_customer_name";
	public static final String CUSTOMER_SEARCH_BY_MOBILE_NO = "search_by_customer_mobile_number";
	public static final String CUSTOMER_SEARCH_BY_EMAIL = "search_by_customer_email";
	public static final String CUSTOMER_SEARCH_BY_NID = "search_by_customer_nid";
	
	public static final String SEARCH_DASHBOARD_POLICY = "search_policy_dashboard";
	public static final String POLICY_SEARCH_BY_POLICY_NO = "search_by_policy_number";
	public static final String POLICY_SEARCH_BY_PLAN_ID = "search_by_plan_id";
	public static final String POLICY_SEARCH_BY_PLAN_NAME = "search_by_plan_name";
	public static final String POLICY_SEARCH_BY_INSURER = "search_by_insurer";
	
	public static final String SEARCH_DASHBOARD_TRANSACTION = "search_transaction_dashboard";
	public static final String POLICY_SEARCH_BY_TRANSACTION_ID = "search_by_transaction_id";
	public static final String POLICY_SEARCH_BY_TRANSACTION_POLICY_NO = "search_by_transaction_policy_number";
	public static final String POLICY_SEARCH_BY_TRANSACTION_PLAN_ID = "search_by_transaction_plan_id";
	
	public static final String SEARCH_DASHBOARD_CLAIMS = "search_claims_dashboard";
	
	// Payment/Transction 
	
	public static final String TRANSACTION_PAYMENT_ID = "transaction_id";
	public static final String TRANSACTION_PAYMENT_METHOD = "payment_method";
	public static final String TRANSACTION_PAYMENT_STATUS = "transaction_status";
	public static final String TRANSACTION_PAYMENT_DATE = "payment_date";
	public static final String TRANSACTION_PAYMENT_AMOUNT = "payment_amount";
	public static final String TRANSACTION_DETAILS = "transaction_details";
	public static final String TRANSACTION_REMARKS = "transaction_remarks";
	//Claims 
	public static final String POLICY_CLAIMS_DETAILS = "policy_claims_detail";
	public static final String CLAIMS_APPLICATION_NUMBER = "claims_application_number";
	public static final String CLAIMS_ID =  "claim_id";
	public static final String CLAIMS_AMOUNT =  "claim_amount";
	public static final String CLAIMS_STATUS =  "claim_status";
	

	public static final String MIDDLE_SECTION_LISTING_TITLE = "middle_section_listing_title";
	public static final String CRITICAL_ILLNESS = "\\|Payment for Critical illness";
	public static final String CRITICAL_ILLNESS_VI = "\\|Quyền lợi bảo hiểm bênh lý nghiêm trọng";
	//uvl savings
	public static final String AGE = "age";
	public static final String INVSTMENT_AMOUNT = "investment_amount";
	public static final String PROTECTION_TERM = "protection_term";
	public static final String LOYALTI_BONUS = "loyality_bonus";
	public static final String TOTAL_PERMANENT = "total_permanant";
	public static final String TERMINAL_ILLNESS = "terminal_illness";
	public static final String DEATH_BENIFIT = "death_benefit";
	public static final String FINAL_INVESTMENT_VALUE = "final_investment_value";
	public static final String PAYMENT_OPTIONS = "paymentOptions";
	public static final String SEMI_ANNUALLY = "Semi-annually";
	public static final String QUARTERLY = "Quarterly";
	public static final String MONTHLY = "Monthly";
	public static final String BANCA_GET_UVL_QUOTE_DETAILS = "banca-get-uvs-quote-details";
	
	public static final String MOBILE_NUMBER_APPENDER_ZERO = "0";
	
	public static final Integer BENEFICIARY_MAX_COUNT = 3;
	
	public static final String UNDER_SCORE = "_";
	
	public static final String BENEFICIARY = "beneficiary";
	public static final String BENEFICIARY_NAME = "fullName";
	public static final String BENEFICIARY_DOB = "dob";
	public static final String BENEFICIARY_NID = "nid";
	public static final String BENEFICIARY_RELATION = "relation";
	
	public static final String BENEFICIARY_SHARE = "share";
	public static final String BENEFICIARY_GENDER = "gender";
	
	public static final String REF_CODE = "couponCode_user";

	public static final String NOMINEE_ROW = "<tr><td>{{nominated_name}}</td><td>{{nominated_dob}}</td><td>{{nominated_gender}}</td><td>{{nominated_nid}}</td><td>{{nominated_relation}}</td><td>{{nominated_share}}</td></tr>"; 
	
	public enum DocType {
		ESIGN_FPT(1);
		int docTypeId;

		DocType(int docTypeId) {
			this.docTypeId = docTypeId;
		}
		
		public int getDocTypeId() {
			return this.docTypeId;
		}
	}
	
	public static final String PAYMENT_REFRENCE = "paymentReference";
	public static final String AMOUNT_DETAILS = "amountDetails";
	
	public static final String CRITICAL_ILLNESS_PROTECTION_PLAN_ID = "1001";
	
	public static final String PROCESS_INSTANCE_ID = "process_instance_id";
	public static final String PA_BASIC_PLAN_ID = "10005001";
	public static final String PA_ADVANCE_PLAN_ID = "10005004";
	public static final String TL_PRODUCT_NAME = "Term Life Protection";
	
	public static final String INIT_STATUS = "INIT";
	public static final String CURRENT_JOURNEY_STAGE = "currentJourneyStage";
	public static final String PAYMENT_TYPE_CHECK = "payment_typeCheck";
	public static final String PAYMENT_KEY = "Payment";
	public static final String PAYOO = "Payoo";
	public static final String CHECKOUT_PA="checkoutPa";
	
	public static final String VTIGER_GENERIC_DROP_OFF_TYPE = "Generic";
	
	public static final String GENERIC_DROP_OFF_PUSH_NOTIFICATION_TITLE_EN = "Enrolment progress about to expire";
	public static final String GENERIC_DROP_OFF_PUSH_NOTIFICATION_TITLE_VN = "Phiên đăng ký của bạn sắp hết thời gian";

	public static final String GENERIC_DROP_OFF_FIRST_PUSH_NOTIFICATION_BODY_EN = "Hi <customer_name>, we only require a few more details to get you protected with our <product_name> protection. Click here to provide those final details - it won't take long!";
	public static final String GENERIC_DROP_OFF_FIRST_PUSH_NOTIFICATION_BODY_VN = "Chào khách hàng <customer_name>, chỉ cần cung cấp thêm một vài thông tin nữa là bạn sẽ mua thành công <product_name>. Hãy nhấn vào đây để tiếp tục!";

	public static final String GENERIC_DROP_OFF_SECOND_PUSH_NOTIFICATION_BODY_EN = "Hi <customer_name>, you have been busy? It's okay. Just a few more details and you will be protected with our <product_name> protection. Click here!";
	public static final String GENERIC_DROP_OFF_SECOND_PUSH_NOTIFICATION_BODY_VN = "Có vẻ như khách hàng <customer_name> đang hơi bận rộn đúng không? Chúng tôi vẫn đang chờ đợi bạn hoàn thành thông tin đăng ký <product_name>. Nhấn vào đây ngay để tiếp tục nhé!";

	public static final String CASH_COMPLETE_PAY_TITLE_EN = "Your <product_name> policy is issued";
	public static final String CASH_COMPLETE_PAY_BODY_EN = "Horray! We have received your payment and your policy has been issued!";
	public static final String CASH_COMPLETE_PAY_TITLE_VN = "Hợp đồng bảo hiểm <product_name> của bạn đã được cấp";
	public static final String CASH_COMPLETE_PAY_BODY_VN = "Thật tuyệt vời! Chúng tôi đã nhận được khoản thanh toán và hợp đồng bảo hiểm của bạn đã được phát hành.";
	public static final String CASH_PARTIAL_PAY_TITLE_EN = "Your payment is pending";
	public static final String CASH_PARTIAL_PAY_BODY_EN = "We have received your payment but the amount seems to be less than required";
	public static final String CASH_PARTIAL_PAY_TITLE_VN = "Thanh toán của bạn đang chờ xử lý";
	public static final String CASH_PARTIAL_PAY_BODY_VN = "Chúng tôi đã nhận được khoản thanh toán của bạn nhưng số tiền thanh toán đang ít hơn so với yêu cầu.";
	

	
	public static final String POLICY_ALREADY_EXISTS = "policy already exixts";
	public static final String NID_NOT_MATCHED = "nid not matched";
	public static final String USER_DETAILS_NULL = "some user fields are null";
	public static final String DMS_FAILED_OR_FILE_NOT_FOUND = "DMS failed or file not found";
	
	public static final String POLICY_DETAILS= "policyDetails";
	public static final String SOURCE_OF_POLICY = "source_of_policy";
	public static final String POLICY_MODE = "policy_mode";
	
	public static final String OFFLINE_POLICIES= "offlinePolicies";
	
	public static final Map<String, String> PRODUCT_NAME_VI = new HashMap<String, String>();
	static {
		PRODUCT_NAME_VI.put("Two Wheeler" , "Bảo hiểm xe máy");
		PRODUCT_NAME_VI.put("Family Health", "Bảo hiểm sức khoẻ gia đình");
		PRODUCT_NAME_VI.put("Term Life", "An tâm trọn vẹn");
		PRODUCT_NAME_VI.put("Single Health", "Bảo hiểm sức khoẻ cá nhân");
		PRODUCT_NAME_VI.put("Personal Accident", "Bảo hiểm tai nạn");
		PRODUCT_NAME_VI.put("Cancer Care", "Bảo hiểm ung thư");
		PRODUCT_NAME_VI.put("Dengue Fever", "Bảo hiểm Sốt xuất huyết");
		
	}
	
	public static final String DATE_MISMATCHED_OR_MOBILE_NUMBER = "date mismatched or check mobile Number";
	public static final String PRODUCT_STATUS = "productStatus";
	
	public static final String MOBILE_CHANGE_MOBILE_KEY="{NEW_MOBILE}";
	public static final String MOBILE_CHANGE_SMS_CONTENT="So dien thoai dang ky cua quy khach tren ung dung FE $HIELD da duoc doi thanh: {NEW_MOBILE}. Vui long luu y thong tin nay. Cam on quy khach!"; 

}
