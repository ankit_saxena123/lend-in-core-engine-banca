package com.kuliza.lending.wf_implementation.integrations;

import java.io.File;
import java.io.IOException;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.transform.TransformerException;

import omnidocsapi.SysRequestType;
import omnidocsapi.UploadDoc;
import omnidocsapi.UploadDocResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.wf_implementation.WfImplConfig;

@Component
public class UploadDocClient extends WebServiceGatewaySupport {
	@Autowired
	private WfImplConfig wfImplConfig;
	
	private static final Logger logger = LoggerFactory
			.getLogger(UploadDocClient.class);

	public UploadDocResponse uploadDoc(String docId, String appId,
			String moduleId, boolean renameFile, SysRequestType sys,
			MultipartFile file) throws IllegalStateException, IOException {
		logger.info("<==============> uploadDoc UploadDocClient <===============>");
		File tmpFile = new File(System.getProperty("java.io.tmpdir")
				+ System.getProperty("file.separator")
				+ file.getOriginalFilename());
		file.transferTo(tmpFile);
		return uploadDoc(docId, appId, moduleId, renameFile, sys, tmpFile);
	}

	public UploadDocResponse uploadDoc(String docId, String appId,
			String moduleId, boolean renameFile, SysRequestType sys,
			File tmpFile) throws IllegalStateException, IOException {
		try {
			logger.info("<============>uploadDoc<==================>");
			
			ObjectMapper map = new ObjectMapper();
			
			UploadDoc request = new UploadDoc();
			request.setSys(sys);
			logger.info("appId --->"+appId);
			request.setAppid(appId);
			request.setDocID(docId);
			request.setModuleID(moduleId);
			request.setRenameFile(renameFile);
                        logger.info("<====================>docId:"+docId+" appId :"+appId+" moduleId :"+moduleId+"<=================>");
			logger.info("<=========>file is :"+tmpFile.getName()+"<=============>");
			logger.info("Upload request : "+map.writeValueAsString(request));
			UploadDocResponse response = new UploadDocResponse();
			final SoapActionCallback soapActionCallback = new SoapActionCallback(
					"http://OmnidocsAPI/uploadDoc");
			response = (UploadDocResponse) getWebServiceTemplate()
					.marshalSendAndReceive(
							wfImplConfig.getOmnidocsEndPoint(),
							request, new WebServiceMessageCallback() {
								@Override
								public void doWithMessage(
										WebServiceMessage message)
										throws IOException,
										TransformerException {
									// TODO Auto-generated method stub
									if (message instanceof SaajSoapMessage) {
										SaajSoapMessage msg = (SaajSoapMessage) message;
										FileDataSource fileDS = new FileDataSource(
												tmpFile);
										DataHandler dataHandler = new DataHandler(
												fileDS);
										msg.addAttachment(tmpFile.getName()
												.toString(), dataHandler);
									}
								}
							});
			logger.info("Upload response : "+map.writeValueAsString(response));
			return response;
			
		} finally {
			if (tmpFile.exists()) {
				tmpFile.delete();
			}

		}

	}
}
