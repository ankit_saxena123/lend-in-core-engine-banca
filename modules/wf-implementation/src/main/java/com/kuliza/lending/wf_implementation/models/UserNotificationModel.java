package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "user_notification")
public class UserNotificationModel extends BaseModel {
	

	@Column(name="user_name", nullable = true)
	private String userName;
	
	@Column(name="national_id", nullable = true)
	private String nationalId;

	@Column(name="contact_number",nullable = true)
	private String contactNumber;
	
	@Column(name="application_id",nullable = true)
	private String applicationId;

	@Column(name="process_instance_id",nullable = true)
	private String processInstanceId;

	@Column(name="notification_message",nullable = true)
	private String notificationMessage;
	
	@Column(name="notification_detailed_message",nullable = false)
	private String notificationDetailedMessage;

	public UserNotificationModel() {
		super();
	}
	
	public UserNotificationModel(String nationalId, String contactNumber, String applicationId, String processInstanceId,
			String notificationMessage, String notificationDetailedMessage) {
		super();
		this.nationalId = nationalId;
		this.contactNumber = contactNumber;
		this.applicationId = applicationId;
		this.processInstanceId = processInstanceId;
		this.notificationMessage = notificationMessage;
		this.notificationDetailedMessage = notificationDetailedMessage;
			
	}
	
	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getNotificationDetailedMessage() {
		return notificationDetailedMessage;
	}

	public void setNotificationDetailedMessage(String notificationDetailedMessage) {
		this.notificationDetailedMessage = notificationDetailedMessage;
	}
	
	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
