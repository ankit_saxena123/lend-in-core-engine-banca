package com.kuliza.lending.wf_implementation.services;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.kuliza.lending.wf_implementation.utils.Constants.Products;

public class InsurerFactoryClass {
	private static final Logger logger = LoggerFactory.getLogger(InsurerFactoryClass.class);
	
	public static InsurerAbstract getInsurer(Map<String, Object> processvariable) throws Exception{
		String categoryId = Utils.getStringValue(processvariable.get(WfImplConstants.JOURNEY_CATEGORY_ID));
		String productId = Utils.getStringValue(processvariable.get(WfImplConstants.JOURNEY_PRODUCT_ID));
		String insurerName=Utils.getStringValue(processvariable.get(WfImplConstants.JOURNEY_INSURER_NAME));
		String paymentType=Utils.getStringValue(processvariable.get(WfImplConstants.JOURNEY_INSURER_PAYMENT_INFO));
		
		logger.info("categoryId :"+categoryId+" productId :"+productId+" insurerName :"+insurerName+" paymentType "+paymentType);
		if(productId.trim().equals(WfImplConstants.SH_PRODUCT_ID) || productId.trim().equals(Products.DENGUE.getProductId())){
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(InsurerService.class);
		}
		else if(insurerName.trim().equalsIgnoreCase(WfImplConstants.PVI_INSURER) && categoryId.trim().equals(WfImplConstants.TW_CATEGORY_ID) && productId.trim().equals(WfImplConstants.TW_PRODUCT_ID)){
			
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(PVIInsurerService.class);
		}
		else if (categoryId.trim().equals(WfImplConstants.PA_CATEGORY_ID) && productId.trim().equals(WfImplConstants.PA_PRODUCT_ID)){
			
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(PersonalAccidentInsurearService.class);
		}
		else if (categoryId.trim().equals(WfImplConstants.CC_CATEGORY_ID) && productId.trim().equals(WfImplConstants.CC_PRODUCT_ID)){
			
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(CancerCareInsurerService.class);
		}
		else if (categoryId.trim().equals(WfImplConstants.FH_CATEGORY_ID) && productId.trim().equals(WfImplConstants.FH_PRODUCT_ID)){
			
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(FamilyHealthInsurerService.class);
		}
		else if (categoryId.trim().equals(WfImplConstants.CV_CATEGORY_ID) && productId.trim().equals(WfImplConstants.CV_PRODUCT_ID)){
			
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(CovidInsurerService.class);
		}else if(productId.trim().equals(WfImplConstants.TL_PRODUCT_ID)) {
			return com.kuliza.lending.wf_implementation.StaticContextAccessor.getBean(TermLifeInsurerService.class);

		}

		
		else{
			throw new Exception("Unable to get Insurer Object");
		}
	}
}
