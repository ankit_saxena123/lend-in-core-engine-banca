package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.services.DashBoardService;

@RestController
@RequestMapping("/dashboard")
public class DashBoardController {
	
	@Autowired
	private DashBoardService dashBoardService;
	
	private static final Logger logger = LoggerFactory.getLogger(DashBoardController.class);
	
	@RequestMapping(method = RequestMethod.GET, value = "/get-dashboard")
	public ResponseEntity<Object> getDashBoard(Principal principal){
	
	try{
		return CommonHelperFunctions.buildResponseEntity(dashBoardService.getDashBoard(principal.getName()));
	}
	catch(Exception e){
		logger.error(CommonHelperFunctions.getStackTrace(e));
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
				Constants.INTERNAL_SERVER_ERROR_MESSAGE));
	}
	}

}
