package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowUserDocs;

@Repository
public interface WorkflowUserDocsDao extends CrudRepository<WorkFlowUserDocs, Long> {

	public WorkFlowUserDocs findById(long id);

	public WorkFlowUserDocs findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowUserDocs findByDocumentIdAndUserIdAndIsDeleted(String docId , String userId, boolean isDeleted);
}
