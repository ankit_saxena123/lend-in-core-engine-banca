package com.kuliza.lending.wf_implementation.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.services.FECreditCMSPostTransactionService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping(value="/fe-credit")
public class FECreditController {
	
	@Autowired
	private FECreditCMSPostTransactionService feCreditCMSPostTransactionService;
	
	@RequestMapping(value="/call-insurer")
	public ApiResponse callInsurer(@RequestBody Map<String, String> callInsurerMap) throws Exception {
		
		if(callInsurerMap == null || callInsurerMap.get(Constants.PROCESS_INSTANCE_ID) == null) {
			throw new FECException(ErrorCodes.INVALID_PROC_ID);
		}
		
		return feCreditCMSPostTransactionService.callInsurerAPIByProcId(callInsurerMap.get(Constants.PROCESS_INSTANCE_ID));
	}

}
