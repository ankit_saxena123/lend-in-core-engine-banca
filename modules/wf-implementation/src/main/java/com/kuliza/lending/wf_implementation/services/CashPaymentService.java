package com.kuliza.lending.wf_implementation.services;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.history.HistoricProcessInstance;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.exception.ProcessInstanceDoesNotExistException;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaSMSIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service("cashPaymentService")
public class CashPaymentService {
	private static final Logger logger = LoggerFactory.getLogger(CashPaymentService.class);

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	private IPATPaymentInfoService IpatInfoService;

	@Autowired
	private FECreditBancaSMSIntegration feCreditBancaSMSIntegration;

	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;

	@Autowired
	private HistoryService historyService;

	@SuppressWarnings("unchecked")
	public ApiResponse cashPayment(String userName, String processInstanceId)
			throws ProcessInstanceDoesNotExistException, Exception {
		logger.info("********CashPayment******* : ");
		String crm_id = null;
		Map<String, Object> IpatResponse = new HashMap<String, Object>();
		try {
			ApiResponse IPATPayInfoServiceResponse = IpatInfoService.generatePaymentBillInfo(userName,processInstanceId);
			IpatResponse = (Map<String, Object>) IPATPayInfoServiceResponse.getData();
			logger.info("APIResponse from IPATINFO=======>>>>>> : " + IpatResponse);
			crm_id = Utils.getStringValue(IpatResponse.get("ipat_crmId"));
			logger.info("crm_id=======>>>>>> : " + crm_id);
			// return sendSms(crm_id,
			// processInstanceId,Constants.SMS_CASH_PAY_INITIATE,false );
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.IPAT_PAYMENT_INFO_ADDED_MESSAGE);
		} catch (Exception e) {
			logger.error("error in cashPayment : " + e);
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.FAILURE_MESSAGE,
					"cash payment not available");

		}

	}

	public ApiResponse sendSms(String crmId, String processInstanceId, String smsContent, boolean isHistory) {
		logger.info("Sending SMS crmId : " + crmId + "processInstanceID:  " + processInstanceId);
		try {
			Map<String, Object> variables = new HashMap<String, Object>();
			if (isHistory) {
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
				variables = processInstance.getProcessVariables();
			} else {
				variables = runtimeService.getVariables(processInstanceId);
			}

			// Map<String, Object> variables =
			// runtimeService.getVariables(processInstanceId);
			Map<String, Object> requestMap = new HashMap<String, Object>();
			String mobileNumber = Utils.getStringValue(variables.get(WfImplConstants.APPLICATION_MOBILE_NUMBER));
			logger.info("<===========> mobileNumber <===========>" + mobileNumber);
			String whatYouPay = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT))
					.replace("triệu", "trieu");
			logger.info("<===========> protectionAmount <===========>" + whatYouPay);
			String productName = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_NAME));
			String crm_id = crmId;
			logger.info("<===========> crm_id <===========>" + crm_id);
			String content = smsContent;
			if (mobileNumber != null)
				mobileNumber = mobileNumber.trim();
			if (productName != null)
				productName = productName.trim();
			if (whatYouPay != null)
				whatYouPay = whatYouPay.trim();
			if (crm_id != null)
				crm_id = crm_id.trim();
			content = content.replace("${crm_id}", crm_id).replace("${sum_insured}", whatYouPay);
			content = decode(content);
			logger.info("<===========> SMSContent <===========>" + content);

			if (mobileNumber.startsWith("+84")) {
				mobileNumber = mobileNumber.substring(3, mobileNumber.length());
			}
			logger.info("<===========> mobileNumber <===========>" + mobileNumber);
			requestMap.put(Constants.MOBILE_NUMBER_KEY, mobileNumber);
			requestMap.put("message", content);
			JSONObject request = feCreditBancaSMSIntegration.buildRequestPayload(requestMap);
			HttpResponse<JsonNode> apiResponse = feCreditBancaSMSIntegration.getDataFromService(request);
			logger.info("<===========> sms trigger apiResponse <===========>" + apiResponse);
			if (apiResponse != null) {
				Map<String, Object> parseResponse = feCreditBancaSMSIntegration
						.parseResponse(apiResponse.getBody().getObject());
				logger.error("<=== sms send successfully ===>");

				logger.info("<===========> sms parseResponse <===========>" + parseResponse);
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						parseResponse.get(Constants.FE_CREDIT_MVP_SMS_DESCRIPTION_KEY));
			} else {
				logger.error("<=== failed to send sms ===>");
				return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.FAILURE_MESSAGE,
						"failed to send sms");
			}
		} catch (Exception e) {
			logger.error("<===========> error while sending sms <===========>" + e.getMessage());
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.FAILURE_MESSAGE,
					"error in sending sms: " + e);
		}
	}

	public String decode(String str) throws Exception {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	public DelegateExecution cashPaymentServiceTask(DelegateExecution delegateExecution) {
		logger.info("<===========> cashPaymentServiceTask <===========>");
		Map<String, Object> variables = delegateExecution.getVariables();
		String pid = delegateExecution.getProcessInstanceId();
		try {
			List<IPATPaymentConfirmModel> ipatConfirmPaymentModelList = ipatPaymentConfirmDao
					.findByProcessInstanceIdAndIsDeletedOrderByIdAsc(pid, false);

			String crmId = ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1).getCrmId();
			String whatYouPay = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			logger.info(
					"<===========> crmId and whatyouPay <===========>: " + crmId + "  " + whatYouPay + "PID: " + pid);
			delegateExecution.setVariable(Constants.PAYMENT_REFRENCE, crmId);
			delegateExecution.setVariable(Constants.AMOUNT_DETAILS, whatYouPay);
		} catch (Exception e) {

			logger.error("<===========> error cashPaymentServiceTask <===========>" + e.getMessage());
		}
		return delegateExecution;

	}

}
