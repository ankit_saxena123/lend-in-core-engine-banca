/**
 * 
 */
package com.kuliza.lending.wf_implementation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.services.DashBoardDocumentService;
import com.kuliza.lending.wf_implementation.services.PortalDashboardChatSupportService;
import com.kuliza.lending.wf_implementation.services.PortalDashboardPasswordService;
import com.kuliza.lending.wf_implementation.services.PortalDashboardSearchService;
import com.kuliza.lending.wf_implementation.services.PortalDashboardService;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.PortalDashboardAuthenticationConstants;

/**
 * @author Garun Mishra
 * @Date 2020-03-03
 * @Description This control file is use to write down the service to to get
 *              records as per Portal Dash board requirements.
 */

@RestController
@RequestMapping("/portal-dashboard")
public class PortalDashBoardController {

	private static final Logger logger = LoggerFactory.getLogger(PortalDashBoardController.class);

	@Autowired
	private PortalDashboardService portalDashboardService;

	@Autowired
	private PortalDashboardSearchService portalDashboardSearchService;

	@Autowired
	private PortalDashboardPasswordService portalDashboardPasswordService;

	@Autowired
	DashBoardDocumentService dashBoardDocumentService;

	@Autowired
	PortalDashboardChatSupportService portalDashboardChatSupportService;

	@RequestMapping(method = RequestMethod.GET, value = "/customers")
	public ApiResponse getAllCustomers(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) throws Exception {
		return portalDashboardService.getAllCustomers(pageNo, pageSize);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/customers/getCustomerInfo/{customer_id}")
	public ApiResponse getCustomerInfo(@PathVariable(value = "customer_id") long id) throws Exception {
		return portalDashboardService.getCustomerInfoDetails(id, "");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/customers/getCustomerPolicyDetails/{customer_id}")
	public ApiResponse getCustomerPolicyDetails(@PathVariable(value = "customer_id") long CustomerId) throws Exception {
		String customer_policy_response_identifier = Constants.CUSTOMER_POLICY_INFO_RESPONSE_IDENTIFIER;
		return portalDashboardService.getClaimsAndPolicyDetailsDetails(CustomerId, customer_policy_response_identifier);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/customers/getCustomerClaimsDetails/{customer_id}")
	public ApiResponse getCustomerClaimsDetails(@PathVariable(value = "customer_id") long CustomerId) throws Exception {
		String customer_policy_response_identifier = "";
		return portalDashboardService.getClaimsAndPolicyDetailsDetails(CustomerId, customer_policy_response_identifier);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/policieslist")
	public ApiResponse getAllPolicies(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) throws Exception {
		return portalDashboardService.getAllPolicies(pageNo, pageSize);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/policieslist/getPolicyInfo")
	public ApiResponse getPolicyInfo(@RequestParam("policy_number") String policy_number) throws Exception {
		return portalDashboardService.getPolicyDetails(policy_number);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/policieslist/customers/getCustomerInfo")
	public ApiResponse getPolicyCustomerInfo(@RequestParam("policy_number") String policy_number) throws Exception {
		long id = 0;
		return portalDashboardService.getCustomerInfoDetails(id, policy_number);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/policieslist/customers/getPolicyTransactionInfo")
	public ApiResponse getPolicyTransactionInfo(@RequestParam("policy_number") String policy_number) throws Exception {
		String transactionId = "";
		return portalDashboardService.getTransactionInfo(transactionId, policy_number);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/transactions")
	public ApiResponse getAllTransaction(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) throws Exception {
		return portalDashboardService.getAllTransaction(pageNo, pageSize);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/transactions/transactionInfo")
	public ApiResponse getTransactionDetails(@RequestParam("transaction_id") String transaction_id) throws Exception {
		String policy_number = "";
		return portalDashboardService.getTransactionInfo(transaction_id, policy_number);
	}

//Claims Dashboard api
	@RequestMapping(method = RequestMethod.GET, value = "/get-claims")
	public ApiResponse getAllClaims(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) throws Exception {
		return portalDashboardService.getAllClaims(pageNo, pageSize);
	}

	// Claims details api
	@RequestMapping(method = RequestMethod.GET, value = "/get-claims/claimDetails")
	public ApiResponse getClaimDetails(@RequestParam String subDashBoardName,
			@RequestParam("process_instance_id") String processInstanceId) throws Exception {
		ApiResponse response = null;
		switch (subDashBoardName) {
		case PortalDashboardAuthenticationConstants.CLAIM_SUB_DASHBOARD_CUSTOMER:
			response = portalDashboardService.getClaimCustomerDetails(processInstanceId);
			break;
		case PortalDashboardAuthenticationConstants.CLAIM_SUB_DASHBOARD_POLICY:
			response = portalDashboardService.getClaimPolicyDetails(processInstanceId);
			break;
		case PortalDashboardAuthenticationConstants.CLAIM_SUB_DASHBOARD_CLAIM_INFO:
			response = portalDashboardService.getClaimInfoDetails(processInstanceId);
			break;

		case PortalDashboardAuthenticationConstants.CLAIM_SUB_DASHBOARD_CLAIM_DOCS:
			response = dashBoardDocumentService.getClaimDocDetails(processInstanceId);
			break;
		case PortalDashboardAuthenticationConstants.CLAIM_SUB_DASHBOARD_CLAIM_STATUS:
			response = dashBoardDocumentService.getClaimStatusDetails(processInstanceId);
			break;
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-claims/claimDetails/docStatus")
	public ApiResponse claimDocumentStatus(@RequestParam("process_instance_id") String processInstanceId,
			@RequestParam("document_number") String document_number,
			@RequestParam("document_status") String document_status) throws Exception {

		return dashBoardDocumentService.claimDocumentStatus(processInstanceId, document_number, document_status);
	}

	// Search api
	@RequestMapping(method = RequestMethod.GET, value = "/api/search")
	public ApiResponse search(@RequestParam String dashboardName, @RequestParam String searchByOption,
			@RequestParam String searchInputValue) throws Exception {
		ApiResponse searchResponse = null;
		switch (dashboardName) {
		case Constants.SEARCH_DASHBOARD_CUSTOMER:
			searchResponse = portalDashboardSearchService.getSearchCustomerList(searchByOption, searchInputValue);
			break;
		case Constants.SEARCH_DASHBOARD_POLICY:
			searchResponse = portalDashboardSearchService.getSearchPolicyList(searchByOption, searchInputValue);
			break;
		case Constants.SEARCH_DASHBOARD_TRANSACTION:
			searchResponse = portalDashboardSearchService.getSearchTransactionList(searchByOption, searchInputValue);
			break;
		case Constants.SEARCH_DASHBOARD_CLAIMS:
			searchResponse = portalDashboardSearchService.getSearchClaimsList(searchByOption, searchInputValue);
			break;

		case PortalDashboardAuthenticationConstants.SEARCH_DASHBOARD_CUSTOMER_SUPPORT:
			searchResponse = portalDashboardSearchService.getSearchTicketList(searchByOption, searchInputValue);
			break;
		}
		return searchResponse;
	}

//Reset and forget password		
	@RequestMapping(method = RequestMethod.GET, value = "/auth/api/update-password")
	public ApiResponse changePassword(@RequestParam("email") String email,
			@RequestParam("current_password") String current_password,
			@RequestParam("new_password") String new_password) throws Exception {
		return portalDashboardPasswordService.updatePassword(email, current_password, new_password);
	}

	// Reset and forget password
	@RequestMapping(method = RequestMethod.GET, value = "/auth/email-notification/password-reset")
	public ApiResponse forgetPasswordEmailNotification(@RequestParam("email") String email,
			@RequestParam("emailSession") String emailSession) throws Exception {

		return portalDashboardPasswordService.forgetPasswordEmailNotification(email, emailSession);
	}

	/**
	 * Portal Dashboard Chat Support API Start
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/api/chat/support/ticketList")
	public ApiResponse getAllNewAndOpenTickets(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam("ticket_dashboard") String ticket_dashboard) throws Exception {
		ApiResponse ticketresponse = null;

		switch (ticket_dashboard) {
		case PortalDashboardAuthenticationConstants.CHAT_LIST_BY_NEW_AND_OPEN:
			ticketresponse = portalDashboardChatSupportService.getAllNewAndOpenTickets(pageNo, pageSize);
			break;

		case PortalDashboardAuthenticationConstants.CHAT_LIST_BY_NEW_AND_CLOSED:
			ticketresponse = portalDashboardChatSupportService.getAllClosedTickets(pageNo, pageSize);
			break;
		case PortalDashboardAuthenticationConstants.CHAT_LIST_BY_NEW_AND_RESOLVED:
			ticketresponse = portalDashboardChatSupportService.getAllResolvedTickets(pageNo, pageSize);
			break;

		}
		return ticketresponse;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/chat/support/saveCSRResponse")
	public ApiResponse saveCSRResponse(@RequestParam("ticket_number") String ticket_number,
			@RequestParam("csr_response") String csr_response, @RequestParam("ticket_status") String ticket_status,
			@RequestParam("user_email") String user_email) throws Exception {
		return portalDashboardChatSupportService.saveCSRResponse(ticket_number, csr_response, ticket_status,
				user_email);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/chat/support/ViewTicketDetails")
	public ApiResponse ViewTicketDetails(@RequestParam("ticket_number") String ticket_number) throws Exception {
		return portalDashboardChatSupportService.ViewTicketDetails(ticket_number);
	}

	/**
	 * Portal Dashboard Chat Support API End
	 */

}
