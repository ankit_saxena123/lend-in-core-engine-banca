package com.kuliza.lending.wf_implementation.services;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.task.api.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.service.GenericServices;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAppPaymentDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowAppPayment;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.pojo.PaymentConfirmForm;
import com.kuliza.lending.wf_implementation.pojo.PaymentStatus;
import com.kuliza.lending.wf_implementation.pojo.PayooConfirm;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("iPATPaymentConfirmationService")
public class IPATPaymentConfirmationService extends GenericServices {

	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private WorkFlowUserService workFlowUserService;
	
	@Autowired
	private WorkflowUserDao workFlowUserDao;
	
	@Autowired
	private WorkflowUserApplicationDao workflowUserAppDao;
	
	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	private HistoryService historyService;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	@Autowired
	private WorkFlowAppPaymentDao workFlowAppPaymentDao;
	
	@Autowired
	private CashPaymentService cashPaymentService;

	private static final Logger logger = LoggerFactory.getLogger(IPATPaymentConfirmationService.class);

	Map<String, Object> parseResponseMap = new HashMap<String, Object>();

	public void sendMsgForComplete(String language,String productName, String userName){
		try{
			logger.info("<==push notitification sendMsgForComplete for product name==>"+productName);

			String notificationTitle = Constants.CASH_COMPLETE_PAY_TITLE_VN;
			String notificationMessage= Constants.CASH_COMPLETE_PAY_BODY_VN;

			 if(Constants.LANGUAGE_VALUE.equals(language.trim())){
				notificationTitle = Constants.CASH_COMPLETE_PAY_TITLE_EN;
				notificationMessage= Constants.CASH_COMPLETE_PAY_BODY_EN;
			 }
				notificationTitle=notificationTitle.replace("<product_name>", productName);
				logger.info("<==push notitification  sendMsgForComplete title =="+notificationTitle +"for user==>"+userName);

			workFlowUserService.pushNotification(userName, notificationTitle, notificationMessage);
			
		}catch(Exception ex){
			logger.info("<==cash sendMsgForComplete ==>"+ex);

		}
	}
	
	public void sendMsgForNotComplete(String language, String productName, String userName){
		try{
			logger.info("<==push notitification sendMsgForNotComplete for product name==>"+productName);

			String notificationTitle = Constants.CASH_PARTIAL_PAY_TITLE_VN;
			String notificationMessage= Constants.CASH_PARTIAL_PAY_BODY_VN;

		    if(Constants.LANGUAGE_VALUE.equals(language.trim())){
				notificationTitle = Constants.CASH_PARTIAL_PAY_TITLE_EN;
				notificationMessage= Constants.CASH_PARTIAL_PAY_BODY_EN;
			}
			notificationTitle=notificationTitle.replace("<product_name>", productName);
			logger.info("<==push notitification  sendMsgForNotComplete title =="+notificationTitle +"for user==>"+userName);

			workFlowUserService.pushNotification(userName, notificationTitle, notificationMessage);
			
		}catch(Exception ex){
			logger.info("<==cash sendMsgForNotComplete ==>"+ex);

		}
	}
	
	public ApiResponse addPaymentConfirmation(PaymentConfirmForm paymentConfirmForm) {
		
		logger.info("Payment conformed for crmID : "+paymentConfirmForm.getContractNumber());
		logger.info("paymentConfirmForm details : "+paymentConfirmForm.toString());
		boolean status = false;
		String leftAmount = null;
		Map<String, Object> variables = new HashMap<String, Object>();
		try {
			IPATPaymentConfirmModel ipatConfirmPaymentModel = ipatPaymentConfirmDao
					.findByCrmIdAndPaymentConfirmStatusAndIsDeleted(paymentConfirmForm.getContractNumber(), false,
							false);

			if (ipatConfirmPaymentModel != null) {
//				String payooTransactionId = Utils.getStringValue(ipatConfirmPaymentModel.getPayooTransactionId());
//				if (payooTransactionId.isEmpty()) {
//					logger.error("<=======>Payoo transactionId is empty");
//					return new ApiResponse(HttpStatus.BAD_REQUEST, "Payoo OrderId is empty", null);
//				}

				ipatConfirmPaymentModel.setContactNumber(paymentConfirmForm.getContactNumber());
				ipatConfirmPaymentModel.setCustomerName(paymentConfirmForm.getCustomerName());
				ipatConfirmPaymentModel.setDescription(paymentConfirmForm.getDescription());
				ipatConfirmPaymentModel.setPaymentAmount(paymentConfirmForm.getPaymentAmount());
				ipatConfirmPaymentModel.setPaymentDate(paymentConfirmForm.getPaymentDate());
				ipatConfirmPaymentModel.setCustomerBankAccount(paymentConfirmForm.getCustomerBankAccount());
				ipatConfirmPaymentModel.setProcessFlag(Constants.BANCA_PAYMENT_DEF_PROCESS_FLAG);
				//ipatConfirmPaymentModel.setPaymentConfirmStatus(true);
				// ipatPaymentConfirmDao.save(ipatConfirmPaymentModel);
				parseResponseMap.put("Description", "SUCCESS");
				logger.info("ipad details are saved succesfully for "+ipatConfirmPaymentModel.getWfApplication().getId());
				logger.info("crmId of wfAppId : "+ipatConfirmPaymentModel.getWfApplication().getId() + "=====>"+paymentConfirmForm.getContractNumber());
				WorkFlowApplication wfApplication = ipatConfirmPaymentModel.getWfApplication();
				String processInstanceId = wfApplication.getProcessInstanceId();
				logger.info("Ipat premium amount ====>>>> : " + paymentConfirmForm.getPaymentAmount()
						+ "PID premium amount ====>>>> : " + variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
				logger.info(
						" WhatYouPay=========>>>>>>>>>> : " + variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
				// cash payment starts
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();

				variables = processInstance.getProcessVariables();
				/*if ("Cash".equals(variables.get(WfImplConstants.JOURNEY_INSURER_PAYMENT_INFO))) {

				}*/
				ipatConfirmPaymentModel.setPaymentConfirmStatus(true);
				status = cashPaymentFlow(paymentConfirmForm, status, ipatConfirmPaymentModel, wfApplication,
						processInstanceId, variables);
				
				WorkFlowUserApplication wf_user_app = workflowUserAppDao.getUserIdentifier(wfApplication.getId());
				logger.info("<<<<<<<<========wf_user_app_name>>>>>>>"+wf_user_app.getUserIdentifier());

				logger.info("insurer status " +status + " of id "+ wfApplication.getId()+" user "+wf_user_app.getUserIdentifier());
				if (status) {
					
					sendMsgForComplete(Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY)),Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_NAME)), wf_user_app.getUserIdentifier());
					ipatConfirmPaymentModel.setInsurerStatus(status);
				}
				// compeleteJourney(processInstanceId);
				ipatPaymentConfirmDao.save(ipatConfirmPaymentModel);
			} else {
				/*
				* edited from here
				* only for partial payment
				* 
				*/	
				List<IPATPaymentConfirmModel> ipatConfirmPaymentModelList = ipatPaymentConfirmDao
						.findByCrmIdAndPaymentConfirmStatusAndIsDeletedOrderByIdAsc(
								paymentConfirmForm.getContractNumber(), true, false);
				if (ipatConfirmPaymentModelList != null) {
					IPATPaymentConfirmModel ipatConfirmPaymentModelNew = new IPATPaymentConfirmModel();
					ipatConfirmPaymentModelNew.setCrmId(
							ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1).getCrmId());
					ipatConfirmPaymentModelNew.setContactNumber(paymentConfirmForm.getContactNumber());
					ipatConfirmPaymentModelNew.setDescription(paymentConfirmForm.getDescription());
					Integer partialAmount = Utils.getIntegerValue(ipatConfirmPaymentModelList
							.get(ipatConfirmPaymentModelList.size() - 1).getPartialPayment());
					if(partialAmount.equals(0)) {
						logger.info("crmId is Invalid");
						return new ApiResponse(HttpStatus.BAD_REQUEST, "crmId is Invalid | payment is confirmed Already", null);
					}
					ipatConfirmPaymentModelNew.setPaymentAmount(paymentConfirmForm.getPaymentAmount());
					ipatConfirmPaymentModelNew.setPaymentDate(paymentConfirmForm.getPaymentDate());
					ipatConfirmPaymentModelNew.setCustomerBankAccount(paymentConfirmForm.getCustomerBankAccount());
					ipatConfirmPaymentModelNew.setRefNum(
							ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1).getRefNum());
					ipatConfirmPaymentModelNew.setCustomerName(paymentConfirmForm.getCustomerName());
					ipatConfirmPaymentModelNew.setIPATResponse(
							ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1).getIPATResponse());
					ipatConfirmPaymentModelNew.setWfApplication(
							ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1).getWfApplication());
					ipatConfirmPaymentModelNew.setProcessInstanceId(ipatConfirmPaymentModelList
							.get(ipatConfirmPaymentModelList.size() - 1).getProcessInstanceId());
					ipatConfirmPaymentModelNew.setProcessFlag(Constants.BANCA_PAYMENT_DEF_PROCESS_FLAG);
					// ipatPaymentConfirmDao.save(ipatConfirmPaymentModel);
					parseResponseMap.put("Description", "SUCCESS");
					logger.info("<=======>ipad details are saved succesfully<==========>");
					WorkFlowApplication wfApplication = ipatConfirmPaymentModelList
							.get(ipatConfirmPaymentModelList.size() - 1).getWfApplication();
					String processInstanceId = wfApplication.getProcessInstanceId();
					logger.info("Ipat premium amount ====>>>> : " + paymentConfirmForm.getPaymentAmount()
							+ "PID premium amount ====>>>> : "
							+ variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
					logger.info(" WhatYouPay=========>>>>>>>>>> : "
							+ variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
					// cash payment starts
					HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
					variables = processInstance.getProcessVariables();
					/*if (variables.get(WfImplConstants.JOURNEY_INSURER_PAYMENT_INFO).equals("Cash")) {
					}*/
					
					leftAmount = Utils.getStringValue(
							partialAmount - Utils.getIntegerValue(paymentConfirmForm.getPaymentAmount()));
					ipatConfirmPaymentModelNew.setPaymentConfirmStatus(true);

					if (leftAmount==null || leftAmount.equals("0") ) {
						status = callInsurerAPI(processInstanceId, paymentConfirmForm.getContractNumber(),
								wfApplication, ipatConfirmPaymentModel);
						leftAmount=null;
					}
					else if(Utils.getIntegerValue(leftAmount)< 0){
						logger.info("inside partial accessPayment:======>>>>:  " + leftAmount);
						ipatConfirmPaymentModelNew.setAccessPayment(Utils.getStringValue(Math.abs(Utils.getIntegerValue(leftAmount))));
						status = callInsurerAPI(processInstanceId, paymentConfirmForm.getContractNumber(),
								wfApplication, ipatConfirmPaymentModel);
						leftAmount=null;
					}
					else{
						
						WorkFlowUserApplication wf_user_app = workflowUserAppDao.getUserIdentifier(wfApplication.getId());
						logger.info("<<<<<<<<========wf_user_app_id>>>>>>>"+wfApplication.getId());
						logger.info("<<<<<<<<========wf_user_app_name>>>>>>>"+wf_user_app.getUserIdentifier());

						sendMsgForNotComplete(Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY)), Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_NAME)),wf_user_app.getUserIdentifier());

					}

					ipatConfirmPaymentModelNew.setPartialPayment(leftAmount);
//					status = cashPaymentFlow(paymentConfirmForm, status,
//							ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1), wfApplication,
//							processInstanceId, variables,leftAmount);

					if (status) {
						cashPaymentService.sendSms(ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size() - 1).getCrmId(), processInstanceId, Constants.SMS_CASH_PAY_COMPLETE,true);
						WorkFlowUserApplication wf_user_app = workflowUserAppDao.getUserIdentifier(wfApplication.getId());
						logger.info("<<<<<<<<========wf_user_app_id>>>>>>>"+wfApplication.getId());
						logger.info("<<<<<<<<========wf_user_app_name>>>>>>>"+wf_user_app.getUserIdentifier());
						sendMsgForComplete(Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY)), Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_NAME)),wf_user_app.getUserIdentifier());
						ipatConfirmPaymentModelNew.setInsurerStatus(status);

					}
					// compeleteJourney(processInstanceId);
					ipatPaymentConfirmDao.save(ipatConfirmPaymentModelNew);
				}
				// to here
//				logger.info("crmId is Invalid");
//				return new ApiResponse(HttpStatus.BAD_REQUEST, "crmId is Invalid | payment is confirmed Already", null);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(Constants.SOMETHING_WRONG_MESSAGE, e.getMessage());
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);

		}

		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, parseResponseMap);
	}

	private boolean cashPaymentFlow(PaymentConfirmForm paymentConfirmForm, boolean status,
			IPATPaymentConfirmModel ipatConfirmPaymentModel, WorkFlowApplication wfApplication,
			String processInstanceId, Map<String, Object> variables) {
		String content=Constants.SMS_CASH_PAY_COMPLETE;
		if ("Cash".equals(variables.get(WfImplConstants.JOURNEY_INSURER_PAYMENT_INFO))) {
			logger.info("Inside Cash Payment for "+processInstanceId);
			if (Utils.getIntegerValue(paymentConfirmForm.getPaymentAmount())
					.equals(Utils.getIntegerValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)))) {
				status = callInsurerAPI(processInstanceId, paymentConfirmForm.getContractNumber(), wfApplication,
						ipatConfirmPaymentModel);
				if(status) {
					content = content.replace("${sum_insured}", paymentConfirmForm.getPaymentAmount());
					logger.info("ipatConfirmPaymentModel.getCrmId() ====>>>>: "+ ipatConfirmPaymentModel.getCrmId() + "    " + processInstanceId+"    " + content );
					cashPaymentService.sendSms(ipatConfirmPaymentModel.getCrmId(), processInstanceId, content,true);
				}
			} else if (Utils.getIntegerValue(paymentConfirmForm.getPaymentAmount()) > Utils
					.getIntegerValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT))) {
				logger.info("whatYouPay amount of iPAt: ====>>>> : "
						+ Utils.getIntegerValue(paymentConfirmForm.getPaymentAmount())
						+ "premium amount of PID======>>>>>>: "
						+ Utils.getIntegerValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)));
				String accessPayment = Utils.getStringValue(Utils.getIntegerValue(paymentConfirmForm.getPaymentAmount())
						- Utils.getIntegerValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)));
				logger.info("inside access payment:=======>>>> : " + accessPayment);
				ipatConfirmPaymentModel.setAccessPayment(accessPayment);
				status = callInsurerAPI(processInstanceId, paymentConfirmForm.getContractNumber(), wfApplication,
						ipatConfirmPaymentModel);
				if(status) {
					content = content.replace("${sum_insured}", paymentConfirmForm.getPaymentAmount());
					logger.info("ipatConfirmPaymentModel.getCrmId() ====>>>>: "+ ipatConfirmPaymentModel.getCrmId() + "    " + processInstanceId+"    " + content );
					cashPaymentService.sendSms(ipatConfirmPaymentModel.getCrmId(), processInstanceId, content,true);
				}
			
			} else
			{
				logger.info("<<<<<<<<========inside partial payment=======>>>>>>>");
				String partialPayment = Utils
						.getStringValue(Utils.getIntegerValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT))
								- Utils.getIntegerValue(paymentConfirmForm.getPaymentAmount()));
				logger.info("<<<<<<<<========inside partial payment=less==>>>>>>>");
				ipatConfirmPaymentModel.setPartialPayment(partialPayment);

				WorkFlowUserApplication wf_user_app = workflowUserAppDao.getUserIdentifier(wfApplication.getId());
				logger.info("<<<<<<<<========wf_user_app_id>>>>>>>"+wfApplication.getId());
				logger.info("<<<<<<<<========wf_user_app_name>>>>>>>"+wf_user_app.getUserIdentifier());

				sendMsgForNotComplete(Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY)), Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_NAME)),wf_user_app.getUserIdentifier());

			}
			// cash payment ends
		} else {
			logger.info("not cash payment=========>>>>>>>>>>");
			status = callInsurerAPI(processInstanceId, paymentConfirmForm.getContractNumber(), wfApplication,
					ipatConfirmPaymentModel);
		}
		return status;
	}
	/*
	 * private void compeleteJourney(String processInstanceId) { Task userTask =
	 * taskService.createTaskQuery().processInstanceId(processInstanceId).active().
	 * singleResult(); if (userTask != null) { logger.info("name of userTask is :" +
	 * userTask.getName());
	 * 
	 * logger.info("<==========>Found one User Task Id: " + userTask.getId());
	 * logger.info("<==========>Found one User Task Name: " + userTask.getName());
	 * String taskId = userTask.getId(); taskService.complete(taskId);
	 * logger.info("<==========> User task is completed<==========>");
	 * 
	 * } }
	 */

	private boolean callInsurerAPI(String processInstanceId, String crmId, WorkFlowApplication wfApplication,
			IPATPaymentConfirmModel ipatConfirmPaymentModel) {
		boolean status = false;
		String planEndDate = null;
		try {
			HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
			Map<String, Object> variables = processInstance.getProcessVariables();
			InsurerAbstract insurer = InsurerFactoryClass.getInsurer(variables);
			// .singleResult();
//			Map<String, Object> variables = runtimeService.getVariables(processInstanceId);
//			InsurerAbstract insurer = InsurerFactoryClass.getInsurer(variables);
			String planStartDate = Utils.getCurrentDateInFormat(Constants.DATE_FORMAT_SLASH);
			Date currentdate = Utils.parseDate(planStartDate, Constants.DATE_FORMAT_SLASH);

			logger.info("Journey product id : " + WfImplConstants.JOURNEY_PRODUCT_ID);

			if (variables.get(WfImplConstants.JOURNEY_PRODUCT_ID).equals(WfImplConstants.CV_PRODUCT_ID)) {
				logger.info("***INSIDE callInsurerAPI for COVID*********** "
						+ variables.get(WfImplConstants.JOURNEY_PRODUCT_ID));
				if (variables.get(WfImplConstants.JOURNEY_PRODUCT_ID).equals(WfImplConstants.BASIC_PLAN_ID)) {
					logger.info("*************** COVID plan ID********"
							+ variables.get(WfImplConstants.JOURNEY_PRODUCT_ID));
					Date addMonthsToDate = Utils.addMonthsToDate(currentdate, 3);
					planEndDate = Utils.getDateInString(addMonthsToDate, Constants.DATE_FORMAT_SLASH);
				} else {
					Date addMonthsToDate = Utils.addMonthsToDate(currentdate, 6);
					planEndDate = Utils.getDateInString(addMonthsToDate, Constants.DATE_FORMAT_SLASH);
				}
			} else if (variables.get(WfImplConstants.JOURNEY_PRODUCT_ID).equals(WfImplConstants.TL_PRODUCT_ID)) {

				logger.info("tl product id found ------------>" + WfImplConstants.JOURNEY_PRODUCT_ID);

				planStartDate = Utils.getStringValue(variables.get(WfImplConstants.TL_POLICY_START_DATE));
				planEndDate = Utils.getStringValue(variables.get(WfImplConstants.TL_POLICY_END_DATE));
			} else {
				Date addOneToDate = Utils.subtractDaysToDateAddYearsToDate(currentdate, 1);
				planEndDate = Utils.getDateInString(addOneToDate, Constants.DATE_FORMAT_SLASH);
				logger.info("Inside Else --> planEndDate -->" + planEndDate);
			}

			logger.info("plan start date ----->" + planStartDate);
			logger.info("plan end date ----->" + planEndDate);

			variables.put(WfImplConstants.IPAT_PAYMENT_START_DATE, planStartDate);
			variables.put(WfImplConstants.IPAT_PLAN_EXPIRING_DATE, planEndDate);
			status = insurer.postPolicyDetails(processInstanceId, variables, crmId, wfApplication,
					ipatConfirmPaymentModel, null);
		} catch (Exception e) {
			logger.error("<=====> calling isurer api is failed : " + e.getMessage(), e);
			status = false;
		}
		return status;

	}

	/**
	 * service Task for UAT which will call ipat_payment_confirm
	 * 
	 * @param execution
	 * @return DelegateExecution
	 */
	public DelegateExecution ipayPaymentConfim(DelegateExecution execution) {
		logger.info("ipayPaymentConfim (execution)");
		// construct the mock request for IpayPayment confirm for UAT
		PaymentConfirmForm paymentConfirmForm = constructRequestOfPayMentConfirm(execution);
		addPaymentConfirmation(paymentConfirmForm);
		return execution;

	}

	/**
	 * construct the ipat confirm request for UAT Service Task
	 * 
	 * @param execution
	 * @return PaymentConfirmForm
	 */
	private PaymentConfirmForm constructRequestOfPayMentConfirm(DelegateExecution execution) {
		Map<String, Object> variables = execution.getVariables();
		String transId = (String) variables.get(WfImplConstants.IPAT_TRANSACTION_ID);
		String crmId = (String) variables.get(WfImplConstants.IPAT_CRM_ID);
		String premium = (String) variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT);
		String customerName = (String) variables.get(WfImplConstants.FULLNAME_HYPERVERGE);
		PaymentConfirmForm paymentConfirmForm = new PaymentConfirmForm();
		paymentConfirmForm.setContractNumber(crmId);
		paymentConfirmForm.setRefNum(transId);
		paymentConfirmForm.setPaymentAmount(premium);
		paymentConfirmForm.setCustomerName(customerName);

		return paymentConfirmForm;
	}

	public ApiResponse payooConfirm(String userName, PayooConfirm payooConfirm) {
		logger.info("payooConfirm called for "+userName+"---->"+payooConfirm.toString());
		String crmId = payooConfirm.getCrmId();
		String transactionId = payooConfirm.getTransactionId();
		logger.info("userName : "+userName+" crmId:" + crmId + "TransactionId" + transactionId);
		IPATPaymentConfirmModel ipatPaymentConfirm = ipatPaymentConfirmDao.findByCrmIdAndIsDeleted(crmId, false);
		if (ipatPaymentConfirm != null) {
			ipatPaymentConfirm.setPayooTransactionId(transactionId);
			ipatPaymentConfirmDao.save(ipatPaymentConfirm);
		} else {
			logger.error("No Record Found in ipat table "+userName);
		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
	}

	public void updateIPATUnknownPayment() {

		LocalDate date = LocalDate.now();
		String upper = date.minusDays(1).toString();
		// String upper = date.minusDays(2).toString();
		String lower = date.minusDays(3).toString();

		List<IPATPaymentConfirmModel> processIds = ipatPaymentConfirmDao.getProcessInstanceIdsOfFailPayment(lower,
				upper);

		logger.info("<=======>Total Number of Unknown IPAT Payment Process Instances<==========>" + processIds.size());

		for (IPATPaymentConfirmModel processId : processIds) {
			logger.info("<=======>IPAT Payment Unknown Process Instance ID <==========>"
					+ processId.getProcessInstanceId());

			changeProcessStateOfPaymentUnknown(processId.getProcessInstanceId());

		}

	}

	public void changeProcessStateOfPaymentUnknown(String processInstanceId) {
		try {
			if (processInstanceId != null) {
				Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).active().singleResult();
				String backTaskName = Utils
						.getStringValue(runtimeService.getVariable(processInstanceId, "await_screen_back_task"));
				if (task == null || "" == backTaskName) {
					logger.info("<=======>Task is not defined for the process instance id <==========>"
							+ processInstanceId);

				} else {
					logger.info("<=======>Back Task " + backTaskName
							+ " defined for the process instance id <==========>" + processInstanceId);

					runtimeService.createChangeActivityStateBuilder().processInstanceId(processInstanceId)
							.moveActivityIdTo(task.getTaskDefinitionKey(), backTaskName).changeState();

				}
			}
		} catch (Exception e) {

			logger.info("<=======>Exception occured while update user back task <==========>" + e);

		}

	}

	public ApiResponse updatePaymentStatus(String userName, PaymentStatus paymentStatusReq) throws FECException {

		logger.info("payment status update called for " + paymentStatusReq.getProcInstId() + " , status : "
				+ paymentStatusReq.getStatus() + " user "+userName);
		
		WorkFlowApplication workFlowApplication = workflowApplicationDao
				.findByProcessInstanceIdAndIsDeleted(paymentStatusReq.getProcInstId(), false);

		if (workFlowApplication == null) {
			logger.error("application is null for " + paymentStatusReq.getProcInstId());
			throw new FECException(ErrorCodes.INVALID_WORKFLOW_APP);
		}

		WorkFlowAppPayment workFlowAppPayment = workFlowAppPaymentDao
				.findByWorkFlowApplicationAndIsDeleted(workFlowApplication, false);

		if (workFlowAppPayment == null) {
			workFlowAppPayment = new WorkFlowAppPayment();
			workFlowAppPayment.setWorkFlowApplication(workFlowApplication);
			workFlowAppPayment.setPaymentType(1);
			logger.info("new payment status created for " + paymentStatusReq.getProcInstId());
		}
		workFlowAppPayment.setStatus(paymentStatusReq.getStatus());
		workFlowAppPaymentDao.save(workFlowAppPayment);
		logger.info("payment status successfully updated for " + paymentStatusReq.getProcInstId() + " , status : "
				+ paymentStatusReq.getStatus());
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
	}

	public void fetchListOfPaymentStatus() throws FECException {
		List<WorkFlowAppPayment> listOfpaymentStatus = workFlowAppPaymentDao
				.findByStatusAndIsDeleted(Constants.INIT_STATUS, false);
		logger.info("ListOfPaymentStatus--->" + listOfpaymentStatus.size() + "<---listOfPaymentStatus--->"
				+ listOfpaymentStatus.toString());
		getWorkflowApplicationFromPaymentStatus(listOfpaymentStatus);
	}

	private void getWorkflowApplicationFromPaymentStatus(List<WorkFlowAppPayment> listOfpaymentStatus)
			throws FECException {
		for (WorkFlowAppPayment paymentStatus : listOfpaymentStatus) {
			WorkFlowApplication workFlowApplication = paymentStatus.getWorkFlowApplication();
			String processInstanceId = getIpatConfirmStatusAndProcID(workFlowApplication);
			logger.info("processInstanceId--->" + processInstanceId);
			getListOfTask(processInstanceId);
		}
	}

	private String getIpatConfirmStatusAndProcID(WorkFlowApplication workFlowApplication) throws FECException {
		String processInstanceId = null;
		if (workFlowApplication == null) {
			throw new FECException(ErrorCodes.INVALID_WORKFLOW_APP);
		}
		IPATPaymentConfirmModel ipatPaymentConfirmStatus = ipatPaymentConfirmDao
				.findByWfApplicationAndPaymentConfirmStatusAndIsDeleted(workFlowApplication, true, false);
		if (ipatPaymentConfirmStatus != null) {
			logger.info("ipatPaymentConfirmStatus--->" + ipatPaymentConfirmStatus.toString());
			processInstanceId = ipatPaymentConfirmStatus.getProcessInstanceId();
			logger.info("processInstanceId--->" + processInstanceId);
		}
		return processInstanceId;
	}

	private void getListOfTask(String processInstanceId) {
		if (processInstanceId != null) {
			logger.info("processInstanceId--->" + processInstanceId);
			List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).active().list();
			logger.info("TaskSize--->" + tasks.size());
			getActiveTask(tasks);
			logger.info("TaskCompletedForPID-->" + processInstanceId);
		} else {
			logger.error("processInstanceId is null");
		}
	}

	private void getActiveTask(List<Task> tasks) {
		if (tasks != null && !tasks.isEmpty()) {
			tasks.forEach(task -> {
				Task activeTask = task;
				logger.info("activeTask--->" + activeTask);
				setIntoProcessVariable(activeTask);
				completeActiveTask(activeTask);
				logger.info("ListOfTask--->" + task);
			});
		}
	}

	private void completeActiveTask(Task activeTask) {
		if (activeTask.getTaskDefinitionKey().equals(Constants.PAYMENT_KEY)
				|| activeTask.getTaskDefinitionKey().equals(Constants.PAYOO)
				|| (activeTask.getTaskDefinitionKey().equals(Constants.CHECKOUT_PA))) {
			taskService.complete(activeTask.getId());
		}
	}

	private void setIntoProcessVariable(Task activeTask) {
		runtimeService.setVariable(activeTask.getProcessInstanceId(), Constants.CURRENT_JOURNEY_STAGE,
				Constants.PAYMENT_KEY);
		runtimeService.setVariable(activeTask.getProcessInstanceId(), Constants.PAYMENT_TYPE_CHECK, Constants.PAYOO);
		runtimeService.setVariable(activeTask.getProcessInstanceId(), Constants.CURRENT_JOURNEY_STAGE,
				Constants.CHECKOUT_PA);
	}

}
