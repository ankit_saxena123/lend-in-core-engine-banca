package com.kuliza.lending.wf_implementation.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FECreditUtilities {

	private static final Logger logger = LoggerFactory.getLogger(FECreditUtilities.class);

	public static String getClientIP(HttpServletRequest request) {
		String xfHeader = request.getHeader("X-Forwarded-For");
		if (xfHeader == null) {
			return request.getRemoteAddr();
		}
		return xfHeader.split(",")[0];
	}

	public static String maskContactNumber(String mobileNumber) {
		String maskedNumber = "";
		maskedNumber = maskedNumber + mobileNumber.charAt(0);
		for (int i = 0; i < mobileNumber.length() - 3; i++) {
			maskedNumber = maskedNumber + "X";
		}
		maskedNumber = maskedNumber + mobileNumber.substring(mobileNumber.length() - 2, mobileNumber.length());
		return maskedNumber;
	}

	public static int checkFrontBack(String hypervergeNationalIdResponse) throws JSONException {
		if (hypervergeNationalIdResponse == null || hypervergeNationalIdResponse.equalsIgnoreCase("null"))
			return -1;
		JSONObject data = new JSONObject(hypervergeNationalIdResponse);
		if (data.has("status") && data.has("statusCode") && data.getString("status").equals("success")
				&& data.getString("statusCode").equals("200")) {
			if (data.has("result") && data.get("result") instanceof JSONArray) {
				JSONArray result = data.getJSONArray("result");
				if (result.length() > 0 && result.get(result.length() - 1) instanceof JSONObject) {
					JSONObject result0 = result.getJSONObject(result.length() - 1);
					if (result0.has("type") && result0.get("type") instanceof String) {
						if (result0.getString("type").equals("id_front")
								|| result0.getString("type").equals("id_new_front")) {
							return 1;
						} else if (result0.getString("type").equals("id_back")
								|| result0.getString("type").equals("id_new_back")) {
							return 0;
						}
					}
				}
			}
		}
		return -1;
	}

	public static int checkSelfieLiveness(String hypervergeNationalIdResponse) throws JSONException {
		if (hypervergeNationalIdResponse == null || hypervergeNationalIdResponse.equalsIgnoreCase("null"))
			return -1;
		JSONObject data = new JSONObject(hypervergeNationalIdResponse);
		if (data.has("status") && data.has("statusCode") && data.getString("status").equals("success")
				&& data.getString("statusCode").equals("200")) {
			if (data.has("result") && data.get("result") instanceof JSONArray) {
				JSONArray result = data.getJSONArray("result");
				if (result.length() > 0 && result.get(result.length() - 1) instanceof JSONObject) {
					JSONObject result0 = result.getJSONObject(result.length() - 1);
					if (result0.has("type") && result0.get("type") instanceof String) {
						if (result0.getString("type").equals("id_front")
								|| result0.getString("type").equals("id_new_front")) {
							return 1;
						} else if (result0.getString("type").equals("id_back")
								|| result0.getString("type").equals("id_new_back")) {
							return 0;
						}
					}
				}
			}
		}
		return -1;
	}

	public static boolean checkSelfieNew(String hypervergeSelfieResponse) throws JSONException {
		JSONObject response = new JSONObject(hypervergeSelfieResponse);
		if (response != null && response.has("status") && response.getInt("status") == 200) {
			JSONObject data = response.getJSONObject("data");
			if (data != null && data.has("sys") && data.get("sys") instanceof JSONObject) {
				JSONObject sys = data.getJSONObject("sys");
				if (sys != null && sys.has("code") && sys.getInt("code") == 1) {
					return true;
				}
			}
		}
		return false;
	}

	public static Map<String, Object> parseHyervergeLivenessApiResponse(String hypervergeResponse)
			throws JSONException {
		JSONObject data = new JSONObject(hypervergeResponse);
		Map<String, Object> response = new HashMap<>();
		if (data.has("status") && data.has("statusCode")) {
			response.put("message", data.getString("status"));
			response.put("status", data.getString("statusCode"));
			if (data.has("result") && data.get("result") instanceof JSONObject) {
				JSONObject result = data.getJSONObject("result");
				if (result.has("error")) {
					response.put("data", result.getString("error"));
				} else {
					Map<String, Object> body = new HashMap<>();
					body.put("live", result.getString("live"));
					body.put("liveness-score", result.getString("liveness-score"));
					body.put("to-be-reviewed", result.getString("to-be-reviewed"));
					response.put("data", body);
				}
			}
			if (data.has("error") && data.get("error") instanceof String) {
				response.put("data", data.getString("error"));
			}
		} else {
			response.put("message", "failure");
			response.put("status", "500");
			response.put("data", "No response from hyperverge");
		}
		return response;
	}

	public static String getHyervergeLivenessApiResponse(String hypervergeResponse) throws JSONException {
		if (hypervergeResponse == null || hypervergeResponse.equalsIgnoreCase("null"))
			return "no";
		JSONObject data = new JSONObject(hypervergeResponse);
		Map<String, Object> response = new HashMap<>();
		if (data.has("status") && data.has("statusCode")) {
			response.put("message", data.getString("status"));
			response.put("status", data.getString("statusCode"));
			if (data.has("result") && data.get("result") instanceof JSONObject) {
				JSONObject result = data.getJSONObject("result");
				if (result.has("error")) {
					response.put("data", result.getString("error"));
				} else {
					return result.getString("live");
				}
			}
			if (data.has("error") && data.get("error") instanceof String) {
				return "no";
			}
		} else {
			return "no";
		}
		return "no";
	}

}
