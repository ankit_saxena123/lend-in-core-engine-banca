package com.kuliza.lending.wf_implementation.pojo;

public class UVLSavingsPremiumPojo {
	
	private String language;
	private String gender;
	private String dob;
	private String investment_amount;
	private String protection_term;
	private String paymentOption;
	private String final_investment_value;
	private String death_benefit;
	private String total_permanant;
	private String terminal_illness;
	
	public String getLanguage() {
		return language;
	}
	public String getGender() {
		return gender;
	}
	public String getDob() {
		return dob;
	}
	public String getInvestment_amount() {
		return investment_amount;
	}
	public String getProtection_term() {
		return protection_term;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public String getFinal_investment_value() {
		return final_investment_value;
	}
	public String getDeath_benefit() {
		return death_benefit;
	}
	public String getTotal_permanant() {
		return total_permanant;
	}
	public String getTerminal_illness() {
		return terminal_illness;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public void setInvestment_amount(String investment_amount) {
		this.investment_amount = investment_amount;
	}
	public void setProtection_term(String protection_term) {
		this.protection_term = protection_term;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public void setFinal_investment_value(String final_investment_value) {
		this.final_investment_value = final_investment_value;
	}
	public void setDeath_benefit(String death_benefit) {
		this.death_benefit = death_benefit;
	}
	public void setTotal_permanant(String total_permanant) {
		this.total_permanant = total_permanant;
	}
	public void setTerminal_illness(String terminal_illness) {
		this.terminal_illness = terminal_illness;
	}
	@Override
	public String toString() {
		return "UVLSavingsPremiumPojo [language=" + language + ", gender=" + gender + ", dob=" + dob
				+ ", investment_amount=" + investment_amount + ", protection_term=" + protection_term
				+ ", paymentOption=" + paymentOption + ", final_investment_value=" + final_investment_value
				+ ", death_benefit=" + death_benefit + ", total_permanant=" + total_permanant + ", terminal_illness="
				+ terminal_illness + "]";
	}

	

}