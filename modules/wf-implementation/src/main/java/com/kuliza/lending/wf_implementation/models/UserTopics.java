package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.journey.model.AbstractWfUserDeviceRegister;

@Entity
@Table(name = "user_topics")
public class UserTopics extends BaseModel {

	@Column(nullable = true, name = "topic_name")
	private String topic;

	@Column(nullable = true, name = "user_name")
	private String userName;

	@Column(nullable = true, name = "subscriber_arn")
	private String subscriberArn;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wf_user_id", nullable = false)
	private WorkFlowUser wfUser;

	public String getSubscriberArn() {
		return subscriberArn;
	}

	public WorkFlowUser getWfUser() {
		return wfUser;
	}

	public void setWfUser(WorkFlowUser wfUser) {
		this.wfUser = wfUser;
	}

	public void setSubscriberArn(String subscriberArn) {
		this.subscriberArn = subscriberArn;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}