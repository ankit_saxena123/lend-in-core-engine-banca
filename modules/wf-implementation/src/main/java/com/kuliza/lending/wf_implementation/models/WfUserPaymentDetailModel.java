package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name="wf_user_payment_details")
public class WfUserPaymentDetailModel extends BaseModel {

	@Column(nullable = true, name="account_holders_name")
	private String accountHoldersName;
	
	@Column(nullable = true, name="account_number",unique=true )
	private String accountNumber;
	
	@Column(nullable = true, name="bank_name")
	private String bankName;
	
	@Column(nullable = true, name="branch_name")
	private String branchName;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wf_user_id", nullable = false)
	private WorkFlowUser wfUser;
	
	@Column(nullable = true, name="uuid_transaction_id")
	private String uUidTransactionId;


	public String getuUidTransactionId() {
		return uUidTransactionId;
	}

	public void setuUidTransactionId(String uUidTransactionId) {
		this.uUidTransactionId = uUidTransactionId;
	}

	public String getAccountHoldersName() {
		return accountHoldersName;
	}

	public void setAccountHoldersName(String accountHoldersName) {
		this.accountHoldersName = accountHoldersName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public WorkFlowUser getWfUser() {
		return wfUser;
	}

	public void setWfUser(WorkFlowUser wfUser) {
		this.wfUser = wfUser;
	}
}