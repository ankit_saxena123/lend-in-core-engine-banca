package com.kuliza.lending.wf_implementation.models;

import com.kuliza.lending.journey.model.AbstractOTPDetails;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

@Entity
@Table(name = "otp_details")
@ConditionalOnExpression("${otp.useCacheForOTP:false}")
public class OTPDetails extends AbstractOTPDetails {
//	Please add your fields here if you want to extend otp details table.

  public OTPDetails() {
    super();
  }

  public OTPDetails(String otpHash, String userIdentifier, String ipAddress,
      String secretKey, long otpGenerationTime) {
    super(otpHash, userIdentifier, ipAddress, secretKey, otpGenerationTime);
  }

  public OTPDetails(String userIdentifier, String ipAddress, String secretKey, long otpGenerationTime) {
    super(userIdentifier, ipAddress, secretKey, otpGenerationTime);
  }
}
