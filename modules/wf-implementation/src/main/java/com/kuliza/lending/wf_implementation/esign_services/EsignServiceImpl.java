package com.kuliza.lending.wf_implementation.esign_services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.flowable.engine.RuntimeService;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.InsurerInfoDao;
import com.kuliza.lending.wf_implementation.dao.TLNumberSequncesDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.fpt.AuthorizeCounterSigningForSignCloudService;
import com.kuliza.lending.wf_implementation.fpt.PrepareCertificateForSignCloudService;
import com.kuliza.lending.wf_implementation.fpt.PrepareFileForSignCloudService;
import com.kuliza.lending.wf_implementation.fpt.SignedFileForSignCloudService;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;
import com.kuliza.lending.wf_implementation.models.TLNumberSequnces;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.pojo.ESignRequest;
import com.kuliza.lending.wf_implementation.pojo.FPTFileRequest;
import com.kuliza.lending.wf_implementation.pojo.FPTRequest;
import com.kuliza.lending.wf_implementation.pojo.NomineeDetails;
import com.kuliza.lending.wf_implementation.pojo.PlanPremiums;
import com.kuliza.lending.wf_implementation.pojo.TLPolicyDetails;
import com.kuliza.lending.wf_implementation.pojo.TermLifeAgePremiumRates;
import com.kuliza.lending.wf_implementation.pojo.WorkFlowUserSms;
import com.kuliza.lending.wf_implementation.services.FECreditBancaNotifyUserServices;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.ESignCloudConstants;
import com.kuliza.lending.wf_implementation.utils.ErrorMessages;
import com.kuliza.lending.wf_implementation.utils.FPTUtils;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import vn.com.fis.ws.SignCloudResp;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class EsignServiceImpl implements IESignService {
	
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private PrepareCertificateForSignCloudService certificateForSignCloudService;
	@Autowired
	private PrepareFileForSignCloudService prepareFileForSignCloudService;
	@Autowired
	private AuthorizeCounterSigningForSignCloudService authorizeFPTService;
	@Autowired
	private SignedFileForSignCloudService signedFileForSignCloudService;
	@Autowired
	private WorkflowUserDao workflowUserDao;
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	@Autowired
	private FECreditBancaNotifyUserServices notifyUserServices;
	@Autowired
	private WfImplConfig wfImplConfig;
	
	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;
	@Autowired
	private WorkFlowAddressDao workFlowAddressDao;
	@Autowired
	private WorkFlowPolicyDAO workPolicyDAO;
	
	@Autowired
	private TLNumberSequncesDao tlNumberSequncesDao;
	
	private static final Logger logger = LoggerFactory.getLogger(EsignServiceImpl.class); 
	
	@Override
	public Map<String, Object>  initESignService(ESignRequest eSignRequest) throws Exception {
		
		logger.info("************************ eSignInitService called for : "+eSignRequest.getProcInstId());
		Map<String, Object> variables = runtimeService.getVariables(eSignRequest.getProcInstId());
		
		Integer fptState = (Integer) variables.get(Constants.FPT_STATE);
		boolean isAuthorize = false;
		if(variables.get(Constants.FPT_ISAUTHORIZE) != null) {
			isAuthorize = (boolean) variables.get(Constants.FPT_ISAUTHORIZE);
		}
		boolean isFileUploaded = false;
		if(variables.get(Constants.FPT_IS_FILE_UPLOADED) != null) {
			isFileUploaded = (boolean) variables.get(Constants.FPT_IS_FILE_UPLOADED);
		}
		
		logger.info("********************************************************");
		logger.info("fptState = "+fptState+" and isAuthorize = "+isAuthorize+", isFileUploaded = "+isFileUploaded+", for procId : "+eSignRequest.getProcInstId());
		logger.info("********************************************************");
		Map<String, Object> response = new HashMap<String, Object>();
		
		if (isAuthorize && isFileUploaded) {
			logger.info("Download fpt file is called");
			response = downLoadESignDoc(eSignRequest);
			response.put(Constants.FPT_IS_SHOW_VERIFY_OTP, false);
		} else {

			if (! isFileUploaded) {
				logger.info("File is not uploaded yet, so prpare files");
				logger.info("Upload fpt file is called" + eSignRequest.getProcInstId());
				response = initESign(eSignRequest);
				response = prepareESignFiles(eSignRequest);
			} else {
				logger.info("Regenrate otp is called" + eSignRequest.getProcInstId());
				response = regenerateESignOTP(eSignRequest);
			}
			response.put(Constants.FPT_IS_SHOW_VERIFY_OTP, true);
		}
		
//		if(fptState == null || fptState < Constants.FPT_GENERATE_FILE_STATE) {
//			logger.info("FPT init called for "+eSignRequest.getProcInstId());
//			response = initESign(eSignRequest);
//			response = prepareESignFiles(eSignRequest);
//		}else if(fptState.equals(Constants.FPT_AUTHORIZE_STATE)) {
//			logger.info("FPT regenerate OTP called for "+eSignRequest.getProcInstId());
//			response = downLoadESignDoc(eSignRequest);
//		}
//		else if(fptState.equals(Constants.FPT_AUTHORIZE_STATE)) {
//			logger.info("FPT regenerate OTP called for "+eSignRequest.getProcInstId());
//			response = regenerateESignOTP(eSignRequest);
//		}

		logger.info("FPT init Response ==================>"+ response.toString());
		return response;
	}
	
	@Override
	public void htmlToPdf(String templateName) throws Exception {
		getTLNumberSequnces();
		
//		String fileName = templateName;
//		TLPolicyDetails tlPolicyDetails = new TLPolicyDetails();
//		String outFileName = genrateApplicationFormPDF(fileName,tlPolicyDetails);

	}
	
	
	private TLNumberSequnces getTLNumberSequnces() {
		
		TLNumberSequnces tlNumberSequnces = tlNumberSequncesDao.findTopByOrderByIllustrationNumber();
		if(tlNumberSequnces == null) {
			tlNumberSequnces = new TLNumberSequnces();
			tlNumberSequnces.setIllustrationNumber(206026000001L);
			tlNumberSequnces.setApplicationNumber(205300001L);
		}else {
			tlNumberSequnces.setIllustrationNumber(tlNumberSequnces.getIllustrationNumber()+1);
			tlNumberSequnces.setApplicationNumber(tlNumberSequnces.getApplicationNumber()+1);
		}
		tlNumberSequncesDao.save(tlNumberSequnces);
		return tlNumberSequnces;
	}
	private String readFile(String fileName) throws Exception {
		String inboundPath = wfImplConfig.getFptFilePath();
		String newFilePath = inboundPath + fileName + ".html";
		String content = null;
		try {
			File fileDir = new File(newFilePath);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(new FileInputStream(fileDir), StandardCharsets.UTF_8));
			content = in.lines().collect(Collectors.joining());
		} catch (IOException e) {
			logger.error("Error while convert file to string", e);
			throw new Exception("Error while read file");
		}
		logger.info("File content " + content);
		return content;
	}
	
	public String getPDFForm(String fileName,TLPolicyDetails tlPolicyDetails) throws Exception {
        logger.info("Genrate pdf started for ===========>>>>>"+fileName);
        String outFilePath = wfImplConfig.getFptFilePath();
        try {
            String outFileName = getOutFileName(fileName);
        
            String outfullPath = outFilePath+outFileName;
            logger.info("outfullPath --------------------------->"+outfullPath);
            
            OutputStream outStream = new FileOutputStream(outfullPath);
            final PdfRendererBuilder pdfBuilder = new PdfRendererBuilder();
            pdfBuilder.useFastMode();
            
			String baseUrl = getClass().getProtectionDomain().getCodeSource().getLocation().toString();
			
			String fileContent = readFile(fileName);
			fileContent = FPTUtils.replacePolicyDetails(fileContent,tlPolicyDetails);
			
			W3CDom w3cDom = new W3CDom();
            Document w3cDoc = w3cDom.fromJsoup(Jsoup.parse(fileContent));
            pdfBuilder.withW3cDocument(w3cDoc, baseUrl);
            pdfBuilder.toStream(outStream);
            pdfBuilder.run();
            outStream.close();
            logger.info("Genrate pdf completed for ===========>>>>>"+fileName);
            return outFileName;

        } catch (Exception e) {
        	logger.info("PDF could not be created: " + e.getMessage());
        	throw new Exception("Error while genrate pdf");
        }
	}

	public String getOutFileName(String fileName) {
		String outFileName = fileName+"-"+ System.nanoTime() + ESignCloudConstants.PDF_FILE_EXTENSTION;
		return outFileName;
	}

	@Override
	public Map<String, Object> initESign(ESignRequest eSignRequest) throws Exception {
		Map<String, Object> processVariables = runtimeService.getVariables(eSignRequest.getProcInstId());
		FPTRequest fptRequest = getFPTRequest(processVariables);
		
		String agreementId = getAgreementId(eSignRequest.getProcInstId());
		fptRequest.setAgreementUUID(agreementId);
		
		Map<String, Object> response = new HashMap<String, Object>();
		// setting error details
		response.put(Constants.STATUS_MESSAGE, Constants.FAILURE_MESSAGE);
		response.put(Constants.STATUS_MAP_KEY, Constants.FAILURE_CODE);

		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATE, Constants.FPT_INIT_STATE);
		
		validateFPTRequest(fptRequest);
		Map<String, Object> fptResponse = new HashMap<String, Object>();
		try {
			fptResponse  = certificateForSignCloudService.prepareCertificateForSignCloud(fptRequest);
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_BILL_CODE, fptResponse.get(Constants.FPT_BILL_CODE));
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATUS, fptResponse.get(Constants.STATUS_MAP_KEY));
		} catch (Exception e) {
			logger.error("Error while call FPT : ",e);
			throw new Exception("Error while hit fpt");
		}
		
		logger.info("Fpt status in : "+fptResponse.get(Constants.STATUS_MAP_KEY));
		// setting success details
		if(fptResponse.get(Constants.STATUS_MAP_KEY).equals(Constants.FPT_SUCCESS_STATUS) ||
				fptResponse.get(Constants.STATUS_MAP_KEY).equals(Constants.FPT_AGREMENT_EXISTS_STATUS) ) {
			response.put(Constants.STATUS_MESSAGE, Constants.SUCCESS_MESSAGE);
			response.put(Constants.STATUS_MAP_KEY, Constants.SUCCESS_CODE);
		}else {
			throw new FECException(ErrorCodes.INVALID_FPT_STATUS);
		}
		return response;
	}

	@Override
	public Map<String, Object> prepareESignFiles(ESignRequest eSignRequest) throws Exception {
		Map<String, Object> processVariables = runtimeService.getVariables(eSignRequest.getProcInstId());

		String agreementId = getAgreementId(eSignRequest.getProcInstId());
		Map<String,String> formNumberMap = genrateFormNumbers(eSignRequest.getProcInstId());
		logger.info("Form Numbers : "+formNumberMap.toString()+" for "+eSignRequest.getProcInstId());
		// storing form number into temporary variables  
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.POLICY_NUMBER,formNumberMap.get(Constants.POLICY_NUMBER));
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.APPLICATION_NUMBER_KEY,formNumberMap.get(Constants.APPLICATION_NUMBER_KEY));
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.ILLUSTRATION_NUMBER,formNumberMap.get(Constants.ILLUSTRATION_NUMBER));

		String policyStartDate = Utils.getCurrentDateInFormat(Constants.DATE_FORMAT_SLASH);
		
		Date currentdate = Utils.parseDate(policyStartDate, Constants.DATE_FORMAT_SLASH);
		currentdate = Utils.subtractDaysToDate(currentdate, 1);
		Date endDate = Utils.addYearsToDate(currentdate, 1);
		String policyEndDae = Utils.getDateInString(endDate, Constants.DATE_FORMAT_SLASH);
		
		// setting policy start date & end date
		runtimeService.setVariable(eSignRequest.getProcInstId(), WfImplConstants.TL_POLICY_START_DATE,policyStartDate);
		runtimeService.setVariable(eSignRequest.getProcInstId(), WfImplConstants.TL_POLICY_END_DATE,policyEndDae);
		
		TLPolicyDetails tlPolicyDetails = buildTLPolicyDetails(eSignRequest.getProcInstId(),processVariables,formNumberMap);
		
		tlPolicyDetails.setPurchasedDate(policyStartDate);
		tlPolicyDetails.setPlanExpiringDate(policyEndDae);
		
		ObjectMapper mapper = new ObjectMapper();
		logger.info("Policy Details for PDF docs : "+mapper.writeValueAsString(tlPolicyDetails));
		
		List<FPTFileRequest> fileMetaList = getFilesAndMetaDetails(tlPolicyDetails);
				
		Map<String, Object> dataToEmbed = new HashMap<String, Object>();
		
		Map<String, Object> response = new HashMap<String, Object>();
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATE,Constants.FPT_GENERATE_FILE_STATE);
		
		SignCloudResp fptResponse = new SignCloudResp();
		try {
			fptResponse = prepareFileForSignCloudService.prepareFilesForFPTSignCloud(agreementId,fileMetaList, dataToEmbed);
		} catch (Exception e) {
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_OPT_SEND,false);
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_IS_FILE_UPLOADED, false);
			logger.error("Error while upload file in FPT : ", e);
			throw new Exception("Error while upload files in fpt");
		}
		
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_BILL_CODE, fptResponse.getBillCode());
		buildRespFromFPTAndSentOTP(eSignRequest.getProcInstId(), (String) processVariables.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY), response, fptResponse,true);
		
		if(response.get(Constants.STATUS_MESSAGE).equals(Constants.SUCCESS_MESSAGE)) {
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_IS_FILE_UPLOADED, true);
			logger.info("File upload and OTP sent success for procId : "+  eSignRequest.getProcInstId());
		}else {
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_IS_FILE_UPLOADED, false);
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_IS_FILE_UPLOADED, false);
		}
		return response;
	}

	private static final String CUSTOMER_SIGN_IDENTIFIER = "Bên mua bảo";
	private static final String CUSTOMER_SIGN_COORDINATES = "0,-80";
	private static final String COUNTER_SIGN_IDENTIFIER = "Tư vấn viên";
	private static final String INSURANCE_COUNTER_SIGN_IDENTIFIER = "Ký, ghi rõ họ tên";
	
	public List<FPTFileRequest> getFilesAndMetaDetails(TLPolicyDetails tlPolicyDetails) throws Exception {
		List<FPTFileRequest> fileMetaList = new ArrayList<FPTFileRequest>();
		
		// Application
		String outFileName = getPDFForm(Constants.FPT_TL_APPLICAION_FORM,tlPolicyDetails);
		FPTFileRequest applicationFPTReq = getFPTFileRequest(outFileName,CUSTOMER_SIGN_COORDINATES,CUSTOMER_SIGN_IDENTIFIER);
		applicationFPTReq.setCustomerSignNeed(true);
		applicationFPTReq.setCounterSignNeed(true);
		applicationFPTReq.setCounterPositionIdentifier(COUNTER_SIGN_IDENTIFIER);
		applicationFPTReq.setCounterCoordinates(CUSTOMER_SIGN_COORDINATES);
		fileMetaList.add(applicationFPTReq);
		
		// illustration
		outFileName = getPDFForm(Constants.FPT_TL_ILLUSTRATION_FORM,tlPolicyDetails);
		FPTFileRequest illustrationFPTReq = getFPTFileRequest(outFileName,CUSTOMER_SIGN_COORDINATES,CUSTOMER_SIGN_IDENTIFIER);
		illustrationFPTReq.setCustomerSignNeed(true);
		illustrationFPTReq.setCounterSignNeed(true);
		illustrationFPTReq.setCounterPositionIdentifier(COUNTER_SIGN_IDENTIFIER);
		illustrationFPTReq.setCounterCoordinates(CUSTOMER_SIGN_COORDINATES);
		fileMetaList.add(illustrationFPTReq);
		
		// Insurance
		outFileName = getPDFForm(Constants.FPT_TL_INSURANCE_FORM,tlPolicyDetails);
		FPTFileRequest insuranceFPTReq = getFPTFileRequest(outFileName, CUSTOMER_SIGN_COORDINATES, "test");
		insuranceFPTReq.setCustomerSignNeed(false);
		insuranceFPTReq.setCounterSignNeed(true);
		insuranceFPTReq.setCounterPositionIdentifier(INSURANCE_COUNTER_SIGN_IDENTIFIER);
		insuranceFPTReq.setCounterCoordinates(CUSTOMER_SIGN_COORDINATES);
		fileMetaList.add(insuranceFPTReq);
		return fileMetaList;
	}

	public FPTFileRequest getFPTFileRequest(String outFileName, String coordinates, String position) {
		FPTFileRequest applicationFPTFileRequest = new FPTFileRequest();
		applicationFPTFileRequest.setName(outFileName);
		applicationFPTFileRequest.setCoordinates(coordinates);
		applicationFPTFileRequest.setPositionIdentifier(position);
		return applicationFPTFileRequest;
	}
	
	private Map<String, String> genrateFormNumbers(String procInstId) {
		TLNumberSequnces tlNumberSequnces = getTLNumberSequnces();
		
		String applicationNumber = Long.toString(tlNumberSequnces.getApplicationNumber());
		String policyNumber = Constants.TL_POLICY_NUMBER_PREFIX+applicationNumber;
//		workFlowApplication.setPolicyNumber(policyNumber);
//		workflowApplicationDao.save(workFlowApplication);
		
		String illustrationNumber = Long.toString(tlNumberSequnces.getIllustrationNumber());
		Map<String, String> formNumberMap = new HashMap<String, String>();
		formNumberMap.put(Constants.POLICY_NUMBER, policyNumber);
		formNumberMap.put(Constants.APPLICATION_NUMBER_KEY, applicationNumber);
		formNumberMap.put(Constants.ILLUSTRATION_NUMBER, illustrationNumber);
		return formNumberMap;
	}

	@Autowired
	private InsurerInfoDao insurerInfoDao;

	private TLPolicyDetails buildTLPolicyDetails(String procInstInd, Map<String, Object> processVariables, Map<String,String> formNumberMap) throws Exception {
		WorkFlowUserApplication  workFlowUserApplication = workflowUserApplicationDao.findByProcInstIdAndIsDeleted(procInstInd, false);
		
		WorkFlowApplication workFlowApplication = workFlowUserApplication.getWfApplication();
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(workFlowUserApplication.getUserIdentifier(), false);
		
		String dateOfBirth = Utils.getStringValue(workFlowUser.getDob());
		logger.info(" <========= dateOfBirth =========> " + dateOfBirth);
		int age = Utils.getAge(dateOfBirth);
		
		TLPolicyDetails tlPolicyDetails= new TLPolicyDetails();
		tlPolicyDetails.setContractNumber(formNumberMap.get(Constants.APPLICATION_NUMBER_KEY));
		tlPolicyDetails.setPolicyNumber(formNumberMap.get(Constants.POLICY_NUMBER));
		tlPolicyDetails.setIllustrationNumber(formNumberMap.get(Constants.ILLUSTRATION_NUMBER));
		
		tlPolicyDetails.setPolicySellerName(workFlowApplication.getInsurer());
		buildPolicyHolderDetails(workFlowUser, tlPolicyDetails);
		logger.info("all plan ids ------>"+workFlowApplication.getPlanId());
		String mainPlanId= Utils.getMainPlanId(workFlowApplication.getPlanId());
		logger.info("mainPlanId is ------>"+mainPlanId);
		
		// set Premium Details
		PlanPremiums planPremiums = Constants.SUM_PPD_CRITICAL_PROTE_AMOUNT.get(mainPlanId);
		if(planPremiums == null) {
			logger.error("planPremiums is null for "+mainPlanId);
			throw new Exception("Invalid Plan Premiums for "+mainPlanId);
		}
		
		TermLifeAgePremiumRates premiumRates = Constants.AGE_PREMIUM_MAP.get(String.valueOf(age));
		if(premiumRates == null) {
			logger.error("premiumRates is null for "+age);
			throw new Exception("Invalid premium Rates for age "+age);
		}
		
		List<WorkFlowPolicy> workFlowPolicyList = workPolicyDAO.findByWorkFlowApplicationAndIsDeleted(workFlowApplication, false);
		if(workFlowPolicyList ==null || workFlowPolicyList.isEmpty()) {
			logger.error("workFlowPolicy list is null for appId "+workFlowApplication.getId());
			throw new Exception("Invalid workFlow Policy for appId "+workFlowApplication.getId());
		}
		
		Map<String, WorkFlowPolicy> policyMap = getPlanIdAndPolicyMap(workFlowPolicyList);
		WorkFlowPolicy workFlowPolicy = policyMap.get(mainPlanId);
		if(workFlowPolicy == null ) {
			logger.error("workFlowPolicy is null for mainPlanId "+mainPlanId);
			throw new Exception("Invalid workFlow Policy for mainPlanId "+mainPlanId);
		}
		WorkFlowPolicy criticalIllnessWorkFlowPolicy = policyMap.get(Constants.CRITICAL_ILLNESS_PROTECTION_PLAN_ID);
		
		// setting production amount
		tlPolicyDetails.setTlProductionAmount(Utils.getInterNationalNumber(Integer.toString(planPremiums.getSumInsuredAmount())));
		tlPolicyDetails.setPermanentDisabilityProductionAmount(Utils.getInterNationalNumber(Integer.toString(planPremiums.getPpdAmount())));
		
		// setting premium amount
		Double premiumAmount = 0.0;
		premiumAmount = Utils.getTLPremiumsByRate(planPremiums.getSumInsuredAmount(), premiumRates.getPremiumRate());
		tlPolicyDetails.setTlPremiumAmount(Utils.getInterNationalNumber(Integer.toString((int)(Math.round(premiumAmount))) ));
		
		premiumAmount = Utils.getTLPremiumsByRate(planPremiums.getPpdAmount(), premiumRates.getPpdRate());
		tlPolicyDetails.setPermanentDisabilityPremiumAmount(Utils.getInterNationalNumber(Integer.toString((int)(Math.round(premiumAmount))) ));
		
		if(criticalIllnessWorkFlowPolicy != null) {
			// setting premium amount
			premiumAmount = Utils.getTLPremiumsByRate(planPremiums.getCriticalIllenessAmount(), premiumRates.getCriticalRate());
			tlPolicyDetails.setCriticalIllnessPremiumAmount(Utils.getInterNationalNumber(Integer.toString((int)(Math.round(premiumAmount))) ));
			
			// setting production amount
			tlPolicyDetails.setCriticalIllnessProductionAmount(Utils.getInterNationalNumber(Integer.toString(planPremiums.getCriticalIllenessAmount())));
		}else {
			tlPolicyDetails.setCriticalIllnessPremiumAmount("0");
			tlPolicyDetails.setCriticalIllnessProductionAmount("0");
		}
		
		// setting total amount
		tlPolicyDetails.setTotalAmount(Utils.getInterNationalNumber(Integer.toString(getTotalAmount(tlPolicyDetails))));
		
		// setting what you pay amount
		tlPolicyDetails.setWhatYouPayAmount(Utils.getInterNationalNumber(workFlowPolicy.getMontlyPayment()));
		tlPolicyDetails.setAgentCode(Constants.AGENT_ID_VALUE);
		
		InsurerInfoModel insurerInfoModel = insurerInfoDao.findByProcessInstanceId(procInstInd);
		if(insurerInfoModel != null) {
			IPATPaymentConfirmModel iPATPaymentConfirmModel = insurerInfoModel.getiPatPaymentConfirm();
			if(iPATPaymentConfirmModel!= null) {
				Date date = iPATPaymentConfirmModel.getCreated();
				tlPolicyDetails.setProductionDuraion( Utils.getDateInString( date, Constants.DATE_FORMAT_SLASH));
			}
		}
		
		List<NomineeDetails> nomineeDetailsList = buildNomineeDetails(processVariables);
		tlPolicyDetails.setNomiantedDetailsList(nomineeDetailsList);
		return tlPolicyDetails;
	}

	public Map<String, WorkFlowPolicy> getPlanIdAndPolicyMap(List<WorkFlowPolicy> workFlowPolicyList) {
		Map<String, WorkFlowPolicy> policyMap = new HashMap<String, WorkFlowPolicy>();
		
		for (WorkFlowPolicy workFlowPolicy : workFlowPolicyList) {
			policyMap.put(workFlowPolicy.getPlanId(), workFlowPolicy);
		}
		return policyMap;
	}

	public List<NomineeDetails> buildNomineeDetails(Map<String, Object> processVariables) {
		
		List<NomineeDetails> nomiantedDetailsList = new ArrayList<NomineeDetails>();
		
		for (int i = 1; i <= Constants.BENEFICIARY_MAX_COUNT; i++) {
			NomineeDetails nomiantedDetails = new NomineeDetails();
			logger.info("Nominee number ------>" + i);
			String name = (String) processVariables
					.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_NAME);
			logger.info("Nominee name ------>" + name);
			if (name == null || name.isEmpty()) {
				continue;
			}
			nomiantedDetails.setName(name);
			nomiantedDetails.setDob((String) processVariables
					.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_DOB));

			nomiantedDetails.setGender((String) processVariables
					.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_GENDER));
			nomiantedDetails.setNid((String) processVariables
					.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_NID));
			nomiantedDetails.setRelation((String) processVariables
					.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_RELATION));

			try {
				logger.info("beneficiary1_share --->" + processVariables
						.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_SHARE));
				if (processVariables.get("beneficiary1_share") != null) {
					nomiantedDetails.setShare(Integer.toString((Integer) processVariables
							.get(Constants.BENEFICIARY + i + Constants.UNDER_SCORE + Constants.BENEFICIARY_SHARE)));
				}
			} catch (Exception e) {
				logger.error("////////////////////////////////////");
				logger.error("Error while convert beneficiary1_share", e);
				logger.error("////////////////////////////////////");
			}
			nomiantedDetailsList.add(nomiantedDetails);
		}
		
//		NomineeDetails nomiantedDetails = new NomineeDetails();
//		nomiantedDetails.setName( (String) processVariables.get("beneficiary1_fullName"));
//		nomiantedDetails.setDob((String)processVariables.get("beneficiary1_dob"));
//		nomiantedDetails.setGender("");
//		nomiantedDetails.setNid((String)processVariables.get("beneficiary1_nid"));
//		nomiantedDetails.setRelation((String)processVariables.get("beneficiary1_relation"));

		return nomiantedDetailsList;
	}
	
	private Integer getTotalAmount(TLPolicyDetails tlPolicyDetails) {
		Integer total = 0;
		if(tlPolicyDetails.getTlPremiumAmount() != null) {
			total += Integer.parseInt( Utils.ConvertViatnumCurrencyToDigits(tlPolicyDetails.getTlPremiumAmount())) ;
		}
		if(tlPolicyDetails.getPermanentDisabilityPremiumAmount() != null) {
			total += Integer.parseInt( Utils.ConvertViatnumCurrencyToDigits(tlPolicyDetails.getPermanentDisabilityPremiumAmount())) ;
		}
		if(tlPolicyDetails.getCriticalIllnessPremiumAmount() != null) {
			total += Integer.parseInt(Utils.ConvertViatnumCurrencyToDigits(tlPolicyDetails.getCriticalIllnessPremiumAmount())) ;
		}
		return total;
	}
	public void buildPolicyHolderDetails(WorkFlowUser workFlowUser, TLPolicyDetails tlPolicyDetails)
			throws ParseException {
		tlPolicyDetails.setPolicyHolderName(workFlowUser.getNid_user_name());
		String nid_gender = workFlowUser.getNid_gender();
		logger.info("nid_gender --> " + nid_gender);
		String vietnameGender = Constants.Gender.getVietnameName(nid_gender);
		logger.info("vietnameGender --->" + vietnameGender);
		tlPolicyDetails.setPolicyHolderGender(vietnameGender);
		tlPolicyDetails.setPolicyHolderDOB(workFlowUser.getNid_dob());
		tlPolicyDetails.setPolicyHolderEmail(workFlowUser.getEmail());
		tlPolicyDetails.setPolicyHolderNID(workFlowUser.getNationalId());
		tlPolicyDetails.setPolicyHolderPhone(Constants.MOBILE_NUMBER_APPENDER_ZERO + workFlowUser.getMobileNumber());
		
		String dateOfBirth = Utils.getStringValue(workFlowUser.getDob());
		tlPolicyDetails.setPolicyHolderAge(Integer.toString(Utils.getAge(dateOfBirth)));
		tlPolicyDetails.setPolicyHolderAddress(getAddress(workFlowUser));
	}

	private String getAddress(WorkFlowUser workFlowUser) {
		List<WorkFlowUserAddress> wfAddressList = workFlowAddressDao.findByWfUser(workFlowUser);
		if (wfAddressList != null && !wfAddressList.isEmpty()) {
			for (WorkFlowUserAddress workFlowUserAddress : wfAddressList) {
				String addressLine1 = workFlowUserAddress.getAddressLine1() != null
						? workFlowUserAddress.getAddressLine1()
						: "";
			return addressLine1;
			}

		}		return null;
	}

	@Override
	public Map<String, Object> authorizeESign(ESignRequest eSignRequest) throws Exception {
		Map<String, Object> processVariables = runtimeService.getVariables(eSignRequest.getProcInstId());
		boolean isFileUploaded = false;
		if(processVariables.get(Constants.FPT_IS_FILE_UPLOADED) != null) {
			isFileUploaded = (boolean) processVariables.get(Constants.FPT_IS_FILE_UPLOADED);
		}
		if( ! isFileUploaded) {
			throw new FECException(ErrorCodes.FPT_FILE_NOT_UPLOAD);
		}
		
		String agreementId = getAgreementId(eSignRequest.getProcInstId());
		String billCode = getBillCode(eSignRequest.getProcInstId());
		
		Map<String, Object> response = new HashMap<String, Object>();
		// fpt status
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATE,Constants.FPT_AUTHORIZE_STATE);
		
		SignCloudResp fptResponse = new SignCloudResp();
		try {
			fptResponse = authorizeFPTService.authorizeSigningForSignCloud(agreementId,
					eSignRequest.getAuthorizeCode(), billCode);
			
		} catch (Exception e) {
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_ISAUTHORIZE,false);
			logger.error("Error while authorize ESign in FPT : ", e);
			throw new Exception("Error while authorize ESign in fpt");
		}
		
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_BILL_CODE, fptResponse.getBillCode());
		buildRespFromFPTAndSentOTP(eSignRequest.getProcInstId(), (String) processVariables.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY), response, fptResponse,false);
		
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_ISAUTHORIZE,false);
		if(response.get(Constants.STATUS_MESSAGE).equals(Constants.SUCCESS_MESSAGE)) {
			logger.info("OTP autorization success for procId : "+  eSignRequest.getProcInstId());
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_ISAUTHORIZE,true);
		}else{
			logger.info("OTP autorization failed for procId : "+  eSignRequest.getProcInstId());
		}
		return response;
	}


	@Override
	public Map<String, Object> regenerateESignOTP(ESignRequest eSignRequest) throws Exception {

		Map<String, Object> processVariables = runtimeService.getVariables(eSignRequest.getProcInstId());
		
		// check file is uploaded or not
		boolean isFileUploaded = false;
		if(processVariables.get(Constants.FPT_IS_FILE_UPLOADED) != null) {
			isFileUploaded = (boolean) processVariables.get(Constants.FPT_IS_FILE_UPLOADED);
		}
		
		if( ! isFileUploaded) {
			logger.info("File is not uploaded yet, so prpare files");
			initESign(eSignRequest);
			return prepareESignFiles(eSignRequest);
		}
		
		String userName = Utils.getStringValue(	processVariables.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
		
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser == null) {
			throw new UsernameNotFoundException("user does not exist");
		}
		Map<String, Object> response = new HashMap<String, Object>();
		String agreementId = getAgreementId(eSignRequest.getProcInstId());
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATE,Constants.FPT_RESENT_OTP_STATE);
		
		SignCloudResp fptResponse = new  SignCloudResp();
		try {
			fptResponse = authorizeFPTService.regenerateAuthorisationCodeForSignCloud(agreementId,Utils.getStringValue(workFlowUser.getMobileNumber()));
		} catch (Exception e) {
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_OPT_SEND, false);
			logger.error("Error while regenerate otp in FPT : ", e);
			throw new Exception("Error while regenerate otp in fpt");
		}
		
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_BILL_CODE, fptResponse.getBillCode());
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_RESENT_OPT_SEND, true);

		buildRespFromFPTAndSentOTP(eSignRequest.getProcInstId(),(String) processVariables.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY),
				response, fptResponse, true);
		return response;
	}


	@Override
	public Map<String, Object> downLoadESignDoc(ESignRequest eSignRequest) throws Exception {
		String agreementId = getAgreementId(eSignRequest.getProcInstId());
		String billCode = getBillCode(eSignRequest.getProcInstId());
		
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATE,Constants.FPT_FILE_DOWNLOAD_STATE);
		SignCloudResp fptResponse = signedFileForSignCloudService.getSignedFileForSignCloud(agreementId, billCode, true,"111",eSignRequest.getProcInstId());
		Map<String, Object> response = new HashMap<String, Object>();
		// setting error details
		response.put(Constants.STATUS_MESSAGE, Constants.FAILURE_MESSAGE);
		response.put(Constants.STATUS_MAP_KEY, Constants.FAILURE_CODE);

		Integer fptStatus = fptResponse.getResponseCode();
		logger.info("download fptStatus is :"+fptStatus+" for : "+eSignRequest.getProcInstId());
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_RESPONSE_MESSAGE, fptResponse.getResponseMessage());
		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_STATUS, fptResponse.getResponseCode());
		
		// setting success details
		if (fptStatus.equals(Constants.FPT_SUCCESS_STATUS)) {
			logger.info("file downloaded successfully for "+ eSignRequest.getProcInstId());
			response.put(Constants.STATUS_MESSAGE, Constants.SUCCESS_MESSAGE);
			response.put(Constants.STATUS_MAP_KEY, Constants.SUCCESS_CODE);
			
			// set fpt completed
			runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.IS_FPT_COMPLETED, "true");

		} else if (fptStatus.equals(Constants.FPT_FILE_IS_BEING_PROCESSED_STATUS)) {
			logger.info("file is being processed for "+ eSignRequest.getProcInstId());
			response.put(Constants.STATUS_MESSAGE, Constants.FPT_RETRY_MESSAGE);
			response.put(Constants.STATUS_MAP_KEY, Constants.SUCCESS_CODE);
			
		} else if (fptStatus.equals(Constants.FPT_BILLCODE_TIMEOUT_STATUS)) {
			logger.info("billcode timeout for "+ eSignRequest.getProcInstId());
			response.put(Constants.STATUS_MESSAGE, Constants.FPT_BILLCODE_TIME_OUT_MESSAGE);
		}else {
			throw new FECException(ErrorCodes.INVALID_FPT_STATUS);
		}
		
//		runtimeService.setVariable(eSignRequest.getProcInstId(), Constants.FPT_BILL_CODE, fptResponse.getBillCode());
		return response;
	}

	public String getAgreementId(String procInstInd) throws Exception {
		WorkFlowApplication workFlowApplication = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(procInstInd, false);
		if(workFlowApplication == null || workFlowApplication.getApplicationId() == null || workFlowApplication.getApplicationId().isEmpty()) {
			throw new Exception("Invalid Application Id");
		}
		return workFlowApplication.getApplicationId();
	}
	
	private String getBillCode(String procInstId) throws Exception {

		Map<String, Object> processVariables = runtimeService.getVariables(procInstId);
		
		if(processVariables.get(Constants.FPT_BILL_CODE) == null) {
			throw new Exception("Invalid Bill Code");
		}
		return (String) processVariables.get(Constants.FPT_BILL_CODE);
	}
	public void sentOTP(String otp, String contactNumber) throws Exception {
//		contactNumber = "+84" + contactNumber.replaceAll("^0", "");
		WorkFlowUserSms workFlowUserSms = new WorkFlowUserSms();
		
		String message = "{OTP}  la ma xac nhan ung dung FE SHIELD cua QK";
		message = message.replace("{OTP}", otp);
		
		workFlowUserSms.setMessage(message);
		workFlowUserSms.setMobileNumber(contactNumber);
		logger.info("FPT otp service init for : "+contactNumber);
		logger.info("FPT otp service message : "+message);
		notifyUserServices.sendSmsNotification(workFlowUserSms);
		logger.info("FPT otp service complete for : "+contactNumber);
	}
	
	public void buildRespFromFPTAndSentOTP(String procInstId, String assignee, Map<String, Object> response,
			SignCloudResp fptResponse, boolean isSendOTP) throws Exception {
		
		logger.info("FPT status : "+fptResponse.getResponseCode()+" , procInstId : "+procInstId);
		logger.info("FPT message : "+fptResponse.getResponseMessage()+" , procInstId : "+procInstId);
		
		runtimeService.setVariable(procInstId, Constants.FPT_RESPONSE_MESSAGE, fptResponse.getResponseMessage());
		runtimeService.setVariable(procInstId, Constants.FPT_STATUS, fptResponse.getResponseCode());
		runtimeService.setVariable(procInstId, Constants.FPT_REMAINING_COUNTER, fptResponse.getRemainingCounter());
		
		response.put(Constants.FPT_STATUS, fptResponse.getResponseCode());
		// setting error details
		response.put(Constants.STATUS_MESSAGE, Constants.FAILURE_MESSAGE);
		response.put(Constants.STATUS_MAP_KEY, Constants.FAILURE_CODE);
		// setting success details
		if (Constants.FPT_SUCCESS_STATUS.equals(fptResponse.getResponseCode()) || Constants.FPT_FILE_UPLOAD_SUCCESS_STATUS.equals(fptResponse.getResponseCode())) {
			if (isSendOTP) {
				String userName = Utils.getStringValue(assignee);
				WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
				if (workFlowUser == null) {
					throw new UsernameNotFoundException("user does not exist");
				}
				sentOTP((String) fptResponse.getAuthorizeCredential(),
						Utils.getStringValue(workFlowUser.getMobileNumber()));
				runtimeService.setVariable(procInstId, Constants.FPT_OPT_SEND, true);
			}
			response.put(Constants.STATUS_MESSAGE, Constants.SUCCESS_MESSAGE);
			response.put(Constants.STATUS_MAP_KEY, Constants.SUCCESS_CODE);
		}else {
			throw new Exception(ErrorMessages.INVALID_FPT_STATUS);
		}
		
	}
	
	public FPTRequest getFPTRequest(Map<String, Object> processVariables) {
		FPTRequest fptRequest = new FPTRequest();
		
		fptRequest.setPersonalName(Utils.getStringValue(processVariables.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		fptRequest.setPersonalID(Utils.getStringValue(processVariables.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
		fptRequest.setCertificateProfile(wfImplConfig.getFptCertificateProfile());
		fptRequest.setCountry(Utils.getStringValue(processVariables.get(WfImplConstants.NATIONALITY_HYPERVERGE)));
		fptRequest.setLocation(Utils.getStringValue(processVariables.get(WfImplConstants.ADDRESS_HYPERVERGE)));
//		fptRequest.setStateProvince(Utils.getStringValue(processVariables.get(WfImplConstants.PROVINCE_HYPERVERGE)));
		fptRequest.setStateProvince("HỒ CHÍ MINH");
		
		String userName = Utils.getStringValue(	processVariables.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
		
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser == null) {
			throw new UsernameNotFoundException("user does not exist");
		}
		fptRequest.setAuthorizationMobileNo(Utils.getStringValue(workFlowUser.getMobileNumber()));
		fptRequest.setAuthorizationEmail(Utils.getStringValue(workFlowUser.getEmail()));
		return fptRequest;
	}
	
private void validateFPTRequest(FPTRequest fptRequest) throws Exception {
		if(fptRequest == null) {
			throw new Exception("Invalid FPT Request");
		}
		if(fptRequest.getPersonalName() == null || fptRequest.getPersonalName().isEmpty()) {
			throw new Exception("Invalid Personal Name");
		}
		if(fptRequest.getPersonalID() == null || fptRequest.getPersonalID().isEmpty()) {
			throw new Exception("Invalid Personal Id");
		}
		if(fptRequest.getLocation() == null || fptRequest.getLocation().isEmpty()) {
			throw new Exception("Invalid Location");
		}
		if(fptRequest.getCountry() == null || fptRequest.getCountry().isEmpty()) {
			throw new Exception("Invalid Country");
		}
		if(fptRequest.getStateProvince()== null || fptRequest.getStateProvince().isEmpty()) {
			throw new Exception("Invalid State");
		}
		if(fptRequest.getAgreementUUID()== null || fptRequest.getAgreementUUID().isEmpty()) {
			throw new Exception("Invalid AgreementUUID");
		}
	}

}
