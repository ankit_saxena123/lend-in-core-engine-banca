package com.kuliza.lending.wf_implementation.services;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.CMSIntegrationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.FECreditCMSCheckEligibilityIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditCMSPostTransactionIntegration;
import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.pojo.PostTransactionInput;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class FECreditCMSPostTransactionService {

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	private CMSIntegrationDao cmsIntegrationDao;

	@Autowired
	private FECreditCMSPostTransactionIntegration postTranxIntegration;

	@Autowired
	private FECreditCMSCheckEligibilityIntegration fecCreditEligibility;
	
	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory.getLogger(FECreditCMSPostTransactionService.class);

	public ApiResponse CheckAccount(Map<String, Object> variables, PostTransactionInput body) throws Exception {
		String accNo = null;
		String tenor = null;
		String premiumAmount = null;
		Map<String, Object> mapResponse = null;
		try {

			JSONObject requestPayload = fecCreditEligibility.getrequestPayload(variables);
			logger.info("FECredit Eligibility Request for "+body.getProcessInstanceId() +","+requestPayload.toString());
			JSONObject responseAsJson = fecCreditEligibility.getDataFromService(requestPayload);
			logger.info("FECredit Eligibility Response for "+body.getProcessInstanceId() +","+responseAsJson.toString());
			
			mapResponse = fecCreditEligibility.parseResponse(responseAsJson);
			if (mapResponse.get(Constants.CMS_CHECK_ELIGIBILITY_ACCOUNT) != null) {
				accNo = Utils.getStringValue(mapResponse.get(Constants.CMS_CHECK_ELIGIBILITY_ACCOUNT));
				tenor = body.getTenor();
				premiumAmount = body.getPremiumAmount();
				premiumAmount=premiumAmount.replace(".","");
				logger.info("--------->>>>>>>>>>>>> premium amount **********->>>>>      " + premiumAmount);
				String lastdigit = accNo.substring(accNo.length() - 4);
				logger.info("--------->>>>>>>>>>>>> lastdigit    " + lastdigit + "  " + body.getAccountNumber());
				if (body.getAccountNumber().equals(lastdigit)) {
					logger.info("--------->>>>>>>>>>>>> INSIDE EQUAL ACC NO*************");
					return CMSpostTransaction(body.getProcessInstanceId(), variables, accNo, tenor, premiumAmount);

				}
			} else {

				return new ApiResponse(HttpStatus.NOT_FOUND, Constants.DATA_NOT_FOUND,
						Constants.POST_TRANSACTION_ACC_NOT_FOUND);
			}

			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.DATA_NOT_FOUND,
					Constants.POST_TRANSACTION_ACC_NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call CMS api :" + e.getMessage(), e);
		}
		return CMSpostTransaction(body.getProcessInstanceId(), variables, accNo, tenor, premiumAmount);

	}

	public ApiResponse CMSpostTransaction(PostTransactionInput body) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		logger.info("CMS Post called for "+body.getProcessInstanceId()+", Request : "+mapper.writeValueAsString(body));
		
		Map<String, Object> variables = runtimeService.getVariables(body.getProcessInstanceId());
		return CheckAccount(variables, body);
	}

	private ApiResponse CMSpostTransaction(String processInstanceId, Map<String, Object> variables, String accNo,
			String tenor, String premiumAmount) throws Exception {
		logger.info("CMSpostTransaction called for : "+processInstanceId +",premiumAmount : "+premiumAmount+", tenor : "+tenor+" ,accNo : "+accNo);
		String api_response = null;
		logger.info(" what u pay amount : "+Utils.ConvertViatnumCurrencyToDigits(Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT))));
		if(premiumAmount==null)
			premiumAmount=Utils.ConvertViatnumCurrencyToDigits(Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)));
		if(tenor==null)
			tenor="1";
		logger.info("**************premiumAmount:"+premiumAmount+"*******tenor"+tenor);
		WorkFlowApplication workFlowApp = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId,
				false);
		if(workFlowApp==null)
			throw new Exception("No Application is runnging for processInstanceId :"+processInstanceId);
		
		String tranxId = Utils.generateUUIDTransanctionId();
		String refNo = Utils.getStringValue(workFlowApp.getId());
		
		Map<String, Object> mapResponse = null;
		try {

			JSONObject requestPayload = postTranxIntegration.requestPayload(variables, accNo, tenor, premiumAmount,
					tranxId, refNo);
			logger.info("Post Tranx Request for "+processInstanceId+", Request : "+requestPayload.toString());
			
			JSONObject responseAsJson = postTranxIntegration.getDataFromService(requestPayload);
			logger.info("Post Tranx Response for "+processInstanceId+", Response : "+responseAsJson.toString());
			
			mapResponse = postTranxIntegration.parseResponse(responseAsJson);
			logger.info("Post Tranx Parsed Response for "+processInstanceId+", Response : "+mapResponse.toString());
			
			api_response = Utils.getStringValue(mapResponse);
			Integer statusCode=(Integer) mapResponse.get(Constants.POST_TRANSACTION_CODE);
			boolean status = (boolean) mapResponse.get(Constants.STATUS_MAP_KEY);
			CMSIntegration cmsIntegration = new CMSIntegration();
			if (status && statusCode==1) {
				boolean payment_confirm = true;
				persistCMSInfo(cmsIntegration,api_response, accNo, tenor, premiumAmount, payment_confirm, variables, tranxId, refNo,
						workFlowApp);
				setProcessVariables(variables, tenor, premiumAmount, processInstanceId);
				
				logger.info("Product Id is : "+variables.get(WfImplConstants.JOURNEY_PRODUCT_ID));
				// other than term life insurance
				if( ! variables.get(WfImplConstants.JOURNEY_PRODUCT_ID).equals(WfImplConstants.TL_PRODUCT_ID)) {
					boolean insurerStatus=callInsurerAPI(processInstanceId, tranxId, workFlowApp, cmsIntegration, false);
					if(insurerStatus){
						cmsIntegration.setInsurerStatus(insurerStatus);
						cmsIntegrationDao.save(cmsIntegration);
					}
				}
				
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, mapResponse);
			} else {
				boolean payment_confirm = false;
				persistCMSInfo(cmsIntegration,api_response, accNo, tenor, premiumAmount, payment_confirm, variables, tranxId, refNo,
						workFlowApp);
				return new ApiResponse(HttpStatus.OK, Constants.TRANSACTION_FAILED,mapResponse);
			}

		} catch (Exception e) {
			logger.error("unable to call insurer api :" + e.getMessage(), e);
			throw e;
		} finally {
			logger.info(" **************************End of PT CMSpostTransaction*************");
		}

	}

	private void setProcessVariables(Map<String, Object> variables, String tenor, String premiumAmount,
			String processInstanceId) {
		logger.info("In setProcessVariables ---> premiumAmount ---> "+premiumAmount);
		String expireDatesInString =null;
			Date currentDate = new Date();
			logger.info(" **************************TENOR*********"+ tenor);
			String fullpremiumAmount=Utils.ConvertViatnumCurrencyToDigits(Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)));
			if(variables.get(WfImplConstants.JOURNEY_PRODUCT_ID).equals(WfImplConstants.CV_PRODUCT_ID)) {
				if(variables.get(WfImplConstants.JOURNEY_PLAN_ID).equals(WfImplConstants.BASIC_PLAN_ID)) {
				Date expireDates = Utils.addMonthsToDate(currentDate, 3);
				expireDatesInString = Utils.convertToZonedDate(expireDates, Constants.VIETNAM_TIME_ZONE, Constants.DATE_FORMAT_SLASH);
				 
				}else {
					Date expireDates = Utils.addMonthsToDate(currentDate, 6);
					expireDatesInString = Utils.convertToZonedDate(expireDates, Constants.VIETNAM_TIME_ZONE, Constants.DATE_FORMAT_SLASH);
				}
			}else {
				Date expireDates = Utils.subtractDaysToDateAddYearsToDate(currentDate, 1);
				logger.info("<======expireDates before removing day one========> "+expireDates);
				logger.info("<======expireDates========> "+expireDates);
				expireDatesInString = Utils.convertToZonedDate(expireDates, Constants.VIETNAM_TIME_ZONE, Constants.DATE_FORMAT_SLASH);
			}
			
			
			Map<String, Object> data = new HashMap<String, Object>();
			data.put(WfImplConstants.IPAT_PLAN_EXPIRING_DATE, expireDatesInString);
			
			data.put(WfImplConstants.IPAT_PAYMENT_START_DATE,Utils.convertToZonedDate(currentDate, Constants.VIETNAM_TIME_ZONE, Constants.DATE_FORMAT_SLASH));
			
			data.put(WfImplConstants.IPAT_PAYMENT_START_DATE_TIME,Utils.convertToZonedDate(currentDate,Constants.VIETNAM_TIME_ZONE,Constants.DATE_TIME_FORMAT_SLASH));
			data.put(Constants.USER_PREMIUM_TENURE, tenor);
			logger.info("<======tenor========>"+tenor);
			if (tenor.equals("12")) {
				Date addOneMonthsToDate = Utils.addMonthsToDate(currentDate, 1);
				String dateInString = Utils.convertToZonedDate(addOneMonthsToDate,Constants.VIETNAM_TIME_ZONE, Constants.DATE_FORMAT_SLASH);
				String whatYouPay = (Utils.convertToViantnumCurrencyTrieu(Utils.getDoubleValue(premiumAmount)));
				logger.info("<=============>whatyouPay<=========>"+whatYouPay);
				data.put(WfImplConstants.IPAT_NEXT_PAYMENT_DUE, dateInString);
//				data.put(WfImplConstants.JOURNEY_MONTHLY_PAYMENT, whatYouPay);
				data.put(WfImplConstants.FULL_PREMIUM_AMOUNT, fullpremiumAmount);
			} else {
				data.put(WfImplConstants.IPAT_NEXT_PAYMENT_DUE, expireDatesInString);
				data.put(WfImplConstants.TOTAL_PREMIUM_AMOUNT, premiumAmount);
				logger.info("totalPremiumAmount ---> "+premiumAmount);

			}
			
			logger.info("CMS data to update in workflow for "+processInstanceId+", data : "+data.toString());
			runtimeService.setVariables(processInstanceId, data);
	}
	
	

	private boolean callInsurerAPI(String processInstanceId, String paymentId, WorkFlowApplication workFlowApp, CMSIntegration cmsIntegration, boolean isHistory) {
		boolean status=false;
		try {
			logger.info("-->Entering CMS callInsurerAPI : "+processInstanceId);
			
			Map<String, Object> variables = new HashMap<String, Object>();
			
			if(isHistory) {
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables()
						.singleResult();
				variables = processInstance.getProcessVariables();
			}else{
				variables = runtimeService.getVariables(processInstanceId);
			}
			
			InsurerAbstract insurer = InsurerFactoryClass.getInsurer(variables);
			status=insurer.postPolicyDetails(processInstanceId, variables, paymentId, workFlowApp, null, cmsIntegration);
		} catch (Exception e) {
			logger.error("<=====> calling isurer api is failed for procId : "+processInstanceId+" : " + e.getMessage(), e);
			status=false;
		}
		return status;

	}

	private void persistCMSInfo(CMSIntegration cmsIntegration, String apiResponse, String accNo, String tenor, String premiumAmount,
			boolean payment_confirm, Map<String, Object> variables, String tranxNo, String refNo,
			WorkFlowApplication workFlowApp) throws Exception {

		logger.info("--->persistCMSInfo");
		cmsIntegration.setApiResponse(apiResponse);
		cmsIntegration.setAccNo(accNo);
		cmsIntegration.setPaymentConfirmStatus(payment_confirm);
		cmsIntegration.setPremiumAmount(premiumAmount);
		cmsIntegration.setTenor(tenor);
		cmsIntegration.setTransactionId(tranxNo);
		cmsIntegration.setRefNum(refNo);
		cmsIntegration.setWfApplication(workFlowApp);
		cmsIntegrationDao.save(cmsIntegration);

	}
	
	public ApiResponse callInsurerAPIByProcId(String processInstanceId)throws Exception {
		logger.info("Term Life call insurer api called for processInstanceId : "+processInstanceId);
		
		WorkFlowApplication workFlowApp = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if(workFlowApp == null) {
			logger.error("workFlowApp is null for : "+processInstanceId);
			throw new FECException(ErrorCodes.INVALID_WORKFLOW_APP);
		}
		
		CMSIntegration cmsIntegration = cmsIntegrationDao.findByWfApplicationAndPaymentConfirmStatusAndIsDeleted(workFlowApp, true, false);
		if(cmsIntegration == null) {
			logger.error("cmsIntegration is null for : "+processInstanceId);
			throw new FECException(ErrorCodes.INVALID_CMS_INTEGRATION);
		}
		
		boolean status =callInsurerAPI(processInstanceId, cmsIntegration.getTransactionId(), workFlowApp, cmsIntegration, true);
		logger.info("insurer status : "+status);
		if(status){
			cmsIntegration.setInsurerStatus(status);
			cmsIntegrationDao.save(cmsIntegration);
		}
		
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, null);
	}

}