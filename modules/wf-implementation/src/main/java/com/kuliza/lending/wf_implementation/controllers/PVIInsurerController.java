package com.kuliza.lending.wf_implementation.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.PVIInsurerInput;
import com.kuliza.lending.wf_implementation.services.PVIInsurerService;

@RestController
@RequestMapping("/fec-shield")
public class PVIInsurerController {
	
	@Autowired
	private PVIInsurerService pviInurerService;
	
	private static final Logger logger = LoggerFactory.getLogger(PVIInsurerController.class);
	
	/**
	 * this api is called by pvi insurer to confirm the policy.
	 * @param pviInsurerInput
	 * @return ResponseEntity<Object>
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/pvi-insurer-confirm")
	public ResponseEntity<Object> pviInsurerConfirm(
			@RequestBody @Valid PVIInsurerInput pviInsurerInput) {
		try {
			return CommonHelperFunctions.buildResponseEntity(pviInurerService.pviInsurerConfirm(pviInsurerInput));
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}
	
}
