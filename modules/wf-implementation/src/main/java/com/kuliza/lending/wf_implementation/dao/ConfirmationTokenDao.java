package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;

import com.kuliza.lending.wf_implementation.models.ConfirmationToken;

public interface ConfirmationTokenDao extends CrudRepository<ConfirmationToken, String> {
	ConfirmationToken findByConfirmationToken(String confirmationToken);
}