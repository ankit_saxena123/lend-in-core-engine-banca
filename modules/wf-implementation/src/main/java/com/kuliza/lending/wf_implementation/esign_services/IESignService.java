package com.kuliza.lending.wf_implementation.esign_services;

import java.util.Map;

import com.kuliza.lending.wf_implementation.pojo.ESignRequest;

public interface IESignService {

	Map<String, Object> initESign(ESignRequest eSignRequest) throws Exception;

	Map<String, Object> prepareESignFiles(ESignRequest eSignRequest) throws Exception;

	Map<String, Object> authorizeESign(ESignRequest eSignRequest) throws Exception;

	Map<String, Object> regenerateESignOTP(ESignRequest eSignRequest) throws Exception;

	Map<String, Object> downLoadESignDoc(ESignRequest eSignRequest) throws Exception;
	
	Map<String, Object>  initESignService(ESignRequest eSignRequest) throws Exception;

	void htmlToPdf(String fileName) throws Exception;

}
