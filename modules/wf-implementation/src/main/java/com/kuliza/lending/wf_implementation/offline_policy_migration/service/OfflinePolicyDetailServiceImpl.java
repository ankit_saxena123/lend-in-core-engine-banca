package com.kuliza.lending.wf_implementation.offline_policy_migration.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.OfflinePolicyDetailsDAO;
import com.kuliza.lending.wf_implementation.dao.RejectedOfflinePoliciesDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.OfflinePolicyDetails;
import com.kuliza.lending.wf_implementation.models.RejectedOfflinePolicies;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.services.PolicyUpdateAutomationService;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.ExcelHelper;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("offlinePolicyDetails")
public class OfflinePolicyDetailServiceImpl implements OfflinePolicyDetailService {

	@Autowired
	OfflinePolicyDetailsDAO offlinePolicyDetailsDAO;

	@Autowired
	private WfImplConfig wfImplConfig;

	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private RejectedOfflinePoliciesDao rejectedOfflinePolicydao;

	@Autowired
	private PolicyUpdateAutomationService uploadDmsService;

	private static final Logger logger = LoggerFactory.getLogger(OfflinePolicyDetailServiceImpl.class);

	public static String IN_BOUND = "/inbound/";

	@Override
	public ApiResponse saveOfflinePolicyDetails() throws Exception {

		File xlsFile = null;
		String ftpPath = wfImplConfig.getLocalDirectory();

		// File xlsFile = new
		// ClassPathResource(wfImplConfig.getLocalDirectory()).getFile();

		List<OfflinePolicyDetails> offlinePolicyDetailsList = new ArrayList<OfflinePolicyDetails>();
		try {
			xlsFile = new File(ftpPath);
			logger.info("xlsFile Path======>>>>>>: " + xlsFile);
			String excelFilePath = xlsFile.getAbsolutePath();
			logger.info("excelFilePath-->" + excelFilePath);
			ExcelHelper excelHelper = new ExcelHelper(excelFilePath);
			offlinePolicyDetailsList = excelHelper.readData(OfflinePolicyDetails.class.getName());
			logger.info("offlinePolicyDetails-->: " + offlinePolicyDetailsList);
			if (xlsFile != null) {
				if (xlsFile.exists()) {
					xlsFile.delete();
				}
			}
		} catch (Exception e) {
			logger.error("error saveOfflinePolicyDetails: " + e);
			e.printStackTrace();
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		}
//		finally {
//			if (xlsFile != null) {
//				if (xlsFile.exists()) {
//					xlsFile.delete();
//				}
//			}
//		}
		return getWorkFlowUserAndStoreIntoDb(offlinePolicyDetailsList);
	}

	private ApiResponse getWorkFlowUserAndStoreIntoDb(List<OfflinePolicyDetails> offlinePolicyDetailsList)
			throws Exception {

		logger.info("getWorkFlowUserAndStoreIntoDb  ======>>>> : " + offlinePolicyDetailsList.size());

		try {
			for (OfflinePolicyDetails policyDetail : offlinePolicyDetailsList) {
				boolean checkNull = checkingNull(policyDetail);
				if (checkNull) {
					logger.info("null check=====>>>>: " + checkNull);

					String policyNo = policyDetail.getPolicyNumber();

					OfflinePolicyDetails policyexist = offlinePolicyDetailsDAO.findByPolicyNumberAndIsDeleted(policyNo,
							false);
					logger.info("policy boolean:  ======>>>> : " + policyexist);

					if (policyexist != null) {
						logger.info("======>>>>>policy  exist ====>>>>>: " + policyexist.getPolicyNumber()
								+ "excel policy number:====>>>>: " + policyNo);
						persistRejectedPolicies(policyDetail.getPolicyNumber(), Constants.POLICY_ALREADY_EXISTS);

					} else {
						logger.info("policy doesnot exist ====>>>>> ");
						WorkFlowUser workFlowUser = workFlowUserDao
								.findByMobileNumberAndIsDeleted(policyDetail.getMobileNumber(), false);

						if (workFlowUser == null) {
							logger.info("workflow user is null======>>>>>>: ");
							boolean Status = getExpiredPolicies(policyDetail.getPolicyExpiryDate(),
									policyDetail.getPolicyPurchaseDate(), policyDetail.getPolicyNextPremiumDueDate());
							boolean mobStatus = policyDetail.getMobileNumber().length() == 9;
							if (Status && mobStatus) {
								persistDataInDb(policyDetail, null);
							} else {
								persistRejectedPolicies(policyDetail.getPolicyNumber(),
										Constants.DATE_MISMATCHED_OR_MOBILE_NUMBER);
							}

						}

						else if (workFlowUser != null && workFlowUser.getNationalId() != null
								&& workFlowUser.getNationalId().equals(policyDetail.getNationalId())) {
							logger.info("inside else wf nid =====>>>>> " + workFlowUser.getIdmUserName());
							boolean Status = getExpiredPolicies(policyDetail.getPolicyExpiryDate(),
									policyDetail.getPolicyPurchaseDate(), policyDetail.getPolicyNextPremiumDueDate());
							boolean mobStatus = policyDetail.getMobileNumber().length() == 9;
							if (Status && mobStatus) {
								persistDataInDb(policyDetail, workFlowUser);
							} else {
								persistRejectedPolicies(policyDetail.getPolicyNumber(),
										Constants.DATE_MISMATCHED_OR_MOBILE_NUMBER);
							}
							// persistDataInDb(policyDetail, workFlowUser);
						} else {

							persistRejectedPolicies(policyDetail.getPolicyNumber(), Constants.NID_NOT_MATCHED);

						}

					}

				} else {
					persistRejectedPolicies(policyDetail.getPolicyNumber(), Constants.USER_DETAILS_NULL);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error in saving data in db" + e);
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);

		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.SUCCESS_MESSAGE);
	}

	public void persistRejectedPolicies(String policyNumber, String message) {
		logger.info("<<<<<<<<========persistRejectedPolicies========>>>>>>>>");
		RejectedOfflinePolicies rejectedPolicie = new RejectedOfflinePolicies();
		rejectedPolicie.setPolicyNumber(policyNumber);
		rejectedPolicie.setResponse(message);
		rejectedOfflinePolicydao.save(rejectedPolicie);
		logger.info("<<<<<<<<========persistRejectedPolicies ended========>>>>>>>>");

	}

	@Transactional
	public void persistDataInDb(OfflinePolicyDetails policyDetail, WorkFlowUser workFlowUser) {
		logger.info("<<<<<<<<========persistDataInDb========>>>>>>>>");
		OfflinePolicyDetails offlinePolicy = new OfflinePolicyDetails();
		offlinePolicy.setInsurerName(policyDetail.getInsurerName());
		offlinePolicy.setPlanId(policyDetail.getPlanId());
		offlinePolicy.setPlanName(policyDetail.getPlanName());
		offlinePolicy.setPolicyNumber(policyDetail.getPolicyNumber());
		offlinePolicy.setProductId(policyDetail.getProductId());
		offlinePolicy.setProductName(policyDetail.getProductName());
		// offlinePolicy.setPolicyDmsDocId(policyDetail.getPolicyDmsDocId());
		offlinePolicy.setNationalId(policyDetail.getNationalId());
		offlinePolicy.setPolicyExpiryDate(policyDetail.getPolicyExpiryDate());
		offlinePolicy.setPolicyNextPremiumDueDate(policyDetail.getPolicyNextPremiumDueDate());
		offlinePolicy.setPolicyMode(policyDetail.getPolicyMode());
		offlinePolicy.setPolicyPurchaseDate(policyDetail.getPolicyPurchaseDate());
		offlinePolicy.setProtectionAmount(policyDetail.getProtectionAmount());
		offlinePolicy.setSourceOfPolicy(policyDetail.getSourceOfPolicy());
		offlinePolicy.setTotalPremiumAmount(policyDetail.getTotalPremiumAmount());
		offlinePolicy.setMobileNumber(policyDetail.getMobileNumber());
		offlinePolicy.setDisplayStatus(false);
		if (workFlowUser != null) {
			offlinePolicy.setIdmUserName(workFlowUser.getIdmUserName());

		} else {
			offlinePolicy.setIdmUserName(null);
		}

		offlinePolicyDetailsDAO.save(offlinePolicy);
		uploadDms(policyDetail.getPolicyNumber());
		logger.info("<<<<<<<<========persistDataInDb ended========>>>>>>>>");
	}

	private void uploadDms(String policyNumber) {
		logger.info("upload Dms doc======>>>>" + policyNumber);
		String dmsId = null;
		OfflinePolicyDetails policyexist = offlinePolicyDetailsDAO.findByPolicyNumberAndIsDeleted(policyNumber, false);
		try {
			if (policyexist != null) {
				dmsId = uploadDmsService.uploadDms(Utils.getStringValue(policyexist.getId()),
						policyNumber.replace("/", "_"));
				logger.info("Dms Doc iD=======>>>>>>: " + dmsId);
				// dmsId="9874";
				policyexist.setPolicyDmsDocId(dmsId);
				offlinePolicyDetailsDAO.save(policyexist);

			}
			if (dmsId.equals(null)) {
				persistRejectedPolicies(policyNumber, Constants.DMS_FAILED_OR_FILE_NOT_FOUND);
			}
		} catch (Exception e) {
			persistRejectedPolicies(policyNumber, Constants.DMS_FAILED_OR_FILE_NOT_FOUND);
			logger.info("error in uploading Dms: " + e);
			e.printStackTrace();
		}

	}

	public boolean checkingNull(OfflinePolicyDetails policyDetail) {
		logger.info("inside checking null====>>>> : " + policyDetail);

		if (policyDetail.getPlanId() == null || policyDetail.getPlanName() == null
				|| policyDetail.getInsurerName() == null || policyDetail.getPolicyNumber() == null
				|| policyDetail.getNationalId() == null || policyDetail.getPolicyExpiryDate() == null
				|| policyDetail.getPolicyNextPremiumDueDate() == null || policyDetail.getPolicyMode() == null
				|| policyDetail.getPolicyPurchaseDate() == null || policyDetail.getProtectionAmount() == null
				|| policyDetail.getSourceOfPolicy() == null || policyDetail.getTotalPremiumAmount() == null
				|| policyDetail.getMobileNumber() == null) {
			return false;
		} else {
			return true;
		}

	}

	public ApiResponse getOfflinePolicyDetails(String name, String mobileNumber) throws Exception {
		List<Map<String, Object>> policyDetailsList = new ArrayList<Map<String, Object>>();
		// Map<String, Object> responseMap = new HashMap<String, Object>();

		try {
			List<OfflinePolicyDetails> offlinePolicyDetailList = offlinePolicyDetailsDAO
					.findByMobileNumberAndIsDeleted(mobileNumber, false);
			logger.info("offlineDetaisList:======>>>>>>: " + offlinePolicyDetailList);
			if (offlinePolicyDetailList != null && !offlinePolicyDetailList.isEmpty()) {
				for (OfflinePolicyDetails offlinePolicyDetail : offlinePolicyDetailList) {
					Map<String, Object> responseMap = new HashMap<String, Object>();
					logger.info("offlinePolicy====>>>>: " + offlinePolicyDetail);
					responseMap.put(WfImplConstants.POLICY_NUMBER, offlinePolicyDetail.getPolicyNumber());
					responseMap.put(WfImplConstants.PRODUCT_ID, offlinePolicyDetail.getProductId());
					responseMap.put(WfImplConstants.PRODUCT_NAME, offlinePolicyDetail.getProductName());
					responseMap.put(WfImplConstants.MONTHLY_PAYMENT, offlinePolicyDetail.getTotalPremiumAmount());
					responseMap.put(WfImplConstants.PLAN_ID, offlinePolicyDetail.getPlanId());
					String purchasedDate = Utils.getDateInString(offlinePolicyDetail.getPolicyPurchaseDate(),
							Constants.DATE_FORMAT_SLASH);
					;
					responseMap.put(WfImplConstants.PURCHASED_DATE, purchasedDate);
					String nextPaymentDate = Utils.getDateInString(offlinePolicyDetail.getPolicyNextPremiumDueDate(),
							Constants.DATE_FORMAT_SLASH);
					responseMap.put(WfImplConstants.NEXT_PAYMENT_DUE, nextPaymentDate);
					responseMap.put(WfImplConstants.PROTECTION_AMOUNT, offlinePolicyDetail.getProtectionAmount());
					responseMap.put(WfImplConstants.INSURER, offlinePolicyDetail.getInsurerName());
					String expiringDate = Utils.getDateInString(offlinePolicyDetail.getPolicyExpiryDate(),
							Constants.DATE_FORMAT_SLASH);
					responseMap.put(WfImplConstants.EXPIRING_DATE, expiringDate);
					responseMap.put(WfImplConstants.PLAN_NAME, offlinePolicyDetail.getPlanName());
					responseMap.put(WfImplConstants.PLAN_NAME_VI,
							Utils.translateEngToViatnam(offlinePolicyDetail.getPlanName()));
					responseMap.put(Constants.POLICY_DETAILS, constructPolicyDetails(offlinePolicyDetail));
					responseMap.put(WfImplConstants.INSURER_PDF_DOC_ID, offlinePolicyDetail.getPolicyDmsDocId());
					responseMap.put(Constants.SOURCE_OF_POLICY, offlinePolicyDetail.getSourceOfPolicy());

					responseMap.put(Constants.POLICY_MODE, offlinePolicyDetail.getPolicyMode());
					responseMap.put(WfImplConstants.PRODUCT_NAME_VI,
							Constants.PRODUCT_NAME_VI.get(offlinePolicyDetail.getProductName()));

					boolean productStatus = getProductAndPlanId(offlinePolicyDetail.getProductId(), offlinePolicyDetail.getPlanId());
					responseMap.put(Constants.PRODUCT_STATUS, productStatus);
					policyDetailsList.add(responseMap);
					changeViewedStatus(offlinePolicyDetail.getPolicyNumber());

					logger.info("====== purchasedPoliciesMap==============>>>>>>> " + responseMap);
				}
			} else {
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.USER_DOES_NOT_EXIST_MESSAGE);
			}
		} catch (Exception e) {
			logger.info("error in saveOfflinePolicyDetails : " + e);
			e.printStackTrace();
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, policyDetailsList);
	}

	public List<Map<String, Object>> constructPolicyDetails(OfflinePolicyDetails offlinePolicyDetail) {

		List<Map<String, Object>> policyDetailsList = new ArrayList<Map<String, Object>>();
		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put(WfImplConstants.POLICY_NUMBER, offlinePolicyDetail.getPolicyNumber());
		responseMap.put(WfImplConstants.PLAN_ID, offlinePolicyDetail.getPlanId());
		responseMap.put(WfImplConstants.MONTHLY_PAYMENT, offlinePolicyDetail.getTotalPremiumAmount());
		responseMap.put(WfImplConstants.PROTECTION_AMOUNT, offlinePolicyDetail.getProtectionAmount());
		responseMap.put(WfImplConstants.PLAN_NAME, offlinePolicyDetail.getPlanName());
		responseMap.put(WfImplConstants.PLAN_NAME_VI, Utils.translateEngToViatnam(offlinePolicyDetail.getPlanName()));
		responseMap.put(WfImplConstants.INSURER_PDF_DOC_ID, offlinePolicyDetail.getPolicyDmsDocId());
		responseMap.put(WfImplConstants.PRODUCT_NAME_VI,
				Constants.PRODUCT_NAME_VI.get(offlinePolicyDetail.getProductName()));
		logger.info("====== purchasedPoliciesMap==============>>>>>>> " + responseMap);
		policyDetailsList.add(responseMap);
		return policyDetailsList;
	}

	public void changeViewedStatus(String policyNo) {
		logger.info("inside status=========>>>>>>>>>>");
		OfflinePolicyDetails findByPolicy = offlinePolicyDetailsDAO.findByPolicyNumberAndIsDeleted(policyNo, false);
		if (findByPolicy != null)
			findByPolicy.setDisplayStatus(true);
		offlinePolicyDetailsDAO.save(findByPolicy);

	}

	public boolean getExpiredPolicies(Date planexpiaryDate, Date planPurchasedDate, Date nextPaymentdate)
			throws Exception {
		boolean status = false;
		logger.info("getExpiredPolicies:  " + planexpiaryDate);
		String currentDate = Utils.getCurrentDateInFormat(Constants.DATE_FORMAT_SLASH);

		try {
			Date currentdateParsed = Utils.parseDate(currentDate, Constants.DATE_FORMAT_SLASH);

			if ((currentdateParsed.after(planPurchasedDate) || currentdateParsed.equals(planPurchasedDate))
					&& (!planexpiaryDate.equals(planPurchasedDate) && planexpiaryDate.after(planPurchasedDate))
					&& (!nextPaymentdate.equals(planPurchasedDate) && nextPaymentdate.after(planPurchasedDate))) {
				status = true;

			}

		} catch (Exception e) {
			logger.error("error in getExpiredPolicies " + e);
			e.printStackTrace();
		}
		return status;

	}

	private Boolean getProductAndPlanId(String productId, String plan_id) {
		String concatPlanAndProductId = (new StringBuilder()).append(productId).append("-").append(plan_id).toString();
		logger.info("concatPlanAndProductId ---> " + concatPlanAndProductId);
		String status=WfImplConstants.CMS_ACTION_CODE_MAP.get(concatPlanAndProductId);
		if(status!=null) {
			return true;
		}
		else 
			return false;
	}

}