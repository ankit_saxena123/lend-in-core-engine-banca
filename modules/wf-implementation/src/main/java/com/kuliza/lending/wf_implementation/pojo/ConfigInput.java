package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class ConfigInput {

	@NotNull
	String name;
	
	String value;
	
	public ConfigInput() {
		super();
	}
	
	public ConfigInput(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}