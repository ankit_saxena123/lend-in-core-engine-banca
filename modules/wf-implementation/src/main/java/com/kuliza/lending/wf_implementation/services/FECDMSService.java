package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.soap.AttachmentPart;

import omnidocsapi.SysRequestType;
import omnidocsapi.SysResponseType;
import omnidocsapi.UploadDocResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.DownloadDocClient;
import com.kuliza.lending.wf_implementation.integrations.GenerateApplicationIdIntegration;
import com.kuliza.lending.wf_implementation.integrations.UploadDocClient;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service
public class FECDMSService {

	@Autowired
	DownloadDocClient downloadDocClient;

	@Autowired
	UploadDocClient uploadDocClient;
	@Autowired
	private WorkflowUserDao workflowUserDao;
	
	@Autowired
	private GenerateApplicationIdIntegration generateApplicationIdIntegration;

	private static final Logger logger = LoggerFactory
			.getLogger(FECDMSService.class);

	public AttachmentPart downloadDocument(String docId, String volumeId,
			boolean toPdf) throws Exception {
		SysRequestType sys = new SysRequestType();
		sys.setRequestorID(Constants.DMS_REQUESTOR_ID);

		sys.setDateTime(Utils.getCurrentDateTimeInGregorianCalendar());
		sys.setTransID(Utils.generateUUIDTransanctionId());
		CustomLogger.log(
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				null, null, JobType.OUTBOUND_API, null, null,
				"downloadDocument request: docId: " + docId + " ,volumeId: "
						+ volumeId + ", toPdf: " + toPdf);
		return downloadDocClient.getDownloadedDoc(docId, volumeId, toPdf, sys);
	}

	public UploadDocResponse uploadDocument(String userName,String docId, String appId, String moduleId, boolean renameFile,
			MultipartFile file) throws Exception {
		logger.info("<=============> uploadDocument FECDMSService <============>");
		SysRequestType sys = new SysRequestType();
		sys.setRequestorID(Constants.DMS_REQUESTOR_ID);
		sys.setDateTime(Utils.getCurrentDateTimeInGregorianCalendar());
		sys.setTransID(Utils.generateUUIDTransanctionId());
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if(workFlowUser!=null){
			String wFAppId=workFlowUser.getAppName();
			if(wFAppId!=null && !wFAppId.isEmpty()){
				logger.info("<=============> appId is present in workflowuser <============>");
				appId=wFAppId;
			}
			else{
				appId=getApplicationId(workFlowUser, appId);
			}
		}
		logger.info("<=============> appId : "+appId+"<============>");
		UploadDocResponse docResponse =uploadDocClient.uploadDoc(docId, appId, moduleId, renameFile, sys, file);
		return docResponse;
	}

	private String getApplicationId(WorkFlowUser workFlowUser, String appId) {
		logger.info("<=============> getApplicationId");
		String wfappId=null;
		try{
		JSONObject getRequestPayloadForBancaBlackList = generateApplicationIdIntegration.buildRequestPayload();
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = generateApplicationIdIntegration
				.getDataFromIb(getRequestPayloadForBancaBlackList);
		JSONObject parsedResponseForApplicationGenerationId = getResponseFromIntegrationBroker.getBody()
				.getObject();
		Map<String, Object> parseResponseMap = generateApplicationIdIntegration.parseResponse(parsedResponseForApplicationGenerationId);
		if(parseResponseMap.containsKey("ApplicationID")){
			wfappId=Utils.getStringValue(parseResponseMap.get("ApplicationID"));
			logger.info("<=============> wfappId :"+wfappId);
			workFlowUser.setAppName(wfappId);
			workflowUserDao.save(workFlowUser);
		}
		else{
			wfappId=appId;
		}
		}
		catch(Exception e ){
			logger.error("<==========>Exception while getting APPID : "+e.getMessage());
			wfappId=appId;
		}
		return wfappId;
	}

	public UploadDocResponse uploadDocument(String docId, String appId,
			String moduleId, boolean renameFile, File file) throws Exception {
		SysRequestType sys = new SysRequestType();
		sys.setRequestorID(Constants.DMS_REQUESTOR_ID);
		sys.setDateTime(Utils.getCurrentDateTimeInGregorianCalendar());
		sys.setTransID(Utils.generateUUIDTransanctionId());
		UploadDocResponse docResponse = uploadDocClient.uploadDoc(docId, appId,
				moduleId, renameFile, sys, file);
		return docResponse;
	}

	/**
	 * gets doc from the dms using docId and saves a temp file locally
	 * 
	 * @param docId
	 * @param volumeId
	 * @param toPdf
	 * @return
	 * @throws Exception
	 */
	public File downloadDocumentAsFile(String docId, String volumeId,
			boolean toPdf) throws Exception {
		AttachmentPart att = downloadDocument(docId, volumeId, toPdf);
		InputStream inputStream = att.getRawContent();
		String fileName = att.getContentId();
		if (fileName.startsWith("<") && fileName.endsWith(">")) {
			fileName = att.getContentId().substring(1,
					att.getContentId().length() - 1);
		}
		Calendar calendar = Calendar.getInstance();
		File file = new File("/tmp/" + calendar.getTimeInMillis() + "-"
				+ fileName);
		OutputStream outputStream = new FileOutputStream(file);
		IOUtils.copy(inputStream, outputStream);
		outputStream.close();
		return file;

	}

	public String getDMSDocIdFromResponse(UploadDocResponse docResponse)
			throws Exception {
		JAXBElement<String> imageIndex = docResponse.getImageIndex();
		String documentId = imageIndex.getValue();
		documentId = documentId.split("#")[0];
		return documentId;
	}

	/**
	 * downloads the pdf by the url and uploads pdf to dms
	 * 
	 * @param policyPdf
	 * @return String
	 * @throws Exception
	 */
	public String uploadPDftoDMS(String appId,String policyPdfUrl) throws Exception {
		String docId = null;
		File file = null;
		try {
			URL url1 = new URL(policyPdfUrl);
			byte[] byteArray = new byte[1024];
			int byteLength;
			String fileName = Utils.generateUUIDTransanctionId();
			file = new File(System.getProperty("java.io.tmpdir")
					+ System.getProperty("file.separator") + fileName + ".pdf");
			FileOutputStream fos1 = new FileOutputStream(file);
			// Contacting the URL
			logger.info(",<<<<<<<<>>>>>Connecting to " + url1.toString()
					+ " ... ");
			URLConnection urlConn = url1.openConnection();
			
			logger.info("policyPdfUrl : "+policyPdfUrl);
			logger.info("content type :"+urlConn.getContentType());
			
			// Read the PDF from the URL and save to a local file
			InputStream is1 = url1.openStream();
			while ((byteLength = is1.read(byteArray)) != -1) {
				fos1.write(byteArray, 0, byteLength);
			}
			fos1.flush();
			fos1.close();
			UploadDocResponse uploadDocument = uploadDocument(Constants.INSURER_CERTIFICATE_DMS_ID, appId, "101", true,
					file);
			is1.close();
			SysResponseType sys = uploadDocument.getSys();
			int code = sys.getCode();

			if (code == 1) {
				docId = getDMSDocIdFromResponse(uploadDocument);
				logger.info(">>>>>>>>>>>>>dmsDocId for Insurer PDF :" + docId);
			} else {
				logger.error(">>>>>>>>>>>Failed to upload pdf to DMS<<<<<<<<<");
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Filed to upload Doc to DMS :" + e.getMessage(), e);
			throw e;
		} finally {
			if (file != null) {
				if (file.exists()) {
					file.delete();
				}
			}
		}
		return docId;
	}

}
