package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.pojo.WfUserPaymentDetail;
import com.kuliza.lending.wf_implementation.pojo.WfUserUpdatePaymentDetail;
import com.kuliza.lending.wf_implementation.services.WfUserPaymentDetailService;

@RestController
@RequestMapping("/wf-payment")
public class WfUserPaymentDetailController {

	@Autowired
	private WfUserPaymentDetailService wfUserPaymentDetailService;

	@RequestMapping(method = RequestMethod.POST, value = "/addPaymentDetails")
	public ApiResponse addPaymentDetail(Principal principal,
			@Valid @RequestBody WfUserPaymentDetail wfUserPaymentDetail) {
		return wfUserPaymentDetailService.addPaymentDetails(principal, wfUserPaymentDetail);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/getPaymentDetails")
	public ApiResponse getPaymentDetail(Principal principal) {
		return wfUserPaymentDetailService.getPaymentDetails(principal);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updatePaymentDetails")
	public ApiResponse updatePaymentDetail(@Valid @RequestBody WfUserUpdatePaymentDetail wfUserUpdatePaymentDetail) {
		return wfUserPaymentDetailService.updatePaymentDetails(wfUserUpdatePaymentDetail);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/deletePaymentDetails")
	public ApiResponse deletePaymentDetail(@Valid @RequestParam String uUidTransactionId ) {
		return wfUserPaymentDetailService.deletePaymentDetails(uUidTransactionId);

	}
}
