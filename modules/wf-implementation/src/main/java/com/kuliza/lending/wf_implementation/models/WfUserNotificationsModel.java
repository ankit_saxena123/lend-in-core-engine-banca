package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "workflow_user_notifications")
public class WfUserNotificationsModel extends BaseModel {

	@Lob
	@Column(nullable = false, columnDefinition = "TEXT")
	private String title;

	@Lob
	@Column(nullable = false, columnDefinition = "TEXT")
	private String message;

	@Column(nullable = false)
	private String category;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wf_user_id", nullable = false)
	private WorkFlowUser workFlowUser;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public WorkFlowUser getWorkFlowUser() {
		return workFlowUser;
	}

	public void setWorkFlowUser(WorkFlowUser workFlowUser) {
		this.workFlowUser = workFlowUser;
	}

}
