package com.kuliza.lending.wf_implementation.models;

public class PremiumCalculateRequest {
	private Integer age;
	
	private String productType;
	
	private String gender;
	
	private String planId;
	
	private String premiumType;

	private Integer additionalAmount;
	
	private Integer protectionAmount;

	public Integer getProtectionAmount() {
		return protectionAmount;
	}

	public void setProtectionAmount(Integer protectionAmount) {
		this.protectionAmount = protectionAmount;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Integer getAdditionalAmount() {
		return additionalAmount;
	}

	public void setAdditionalAmount(Integer additionalAmount) {
		this.additionalAmount = additionalAmount;
	}
	

}
