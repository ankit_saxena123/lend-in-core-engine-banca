/**
 * 
 */
package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.ClaimDocModel;
import com.kuliza.lending.wf_implementation.models.ClaimInfoModel;

/**
 * @author garun
 *
 */

@Repository
public interface PortalClaimDocsDao extends CrudRepository<ClaimDocModel, Long>,JpaRepository<ClaimDocModel, Long> {
	
	public List<ClaimDocModel> findAllByIsDeleted(Pageable paging, boolean isDeleted);
	public List<ClaimDocModel> findAllByIsDeleted( boolean isDeleted);
	public List<ClaimDocModel> findByProcessInstanceId(String processInstanceId);
	public ClaimDocModel findByDocID(String docId);
	public ClaimDocModel findByProcessInstanceIdAndDocID(String processInstanceId, String document_number);
}
