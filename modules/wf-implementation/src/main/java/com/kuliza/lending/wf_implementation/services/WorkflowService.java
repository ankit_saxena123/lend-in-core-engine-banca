package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.api.FormService;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import com.kuliza.lending.authorization.exception.UserNotFoundException;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.service.GenericServices;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.BackToJourneyInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.utils.HelperFunctions;
import com.kuliza.lending.journey.utils.Validation;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowSourceDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserVariablesDao;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowSource;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.models.WorkflowUserVariables;
import com.kuliza.lending.wf_implementation.pojo.WorkflowInitiateRequest;
import com.kuliza.lending.wf_implementation.pojo.WorkflowSourceForm;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("WorkflowService")
public class WorkflowService extends GenericServices {

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private CmmnRuntimeService cmmnRuntimeService;

	@Autowired
	private FormService formService;

	@Autowired
	private WorkflowSourceDao workflowSourceDao;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	private WorkflowUserVariablesDao workflowUserVariablesDao;

	@Autowired
	private WorkflowUserDao workflowuserDao;

	@Autowired
	private WorkFlowAddressDao workFlowAddressDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDao;

	private static final Logger logger = LoggerFactory
			.getLogger(WorkflowService.class);

	/**
	 * This functions takes Parent/Root Process Instance Id and returns List of
	 * All Process Instance Ids of Running Child Executions
	 *
	 * @return List of String Process Instance Ids
	 * 
	 * @author Arpit Agrawal
	 */
	public List<String> allProcessInstances(String parentProcInst) {
		List<Execution> allRunningExecutions = runtimeService
				.createExecutionQuery().rootProcessInstanceId(parentProcInst)
				.list();
		List<String> allProcessInstanceIds = new ArrayList<>();
		for (Execution singleExecution : allRunningExecutions) {
			allProcessInstanceIds.add(singleExecution.getProcessInstanceId());
		}
		allProcessInstanceIds.add(parentProcInst);
		return allProcessInstanceIds;
	}

	/**
	 * This service returns the current task data be it in the sub-process or
	 * main process
	 * 
	 * @param username
	 * @param parentProcInst
	 * @param errorcode
	 * @param errorMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public ApiResponse getTaskData(String username, String parentProcInst,
			HttpStatus errorcode, String errorMessage) throws Exception {
		ApiResponse response;
		List<String> allProcessInstanceIds = allProcessInstances(parentProcInst);

		List<Task> allTasks = taskService.createTaskQuery()
				.processInstanceIdIn(allProcessInstanceIds)
				.taskAssignee(username).active().list();
		if (allTasks.size() > 1) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST,
					Constants.TOO_MANY_TASKS);
		} else {
			if (allTasks.size() != 1) {
				response = new ApiResponse(errorcode, errorMessage);
			} else {
				Task task = allTasks.get(0);
				Map<String, Object> processVariables = runtimeService
						.getVariables(task.getProcessInstanceId());
				Map<String, Object> data = new HashMap<>();

				Map<String, Object> rootProcessVariables = runtimeService
						.getVariables(parentProcInst);
				data.put(Constants.LOAN_APPLICATION_ID_KEY,
						rootProcessVariables.get(Constants.APPLICATION_ID_KEY));
				data.put(Constants.APPLICATION_NUMBER,
						rootProcessVariables.get(Constants.APPLICATION_NUMBER));

				SimpleFormModel formModel = getSimpleFormModel(allTasks.get(0),
						processVariables);
				Map<String, FormField> formFields = formModel.allFieldsAsMap();
				response = HelperFunctions.makeResponse(HttpStatus.OK,
						Constants.SUCCESS_MESSAGE, parentProcInst, task, data,
						formFields);
			}
		}
		return response;
	}

	/**
	 * This service either starts a new process or resume existing processes.
	 * 
	 * @param username
	 * @param workflowInitiateRequest
	 * @return
	 */
	public ApiResponse startOrResumeProcess(String username,
			WorkflowInitiateRequest workflowInitiateRequest) {
		ApiResponse response;
		ProcessInstance processInstance = null;
		try {
			if (repositoryService
					.createDeploymentQuery()
					.processDefinitionKey(
							workflowInitiateRequest.getWorkflowName()).count() < 1) {
				// no process deployed with given journey name
				response = new ApiResponse(HttpStatus.BAD_REQUEST,
						Constants.FAILURE_MESSAGE,
						Constants.NO_PROCESS_DEPLOYED_FOR_GIVEN_KEY_MESSAGE);
				return response;
			}

			WorkflowSourceForm wfSource = workflowInitiateRequest
					.getWorkflowSource();
			String parentProcInstId = null;
			logger.info("username :" + username + " journey Name :"
					+ workflowInitiateRequest.getWorkflowName());
			List<WorkFlowUserApplication> workflowUserApps = workflowUserApplicationDao
					.findByUserIdentifierAndParentProcInstIdAndProcessNameAndIsDeleted(
							username, parentProcInstId,
							workflowInitiateRequest.getWorkflowName(), false);
			logger.info("workflowUserApps :" + workflowUserApps);
			// check for whether wants to create a new Journey
			if ((workflowInitiateRequest.getIsNew() != null && workflowInitiateRequest
					.getIsNew())) {
				logger.info("creating the new journey");
				Map<String, Object> processVariables = null;
				if (workflowInitiateRequest.getProcessVariables() != null) {
					processVariables = workflowInitiateRequest
							.getProcessVariables();
				} else {
					processVariables = new HashMap<String, Object>();
				}
				logger.info("processVariables are :" + processVariables);
				// check user has journey already
				if (workflowUserApps != null && !workflowUserApps.isEmpty()) {
					/*
					 * if (workflowInitiateRequest.getWorkflowName().equals(
					 * WfImplConstants.FEC_SINGLE_HEALTH_INSURANCEFINAL) || workflowInitiateRequest
					 * .getWorkflowName() .equals(WfImplConstants.FEC_BANCA_TWOWHEELER)) { // check
					 * for dulipcate journey for the same policy and // delete if duplicate
					 * checkForDuplicateJourneyAndDelete(workflowUserApps, processVariables);
					 * 
					 * }
					 */
					checkForDuplicateJourneyAndDelete(workflowUserApps, processVariables);

				}
				if (wfSource != null && wfSource.getType() != null
						&& wfSource.getType().equals(SourceType.bpmn)) {

					WorkFlowApplication wfAppObj = new WorkFlowApplication();
					/*if (workflowInitiateRequest.getWorkflowName().equals(
							WfImplConstants.FEC_SINGLE_HEALTH_INSURANCEFINAL)
							|| workflowInitiateRequest
									.getWorkflowName()
									.equals(WfImplConstants.FEC_BANCA_TWOWHEELER)) {*/
						updateWorkFlowApp(processVariables, username, wfAppObj);
						addWorkFlowPolicyDetails(processVariables, wfAppObj);

					//}

					wfAppObj = workflowApplicationDao.save(wfAppObj);

					WorkFlowUserApplication wfUserAppObj = new WorkFlowUserApplication();
					wfUserAppObj.setWfApplication(wfAppObj);
					wfUserAppObj.setUserIdentifier(username);
					wfUserAppObj.setProcessName(workflowInitiateRequest
							.getWorkflowName());
					wfUserAppObj = workflowUserApplicationDao
							.save(wfUserAppObj);

				/*	if (workflowInitiateRequest.getWorkflowName().equals(
							WfImplConstants.FEC_SINGLE_HEALTH_INSURANCEFINAL)
							|| workflowInitiateRequest
									.getWorkflowName()
									.equals(WfImplConstants.FEC_BANCA_TWOWHEELER)) {*/
						addJourneyVariables(processVariables, username);
					//}
					if (wfSource != null
							&& wfSource.getCopyFromSource() != null
							&& wfSource.getCopyFromSource()) {
						processVariables.putAll(runtimeService
								.getVariables(wfSource.getId()));
					}

					processVariables.put(Constants.PROCESS_TASKS_ASSIGNEE_KEY,
							username);
					processVariables.put(Constants.APPLICATION_ID_KEY,
							wfUserAppObj.getId());
					processVariables.put(Constants.APPLICATION_NUMBER,
							wfAppObj.getId());
					Authentication.setAuthenticatedUserId(username);
					processInstance = runtimeService.startProcessInstanceByKey(
							workflowInitiateRequest.getWorkflowName(),
							CommonHelperFunctions.getStringValue(wfUserAppObj
									.getId()), processVariables);
					wfUserAppObj.setParentProcInstId(parentProcInstId);
					wfUserAppObj.setProcInstId(processInstance.getId());
					wfUserAppObj = workflowUserApplicationDao
							.save(wfUserAppObj);
					registerOrGetSource(wfSource, wfUserAppObj);
					// storing process_instance_id in workflowApplicationDao
					wfAppObj.setProcessInstanceId(processInstance.getId());
					workflowApplicationDao.save(wfAppObj);
					response = getTaskData(
							username,
							(wfUserAppObj.getParentProcInstId() != null ? wfUserAppObj
									.getParentProcInstId() : wfUserAppObj
									.getProcInstId()), HttpStatus.BAD_REQUEST,
							Constants.NO_TASKS_MESSAGE);
				} else {
					response = new ApiResponse(HttpStatus.BAD_REQUEST,
							Constants.FAILURE_MESSAGE, "type is not provided");
				}

			} else {
				logger.info(" initiating already existing journey ");
				if (wfSource != null && wfSource.getId() != null) {
					WorkFlowUserApplication workflowUserApp = workflowUserApplicationDao
							.findByUserIdentifierAndProcInstIdAndIsDeleted(
									username, wfSource.getId(), false);
					if (workflowUserApp != null) {
						logger.info("process instance id is : "
								+ workflowUserApp.getProcInstId());
						response = getTaskData(username,
								workflowUserApp.getProcInstId(),
								HttpStatus.BAD_REQUEST,
								Constants.NO_TASKS_MESSAGE);
					} else {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								Constants.FAILURE_MESSAGE,
								Constants.NO_TASKS_MESSAGE);
					}

				} else {
					response = new ApiResponse(HttpStatus.BAD_REQUEST,
							Constants.FAILURE_MESSAGE,
							Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				}

			}

		} catch (Exception e) {
			logger.error(
					"exception while intiating the journey :" + e.getMessage(),
					e);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.SOMETHING_WRONG_MESSAGE, e.getMessage());
		}
		return response;
	}

	private void addWorkFlowPolicyDetails(Map<String, Object> processVariables,
			WorkFlowApplication wfAppObj) {
		String planId = (String) processVariables
				.get(WfImplConstants.JOURNEY_PLAN_ID);
		String planName = (String) processVariables
				.get(WfImplConstants.JOURNEY_PLAN_TYPE);
		String monthlyPayment = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
		String protectionAmount = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));

		String[] planIdArray = planId.split(",");
		String[] planNameArray = planName.split(",");
		for (int i = 0; i < planIdArray.length; i++) {
			WorkFlowPolicy workFlowPolicy = new WorkFlowPolicy();
			workFlowPolicy.setWorkFlowApplication(wfAppObj);
			workFlowPolicy.setPlanId(planIdArray[i]);
			workFlowPolicy.setPlanName(planNameArray[i]);
			workFlowPolicy.setMontlyPayment(monthlyPayment);
			workFlowPolicy.setProtectionAmount(protectionAmount);
			workFlowPolicy.setTransactionId(Utils.generateUUIDTransanctionId());
			workFlowPolicyDao.save(workFlowPolicy);

		}

	}

	private void checkForDuplicateJourneyAndDelete(
			List<WorkFlowUserApplication> workflowUserApps,
			Map<String, Object> processVariables) throws Exception {
		logger.info("checkForDuplicateJourneyAndDelete method");
		String categoryId = (String) processVariables
				.get(WfImplConstants.JOURNEY_CATEGORY_ID);
		String productId = (String) processVariables
				.get(WfImplConstants.JOURNEY_PRODUCT_ID);
		String planId = (String) processVariables
				.get(WfImplConstants.JOURNEY_PLAN_ID);
		String insurerName = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_INSURER_NAME));
		logger.info("categoryId : " + categoryId + " productId " + productId
				+ " planId " + planId+" InsurerName :"+insurerName);
		for (WorkFlowUserApplication workflowUserApp : workflowUserApps) {
			WorkFlowApplication wfApplication = workflowUserApp
					.getWfApplication();
			if (wfApplication != null) {
				String wfCategoryId = wfApplication.getCategoryId();
				String wfProductId = wfApplication.getProductId();
				String journeyStatus = wfApplication.getCurrentJourneyStage();
				String wfInsurerName=wfApplication.getInsurer();
				//One user can buy only one SH policy from the one Insurer.
				/*if(productId.equals(WfImplConstants.JOURNEY_SH_PRODUCT_ID) && productId.equals(wfProductId) && insurerName.equalsIgnoreCase(wfInsurerName) && journeyStatus!=null && journeyStatus
						.equals(WfImplConstants.APP_STATUS_COMPLETED)){
					logger.info("<=======> user already purchased the policy <===========>");
					throw new Exception("user already purchased the policy");
				}*/
				if (!(journeyStatus != null && (journeyStatus
						.equals(WfImplConstants.APP_STATUS_COMPLETED) || journeyStatus
						.equals(WfImplConstants.APP_STATUS_REJECTED)))) {
					if (categoryId != null && productId != null
							&& wfCategoryId != null && wfProductId != null) {
						if (categoryId.equals(wfCategoryId)
								&& productId.equals(wfProductId)) {
							wfApplication.setIsDeleted(true);
							workflowUserApp.setIsDeleted(true);
							try {
								runtimeService.deleteProcessInstance(wfApplication.getProcessInstanceId(), null);
							}
							catch(Exception e) {
									logger.error("journey already completed");
							}
							workflowUserApplicationDao.save(workflowUserApp);
							logger.info("journey " + wfApplication.getId()
									+ " is deleted");
							List<WorkFlowPolicy> workFlowPolicyList = workFlowPolicyDao
									.findByWorkFlowApplicationAndIsDeleted(
											wfApplication, false);
							if (workFlowPolicyList != null) {
								for (WorkFlowPolicy workFlowPolicy : workFlowPolicyList) {
									workFlowPolicy.setIsDeleted(true);
									logger.info("workFlowPolicy " + workFlowPolicy.getId()
											+ " is deleted");
								}
							}

						}

					}
				}
			}
		}

	}

	private void updateWorkFlowApp(Map<String, Object> processVariables,
			String username, WorkFlowApplication wfAppObj) {
		logger.info("---->inside updateWorkFlowApp");
		String categoryId = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_CATEGORY_ID));
		String productId = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_PRODUCT_ID));
		String planId = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_PLAN_ID));
		String productName = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_PRODUCT_NAME));
		String planName = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_PLAN_TYPE));
			planName=getPlanName(planName);
		String monthlyPayment = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
		String protectionAmount = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
		String insurerName = Utils.getStringValue(processVariables
				.get(WfImplConstants.JOURNEY_INSURER_NAME));
		if(insurerName.isEmpty())
			insurerName=WfImplConstants.JOURNEY_INSURER;
		wfAppObj.setCategoryId(categoryId);
		wfAppObj.setPlanId(planId);
		wfAppObj.setProductId(productId);
		wfAppObj.setProductName(Utils.translateViatnamToEng(productName));
		wfAppObj.setPlanName(Utils.translateViatnamToEng(planName));
		wfAppObj.setMontlyPayment(monthlyPayment);
		wfAppObj.setProtectionAmount(protectionAmount);
		wfAppObj.setInsurer(insurerName);
		logger.info("categoryId " + categoryId + " productId " + productId
				+ " planId " + planId + " productName " + productName
				+ " planName " + planName + " monthlyPayment " + monthlyPayment
				+ " protectionAmount " + protectionAmount);
		logger.info("workflow_app updated");

	}

	private String getPlanName(String planName) {
		logger.info("<====planName======>:"+planName);
		if(planName.contains(",")){
			String[] split = planName.split(",");
			if(split.length>0){
				return split[0];
			}
		}
		return planName;
	
	}

	private void addJourneyVariables(Map<String, Object> processVariables,
			String userName) {

		WorkFlowUser workflowUser = workflowuserDao
				.findByIdmUserNameAndIsDeleted(userName, false);
		if (workflowUser != null) {
			String workflowUserName = workflowUser.getUsername();
			String gender = workflowUser.getGender();
			String dob = workflowUser.getDob();
			String nationalId = workflowUser.getNationalId();
			String mobileNumber = workflowUser.getMobileNumber();
			String applicantEmailAddress = Utils.getStringValue(processVariables.getOrDefault(WfImplConstants.APPLICANT_EMAIL_USER, ""));
			
			processVariables.put(WfImplConstants.APPLICATION_MOBILE_NUMBER,
					mobileNumber);
			processVariables.put(WfImplConstants.USER_FULL_NAME,
					workflowUserName);
			processVariables.put(WfImplConstants.USER_DOB, dob);
			processVariables.put(WfImplConstants.USER_GENDER, gender);
			processVariables.put(WfImplConstants.APPLICANT_EMAIL, applicantEmailAddress);
			
			if (nationalId != null) {
				processVariables.put(WfImplConstants.IS_NID, true);
				processVariables.put(WfImplConstants.NATIONAL_ID_HYPERVERGE,
						workflowUser.getNationalId());
				processVariables.put(WfImplConstants.GENDER_HYPERVERGE,
						workflowUser.getNid_gender());
				processVariables.put(WfImplConstants.FULLNAME_HYPERVERGE,
						workflowUser.getNid_user_name());
				processVariables.put(WfImplConstants.NATIONALITY_HYPERVERGE,
						workflowUser.getNationality());
				processVariables.put(WfImplConstants.DOB_HYPERVERGE,
						workflowUser.getDob());
				processVariables.put(WfImplConstants.APPLICANT_EMAIL_USER,
						applicantEmailAddress);
				List<WorkFlowUserAddress> wfAddressList = workFlowAddressDao
						.findByWfUser(workflowUser);

				if (wfAddressList != null && !wfAddressList.isEmpty()) {
					for (WorkFlowUserAddress workFlowUserAddress : wfAddressList) {
						String addressType = workFlowUserAddress
								.getAddressType();
						String addressLine1 = workFlowUserAddress
								.getAddressLine1() != null ? workFlowUserAddress
								.getAddressLine1() : "";
						String cityDist = workFlowUserAddress.getCityDistrict() != null ? workFlowUserAddress
								.getCityDistrict() : "";
						String province = workFlowUserAddress.getProvince();

						if (addressType
								.equals(com.kuliza.lending.wf_implementation.utils.Constants.NID_ADDRESS_KEY)) {
							processVariables.put(
									WfImplConstants.ADDRESS_HYPERVERGE,
									addressLine1);
							processVariables.put(
									WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
									province);
						} else if (addressType
								.equals(com.kuliza.lending.wf_implementation.utils.Constants.DELIVERY_ADDRESS_KEY)) {
							processVariables.put(
									WfImplConstants.DELIVERY_ADDRESS_LINE,
									addressLine1);
							processVariables.put(WfImplConstants.DELIVERY_CITY,
									cityDist);
							processVariables
									.put(WfImplConstants.DELIVERY_PROVINCE,
											province);
						}
					}

				}
			} else {
				processVariables.put(WfImplConstants.IS_NID, false);
			}
		} else {
			throw new UserNotFoundException("user not found");
		}

	}

	/**
	 * This function registers or return source from which process is initiated
	 * 
	 * @param workflowSource
	 * @param wfUserApp
	 * @return
	 */
	private WorkFlowSource registerOrGetSource(
			WorkflowSourceForm workflowSource, WorkFlowUserApplication wfUserApp) {
		WorkFlowSource workflowSourceObj = null;
		if (workflowSource != null) {
			workflowSourceObj = workflowSourceDao.findByWfUserAppAndIsDeleted(
					wfUserApp, false);
			if (workflowSourceObj != null) {
				workflowSourceObj.setIsDeleted(true);
				workflowSourceDao.save(workflowSourceObj);
			} else {
				workflowSourceObj = new WorkFlowSource(workflowSource.getId(),
						workflowSource.getType(), workflowSource.getName(),
						wfUserApp);
				workflowSourceObj = workflowSourceDao.save(workflowSourceObj);
			}
		}
		return workflowSourceObj;
	}

	/**
	 * This service returns current state of the process
	 * 
	 * @param username
	 * @param parentProcInstId
	 * @return
	 */
	public ApiResponse getFormData(String username, String parentProcInstId) {
		ApiResponse response;
		try {
			if (parentProcInstId == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST,
						Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			} else {
				response = getTaskData(username, parentProcInstId,
						HttpStatus.BAD_REQUEST,
						Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			}
		} catch (Exception e) {
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	/**
	 * This service submits the proc variables and add user level variables in
	 * WorkflowUserVariables Model
	 * 
	 * @param username
	 * @param input
	 * @return
	 */
	public ApiResponse submitFormData(String username, SubmitFormClass input) {
		ApiResponse response;
		try {

			Task task = taskService.createTaskQuery().taskAssignee(username)
					.active().taskId(input.getTaskId()).singleResult();
			if (task == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST,
						Constants.INVALID_TASK_ID_MESSAGE);
			} else {
				WorkFlowUserApplication wfUserApp = workflowUserApplicationDao
						.findByUserIdentifierAndProcInstIdAndIsDeleted(
								username, task.getProcessInstanceId(), false);
				if (wfUserApp != null) {
					if (wfUserApp.getParentProcInstId() != null
							&& !wfUserApp.getParentProcInstId().equals(
									input.getProcessInstanceId())) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
					} else {
						SimpleFormModel formModel = getSimpleFormModel(task,
								null);
						List<FormField> listOfFormProp = formModel.getFields();
						Map<String, Object> formPropertiesMap = (!input
								.getFormProperties().isEmpty() ? input
								.getFormProperties()
								: new HashMap<String, Object>());
						Map<Object, Object> validationResult = Validation
								.submitValidation(listOfFormProp,
										formPropertiesMap, true);
						if (!(boolean) validationResult
								.get(WfImplConstants.VALIDATION_STATUS_KEY)) {
							response = new ApiResponse(HttpStatus.BAD_REQUEST,
									validationResult.get(Constants.MESSAGE_KEY)
											.toString());
						} else {
							addWorkflowUserVariables(username,
									formPropertiesMap, task.getId(),
									task.getProcessInstanceId());
							runtimeService.setVariables(
									task.getProcessInstanceId(),
									formPropertiesMap);
							taskService.complete(input.getTaskId());
							response = getTaskData(username,
									input.getProcessInstanceId(),
									HttpStatus.OK, Constants.SUCCESS_MESSAGE);
						}
					}
				} else {
					response = new ApiResponse(HttpStatus.BAD_REQUEST,
							Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;

	}

	/**
	 * This service fetched the user variables from input and save it in
	 * WorkflowUserVariables
	 * 
	 * @param username
	 * @param formPropertiesMap
	 * @param taskId
	 * @param procInstId
	 */
	private void addWorkflowUserVariables(String username,
			Map<String, Object> formPropertiesMap, String taskId,
			String procInstId) {
		Set<String> keys = new HashSet<String>();
		keys.addAll(formPropertiesMap.keySet());
		keys.retainAll(WfImplConstants.workflowUserVariablesList);
		if (keys.size() > 0) {
			for (String key : keys) {
				WorkflowUserVariables variableObj = workflowUserVariablesDao
						.findByUsernameAndNameAndIsDeleted(username, key, false);
				if (variableObj != null) {
					variableObj.setIsDeleted(true);
					workflowUserVariablesDao.save(variableObj);
				}
				variableObj = new WorkflowUserVariables();
				variableObj.setName(key);
				variableObj.setUsername(username);
				variableObj.setValueText(CommonHelperFunctions
						.getStringValue(formPropertiesMap.get(key)));
				variableObj.setLastProcInstId(procInstId);
				variableObj.setLastTaskId(taskId);
				workflowUserVariablesDao.save(variableObj);
			}
		}
	}

	/**
	 * This service is used for back functionality and takesthe execution to a
	 * previous state
	 * 
	 * @param username
	 * @param input
	 * @param result
	 * @return
	 */
	public ApiResponse changeProcessState(String username, BackTaskInput input,
			BindingResult result) {
		ApiResponse response;
		try {
			response = checkErrors(result);
			if (response == null) {
				Task task = taskService.createTaskQuery()
						.taskId(input.getCurrentTaskId())
						.taskAssignee(username).active().singleResult();
				if (task == null) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST,
							Constants.INVALID_TASK_ID_MESSAGE);
				} else {
					List<HistoricTaskInstance> taskToGoTo = historyService
							.createHistoricTaskInstanceQuery()
							.processInstanceId(task.getProcessInstanceId())
							.taskDefinitionKey(input.getBackTaskDefinitionKey())
							.list();
					if (!taskToGoTo.isEmpty()) {
						runtimeService
								.createChangeActivityStateBuilder()
								.processInstanceId(task.getProcessInstanceId())
								.moveActivityIdTo(task.getTaskDefinitionKey(),
										input.getBackTaskDefinitionKey())
								.changeState();
						response = getTaskData(username,
								input.getProcessInstanceId(),
								HttpStatus.BAD_REQUEST,
								Constants.INVALID_TASK_ID_MESSAGE);
					} else {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								Constants.INVALID_BACK_TASK_DEFINITION_KEY);
					}
				}
			}
		} catch (Exception e) {
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	// /**
	// * This functions copies all variables of root process to sub processes
	// and vice
	// * versa.
	// *
	// * @param execution
	// * @param copyToRoot
	// * @return Execution
	// *
	// * @author Kunal Arneja
	// */
	// public Execution copyVariablesFromToRootProcess(Execution execution,
	// boolean copyToRoot) {
	// String processInstanceId = execution.getProcessInstanceId();
	// String rootProcessInstanceId = execution.getRootProcessInstanceId();
	// if (processInstanceId != null && rootProcessInstanceId != null) {
	// if (copyToRoot) {
	// runtimeService.setVariables(rootProcessInstanceId,
	// runtimeService.getVariables(processInstanceId));
	// } else {
	// runtimeService.setVariables(processInstanceId,
	// runtimeService.getVariables(rootProcessInstanceId));
	// }
	// }
	// return execution;
	// }

	// /**
	// * @param businessKey:
	// * applicationId for the user
	// * @param journeyType:
	// * Process Definition Key
	// *
	// * @return processInstanceId
	// *
	// * The function takes processDefinitionKey and businessKey as input and
	// * returns processInstanceId
	// *
	// **/
	// public String getProcessInstanceId(String businessKey, String
	// journeyType) {
	// String processInstanceId = "";
	// List<ProcessInstance> processInstanceList =
	// runtimeService.createProcessInstanceQuery()
	// .processInstanceBusinessKey(businessKey, journeyType).desc().list();
	// if(!processInstanceList.isEmpty()) {
	// ProcessInstance processInstance = processInstanceList.get(0);
	// processInstanceId = processInstance.getProcessInstanceId();
	// }
	// return processInstanceId;
	// }

	/**
	 * Function updates the processVariables for the given businessKey from the
	 * POJO BackToJourneyInput
	 * 
	 * @param backToJourneyInput
	 * 
	 * @return ApiResponse
	 * 
	 **/
	public ApiResponse setUserData(BackToJourneyInput backToJourneyInput) {
		String processInstanceId = backToJourneyInput.getProcessInstanceId();
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST,
				Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
		if (!processInstanceId.isEmpty()) {
			runtimeService.setVariables(processInstanceId,
					backToJourneyInput.getVariablesToSave());
			if (backToJourneyInput.isCloseTask()) {
				for (String taskKey : backToJourneyInput.getTaskList()) {
					if (taskKey != null && !taskKey.isEmpty()) {
						try {
							Task task = taskService.createTaskQuery()
									.processInstanceId(processInstanceId)
									.active().taskDefinitionKey(taskKey.trim())
									.singleResult();
							if (task != null) {
								taskService.complete(task.getId());
							}
						} catch (Exception exception) {
							exception.printStackTrace();
						}
					}
				}
			}
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		}
		return response;

	}

	public SimpleFormModel getSimpleFormModel(String key,
			Map<String, Object> processVariables) {
		return ((SimpleFormModel) formService.getFormModelWithVariablesByKey(
				key, "", processVariables).getFormModel());
	}

	public SimpleFormModel getSimpleFormModel(Task task,
			Map<String, Object> processVariables) {
		SimpleFormModel simpleFormModel = null;
		if (task.getFormKey() != null && !task.getFormKey().isEmpty()) {
			simpleFormModel = getSimpleFormModel(task.getFormKey(),
					processVariables);
		} else {
			simpleFormModel = (SimpleFormModel) taskService.getTaskFormModel(
					task.getId()).getFormModel();
		}
		return simpleFormModel;
	}

	/**
	 * This service Changes assignee and make an entry into WorkflowuserApp
	 * Model
	 * 
	 * @param procInstId
	 * @param assignee
	 * 
	 * @return Object
	 */
	public Object changeAssignee(String procInstId, String assignee) {
		try {
			String currentAssignee = CommonHelperFunctions
					.getStringValue(runtimeService.getVariable(procInstId,
							Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			WorkFlowUserApplication wfUserApp = workflowUserApplicationDao
					.findByUserIdentifierAndProcInstIdAndIsDeleted(
							currentAssignee, procInstId, false);
			if (wfUserApp != null) {
				WorkFlowUserApplication wfUserAppNew = null;
				wfUserAppNew = workflowUserApplicationDao
						.findByUserIdentifierAndProcInstIdAndIsDeleted(
								assignee, procInstId, false);
				if (wfUserAppNew == null) {
					wfUserAppNew = new WorkFlowUserApplication();
					wfUserAppNew.setProcessName(wfUserApp.getProcessName());
					wfUserAppNew.setProcInstId(wfUserApp.getProcInstId());
					wfUserAppNew.setUserIdentifier(assignee);
					wfUserAppNew.setWfApplication(wfUserApp.getWfApplication());
					workflowUserApplicationDao.save(wfUserAppNew);
				}
				runtimeService.setVariable(procInstId,
						Constants.PROCESS_TASKS_ASSIGNEE_KEY, assignee);
				return new ApiResponse(HttpStatus.OK,
						Constants.SUCCESS_MESSAGE,
						WfImplConstants.ASSIGNEE_CHANGE_SUCCESS_MESSAGE);
			} else {
				return new InterruptedException();
			}
		} catch (Exception e) {
			return new InterruptedException(e.getMessage());
		}
	}

	/**
	 * This process saves variables to source, parent and soft deletes
	 * WorkfloUserApp and its children
	 * 
	 * @param procInstId
	 * @param copyToSource
	 * @param copyToParent
	 * @param notClose
	 * 
	 * @return Object
	 */
	@SuppressWarnings("unchecked")
	public Object endProcess(String procInstId, Boolean copyToSource,
			Boolean copyToParent, Boolean notClose) {
		try {
			String currentAssignee = CommonHelperFunctions
					.getStringValue(runtimeService.getVariable(procInstId,
							Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			WorkFlowUserApplication wfUserApp = workflowUserApplicationDao
					.findByUserIdentifierAndProcInstIdAndIsDeleted(
							currentAssignee, procInstId, false);
			if (wfUserApp != null) {
				if (copyToParent != null && copyToParent) {
					if (wfUserApp.getParentProcInstId() != null
							&& !wfUserApp.getParentProcInstId().equals("")) {
						Map<String, Map<String, Object>> childDataList = (Map<String, Map<String, Object>>) runtimeService
								.getVariable(
										wfUserApp.getParentProcInstId(),
										wfUserApp.getProcessName()
												+ WfImplConstants.WORKFLOW_MAP_SUFFIX);
						if (childDataList == null) {
							childDataList = new HashMap<String, Map<String, Object>>();
						}
						childDataList.put(wfUserApp.getUserIdentifier(),
								runtimeService.getVariables(procInstId));
						runtimeService.setVariable(
								wfUserApp.getParentProcInstId(),
								wfUserApp.getProcessName()
										+ WfImplConstants.WORKFLOW_MAP_SUFFIX,
								childDataList);
					}
				}
				if (copyToSource != null && copyToSource) {
					WorkFlowSource wfSource = workflowSourceDao
							.findByWfUserAppAndIsDeleted(wfUserApp, false);

					if (wfSource != null
							&& wfSource.getSourceType().equals(SourceType.bpmn)) {
						Map<String, Map<String, Object>> childDataList = (Map<String, Map<String, Object>>) runtimeService
								.getVariable(
										wfSource.getSourceId(),
										wfUserApp.getProcessName()
												+ WfImplConstants.WORKFLOW_MAP_SUFFIX);
						if (childDataList == null) {
							childDataList = new HashMap<String, Map<String, Object>>();
						}
						childDataList.put(wfUserApp.getUserIdentifier(),
								runtimeService.getVariables(procInstId));
						runtimeService.setVariable(wfSource.getSourceId(),
								wfUserApp.getProcessName()
										+ WfImplConstants.WORKFLOW_MAP_SUFFIX,
								childDataList);
					} else if (wfSource != null
							&& wfSource.getSourceType().equals(SourceType.cmmn)) {
						Map<String, Map<String, Object>> childDataList = (Map<String, Map<String, Object>>) cmmnRuntimeService
								.getVariable(
										wfSource.getSourceId(),
										wfUserApp.getProcessName()
												+ WfImplConstants.WORKFLOW_MAP_SUFFIX);
						if (childDataList == null) {
							childDataList = new HashMap<String, Map<String, Object>>();
						}
						childDataList.put(wfUserApp.getUserIdentifier(),
								runtimeService.getVariables(procInstId));
						cmmnRuntimeService.setVariable(wfSource.getSourceId(),
								wfUserApp.getProcessName()
										+ WfImplConstants.WORKFLOW_MAP_SUFFIX,
								childDataList);
					}

				}
				if (notClose != null && !notClose) {
					List<WorkFlowUserApplication> wfUserAppChilds = workflowUserApplicationDao
							.findByUserIdentifierAndParentProcInstIdAndIsDeleted(
									currentAssignee, procInstId, false);
					for (WorkFlowUserApplication wfUserAppChild : wfUserAppChilds) {
						wfUserAppChild.setIsDeleted(true);
						workflowUserApplicationDao.save(wfUserAppChild);
					}
					wfUserApp.setIsDeleted(true);
					workflowUserApplicationDao.save(wfUserApp);
				}
				return new ApiResponse(HttpStatus.OK,
						Constants.SUCCESS_MESSAGE,
						WfImplConstants.END_PROCESS_SUCCESS_MESSAGE);
			} else {
				return new InterruptedException();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new InterruptedException(e.getMessage());
		}
	}

	/**
	 * This Service has to be the first service of called activity and makes
	 * entry into WorkflowUserApp model
	 * 
	 * @param exec
	 * @return
	 */
	public Object createChild(Execution exec) {
		try {
			String rootProcInstId = exec.getRootProcessInstanceId();
			if (rootProcInstId != null && !rootProcInstId.equals("")) {
				String rootAssignee = CommonHelperFunctions
						.getStringValue(runtimeService.getVariable(
								rootProcInstId,
								Constants.PROCESS_TASKS_ASSIGNEE_KEY));
				WorkFlowUserApplication rootWfUserApp = workflowUserApplicationDao
						.findByUserIdentifierAndProcInstIdAndIsDeleted(
								rootAssignee, rootProcInstId, false);
				if (rootWfUserApp != null) {
					WorkFlowUserApplication wfUserAppNew = null;
					String currentAssignee = CommonHelperFunctions
							.getStringValue(runtimeService.getVariable(
									exec.getProcessInstanceId(),
									Constants.PROCESS_TASKS_ASSIGNEE_KEY));
					wfUserAppNew = workflowUserApplicationDao
							.findByUserIdentifierAndProcInstIdAndIsDeleted(
									currentAssignee,
									exec.getProcessInstanceId(), false);
					if (wfUserAppNew == null) {
						wfUserAppNew = new WorkFlowUserApplication();
						wfUserAppNew.setProcInstId(exec.getProcessInstanceId());
						wfUserAppNew.setProcessName(rootWfUserApp
								.getProcessName());
						wfUserAppNew.setUserIdentifier(currentAssignee);
						wfUserAppNew.setWfApplication(rootWfUserApp
								.getWfApplication());
						wfUserAppNew.setParentProcInstId(rootProcInstId);
						workflowUserApplicationDao.save(wfUserAppNew);
					}
					return exec;
				} else {
					return new InterruptedException(
							WfImplConstants.NO_WF_APP_ERROR_MESSAGE);
				}
			} else {
				return new InterruptedException(
						WfImplConstants.NO_ROOT_PROC_ID_ERROR_MESSAGE);
			}
		} catch (Exception e) {
			return new InterruptedException(e.getMessage());
		}
	}

	public ApiResponse uploadAll(MultipartFile submissions, String fileType,
			String procInstId) {
		ApiResponse response = null;
		try {
			Map<String, Object> data = new HashMap<>();
			Calendar calendar = Calendar.getInstance();
			File file = new File("/tmp/" + calendar.getTimeInMillis() + "-"
					+ submissions.getOriginalFilename());
			OutputStream outputStream = new FileOutputStream(file);
			IOUtils.copy(submissions.getInputStream(), outputStream);
			outputStream.close();
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpPost httpPost = null;
			MultipartEntityBuilder builder = null;
			CloseableHttpResponse closeableHttpResponse = null;
			HttpEntity multipart = null;
			ProcessInstance instance = runtimeService
					.createProcessInstanceQuery().processInstanceId(procInstId)
					.active().singleResult();
			if (instance == null)
				response = new ApiResponse(Constants.FAILURE_CODE,
						Constants.FAILURE_MESSAGE,
						"Invalid Process Instance Id.");
			else {
				if (fileType.equals("nid-front") || fileType.equals("nid-back")) {
					httpClient = HttpClients.createDefault();
					httpPost = new HttpPost(
							WfImplConstants.HYPERVERGE_READ_NID_URL);
					httpPost.setHeader("appid",
							WfImplConstants.HYPERVERGE_APP_KEY);
					httpPost.setHeader("appkey",
							WfImplConstants.HYPERVERGE_APP_SECRET);
					builder = MultipartEntityBuilder.create();
					builder.addPart("image", new FileBody(file));
					multipart = builder.build();
					httpPost.setEntity(multipart);
					closeableHttpResponse = httpClient.execute(httpPost);
					if (closeableHttpResponse.getStatusLine().getStatusCode() == HttpStatus.OK
							.value()) {
						String responseString = EntityUtils
								.toString(closeableHttpResponse.getEntity());
						System.out.println(responseString.toString());
						int frontBackStatus = Utils
								.checkFrontBack(responseString);
						if (fileType
								.equals(WfImplConstants.UPLOAD_FILE_TYPE_NATIONAL_ID_FRONT_KEY)
								&& frontBackStatus == 1) {
							Map<String, Object> scrapedData = Utils
									.parseNationalIdFrontData(responseString);
							runtimeService
									.setVariables(procInstId, scrapedData);
							data.putAll(scrapedData);
						} else if (fileType
								.equals(WfImplConstants.UPLOAD_FILE_TYPE_NATIONAL_ID_FRONT_KEY)
								&& (frontBackStatus == 0 || frontBackStatus == -1)) {
							data.put(Constants.DATA_ERRORS_KEY, 1);
						} else if (fileType
								.equals(WfImplConstants.UPLOAD_FILE_TYPE_NATIONAL_ID_BACK_KEY)
								&& frontBackStatus == 0) {
							Map<String, Object> scrapedData = Utils
									.parseNationalIdBackData(responseString);
							data.putAll(scrapedData);
							runtimeService
									.setVariables(procInstId, scrapedData);
						} else if (fileType
								.equals(WfImplConstants.UPLOAD_FILE_TYPE_NATIONAL_ID_BACK_KEY)
								&& (frontBackStatus == 1 || frontBackStatus == -1)) {
							data.put(Constants.DATA_ERRORS_KEY, 1);
						} else {
							data.put(Constants.DATA_ERRORS_KEY, 1);
						}
						response = new ApiResponse(Constants.SUCCESS_CODE,
								Constants.SUCCESS_MESSAGE, data);
					} else {
						response = new ApiResponse(Constants.FAILURE_CODE,
								Constants.FAILURE_MESSAGE,
								"Error From Hyperverge");
					}
				} else {
					response = new ApiResponse(Constants.FAILURE_CODE,
							Constants.FAILURE_MESSAGE, "Invalid File Type.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}
}