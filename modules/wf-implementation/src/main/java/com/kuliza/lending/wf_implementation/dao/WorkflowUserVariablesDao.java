package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.models.WorkflowUserVariables;

@Repository
public interface WorkflowUserVariablesDao extends CrudRepository<WorkflowUserVariables, Long> {

	public WorkflowUserVariables findById(long id);

	public WorkflowUserVariables findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkflowUserVariables findByUsernameAndNameAndIsDeleted(String username, String key, boolean isDeleted);
}
