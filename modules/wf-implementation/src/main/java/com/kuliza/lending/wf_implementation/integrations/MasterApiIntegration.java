package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

@Service
public class MasterApiIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(MasterApiIntegration.class);
	
	public JSONObject getFilterJsonObject(String key, String value)
			throws JSONException {
		JSONObject filterJson = new JSONObject();
		filterJson.put(Constants.KEY, key);
		filterJson.put(Constants.VALUE, value);
		return filterJson;
	}

	public JSONObject buildRequestPayload(JSONArray data) throws JSONException {
		JSONObject requestPayload = new JSONObject();
		requestPayload.put(Constants.KEYLIST, data);
		logger.info("<===========> RequestPayLoad <===========>" + requestPayload);
		return requestPayload;
	}

	public JSONObject getDataFromService(JSONObject requestPayload, String slug) throws Exception {
		
		logger.info("slug : "+slug+", Request : "+requestPayload);
		Map<String, String> requestHeadersMap = new HashMap<String, String>();
		requestHeadersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
		HttpResponse<String> masterResponse = null;
		JSONObject masterJsonObject = null;
		String masterGenerationUrl = wfImplConfig.getMasterEndpoint() + slug;
		logger.info(" masterGenerationUrl : " + masterGenerationUrl);
		try {
			masterResponse = Unirest.post(masterGenerationUrl).headers(requestHeadersMap).body(requestPayload)
					.asString();
			masterJsonObject = new JSONObject(masterResponse.getBody());
//			logger.info("<===========> masterJsonObject <===========>" + masterJsonObject);
		} catch (Exception e) {
			logger.error("error while hit master ::" + e.getMessage(),e);
			throw e;
		}
		return masterJsonObject;
	}
	
	public JSONObject getDataFromService(String slug) throws Exception {
		logger.info("-->Entering MasterApiIntegration.getDataFromService()");
		Map<String, String> requestHeadersMap = new HashMap<String, String>();
		requestHeadersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
		HttpResponse<String> masterResponse = null;
		JSONObject masterJsonObject = null;
		String masterGenerationUrl = wfImplConfig.getMasterEndpoint() + slug;
		logger.info("<===========> masterGenerationUrl <===========> " + masterGenerationUrl);
		try {
			masterResponse = Unirest.get(masterGenerationUrl).headers(requestHeadersMap)
					.asString();
			masterJsonObject = new JSONObject(masterResponse.getBody());
			logger.info("<===========> masterJsonObject <===========>" + masterJsonObject);
		} catch (Exception e) {
			logger.error("ErrorMessage ::" + e.getMessage());
			throw e;
		}
		logger.info("<-- Exiting MasterApiIntegration.getDataFromService()");
		return masterJsonObject;
	}

	public JSONArray parseResponseFromMaster(JSONObject masterFinalJsonResponse) throws JSONException {
		JSONArray masterFinalJsonArrayResponse = new JSONArray();
		if (masterFinalJsonResponse.has(Constants.JSON_ARRAY_DATA_KEY)) {
			masterFinalJsonArrayResponse = masterFinalJsonResponse.getJSONArray(Constants.JSON_ARRAY_DATA_KEY);
		}
		return masterFinalJsonArrayResponse;
	}
}
