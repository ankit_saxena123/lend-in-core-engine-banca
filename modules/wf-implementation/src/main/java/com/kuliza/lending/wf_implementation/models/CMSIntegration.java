package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "cms_integration_conform_model")
public class CMSIntegration extends BaseModel {

	@Column(nullable = true, name = "tranx_id", unique = true)
	private String transactionId;

	@Column(nullable = true, name = "ref_num")
	private String refNum;

	@Column(nullable = true, name = "acc_no")
	private String accNo;

	@Column(nullable = true, name = "api_response", columnDefinition = "LONGTEXT")
	private String apiResponse;

	@Column(nullable = true, name = "tenor")
	private String tenor;

	@Column(nullable = true, name = "premium_amount")
	private String premiumAmount;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable = false, name = "payment_confirm_status")
	private boolean paymentConfirmStatus = false;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable = false, name = "insurer_status")
	private boolean insurerStatus = false;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false, name = "wf_application_id")
	private WorkFlowApplication wfApplication;

	public CMSIntegration() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CMSIntegration(String transactionId, String refNum, String accNo, String apiResponse, String tenor,
			String premiumAmount, boolean paymentConfirmStatus, WorkFlowApplication wfApplication) {
		super();
		this.transactionId = transactionId;
		this.refNum = refNum;
		this.accNo = accNo;
		this.apiResponse = apiResponse;
		this.tenor = tenor;
		this.premiumAmount = premiumAmount;
		this.paymentConfirmStatus = paymentConfirmStatus;
		this.wfApplication = wfApplication;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getApiResponse() {
		return apiResponse;
	}

	public void setApiResponse(String apiResponse) {
		this.apiResponse = apiResponse;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public boolean isPaymentConfirmStatus() {
		return paymentConfirmStatus;
	}

	public void setPaymentConfirmStatus(boolean paymentConfirmStatus) {
		this.paymentConfirmStatus = paymentConfirmStatus;
	}

	public WorkFlowApplication getWfApplication() {
		return wfApplication;
	}

	public void setWfApplication(WorkFlowApplication wfApplication) {
		this.wfApplication = wfApplication;
	}

	public boolean isInsurerStatus() {
		return insurerStatus;
	}

	public void setInsurerStatus(boolean insurerStatus) {
		this.insurerStatus = insurerStatus;
	}
	
	

}
