package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name="mobile_number_update_history")
public class MobileNumberUpdateHistory extends BaseModel{
	
	@Column(name="old_number")
	private String oldNumber;
	
	@Column(name="new_number")
	private String newNumber;
	
	@ManyToOne
	@JoinColumn(name="wf_user_id")
	private WorkFlowUser workFlowUser;

	public String getOldNumber() {
		return oldNumber;
	}

	public void setOldNumber(String oldNumber) {
		this.oldNumber = oldNumber;
	}

	public String getNewNumber() {
		return newNumber;
	}

	public void setNewNumber(String newNumber) {
		this.newNumber = newNumber;
	}

	public WorkFlowUser getWorkFlowUser() {
		return workFlowUser;
	}

	public void setWorkFlowUser(WorkFlowUser workFlowUser) {
		this.workFlowUser = workFlowUser;
	}

}
