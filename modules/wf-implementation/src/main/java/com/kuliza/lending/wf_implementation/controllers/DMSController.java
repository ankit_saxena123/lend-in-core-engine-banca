package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.FileDownloadInput;
import com.kuliza.lending.wf_implementation.pojo.FileUploadForm;
import com.kuliza.lending.wf_implementation.services.DMSService;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;


@RestController
@RequestMapping(WfImplConstants.DMS_API_ENDPOINT)
public class DMSController {

	@Autowired
	public DMSService dmsService;

	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.UPLOAD_API_ENDPOINT)
	public ResponseEntity<Object> upload(Principal principal, @Valid FileUploadForm uploadForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(dmsService.upload(principal.getName(), uploadForm));
		} catch (Exception e) {
			return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage()));
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.DOWNLOAD_API_ENDPOINT)
	public ResponseEntity<Object> download(Principal principal, @RequestBody @Valid FileDownloadInput fileDownloadInput) {
		try {
			return new ResponseEntity<>(dmsService.download(principal.getName(), fileDownloadInput), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = WfImplConstants.DOWNLOAD_BY_PORTAL_API_ENDPOINT)
	public ResponseEntity<Object> downloadByPortal(Principal principal, FileDownloadInput fileDownloadInput) {
		try {
			return new ResponseEntity<>(dmsService.downloadFile(principal.getName(), fileDownloadInput), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

}
