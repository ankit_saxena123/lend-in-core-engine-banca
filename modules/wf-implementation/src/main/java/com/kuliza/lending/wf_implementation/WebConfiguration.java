package com.kuliza.lending.wf_implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

import com.kuliza.lending.wf_implementation.integrations.DownloadDocClient;
import com.kuliza.lending.wf_implementation.integrations.FileDownloadInterceptor;
import com.kuliza.lending.wf_implementation.integrations.UploadDocClient;

@Configuration
public class WebConfiguration {

@Value("${omnidocs.URL}")	
private String url;

@Value("${omnidocs.password}")	
private String password;

@Value("${omnidocs.userName}")	
private String userName;

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("omnidocsapi");
		return marshaller;
	}

	@Bean
	public DownloadDocClient downloadDocClient(Jaxb2Marshaller marshaller) {
		DownloadDocClient client = new DownloadDocClient();
		client.setDefaultUri(url);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		client.setInterceptors(new ClientInterceptor[] { new PreemptiveAuth(userName, password), new FileDownloadInterceptor() });
		return client;
	}

	@Bean
	public UploadDocClient uploadDocClient(Jaxb2Marshaller marshaller) {
		UploadDocClient client = new UploadDocClient();
		client.setDefaultUri(url);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		client.setInterceptors(new ClientInterceptor[] { new PreemptiveAuth(userName, password)});
		return client;
	}

	

}
