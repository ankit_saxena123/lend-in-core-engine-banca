package com.kuliza.lending.wf_implementation.utils;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public class WfImplConstants

{

	private WfImplConstants() {
		throw new UnsupportedOperationException();
	}

	// Workflow Service Constants
	public static final String WORFKFLOW_USERNAME_CONSTANT = "workflowuser@%s.com";
	public static final String WORKFLOW_MAP_SUFFIX = "Map";
	public static final String END_PROCESS_SUCCESS_MESSAGE = "Ended  Process Successfully.";
	public static final String NO_WF_APP_ERROR_MESSAGE = "No Root Workflow App for given Execution";
	public static final String NO_ROOT_PROC_ID_ERROR_MESSAGE = "No Root Proc Id Found";
	public static final String ASSIGNEE_CHANGE_SUCCESS_MESSAGE = "Assignee Changed Successfully.";
	public static final String VALIDATION_STATUS_KEY = "status";

	public static final String WORKFLOW_API_ENDPOINT = "/workflow";
	public static final String WORKFLOW_INITIATE_API_ENDPOINT = "/initiate";
	public static final String WORKFLOW_INITIATE_BY_ASSIGNEE_API_ENDPOINT = "/initiate-by-assignee";
	public static final String WORKFLOW_SUBMIT_FORM_API_ENDPOINT = "/submit-form";
	public static final String WORKFLOW_BACK_API_ENDPOINT = "/back";
	public static final String WORKFLOW_BACK_TO_JOURNEY_API_ENDPOINT = "/back-to-journey";
	public static final String WORKFLOW_UPLOAD_NID_ENDPOINT = "/upload-nid";

	public static final String HYPERVERGE_READ_NID_URL = "http://apac.docs.hyperverge.co/v1/readNID";
	public static final String HYPERVERGE_APP_KEY = "04193a";
	public static final String HYPERVERGE_APP_SECRET = "b9ebc0fa8373ae5c3e72";

	// Add to this list if process variables needed to be added to user
	public static final Set<String> workflowUserVariablesList;
	static {
		workflowUserVariablesList = new HashSet<String>();
		workflowUserVariablesList.add("addressLine1");
		workflowUserVariablesList.add("addressLine2");
		workflowUserVariablesList.add("city");
		workflowUserVariablesList.add("district");
		workflowUserVariablesList.add("state");
		workflowUserVariablesList.add("country");
		workflowUserVariablesList.add("employmentStatus");
		workflowUserVariablesList.add("companyName");
		workflowUserVariablesList.add("companyType");
		workflowUserVariablesList.add("designation");
	}

	// Validation Service Constants
	public static final String VALIDATION_API_ENDPOINT = "/validate";
	public static final String GENERATE_PASSWORD_API_ENDPOINT = "/generate-password";
	public static final String VALIDATE_USER_API_ENDPOINT = "/validate-user";
	public static final String SET_MPIN_API_ENDPOINT = "/set-mpin";
	public static final String VALIDATE_MPIN_API_ENDPOINT = "/validate-mpin";
	public static final String VALIDATE_OTP = "/validate-otp";
	public static final String USER_NOT_FOUND = "User with this token not found.";
	public static final String USER_DEVICE_NOT_REGISTERED = "User Mpin Not Set.";
	public static final String MPIN_SET_SUCCESS_MESSAGE = "MPIN Set Successfully.";
	public static final String MPIN_VALIDATION_SUCCESS_MESSAGE = "MPIN Validation Successfull.";
	public static final String INVALID_MPIN_ERROR_MESSAGE = "Invalid MPIN.";
	public static final String MPIN_SET_FLAG_RESPONSE_KEY = "isMpinSet";

	// DMS Constants
	public static final String DMS_API_ENDPOINT = "/document";
	public static final String UPLOAD_API_ENDPOINT = "/upload";
	public static final String DOWNLOAD_API_ENDPOINT = "/download";
	public static final String DOWNLOAD_BY_PORTAL_API_ENDPOINT = "/portal-download";
	public static final String DMS_UPLOAD_RESPONSE_KEY = "response";
	public static final String DMS_UPLOAD_RESPONSE_DOC_ID_KEY = "id";
	public static final String DMS_UPLOAD_RESPONSE_DOC_LABEL_KEY = "label";

	// Feature Flags
	public static final boolean DEVICE_INDEPENDENT_MPIN_FLAG = true;
	public static final boolean DISABLE_OTP_VALIDATION_FLAG = false;

	public static final String FE_CREDIT_ANDROID_LATEST_VERSION_KEY = "androidLatestVersion";
	public static final String FE_CREDIT_IOS_LATEST_VERSION_KEY = "iosLatestVersion";
	public static final String FE_CREDIT_ANDROID_LEAST_SUPPORTED_VERSION_KEY = "androidLeastSupportedVersion";
	public static final String FE_CREDIT_IOS_LEAST_SUPPORTED_VERSION_KEY = "iosLeastSupportedVersion";

	// nid upload Constants
	public static final String UPLOAD_FILE_TYPE_NATIONAL_ID_FRONT_KEY = "nid-front";
	public static final String UPLOAD_FILE_TYPE_NATIONAL_ID_BACK_KEY = "nid-back";

	// Hyperverge Values constants
	public static final String HYPERVERGE_DATA_COMPLETE_STATUS_FLAG = "hypervergeDataCompleteStatus";
	public static final String HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG = "hypervergeDataFrontCompleteStatus";
	public static final String HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG = "hypervergeDataBackCompleteStatus";
	public static final String PLACE_OF_ISSUE_OF_NATIONAL_ID_FROM_HYPERVERGE_KEY = "placeOfIssueFromHyperverge";
	public static final String PLACE_OF_ISSUE_OF_NATIONAL_ID_KEY = "placeOfIssue";
	public static final String DATE_OF_EXPIRY_OF_NATIONAL_ID_FROM_HYPERVERGE_KEY = "dateOfExpiry";
	public static final String NATIONAL_ID_FROM_HYPERVERGE_KEY = "nationalIdFromHyperverge";
	public static final String FRAUD_CHECK_IS_BLACK_WHITE_FROM_HYPERVERGE_KEY = "fraudCheckIsBlackWhiteFromHyperverge";
	public static final String FRAUD_CHECK_PROVINCE_MISMATCH_FROM_HYPERVERGE_KEY = "fraudCheckProvinceMismatchFromHyperverge";
	public static final String BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY = "borrowerFullNameFromHyperverge";
	public static final String NAME_CONFIDENCE_SCORE_FROM_HYPERVERGE_KEY = "nameConfidenceScoreFromHyperverge";
	public static final String NAME_TO_BE_REVIEWED_FROM_HYPERVERGE_KEY = "nameToBeReviewedFromHyperverge";
	public static final String DATE_OF_ISSUE_FROM_HYPERVERGE_KEY = "dateOfIssuefromHyperverge";
	public static final String DATE_OF_ISSUE_KEY = "dateOfIssue";
	public static final String ADDRESS_FROM_HYPERVERGE_KEY = "addressFromHyperverge";
	public static final String GENDER_FROM_HYPERVERGE_KEY = "genderFromHyperverge";
	public static final String DATE_OF_BIRTH_FROM_HYPERVERGE_KEY = "dateOfBirthFromHyperverge";
	public static final String PROVINCE_FROM_HYPERVERGE_KEY = "provinceFromHyperverge";
	public static final String PROVINCE_CODE_FROM_HYPERVERGE_CITY = "provinceCodeFromHypervergeCity";

	// hyperverge response constants

	public static final String HYPERVERGE_ID_NEW_BACK = "id_new_back";
	public static final String HYPERVERGE_ID_BACK = "id_back";
	public static final String HYPERVERGE_ID_NEW_FRONT = "id_new_front";
	public static final String HYPERVERGE_ID_FRONT = "id_front";
	public static final String HYPERVERGE_TYPE = "type";
	public static final String HYPERVERGE_SUCCESS = "success";
	public static final String HYEPRVERGE_STATUS_CODE = "statusCode";
	public static final String HYPERVERGE_STATUS = "status";
	public static final String HYPERVERGE_GENDER = "gender";
	public static final String HYPERVERGE_ADDRESS = "address";
	public static final String HYPERVERGE_PROVINCE = "province";
	public static final String HYPERVERGE_TO_BE_REVIEWED = "to-be-reviewed";
	public static final String HYPERVERGE_CONF = "conf";
	public static final String HYPERVERGE_NAME = "name";
	public static final String HYPERVERGE_ID = "id";
	public static final String HYPERVERGE_VALUE = "value";
	public static final String HYPERVERGE_DOB = "dob";
	public static final String HYPERVERGE_DETAILS = "details";
	public static final String HYPERVERGE_RESULT = "result";
	public static final String HYPERVERGE_REQUEST_IMAGE = "image";
	public static final String HYPERVERGE_KEY = "appkey";
	public static final String HYPERVERGE_APPID = "appid";

	// initiate journey

	public static final String IS_NID = "is_NID";
	public static final String USER_GENDER = "user_gender";
	public static final String USER_DOB = "user_dob";
	public static final String USER_FULL_NAME = "user_fullName";
	public static final String FEC_SINGLE_HEALTH_INSURANCEFINAL = "fecBancaSingleHealth";
	public static final String FEC_BANCA_TWOWHEELER = "fecBancaTwoWheeler";
	public static final String FEC_BANCA_CC = "fecBancaCancerCare";
	public static final String FEC_BANCA_PA = "fecBancaPersonalAccident";
	public static final String FEC_BANCA_TL = "fecTermLifeInsurance";
	public static final String FEC_BANCA_FAMILY_HEALTH = "fecFamilyHealthInsurance";

	public static final String PLACEOFORIGIN_HYPERVERGE = "placeoforigin_hyperverge";
	public static final String DELIVERY_CITY = "cityDelivery_user";
	public static final String DELIVERY_PROVINCE = "provinceDelivery_user";
	public static final String DELIVERY_ADDRESS_LINE = "AddressLineDelivery_user";
	public static final String ADDRESS_HYPERVERGE = "address_hyperverge";
	public static final String DOB_HYPERVERGE = "dob_hyperverge";
	public static final String MEMBER_PREMIUM = "member_premium";
	public static final String NATIONALITY_HYPERVERGE = "nationality_hyperverge";
	public static final String NATIONALITY_HYPERVERGE_VI = "nationality_hyperverge_vi";
	public static final String FULLNAME_HYPERVERGE = "fullname_hyperverge";
	public static final String FAMILY_HEALTH_RELATION_SHIP_LABEL = "relationship_label";
	public static final String GENDER_HYPERVERGE = "gender_hyperverge";
	public static final String GENDER_HYPERVERGE_VI = "gender_hyperverge_vi";
	public static final String NATIONAL_ID_HYPERVERGE = "nid_hyperverge";
	public static final String APPLICATION_MOBILE_NUMBER = "applicantMobileNumber_user";
	public static final String NID_ERROR = "nid_error";
	public static final String NID_ERROR_MESSAGE = "nid_error_message";
	public static final String NID_BACK = "nid_back";
	public static final String NID_FRONT = "nid_front";
	public static final String HYPERVERGE_BACK_RESPONSE = "hyperverge_back_response";
	public static final String HYPERVERGE_FRONT_RESPONSE = "hyperverge_front_response";
	public static final String CIF_RESPONSE = "cif_response";
	public static final String VIETNAMESE = "Việt Nam";
	public static final String DUPLCATE_JOURNEY = "Duplicate Journey!";
	public static final String VIETNAMESE_VI = "Việt Nam";
	public static final String APPLICANT_MOBILE_NO = "applicantMobileNo";
	public static final String APPLICANT_EMAIL = "applicantEmail";
	public static final String APPLICANT_EMAIL_USER = "applicantEmail_user";

	public static final String PROVINCE_HYPERVERGE = "province_hyperverge";

	public static final String JOURNEY_APPLICATION_ID = "application_id";
	public static final String JOURNEY_CATEGORY_ID = "category_id";
	public static final String JOURNEY_PRODUCT_ID = "product_id";
	public static final String JOURNEY_PLAN_ID = "plan_id";
	public static final String JOURNEY_CATEGORY_NAME = "category_name";
	public static final String JOURNEY_PRODUCT_NAME = "product_name";
	public static final String JOURNEY_PLAN_NAME = "plan_name";
	public static final String JOURNEY_PLAN_TYPE = "plan_type";
	public static final String JOURNEY_MONTHLY_PAYMENT = "whatYouPay";
	public static final String FULL_PREMIUM_AMOUNT = "fullPremiumAmount";
	public static final String TOTAL_PREMIUM_AMOUNT = "totalPremiumAmount";
	public static final String JOURNEY_PROTECTION_AMOUNT = "protectionAmount";
	public static final String JOURNEY_INSURER_NAME = "insurer_name";
	/*
	 * temporary inurer changes
	 */
	// public static final String JOURNEY_INSURER = "Bao Minh Corporation";
	public static final String JOURNEY_INSURER = "Tổng công ty cổ phần Bảo Minh";
	public static final String PVI_INSURER = "Tổng Công ty Bảo hiểm PVI";
	public static final String PA_INSURER = "PetroVietnam Insurance";
	// public static final String PVI_INSURER = "PetroVietnam Insurance";
	public static final String JOURNEY_INSURER_PAYMENT_INFO = "payment_typeCheck";

	// cif constants

	public static final String JOURNEY_DUE_DATE = "due_date";

	// SH productId
	public static final String JOURNEY_SH_PRODUCT_ID = "10001";

	// cif constants

	public static final String CIF_NUMBER = "CIFNumber";
	public static final String CIF_CUSTOMER = "Customer";
	public static final String CIF_CUSTOMERS = "Customers";
	public static final String NS1_GET_CUSTOMER_LIST_RESPONSE = "NS1:GetCustomerListResponse";
	public static final String CIF_ERROR = "error";

	// dashBoard constants

	public static final String PLAN_NAME = "planName";
	public static final String PLAN_NAME_VI = "planName_vi";
	public static final String PRODUCT_NAME = "productName";
	public static final String PRODUCT_NAME_VI = "productName_vi";
	public static final String CATEGORY_NAME = "categoryName";
	public static final String PLAN_ID = "planId";
	public static final String PRODUCT_ID = "productId";
	public static final String CATEGORY_ID = "categoryId";
	public static final String DATE_MODIFIED = "dateModified";
	public static final String PROCESS_INSTANCE_ID = "processInstanceId";
	public static final String INSURER_PDF_DOC_ID = "insurerPdfDocId";
	public static final String FH_MEMBER_DOC_ID = "fhMemberDocId";
	public static final String PURCHASED_POLICIES = "purchasedPolicies";
	public static final String PENDING_POLICIES = "pendingPolicies";
	public static final String EXPIRING_DATE = "expiringDate";
	public static final String PURCHASED_DATE = "purchase_date";
	public static final String INSURER = "insurer";
	public static final String PROTECTION_AMOUNT = "protectionAmount";
	public static final String MONTHLY_PAYMENT = "monthlyPayment";
	public static final String NEXT_PAYMENT_DUE = "nextPaymentDue";
	public static final String POLICY_NUMBER = "policyNumber";
	public static final String PROCESS_NAME = "processName";
	public static final String POLICY_DETAILS = "policyDetails";
	public static final String PAYMENT_MODE_TYPE = "payment_mode_type";
	public static final String USER_PREMIUM_TENURE = "userPremiumTenure";
	public static final String TYPE_OF_PAYMENT = "type_Of_payment";
	public static final String POLICY_NUMBER_LABLE = "policyNumberLable";
	public static final String POLICY_NUMBER_LABLE_VALUE = "Số hợp đồng bảo hiểm";

	public static final String APPLICATION_STATUS = "application_status";
	public static final String APP_STATUS_REJECTED = "rejected";
	public static final String APP_STATUS_COMPLETED = "completed";

	public static final String MEDICAL_Q1_KEY = "q1SingleHealth_user";
	public static final String MEDICAL_Q2_KEY = "q2SingleHealth_user";
	public static final String MEDICAL_Q3_KEY = "q3SingleHealth_user";

	public static final String MEDICAL_Q1_FH_KEY = "q1FamilyHealth_user";
	public static final String MEDICAL_Q2_FH_KEY = "q2FamilyHealth_user";
	public static final String MEDICAL_Q3_FH_KEY = "q3FamilyHealth_user";

	public static final String MEDICAL_Q1_COVID_KEY = "q1Covid_user";
	public static final String MEDICAL_Q2_COVID_KEY = "q2Covid_user";
	public static final String MEDICAL_Q3_COVID_KEY = "q3Covid_user";
	public static final String MEDICAL_Q4_COVID_KEY = "q4Covid_user";

	// Ipat Constants

	public static final String IPAT_TRANSACTION_ID = "Ipat_TransactionId";
	public static final String IPAT_CRM_ID = "Ipat_CRMID";
	public static final String IPAT_TRANSACTION_STATUS = "ipat_transaction_status";

	public static final String IPAT_PAYMENT_START_DATE = "payment_start_date";
	public static final String IPAT_PLAN_EXPIRING_DATE = "plan_expiring_date";
	public static final String IPAT_NEXT_PAYMENT_DUE = "next_payment_due";

	public static final String IPAT_PAYMENT_START_DATE_TIME = "payment_start_date_time";

	public static final String IPAT_PAYOO_SERVICE_ID = "payooSeriviceId";
	public static final String IPAT_PAYOO_PROVIDER_ID = "payooProviderId";
	public static final String IPAT_CRMID = "crmId";
	public static final String IPAT_CRMID_KEY = "5391460020452779";

	// TL start & end date

	public static final String TL_POLICY_START_DATE = "tl_policy_start_date";
	public static final String TL_POLICY_END_DATE = "tl_policy_end_date";

	// primium calculation constants
	public static final String MASTER_VEHICLE_VALUE = "vehicle_value";
	public static final String BANCA_GET_BIKE_DETAILS = "banca-get-bike-details";
	public static final String MASTER_CC = "cc";
	public static final String MASTER_MODEL_ID = "model_id";
	public static final String MASTER_BRAND_ID = "brand_id";
	public static final String MASTER_BRAND_NAME = "brand_name";
	public static final String MASTER_MODEL_NAME = "model_name";
	public static final String PROCESS_VARIABLE_PREMIUM = "premium";
	public static final int THIRD_PARTY_LIABILITY = 60000;
	public static final double MOTOR_LOSS = 1.65;
	public static final String PROCESS_VARIABLE_PROTECTION = "protection";
	public static final String MASTER_YEAR_OF_REGISTRATION = "year_of_registration";
	public static final String MASTER_MONTHS = "months";
	public static final String PROCESS_VARIABLE_YEAR_OF_REGISTRATION = "q4TwoWheeler_Tw";
	public static final String PROCESS_VARIABLE_CC = "q3TwoWheeler_Tw";
	public static final String PROCESS_VARIABLE_MODEL_ID = "q2TwoWheeler_Tw";
	public static final String PROCESS_VARIABLE_BRAND_ID = "q1TwoWheeler_Tw";
	public static final double ACCIDENT = 20000;
	public static final double ACCIDENT_PROTECTION_AMOUNT = 20000000;
	public static final String PERCENTAGE_OF_THE_VEHICLE_VALUE = "percentage_of_the_vehicle_value";
	public static final String BANCA_GET_BIKE_VALUE = "banca-get-bike-value";
	public static final String BANCA_GET_PERCENTAGE_VEHICLE_VALUE = "banca-get-percentage-vehicle-value";
	public static final String BANCA_GET_QUOTE_DETAILS = "banca-get-quote-details";
	public static final String THIRD_PARTY_LIABILITY_KEY = "third_party_liability";
	public static final String PERSONAL_ACCIDENT_KEY = "personal_accident";
	public static final String VEHICLE_VALUE = "vehicle_value";
	public static final String VEHICLE_REGISTRATION_NUMBER = "q5TwoWheeler_Tw";
	public static final String MASTER_AGE = "age_of_the_vehicle";
	public static final LinkedHashMap<String, String> CMS_ACTION_CODE_MAP;
	static {
//			CMS_ACTION_CODE_MAP = new HashMap<String,String>();
//			CMS_ACTION_CODE_MAP.put("10001", "BMSH");
//			CMS_ACTION_CODE_MAP.put("10002", "PVTW");
//			CMS_ACTION_CODE_MAP.put("10005", "PVPA");
//			CMS_ACTION_CODE_MAP.put("10004", "PVCC");
//			CMS_ACTION_CODE_MAP.put("10006", "BVTB");
//			CMS_ACTION_CODE_MAP.put("10006", "BVTA");
		CMS_ACTION_CODE_MAP = new LinkedHashMap<String, String>();

		CMS_ACTION_CODE_MAP.put("10003-10003001", "BMFH");
		CMS_ACTION_CODE_MAP.put("10003-10003002", "BMFH");
		CMS_ACTION_CODE_MAP.put("10003-10003003", "BMFH");

		// Need To Change Product ID And Plan Id
//			CMS_ACTION_CODE_MAP.put("10003-10003003", "BVUE");
//			CMS_ACTION_CODE_MAP.put("10003-10004001", "BVUS");
//			CMS_ACTION_CODE_MAP.put("10003-10004001", "BVUH");
		
		CMS_ACTION_CODE_MAP.put("10011-10011001", "BMDF");
		CMS_ACTION_CODE_MAP.put("10011-10011002", "BMDF");

		CMS_ACTION_CODE_MAP.put("10001-10001001", "BMSH");
		CMS_ACTION_CODE_MAP.put("10001-10001002", "BMSH");
		CMS_ACTION_CODE_MAP.put("10001-10001003", "BMSH");
		CMS_ACTION_CODE_MAP.put("10002-10002001", "PVPD");
		//CMS_ACTION_CODE_MAP.put("10002-10002001", "PVPD");
		CMS_ACTION_CODE_MAP.put("10002-10002001,10002002", "PVPC");
		CMS_ACTION_CODE_MAP.put("10002-10002001,10002002,10002003", "PVTW");
		CMS_ACTION_CODE_MAP.put("10005-10005001", "PVPA");
		CMS_ACTION_CODE_MAP.put("10005-10005004", "PVPA");
		CMS_ACTION_CODE_MAP.put("10005-10005005", "PVPA");
		CMS_ACTION_CODE_MAP.put("20001-10004001", "BVTB");
		CMS_ACTION_CODE_MAP.put("20001-10004002", "BVTB");
		CMS_ACTION_CODE_MAP.put("20001-10004003", "BVTB");
		CMS_ACTION_CODE_MAP.put("20001-10004001,1001", "BVTA");
		CMS_ACTION_CODE_MAP.put("20001-10004002,1001", "BVTA");
		CMS_ACTION_CODE_MAP.put("20001-10004003,1001", "BVTA");
		CMS_ACTION_CODE_MAP.put("10004-10004001", "PVCC");
		CMS_ACTION_CODE_MAP.put("10004-10004002", "PVCC");
	}
	// primium calculation constants for family health
	public static final String BANCA_GET_FH_PREMIUM = "banca-get-fh-premium";
	public static final String BANCA_GET_FH_QUICK_QUOTE = "banca-get-fh-quick-quote";
	public static final String BANCA_GET_FH_QUOTE_DETAILS = "banca-get-fh-quote-details";

	public static final String TRIỆU_VNĐ = " triệu VNĐ";
	public static final String NGÀN_VNĐ = " ngàn VNĐ";

	public static final String VIATNUM_TRIEU_CURRENCY = "triệu";
	public static final String VIATNUM_NGAN_CURRENCY = "ngàn";

	public static final String SH_CATEGORY_ID = "1001";
	public static final String SH_PRODUCT_ID = "10001";
	public static final String TW_CATEGORY_ID = "1003";
	public static final String TW_PRODUCT_ID = "10002";
	public static final String TL_PRODUCT_ID = "20001";
	public static final String PA_CATEGORY_ID = "1002";
	public static final String PA_PRODUCT_ID = "10005";
	public static final String CC_CATEGORY_ID = "1001";
	public static final String CC_PRODUCT_ID = "10004";
	public static final String FH_CATEGORY_ID = "1001";
	public static final String FH_PRODUCT_ID = "10003";
	public static final String CV_PRODUCT_ID = "10010";
	public static final String CV_CATEGORY_ID = "1001";

	public static final String TW_MOTOR_LOSS_PLAN_ID = "10002001";
	public static final String TW_THIRD_PARTY_PLAN_ID = "10002002";
	public static final String TW_PERSONAL_ACCIDENT_PLAN_ID = "10002003";
	public static final String PA_PERSONAL_ACCIDENT_PLAN_ID = "10005001";
	public static final String CC_CANCER_CARE_PLAN_ID = "10004001";
	public static final String VTIGER_TW_PLAN_TYPE = "motorLoss";
	public static final String VTIGER_TL_PLAN_TYPE = "TermLifeProtection";

	// familyHealth
	public static final String RELATION = "relation";
	public static final String FAMILY_MEMBER_LIST = "family_member_list";
	public static final String FAMILY_MEMBER_COUNT = "familyMemberCount";
	public static final String Q1_CHILDREN = "q1Children_FamilyHealth_user";
	public static final String Q2_CHILDERN = "q2Children_FamilyHealth_user";
	public static final String DASH = "-";
	public static final String EXPANDABLE_HYPERVERGE = "expandableHyperverge";

	public static final String BASIC_PLAN_ID = "10010001";

	public static final String E_SIGN_DOC_LIST = "esignDocList";

	public static final String CLAIMS_QUESTIIONS = "allQuestion_system";
	public static final String CLAIMS_QUS_MAP="question";
	public static final String CLAIMS_ANS_MAP="answer";
	
	public static final String CLAIM_HOSPITAL_DOCS = "hospitalBills_system";
	
	public static final String CLAIM_HOSPITAL_DOCS_UAT_URL = "https://banca-dev.fecredit.com.vn/wf-implementation-0.0.1-SNAPSHOT/dms/download-document/";

	public static final String CLAIM_HOSPITAL_DOCS_PROD_URL = "https://shield.fecredit.com.vn/wf-implementation-0.0.1-SNAPSHOT/dms/download-document/";

	
	// UVL
	public static final String UVL_SAVINGS_PREMIUM = "banca-get-uvl-savings-premium";

	public static final String CLAIMS_BENIFICIARY_CODES = "claimQs_system";
	public static final String LANGUAGE = "language";
	public static final String HOSPITAL_BILL_SYSTEM = "hospitalBills_system";
	public static final String BANCA_GET_BENIFICIARY_DOC = "banca-get-benificiary-hospital-docs";
	public static final String BENIFICIARY_CODE = "benefitCode";

	public static final String BENIFICIARY_MASTER_CODE = "benefit_code";
	public static final String BENIFICIARY_DESCRIPTION = "description";
	public static final String BENIFICIARY_TITLE = "title";
	
	public static final String REG_TOPIC = "registration";
	public static final String SH_TOPIC = "singlehealth";
	public static final String TW_TOPIC = "twowheeler";
	public static final String PA_TOPIC = "personalaccident";
	public static final String CC_TOPIC = "cancercare";
	public static final String FH_TOPIC = "familyhealth";
	public static final String TL_TOPIC = "termlife";
	public static final String US_TOPIC = "uvlsavings";
	public static final String UH_TOPIC = "uvlhealth";
	public static final String UE_TOPIC = "uvleducation";
	
	public static final String PARTIAL_AMOUNT = "partialAmount";
	public static final String ACCESS_AMOUNT = "accessAmount";
	public static final String TOTAL_PAID_AMOUNT = "totalPaidAmount";

}
