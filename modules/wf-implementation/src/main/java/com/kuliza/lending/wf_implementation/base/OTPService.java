package com.kuliza.lending.wf_implementation.base;

import static com.kuliza.lending.wf_implementation.utils.OTPUtil.checkOptionalOTP;

import com.kuliza.lending.authorization.config.keycloak.Secret;
import com.kuliza.lending.authorization.config.keycloak.TOTPManager;

import com.kuliza.lending.engine_common.configs.OTPConfig;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.OTPDetailsDao;
import com.kuliza.lending.wf_implementation.models.OTPDetails;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.utils.OTPConstants;
import com.kuliza.lending.wf_implementation.utils.OTPUtil;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OTPService {

  @Autowired
  private OTPConfig otpConfig;

  @Autowired
  private WfImplConfig wfImplConfig;

  private TOTPManager manager;
  private OTPDetailsDao otpDetailsDao;
  private Integer otpLength;

  private static final Logger logger = LoggerFactory.getLogger(OTPService.class);

  @Autowired
  public OTPService(final OTPDetailsDao otpDetailsDao,
      @Value("${otp.otpLength}") final Integer otpLength,
      @Value("${otp.otpExpiryTimeInMinutes}") final Integer otpExpiryTime) {
    super();
    this.otpDetailsDao = otpDetailsDao;
    this.otpLength = otpLength;
    this.manager = new TOTPManager(otpLength, otpExpiryTime * 60);
  }

  /**
   * Generates the OTP and sends it to user
   *
   * @return ApiResponse
   */
  protected String otpGenerate(HttpServletRequest request, UserPasswordGenerationForm userForm) {
    String mobileNumber = userForm.getMobile();
    logger.info("OTP generate: "+ mobileNumber);
    long currentTime = new Date().getTime();
    String deviceIdentifier = OTPUtil.getDeviceIdentifier(request, userForm.getDeviceId());
    OTPDetails otpDetails = otpDetailsDao
        .findTopByUserIdentifierAndIsDeletedOrderByIdDesc(mobileNumber, false);
    if (otpDetails == null
        || (otpDetails != null && otpDetails.getOtpGenerateCount() <= otpConfig.getMaxOtpGenerateAttempts())) {
      long otpTime = System.currentTimeMillis();
      byte[] secret = Secret.generate();
      String otp = manager.generate(secret, otpTime);
      String key = Secret.toHex(secret);
      logger.info(key);
      long userBlockTime = otpDetails != null && otpDetails.getUserBlockTime() != null ? otpDetails
          .getUserBlockTime().getTime() :
          Long.MAX_VALUE;
      long otpThresholdTime = otpDetails != null ? otpDetails.getOtpGenerationTime() + (
          otpConfig.getOtpThresholdInMinutes() * OTPConstants.ONE_MINUTE_TO_MILLIS) : Long.MAX_VALUE;
      logger.info(String.format(OTPConstants.OTP_TIME_DETAILS, userBlockTime, otpThresholdTime));
      if (otpDetails == null || (userBlockTime < currentTime) || (userBlockTime == Long.MAX_VALUE
          && otpThresholdTime < currentTime)) {
        // New row for otp
        if (otpDetails != null && (userBlockTime < currentTime) || (userBlockTime == Long.MAX_VALUE
            && otpThresholdTime < currentTime)) {
          // As cool down time or block time is passed, mark previous record deleted
          otpDetails.setIsDeleted(true);
          otpDetailsDao.save(otpDetails);
        }
        OTPDetails newOTPDetails = new OTPDetails(mobileNumber, deviceIdentifier, key, otpTime);
        otpDetailsDao.save(newOTPDetails);
        return otp;
      } else {
        // Resend OTP
        checkForBlockedUser(otpDetails, otpConfig.getMaxOtpGenerateAttempts(), false);
        long otpExpiryTime = otpDetails.getOtpGenerationTime() +
            (otpConfig.getOtpExpiryTimeInMinutes()*OTPConstants.ONE_MINUTE_TO_MILLIS);
        if(otpExpiryTime > currentTime) {
          // Previous otp not expired yet
          return manager.generate(Secret.fromHex(otpDetails.getSecretKey()),
              otpDetails.getOtpGenerationTime());
        } else {
          // Resend new otp
          otpDetails.setSecretKey(key);
          otpDetails.setOtpGenerationTime(otpTime);
          otpDetailsDao.save(otpDetails);
          return otp;
        }
      }
    } else {
      logger.info(String.format(OTPConstants.USER_BLOCKED, otpConfig.getUserBlockTimeInMinutes()));
      return null;
    }
  }

  /**
   * Checks for otp validation
   *
   * @param request: User Request
   * @param userValidationinputForm: user Input details for validationg
   * @return ApiResponse
   */
  public boolean checkBlockedOrValidateOTP(HttpServletRequest request, UserValidationinputForm userValidationinputForm) {
    String mobileNumber = userValidationinputForm.getMobile();
    String otp = userValidationinputForm.getOtp();
    long currentTime = new Date().getTime();
    String deviceIdentifier = OTPUtil
        .getDeviceIdentifier(request, userValidationinputForm.getDeviceId());
    OTPDetails otpDetails = otpDetailsDao
        .findTopByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
            mobileNumber, deviceIdentifier, false, false, false);

    if (otpDetails == null || ((otpDetails.getOtpGenerationTime() + (otpConfig.getOtpThresholdInMinutes()
            * OTPConstants.ONE_MINUTE_TO_MILLIS) < currentTime))) {
      logger.info("OTP details are empty for mobile: " + mobileNumber);
      return false;
    } else {
      // Sent OTP is valid as per time
      checkForBlockedUser(otpDetails,
          otpConfig.getMaxOtpValidateAttempts(), true);
      if (otpValidate(otp, otpDetails.getSecretKey(), otpDetails.getOtpGenerationTime())) {
        otpDetails.setIsDeleted(true);
        otpDetailsDao.save(otpDetails);
        return true;
      } else {
        logger.info(String.format(OTPConstants.OTP_INVALID, mobileNumber));
        return false;
      }
    }
  }

  /**
   * Validates the otp submitted by user
   *
   * @return boolean flag
   */
  private Boolean otpValidate(String otp, String secret, long otpGenerationTime) {
    if (wfImplConfig.getValidateDefaultOTP() && checkOptionalOTP(otp)) {
      return true;
    }
    byte[] v = Secret.fromHex(secret);
    return manager.validate(v, otp, otpGenerationTime);
  }

  /**
   * This method checks number of requests from user and decides whether current user should be
   * blocked or not
   */
  private void checkForBlockedUser(OTPDetails otpDetails, int maxAttempts,
      boolean isValidatingOTP) {
    if (isValidatingOTP) {
      int count = otpDetails.getOtpValidateCount();
      otpDetails.setOtpValidateCount(++count);
      if (count >= maxAttempts) {
        otpDetails.setBlockedOnValidate(true);
        otpDetails.setUserBlockTime(otpConfig.getUserBlockTimeInMinutes());
      }
    } else {
      int count = otpDetails.getOtpGenerateCount();
      otpDetails.setOtpGenerateCount(++count);
      if (count >= maxAttempts) {
        otpDetails.setBlockedOnGenerate(true);
        otpDetails.setUserBlockTime(otpConfig.getUserBlockTimeInMinutes());
      }
    }
    otpDetailsDao.save(otpDetails);
  }

}
