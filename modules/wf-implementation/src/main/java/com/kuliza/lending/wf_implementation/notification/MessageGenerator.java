package com.kuliza.lending.wf_implementation.notification;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageGenerator {

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static enum Platform {
		APNS,
		GCM;
	}

	public static String jsonify(Object message) {
		try {
			return objectMapper.writeValueAsString(message);
		} catch (Exception e) {
			e.printStackTrace();
			throw (RuntimeException) e;
		}
	}

	private static Map<String, String> getAndroidData(String message) {
		Map<String, String> payload = new HashMap<String, String>();
		payload.put("message", message);
		return payload;
	}

	public static String getAppleMessage(String message) {
		Map<String, Object> appleMessageMap = new HashMap<String, Object>();
		Map<String, Object> appMessageMap = new HashMap<String, Object>();
		appMessageMap.put("alert", message);
		appMessageMap.put("badge", 9);
		appMessageMap.put("sound", "default");
		appleMessageMap.put("aps", appMessageMap);
		return jsonify(appleMessageMap);

	}

	
	public static String getAndroidMessage(String message) {
		Map<String, Object> androidMessageMap = new HashMap<String, Object>();
		androidMessageMap.put("collapse_key", "New Message");
		androidMessageMap.put("data", getAndroidData(message));
		androidMessageMap.put("delay_while_idle", true);
		androidMessageMap.put("dry_run", false);
		return jsonify(androidMessageMap);
	}

}