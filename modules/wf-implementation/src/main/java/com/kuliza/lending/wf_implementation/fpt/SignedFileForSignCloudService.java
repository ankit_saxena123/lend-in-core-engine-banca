package com.kuliza.lending.wf_implementation.fpt;


import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;
import org.flowable.engine.RuntimeService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.services.FECDMSService;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.FPTUtils;
import com.kuliza.lending.wf_implementation.utils.SSLUtilities;

import omnidocsapi.SysResponseType;
import omnidocsapi.UploadDocResponse;
import vn.com.fis.ws.CredentialData;
import vn.com.fis.ws.MultipleSignedFileData;
import vn.com.fis.ws.Services;
import vn.com.fis.ws.Services_Service;
import vn.com.fis.ws.SignCloudReq;
import vn.com.fis.ws.SignCloudResp;


@Service
public class SignedFileForSignCloudService {

	Logger logger = LoggerFactory.getLogger(SignedFileForSignCloudService.class);

	@Autowired
	RuntimeService runtimeService;
	
	@Autowired
	private FPTUtils fptUtils;
	
	@Autowired
	private WfImplConfig wfImplConfig;
	
	@Autowired
	private FECDMSService fECDMSService;

	public SignCloudResp getSignedFileForSignCloud(String agreementUUID, String billCode, boolean p2pEnabled,
			String appId,String procInstId) throws Exception {

		JSONObject response = new JSONObject();
			logger.info("Requested for download file FPT :agreementUUID " + agreementUUID + " billCode : " + billCode);
			SSLUtilities.trustAllHostnames();
			SSLUtilities.trustAllHttpsCertificates();
			
			Services_Service services = fptUtils.createFPTService();

			Services ws = services.getServicesPort();

			String timestamp = String.valueOf(System.currentTimeMillis());

//			String data2sign = ESignCloudConstants.RELYING_PARTY_USER + ESignCloudConstants.RELYING_PARTY_PASSWORD
//					+ ESignCloudConstants.RELYING_PARTY_SIGNATURE + timestamp;
//			String pkcs1Signature = FPTUtils.getPKCS1Signature(data2sign, ESignCloudConstants.RELYING_PARTY_KEY_STORE,
//					ESignCloudConstants.RELYING_PARTY_KEY_STORE_PASSWORD);

			String pkcs1Signature = fptUtils.getPKCS1Signature(timestamp);
			
			SignCloudReq signCloudReq = new SignCloudReq();
			signCloudReq.setRelyingParty(wfImplConfig.getFptRelyingParty());
			signCloudReq.setAgreementUUID(agreementUUID);
			signCloudReq.setP2PEnabled(p2pEnabled);
			signCloudReq.setBillCode(billCode);

			CredentialData credentialData = new CredentialData();
			credentialData.setUsername(wfImplConfig.getFptRelyingPartyUserName());
			credentialData.setPassword(wfImplConfig.getFptRelyingPartyPassword());
			credentialData.setTimestamp(timestamp);
			credentialData.setSignature(wfImplConfig.getFptRelyingPartySignature());

			credentialData.setPkcs1Signature(pkcs1Signature);
			signCloudReq.setCredentialData(credentialData);

			SignCloudResp signCloudResp = ws.getSignedFileForSignCloud(signCloudReq);

			logger.info("Code: " + signCloudResp.getResponseCode());
			logger.info("Message: " + signCloudResp.getResponseMessage());
			logger.info("BillCode: " + signCloudResp.getBillCode());
			logger.info("NotificationMessage: " + signCloudResp.getNotificationMessage());
			logger.info("NotificationSubject: " + signCloudResp.getNotificationSubject());
			logger.info("RemainingCounter: " + signCloudResp.getRemainingCounter());

			response.put("statusCode", signCloudResp.getResponseCode());
			response.put("message", signCloudResp.getResponseMessage());
			response.put("billCode", signCloudResp.getBillCode());
			response.put("notificationMessage", signCloudResp.getNotificationMessage());
			response.put("notificationSubject", signCloudResp.getNotificationSubject());
			response.put("remainingCounter", signCloudResp.getRemainingCounter());
			response.put("signedCertificate", signCloudResp.getSignedFileData());

			if (signCloudResp.getResponseCode() == 0) {

				if (!signCloudResp.getMultipleSignedFileData().isEmpty()) {
					
					String fptOutBoundFileDirectory = wfImplConfig.getFptOutBoundFilePath();

					for (int i = 0; i < signCloudResp.getMultipleSignedFileData().size(); i++) {
						MultipleSignedFileData multipleSignedFileData = signCloudResp.getMultipleSignedFileData()
								.get(i);

						if (multipleSignedFileData.getSignedFileData() != null) {
							String filePath = fptOutBoundFileDirectory + multipleSignedFileData.getSignedFileName();
							logger.info("Saved in " + filePath);
							logger.info("Multiple signed file: ");
							logger.info("FPT SIGNED FILE NAME :" + multipleSignedFileData.getSignedFileName());
							IOUtils.write(multipleSignedFileData.getSignedFileData(), new FileOutputStream(filePath));
							
							File file = new File(filePath);
							runtimeService.setVariable(procInstId, Constants.FPT_ESIGN_DOC_PATH_+(i+1), filePath);
							
							UploadDocResponse uploadDocument = fECDMSService.uploadDocument(Constants.ESIGN_UPLOAD_DMS_ID, appId, "101",
									true, file);
							SysResponseType sys = uploadDocument.getSys();
							
							String docId = "";
							int code = sys.getCode();
							if (code == 1) {
								docId = fECDMSService.getDMSDocIdFromResponse(uploadDocument);
								logger.info("DOC id for :"+ docId+" ,File"+ filePath);
								runtimeService.setVariable(procInstId, Constants.FPT_ESIGN_DOC_ID_+(i+1), docId);
							} else {
								logger.info("DOC upload failed for File"+ filePath);
								throw new FECException(ErrorCodes.DMS_UPLOAD_ERROR);
							}
							

						} else {
							logger.info("FPT : " + multipleSignedFileData.getSignedFileUUID());
							logger.info("FPT : " + multipleSignedFileData.getDmsMetaData());
						}
					}

				}
			}
		return signCloudResp;
	}


}
