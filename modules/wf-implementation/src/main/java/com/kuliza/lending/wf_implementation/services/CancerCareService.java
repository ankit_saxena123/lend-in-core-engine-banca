package com.kuliza.lending.wf_implementation.services;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.pojo.ProductRequest;
import com.kuliza.lending.wf_implementation.pojo.TlPremiumInput;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Constants.Gender;
import com.kuliza.lending.wf_implementation.utils.Utils;

@Service
public class CancerCareService {
	
	@Autowired
	private WorkflowUserDao workFlowUserDao;
	
	@Autowired
	private MasterApiIntegration masterApiIntegration;
	
	@Autowired
	TermLifeService termLifeService;
	
	private static final Logger logger = LoggerFactory.getLogger(CancerCareService.class);

	
	public ApiResponse getQuickQuote(String userName, ProductRequest productRequest, String slug) {
		
		try {
		
			ObjectMapper mapper = new ObjectMapper();
			logger.info("productRequest --->"+mapper.writeValueAsString(productRequest));
			
			WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);

			if (workFlowUser == null) {
				return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
			}
			
			JSONArray planDetails = getPlanList(productRequest.getKeyList(),slug);
			
			if(planDetails == null) {
				return new ApiResponse(HttpStatus.FOUND,"Plan details empty");
			}
			
			String dateOfBirth = Utils.getStringValue(workFlowUser.getDob());
			int age = Utils.getAgeFromDOB(dateOfBirth);
			logger.info(" <========= age =========> " + age);
			
			String gender = workFlowUser.getGender();
			Integer genderId = getGenderIdByGender(gender);
			
			logger.info("gender : " + gender+", genderId: "+genderId);
			ApiResponse quoteResponse = buildPlanDetails(planDetails, age,genderId.toString());

			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, quoteResponse);

		} catch (Exception e) {
			logger.error("Error in Cancer quick quote",e);
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}
	
		
	}


	private ApiResponse buildPlanDetails(JSONArray planDetails, int age, String genderId) throws Exception {

		if (planDetails == null) {
			new ApiResponse();
		}

		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();

		for (int i = 0; i < planDetails.length(); i++) {

			JSONObject plan = planDetails.getJSONObject(i);
			String plan_id = plan.getString("plan_id");
			String premiumKey = plan_id + "_" + genderId + "_" + age;

			logger.info("Premium Key : " + premiumKey);
			String premium = Constants.cancer_premium_map.get(premiumKey);

			logger.info("premium is " + premium);
			if (premium == null) {
				logger.info("Premium is null for " + premiumKey);
				throw new Exception("Invlaid Premium");
			}

			// replace plan value short value
			String planValues = plan.getString("plan_values");
			String[] splitPlanValue = planValues.split(" ");
			splitPlanValue[0] = String.valueOf(premium);
			String planVal = Utils.createStringForPlanValues(splitPlanValue, " ");
			plan.put("plan_values", planVal);

			// replace plan short value
			String plan_short_values = plan.getString("plan_short_values");
			String[] planShortValuesArrray = plan_short_values.split("[|]");
			planShortValuesArrray[0] = String.valueOf(premium);
			String protectAmount = String.join("|", planShortValuesArrray);

			plan.put("plan_short_values", protectAmount);
			
			plan.put(Constants.BASIC_PREMIUM, premium);

		}
		finalMap = new ObjectMapper().readValue(planDetails.toString(), new TypeReference<List<Map<String, Object>>>() {
		});

		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, finalMap);
	}


	private JSONArray getPlanList(List<HashMap<String, Object>> filterList, String slug) throws Exception {
		
		JSONArray plansList = null;
		try {
			JSONArray data = Utils.filterJsonArray(filterList);
			JSONObject requestPayloadForMaster = masterApiIntegration.buildRequestPayload(data);
			JSONObject masterResponse = masterApiIntegration.getDataFromService( requestPayloadForMaster, slug);
			
			logger.info("masterResponse ----------->"+masterResponse.toString());
			
			if(masterResponse.getJSONArray("data") == null) {
				return plansList;
			}
			
			plansList = masterResponse.getJSONArray("data");
		} catch (Exception e) {
			logger.error("Error while call master", e);
			throw e;
		}
		
		
		return plansList;
	}


	public ApiResponse getQuickQuoteDetails(String userName, TlPremiumInput ccPremiumInput, String slug) {
		
		JSONArray keyListArray = new JSONArray();
		JSONObject quoteDetailsMasterData = null;
		JSONObject requestPayload;
		List<Map<String, Object>> finalMap = new ArrayList<Map<String, Object>>();
		try {
			String languageValue = Utils.getStringValue(ccPremiumInput.getLanguage());
			JSONObject languageAsJson = masterApiIntegration.getFilterJsonObject(Constants.LANGUAGE_KEY, languageValue);
			keyListArray.put(languageAsJson);
			logger.info("<============> languageAsJson <============>" + languageAsJson);
			requestPayload = masterApiIntegration.buildRequestPayload(keyListArray);
			logger.info("<============> requestPayload <============> :: " + requestPayload);
//			quickQuoteMasterData = masterApiIntegration.getDataFromService(requestPayload,Constants.MASTERS_BANCA_GET_TERM_LIFE_QUICK_QUOTE);
			
			quoteDetailsMasterData = masterApiIntegration.getDataFromService(requestPayload,Constants.MASTERS_BANCA_GET_CANCER_CARE_QUOTE_DETAILS);
//			JSONArray quickQuoteJsonArray = quickQuoteMasterData.getJSONArray("data");
			
			JSONArray quoteDetailsJsonArray = quoteDetailsMasterData.getJSONArray("data");
			
			WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);

			if (workFlowUser == null) {
				return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
			}
			
			String dateOfBirth = Utils.getStringValue(workFlowUser.getDob());
			int age = Utils.getAgeFromDOB(dateOfBirth);
			logger.info(" <========= age =========> " + age);
			
			String gender = workFlowUser.getGender();
			
			Integer genderId = getGenderIdByGender(gender);
			
			logger.info("gender : " + gender+", genderId: "+genderId);
			
			if (quoteDetailsJsonArray != null) {
				for (int j = 0; j < quoteDetailsJsonArray.length(); j++) {
					JSONObject quoteDetailsJsonObject = quoteDetailsJsonArray.getJSONObject(j);
					String qd_plan_id = quoteDetailsJsonObject.getString("plan_id");

					String premiumKey = qd_plan_id + "_" + genderId + "_" + age;

					logger.info("Premium Key : " + premiumKey);
					String premium = Constants.cancer_premium_map.get(premiumKey);

					logger.info("premium is " + premium);
					if (premium == null) {
						logger.info("Premium is null for " + premiumKey);
						throw new FECException(ErrorCodes.INVALID_PREMIUM_AMOUNT);
					}

					String topSectionValues = quoteDetailsJsonObject.getString("top_section_values");
					String[] topSectionValuesArray = topSectionValues.split(" ");
					logger.info("<========> topSectionValuesArray <========>" + Arrays.toString(topSectionValuesArray));

					topSectionValuesArray[0] = String.valueOf(premium);
					String topsectionValues = Utils.createStringForPlanValues(topSectionValuesArray, " ");
					logger.info("<========> topsectionValues <========>" + topsectionValues);
					quoteDetailsJsonObject.put("top_section_values", topsectionValues);
					
					quoteDetailsJsonObject.put(Constants.BASIC_PREMIUM, premium);
					
				}
				finalMap = new ObjectMapper().readValue(quoteDetailsJsonArray.toString(),
						new TypeReference<List<Map<String, Object>>>() {
						});
			}
				logger.info("<-- Exiting TermLifeService.quoteDetailsMasterModification()");
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, finalMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<============> Internal Error <============> " + e.getMessage(), e);
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}
	
	}


	public Integer getGenderIdByGender(String gender) throws Exception {
		
		if(gender == null || gender.isEmpty()) {
			throw new Exception("Invalid Gender");
		}
		
		Integer genderId = gender.equals(Gender.Male.name()) ?  Gender.Male.getGenderId() : Gender.Female.getGenderId();
		return genderId;
	}

	

}
