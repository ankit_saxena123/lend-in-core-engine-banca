package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.wf_implementation.models.WorkFlowConfig;

@Transactional
public interface WorkflowConfigDao extends CrudRepository<WorkFlowConfig, Long> {
	
	public WorkFlowConfig findById(long id);
	
	public WorkFlowConfig findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowConfig findByName(String name);
	
	public WorkFlowConfig findByNameAndIsDeleted(String name, boolean isDeleted);
}
