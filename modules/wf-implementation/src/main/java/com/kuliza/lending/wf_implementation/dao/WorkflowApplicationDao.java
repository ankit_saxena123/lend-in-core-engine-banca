package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface WorkflowApplicationDao extends CrudRepository<WorkFlowApplication, Long>, JpaRepository<WorkFlowApplication, Long> {

	public WorkFlowApplication findById(long id);

	public WorkFlowApplication findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowApplication findByProcessInstanceIdAndIsDeleted(String processInstanceId, boolean isDeleted);

	public WorkFlowApplication findByIsDeleted(long id, boolean isDeleted);
	
	@Modifying
	@Query(value= "update workflow_app set is_deleted=1 where date(modified) > ?1 and  is_deleted=0 ",nativeQuery = true)
	public int setIsDeletedForExpiryJourney(String oldJourney);
	
	@Modifying
	@Query(value= "update workflow_app set is_deleted=1 where modified < DATE_SUB(NOW(), INTERVAL 1 HOUR) and  is_deleted=0 ",nativeQuery = true)
	public int setIsDeletedForTimelyExpiryJourney();
}
