package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadForm {

	@NotNull(message = "File cannot be null")
	MultipartFile file;

	Integer documentType;

	String label;

	public FileUploadForm() {
		super();
	}

	public FileUploadForm(MultipartFile file, Integer documentType,
			String label) {
		super();
		this.file = file;
		this.documentType = documentType;
		this.label = label;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
