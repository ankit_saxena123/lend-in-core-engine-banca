package com.kuliza.lending.wf_implementation.notification;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.wf_implementation.notification.SNSConfiguration.Platform;
import com.kuliza.lending.wf_implementation.utils.Constants;

@Service
public class SNSMessagePublish extends SNSClient{ 

	    PublishRequest publishRequest = new PublishRequest();;
		
		private static final Logger logger = LoggerFactory
			.getLogger(SNSMessagePublish.class);
	
		public String publishMessageToEndpoint(String body, String title,String endPointArn, DeviceType deviceType){
		   
			try{
				//String platformMessage = null;
				
				publishRequest.setTargetArn(endPointArn);
				publishRequest.setMessageStructure("json");
				
				if(DeviceType.android==deviceType||DeviceType.ios==deviceType) {
					
					//platformMessage =  MessageGenerator.getAndroidMessage(message);
					return publishMessage(body,title,endPointArn,"",Platform.GCM);
					
				}else {
					//platformMessage = MessageGenerator.getAppleMessage(message);
					return publishMessage(body,title,endPointArn,"",Platform.GCM);
				
				}
			
		    }catch(Exception ex){
		    	logger.error("exception occured while publish message to endpoint " + ex);
		    	return null;
		    }

        
    	}
		
		private String publishMessage(String body, String title,String endpointArn, String platformMessage,Platform platform) {
			try {
				Map<String, MessageAttributeValue> notificationAttributes = getValidNotificationAttributes(attributesMap
						.get(platform));
				
				if (notificationAttributes != null && !notificationAttributes.isEmpty()) {
					publishRequest.setMessageAttributes(notificationAttributes);
				}
				
				publishRequest.setMessageStructure("json");
				
				Map<String, String> messageMap = new HashMap<String, String>();
				logger.info("platform name :"+platform.name());
				messageMap.put(platform.name(), platformMessage);
				String message = MessageGenerator.jsonify(messageMap);
				publishRequest.setTargetArn(endpointArn);
	
				StringBuilder builder = new StringBuilder();
				builder.append("{Message Attributes: ");
				for (Map.Entry<String, MessageAttributeValue> entry : notificationAttributes
						.entrySet()) {
					builder.append("(\"" + entry.getKey() + "\": \""
							+ entry.getValue().getStringValue() + "\"),");
				}
				builder.deleteCharAt(builder.length() - 1);
				builder.append("}");
				
				String publishedMessage = builder.toString();
				logger.info("published sns message " + publishedMessage);
				
				logger.info("published sns message " + message);
				String snsMessage=Constants.AMAZON_SNS_MESSAGE;
				logger.info("snsMessage " + snsMessage);
				snsMessage=snsMessage.replace("${body}", body).replace("${title}", title);
				logger.info("snsMessage " + snsMessage);
				JSONObject jsonObject=new JSONObject();
				jsonObject.put("default", body);
				jsonObject.put("GCM", snsMessage);
				logger.info("jsonObject String " + jsonObject.toString());
				publishRequest.setMessage(jsonObject.toString());
				AmazonSNSClient snsClient = getSNSClient();
				final PublishResult publishResponse =snsClient.publish(publishRequest);
			        
				logger.info("published sns message id " + publishResponse.getMessageId());
				
				return message;
				
			}catch(Exception ex) {
		    	
				logger.error("exception occured while publish sns message " + ex);
				return null;

			}
	

	}

	
	public static Map<String, MessageAttributeValue> getValidNotificationAttributes(
			Map<String, MessageAttributeValue> notificationAttributes) {
		Map<String, MessageAttributeValue> validAttributes = new HashMap<String, MessageAttributeValue>();

		if (notificationAttributes == null) return validAttributes;

		for (Map.Entry<String, MessageAttributeValue> entry : notificationAttributes
				.entrySet()) {
			if (!StringUtils.isBlank(entry.getValue().getStringValue())) {
				validAttributes.put(entry.getKey(), entry.getValue());
			}
		}
		return validAttributes;
	}
	
	
}
