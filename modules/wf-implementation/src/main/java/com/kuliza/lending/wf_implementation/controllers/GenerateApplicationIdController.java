package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.services.GenerateApplicationIdService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping("/get-appid")
public class GenerateApplicationIdController {

	@Autowired
	private GenerateApplicationIdService generateApplicationIdService;
	private final Logger logger = LoggerFactory.getLogger(GenerateApplicationIdController.class);

	@RequestMapping(method = RequestMethod.GET, value = "/getApplicationId")
	public ResponseEntity<Object> getApplicationId(Principal principal) {
		try{
			logger.info("<---===app id for user"+principal.getName()+"===--->");
		return CommonHelperFunctions
				.buildResponseEntity(generateApplicationIdService.generateApplicationId(principal.getName()));
		}
		 catch (Exception e) {
				logger.info(CommonHelperFunctions.getStackTrace(e));
				return CommonHelperFunctions.buildResponseEntity((new ApiResponse(
						HttpStatus.INTERNAL_SERVER_ERROR,
						Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
			}
	}
}
