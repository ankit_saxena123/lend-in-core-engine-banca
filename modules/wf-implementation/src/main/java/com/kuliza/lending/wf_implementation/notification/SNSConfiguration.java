package com.kuliza.lending.wf_implementation.notification;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SNSConfiguration {

	public static final String AWS_REGION_NAME = "us-east-1";
	public static final String AWS_ACCESS_KEY = "AKIAIFYSQMTLGOO4MCIQ";
	public static final String AWS_SECRET_KEY = "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";

	public static final String FEC_APP_NAMES = "com.fecredit.Bancassurance";

	public static final String AWS_BANCA_PLATFORM_APPLICATION_ARN = "arn:aws:sns:us-east-1:657462007047:app/GCM/FEC-SHIELD-TEST";
	public static final String AWS_BANCA_PLATFORM_IOS_APPLICATION_ARN = "arn:aws:sns:us-east-1:657462007047:app/APNS/FEC-SHIELD-TEST";

	public static final Map<String, String> ENDPOINT_DICT;
    static {
        Map<String, String> eMap = new HashMap<String,String>();
        eMap.put("android", AWS_BANCA_PLATFORM_APPLICATION_ARN);
        eMap.put("ios", AWS_BANCA_PLATFORM_IOS_APPLICATION_ARN);
        ENDPOINT_DICT = Collections.unmodifiableMap(eMap);
    }

    public static final String DEVICE_TYPES[] = {"ios","android"};
    public static final String MESSAGE_STRUCTURES[] = {"string","json"};
    
    public static enum Platform {
		APNS,
		GCM;
	}
    
}
