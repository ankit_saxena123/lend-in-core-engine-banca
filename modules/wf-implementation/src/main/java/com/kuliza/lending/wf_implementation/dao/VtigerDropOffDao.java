package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.VtigerDropoffModel;

@Repository
@Transactional
public interface VtigerDropOffDao extends JpaRepository<VtigerDropoffModel, Long> {

	public VtigerDropoffModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public VtigerDropoffModel findByProcessInstanceIdAndIsDeleted(String id, boolean isDeleted);

	public VtigerDropoffModel findByProcessInstanceId(String id);

	public List<VtigerDropoffModel> findByUserNameAndIsDeleted(String userName, boolean isDeleted);

	public VtigerDropoffModel findTopByUserNameAndVTigerParentCrmIdNotNullOrderByIdDesc(String userName);

	@Query(value= "select * from vtiger_dropoff_status where user_name = ?1 and vtiger_parent_crm_id is not null order by id desc limit 1",nativeQuery = true)
	public VtigerDropoffModel getTopParentCrmIdOfUser(String userName);
	
	public List<VtigerDropoffModel> findByStatusAndIsUpdatedInVtigerAndIsDeleted(String status,
			boolean isUpdatedInVtiger, boolean isDeleted);

	public List<VtigerDropoffModel> findByStatusAndIsUpdatedInVtiger(String status, boolean isUpdatedInVtiger);

	public List<VtigerDropoffModel> findByDropOffTypeAndIsUpdatedInVtigerAndIsDeleted(String dropOffType,
			boolean isUpdatedInVtiger, boolean isDeleted);

	public VtigerDropoffModel findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(String userName,
			String processInstanceId, String dropOffType, boolean isDeleted);
	
	@Modifying
	@Query(value = "update vtiger_dropoff_status set is_deleted=1 where date(modified) < ?1 and  is_deleted=0 ", nativeQuery = true)
	public int setIsDeletedForExpiryJourney(String expiryDate);

	public List<VtigerDropoffModel> findByUserNameAndIsUpdatedInVtigerAndDropOffTypeAndIsDeletedAndProcessInstanceIdNot(
			String userName, boolean isUpdate, String dropOffType, boolean isDeleted, String pid);

	public List<VtigerDropoffModel> findByUserNameAndIsUpdatedInVtigerAndDropOffTypeAndIsDeleted(String userName,
			boolean isUpdate, String dropOffType, boolean isDeleted);
}
