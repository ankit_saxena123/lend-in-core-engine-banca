package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class ViewPdfForm {
	
	@NotNull
	private String dmsDocId;

	public String getDmsDocId() {
		return dmsDocId;
	}

	public void setDmsDocId(String dmsDocId) {
		this.dmsDocId = dmsDocId;
	}
	

}
