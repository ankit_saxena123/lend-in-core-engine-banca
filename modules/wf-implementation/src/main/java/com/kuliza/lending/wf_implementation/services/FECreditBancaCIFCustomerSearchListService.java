package com.kuliza.lending.wf_implementation.services;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaCIFCustomerInfoBasicIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaCIFCustomerSearchListIntegration;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service
public class FECreditBancaCIFCustomerSearchListService {

	@Autowired
	private FECreditBancaCIFCustomerSearchListIntegration feCreditBancaCifCustomerSearchListIntegration;

	@Autowired
	private FECreditBancaCIFCustomerInfoBasicIntegration feCreditBancaCifCustomerInfoBasicIntegration;

	@Autowired
	private RuntimeService runtimeService;

	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaCIFCustomerSearchListService.class);

	/**
	 * This Service takes nationalId and phoneNumber as parameter return as
	 * responseMap
	 * 
	 * @param nationalId
	 * @param phoneNumber
	 * @return responseMap
	 */
	public ApiResponse cifInfoBasic(String nationalId, String phoneNumber) throws Exception {
		logger.info("-->Entering FECreditBancaCIFCustomerSearchListService.cifInfoBasic()");
		Map<String, Object> responseMap = new HashMap<String, Object>();

		HttpResponse<JsonNode> responseFromIntegrationBroker = null;
		JSONObject requestPayload = feCreditBancaCifCustomerSearchListIntegration.buildRequestPayload(nationalId,
				phoneNumber);
		logger.info("Request Payload::>>>>>>>>>>>>>>>>>>>" + requestPayload);
		responseFromIntegrationBroker = feCreditBancaCifCustomerSearchListIntegration
				.getDataFromService(requestPayload);
		logger.info("responseFromIntegrationBroker ::>>>>>>>>>>>" + responseFromIntegrationBroker.getBody());
		Map<String, Object> parseJsonObject = new HashMap<String, Object>();
		Map<String, Object> getCustomerListResponseMapObject = new HashMap<String, Object>();
		Map<String, Object> customersMapObject = new HashMap<String, Object>();
		Map<String, Object> customerMapObject = new HashMap<String, Object>();
		if (responseFromIntegrationBroker != null) {

			parseJsonObject = feCreditBancaCifCustomerSearchListIntegration
					.parseResponse(responseFromIntegrationBroker.getBody().getObject());
			if ((Boolean) parseJsonObject.get("status") == true) {
				logger.info("parseJsonObject::>>>>>>>>>>>>>>>>::::::" + parseJsonObject);

				getCustomerListResponseMapObject = (Map<String, Object>) parseJsonObject
						.get("NS1:GetCustomerListResponse");

				customersMapObject = (Map<String, Object>) getCustomerListResponseMapObject.get("Customers");
				logger.info("customersMapObject::>>>>>>>>>>>>>>>>>>>>>>>>>>>" + customersMapObject);

				customerMapObject = (Map<String, Object>) customersMapObject.get("Customer");
				String cifNumber = customerMapObject.get("CIFNumber").toString();
				logger.info("cifNumber:" + cifNumber);
				responseMap = getCustomerBasicInfo(cifNumber);
				logger.debug("responseMap ::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + responseMap);
			} else {
				return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
						parseJsonObject.get("description"));
			}
		}

		logger.info("<-- Exiting FECreditBancaCIFCustomerSearchListService.cifInfoBasic()");
		logger.info("returned response---------------->" + responseMap);
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseMap);

	}

	public DelegateExecution cifInfoBasic(DelegateExecution execution) throws Exception {
		String processInstanceId = execution.getProcessInstanceId();
		String nationalId = CommonHelperFunctions
				.getStringValueOrDefault(runtimeService.getVariable(processInstanceId, "nationalId_hyperverge"), "")
				.toString();
		String phoneNumber = CommonHelperFunctions.getStringValueOrDefault(
				runtimeService.getVariable(processInstanceId, "applicantMobileNumber_user"), "").toString();
		HttpResponse<JsonNode> responseFromIntegrationBroker = null;
		Map<String, Object> parseJsonObject = new HashMap<String, Object>();
		Map<String, Object> getCustomerListResponseMapObject = new HashMap<String, Object>();
		Map<String, Object> customersMapObject = new HashMap<String, Object>();
		Map<String, Object> customerMapObject = new HashMap<String, Object>();
		if (!nationalId.equalsIgnoreCase("") && !phoneNumber.equalsIgnoreCase("")) {
			JSONObject requestPayload = feCreditBancaCifCustomerSearchListIntegration.buildRequestPayload(nationalId,
					phoneNumber);
			responseFromIntegrationBroker = feCreditBancaCifCustomerSearchListIntegration
					.getDataFromService(requestPayload);
			logger.info("response::>>>>>>>>>>>" + responseFromIntegrationBroker.toString());
			Map<String, Object> responseMap = new HashMap<String, Object>();
			if (responseFromIntegrationBroker != null) {
				parseJsonObject = feCreditBancaCifCustomerSearchListIntegration
						.parseResponse(responseFromIntegrationBroker.getBody().getObject());
				if ((Boolean) parseJsonObject.get("status") == true) {
					logger.info("parseJsonObject::>>>>>>>>>>>>>>>>::::::" + parseJsonObject);
					getCustomerListResponseMapObject = (Map<String, Object>) parseJsonObject
							.get("NS1:GetCustomerListResponse");
					customersMapObject = (Map<String, Object>) getCustomerListResponseMapObject.get(("Customers"));
					logger.info("customersMapObject::>>>>>>>>>>>>>>>>>>>>>>>>>>>" + customersMapObject);
					customerMapObject = (Map<String, Object>) customersMapObject.get("Customer");
					String cifNumber = customerMapObject.get("CIFNumber").toString();
					logger.info("cifNumber:" + cifNumber);
					responseMap = getCustomerBasicInfo(cifNumber);
					logger.info("responseMap :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + responseMap);
					runtimeService.setVariable(processInstanceId, "nationalId_hyperverge", nationalId);
					runtimeService.setVariable(processInstanceId, "gender_hyperverge",
							responseMap.getOrDefault(Constants.CIF_GENDER_KEY, ""));
					runtimeService.setVariable(processInstanceId, "fullname_hyperverge",
							responseMap.getOrDefault(Constants.CIF_FULL_NAME_KEY, ""));
					runtimeService.setVariable(processInstanceId, "nationality_hyperverge",
							responseMap.getOrDefault(Constants.CIF_CITIZENSHIP_KEY, ""));
					runtimeService.setVariable(processInstanceId, "dob_hyperverge",
							responseMap.getOrDefault(Constants.CIF_BIRTHDAY_KEY, ""));
					runtimeService.setVariable(processInstanceId, "placeoforigin_hyperverge",
							responseMap.getOrDefault(Constants.CIF_COUNTRY_NAME_KEY, ""));
				} else {
					return (DelegateExecution) new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
							Constants.INTERNAL_SERVER_ERROR_MESSAGE, parseJsonObject.get("description"));
				}
			}
		}

		return execution;
	}

	/**
	 * This Method takes cifNumber and return parseResponse of Map
	 * 
	 * 
	 * @param cifNumber
	 * @return parseResponseMap
	 */
	private Map<String, Object> getCustomerBasicInfo(String cifNumber) {
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();
		try {

			JSONObject getRequestPayloadForCif = feCreditBancaCifCustomerInfoBasicIntegration
					.buildRequestPayload(cifNumber);
			getResponseFromIntegrationBroker = feCreditBancaCifCustomerInfoBasicIntegration
					.getDataFromService(getRequestPayloadForCif);
			JSONObject parsedResponseForCif = getResponseFromIntegrationBroker.getBody().getObject();
			parseResponseMap = feCreditBancaCifCustomerInfoBasicIntegration.parseResponse(parsedResponseForCif);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return parseResponseMap;
	}

}
