/**
 * 
 */
package com.kuliza.lending.wf_implementation.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimDocsDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimInfoDao;
import com.kuliza.lending.wf_implementation.dao.PortalDashboardIPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.ClaimDocModel;
import com.kuliza.lending.wf_implementation.models.ClaimInfoModel;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.PortalDashboardAuthenticationConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

/**
 * @author garun
 *
 */
@Service
public class PortalDashboardService {

	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private WorkFlowAddressDao workFlowAddressDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	IPATPaymentConfirmDao iPATPaymentConfirmDao;

	@Autowired
	PortalDashboardIPATPaymentConfirmDao PortalDashboardIPATPaymentConfirmDao;

	@Autowired
	PortalClaimInfoDao portalClaimInfoDao;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private PortalClaimDocsDao portalClaimDocsDao;

	private static final Logger logger = LoggerFactory.getLogger(PortalDashboardService.class);

	/**
	 * Customer Dashboard
	 */
	public ApiResponse getAllCustomers(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardService.getAllCustomer()");
		Pageable paging = new PageRequest(pageNo, pageSize);
		List<WorkFlowUser> workFlowUser = (List<WorkFlowUser>) workFlowUserDao.findAllByIsDeleted(paging, false);
		List<WorkFlowUser> countRecords = (List<WorkFlowUser>) workFlowUserDao.findAllByIsDeleted(false);
		List<Map<String, Object>> response = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

		Map<String, Object> totalRecords = new HashMap<String, Object>();
		if (workFlowUser != null) {

			for (WorkFlowUser wfCustomer : workFlowUser) {
				Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
				Date date = wfCustomer.getModified();
				String strDate = dateFormat.format(date);
				String splitDateTime[] = strDate.split(" ");
				workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(wfCustomer.getId()));
				String customerAction = "getCustomerInfo/" + Utils.getStringValue(wfCustomer.getId());
				workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(wfCustomer.getUsername()));
				workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER,
						Utils.getStringValue(wfCustomer.getMobileNumber()));
				workFlowUserMap.put(Constants.CUSTOMER_LAST_UPDATE_DATE, Utils.getStringValue(splitDateTime[0]));
				workFlowUserMap.put(Constants.CUSTOMER_LAST_UPDATE_TIME, Utils.getStringValue(splitDateTime[1]));
				workFlowUserMap.put(Constants.CUSTOMER_EMAIL, Utils.getStringValue(wfCustomer.getEmail()));
				workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID, Utils.getStringValue(wfCustomer.getNationalId()));
				workFlowUserMap.put(Constants.ACTION, customerAction);
				response.add(workFlowUserMap);
			}
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(countRecords.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardService.getAllCustomer()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	/**
	 * @Descritpion to get customer details as per csutomer id
	 * @param customerId
	 * @return
	 */

	public ApiResponse getCustomerInfoDetails(long customerId, String policyNumber) {
		logger.info("-->Entering PortalDashboardService.getCustomerInfoDetails()");
		WorkFlowUser workFlowUser = null;
		if (!policyNumber.isEmpty()) {
			WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO.findByPolicyNumberAndIsDeleted(policyNumber, false);
			WorkFlowUserApplication wfUserApplication = workflowUserApplicationDao
					.findById(workFlowPolicy.getWorkFlowApplication().getId());
			workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(wfUserApplication.getUserIdentifier(), false);

		} else {
			workFlowUser = workFlowUserDao.findByIdAndIsDeleted(customerId, false);

		}
		List<WorkFlowUserAddress> workFlowUserAddress = workFlowAddressDao.findByWfUser(workFlowUser);
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		List<Map<String, Object>> address = new ArrayList<>();
		if (workFlowUser != null) {
			Map<String, Object> customerAddress = new HashMap<String, Object>();

			workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workFlowUser.getId()));
			workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(workFlowUser.getUsername()));
			workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID, Utils.getStringValue(workFlowUser.getNationalId()));
			workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER, Utils.getStringValue(workFlowUser.getMobileNumber()));
			workFlowUserMap.put(Constants.CUSTOMER_GENDER, Utils.getStringValue(workFlowUser.getGender()));
			// if address will null then need to send atleast keay as response
			if (workFlowUserAddress != null) {
				if (workFlowUserAddress.size() > 0) {
					for (WorkFlowUserAddress wfCustomer : workFlowUserAddress) {
						if (!policyNumber.isEmpty()) {
							workFlowUserMap.put(Constants.CUSTOMER_DOB, Utils.getStringValue(workFlowUser.getDob()));
						}
						String policyCustomerAddress = Utils.getStringValue(wfCustomer.getAddressLine1())
								+ Utils.getStringValue(wfCustomer.getAddressLine2());
						customerAddress.put(Constants.POLICY_CUSTOMER_ADDRESS, policyCustomerAddress);
						customerAddress.put(Constants.CUSTOMER_ADDRESS_PROVINCE,
								Utils.getStringValue(wfCustomer.getProvince()));
						customerAddress.put(Constants.CUSTOMER_ADDRESS_CITY,
								Utils.getStringValue(wfCustomer.getCityDistrict()));
						customerAddress.put(Constants.CUSTOMER_ADDRESS_STATE,
								Utils.getStringValue(wfCustomer.getState()));
						customerAddress.put(Constants.CUSTOMER_ADDRESS_COUNTRY,
								Utils.getStringValue(wfCustomer.getState()));
					}
				} else {
					workFlowUserMap.put(Constants.CUSTOMER_DOB, "");
					customerAddress.put(Constants.POLICY_CUSTOMER_ADDRESS, "");
					customerAddress.put(Constants.CUSTOMER_ADDRESS_PROVINCE, "");
					customerAddress.put(Constants.CUSTOMER_ADDRESS_CITY, "");
					customerAddress.put(Constants.CUSTOMER_ADDRESS_STATE, "");
					customerAddress.put(Constants.CUSTOMER_ADDRESS_COUNTRY, "");
				}
			}
			address.add(customerAddress);
			workFlowUserMap.put(Constants.CUSTOMER_ADDRESS, address);
			workFlowUserMap.put(Constants.CUSTOMER_EMAIL, Utils.getStringValue(workFlowUser.getEmail()));

			logger.info("<-- Exiting PortalDashboardService.getCustomerInfoDetails()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);

		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	/**
	 * 
	 * @Description This method return the details list of policies and claims
	 *              purchased by particular customer
	 * @return
	 */
	public ApiResponse getClaimsAndPolicyDetailsDetails(long CustomerId, String customer_policy_response_identifier) {
		logger.info("-->Entering PortalDashboardService.getAllPolicies()");
		Calendar cal = Calendar.getInstance();
		WorkFlowUser workFlowUser = workFlowUserDao.findById(CustomerId);
		List<Map<String, Object>> response = new ArrayList<>();
		int slno = 0;
		if (workFlowUser != null) {
			Map<String, Object> totalRecords = new HashMap<String, Object>();
			List<WorkFlowUserApplication> workFlowAppList = workflowUserApplicationDao
					.findByUserIdentifierAndIsDeleted(workFlowUser.getIdmUserName(), false);
			if (workFlowAppList != null) {
				for (WorkFlowUserApplication workFlowUserApp : workFlowAppList) {
					List<WorkFlowPolicy> policylists = workFlowPolicyDAO
							.findByWorkFlowApplicationAndIsDeleted(workFlowUserApp.getWfApplication(), false);
					if (policylists != null) {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						for (WorkFlowPolicy policylist : policylists) {
							WorkFlowApplication workFloweApplication = workflowApplicationDao
									.findById(policylist.getWorkFlowApplication().getId());
							ClaimInfoModel claimInfo = portalClaimInfoDao.findByClaimsid(policylist.getClaimNumber());
							if (!customer_policy_response_identifier.isEmpty() && customer_policy_response_identifier
									.equalsIgnoreCase(Constants.CUSTOMER_POLICY_INFO_RESPONSE_IDENTIFIER)) {
								// Generate Customer policy information response
								workFlowUserMap.put(Constants.SERIAL_NO, slno);
								workFlowUserMap.put(Constants.POLICY_PRODUCT_NAME,
										Utils.getStringValue(workFloweApplication.getProductName()));
								workFlowUserMap.put(Constants.CLAIMS_APPLICATION_NUMBER,
										Utils.getStringValue(workFloweApplication.getApplicationId()));
								workFlowUserMap.put(Constants.PLAN_ID_KEY,
										Utils.getStringValue(policylist.getPlanId()));
								workFlowUserMap.put(Constants.POLICY_NUMBER,
										Utils.getStringValue(policylist.getPolicyNumber()));

								workFlowUserMap.put(Constants.POLICY_PREMIUM_AMOUNT,
										Utils.getStringValue(policylist.getMontlyPayment()));
								workFlowUserMap.put(Constants.POLICY_COVERAGE_AMOUNT,
										Utils.getStringValue(policylist.getProtectionAmount()));
								workFlowUserMap.put(Constants.POLICY_DOCUMENT, "PDF");
								workFlowUserMap.put(Constants.PORTAL_POLICY_START_DATE,
										Utils.getStringValue(policylist.getPurchasedDate()));
								workFlowUserMap.put(Constants.PORTAL_POLICY_END_DATE,
										Utils.getStringValue(policylist.getPlanExpiringDate()));

								String ploicy_renewal_date = "";
								String startDate_String = policylist.getPurchasedDate();
								String planExpiryDate = policylist.getPlanExpiringDate();
								if ((startDate_String != null && startDate_String != "")) {
									ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(startDate_String,
											"start_date_exist");
								} else {
									if (planExpiryDate != null && planExpiryDate != "") {
										ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(planExpiryDate,
												"exipry_date_exist");
									}
								}

								workFlowUserMap.put(Constants.POLICY_RENEWAL_DATE,
										Utils.getStringValue(ploicy_renewal_date));
								// Possible to get more than one transaction records
								List<IPATPaymentConfirmModel> payTransactionInfo = PortalDashboardIPATPaymentConfirmDao
										.findByWfApplicationAndIsDeleted(workFloweApplication, false);
								if (payTransactionInfo != null) {
									if (payTransactionInfo.size() > 0) {
										for (IPATPaymentConfirmModel iPATransactionInfo : payTransactionInfo) {
											workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID,
													Utils.getStringValue(iPATransactionInfo.getPayooTransactionId()));
											workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "Payoo");
											boolean tranasctionStatus = iPATransactionInfo.isPaymentConfirmStatus();
											if (!tranasctionStatus) {
												workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Fail..!!");
											} else {
												workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS,
														"Success..!!");
											}
										}
									} else {
										workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID, "");
										workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "");
										workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "");
									}
								}
								workFlowUserMap.put(Constants.POLICY_CLAIMS_DETAILS, "View");
								workFlowUserMap.put(Constants.POLICY_INSURER,
										Utils.getStringValue(policylist.getInsurer()));
								response.add(workFlowUserMap);

							} else {
								// Generate Customer claim information response
								workFlowUserMap.put(Constants.SERIAL_NO, slno);
								workFlowUserMap.put(Constants.POLICY_PRODUCT_NAME,
										Utils.getStringValue(workFloweApplication.getProductName()));
								workFlowUserMap.put(Constants.PLAN_ID_KEY,
										Utils.getStringValue(policylist.getPlanId()));
								workFlowUserMap.put(Constants.POLICY_NUMBER,
										Utils.getStringValue(policylist.getPolicyNumber()));
								workFlowUserMap.put(Constants.CLAIMS_ID,
										Utils.getStringValue(workFloweApplication.getApplicationId()));

								workFlowUserMap.put(Constants.CLAIMS_APPLICATION_NUMBER,
										Utils.getStringValue(workFloweApplication.getApplicationId()));

								workFlowUserMap.put(Constants.POLICY_CLAIMS_DETAILS, "PDF");

								boolean claimStatus = workFloweApplication.isApplicationCompleted();
								// Need to change the message
								if (!claimStatus) {
									workFlowUserMap.put(Constants.CLAIMS_STATUS, "Fail..!!");
								} else {
									workFlowUserMap.put(Constants.CLAIMS_STATUS, "Success..!!");
								}
								if (claimInfo.getClaimAmount().isEmpty()) {
									workFlowUserMap.put(Constants.CLAIMS_AMOUNT, Utils.getStringValue("0"));
								} else {
									workFlowUserMap.put(Constants.CLAIMS_AMOUNT,
											Utils.getStringValue(claimInfo.getClaimAmount()));
								}

							}
							slno = slno + 1;
						}
					}

				}
			}

			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(slno)));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardService.getAllPolicies()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}

	}

	/**
	 * Policy Dashboard
	 * 
	 */

	public ApiResponse getAllPolicies(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardService.getAllPolicies()");
		Pageable paging = new PageRequest(pageNo, pageSize);
		List<WorkFlowPolicy> policylists = workFlowPolicyDAO.findAllByIsDeleted(paging, false);
		List<WorkFlowPolicy> totalRecords = workFlowPolicyDAO.findAllByIsDeleted(false);
		List<Map<String, Object>> response = new ArrayList<>();

		int slno = 1;
		if (policylists != null) {
			Map<String, Object> addTotalRecords = new HashMap<String, Object>();
			for (WorkFlowPolicy policylist : policylists) {
				WorkFlowUserApplication workflowuserapplication = workflowUserApplicationDao
						.findById(policylist.getWorkFlowApplication().getId());
				if (workflowuserapplication != null) {
					WorkFlowUser workFlowUser = workFlowUserDao
							.findByIdmUserNameAndIsDeleted(workflowuserapplication.getUserIdentifier(), false);
					if (workFlowUser != null) {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						workFlowUserMap.put(Constants.SERIAL_NO, slno);
						workFlowUserMap.put(Constants.POLICY_NUMBER,
								Utils.getStringValue(policylist.getPolicyNumber()));
						workFlowUserMap.put(Constants.PLAN_ID_KEY, Utils.getStringValue(policylist.getPlanId()));
						workFlowUserMap.put(Constants.PLAN_NAME, Utils.getStringValue(policylist.getPlanName()));

						workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workFlowUser.getId()));
						workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(workFlowUser.getUsername()));
						workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER,
								Utils.getStringValue(workFlowUser.getMobileNumber()));
						workFlowUserMap.put(Constants.CUSTOMER_EMAIL, Utils.getStringValue(workFlowUser.getEmail()));
						workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID,
								Utils.getStringValue(workFlowUser.getNationalId()));
						String customerAction = "getCustomerInfo/" + Utils.getStringValue(workFlowUser.getId());
						workFlowUserMap.put(Constants.CUSTOMER_DETAILS, customerAction);
						workFlowUserMap.put(Constants.POLICY_DOCUMENT, "View");

						workFlowUserMap.put(Constants.PORTAL_POLICY_START_DATE,
								Utils.getStringValue(policylist.getPurchasedDate()));
						workFlowUserMap.put(Constants.PORTAL_POLICY_END_DATE,
								Utils.getStringValue(policylist.getPlanExpiringDate()));
						String ploicy_renewal_date = "";
						String startDate_String = policylist.getPurchasedDate();
						String planExpiryDate = policylist.getPlanExpiringDate();
						if ((startDate_String != null && startDate_String != "")) {
							ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(startDate_String, "start_date_exist");
						} else {
							if (planExpiryDate != null && planExpiryDate != "") {
								ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(planExpiryDate,
										"exipry_date_exist");
							}
						}
						workFlowUserMap.put(Constants.POLICY_RENEWAL_DATE, Utils.getStringValue(ploicy_renewal_date));
						workFlowUserMap.put(Constants.POLICY_INSURER, Utils.getStringValue(policylist.getInsurer()));
						workFlowUserMap.put(Constants.POLICY_SUM_ASSURED,
								Utils.getStringValue(policylist.getProtectionAmount()));

						response.add(workFlowUserMap);
						slno = slno + 1;
					}
				}

			}
			addTotalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(totalRecords.size())));
			response.add(addTotalRecords);
			logger.info("<-- Exiting PortalDashboardService.getAllPolicies()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}

	}

	public ApiResponse getPolicyDetails(String policy_number) {
		logger.info("-->Entering PortalDashboardService.getPolicyDetails()");
		WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO.findByPolicyNumberAndIsDeleted(policy_number, false);
		WorkFlowApplication wFApplication = workflowApplicationDao
				.findById(workFlowPolicy.getWorkFlowApplication().getId());
		List<Map<String, Object>> response = new ArrayList<>();

		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		if (workFlowPolicy != null && workFlowPolicy != null) {
			workFlowUserMap.put(Constants.POLICY_NUMBER, Utils.getStringValue(workFlowPolicy.getPolicyNumber()));
			workFlowUserMap.put(Constants.PLAN_NAME, Utils.getStringValue(workFlowPolicy.getPlanName()));
			workFlowUserMap.put(Constants.POLICY_INSURER, Utils.getStringValue(wFApplication.getInsurer()));
			workFlowUserMap.put(Constants.POLICY_SUM_ASSURED,
					Utils.getStringValue(workFlowPolicy.getProtectionAmount()));
			workFlowUserMap.put(Constants.PORTAL_POLICY_START_DATE,
					Utils.getStringValue(workFlowPolicy.getPurchasedDate()));
			workFlowUserMap.put(Constants.POLICY_NEXT_INSTALLMENT_DATE,
					Utils.getStringValue(workFlowPolicy.getNextPaymentDue()));
			workFlowUserMap.put(Constants.PORTAL_POLICY_END_DATE,
					Utils.getStringValue(workFlowPolicy.getPlanExpiringDate()));

			String ploicy_renewal_date = "";
			String startDate_String = workFlowPolicy.getPurchasedDate();
			String planExpiryDate = workFlowPolicy.getPlanExpiringDate();
			if ((startDate_String != null && startDate_String != "")) {
				ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(startDate_String, "start_date_exist");
			} else {
				if (planExpiryDate != null && planExpiryDate != "") {
					ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(planExpiryDate, "exipry_date_exist");
				}
			}
			workFlowUserMap.put(Constants.POLICY_RENEWAL_DATE, Utils.getStringValue(ploicy_renewal_date));
			workFlowUserMap.put(Constants.POLICY_PRODUCT_NAME, Utils.getStringValue(wFApplication.getProductName()));
			workFlowUserMap.put(Constants.POLICY_PREMIUM_AMOUNT, Utils.getStringValue("10000000"));
			response.add(workFlowUserMap);

			logger.info("<-- Exiting PortalDashboardService.getPolicyDetails()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);

		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse getTransactionInfo(String transactionId, String policyNumber) {
		logger.info("-->Entering PortalDashboardService.getTransactionInfo()");
		IPATPaymentConfirmModel payTransactionInfo = null;
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		if (!policyNumber.isEmpty()) {
			WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO.findByPolicyNumberAndIsDeleted(policyNumber, false);
			WorkFlowApplication workFlowApp = workflowApplicationDao
					.findById(workFlowPolicy.getWorkFlowApplication().getId());
			payTransactionInfo = iPATPaymentConfirmDao.findByWfApplicationAndIsDeleted(workFlowApp, false);
			if (payTransactionInfo != null && payTransactionInfo != null) {
				workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID,
						Utils.getStringValue(payTransactionInfo.getPayooTransactionId()));
				workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "Payoo");
				boolean tranasctionStatus = payTransactionInfo.isPaymentConfirmStatus();
				if (!tranasctionStatus) {
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Fail..!!");
				} else {
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Success..!!");
				}
				workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_DATE,
						Utils.getStringValue(payTransactionInfo.getPaymentDate()));
				logger.info("<-- Exiting PortalDashboardService.getTransactionInfo()");
			}

		}
//		transaction details from Transaction List
		if (!transactionId.isEmpty()) {
			WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO.findByTransactionIdAndIsDeleted(transactionId, false);
			if (workFlowPolicy != null) {
				WorkFlowApplication workFlowApp = workflowApplicationDao
						.findById(workFlowPolicy.getWorkFlowApplication().getId());
				payTransactionInfo = iPATPaymentConfirmDao.findByWfApplicationAndIsDeleted(workFlowApp, false);
				WorkFlowUserApplication workFlowUserApp = workflowUserApplicationDao
						.findById(workFlowPolicy.getWorkFlowApplication().getId());

				WorkFlowUser workflowuser = workFlowUserDao
						.findByIdmUserNameAndIsDeleted(workFlowUserApp.getUserIdentifier(), false);
				// Transaction Information
				workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID,
						Utils.getStringValue(workFlowPolicy.getTransactionId()));
				workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "Payoo");
				boolean tranasctionStatus = payTransactionInfo.isPaymentConfirmStatus();
				if (!tranasctionStatus) {
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Fail..!!");
				} else {
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Success..!!");
				}
				workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_DATE,
						Utils.getStringValue(payTransactionInfo.getPaymentDate()));
				workFlowUserMap.put(Constants.POLICY_NEXT_INSTALLMENT_DATE,
						Utils.getStringValue(workFlowPolicy.getNextPaymentDue()));
				// Product Information
				workFlowUserMap.put(Constants.PLAN_NAME, Utils.getStringValue(workFlowPolicy.getPlanName()));
				workFlowUserMap.put(Constants.POLICY_DOCUMENT, "View");
				workFlowUserMap.put(Constants.PLAN_ID_KEY, Utils.getStringValue(workFlowPolicy.getPlanId()));
				workFlowUserMap.put(Constants.PORTAL_POLICY_START_DATE,
						Utils.getStringValue(workFlowPolicy.getPurchasedDate()));
				workFlowUserMap.put(Constants.PORTAL_POLICY_END_DATE,
						Utils.getStringValue(workFlowPolicy.getPlanExpiringDate()));
				String ploicy_renewal_date = "";
				String startDate_String = workFlowPolicy.getPurchasedDate();
				String planExpiryDate = workFlowPolicy.getPlanExpiringDate();
				if ((startDate_String != null && startDate_String != "")) {
					ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(startDate_String, "start_date_exist");
				} else {
					if (planExpiryDate != null && planExpiryDate != "") {
						ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(planExpiryDate, "exipry_date_exist");
					}
				}
				workFlowUserMap.put(Constants.POLICY_RENEWAL_DATE, Utils.getStringValue(ploicy_renewal_date));
				// Customer Information
				workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workflowuser.getId()));
				workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(workflowuser.getUsername()));

				workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID, Utils.getStringValue(workflowuser.getNationalId()));
				workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER,
						Utils.getStringValue(workflowuser.getMobileNumber()));
				workFlowUserMap.put(Constants.CUSTOMER_GENDER, Utils.getStringValue(workflowuser.getGender()));
				// Add address which can be more than one for one customer
				List<Map<String, Object>> address = new ArrayList<>();
				Map<String, Object> customerAddress = new HashMap<String, Object>();
				List<WorkFlowUserAddress> workFlowUserAddress = workFlowAddressDao.findByWfUser(workflowuser);
				if (workflowuser != null && workFlowUserAddress != null) {
					for (WorkFlowUserAddress wfCustomer : workFlowUserAddress) {

						String policyCustomerAddress = Utils.getStringValue(wfCustomer.getAddressLine1())
								+ Utils.getStringValue(wfCustomer.getAddressLine2());
						customerAddress.put(Constants.POLICY_CUSTOMER_ADDRESS, policyCustomerAddress);
						customerAddress.put(Constants.CUSTOMER_ADDRESS_PROVINCE,
								Utils.getStringValue(wfCustomer.getProvince()));
						customerAddress.put(Constants.CUSTOMER_ADDRESS_CITY,
								Utils.getStringValue(wfCustomer.getCityDistrict()));
						customerAddress.put(Constants.CUSTOMER_ADDRESS_STATE,
								Utils.getStringValue(wfCustomer.getState()));
					}
					address.add(customerAddress);
				}
				workFlowUserMap.put(Constants.CUSTOMER_ADDRESS, address);
				logger.info("<-- Exiting PortalDashboardService.getAllTransaction()");
			}

		}

		if (workFlowUserMap != null) {
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	/**
	 * Transaction Dashboard
	 */

	public ApiResponse getAllTransaction(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardService.getAllTransaction()");
		Pageable paging = new PageRequest(pageNo, pageSize);
		List<IPATPaymentConfirmModel> ipatPaymentConfirmTransactions = (List<IPATPaymentConfirmModel>) PortalDashboardIPATPaymentConfirmDao
				.findAllByIsDeleted(paging, false);
		List<IPATPaymentConfirmModel> countRecords = (List<IPATPaymentConfirmModel>) PortalDashboardIPATPaymentConfirmDao
				.findAllByIsDeleted(false);
		List<Map<String, Object>> response = new ArrayList<>();
		Map<String, Object> totalRecords = new HashMap<String, Object>();
		if (ipatPaymentConfirmTransactions != null) {
			for (IPATPaymentConfirmModel ipatPaymentConfirm : ipatPaymentConfirmTransactions) {
				Map<String, Object> workFlowUserMap = new HashMap<String, Object>();

				List<WorkFlowPolicy> workFlowPolicies = workFlowPolicyDAO
						.findByWorkFlowApplicationAndIsDeleted(ipatPaymentConfirm.getWfApplication(), false);
				if (workFlowPolicies != null) {
					for (WorkFlowPolicy workFlowPolicy : workFlowPolicies) {
						WorkFlowUserApplication workflowuserapplication = workflowUserApplicationDao
								.findById(workFlowPolicy.getWorkFlowApplication().getId());
						if (workflowuserapplication != null) {
							WorkFlowUser workFlowUser = workFlowUserDao
									.findByIdmUserNameAndIsDeleted(workflowuserapplication.getUserIdentifier(), false);
							workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID,
									Utils.getStringValue(workFlowPolicy.getTransactionId()));
							workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "Payoo");
							boolean tranasctionStatus = ipatPaymentConfirm.isPaymentConfirmStatus();
							if (!tranasctionStatus) {
								workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Fail..!!");
							} else {
								workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Success..!!");
							}
							workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_DATE,
									Utils.getStringValue(ipatPaymentConfirm.getPaymentDate()));

							workFlowUserMap.put(Constants.PLAN_ID_KEY,
									Utils.getStringValue(workFlowPolicy.getPlanId()));
							if (workFlowUser != null) {
								workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workFlowUser.getId()));
								workFlowUserMap.put(Constants.CUSTOMER_NAME,
										Utils.getStringValue(workFlowUser.getUsername()));
							} else {
								workFlowUserMap.put(Constants.CUSTOMER_ID, "");
								workFlowUserMap.put(Constants.CUSTOMER_NAME, "");
							}
							workFlowUserMap.put(Constants.PLAN_NAME,
									Utils.getStringValue(workFlowPolicy.getPlanName()));
							workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_AMOUNT,
									Utils.getStringValue(ipatPaymentConfirm.getPaymentAmount()));

							workFlowUserMap.put(Constants.POLICY_DOCUMENT, "View");
							workFlowUserMap.put(Constants.TRANSACTION_DETAILS, "View");

							workFlowUserMap.put(Constants.TRANSACTION_REMARKS,
									Utils.getStringValue(ipatPaymentConfirm.getDescription()));
							workFlowUserMap.put(Constants.POLICY_NUMBER,
									Utils.getStringValue(workFlowPolicy.getPolicyNumber()));
						}

					}

				}

				response.add(workFlowUserMap);
			}
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(countRecords.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardService.getAllTransaction()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

//Claims Dashboard
	/**
	 * Customer Dashboard
	 */
	@SuppressWarnings("unlikely-arg-type")
	public ApiResponse getAllClaims(Integer pageNo, Integer pageSize) {
		logger.info("-->Entering PortalDashboardService.getAllClaims()");
		Pageable paging = new PageRequest(pageNo, pageSize);
		List<ClaimDocModel> claimList = portalClaimDocsDao.findAllByIsDeleted(paging, false);
		List<ClaimDocModel> countRecords = portalClaimDocsDao.findAllByIsDeleted(false);
		Map<String, Object> variables = null;
		Set<Map<String, Object>> response = new HashSet<>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Map<String, Object> totalRecords = new HashMap<String, Object>();
		if (claimList.size() > 0) {
			for (ClaimDocModel claim : claimList) {

				try {
					variables = runtimeService.getVariables(claim.getProcessInstanceId());

				} catch (Exception ex) {
					logger.info("<==Claim getAllClaims occur while get process variable==>" + ex);
					HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(claim.getProcessInstanceId()).includeProcessVariables().singleResult();
					variables = processInstance.getProcessVariables();
				}
				if (variables != null) {
					String policyNo = Utils.getStringValue(
							variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER));
					List<ClaimInfoModel> claimInfs = portalClaimInfoDao.findByPolicyNo(policyNo);

					if (claimInfs.size() > 0) {
						for (ClaimInfoModel claimInf : claimInfs) {
							Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_APPLICATION_NO,
									Utils.getStringValue(variables
											.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_APPLICATION_NUMBER)));
							if (claimInf != null) {
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID,
										Utils.getStringValue(claimInf.getClaimsid()));

								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
										Utils.getStringValue(claimInf.getClaimStatus()));
							} else {
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID,
										Utils.getStringValue(""));

								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
										Utils.getStringValue("0"));
							}

							if (!Utils.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT))
									.isEmpty()) {
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT, Utils
										.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT)));
							} else {
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
										Utils.getStringValue("0"));
							}
							workFlowUserMap.put(Constants.POLICY_NUMBER, Utils.getStringValue(
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER)));

							workFlowUserMap.put(Constants.PRODUCT_NAME,
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PRODUCT_NAME));
							workFlowUserMap.put(Constants.PLAN_NAME,
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PLAN_NAME));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_PROCESS_INSTANCE_ID,
									Utils.getStringValue(claim.getProcessInstanceId()));

							Date date = claim.getModified();
							String strDate = dateFormat.format(date);
							String splitDateTime[] = strDate.split(" ");
							String last_update = splitDateTime[0] + splitDateTime[0];
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_LAST_UPDATE,
									Utils.getStringValue(last_update));
							String action = "getClaimDetails/" + Utils.getStringValue(claim.getProcessInstanceId());
							workFlowUserMap.put(Constants.ACTION, action);
							response.add(workFlowUserMap);
						}
					} else {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_APPLICATION_NO,
								Utils.getStringValue(variables
										.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_APPLICATION_NUMBER)));

						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID, Utils.getStringValue(""));

						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
								Utils.getStringValue("0"));

						if (!Utils.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT))
								.isEmpty()) {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
									Utils.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT)));
						} else {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
									Utils.getStringValue("0"));
						}
						workFlowUserMap.put(Constants.POLICY_NUMBER, Utils.getStringValue(
								variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER)));

						workFlowUserMap.put(Constants.PRODUCT_NAME,
								variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PRODUCT_NAME));
						workFlowUserMap.put(Constants.PLAN_NAME,
								variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PLAN_NAME));
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_PROCESS_INSTANCE_ID,
								Utils.getStringValue(claim.getProcessInstanceId()));

						Date date = claim.getModified();
						String strDate = dateFormat.format(date);
						String splitDateTime[] = strDate.split(" ");
						String last_update = splitDateTime[0] + splitDateTime[0];
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_LAST_UPDATE,
								Utils.getStringValue(last_update));
						String action = "getClaimDetails/" + Utils.getStringValue(claim.getProcessInstanceId());
						workFlowUserMap.put(Constants.ACTION, action);
						response.add(workFlowUserMap);
					}
				}

			}
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(countRecords.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardService.getAllClaims()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	// Claim Details start..
	public ApiResponse getClaimCustomerDetails(String processInstanceId) {
		logger.info("-->Entering PortalDashboardService.getTransactionInfo()");
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Map<String, Object> variables = null;
		Set<Map<String, Object>> response = new HashSet<>();
		if (!processInstanceId.isEmpty()) {
			try {
				variables = runtimeService.getVariables(processInstanceId);

			} catch (Exception ex) {
				logger.info("<==Claim saveDocs occur while get process variable==>" + ex);
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
				variables = processInstance.getProcessVariables();
			}
			if (variables.size() > 0) {
				List<ClaimInfoModel> claimCustomerDetaillist = portalClaimInfoDao
						.findByProcessInstanceId(processInstanceId);
				if (claimCustomerDetaillist != null) {
					for (ClaimInfoModel claimCustomerDetails : claimCustomerDetaillist) {
						WorkFlowUser workflowuser = workFlowUserDao.findByIdmUserNameAndIsDeleted(
								Utils.getStringValue(
										variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_ASSIGNEE)),
								false);
						workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workflowuser.getId()));
						workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(workflowuser.getUsername()));
						workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID,
								Utils.getStringValue(workflowuser.getNationalId()));
						workFlowUserMap.put(Constants.CUSTOMER_EMAIL, Utils.getStringValue(workflowuser.getEmail()));
						workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER,
								Utils.getStringValue(workflowuser.getMobileNumber()));
						workFlowUserMap.put(Constants.CUSTOMER_DOB, Utils.getStringValue(workflowuser.getDob()));
						workFlowUserMap.put(Constants.CUSTOMER_GENDER, Utils.getStringValue(workflowuser.getGender()));
						List<Map<String, Object>> address = new ArrayList<>();
						Map<String, Object> customerAddress = new HashMap<String, Object>();
						List<WorkFlowUserAddress> workFlowUserAddress = workFlowAddressDao.findByWfUser(workflowuser);
						if (workflowuser != null && workFlowUserAddress != null) {
							for (WorkFlowUserAddress wfCustomer : workFlowUserAddress) {

								String claimCustomerAddress = Utils.getStringValue(wfCustomer.getAddressLine1())
										+ Utils.getStringValue(wfCustomer.getAddressLine2());
								customerAddress.put(Constants.POLICY_CUSTOMER_ADDRESS, claimCustomerAddress);
								customerAddress.put(Constants.CUSTOMER_ADDRESS_PROVINCE,
										Utils.getStringValue(wfCustomer.getProvince()));
								customerAddress.put(Constants.CUSTOMER_ADDRESS_CITY,
										Utils.getStringValue(wfCustomer.getCityDistrict()));
								customerAddress.put(Constants.CUSTOMER_ADDRESS_STATE,
										Utils.getStringValue(wfCustomer.getState()));
							}
							address.add(customerAddress);
						}
						workFlowUserMap.put(Constants.CUSTOMER_ADDRESS, address);
						Date date = claimCustomerDetails.getModified();
						String strDate = dateFormat.format(date);
						String splitDateTime[] = strDate.split(" ");
						String last_update = splitDateTime[0] + " " + splitDateTime[1];
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_LAST_UPDATE,
								Utils.getStringValue(last_update));
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_LAST_ACTIVE_ON,
								Utils.getStringValue(claimCustomerDetails.getModified()));
						response.add(workFlowUserMap);
						logger.info("<-- Exiting PortalDashboardService.getAllTransaction()");
					}

				}

			}
		}

		if (workFlowUserMap != null) {
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse getClaimPolicyDetails(String processInstanceId) {
		logger.info("-->Entering PortalDashboardService.getTransactionInfo()");
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		Map<String, Object> variables = null;
		if (!processInstanceId.isEmpty()) {
			try {
				variables = runtimeService.getVariables(processInstanceId);

			} catch (Exception ex) {
				logger.info("<==Claim saveDocs occur while get process variable==>" + ex);
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
				variables = processInstance.getProcessVariables();
			}
			if (variables.size() > 0) {
				List<ClaimInfoModel> claimCustomerDetaillist = portalClaimInfoDao
						.findByProcessInstanceId(processInstanceId);

				if (claimCustomerDetaillist != null) {
					for (ClaimInfoModel claimCustomerDetails : claimCustomerDetaillist) {
						List<WorkFlowUserApplication> workFlowAppList = workflowUserApplicationDao
								.findByUserIdentifierAndIsDeleted(
										Utils.getStringValue(variables
												.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_ASSIGNEE)),
										false);
						if (workFlowAppList != null) {
							for (WorkFlowUserApplication workFlowUserApp : workFlowAppList) {
								List<WorkFlowPolicy> claimPolicylist = workFlowPolicyDAO
										.findByWorkFlowApplicationAndIsDeleted(workFlowUserApp.getWfApplication(),
												false);

								if (claimPolicylist.size() > 0) {
									for (WorkFlowPolicy workFlowPolicy : claimPolicylist) {
										if (workFlowPolicy != null) {
											WorkFlowApplication workFlowApp = workflowApplicationDao
													.findById(workFlowPolicy.getWorkFlowApplication().getId());

											workFlowUserMap.put(
													PortalDashboardAuthenticationConstants.CLAIM_APPLICATION_NO,
													Utils.getStringValue(variables.get(
															PortalDashboardAuthenticationConstants.CLAIM_VAR_APPLICATION_NUMBER)));

											if (!Utils
													.getStringValue(
															variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT))
													.isEmpty()) {
												workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
														Utils.getStringValue(variables
																.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT)));
											} else {
												workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
														Utils.getStringValue("0"));
											}
											workFlowUserMap.put(Constants.POLICY_NUMBER, Utils.getStringValue(variables
													.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER)));

											workFlowUserMap.put(Constants.PRODUCT_NAME, variables.get(
													PortalDashboardAuthenticationConstants.CLAIM_VAR_PRODUCT_NAME));
											workFlowUserMap.put(Constants.PLAN_NAME, variables
													.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PLAN_NAME));
											workFlowUserMap.put(
													PortalDashboardAuthenticationConstants.CLAIM_PROCESS_INSTANCE_ID,
													Utils.getStringValue(processInstanceId));

											workFlowUserMap.put(
													PortalDashboardAuthenticationConstants.CLAIM_POLICY_COVERAGE_AMOUNT,
													variables.get(
															PortalDashboardAuthenticationConstants.CLAIM_VAR_PROTECTION_AMOUNT));
											workFlowUserMap.put(Constants.PORTAL_POLICY_START_DATE,
													Utils.getStringValue(workFlowPolicy.getPurchasedDate()));
											workFlowUserMap.put(Constants.PORTAL_POLICY_END_DATE,
													Utils.getStringValue(workFlowPolicy.getPlanExpiringDate()));
											String ploicy_renewal_date = "";
											String startDate_String = workFlowPolicy.getPurchasedDate();
											String planExpiryDate = workFlowPolicy.getPlanExpiringDate();
											if ((startDate_String != null && startDate_String != "")) {
												ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(startDate_String,
														"start_date_exist");
											} else {
												if (planExpiryDate != null && planExpiryDate != "") {
													ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(planExpiryDate,
															"exipry_date_exist");
												}
											}
											workFlowUserMap.put(Constants.POLICY_RENEWAL_DATE,
													Utils.getStringValue(ploicy_renewal_date));
											workFlowUserMap.put(Constants.POLICY_NEXT_INSTALLMENT_DATE,
													Utils.getStringValue(workFlowPolicy.getNextPaymentDue()));
											workFlowUserMap.put(Constants.POLICY_INSURER,
													Utils.getStringValue(workFlowPolicy.getInsurer()));
											workFlowUserMap.put(Constants.POLICY_PREMIUM_AMOUNT,
													Utils.getStringValue(workFlowPolicy.getMontlyPayment()));

											WorkFlowApplication workFloweApplication = workflowApplicationDao
													.findById(workFlowPolicy.getWorkFlowApplication().getId());
											// Possible to get more than one transaction records
											List<IPATPaymentConfirmModel> payTransactionInfo = PortalDashboardIPATPaymentConfirmDao
													.findByWfApplicationAndIsDeleted(workFloweApplication, false);
											if (payTransactionInfo != null) {
												if (payTransactionInfo.size() > 0) {
													for (IPATPaymentConfirmModel iPATransactionInfo : payTransactionInfo) {
														workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID,
																Utils.getStringValue(
																		iPATransactionInfo.getPayooTransactionId()));
														workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD,
																"Payoo");
														boolean tranasctionStatus = iPATransactionInfo
																.isPaymentConfirmStatus();
														if (!tranasctionStatus) {
															workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS,
																	"Fail..!!");
														} else {
															workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS,
																	"Success..!!");
														}
													}
												} else {
													workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID, "");
													workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "");
													workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "");
												}
											}

											// need to change later
											workFlowUserMap.put(
													PortalDashboardAuthenticationConstants.CLAIM_POLICY_DOCUMENT_LINK,
													"");

											logger.info("<-- Exiting PortalDashboardService.getAllTransaction()");
										}

									}

								}

							}
						}
					}
				}

			}
		}
		if (workFlowUserMap != null) {
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse getClaimInfoDetails(String processInstanceId) {
		Map<String, Object> variables = null;
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();

		if (processInstanceId != null) {
			try {

				variables = runtimeService.getVariables(processInstanceId);

				logger.info("<==getClaimInfoDetails variable size ==>" + variables.size());
				for (Entry<String, Object> entry : variables.entrySet()) {
					logger.info("<==ClaimServiceIPQuestionaireMapKey==>" + entry.getKey());
					logger.info("<==ClaimServiceIPQuestionaireMapValue==>" + entry.getValue());

				}

			} catch (Exception ex) {
				logger.info("<==Claim Reg Error occur while get process variable==>" + ex);
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
				variables = processInstance.getProcessVariables();
			}

			List<Map<String, Object>> claimQusLists = (List<Map<String, Object>>) variables
					.get(WfImplConstants.CLAIMS_QUESTIIONS);
			if (claimQusLists != null) {
				int question_number = 1;
				for (Map<String, Object> claimQusAnsMap : claimQusLists) {
					Map<String, Object> claimQA = new HashMap<String, Object>();
					String ans = "";
					for (Entry<String, Object> entry : claimQusAnsMap.entrySet()) {
						logger.info("<==ClaimServiceIPQuestionaireMapKey==>" + entry.getKey());
						logger.info("<==ClaimServiceIPQuestionaireMapValue==>" + entry.getValue());

					}

					Map<String, String> qusMap = (Map<String, String>) claimQusAnsMap
							.get(WfImplConstants.CLAIMS_QUS_MAP);
					for (Entry<String, String> entry : qusMap.entrySet()) {
						logger.info("<==ClaimServiceIPQuestionaireMapKey==>" + entry.getKey());
						logger.info("<==ClaimServiceIPQuestionaireMapValue==>" + entry.getValue());

					}
					String question = qusMap.get("en-US");
					ans = (String) claimQusAnsMap.get(WfImplConstants.CLAIMS_ANS_MAP);
					logger.info("<==ClaimServiceIPQuestionaireQuestion claimQusAnsMap==>"
							+ claimQusAnsMap.get(WfImplConstants.CLAIMS_QUS_MAP));
					logger.info("<==ClaimServiceIPQuestionaireQuestion==>" + question);
					logger.info("<==ClaimServiceIPQuestionaireMapKey==>" + ans);

					if (question.equals("Why did you have to stay in hospital?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("Which hospital did you go to?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("When did you go to the hospital?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("When did you leave the hospital?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("Was this a Chinese medical treatment?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals(
							"Looks like you also have income protection, would you also like to claim for this?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("Why did you have to undergo this surgery?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("How much do you need to claim?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals("Do you remember what time the incident happened?")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else if (question.equals(
							"Where were you when the incident happened? Give us the most accurate address you can remember.")) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER),
								Utils.getStringValue(ans));
					} else {

					}
					if (question != null && claimQA.size() == 0) {
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_QUESTION),
								Utils.getStringValue(question));
						claimQA.put(Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER), "");
					}

					workFlowUserMap.put(
							Utils.getStringValue(PortalDashboardAuthenticationConstants.CLAIM_ANSWER + question_number),
							claimQA);

					question_number++;
				}
			} else {
				logger.info("<==claimQusLists is empty==>");
			}

			logger.info("<-- Exiting PortalDashboardService.getCustomerInfoDetails()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);

		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}

	}
	// Claim Details End

}
