package com.kuliza.lending.wf_implementation.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.TopicMessages;

@Repository
public interface TopicMessagesDao extends CrudRepository<TopicMessages, Long> {

	public TopicMessages findById(long id);

	public TopicMessages findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<TopicMessages> findByTopicAndIsDeleted(String topic, boolean isDeleted);

	@Query(value= "select * from topic_messages where topic_name=?1 and created >= ?2 and  is_deleted=?3 ",nativeQuery = true)
	public List<TopicMessages> findMessagesForTopic(String topic, String created, boolean isDeleted);

}
