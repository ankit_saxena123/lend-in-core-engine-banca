package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class UnSubscribeUserTopicForm {

	@NotNull(message = "topic cannot be null")
	String topic;
	
	@NotNull(message = "mobile number cannot be null")
	String contactNumber;
	
	public UnSubscribeUserTopicForm() {
		super();
	}

	public UnSubscribeUserTopicForm(String contactNumber, String topic) {
		super();
		this.contactNumber = contactNumber;
		this.topic = topic;
		
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
