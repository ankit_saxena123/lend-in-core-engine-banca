package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.kuliza.lending.journey.model.AbstractWorkFlowUser;

@Entity
@Table(name = "workflow_user")
public class WorkFlowUser extends AbstractWorkFlowUser {

	@Column(nullable = true)
	private String dob;
	@Column(nullable = true)
	private String gender;
	@Column(nullable = true)
	private int age;
	@Column(nullable = true, name = "arn_end_point")
	private String arnEndPoint;
	@Column(nullable = true)
	private String cif;

	@Column(nullable = true, name = "customer_type")
	private String customerType;
	@Column(nullable = true, name = "date_of_issue")
	private String dateOfIssue;
	@Column(nullable = true, name = "language_preference")
	private String languagePreference;
	@Column(nullable = true, name = "national_id")
	private String nationalId;
	@Column(nullable = true, name = "national_id_back_link")
	private String nationalIdBackLink;
	@Column(nullable = true, name = "national_id_front_link")
	private String nationalIdFrontLink;
	@Column(nullable = true, name = "place_of_issue")
	private String placeOfIssue;
	@Column(nullable = true, name = "selfie_photo_link")

	private String selfiePhotoLink;
	@Column(nullable = true, name = "sex")
	private String sex;
	@Column(nullable = false, name = "notification_preference")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean notificationPreference = false;
	@Column(nullable = false, name = "touch_id_enabled")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean touchIdEnabled = true;
	@Column(nullable = true, name = "app_name")
	private String appName;
	@Column(nullable = true, name = "imei_number")
	private String imeiNumber;
	@Column(nullable = true, name = "is_app_install")
	private String isAppInstall;

	@Column(nullable = false, name = "is_device_id_changed")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isDeviceIdChanged = true;

	@Column(nullable = false, name = "is_push_notification")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isPushNotification = true;

	@Column(nullable = false, name = "is_email_notification")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isEmailNotification = true;

	@Column(nullable = false, name = "is_sms_notification")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isSmsNotification = true;

	@Column(nullable = false, name = "is_promotion_push_notification")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isPromotionPushNotification = true;

	@Column(nullable = false, name = "is_promotion_sms_notification")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isPromotionSmsNotification = true;

	@Column(nullable = false, name = "is_promotion_email_notification")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isPromotionEmailNotification = true;

	@Column(name = "nid_user_name")
	private String nid_user_name;

	@Column(name = "nid_dob")
	private String nid_dob;

	@Column(name = "nid_gender")
	private String nid_gender;

	@Column(name = "nationality")
	private String nationality;

	@Column(name = "hyper_verge_front_response", columnDefinition = "LONGTEXT")
	private String hyperVergeFrontResponse;

	@Column(name = "hyper_verge_back_response", columnDefinition = "LONGTEXT")
	private String hyperVergeBackResponse;

	@Column(name = "cif_response", columnDefinition = "LONGTEXT")
	private String CIFResponse;

	@Column(nullable = false, name = "is_black_listed")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean isBlackListed = false;

	@Column(name = "blacklisted_response", columnDefinition = "LONGTEXT")
	private String blackListedResponse;

	public boolean isPushNotification() {
		return isPushNotification;
	}

	public void setPushNotification(boolean isPushNotification) {
		this.isPushNotification = isPushNotification;
	}

	public boolean isEmailNotification() {
		return isEmailNotification;
	}

	public void setEmailNotification(boolean isEmailNotification) {
		this.isEmailNotification = isEmailNotification;
	}

	public boolean isSmsNotification() {
		return isSmsNotification;
	}

	public void setSmsNotification(boolean isSmsNotification) {
		this.isSmsNotification = isSmsNotification;
	}
//	TO ADD ANY FIELD IN WORKFLOW USER

//	@Column(nullable = true)
//	private String pan;
//	
	public WorkFlowUser() {
		super();
		this.setIsDeleted(false);
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getArnEndPoint() {
		return arnEndPoint;
	}

	public void setArnEndPoint(String arnEndPoint) {
		this.arnEndPoint = arnEndPoint;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getLanguagePreference() {
		return languagePreference;
	}

	public void setLanguagePreference(String languagePreference) {
		this.languagePreference = languagePreference;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getNationalIdBackLink() {
		return nationalIdBackLink;
	}

	public void setNationalIdBackLink(String nationalIdBackLink) {
		this.nationalIdBackLink = nationalIdBackLink;
	}

	public String getNationalIdFrontLink() {
		return nationalIdFrontLink;
	}

	public void setNationalIdFrontLink(String nationalIdFrontLink) {
		this.nationalIdFrontLink = nationalIdFrontLink;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public String getSelfiePhotoLink() {
		return selfiePhotoLink;
	}

	public void setSelfiePhotoLink(String selfiePhotoLink) {
		this.selfiePhotoLink = selfiePhotoLink;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public boolean isNotificationPreference() {
		return notificationPreference;
	}

	public void setNotificationPreference(boolean notificationPreference) {
		this.notificationPreference = notificationPreference;
	}

	public boolean isTouchIdEnabled() {
		return touchIdEnabled;
	}

	public void setTouchIdEnabled(boolean touchIdEnabled) {
		this.touchIdEnabled = touchIdEnabled;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getImeiNumber() {
		return imeiNumber;
	}

	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}

	public String getIsAppInstall() {
		return isAppInstall;
	}

	public void setIsAppInstall(String isAppInstall) {
		this.isAppInstall = isAppInstall;
	}

	public boolean isDeviceIdChanged() {
		return isDeviceIdChanged;
	}

	public void setDeviceIdChanged(boolean isDeviceIdChanged) {
		this.isDeviceIdChanged = isDeviceIdChanged;
	}

	public String getNid_user_name() {
		return nid_user_name;
	}

	public void setNid_user_name(String nid_user_name) {
		this.nid_user_name = nid_user_name;
	}

	public String getNid_dob() {
		return nid_dob;
	}

	public void setNid_dob(String nid_dob) {
		this.nid_dob = nid_dob;
	}

	public String getNid_gender() {
		return nid_gender;
	}

	public void setNid_gender(String nid_gender) {
		this.nid_gender = nid_gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getHyperVergeFrontResponse() {
		return hyperVergeFrontResponse;
	}

	public void setHyperVergeFrontResponse(String hyperVergeFrontResponse) {
		this.hyperVergeFrontResponse = hyperVergeFrontResponse;
	}

	public String getHyperVergeBackResponse() {
		return hyperVergeBackResponse;
	}

	public void setHyperVergeBackResponse(String hyperVergeBackResponse) {
		this.hyperVergeBackResponse = hyperVergeBackResponse;
	}

	public String getCIFResponse() {
		return CIFResponse;
	}

	public void setCIFResponse(String cIFResponse) {
		CIFResponse = cIFResponse;
	}

	public boolean isPromotionPushNotification() {
		return isPromotionPushNotification;
	}

	public void setPromotionPushNotification(boolean isPromotionPushNotification) {
		this.isPromotionPushNotification = isPromotionPushNotification;
	}

	public boolean isPromotionSmsNotification() {
		return isPromotionSmsNotification;
	}

	/*
	 * public WorkFlowUser(String mobileNumber, String email, String username,
	 * String deviceId, String mpin, String pan, String keycloakUsername) { super();
	 * this.setMobileNumber(mobileNumber); this.setEmail(email);
	 * this.setUsername(username); this.setDeviceId(deviceId); this.setMpin(mpin);
	 * this.pan = pan; this.keycloakUsername = keycloakUsername; }
	 */
	public void setPromotionSmsNotification(boolean isPromotionSmsNotification) {
		this.isPromotionSmsNotification = isPromotionSmsNotification;
	}

	public boolean isPromotionEmailNotification() {
		return isPromotionEmailNotification;
	}

	public void setPromotionEmailNotification(boolean isPromotionEmailNotification) {
		this.isPromotionEmailNotification = isPromotionEmailNotification;
	}

	public boolean isBlackListed() {
		return isBlackListed;
	}

	public void setBlackListed(boolean isBlackListed) {
		this.isBlackListed = isBlackListed;
	}

	public String getBlackListedResponse() {
		return blackListedResponse;
	}

	public void setBlackListedResponse(String blackListedResponse) {
		this.blackListedResponse = blackListedResponse;
	}
	/*
	 * public WorkFlowUser(String mobileNumber, String email, String username,
	 * String deviceId, String mpin, String pan, String keycloakUsername) { super();
	 * this.setMobileNumber(mobileNumber); this.setEmail(email);
	 * this.setUsername(username); this.setDeviceId(deviceId); this.setMpin(mpin);
	 * this.pan = pan; this.keycloakUsername = keycloakUsername; }
	 */
//
//	public String getPan() {
//		return pan;
//	}
//
//	public void setPan(String pan) {
//		this.pan = pan;
//	}

}