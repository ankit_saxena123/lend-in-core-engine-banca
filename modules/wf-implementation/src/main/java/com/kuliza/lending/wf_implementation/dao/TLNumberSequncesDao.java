package com.kuliza.lending.wf_implementation.dao;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.TLNumberSequnces;

@Repository
public interface TLNumberSequncesDao  extends CrudRepository<TLNumberSequnces, Long>  {
	
	@Lock(LockModeType.PESSIMISTIC_WRITE) 
	public TLNumberSequnces findTopByOrderByIllustrationNumber();
}
