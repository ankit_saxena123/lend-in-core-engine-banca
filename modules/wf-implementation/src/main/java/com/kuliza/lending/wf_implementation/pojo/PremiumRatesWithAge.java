package com.kuliza.lending.wf_implementation.pojo;

public class PremiumRatesWithAge {

	public PremiumRatesWithAge(double premiumRates) {
		super();
		this.premiumRates = premiumRates;
	}

	double premiumRates;

	public double getPremiumRates() {
		return premiumRates;
	}

	public void setPremiumRates(double premiumRates) {
		this.premiumRates = premiumRates;
	}
}
