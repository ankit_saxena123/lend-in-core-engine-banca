package com.kuliza.lending.wf_implementation.integrations;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class PersonalAccidentInsurearIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(PersonalAccidentInsurearIntegration.class);

	public JSONObject createRequestPayload(Map<String, Object> variables, String transId, String refPaymentId)
			throws JSONException {
		logger.info("variables :" + variables);
		JSONObject requestBodyAsJson = new JSONObject();
		String sumInsured = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
		sumInsured = Utils.ConvertViatnumCurrencyToDigits(sumInsured);
		requestBodyAsJson.put(Constants.SUM_INSURED, sumInsured);
		String premium = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
		premium = Utils.ConvertViatnumCurrencyToDigits(premium);
		requestBodyAsJson.put(Constants.PREMIUM_AMOUNT, premium);
		requestBodyAsJson.put(Constants.DISCLAIMER_AGREED_TAG, Constants.DISCLAIMER_AGREED_TAG_VALUE);
		requestBodyAsJson.put(Constants.TRANSACTION_ID, transId);
		requestBodyAsJson.put(Constants.REFERENCE_NUMBER, refPaymentId);
		requestBodyAsJson.put(Constants.AGENT_ID, Constants.AGENT_ID_VALUE);
		requestBodyAsJson.put(Constants.PAYMENT_DATE,
				Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE_TIME)));
		requestBodyAsJson.put(Constants.PROPOSER_DETAILS, getProposerDetails(variables));
		requestBodyAsJson.put(Constants.PRODUCT_DETAILS, getProductDetails(variables));
		requestBodyAsJson.put(Constants.DELIVERY_ADDRESS, getDiliveryAddress(variables));
		requestBodyAsJson.put(Constants.INSURED_DETAILS, getInsuredDetails(variables));

		logger.info("PA RequestBody : " + requestBodyAsJson);
		return requestBodyAsJson;

	}

	private JSONObject getProposerDetails(Map<String, Object> variables) throws JSONException {
		JSONObject proposerDetails = new JSONObject();
		proposerDetails.put(Constants.PROPOSER_NAME_KEY,
				Utils.getStringValue(variables.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		proposerDetails.put(Constants.PROPOSER_GENDER_KEY, Utils
				.convertVietnamGenderToEnglish(Utils.getStringValue(variables.get(WfImplConstants.GENDER_HYPERVERGE)))
				.toLowerCase());
		String dob = null;
		try {
			dob = Utils.formatDate(Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE)),
					Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_SLASH);

		} catch (Exception e) {
			dob = Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE));
		}
		proposerDetails.put(Constants.DATE_OF_BIRTH_KEY, dob);
		proposerDetails.put(Constants.PROPOSER_DETAILS_NATIONALITY, Utils.convertVietnamNationalityToEnglish(
				Utils.getStringValue(variables.get(WfImplConstants.NATIONALITY_HYPERVERGE))));
		proposerDetails.put(Constants.PROPOSER_DETAILS_NATIONAL_ID,
				Utils.getStringValue(variables.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
		proposerDetails.put(Constants.PROPOSER_DETAILS_ADDRESS,
				Utils.getStringValue(variables.get(WfImplConstants.ADDRESS_HYPERVERGE)));
		proposerDetails.put(Constants.PROPOSER_DETAILS_PLACE_OF_ORIGIN,
				Utils.getStringValue(variables.get(WfImplConstants.PLACEOFORIGIN_HYPERVERGE)));
		return proposerDetails;
	}

	private JSONObject getProductDetails(Map<String, Object> variables) throws JSONException {
		JSONObject productDetails = new JSONObject();
		Date date = new Date();
		date = DateUtils.addYears(date, 1);
		String productId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_ID));
		String planId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PLAN_ID));
		if(productId.equals(WfImplConstants.PA_PRODUCT_ID) || planId.equals(Constants.PA_BASIC_PLAN_ID) || planId.equals(Constants.PA_ADVANCE_PLAN_ID)) {
			productDetails.put(Constants.PRODUCT_PACKAGE,Constants.PACKAGE_PA1);
		}else {
			productDetails.put(Constants.PRODUCT_PACKAGE,Constants.PACKAGE_PA2);
		}
		productDetails.put(Constants.PRODUCT_ID,productId);
		productDetails.put(Constants.PLAN_ID,planId );
		productDetails.put(Constants.TYPE_OF_BUSINESS, Constants.TYPE_OF_BUSINESS_VALUE);
		productDetails.put(Constants.POLICY_START_DATE,
				Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE)));
		productDetails.put(Constants.POLICY_END_DATE,
				Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE)));
		logger.info("productDetails --->"+productDetails);
		return productDetails;
	}

	private JSONObject getDiliveryAddress(Map<String, Object> variables) throws JSONException {
		JSONObject deliveryAddress = new JSONObject();
		deliveryAddress.put(Constants.IS_SAME_CURRENT_KEY, Constants.IS_SAME_CURRENT_VALUE);
		deliveryAddress.put(Constants.ADDRESS_KEY,
				Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_ADDRESS_LINE)));
		deliveryAddress.put(Constants.CITY_KEY, Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_CITY)));
		deliveryAddress.put(Constants.PROVINCE_KEY,
				Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_PROVINCE)));
		return deliveryAddress;
	}

	private JSONArray getInsuredDetails(Map<String, Object> variables) throws JSONException {
		JSONObject insuredDetails = new JSONObject();
		JSONArray insuredDetailsAsJsonArray = new JSONArray();
		insuredDetailsAsJsonArray.put(insuredDetails);
		insuredDetails.put(Constants.PA_INSURED_NAME,
				Utils.getStringValue(variables.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		insuredDetails.put(Constants.PA_INSURED_GENDER, Utils
				.convertVietnamGenderToEnglish(Utils.getStringValue(variables.get(WfImplConstants.GENDER_HYPERVERGE)))
				.toLowerCase());
		String dob = null;
		try {
			dob = Utils.formatDate(Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE)),
					Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_SLASH);

		} catch (Exception e) {
			dob = Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE));
		}
		insuredDetails.put(Constants.PA_INSURED_DOB, dob);
		insuredDetails.put(Constants.PA_INSURED_NATIONAL_ID,
				Utils.getStringValue(variables.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
		insuredDetails.put(Constants.PA_INSURED_RELATIONSHIP, Constants.SELF_KEY);
		insuredDetails.put(Constants.PA_INSURED_Address,
				Utils.getStringValue(variables.get(WfImplConstants.ADDRESS_HYPERVERGE)));
		return insuredDetailsAsJsonArray;
	}

	public JSONObject getDataFromIb(JSONObject requestPayload) {
		logger.info("-->Entering PersonalAccidentInsurearIntegration.getDataFromIb()");
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();
			headersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.IB_PERSONAL_ACCIDENT_SLUG_KEY + "/"
					+ Constants.IB_PA_INSUREAR_KEY + "/" + wfImplConfig.getIbCompany() + "/" + Constants.IB_HASH_VALUE
					+ "/?env=" + wfImplConfig.getIbEnv();
			logger.info("<===========> HashGenerationUrl <===========> :: " + hashGenerationUrl);

			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			logger.info("ResponseJson :: " + responseJson);
			responseJson = resposeAsJson.getBody().getObject();
			return responseJson;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Unable to call PersonalAccidentInsurear post api :" + e.getMessage(), e);
		}
		return responseJson;
	}

	public HashMap<String, Object> parseResponseDataFromIb(JSONObject responseDataFronIb) throws JSONException {
		logger.info("-->Entering PersonalAccidentInsurearIntegration.parseResponseDataFromIb()");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		String message = null;
		if (responseDataFronIb != null) {
			Integer statusCode = responseDataFronIb.getInt(Constants.PA_INSUREAR_RESPONSE_STATUS_KEY);
			if (statusCode > 0) {
				responseMap.put(Constants.PA_INSUREAR_RESPONSE_STATUS_KEY, true);
				logger.info("<========> Inside parseResponseDataFromIb <========> :: "
						+ responseMap.get(Constants.PA_INSUREAR_RESPONSE_STATUS_KEY));
			} else {
				responseMap.put(Constants.PA_INSUREAR_RESPONSE_STATUS_KEY, false);
				message = responseDataFronIb.getString(Constants.PA_INSUREAR_RESPONSE_MESSAGE);
				responseMap.put(Constants.PA_INSUREAR_RESPONSE_MESSAGE, message);
			}
		}
		logger.info("<-- Exiting PersonalAccidentInsurearIntegration.parseResponseDataFromIb()");
		return responseMap;
	}

}
