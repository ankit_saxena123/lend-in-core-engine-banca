package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.Pattern;

public class AppUpdateInfo {

	@Pattern(regexp = "^(ios)|(android)$", message = "Invalid platform. Only ios and android supported.")
	private String platform;

	@Pattern(regexp = "^[0-9]+\\.[0-9]+\\.[0-9]+$", message = "Invalid version format.")
	private String version;

	public AppUpdateInfo() {
		super();
	}

	public AppUpdateInfo(String platform, String version, String appName, String customerType) {
		super();
		this.platform = platform;
		this.version = version;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}