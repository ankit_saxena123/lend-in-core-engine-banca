package com.kuliza.lending.wf_implementation.pojo;

import java.util.List;
import java.util.Map;

public class FamilyPremiumPojo {

	private String language;
	
	private List<Map<String, String>> familyDetails;
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public List<Map<String, String>> getFamilyDetails() {
		return familyDetails;
	}
	public void setFamilyDetails(List<Map<String, String>> familyDetails) {
		this.familyDetails = familyDetails;
	}
	
	
}
