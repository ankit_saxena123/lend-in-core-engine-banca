package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;

@Repository
public interface CMSIntegrationDao extends JpaRepository<CMSIntegration, Long> {

	public CMSIntegration findById(long id);

	public CMSIntegration findByWfApplicationAndPaymentConfirmStatusAndIsDeleted(WorkFlowApplication workFlowApp,
			boolean paymentConfirmStatus, boolean isDeleted);
	
	public CMSIntegration findBytransactionIdAndIsDeleted(String transactionId, boolean isDeleted);

}
