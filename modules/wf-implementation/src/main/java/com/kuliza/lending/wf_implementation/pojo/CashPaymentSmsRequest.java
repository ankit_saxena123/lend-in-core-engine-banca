package com.kuliza.lending.wf_implementation.pojo;

public class CashPaymentSmsRequest {
	
	private String processInstanceId;
	private String crmId;
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getCrmId() {
		return crmId;
	}
	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}
	@Override
	public String toString() {
		return "CashSms [processInstanceId=" + processInstanceId + ", crmId=" + crmId + "]";
	}
	
	
}
