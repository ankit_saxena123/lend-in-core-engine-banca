package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name="rejected_offline_policies")
public class RejectedOfflinePolicies extends BaseModel {
	
	
	@Column(nullable = true, name = "policy_number")
	private String policyNumber;

	
	@Column(nullable = true, name = "response")
	private String response;


	public String getPolicyNumber() {
		return policyNumber;
	}


	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}


	public String getResponse() {
		return response;
	}


	public void setResponse(String response) {
		this.response = response;
	}
	
	

	

}
