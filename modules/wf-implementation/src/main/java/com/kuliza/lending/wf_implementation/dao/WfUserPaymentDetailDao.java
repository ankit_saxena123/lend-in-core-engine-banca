package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WfUserPaymentDetailModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;

@Repository
public interface WfUserPaymentDetailDao extends CrudRepository<WfUserPaymentDetailModel, Long> {

	public List<WfUserPaymentDetailModel> findByWfUser(WorkFlowUser user);
	
	public WfUserPaymentDetailModel findByuUidTransactionIdAndIsDeleted(String uUidTransactionId,boolean isDeleted);
}
