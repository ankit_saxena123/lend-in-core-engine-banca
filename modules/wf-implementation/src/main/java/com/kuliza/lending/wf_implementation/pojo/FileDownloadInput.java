package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

public class FileDownloadInput {

	@NotNull(message = "Doc Id cannot be null")
	String documentId;

	public FileDownloadInput() {
		super();
	}

	public FileDownloadInput(String documentId) {
		super();
		this.documentId = documentId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

}
