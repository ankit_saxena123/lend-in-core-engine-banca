package com.kuliza.lending.wf_implementation.pojo;

import org.hibernate.validator.constraints.NotEmpty;

public class PVIInsurerInput {
	@NotEmpty(message = "transactionID cannot be null or empty")
	private String transactionID;
	@NotEmpty(message = "referenceNumber cannot be null or empty")
	private String referenceNumber;
	@NotEmpty(message = "policyNo cannot be null or empty")
	private String policyNo;
	@NotEmpty(message = "status cannot be null or empty")
	private String status;
	@NotEmpty(message = "policyDocumentPDF cannot be null or empty")
	private String policyDocumentPDF;
	@NotEmpty(message = "agentID cannot be null or empty")
	private String agentID;
	@NotEmpty(message = "policyType cannot be null or empty")
	private String policyType;
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPolicyDocumentPDF() {
		return policyDocumentPDF;
	}
	public void setPolicyDocumentPDF(String policyDocumentPDF) {
		this.policyDocumentPDF = policyDocumentPDF;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getAgentID() {
		return agentID;
	}
	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}
	@Override
	public String toString() {
		return "PVIInsurerInput [transactionID=" + transactionID + ", referenceNumber=" + referenceNumber
				+ ", policyNo=" + policyNo + ", status=" + status + ", policyDocumentPDF=" + policyDocumentPDF
				+ ", agentID=" + agentID + ", policyType=" + policyType + "]";
	}
	
	
	

}
