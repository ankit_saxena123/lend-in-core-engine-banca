package com.kuliza.lending.wf_implementation.base;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.utils.AuthUtils;
import com.kuliza.lending.wf_implementation.utils.OTPConstants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.WfUserDeviceRegDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.WfUserDeviceRegister;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.services.FECreditOTPService;

@Service
public abstract class BaseUserValidationService {

	@Autowired
	private WorkflowUserDao workflowUserDao;

	@Autowired
	private KeyCloakManager keycloakManager;

	@Autowired
	private WfUserDeviceRegDao wfUserDeviceRegDao;

	@Autowired
	private FECreditOTPService feCreditOTPService;

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(BaseUserValidationService.class);

	/**
	 * @param userForm
	 * @return
	 * @throws Exception Service to generate password
	 */
	protected ApiResponse generateUserPassword(HttpServletRequest request, UserPasswordGenerationForm userForm)
			throws Exception {
		ApiResponse apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, null);
		// TODO GENERATE AND SEND OTP
		if (!WfImplConstants.DISABLE_OTP_VALIDATION_FLAG) {
			apiResponse = generateAndSendPassword(request, userForm);
		}
		return apiResponse;
	}

	/**
	 * @param mobileNumber
	 * @return Utility to send password
	 */

	protected abstract ApiResponse sendOTP(String mobileNumber, String otp);

	/**
	 * @param userForm
	 * @return Utility to create and send password
	 */
	protected abstract ApiResponse generateAndSendPassword(HttpServletRequest request,
			UserPasswordGenerationForm userForm);

	/**
	 * @param validationForm
	 * @return
	 * @throws Exception Service to validate and register user
	 */
	protected ApiResponse validateAndRegisterUser(HttpServletRequest request, UserValidationinputForm validationForm)
			throws Exception {
		ApiResponse apiResponse = null;
		boolean isValidated = true;
		try {
			if (wfImplConfig.getValidateDefaultOTP()) 
			isValidated = feCreditOTPService.validateOTP(validationForm.getMobile(), validationForm.getOtp());
			logger.info("OTP Resposne " + isValidated);
			/*if (!wfImplConfig.getValidateDefaultOTP()) {
				isValidated = true;
			}*/
			if (isValidated) {
				WorkFlowUser workFlowUser = workflowUserDao.findByMobileNumberAndIsDeleted(validationForm.getMobile(),
						false);
				boolean isNewUser = false;
				if (workFlowUser == null) {
					isNewUser = true;
					workFlowUser = new WorkFlowUser();
					workFlowUser.setMobileNumber(validationForm.getMobile());
					workFlowUser = workflowUserDao.save(workFlowUser);
					workFlowUser.setIdmUserName(
							String.format(WfImplConstants.WORFKFLOW_USERNAME_CONSTANT, workFlowUser.getId()));
					workFlowUser = workflowUserDao.save(workFlowUser);
				}
				String password = AuthUtils.textEncoding(workFlowUser.getIdmUserName());
				ApiResponse registerResponse = null;
				if (isNewUser) {
					try {
						registerResponse = keycloakManager.createUserWithRole(workFlowUser.getIdmUserName(), password,
								"user");
					} catch (Exception e) {
						apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
					}
				}
				if (registerResponse == null || registerResponse.getStatus() == 200) {
					apiResponse = keycloakManager.loginWithEmailAndPassword(workFlowUser.getIdmUserName(), password,
							false);
					if (validationForm.getDeviceId() != null && validationForm.getDeviceType() != null) {
						Map<String, Object> responseMap = (Map<String, Object>) apiResponse.getData();
						responseMap.put(WfImplConstants.MPIN_SET_FLAG_RESPONSE_KEY, false);
						WfUserDeviceRegister wfUserDeviceObj = null;
						List<WfUserDeviceRegister> wfUserDeviceObjs = null;
						wfUserDeviceObjs = wfUserDeviceRegDao.findByWfUserAndDeviceIdAndDeviceTypeAndIsDeleted(
								workFlowUser, validationForm.getDeviceId(), validationForm.getDeviceType(), false);
						if (wfUserDeviceObjs != null && !wfUserDeviceObjs.isEmpty()) {
							wfUserDeviceObj = wfUserDeviceObjs.get(0);
						} else {
							wfUserDeviceObj = new WfUserDeviceRegister(workFlowUser, validationForm.getDeviceType(),
									validationForm.getDeviceId());
							wfUserDeviceObj = wfUserDeviceRegDao.save(wfUserDeviceObj);
						}
						if (WfImplConstants.DEVICE_INDEPENDENT_MPIN_FLAG && wfUserDeviceObj.getMpin() == null) {
							wfUserDeviceObjs = wfUserDeviceRegDao.findByWfUserAndIsDeletedAndMpinNotNull(workFlowUser,
									false);
							if (wfUserDeviceObjs != null && !wfUserDeviceObjs.isEmpty()) {
								wfUserDeviceObj.setMpin(wfUserDeviceObjs.get(0).getMpin());
								wfUserDeviceObj = wfUserDeviceRegDao.save(wfUserDeviceObj);
							}
						}
						if (wfUserDeviceObj.getMpin() != null) {
							responseMap.put(WfImplConstants.MPIN_SET_FLAG_RESPONSE_KEY, true);
						}
						apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseMap);
					}
				} else {
					apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
				}
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "Invalid OTP");
			}
		} catch (Exception e) {
			logger.warn(CommonHelperFunctions.getStackTrace(e));
			apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE,
					e.getMessage());
		}
		return apiResponse;
	}
	
	/**
	 * @param validationForm
	 * @return ApiResponse
	 * @throws Exception Service to validate otp
	 */
	protected ApiResponse validateOtp(HttpServletRequest request, UserValidationinputForm validationForm)
			throws Exception {
		ApiResponse apiResponse = null;
		boolean isValidated = true;
		try {
//			if (wfImplConfig.getValidateDefaultOTP())
			isValidated = feCreditOTPService.validateOTP(validationForm.getMobile(), validationForm.getOtp());
			logger.info("OTP Resposne " + isValidated);
			if (!wfImplConfig.getValidateDefaultOTP()) {
				isValidated = true;
			}
			if (isValidated) {
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, null);
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "Invalid OTP");
			}
		} catch (Exception e) {
			logger.warn(CommonHelperFunctions.getStackTrace(e));
			apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE,
					e.getMessage());
		}
		return apiResponse;
	}

	/**
	 * @param validationForm
	 * @return Function to validate user
	 */
	protected abstract Boolean validateUser(HttpServletRequest request, UserValidationinputForm validationForm);
}
