package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;

import com.kuliza.lending.wf_implementation.models.MobileNumberUpdateHistory;

public interface MobileNumberUpdateHistoryDao extends CrudRepository<MobileNumberUpdateHistory, Long>{

}
