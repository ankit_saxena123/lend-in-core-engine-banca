package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.pojo.PaymentConfirmForm;
import com.kuliza.lending.wf_implementation.pojo.PaymentStatus;
import com.kuliza.lending.wf_implementation.pojo.PayooConfirm;
import com.kuliza.lending.wf_implementation.services.IPATPaymentConfirmationService;
import com.kuliza.lending.wf_implementation.services.IPATPaymentInfoService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping("/ipat")
public class IPATController {

	@Autowired
	public IPATPaymentConfirmationService ipatPaymentConfirmService;

	@Autowired
	public IPATPaymentInfoService ipatPaymentInfoService;

	private final Logger logger = LoggerFactory.getLogger(IPATController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/payment-info")
	public ResponseEntity<Object> generatePaymentInfo(Principal principal,
			@RequestParam String processInstanceId) {

		try {
			return CommonHelperFunctions
					.buildResponseEntity(ipatPaymentInfoService.generatePaymentBillInfo(principal.getName(), processInstanceId));

		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/payment-confirm")
	public ResponseEntity<Object> generatePaymentConfirm(@RequestBody @Valid PaymentConfirmForm paymentConfirmForm) {

		try {

			return CommonHelperFunctions
					.buildResponseEntity(ipatPaymentConfirmService.addPaymentConfirmation(paymentConfirmForm));

		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/payoo-confirm")
	public ResponseEntity<Object> payooConfirm(Principal principal,@RequestBody @Valid PayooConfirm payooConfirm) {

		try {

			return CommonHelperFunctions.buildResponseEntity(ipatPaymentConfirmService.payooConfirm(principal.getName(),payooConfirm));

		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/repay")
	public void repay() {
		try {
			ipatPaymentConfirmService.updateIPATUnknownPayment();
		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
		}

	}

	@RequestMapping(value = "/payment/status", method = RequestMethod.POST)
	public ApiResponse updatePaymentStatus(Principal principal,@RequestBody PaymentStatus paymentStatusReq) throws FECException {

		try {
			ObjectMapper mapper = new ObjectMapper();
			logger.info("payment update status Request " +mapper.writeValueAsString(paymentStatusReq));
		} catch (Exception e) {
			logger.error("Error while printing log", e);
		}

		validate(paymentStatusReq);

		return ipatPaymentConfirmService.updatePaymentStatus(principal.getName(), paymentStatusReq);

	}

	private void validate(PaymentStatus paymentStatus) throws FECException {

		if (paymentStatus.getProcInstId() == null || paymentStatus.getProcInstId().isEmpty()) {
			throw new FECException(ErrorCodes.INVALID_PROC_ID);
		}

		if (paymentStatus.getStatus() == null || paymentStatus.getStatus().isEmpty()) {
			throw new FECException(ErrorCodes.INVALID_PAYMENT_STATUS);
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/update/payment-status")
	public void updatePaymentIfStatusIsNull() {
		try {
			ipatPaymentConfirmService.fetchListOfPaymentStatus();
		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
		}
	}

}
