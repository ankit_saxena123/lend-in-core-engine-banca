package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import omnidocsapi.UploadDocResponse;

import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.GenerateApplicationIdIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service("generateApplicationIdService")
public class GenerateApplicationIdService {

	@Autowired
	private GenerateApplicationIdIntegration generateApplicationIdIntegration;
	
	@Autowired
	private WorkflowUserDao workflowUserDao;
	
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	
	@Autowired
	private FECDMSService fecdmsService;

	private static final Logger logger = LoggerFactory.getLogger(GenerateApplicationIdService.class);
/**
 * this method is called in Account Management
 * @param userName
 * @return
 * @throws Exception
 */
	public ApiResponse generateApplicationId(String userName) throws Exception {
		logger.info("--> GenerateApplicationIdService.generateApplicationId()");
		logger.info("--> GenerateApplicationIdService username "+userName+"----->");

		Map<String, String> map=new HashMap<String, String>();
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if(workFlowUser==null)
			logger.info("--> GenerateApplicationIdService workFlowUser is null ");

		else 	
			logger.info("--> GenerateApplicationIdService workFlowUser is not null ");

		if(workFlowUser!=null){
				String wfAppId=callGenerateApplicationId();
				logger.info("<========>APPID :"+wfAppId);
				workFlowUser.setAppName(wfAppId);
				workflowUserDao.save(workFlowUser);
				map.put("ApplicationID", wfAppId);
				return new ApiResponse(HttpStatus.OK,Constants.SUCCESS_MESSAGE,map);
		}
		else{
			throw new Exception("User does not exist");
		}
		
		
	}
	/**
	 * this is a service task
	 * @param deletegateExecution
	 * @return
	 */
	public DelegateExecution generateApplicationId(DelegateExecution deletegateExecution) {
		File nidFrontfile =null;
		File nidBackfile=null;
		try{
		logger.info("-->Delegate execution GenerateApplicationIdService.generateApplicationId()");
		logger.info("-->Delegate execution assignee -->"+deletegateExecution.getVariable(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
		
		String userName=Utils.getStringValue(deletegateExecution.getVariable(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
		logger.info("-->Delegate execution username -->"+userName);
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if(workFlowUser!=null)
			logger.info("-->Delegate execution getIdmUserName   -->"+workFlowUser.getIdmUserName());
		else 
			logger.info("-->Delegate execution getIdmUserName  is null -->");
			 
		if(workFlowUser==null){
			logger.error("<======>user does not exist<========>");
			return deletegateExecution;
		}
		String appId = workFlowUser.getAppName();
		logger.info("-->Delegate execution appId   -->"+appId);

		String processInstanceId=deletegateExecution.getProcessInstanceId();
		logger.info("-->Delegate execution processInstanceId   -->"+processInstanceId);

		WorkFlowApplication workFlowApp=workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		
		if(workFlowApp==null)
			logger.info("-->Delegate execution workFlowApp  is null -->");

		else 	
			logger.info("-->Delegate execution workFlowApp  is not null -->");

		if(workFlowApp==null){
			logger.error("<======>workflow app does not exist<========>");
			return deletegateExecution;
		}
		if(appId!=null && !appId.isEmpty()){
			logger.info("<========>APPID :"+appId);
			deletegateExecution.setVariable(WfImplConstants.JOURNEY_APPLICATION_ID, appId);
				workFlowApp.setApplicationId(appId);
				workflowApplicationDao.save(workFlowApp);
				workFlowUser.setAppName(null);
				workflowUserDao.save(workFlowUser);
		}
		else{
			logger.info("<========>delegate exception else :");

		String wfAppId=callGenerateApplicationId();
		logger.info("<========>APPID :"+wfAppId);
		deletegateExecution.setVariable(WfImplConstants.JOURNEY_APPLICATION_ID, wfAppId);
		workFlowApp.setApplicationId(wfAppId);
		workflowApplicationDao.save(workFlowApp);
		String nidFrontDMSId= Utils.getStringValue(workFlowUser.getNationalIdFrontLink());
		String nidBackDMSId= Utils.getStringValue(workFlowUser.getNationalIdBackLink());
		if(!nidFrontDMSId.isEmpty() && !nidBackDMSId.isEmpty()){
			 nidFrontfile = fecdmsService
					.downloadDocumentAsFile(nidFrontDMSId, "", true);
			UploadDocResponse uploadDocument = fecdmsService.uploadDocument(
					"9012", wfAppId, "101",
					true, nidFrontfile);
			String frontdocId = fecdmsService.getDMSDocIdFromResponse(uploadDocument);
			logger.info("<========>frontdocId :"+frontdocId);
			nidBackfile = fecdmsService
					.downloadDocumentAsFile(nidFrontDMSId, "", true);
			 uploadDocument = fecdmsService.uploadDocument(
					"9035", wfAppId, "101",
					true, nidBackfile);
			 String backdocId = fecdmsService.getDMSDocIdFromResponse(uploadDocument);
			 logger.info("<========>backdocId :"+backdocId);
		}
		
		}
		 
		}
		catch(Exception e){
			logger.error("<=======>unable to get appId :"+e.getMessage());
		}
		finally{
			if(nidFrontfile!=null){
				nidFrontfile.deleteOnExit();
			}
			if(nidBackfile!=null){
				nidBackfile.deleteOnExit();
			}
		}
		return deletegateExecution;
	}
	
	
	public  String callGenerateApplicationId() throws Exception{
		try {
			logger.info("<========>callGenerateApplicationId  :");

		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();
		
			JSONObject getRequestPayloadForBancaBlackList = generateApplicationIdIntegration.buildRequestPayload();
			getResponseFromIntegrationBroker = generateApplicationIdIntegration
					.getDataFromIb(getRequestPayloadForBancaBlackList);
			JSONObject parsedResponseForApplicationGenerationId = getResponseFromIntegrationBroker.getBody()
					.getObject();
			parseResponseMap = generateApplicationIdIntegration.parseResponse(parsedResponseForApplicationGenerationId);
			
			if(parseResponseMap.containsKey("ApplicationID")){
				String appId=Utils.getStringValue(parseResponseMap.get("ApplicationID"));
				logger.info("<========>callGenerateApplicationId  app id:"+appId);

				if(appId.isEmpty()){
					throw new Exception("APPID is Empty || NULL ");
				}
				return appId;
			}
			else{
				throw new Exception("unable to get AppID");
			}
		
				
		} catch (Exception e) {
			logger.error("<-- Unable to get APPID :"+e.getMessage());
			throw e;
			
		}

	}
}
