package com.kuliza.lending.wf_implementation.utils;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;



@SuppressWarnings("restriction")
public final class SSLUtilities {

	private static javax.net.ssl.HostnameVerifier __hostnameVerifier;
	private static javax.net.ssl.TrustManager[] __trustManagers;
	private static HostnameVerifier _hostnameVerifier;
	private static TrustManager[] _trustManagers;
	private static void __trustAllHostnames() {
		if (__hostnameVerifier == null) {
			__hostnameVerifier = new _FakeHostnameVerifier();
		} // if
		javax.net.ssl.HttpsURLConnection
				.setDefaultHostnameVerifier(__hostnameVerifier);
	} // __trustAllHttpsCertificates

	private static void __trustAllHttpsCertificates() {
		javax.net.ssl.SSLContext context;

		// Create a trust manager that does not validate certificate chains
		if (__trustManagers == null) {
			__trustManagers = new javax.net.ssl.TrustManager[] { new _FakeX509TrustManager() };
		} // if
			// Install the all-trusting trust manager
		try {
			context = javax.net.ssl.SSLContext.getInstance("SSL");
			context.init(null, __trustManagers, new SecureRandom());
		} catch (GeneralSecurityException gse) {
			throw new IllegalStateException(gse.getMessage());
		} // catch
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(context
				.getSocketFactory());
	} // __trustAllHttpsCertificates


	private static boolean isDeprecatedSSLProtocol() {
		return ("javax.net.ssl.internal.www.protocol".equals(System
				.getProperty("java.protocol.handler.pkgs")));
	} // isDeprecatedSSLProtocol

	private static void _trustAllHostnames() {
		// Create a trust manager that does not validate certificate chains
		if (_hostnameVerifier == null) {
			_hostnameVerifier = new FakeHostnameVerifier();
		} // if
			// Install the all-trusting host name verifier:
		HttpsURLConnection.setDefaultHostnameVerifier(_hostnameVerifier);
	} // _trustAllHttpsCertificates

	private static void _trustAllHttpsCertificates() {
		SSLContext context;

		// Create a trust manager that does not validate certificate chains
		if (_trustManagers == null) {
			_trustManagers = new TrustManager[] { new FakeX509TrustManager() };
		} // if
			// Install the all-trusting trust manager:
		try {
			context = SSLContext.getInstance("SSL");
			context.init(null, _trustManagers, new SecureRandom());
		} catch (GeneralSecurityException gse) {
			throw new IllegalStateException(gse.getMessage());
		} // catch
		HttpsURLConnection.setDefaultSSLSocketFactory(context
				.getSocketFactory());
	} // _trustAllHttpsCertificates

	public static void trustAllHostnames() {
		// Is the deprecated protocol setted?
		if (isDeprecatedSSLProtocol()) {
			__trustAllHostnames();
		} else {
			_trustAllHostnames();
		} // else
	} // trustAllHostnames


	public static void trustAllHttpsCertificates() {
		// Is the deprecated protocol setted?
		if (isDeprecatedSSLProtocol()) {
			__trustAllHttpsCertificates();
		} else {
			_trustAllHttpsCertificates();
		} // else
	} // trustAllHttpsCertificates


	public static class _FakeHostnameVerifier implements
		javax.net.ssl.HostnameVerifier {

		public boolean verify(String hostname, javax.net.ssl.SSLSession session) {
			return (true);
		}
	} 

	public static class _FakeX509TrustManager implements
			javax.net.ssl.X509TrustManager {

		private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};
		public boolean isClientTrusted(X509Certificate[] chain) {
			return (true);
		} // checkClientTrusted
		public boolean isServerTrusted(X509Certificate[] chain) {
			return (true);
		}
		public void checkClientTrusted(java.security.cert.X509Certificate[] chain,java.lang.String authType) {
			
		}
		public void checkServerTrusted(java.security.cert.X509Certificate[] cert,java.lang.String string) {
			
		}
		
		public X509Certificate[] getAcceptedIssuers() {
			return (_AcceptedIssuers);
		}
	} 
	
	public static class FakeHostnameVerifier implements HostnameVerifier {

		public boolean verify(String hostname, javax.net.ssl.SSLSession session) {
			return (true);
		} 
	} 

	public static class FakeX509TrustManager implements X509TrustManager {

		private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};
		public void checkClientTrusted(X509Certificate[] chain, String authType) {
		} 
		
		public void checkServerTrusted(X509Certificate[] chain, String authType) {
		} 
		public X509Certificate[] getAcceptedIssuers() {
			return (_AcceptedIssuers);
		} 
	} 
}
