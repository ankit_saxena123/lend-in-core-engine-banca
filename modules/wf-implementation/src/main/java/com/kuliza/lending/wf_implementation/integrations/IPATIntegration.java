package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class IPATIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;
	
	private static final Logger logger = LoggerFactory.getLogger(IPATIntegration.class);

	public JSONObject buildRequestPayload(Map<String, Object> data) throws JSONException {
		
		JSONObject requestPayload = new JSONObject();
		JSONObject bodyRequestData = new JSONObject();
		
		bodyRequestData.put("vt:UpdateLeadStatus.SysRequest.TransID", Utils.getStringValue(data.get(Constants.BANCA_TRANS_ID)));
		bodyRequestData.put("vt:UpdateLeadStatus.SysRequest.RequestorID", Constants.BANCA_VT_REQUESTOR_ID);
		bodyRequestData.put("vt:UpdateLeadStatus.SysRequest.DateTime", Utils.getCurrentDateTime());
		bodyRequestData.put("vt:UpdateLeadStatus.CRMID",
				Utils.getStringValue(data.get(Constants.BANCA_CRM_ID_KEY)));
		bodyRequestData.put("vt:UpdateLeadStatus.CampaignID",Constants.BANCA_CAMPAIGN_ID);
		bodyRequestData.put("vt:UpdateLeadStatus.PhoneNumber", Utils.getStringValue(data.get(Constants.MOBILE_NUMBER_KEY)));
		bodyRequestData.put("vt:UpdateLeadStatus.NationalID", Utils.getStringValue(data.get(Constants.BANCA_NATIONAL_ID_KEY)));
		bodyRequestData.put("vt:UpdateLeadStatus.IOStage", Constants.BANCA_IO_STAGE);
		bodyRequestData.put("vt:UpdateLeadStatus.IOReason",Constants.BANCA_IO_REASON);
		bodyRequestData.put("vt:UpdateLeadStatus.VTInstance",
				Constants.BANCA_VT_Instance);
		bodyRequestData.put("vt:UpdateLeadStatus.ProductID", Constants.BANCA_VT_PRODUCT_ID);
		bodyRequestData.put("vt:UpdateLeadStatus.LeadStatus",
				Constants.BANCA_LEAD_STATUS);
		bodyRequestData.put("vt:UpdateLeadStatus.PaymentChannel",
				Constants.BANCA_PAYMENT_CHANNEL);
		bodyRequestData.put("vt:UpdateLeadStatus.NationalID2", Utils.getStringValue(data.get(Constants.BANCA_NATIONAL_ID_KEY)));
		bodyRequestData.put("vt:UpdateLeadStatus.Premium",Utils.getStringValue(data.get(Constants.BANCA_PREMIUM_KEY)));
		bodyRequestData.put("vt:UpdateLeadStatus.DueDate", Utils.getStringValue(data.get(Constants.BANCA_DUE_DATE_KEY))); 
		bodyRequestData.put("vt:UpdateLeadStatus.CustomerName",
				Utils.getStringValue(data.get(Constants.BANCA_CUSTOMER_NAME_KEY)));
		
		requestPayload.put("body", bodyRequestData);
		
		logger.info("requestPayload ---> "+requestPayload.toString());
		
		return requestPayload;
	}

	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put("Content-Type", Constants.CONTENT_TYPE);
		
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.IPAT_SHIELD_INTEGRATIONS + "/"
				+ Constants.IPAT_UPDATE_LEAD_STATUS + "/" + wfImplConfig.getIbCompany()+ "/"
				+ Constants.IB_HASH_VALUE +"/?env="+wfImplConfig.getIbEnv()+ Constants.IPAT_SOAP_KEY;
		
		logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);
		
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		
		logger.info("resposeAsJson :: ==========================>" + resposeAsJson.getBody());
		return resposeAsJson;
	}

	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
	
		logger.info("IPAT Integration Parse Response :: ==========================>");
		
		Map<String, Object> ipatIBResponses = new HashMap<>();
		JSONObject apiResponse = null;
		
		try {
			
			apiResponse = responseFromIntegrationBroker.getJSONObject("NS1:UpdateLeadStatusResponse");
			logger.info(apiResponse.toString());
			
			JSONObject sysResponse = apiResponse.getJSONObject("SysResponse");
			if (sysResponse.getString("Description").equals("SUCCESS")) {
				
				ipatIBResponses.put("TransID", sysResponse.getString("TransID"));
				ipatIBResponses.put(Constants.STATUS_MAP_KEY, true);
			
			} else {
				
				logger.info("========================== :: IPAT Integration Response is failed:: ========================== ");
				
				ipatIBResponses.put(Constants.STATUS_MAP_KEY, false);
				ipatIBResponses.put(Constants.DESCRIPTION_MAP_KEY, sysResponse.getString("Description"));
			}
			
		} catch (Exception e) {
			
			ipatIBResponses.put(Constants.STATUS_MAP_KEY, false);
			ipatIBResponses.put(Constants.DESCRIPTION_MAP_KEY, e.getMessage());
		
		}
		
		return ipatIBResponses;
	}
	
}
