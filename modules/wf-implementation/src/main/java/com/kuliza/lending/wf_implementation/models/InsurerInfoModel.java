package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "insurer_info")
public class InsurerInfoModel extends BaseModel{

@Column(nullable = true, name = "process_instance_id")
private String processInstanceId;

@Column(nullable = true, name = "insurer_name")
private String insurerName;

@Column(nullable = true, name = "api_response", columnDefinition = "LONGTEXT")
private String apiResponse;

@Column(nullable = true, name = "policy_id")
private String policyId;

@Column(nullable = true, name = "policy_no")
private String policyNo;

@Column(nullable = true, name = "pdf_link")
private String pdfLink;

@Column(nullable = true, name = "excel_link")
private String excelLink;

public String getExcelLink() {
	return excelLink;
}

public void setExcelLink(String excelLink) {
	this.excelLink = excelLink;
}

@Column(nullable = true, name = "api_request", columnDefinition = "LONGTEXT")
private String apiRequest;

@Column(nullable = true, name = "schedular_count")
@ColumnDefault("0")
private Integer schedularCount=0;

@Type(type = "org.hibernate.type.NumericBooleanType")
@ColumnDefault("0")
@Column(nullable=false)
private Boolean success = false;

@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(nullable = true, name="ipay_payment_confirm_id")
private IPATPaymentConfirmModel iPatPaymentConfirm;


@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(nullable = true, name="cms_payment_confirm_id")
private CMSIntegration cmsPaymentConfirm;


public Integer getSchedularCount() {
	return schedularCount;
}

public void setSchedularCount(Integer schedularCount) {
	this.schedularCount = schedularCount;
}

public String getApiRequest() {
	return apiRequest;
}

public void setApiRequest(String apiRequest) {
	this.apiRequest = apiRequest;
}

public CMSIntegration getCmsPaymentConfirm() {
	return cmsPaymentConfirm;
}

public void setCmsPaymentConfirm(CMSIntegration cmsPaymentConfirm) {
	this.cmsPaymentConfirm = cmsPaymentConfirm;
}

public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public String getInsurerName() {
	return insurerName;
}

public void setInsurerName(String insurerName) {
	this.insurerName = insurerName;
}

public String getApiResponse() {
	return apiResponse;
}

public void setApiResponse(String apiResponse) {
	this.apiResponse = apiResponse;
}

public String getPolicyId() {
	return policyId;
}

public void setPolicyId(String policyId) {
	this.policyId = policyId;
}

public Boolean getSuccess() {
	return success;
}

public void setSuccess(Boolean success) {
	this.success = success;
}


public IPATPaymentConfirmModel getiPatPaymentConfirm() {
	return iPatPaymentConfirm;
}

public void setiPatPaymentConfirm(IPATPaymentConfirmModel iPatPaymentConfirm) {
	this.iPatPaymentConfirm = iPatPaymentConfirm;
}

public String getPdfLink() {
	return pdfLink;
}

public void setPdfLink(String pdfLink) {
	this.pdfLink = pdfLink;
}

public String getPolicyNo() {
	return policyNo;
}

public void setPolicyNo(String policyNo) {
	this.policyNo = policyNo;
}





}