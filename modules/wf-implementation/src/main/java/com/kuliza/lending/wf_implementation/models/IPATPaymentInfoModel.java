package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "ipat_payment_info")
public class IPATPaymentInfoModel extends BaseModel {

	@Column(nullable = true, name = "trans_id")
	private String transID;

	@Column(nullable = true, name = "requestor_id")
	private String requestorID;

	@Column(nullable = true, name = "current_date_time")
	private String currentDateTime;

	@Column(nullable = true, name = "crm_id")
	private String crmID;

	@Column(nullable = true, name = "campaign_id")
	private String campaignID;

	@Column(nullable = true, name = "contact_number")
	private String contactNumber;

	@Column(nullable = true, name = "national_id")
	private String nationalID;

	@Column(nullable = true, name = "io_stage")
	private String ioStage;

	@Column(nullable = true, name = "io_reason")
	private String ioReason;

	@Column(nullable = true, name = "vt_instance")
	private String vtInstance;

	@Column(nullable = true, name = "product_id")
	private String productID;

	@Column(nullable = true, name = "lead_status")
	private String leadStatus;

	@Column(nullable = true, name = "payment_channel")
	private String paymentChannel;
	
	@Column(nullable = true, name = "national_id_2")
	private String nationalID2;

	@Column(nullable = true, name = "premium")
	private String premium;

	@Column(nullable = true, name = "due_date")
	private String dueDate;

	@Column(nullable = true, name = "customer_name")
	private String customerName;
	
	public String getTransID() {
		return transID;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public String getRequestorID() {
		return requestorID;
	}

	public void setRequestorID(String requestorID) {
		this.requestorID = requestorID;
	}

	public String getCurrentDateTime() {
		return currentDateTime;
	}

	public void setCurrentDateTime(String currentDateTime) {
		this.currentDateTime = currentDateTime;
	}

	public String getCrmID() {
		return crmID;
	}

	public void setCrmID(String crmID) {
		this.crmID = crmID;
	}

	public String getCampaignID() {
		return campaignID;
	}

	public void setCampaignID(String campaignID) {
		this.campaignID = campaignID;
	}

	public String getPhoneNumber() {
		return contactNumber;
	}

	public void setPhoneNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getNationalID() {
		return nationalID;
	}

	public void setNationalID(String nationalID) {
		this.nationalID = nationalID;
	}

	public String getIoStage() {
		return ioStage;
	}

	public void setIoStage(String ioStage) {
		this.ioStage = ioStage;
	}

	public String getIoReason() {
		return ioReason;
	}

	public void setIoReason(String ioReason) {
		this.ioReason = ioReason;
	}

	public String getVtInstance() {
		return vtInstance;
	}

	public void setVtInstance(String vtInstance) {
		this.vtInstance = vtInstance;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	public String getNationalID2() {
		return nationalID2;
	}

	public void setNationalID2(String nationalID2) {
		this.nationalID2 = nationalID2;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public IPATPaymentInfoModel() {
		super();
	}
	
	public IPATPaymentInfoModel(String transID, String requestorID, String currentDateTime, String crmID,
			String campaignID, String contactNumber, String nationalID, String ioStage, String ioReason,
			String vtInstance, String productID, String leadStatus, String paymentChannel, String nationalID2,
			String premium, String dueDate, String customerName) {
		super();
		this.transID = transID;
		this.requestorID = requestorID;
		this.currentDateTime = currentDateTime;
		this.crmID = crmID;
		this.campaignID = campaignID;
		this.contactNumber = contactNumber;
		this.nationalID = nationalID;
		this.ioStage = ioStage;
		this.ioReason = ioReason;
		this.vtInstance = vtInstance;
		this.productID = productID;
		this.leadStatus = leadStatus;
		this.paymentChannel = paymentChannel;
		this.nationalID2 = nationalID2;
		this.premium = premium;
		this.dueDate = dueDate;
		this.customerName = customerName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
