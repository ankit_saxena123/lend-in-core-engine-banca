package com.kuliza.lending.wf_implementation.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.UVLSavingsPremiumPojo;
import com.kuliza.lending.wf_implementation.services.UVLSavingsPremiumService;

@RestController
@RequestMapping(value="/uvl-health")
public class UVLHealthController {
	
	private static final Logger logger = LoggerFactory.getLogger(UVLHealthController.class);
	
	@Autowired
	private UVLSavingsPremiumService uvlPremium;

	
	@PostMapping("/quote-details")
	public ResponseEntity<Object> getmaster(@Valid @RequestBody UVLSavingsPremiumPojo input) {
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			logger.info("UVL health quote details request" + mapper.writeValueAsString(input));

			return CommonHelperFunctions.buildResponseEntity(uvlPremium.uvlMaster(input));

		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}

}
