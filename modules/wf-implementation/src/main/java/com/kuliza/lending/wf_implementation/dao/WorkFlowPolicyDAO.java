package com.kuliza.lending.wf_implementation.dao;

import java.util.List; 

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;

@Repository
public interface WorkFlowPolicyDAO extends PagingAndSortingRepository<WorkFlowPolicy, Long>, CrudRepository<WorkFlowPolicy, Long>{
	
	public WorkFlowPolicy findByWorkFlowApplicationAndPlanIdAndIsDeleted(WorkFlowApplication workFlowApp, String planId, boolean isDelete);
	public List<WorkFlowPolicy> findByWorkFlowApplicationAndIsDeleted(WorkFlowApplication workFlowApp, boolean isDelete);
	public List<WorkFlowPolicy> findByWorkFlowApplication(WorkFlowApplication workFlowApp);

	public WorkFlowPolicy findByTransactionIdAndIsDeleted(String transactionId, boolean isDelete);
	
	public WorkFlowPolicy findByPolicyNumberAndIsDeleted(String policyNumber, boolean isDelete);
	public List<WorkFlowPolicy> findAllByIsDeleted(boolean isDelete);
	public List<WorkFlowPolicy> findAllByIsDeleted(Pageable paging, boolean isDelete);
	
	//Search list options - portal dashboard
	public List<WorkFlowPolicy> findAllByPolicyNumberAndIsDeleted(String policyNumber, boolean isDelete);
	public List<WorkFlowPolicy> findAllByPlanIdAndIsDeleted(String planId, boolean isDelete);
	public List<WorkFlowPolicy> findAllByPlanNameAndIsDeleted(String planName, boolean isDelete);
	public List<WorkFlowPolicy> findAllByInsurerAndIsDeleted(String insurer, boolean isDelete);
	public List<WorkFlowPolicy> findAllByTransactionIdAndIsDeleted(String transactionId, boolean isDelete);
}
