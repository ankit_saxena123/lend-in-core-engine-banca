package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "claim_docs")
public class ClaimDocModel extends BaseModel{

@Column(nullable = true, name = "process_instance_id")
private String processInstanceId;

@Type(type = "org.hibernate.type.IntegerType")
@ColumnDefault("0")
@Column(nullable = false, name = "doc_status")
private int docStatus=0;

@Column(nullable = true, name = "username")
private String userName;

@Column(nullable = true, name = "benefit_code")
private String benefitCode;

@Column(nullable = true, name = "title")
private String title;

@Column(nullable = true, name = "document")
private String document;

@Column(nullable = true, name = "doc_url")
private String docUrl;

@Column(nullable = true, name = "doc_id")
private String docID;

public String getDocID() {
	return docID;
}

public void setDocID(String docID) {
	this.docID = docID;
}

public String getUserName() {
	return userName;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public String getBenefitCode() {
	return benefitCode;
}

public void setBenefitCode(String benefitCode) {
	this.benefitCode = benefitCode;
}

public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public int getDocStatus() {
	return docStatus;
}

public void setDocStatus(int docStatus) {
	this.docStatus = docStatus;
}

public String getDocument() {
	return document;
}

public void setDocument(String document) {
	this.document = document;
}

public String getDocUrl() {
	return docUrl;
}

public void setDocUrl(String docUrl) {
	this.docUrl = docUrl;
}


}