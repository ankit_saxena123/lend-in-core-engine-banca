/**
 * 
 */
package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.ClaimInfoModel;

@Repository
public interface PortalClaimInfoDao extends PagingAndSortingRepository<ClaimInfoModel, Long>, CrudRepository<ClaimInfoModel, Long> {
	public List<ClaimInfoModel> findAllByIsDeleted(Pageable paging, boolean isDeleted);
	public List<ClaimInfoModel> findAllByIsDeleted( boolean isDeleted);
	//Claim search
	public List<ClaimInfoModel> findAllByClaimsidContainingIgnoreCase(String claimIdOption);
	public List<ClaimInfoModel> findAllByPolicyNoContainingIgnoreCase(String claimPolicyOption);
//	public ClaimInfoModel findByProcessInstanceId(String pId);
	public ClaimInfoModel findByClaimsid(String claimNumber);
	public List<ClaimInfoModel> findByPolicyNo(String policyNo);
	public List<ClaimInfoModel> findByProcessInstanceId(String pId);
	
}


