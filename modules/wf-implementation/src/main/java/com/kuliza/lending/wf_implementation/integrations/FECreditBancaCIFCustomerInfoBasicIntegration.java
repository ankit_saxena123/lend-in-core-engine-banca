package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditBancaCIFCustomerInfoBasicIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;
	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaCIFCustomerInfoBasicIntegration.class);

	public JSONObject buildRequestPayload(String cifNumber) throws JSONException {
		logger.info("-->Entering FECreditBancaCIFCustomerInfoBasicIntegration.buildRequestPayload()");
		JSONObject requestPayload = new JSONObject();
		JSONObject bodyRequestData = new JSONObject();
		bodyRequestData.put("cif:GetCustomerInfoBasic.Sys.TransID", Utils.generateUUIDTransanctionId());
		bodyRequestData.put("cif:GetCustomerInfoBasic.Sys.RequestorID", Constants.FEC_CIF_INFO_REQUESTOR_ID);
		bodyRequestData.put("cif:GetCustomerInfoBasic.Sys.DateTime", Utils.getCurrentDateTime());
		bodyRequestData.put("cif:GetCustomerInfoBasic.CIFNumber", cifNumber);
		requestPayload.put("body", bodyRequestData);
		logger.info("FECreditBanca CIF Customer Info Basic Request Payload : " + requestPayload.toString());
		logger.info("<-- Exiting FECreditBancaCIFCustomerInfoBasicIntegration.buildRequestPayload()");
		return requestPayload;
	}

	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put("Content-Type", Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.CIF_CUSTOMER_INFO_BASIC_INTEGRATION_SLUG_KEY + "/"
				+ Constants.CIF_CUSTOMER_CUSTOMER_GET_INFO_BASIC + "/" + wfImplConfig.getIbCompany() + "/"
				+ Constants.IB_HASH_VALUE +"/?env="+wfImplConfig.getIbEnv()+ Constants.CIF_CUSTOMER_INFO_BASIC__SAOP_KEY;
		logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		logger.info("resposeAsJson>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + resposeAsJson.getBody());
		return resposeAsJson;
	}

	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		logger.info("-->Entering FECreditBancaCIFCustomerInfoBasicIntegration.parseResponse()");
		LinkedHashMap<String, Object> cifResponseMap = new LinkedHashMap<String, Object>();
		String apiResponse = null;
		logger.debug("Success Response for FEC CIF Customer Info Basic");

		apiResponse = responseFromIntegrationBroker.getString("monoResponse");
		logger.info("Api Response : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + apiResponse);
		JSONObject apiResponseAsJson = new JSONObject(apiResponse);
		JSONObject jsonObjectResponse = apiResponseAsJson.getJSONObject("NS1:GetCustomerInfoBasicResponse");
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + jsonObjectResponse);

		JSONObject sysResponse = jsonObjectResponse.getJSONObject("Sys");
		if (sysResponse.getString("Description").equals("DATA FOUND")) {
			if (jsonObjectResponse.has("CustomerBasicInfo")
					&& jsonObjectResponse.get("CustomerBasicInfo") instanceof JSONObject) {
				JSONObject customerBasicInfo = jsonObjectResponse.getJSONObject("CustomerBasicInfo");
				Map<String, Object> addressMap = new HashMap<String, Object>();
				if (customerBasicInfo.has("CurrentAddresses")) {
					JSONObject currentAddress = customerBasicInfo.getJSONObject("CurrentAddresses");
					addressMap = parseAddress(currentAddress);
				} else {
					logger.info("CurrentAddresses is not there");
				}
				cifResponseMap.putAll(addressMap);
				if (customerBasicInfo.has("CIFNumber")) {
					cifResponseMap.put(Constants.CIF_NUMBER_KEY,
							Utils.getStringValue(customerBasicInfo.get("CIFNumber")));
				}
				if (customerBasicInfo.has("Title")) {
					cifResponseMap.put(Constants.CIF_TITLE_KEY, Utils.getStringValue(customerBasicInfo.get("Title")));
				}
				if (customerBasicInfo.has("FullName")) {
					cifResponseMap.put(Constants.CIF_FULL_NAME_KEY,
							Utils.getStringValue(customerBasicInfo.get("FullName")));
				}
				if (customerBasicInfo.has("FirstName")) {
					cifResponseMap.put(Constants.CIF_FIRST_NAME_KEY,
							Utils.getStringValue(customerBasicInfo.get("FirstName")));
				}
				if (customerBasicInfo.has("MiddleName")) {
					cifResponseMap.put(Constants.CIF_MIDDLE_NAME_KEY,
							Utils.getStringValue(customerBasicInfo.get("MiddleName")));
				}
				if (customerBasicInfo.has("LastName")) {
					cifResponseMap.put(Constants.CIF_LAST_NAME_KEY,
							Utils.getStringValue(customerBasicInfo.get("LastName")));
				}
				if (customerBasicInfo.has("Birthday")) {
					cifResponseMap.put(Constants.CIF_BIRTHDAY_KEY,
							Utils.getStringValue(customerBasicInfo.get("Birthday")));
				}
				if (customerBasicInfo.has("Gender")) {
					cifResponseMap.put(Constants.CIF_GENDER_KEY, Utils.getStringValue(customerBasicInfo.get("Gender")));
				}
				if (customerBasicInfo.has("Citizenship")) {
					cifResponseMap.put(Constants.CIF_CITIZENSHIP_KEY,
							Utils.getStringValue(customerBasicInfo.get("Citizenship")));
				}
				if (customerBasicInfo.has("SocialStatus")) {
					cifResponseMap.put(Constants.CIF_SOCIAL_STATUS_KEY,
							Utils.getStringValue(customerBasicInfo.get("SocialStatus")));
				}
				if (customerBasicInfo.has("CustomerStatus")) {
					cifResponseMap.put(Constants.CIF_CUSTOMER_STATUS_KEY,
							Utils.getStringValue(customerBasicInfo.get("CustomerStatus")));
				}
			} else {
				logger.debug("No Customer Info in FEC CIF Customer Info");
			}
		} else {
			logger.debug("Data Not Found In Description for FEC CIF Customer Info");
		}
		logger.info("<-- Exiting FECreditBancaCIFCustomerInfoBasicIntegration.parseResponse()");
		return cifResponseMap;
	}

	private Map<String, Object> parseAddress(JSONObject currentAddress) throws JSONException {
		logger.info("-->Entering FECreditBancaCIFCustomerInfoBasicIntegration.parseAddress()");
		Map<String, Object> addressMap = new HashMap<String, Object>();
		JSONObject addressJsonObject = currentAddress.getJSONObject("Address");
		if (addressJsonObject.has("AddressType")) {
			addressMap.put(Constants.CIF_ADDRESS_TYPE_KEY, Utils.getStringValue(addressJsonObject.get("AddressType")));
		}
		if (addressJsonObject.has("AddressId")) {
			addressMap.put(Constants.CIF_ADDRESS_ID_KEY, Utils.getStringValue(addressJsonObject.get("AddressId")));
		}
		if (addressJsonObject.has("Number")) {
			addressMap.put(Constants.CIF_ADDRESS_NUMBER_KEY, Utils.getStringValue(addressJsonObject.get("Number")));
		}
		if (addressJsonObject.has("Building")) {
			addressMap.put(Constants.CIF_BUILDING_NUMBER_KEY, Utils.getStringValue(addressJsonObject.get("Building")));
		}
		if (addressJsonObject.has("Street")) {
			addressMap.put(Constants.CIF_STREET_KEY, Utils.getStringValue(addressJsonObject.get("Street")));
		}
		if (addressJsonObject.has("Ward")) {
			addressMap.put(Constants.CIF_WARD_KEY, Utils.getStringValue(addressJsonObject.get("Ward")));
		}
		if (addressJsonObject.has("District")) {
			addressMap.put(Constants.CIF_DISTRICT_CODE_KEY, Utils.getStringValue(addressJsonObject.get("District")));
		}
		if (addressJsonObject.has("City")) {
			addressMap.put(Constants.CIF_CITY_CODE, Utils.getStringValue(addressJsonObject.get("City")));
		}
		if (addressJsonObject.has("CountryName")) {
			addressMap.put(Constants.CIF_COUNTRY_NAME_KEY, Utils.getStringValue(addressJsonObject.get("CountryName")));
		}
		if (addressJsonObject.has("CountryCode")) {
			addressMap.put(Constants.CIF_COUNTRY_CODE_KEY, Utils.getStringValue(addressJsonObject.get("CountryCode")));
		}
		if (addressJsonObject.has("ZipCode")) {
			addressMap.put(Constants.CIF_ZIP_CODE_KEY, Utils.getStringValue(addressJsonObject.get("ZipCode")));
		}
		if (addressJsonObject.has("Years")) {
			addressMap.put(Constants.CIF_YEARS_KEY, Utils.getStringValue(addressJsonObject.get("Years")));
		}
		if (addressJsonObject.has("Months")) {
			addressMap.put(Constants.CIF_MONTHS_KEY, Utils.getStringValue(addressJsonObject.get("Months")));
		}
		if (addressJsonObject.has("PropertyStatus")) {
			addressMap.put(Constants.CIF_PROPERTY_STATUS_KEY,
					Utils.getStringValue(addressJsonObject.get("PropertyStatus")));
		}
		logger.info("<-- Exiting FECreditBancaCIFCustomerInfoBasicIntegration.parseAddress()");
		return addressMap;
	}
	
	
	/**
	 * gets the gender from cif
	 * @param gender
	 * @return String
	 */
		public String getGender(String gender){
			gender=Utils.getStringValue(gender);
			
			if(gender.equalsIgnoreCase("m")){
				gender=Constants.NID_MALE;
			}
			else if(gender.equalsIgnoreCase("f")){ 
				gender=Constants.NID_FEMALE;
			}
			logger.info("gender from cif :"+gender);
			return gender;
			
		}
}
