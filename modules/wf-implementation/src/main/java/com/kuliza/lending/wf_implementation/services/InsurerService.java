package com.kuliza.lending.wf_implementation.services;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.InsurerInfoDao;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.InsurerIntegration;
import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.notification.SNSEventSubscription;
import com.kuliza.lending.wf_implementation.notification.SNSParamInit;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;


@Service("insurerService")
public class InsurerService extends InsurerAbstract {

	@Autowired
	private InsurerIntegration insurerIntegration;
	@Autowired
	private WorkflowApplicationDao workFlowApplicationDao;

	@Autowired
	public SNSParamInit snsParamInit;
	
	@Autowired
	private InsurerInfoDao insurerInfoDao;

	@Autowired
	private FECDMSService fecdmsService;
	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private WfImplConfig wfImplConfig;
	
	@Autowired
	private SNSEventSubscription snsSubscription;
	
	@Autowired
	private UserTopicsDao userTopicsDao;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;
	
	@Autowired
	private WorkflowUserDao workflowUserDao;
	
	private static final Logger logger = LoggerFactory.getLogger(InsurerService.class);

	/**
	 * Generic method called by both service task and api.
	 * 
	 * @param processInstanceId
	 * @param variables
	 * @throws Exception
	 */

	public boolean postPolicyDetails(String processInstanceId, Map<String, Object> variables, String paymentId,
			WorkFlowApplication workflowApp, IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration)
			throws Exception {
		boolean status = false;
		String policyId = null;
		String apiResponse = null;
		String policyPdf = null;
		String policyNo = null;
		String apiRequest = null;
		String transId = null;
		try {

			logger.info("<=======>paymentId:" + paymentId);
			transId = Utils.generateUUIDTransanctionId();
			logger.info("<=======>referenceNo:" + transId);

			paymentId = Utils.getStringValue(paymentId);
			if (paymentId.isEmpty()) {
				logger.error("<=======>failed to call Insurer API :transactionId || refNo is empty");
				throw new Exception("failed to call Insurer API :transactionId || refNo is empty");
			}

			String authToken = insurerIntegration.getAuthToken();

			JSONObject requestPayload = insurerIntegration.createRequestPayload(variables, paymentId, transId);
			apiRequest = Utils.getStringValue(requestPayload);
			JSONObject responseAsJson = insurerIntegration.getDataFromIb(authToken, requestPayload);
			apiResponse = responseAsJson.toString();
			
			if (responseAsJson.getBoolean(Constants.INSURER_RESPONSE_SUCCESS)) {
				HashMap<String, String> responseMap = insurerIntegration.parseResponse(responseAsJson);
				policyId = responseMap.get(Constants.INSURER_RESPONSE_POLICYID);
				logger.info("<=====>policyId Is :" + policyId);
				policyPdf = responseMap.get(Constants.INSURER_RESPONSE_PDFLINK);
				logger.info("<======>policyDocumentPDF Is :" + policyPdf);
				
				if(policyPdf == null || policyPdf.isEmpty()) {
					throw new FECException(ErrorCodes.INVALID_POLICY_PDF);
				}
				
				policyNo = responseMap.get(Constants.INSURER_RESPONSE_POLICYNO);
				logger.info("<=====>policyNo Is :" + policyNo);
				status = true;
				logger.info("<===>insurer api is Sucessfull<======>");

			}else{
				logger.info("<===>insurer api is failed<======>");

			}
			
			
			if (status) {
				logger.info("<-- Status : " + status);
				if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
					logger.info("<=======> sms will be send for Insurer service  <=======>");
					super.sendSms(variables);
				}
				try{
					WorkFlowUserApplication wfUserapp = workflowUserApplicationDao.findByProcInstIdAndIsDeleted(processInstanceId,false);
					WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(wfUserapp.getUserIdentifier(), false);
					String subscriberArn = snsSubscription.subscribeToTopic(snsParamInit.getShTopicARN(),"application",wfUser.getArnEndPoint());
					if(subscriberArn!=null){
						UserTopics userTopic = new UserTopics();
						userTopic.setTopic(WfImplConstants.SH_TOPIC);
						userTopic.setUserName(wfUser.getIdmUserName());
						userTopic.setSubscriberArn(subscriberArn);
						userTopic.setWfUser(wfUser);
						userTopicsDao.save(userTopic);
					}else{
						logger.info("<==Subscription To Registration Topic is Failed For User==>"+wfUser.getIdmUserName());
					}
				}catch(Exception ex)	{
					logger.info("<==Subscription To Registration Topic is Failed For User==>"+ex);
				}
			}
			logger.info("<-- Exiting InsurerServiceTask.getInsurerData()");
		} catch (Exception e) {
			logger.error("unable to call insurer api :" + e.getMessage(), e);
			status = false;
		} finally {
			logger.info("<-- fineally InsurerServiceTask.getInsurerData()");
			persistPolicyInfo(processInstanceId, policyId, policyPdf, policyNo, transId, status, apiResponse, variables,
					workflowApp, ipatPaymentConfim, cmsIntegration, apiRequest);
		}
		return status;

	}

	public void schedulerInsurer(String processInstanceId, String requestBody, InsurerInfoModel insurerInfo, int count,
			Map<String, Object> variables) {
		logger.info("<=====> INSIDE schedulerInsurer******************");
		boolean status = false;
		String policyId = null;
		String apiResponse = null;
		String policyPdf = null;
		String policyNo = null;
		JSONObject payload = null;

		try {
			count = count + 1;
			logger.info("<=====> INSIDE schedulerInsurer Count****************** " + count);
			String authToken = insurerIntegration.getAuthToken();
			payload = new JSONObject(requestBody);
			logger.info("<=====> INSIDE schedulerInsurer pAYLOAD****************** " + payload);
			JSONObject responseAsJson = insurerIntegration.getDataFromIb(authToken, payload);
			apiResponse = responseAsJson.toString();
			if (responseAsJson.getBoolean(Constants.INSURER_RESPONSE_SUCCESS)) {
				HashMap<String, String> responseMap = insurerIntegration.parseResponse(responseAsJson);
				policyId = responseMap.get(Constants.INSURER_RESPONSE_POLICYID);
				logger.info("<=====>policyId In scheduler Is :" + policyId);
				policyPdf = responseMap.get(Constants.INSURER_RESPONSE_PDFLINK);
				logger.info("<======>policyDocumentPDF In scheduler Is :" + policyPdf);
				policyNo = responseMap.get(Constants.INSURER_RESPONSE_POLICYNO);
				logger.info("<=====>policyNo In scheduler Is :" + policyNo);
				status = true;
				logger.info("<===>insurer scheduler api is Sucessfull<======>");
				updatePersistPolicyInfo(status, policyId, apiResponse, policyPdf, policyNo, payload, insurerInfo,
						count);
				if (status) {
					if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
						super.sendSms(variables);
//						super.sendEmail(variables, processInstanceId);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			updatePersistPolicyInfo(status, policyId, apiResponse, policyPdf, policyNo, payload, insurerInfo, count);
		}

	}

	private void updatePersistPolicyInfo(boolean status, String policyId, String apiResponse, String policyPdf,
			String policyNo, Object payload, InsurerInfoModel insurerInfo, int count) {
		logger.info("--->updatePersistPolicyInfo****"+ policyNo);
		String apiRequest = Utils.getStringValue(payload);
		insurerInfo.setApiResponse(apiResponse);
		insurerInfo.setSuccess(status);
		insurerInfo.setPolicyId(policyId);
		insurerInfo.setPdfLink(policyPdf);
		insurerInfo.setPolicyNo(policyNo);
		insurerInfo.setApiRequest(apiRequest);
		insurerInfo.setSchedularCount(count);
		insurerInfoDao.save(insurerInfo);

	}

	private void persistPolicyInfo(String processInstanceId, String policyId, String policyPdf, String policyNo,
			String transactionId, boolean insurerStatus, String apiResponse, Map<String, Object> variables,
			WorkFlowApplication workFlowApp, IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration,
			String apiRequest) throws Exception {

		logger.info("--->persistPolicyInfo" + policyNo + "insurer Status " + insurerStatus  );
		InsurerInfoModel insurerInfo = new InsurerInfoModel();
		insurerInfo.setApiResponse(apiResponse);
		insurerInfo.setInsurerName(WfImplConstants.JOURNEY_INSURER);
		insurerInfo.setiPatPaymentConfirm(ipatPaymentConfim);
		insurerInfo.setCmsPaymentConfirm(cmsIntegration);
		insurerInfo.setPolicyId(policyId);
		insurerInfo.setProcessInstanceId(processInstanceId);
		insurerInfo.setSuccess(insurerStatus);
		insurerInfo.setPdfLink(policyPdf);
		insurerInfo.setPolicyNo(policyNo);
		insurerInfo.setApiRequest(apiRequest);
		insurerInfoDao.save(insurerInfo);
		// if insurer api is success update insurer_status in
		// ipat_payment_confirm
		if (insurerStatus) {
			logger.info("inside insurer status true" + policyNo + "insurer Status " + insurerStatus  );
			String sumInsured = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
			String premium = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			String paymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE));
			String endDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
			String nextPaymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_NEXT_PAYMENT_DUE));
			String planId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PLAN_ID));
			WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO
					.findByWorkFlowApplicationAndPlanIdAndIsDeleted(workFlowApp, planId, false);

			workFlowApp.setIs_purchased(true);
			workFlowApp.setProtectionAmount(sumInsured);
			workFlowApp.setMontlyPayment(premium);
			workFlowApp.setPlanExpiringDate(endDate);
			workFlowApp.setNextPaymentDue(nextPaymentDate);
			workFlowApp.setApplicationDate(paymentDate);
			workFlowApp.setPolicyNumber(policyNo);
			logger.info("<================>appId:" + workFlowApp.getApplicationId() + "<============>");
			String docID = fecdmsService.uploadPDftoDMS(workFlowApp.getApplicationId(), policyPdf);
			workFlowApp.setDmsDocId(docID);
			workFlowApplicationDao.save(workFlowApp);
			logger.info("<=======> email will be send for Insurer service <=======>");
			super.sendEmail(variables, processInstanceId,docID);
			if (workFlowPolicy != null) {
				logger.info("--->inside workflow policy" + policyNo + "dms Doc id " + docID +  "payment date: "+ paymentDate + "endDate: " + endDate + "transactionId: " +  transactionId);
				workFlowPolicy.setDmsDocId(docID);
				workFlowPolicy.setPolicyNumber(policyNo);
				workFlowPolicy.setPurchasedDate(paymentDate);
				workFlowPolicy.setPlanExpiringDate(endDate);
				workFlowPolicy.setTransactionId(transactionId);
				workFlowPolicyDAO.save(workFlowPolicy);
				logger.info("--->Saved in workFlowPolicyDAO ******* ");
			} else {
				logger.error("<=======>workFlowPolicy is NULL<=========>");

			}

		}
		logger.info("--->PolicyInfo is saved SucessFully");

	}

}