package com.kuliza.lending.wf_implementation.pojo;

public class FPTFileRequest {

	private String name;
	private String coordinates;
	private String positionIdentifier;
	
	private String counterCoordinates;
	private String counterPositionIdentifier;
	
	private boolean isCustomerSignNeed;
	private boolean isCounterSignNeed;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}
	public String getPositionIdentifier() {
		return positionIdentifier;
	}
	public void setPositionIdentifier(String positionIdentifier) {
		this.positionIdentifier = positionIdentifier;
	}
	public String getCounterCoordinates() {
		return counterCoordinates;
	}
	public void setCounterCoordinates(String counterCoordinates) {
		this.counterCoordinates = counterCoordinates;
	}
	public String getCounterPositionIdentifier() {
		return counterPositionIdentifier;
	}
	public void setCounterPositionIdentifier(String counterPositionIdentifier) {
		this.counterPositionIdentifier = counterPositionIdentifier;
	}
	public boolean isCustomerSignNeed() {
		return isCustomerSignNeed;
	}
	public void setCustomerSignNeed(boolean isCustomerSignNeed) {
		this.isCustomerSignNeed = isCustomerSignNeed;
	}
	public boolean isCounterSignNeed() {
		return isCounterSignNeed;
	}
	public void setCounterSignNeed(boolean isCounterSignNeed) {
		this.isCounterSignNeed = isCounterSignNeed;
	}
}
