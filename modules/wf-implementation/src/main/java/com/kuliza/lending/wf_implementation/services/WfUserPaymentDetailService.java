package com.kuliza.lending.wf_implementation.services;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WfUserPaymentDetailDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.WfUserPaymentDetailModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.pojo.WfUserPaymentDetail;
import com.kuliza.lending.wf_implementation.pojo.WfUserUpdatePaymentDetail;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;

@Service
public class WfUserPaymentDetailService {

	@Autowired
	WfUserPaymentDetailDao wfUserPaymentDetailDao;

	@Autowired
	WorkflowUserDao workflowUserDao;

	private static final Logger logger = LoggerFactory.getLogger(WfUserPaymentDetailService.class);

	public ApiResponse addPaymentDetails(Principal principal, WfUserPaymentDetail wfUserPaymentDetail) {
		logger.info("-->Entering WfUserPaymentDetailService.addPaymentDetails()");
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(principal.getName(), false);
		if (workFlowUser != null) {
			WfUserPaymentDetailModel wfUserPaymentDetailModel = new WfUserPaymentDetailModel();
			wfUserPaymentDetailModel.setAccountHoldersName(wfUserPaymentDetail.getAccountHoldersName());
			wfUserPaymentDetailModel.setBankName(wfUserPaymentDetail.getBankName());
			wfUserPaymentDetailModel.setBranchName(wfUserPaymentDetail.getBranchName());
			wfUserPaymentDetailModel.setAccountNumber(wfUserPaymentDetail.getAccountNumber());
			wfUserPaymentDetailModel.setuUidTransactionId(Utils.generateUUIDTransanctionId());
			wfUserPaymentDetailModel.setWfUser(workFlowUser);
			wfUserPaymentDetailDao.save(wfUserPaymentDetailModel);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					wfUserPaymentDetailModel.getuUidTransactionId());
		} else {
			logger.info("<-- Exiting WfUserPaymentDetailService.addPaymentDetails()");
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.FAILURE_MESSAGE,
					Constants.PAYMENTS_DETAILS_NOT_ADDED_MESSAGE);
		}
	}

	public ApiResponse getPaymentDetails(Principal principal) {
		logger.info("-->Entering WfUserPaymentDetailService.getPaymentDetails()");
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(principal.getName(), false);
		if (workFlowUser != null) {
			return fetchPaymentDetails(workFlowUser);
		} else {
			logger.info("<-- Exiting WfUserPaymentDetailService.getPaymentDetails()");
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.FAILURE_MESSAGE,
					Constants.PAYMENTS_DETAILS_NOT_ADDED_MESSAGE);
		}
	}

	private ApiResponse fetchPaymentDetails(WorkFlowUser workFlowUser) {
		List<Map<String, String>> listOfPaymentDetails = new ArrayList<Map<String, String>>();
		List<WfUserPaymentDetailModel> wfUserPaymentDetailModels = wfUserPaymentDetailDao
				.findByWfUser(workFlowUser);
		if (wfUserPaymentDetailModels != null && !wfUserPaymentDetailModels.isEmpty()) {
			getListOfPaymentMap(listOfPaymentDetails, wfUserPaymentDetailModels);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, listOfPaymentDetails);
		} else {
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					Constants.PAYMENT_DETAILS_NOT_FOUND);
		}
	}

	private void getListOfPaymentMap(List<Map<String, String>> listOfPaymentDetails,
			List<WfUserPaymentDetailModel> wfUserPaymentDetailModels) {
		for (WfUserPaymentDetailModel paymentDetails : wfUserPaymentDetailModels) {
			Map<String, String> paymentDetailsMap = new HashMap<String, String>();
			if (!paymentDetails.getIsDeleted()) {
				paymentDetailsMap.put(Constants.ACCOUNT_HOLDERS_NAME_MAP_KEY, paymentDetails.getAccountHoldersName());
				paymentDetailsMap.put(Constants.BANK_NAME_MAP_KEY, paymentDetails.getBankName());
				paymentDetailsMap.put(Constants.BRANCH_NAME_MAP_KEY, paymentDetails.getBranchName());
				paymentDetailsMap.put(Constants.ACCOUNT_NUMBER_MAP_KEY, paymentDetails.getAccountNumber());
				paymentDetailsMap.put(Constants.UUID_TRANSACTION_ID_KEY, paymentDetails.getuUidTransactionId());
				listOfPaymentDetails.add(paymentDetailsMap);
			}
		}
	}

	public ApiResponse updatePaymentDetails(WfUserUpdatePaymentDetail wfUserUpdatePaymentDetail) {
		logger.info("-->Entering WfUserPaymentDetailService.updatePaymentDetails()");
		WfUserPaymentDetailModel wfUserPaymentDetailModel = wfUserPaymentDetailDao
				.findByuUidTransactionIdAndIsDeleted(wfUserUpdatePaymentDetail.getuUidTransactionId(), false);
		if (wfUserPaymentDetailModel != null) {
			wfUserPaymentDetailModel.setAccountHoldersName(wfUserUpdatePaymentDetail.getAccountHoldersName());
			wfUserPaymentDetailModel.setBankName(wfUserUpdatePaymentDetail.getBankName());
			wfUserPaymentDetailModel.setBranchName(wfUserUpdatePaymentDetail.getBranchName());
			wfUserPaymentDetailModel.setAccountNumber(wfUserUpdatePaymentDetail.getAccountNumber());
			wfUserPaymentDetailDao.save(wfUserPaymentDetailModel);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					Constants.PAYMENTS_DETAILS_UPDATED_MESSAGE);
		} else {
			logger.info("<-- Exiting WfUserPaymentDetailService.updatePaymentDetails()");
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					Constants.PAYMENTS_DETAILS_NOT_UPDATED_MESSAGE);
		}
	}

	public ApiResponse deletePaymentDetails(String uUidTransactionId) {
		logger.info("-->Entering WfUserPaymentDetailService.deletePaymentDetails()");
		WfUserPaymentDetailModel wfUserPaymentDetailModel = wfUserPaymentDetailDao
				.findByuUidTransactionIdAndIsDeleted(uUidTransactionId, false);
		if (wfUserPaymentDetailModel != null) {
			wfUserPaymentDetailModel.setIsDeleted(true);
			wfUserPaymentDetailDao.save(wfUserPaymentDetailModel);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.PAYMENT_DETAILS_DELETED_MESSAGE);
		} else {
			logger.info("<-- Exiting WfUserPaymentDetailService.deletePaymentDetails()");
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					Constants.PAYMENT_DETAILS_NOT_DELETED_MESSAGE);
		}
	}
}
