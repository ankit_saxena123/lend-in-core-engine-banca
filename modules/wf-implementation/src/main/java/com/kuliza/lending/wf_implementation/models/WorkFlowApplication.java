package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.journey.model.AbstractWorkFlowApplication;

@Entity
@Table(name = "workflow_app")
public class WorkFlowApplication extends AbstractWorkFlowApplication {
	
	@Column(nullable = false, name="application_completed")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean applicationCompleted = false;
	
	@Column(nullable = true, name="application_date")
	private String applicationDate;
	
	@Column(nullable = true, name="application_stage")
	private String applicationStage;
	
	@Column(nullable = true, name="current_Journey_stage")
	private String currentJourneyStage;
	
	@Column(nullable = true, name="loan_application_id")
	private int loanApplicationId;
	
	@Column(nullable = true, unique = true, name="process_instance_id")
	private String processInstanceId;
	
	@Column(nullable = true, name="product_code")
	private String productCode;
	
	@Column(nullable = true, name="scheme_id")
	private String schemeId;
	
	@Column(nullable = true, name="los_application_id") 
	private int losApplicationId;
	
	@Column(nullable = true, name="user_id")
	private int userId;
	
	@Column(nullable = false, name="email_sent")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean emailSent=false;
	
	@Column(nullable = true, name="journey")
	private String journey;
	
	@Column(nullable = true, name="reject_or_cancel_reason")
	private String rejectOrCancelReason;
	
	@Column(nullable = true, name="hold_comment")
	private String holdComment;
	
	@Column(nullable = true, name="hold_flag")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean holdflag=false;
	
	@Column(nullable = true, name="hold_reason")
	private String holdReason;
	
	@Column(nullable = true, name="category_id")
	private String categoryId;
	
	@Column(nullable = true, name="product_id")
	private String productId;
	
	@Column(nullable = true, name="plan_id")
	private String planId;
	
	@Column(nullable = true, name="category_name")
	private String categoryName;
	
	@Column(nullable = true, name="product_name")
	private String productName;
	
	@Column(nullable = true, name="plan_name")
	private String planName;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(nullable=false, name="is_purchased")
	private Boolean is_purchased = false;
	
	@Column(nullable = true, name="policy_number")
	private String policyNumber;
	
	@Column(nullable = true, name="next_payment_due")
	private String nextPaymentDue;
	
	@Column(nullable = true, name="plan_expiring_date")
	private String planExpiringDate;
	
	@Column(nullable = true, name="insurer")
	private String insurer;
	
	@Column(nullable = true, name="montly_payment")
	private String montlyPayment;
	
	@Column(nullable = true, name="protection_amount")
	private String protectionAmount;
	
	@Column(nullable = true, name="dms_doc_id")
	private String dmsDocId;
	
	@Column(nullable = true, name="doc_id_for_excel")
	private String docIdForExcel;
	
	public String getDocIdForExcel() {
		return docIdForExcel;
	}

	public void setDocIdForExcel(String docIdForExcel) {
		this.docIdForExcel = docIdForExcel;
	}

	@Column(nullable = true, name="application_id")
	private String applicationId;
	

	public boolean isApplicationCompleted() {
		return applicationCompleted;
	}

	public void setApplicationCompleted(boolean applicationCompleted) {
		this.applicationCompleted = applicationCompleted;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationStage() {
		return applicationStage;
	}

	public void setApplicationStage(String applicationStage) {
		this.applicationStage = applicationStage;
	}

	public String getCurrentJourneyStage() {
		return currentJourneyStage;
	}

	public void setCurrentJourneyStage(String currentJourneyStage) {
		this.currentJourneyStage = currentJourneyStage;
	}

	public int getLoanApplicationId() {
		return loanApplicationId;
	}

	public void setLoanApplicationId(int loanApplicationId) {
		this.loanApplicationId = loanApplicationId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}

	public int getLosApplicationId() {
		return losApplicationId;
	}

	public void setLosApplicationId(int losApplicationId) {
		this.losApplicationId = losApplicationId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean isEmailSent() {
		return emailSent;
	}

	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

	public String getJourney() {
		return journey;
	}

	public void setJourney(String journey) {
		this.journey = journey;
	}

	public String getRejectOrCancelReason() {
		return rejectOrCancelReason;
	}

	public void setRejectOrCancelReason(String rejectOrCancelReason) {
		this.rejectOrCancelReason = rejectOrCancelReason;
	}

	public String getHoldComment() {
		return holdComment;
	}

	public void setHoldComment(String holdComment) {
		this.holdComment = holdComment;
	}

	public boolean isHoldflag() {
		return holdflag;
	}

	public void setHoldflag(boolean holdflag) {
		this.holdflag = holdflag;
	}

	public String getHoldReason() {
		return holdReason;
	}

	public void setHoldReason(String holdReason) {
		this.holdReason = holdReason;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Boolean getIs_purchased() {
		return is_purchased;
	}

	public void setIs_purchased(Boolean is_purchased) {
		this.is_purchased = is_purchased;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getNextPaymentDue() {
		return nextPaymentDue;
	}

	public void setNextPaymentDue(String nextPaymentDue) {
		this.nextPaymentDue = nextPaymentDue;
	}

	public String getPlanExpiringDate() {
		return planExpiringDate;
	}

	public void setPlanExpiringDate(String planExpiringDate) {
		this.planExpiringDate = planExpiringDate;
	}

	public String getInsurer() {
		return insurer;
	}

	public void setInsurer(String insurer) {
		this.insurer = insurer;
	}

	public String getMontlyPayment() {
		return montlyPayment;
	}

	public void setMontlyPayment(String montlyPayment) {
		this.montlyPayment = montlyPayment;
	}

	public String getProtectionAmount() {
		return protectionAmount;
	}

	public void setProtectionAmount(String protectionAmount) {
		this.protectionAmount = protectionAmount;
	}

	public String getDmsDocId() {
		return dmsDocId;
	}

	public void setDmsDocId(String dmsDocId) {
		this.dmsDocId = dmsDocId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	
	
	
	
//	Please add your fields here if you want to extent workflow app table.
	
//	@Column(nullable = true)
//	private String disbursmentStatus;
//
//	public WorkFlowApplication() {
//		super();
//		this.setIsDeleted(false);
//	}
//
//	public WorkFlowApplication(String applicationType,
//			String applicationStatus, String applicationStage, String disbursmentStatus) {
//		super();
//		this.setApplicationType(applicationType);
//		this.setApplicationStage(applicationStage);
//		this.setApplicationStatus(applicationStatus);
//		this.disbursmentStatus = disbursmentStatus;
//	}
//
//	public String getDisbursmentStatus() {
//		return disbursmentStatus;
//	}
//
//	public void setDisbursmentStatus(String disbursmentStatus) {
//		this.disbursmentStatus = disbursmentStatus;
//	}

}
