package com.kuliza.lending.wf_implementation.pojo;

import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class WorkflowInitiateRequest {

	@NotNull(message = "UserName cannot be null")
	String workflowName;

	Map<String, Object> processVariables;

	Boolean isNew;

	WorkflowSourceForm workflowSource;
	
	String assignee;

	public WorkflowInitiateRequest() {
		super();
	}

	public WorkflowInitiateRequest(String workflowName,
			Map<String, Object> processVariables, Boolean isNew,
			WorkflowSourceForm workflowSource, String assignee) {
		super();
		this.workflowName = workflowName;
		this.processVariables = processVariables;
		this.isNew = isNew;
		this.workflowSource = workflowSource;
		this.assignee = assignee;
	}


	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

	public Boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public WorkflowSourceForm getWorkflowSource() {
		return workflowSource;
	}

	public void setWorkflowSource(WorkflowSourceForm workflowSource) {
		this.workflowSource = workflowSource;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	
}
