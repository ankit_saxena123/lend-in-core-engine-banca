package com.kuliza.lending.wf_implementation.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class DashBoardService {

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;
	
	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;
	
	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;
	
	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory
			.getLogger(DashBoardService.class);

	public ApiResponse getDashBoard(String userName) {
		
		logger.info("----->getDashBoard ()for user-->"+userName);
		Map<String, List<Map<String, Object>>> responseMap = new HashMap<String, List<Map<String, Object>>>();
		
		List<Map<String, Object>> pendingPoliciesList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> purchasedPoliciesList = new ArrayList<Map<String, Object>>();
		
		List<WorkFlowUserApplication> workflowAppUserList = workflowUserApplicationDao
				.findByUserIdentifierAndIsDeleted(userName,false);
		logger.info("----->dashboard  workflowAppUserList size-->"+workflowAppUserList.size());

		if (workflowAppUserList != null) {
			for (WorkFlowUserApplication workflowUserApp : workflowAppUserList) {
				WorkFlowApplication wfApplication = workflowUserApp
						.getWfApplication();
				if (wfApplication != null) {
					String currentJourneyStage = wfApplication
							.getCurrentJourneyStage();
					Long wfAppId=wfApplication.getId();
					logger.info("wfAppId :"+ wfAppId);
					logger.info("currentJourneyStage :"+currentJourneyStage);
					if (currentJourneyStage == null) {
						logger.info("----->getDashBoard current journey null -->");

						constructPendingPoliciesList(pendingPoliciesList,
								wfApplication,workflowUserApp.getProcessName(),wfAppId);
					} else if (currentJourneyStage
							.equalsIgnoreCase(WfImplConstants.APP_STATUS_COMPLETED)) {
						logger.info("----->getDashBoard current journey completed -->");

						constructPurchasedPoliciesList(purchasedPoliciesList, wfApplication,wfAppId);

					} else if (!currentJourneyStage
							.equalsIgnoreCase(WfImplConstants.APP_STATUS_REJECTED)) {

						constructPendingPoliciesList(pendingPoliciesList,
								wfApplication, workflowUserApp.getProcessName(),wfAppId);
					}

				}
			}
		}
		logger.info("pendingPoliciesList : " + pendingPoliciesList);
		logger.info("purchasedPoliciesList : " + purchasedPoliciesList);
		responseMap.put(WfImplConstants.PENDING_POLICIES, pendingPoliciesList);
		responseMap.put(WfImplConstants.PURCHASED_POLICIES,
				purchasedPoliciesList);
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
				responseMap);
	}

	private void constructPendingPoliciesList(
			List<Map<String, Object>> pendingPoliciesList,
			WorkFlowApplication wfApplication, String journeyName,Long wfAppId) {
		List<Map<String, String>> policiesList=getPolicyInfo(wfApplication);
		Map<String, Object> pendingPoliciesMap = new HashMap<String, Object>();
		pendingPoliciesMap.put(WfImplConstants.PROCESS_INSTANCE_ID,
				wfApplication.getProcessInstanceId());
		pendingPoliciesMap.put(WfImplConstants.DATE_MODIFIED, wfApplication
				.getModified().toString());
		pendingPoliciesMap.put(WfImplConstants.CATEGORY_ID,
				wfApplication.getCategoryId());
		pendingPoliciesMap.put(WfImplConstants.PRODUCT_ID,
				wfApplication.getProductId());
		pendingPoliciesMap.put(WfImplConstants.CATEGORY_NAME,
				wfApplication.getCategoryName());
		
		pendingPoliciesMap.put(WfImplConstants.PRODUCT_NAME,
				Utils.translateToEngDashBoard(wfApplication.getProductName()));
		
		pendingPoliciesMap.put(WfImplConstants.PRODUCT_NAME_VI,
				Utils.translateEngToViatnam(wfApplication.getProductName()));
		
		
		pendingPoliciesMap.put(WfImplConstants.PLAN_NAME,
				wfApplication.getPlanName());
		pendingPoliciesMap.put(WfImplConstants.PLAN_NAME_VI,
			Utils.translateEngToViatnam(wfApplication.getPlanName()));
		
		pendingPoliciesMap.put(WfImplConstants.PROCESS_NAME,
				journeyName);
		pendingPoliciesMap.put("policyDetails",
				policiesList);
		pendingPoliciesList.add(pendingPoliciesMap);

	}

	private void constructPurchasedPoliciesList(
			List<Map<String, Object>> purchasedPoliciesList,
			WorkFlowApplication wfApplication,Long wfAppId) {
		logger.info("====== inside constructPurchasedPoliciesList===============>>>>>>> ");
		
		Map<String, Object> purchasedPoliciesMap = new HashMap<String, Object>();
		String partialAmount=null;
		String accessAmount=null;
		String totalPaidAmount=null;
		String crmId=null;
		List<Map<String, String>> policiesList=getPolicyInfo(wfApplication);
		
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceId(wfApplication.getProcessInstanceId()).includeProcessVariables().singleResult();
		
		
		Map<String, Object> variables = processInstance.getProcessVariables();
		
		String paymentModeType = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_INSURER_PAYMENT_INFO));
		String tenor = Utils.getStringValue(((variables.get(WfImplConstants.USER_PREMIUM_TENURE))));
		logger.info("*************** tenor inside constructPurchasedPoliciesList***************" + tenor + " paymentModeType : " + paymentModeType);
		
		
		List<IPATPaymentConfirmModel> ipatConfirmPaymentModelList = ipatPaymentConfirmDao
				.findByWfApplicationIdAndIsDeletedOrderByIdAsc(wfAppId,false);
		logger.info("====== ipatConfirmPaymentModelList==============>>>>>>> " + ipatConfirmPaymentModelList );
		boolean ipatConfirmPaymentStatus=ipatConfirmPaymentModelList.isEmpty();
		if(!ipatConfirmPaymentStatus) {
			logger.info("====== inside if for ipatConfirmPaymentModelList==============>>>>>>> " + ipatConfirmPaymentModelList );
			crmId=ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size()-1).getCrmId();
			totalPaidAmount=getTotalAmountSum(ipatConfirmPaymentModelList);
			
			if(ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size()-1).getPartialPayment()!=null) {
				
				partialAmount=ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size()-1).getPartialPayment();
				logger.info("====== purchasedPolicy=====>>Amount: "+ partialAmount);
			}
			if(ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size()-1).getAccessPayment()!=null) {
				accessAmount=ipatConfirmPaymentModelList.get(ipatConfirmPaymentModelList.size()-1).getAccessPayment();
			}
			
			
		}
		purchasedPoliciesMap.put(WfImplConstants.IPAT_CRM_ID,crmId);
		purchasedPoliciesMap.put(WfImplConstants.PARTIAL_AMOUNT,partialAmount);
		purchasedPoliciesMap.put(WfImplConstants.ACCESS_AMOUNT,accessAmount);
		purchasedPoliciesMap.put(WfImplConstants.TOTAL_PAID_AMOUNT,totalPaidAmount);
		logger.info("====== purchasedPolicy=====>>Amount: "+ partialAmount + "crmId: "
		+ crmId + " ==== totalPaidAmount====>>: " + totalPaidAmount);
		
		purchasedPoliciesMap.put(WfImplConstants.POLICY_NUMBER,
				wfApplication.getPolicyNumber());
		purchasedPoliciesMap.put(WfImplConstants.PURCHASED_DATE,
				wfApplication.getApplicationDate());
		purchasedPoliciesMap.put(WfImplConstants.NEXT_PAYMENT_DUE,
				wfApplication.getNextPaymentDue());
		purchasedPoliciesMap.put(WfImplConstants.MONTHLY_PAYMENT,
				wfApplication.getMontlyPayment());
		purchasedPoliciesMap.put(WfImplConstants.PROTECTION_AMOUNT,
				wfApplication.getProtectionAmount());
		purchasedPoliciesMap.put(WfImplConstants.INSURER,
				wfApplication.getInsurer());
		purchasedPoliciesMap.put(WfImplConstants.EXPIRING_DATE,
				wfApplication.getPlanExpiringDate());
		purchasedPoliciesMap.put(WfImplConstants.CATEGORY_ID,
				wfApplication.getCategoryId());
		purchasedPoliciesMap.put(WfImplConstants.PRODUCT_ID,
				wfApplication.getProductId());
		purchasedPoliciesMap.put(WfImplConstants.CATEGORY_NAME,
				wfApplication.getCategoryName());
		purchasedPoliciesMap.put(WfImplConstants.PRODUCT_NAME,
				Utils.translateToEngDashBoard(wfApplication.getProductName()));
		purchasedPoliciesMap.put(WfImplConstants.PRODUCT_NAME_VI,
				Utils.translateEngToViatnam(wfApplication.getProductName()));
		purchasedPoliciesMap.put(WfImplConstants.PLAN_NAME,
				wfApplication.getPlanName());
		purchasedPoliciesMap.put(WfImplConstants.PLAN_NAME_VI,
				Utils.translateEngToViatnam(wfApplication.getPlanName()));
		purchasedPoliciesMap.put(WfImplConstants.PROCESS_INSTANCE_ID,
				wfApplication.getProcessInstanceId());
		purchasedPoliciesMap.put(WfImplConstants.INSURER_PDF_DOC_ID, wfApplication.getDmsDocId());
		purchasedPoliciesMap.put(WfImplConstants.POLICY_DETAILS,
				policiesList);
		purchasedPoliciesMap.put(WfImplConstants.PAYMENT_MODE_TYPE,
				paymentModeType);
		
		
		purchasedPoliciesMap.put(WfImplConstants.POLICY_NUMBER,	wfApplication.getPolicyNumber());
		
		if(tenor.isEmpty()) {
			tenor="1";
		}
		purchasedPoliciesMap.put(WfImplConstants.TYPE_OF_PAYMENT,tenor);
		purchasedPoliciesList.add(purchasedPoliciesMap);
		logger.info("====== purchasedPoliciesList==============>>>>>>> " + purchasedPoliciesList );

	}


	private List<Map<String, String>> getPolicyInfo(
			WorkFlowApplication wfApplication) {
		List<Map<String, String>> policyList=new ArrayList<Map<String,String>>();
		List<WorkFlowPolicy> workflowPolicyList = workFlowPolicyDAO.findByWorkFlowApplication(wfApplication);
		for(WorkFlowPolicy workFlowPolicy:workflowPolicyList){
			String planId=workFlowPolicy.getPlanId();
			if(planId.equals(WfImplConstants.TW_PERSONAL_ACCIDENT_PLAN_ID)){
				continue;
			}
			Map<String, String> policiesMap = new HashMap<String, String>();
			policiesMap.put(WfImplConstants.INSURER_PDF_DOC_ID, workFlowPolicy.getDmsDocId());
			policiesMap.put(WfImplConstants.POLICY_NUMBER, workFlowPolicy.getPolicyNumber());
			policiesMap.put(WfImplConstants.PLAN_ID, workFlowPolicy.getPlanId());
			policiesMap.put(WfImplConstants.PLAN_NAME, workFlowPolicy.getPlanName());
			policiesMap.put(WfImplConstants.MONTHLY_PAYMENT, workFlowPolicy.getMontlyPayment());
			policiesMap.put(WfImplConstants.PROTECTION_AMOUNT, workFlowPolicy.getProtectionAmount());
			policiesMap.put(WfImplConstants.POLICY_NUMBER_LABLE, WfImplConstants.POLICY_NUMBER_LABLE_VALUE);
			policyList.add(policiesMap);
		}
		
		return policyList;
	}
	
	
	public String getTotalAmountSum(List<IPATPaymentConfirmModel> ipatConfirmPaymentModelList) {
		logger.info("======getTotalAmountSum========" );
		Integer sum=0;
		try {
			for(IPATPaymentConfirmModel i: ipatConfirmPaymentModelList) {
				logger.info("======i.getPaymentAmount()========: " + i.getPaymentAmount());
				sum=sum + Utils.getIntegerValue(i.getPaymentAmount());
			}
			logger.info("======TotalAmountSum========: " + sum);
		} catch (Exception e) {
			logger.error(" error in calculating sum of total paid amount: " + e);
		}
		
		return Utils.getStringValue(sum);
		
		
	}


}
