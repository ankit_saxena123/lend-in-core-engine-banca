package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.OfflinePolicyDetails;

@Repository("offlinePolicyDetailsDAO")
@Transactional
public interface OfflinePolicyDetailsDAO extends CrudRepository<OfflinePolicyDetails, Long> {

	public List<OfflinePolicyDetails> findByMobileNumberAndIsDeleted(String mobileNumber, boolean isDeleted);
	
//	public boolean findByPolicyNumberAndIsDeleted(String policyNumber,boolean isDeleted);
	public OfflinePolicyDetails findByPolicyNumberAndIsDeleted(String policyNumber,boolean isDeleted);
	
}
