package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface IPATPaymentConfirmDao extends CrudRepository<IPATPaymentConfirmModel, Long>,JpaRepository<IPATPaymentConfirmModel, Long> {
	
	public IPATPaymentConfirmModel findById(long id);
	public IPATPaymentConfirmModel findByRefNumAndIsDeleted(String refNum, boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndPaymentInfoStatusAndInsurerStatusAndIsDeleted(WorkFlowApplication workFlowApp, boolean paymentInfoStatus, boolean insurerStatus, boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndRefNumAndPaymentInfoStatusAndInsurerStatusAndIsDeleted(WorkFlowApplication workFlowApp, String refNum,boolean paymentInfoStatus, boolean insurerStatus, boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndPaymentInfoStatusAndIsDeleted(WorkFlowApplication workFlowApp, boolean paymentInfoStatus,  boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndIsDeleted(WorkFlowApplication workFlowApp,  boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndRefNumAndPaymentConfirmStatusAndInsurerStatusAndIsDeleted(WorkFlowApplication workFlowApp, String refNum,boolean paymentInfoStatus, boolean insurerStatus, boolean isDeleted);
	public IPATPaymentConfirmModel findByCrmIdAndIsDeleted(String crmId, boolean isDeleted);
	public IPATPaymentConfirmModel findByCrmIdAndPaymentConfirmStatusAndIsDeleted(String crmId, boolean status,boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndCrmIdAndPaymentConfirmStatusAndInsurerStatusAndIsDeleted(WorkFlowApplication workFlowApp, String crmId,boolean paymentInfoStatus, boolean insurerStatus, boolean isDeleted);
	@Query(value= " select * from ipat_payment_confirm where date(modified) = '2020-02-11' and payment_confirm_status=0 and contact_number='+841584004911' ",nativeQuery = true)
	public List<IPATPaymentConfirmModel> getProcessInstanceIdsOfFailPayment(String lower, String upper);
	public List<IPATPaymentConfirmModel> findByCrmIdAndPaymentConfirmStatusAndIsDeletedOrderByIdAsc(String crmId, boolean status,
			boolean isDeleted);
	public List<IPATPaymentConfirmModel> findByWfApplicationIdAndIsDeletedOrderByIdAsc(Long wfApplicationId, boolean isDeleted);
	public List<IPATPaymentConfirmModel> findByProcessInstanceIdAndIsDeletedOrderByIdAsc(String processInstanceId, boolean isDeleted);
	public IPATPaymentConfirmModel findByWfApplicationAndPaymentConfirmStatusAndIsDeleted(
			WorkFlowApplication workFlowApp, boolean paymentConfirmStatus, boolean isDeleted);
	@Query(value = "select count(*) from ipat_payment_confirm where contact_number = ?1 and date(modified) > ?2 and insurer_status=1", nativeQuery = true)
	public int getTotalPolicyPurchased(String contact_number, String modified);
	

}
