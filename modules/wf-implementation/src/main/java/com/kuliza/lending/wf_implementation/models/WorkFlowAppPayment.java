package com.kuliza.lending.wf_implementation.models;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name="wf_app_payment")
public class WorkFlowAppPayment extends BaseModel{
	
	@Column(name="payment_type")
	private Integer paymentType;
	
	@Column(name="status")
	private String status;
	
	@ManyToOne
	@JoinColumn(name="wf_application_id")
	private WorkFlowApplication workFlowApplication;

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public WorkFlowApplication getWorkFlowApplication() {
		return workFlowApplication;
	}

	public void setWorkFlowApplication(WorkFlowApplication workFlowApplication) {
		this.workFlowApplication = workFlowApplication;
	}
	
	

}
