package com.kuliza.lending.wf_implementation.pojo;

public class ESignRequest {
	
	private String procInstId;
	
	private String authorizeCode;

	public String getProcInstId() {
		return procInstId;
	}

	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}

	public String getAuthorizeCode() {
		return authorizeCode;
	}

	public void setAuthorizeCode(String authorizeCode) {
		this.authorizeCode = authorizeCode;
	}

}
