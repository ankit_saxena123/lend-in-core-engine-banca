package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;

@Repository
public interface InsurerInfoDao extends CrudRepository<InsurerInfoModel, Long> {

	public InsurerInfoModel findByIPatPaymentConfirmAndSuccess(IPATPaymentConfirmModel ipayPaymentConfirm,
			boolean success);

	public List<InsurerInfoModel> findByIsDeletedAndSuccess(boolean isDeleted, boolean success);

	public InsurerInfoModel findBySchedularCount(Integer schedularCount);
	
	public InsurerInfoModel findByProcessInstanceId(String processInstanceId);

}
