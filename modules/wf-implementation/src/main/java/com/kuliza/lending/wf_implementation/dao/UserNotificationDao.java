package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.wf_implementation.models.UserNotificationModel;

@Repository
@Transactional
public interface UserNotificationDao extends CrudRepository<UserNotificationModel, Long>,JpaRepository<UserNotificationModel, Long> {

	public UserNotificationModel findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<UserNotificationModel> findByProcessInstanceIdAndIsDeleted(String id, boolean isDeleted);
	
	public List<UserNotificationModel> findByUserNameAndProcessInstanceIdAndIsDeleted(
			String userName, String processInstanceId, boolean isDeleted);
	
	public List<UserNotificationModel> findByUserNameAndIsDeleted(String userName, boolean isDeleted);
}
