package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserPasswordGenerationForm {

	@NotNull
	String mobile;
	
	@NotNull(message = "Id type cannot be null")
	@Size(min = 1)
	String idType;

	String deviceId;
	
	public UserPasswordGenerationForm() {
		super();
	}

	public UserPasswordGenerationForm(String mobile, String idType, String deviceId) {
		super();
		this.mobile = mobile;
		this.idType = idType;
		this.deviceId = deviceId;
	}

	

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
