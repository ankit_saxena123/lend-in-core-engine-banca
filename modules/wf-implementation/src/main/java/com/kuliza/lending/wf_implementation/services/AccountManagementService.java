package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaCIFCustomerInfoBasicIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaCIFCustomerSearchListIntegration;
import com.kuliza.lending.wf_implementation.integrations.FECreditHypervergeIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service
public class AccountManagementService {

	@Autowired
	private WorkflowUserDao workflowuserDao;
	
	@Autowired
	private WorkFlowAddressDao workFlowAddressDao;

	@Autowired
	private FECDMSService fecDmsService;

	@Autowired
	private FECreditHypervergeIntegration hypervergeIntegration;

	@Autowired
	private FECreditBancaCIFCustomerSearchListIntegration cifCustomerSearchIntegration;

	@Autowired
	private FECreditBancaCIFCustomerInfoBasicIntegration cifCustomerBasicInfoIntegration;

	private static final Logger logger = LoggerFactory
			.getLogger(AccountManagementService.class);
/**
 * gets nid data by using dms doc id. if data is not present in cif gets data from hyperverge.
 * @param userName
 * @param nidFrontDocId
 * @param nidBackDocId
 * @return ApiResponse
 * @throws Exception
 */
	public ApiResponse getNidData(String userName, String nidFrontDocId,
			String nidBackDocId) throws Exception {
		try {
		Map<String, Object> cifDataMap = new HashMap<String, Object>();
		Map<String, Object> hypervergeFrontData = new HashMap<String, Object>();
		Map<String, Object> hypervergeBackData = new HashMap<String, Object>();
		String nationalId = null;
		String mobileNumber = null;
		WorkFlowUser workFlowUser = workflowuserDao
				.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser != null) {
			mobileNumber = workFlowUser.getMobileNumber();
		} else {
			throw new UsernameNotFoundException("user does not exist");
		}
		CloseableHttpResponse closeableHttpResponse = callHyperVerge(nidFrontDocId);
		int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
		String hypervergeFrontResponse = EntityUtils
				.toString(closeableHttpResponse.getEntity());
		logger.info("hyperverge Front Response : " + hypervergeFrontResponse);
		closeableHttpResponse.close();

		if (statusCode != HttpStatus.OK.value()) {
			String error = new JSONObject(hypervergeFrontResponse)
					.getString(WfImplConstants.CIF_ERROR);
			logger.info("hyperverge front call failed : " + error);
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					"hyperverge front call failed : " + error);
		} else {

			hypervergeFrontData = hypervergeIntegration
					.parseNationalIdFrontData(hypervergeFrontResponse);
		}
		logger.info("hypervergeFrontData :"+hypervergeFrontData);
		nationalId = (String) hypervergeFrontData.getOrDefault(
				WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY, "");

		// calling hyperverge to get nid back details
		closeableHttpResponse = callHyperVerge(nidBackDocId);
		statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
		String hypervergeBackResponse = EntityUtils
				.toString(closeableHttpResponse.getEntity());
		logger.info("hyperverge back Response : " + hypervergeBackResponse);
		closeableHttpResponse.close();

		if (statusCode != HttpStatus.OK.value()) {
			String error = new JSONObject(hypervergeBackResponse)
					.getString(WfImplConstants.CIF_ERROR);
			logger.info("hyperverge back call failed : " + error);
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					"hyperverge back call failed : " + error);
		} else {
			hypervergeBackData = hypervergeIntegration
					.parseNationalIdBackData(hypervergeBackResponse);
			logger.info("hypervergeBackData :"+hypervergeBackData);
		}
		logger.info("nationalId is : " + nationalId);

		// calling cif apis
		cifDataMap = getCIFDataMap(nationalId, mobileNumber, workFlowUser);
		Map<String, Object> responseMap = ConstructResponse(hypervergeFrontData, hypervergeBackData, cifDataMap, nationalId, workFlowUser.getGender());
		if(responseMap.isEmpty()){
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					"Data not found in Hyperverge or CIF");
		}
		else{
			addUserData(workFlowUser,responseMap);
			workFlowUser.setHyperVergeFrontResponse(hypervergeFrontResponse);
			workFlowUser.setNationalIdFrontLink(nidFrontDocId);
			workFlowUser.setNationalIdBackLink(nidBackDocId);
			workFlowUser.setHyperVergeFrontResponse(hypervergeFrontResponse);
			workFlowUser.setHyperVergeBackResponse(hypervergeBackResponse);
			workflowuserDao.save(workFlowUser);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
					responseMap);
			
		}
		}
		catch(Exception e) {
			logger.error("faied to get Nid data ", e);
			throw e;
		}
		
		
	}

	private void addUserData(WorkFlowUser workFlowUser,
			Map<String, Object> responseMap) {
		String workflowUserName = workFlowUser.getUsername();
		String gender = workFlowUser.getGender();
		String dob = workFlowUser.getDob();

		responseMap.put(WfImplConstants.USER_FULL_NAME,
				workflowUserName);
		responseMap.put(WfImplConstants.USER_DOB, dob);
		responseMap.put(WfImplConstants.USER_GENDER, gender);
		
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getCIFDataMap(String nationalId,
			String mobileNumber, WorkFlowUser workFlowUser ) {
		Map<String, Object> customerBasicInfoResponse = new HashMap<String, Object>();
		if (nationalId == null || nationalId.isEmpty()) {
			return customerBasicInfoResponse;
		}
		try {
			JSONObject requestPayload = cifCustomerSearchIntegration
					.buildRequestPayload(nationalId, mobileNumber);
			HttpResponse<JsonNode> responseFromIntegrationBroker = cifCustomerSearchIntegration
					.getDataFromService(requestPayload);
			logger.info("response::>>>>>>>>>>>"
					+ cifCustomerSearchIntegration.toString());
			Map<String, Object> getCustomerListResponseMapObject = new HashMap<String, Object>();
			Map<String, Object> customersMapObject = new HashMap<String, Object>();
			Map<String, Object> customerMapObject = new HashMap<String, Object>();
			Map<String, Object> parseResponseMap = cifCustomerSearchIntegration
					.parseResponse(responseFromIntegrationBroker.getBody()
							.getObject());
			if ((Boolean) parseResponseMap.get(Constants.STATUS_MAP_KEY) == true) {
				customerBasicInfoResponse.put(Constants.STATUS_MAP_KEY, true);
				logger.info("parseJsonObject::>>>>>>>>>>>>>>>>::::::"
						+ parseResponseMap);
				getCustomerListResponseMapObject = (Map<String, Object>) parseResponseMap
						.get(WfImplConstants.NS1_GET_CUSTOMER_LIST_RESPONSE);
				customersMapObject = (Map<String, Object>) getCustomerListResponseMapObject
						.get(WfImplConstants.CIF_CUSTOMERS);
				logger.info("customersMapObject::>>>>>>>>>>>>>>>>>>>>>>>>>>>"
						+ customersMapObject);
				if (customersMapObject.get(WfImplConstants.CIF_CUSTOMER) instanceof Map<?, ?>) {
					customerMapObject = (Map<String, Object>) customersMapObject
							.get(WfImplConstants.CIF_CUSTOMER);
				} else if (customersMapObject.get(WfImplConstants.CIF_CUSTOMER) instanceof List) {
					List<Map<String, Object>> customersList = (List<Map<String, Object>>) customersMapObject
							.get(WfImplConstants.CIF_CUSTOMER);
					customerMapObject = customersList.get(0);
				} else {
					logger.info("Customer object is type : "
							+ customersMapObject.get(
									WfImplConstants.CIF_CUSTOMER).getClass());
				}
				String cifNumber = customerMapObject.get(
						WfImplConstants.CIF_NUMBER).toString();
				logger.info("cifNumber:" + cifNumber);
				customerBasicInfoResponse = getCustomerBasicInfo(cifNumber, workFlowUser);
				logger.info("customerBasicInfoResponse :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
						+ customerBasicInfoResponse);

			} else {
				customerBasicInfoResponse.put(Constants.STATUS_MAP_KEY, false);
			}
		} catch (Exception e) {
			logger.error("failed to getCIFData  " + e.getMessage(), e);
			customerBasicInfoResponse.put("status", false);
		}
		return customerBasicInfoResponse;
	}

	/**
	 * This Method takes cifNumber and return parseResponse of Map
	 * 
	 * 
	 * @param cifNumber
	 * @return parseResponseMap
	 * @throws Exception
	 */
	private Map<String, Object> getCustomerBasicInfo(String cifNumber, WorkFlowUser workFlowUser )
			throws Exception {
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();

		JSONObject getRequestPayloadForCif = cifCustomerBasicInfoIntegration
				.buildRequestPayload(cifNumber);
		getResponseFromIntegrationBroker = cifCustomerBasicInfoIntegration
				.getDataFromService(getRequestPayloadForCif);
		JSONObject parsedResponseForCif = getResponseFromIntegrationBroker
				.getBody().getObject();
		workFlowUser.setCIFResponse(parsedResponseForCif.toString());
		parseResponseMap = cifCustomerBasicInfoIntegration
				.parseResponse(parsedResponseForCif);

		return parseResponseMap;
	}

	private Map<String, Object> ConstructResponse(
			Map<String, Object> hypervergeFrontData,
			Map<String, Object> hypervergeBackData,
			Map<String, Object> cifDataMap,
			String nationalId, String userGender) {
		Map<String, Object> processVariableMap = new HashMap<String, Object>();

		boolean cifStatus = (boolean) cifDataMap.getOrDefault(
				Constants.STATUS_MAP_KEY, false);
		if (cifStatus) {
			logger.info("cif data is available");
			processVariableMap.put(WfImplConstants.NATIONAL_ID_HYPERVERGE,
					nationalId);
			processVariableMap.put(WfImplConstants.FULLNAME_HYPERVERGE,
					Utils.getStringValue(cifDataMap
							.get(Constants.CIF_FULL_NAME_KEY)));
			String gender=(String)cifDataMap.getOrDefault(Constants.CIF_GENDER_KEY, "");
			gender=cifCustomerBasicInfoIntegration.getGender(gender);
			if(gender.isEmpty())
				gender=userGender;
			processVariableMap.put(WfImplConstants.GENDER_HYPERVERGE, gender);
			processVariableMap.put(WfImplConstants.GENDER_HYPERVERGE_VI, Utils.convertEnglishGenderToVietnam(gender));
			processVariableMap
					.put(WfImplConstants.DOB_HYPERVERGE, Utils
							.getStringValue(cifDataMap
									.get(Constants.CIF_BIRTHDAY_KEY)));
			processVariableMap.put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
					Utils.getStringValue(cifDataMap
							.get(Constants.CIF_COUNTRY_NAME_KEY)));
			String addressNumber = Utils.getStringValue(cifDataMap
					.get(Constants.CIF_ADDRESS_NUMBER_KEY));
			String street = Utils.getStringValue(cifDataMap
					.get(Constants.CIF_STREET_KEY));
			processVariableMap.put(WfImplConstants.ADDRESS_HYPERVERGE,
					addressNumber + " " + street);
			processVariableMap.put(WfImplConstants.NATIONALITY_HYPERVERGE,
					Constants.NATIONALITY_EN);
			processVariableMap.put(WfImplConstants.NATIONALITY_HYPERVERGE_VI,
					Constants.NATIONALITY_VI);
		} else {
			boolean hypervergeFrontStatus = (boolean) hypervergeFrontData
					.getOrDefault(
							WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG,
							false);
			boolean hypervergeBackStatus = (boolean) hypervergeBackData
					.getOrDefault(
							WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG,
							false);
			logger.info("hypervergeFrontStatus : "+hypervergeFrontStatus);
			logger.info("hypervergeBackStatus : "+hypervergeBackStatus);
			
			if (hypervergeFrontStatus && hypervergeBackStatus) {
				logger.info("hyperverge data is available");
				String nidName = (String) hypervergeFrontData.getOrDefault(
						WfImplConstants.BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY,
						"");
				String nidGender = (String) hypervergeFrontData.getOrDefault(
						WfImplConstants.GENDER_FROM_HYPERVERGE_KEY, "");
				
				nidGender=hypervergeIntegration.getGender(nidGender);
				if(nidGender.isEmpty())
					nidGender=userGender;
				String nidDob = (String) hypervergeFrontData.getOrDefault(
						WfImplConstants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY, "");
				processVariableMap
						.put(WfImplConstants.NATIONAL_ID_HYPERVERGE,
								Utils.getStringValue(hypervergeFrontData
										.get(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY)));
				processVariableMap.put(WfImplConstants.FULLNAME_HYPERVERGE,
						nidName);
				processVariableMap.put(WfImplConstants.GENDER_HYPERVERGE,
						nidGender);
				processVariableMap.put(WfImplConstants.GENDER_HYPERVERGE_VI, Utils.convertEnglishGenderToVietnam(nidGender));
				processVariableMap.put(WfImplConstants.DOB_HYPERVERGE, nidDob);
				processVariableMap
						.put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
								Utils.getStringValue(hypervergeFrontData
										.get(WfImplConstants.PROVINCE_FROM_HYPERVERGE_KEY)));
				processVariableMap
						.put(WfImplConstants.ADDRESS_HYPERVERGE,
								Utils.getStringValue(hypervergeFrontData
										.get(WfImplConstants.ADDRESS_FROM_HYPERVERGE_KEY)));
				processVariableMap.put(WfImplConstants.NATIONALITY_HYPERVERGE,
						Constants.NATIONALITY_EN);
				processVariableMap.put(WfImplConstants.NATIONALITY_HYPERVERGE,
						Constants.NATIONALITY_VI);

			}
		}

		return processVariableMap;
	}

	private CloseableHttpResponse callHyperVerge(String nidDocId)
			throws Exception {
		File file = fecDmsService.downloadDocumentAsFile(nidDocId, "", true);
		CloseableHttpResponse hypervergeResponse = hypervergeIntegration
				.getDataFromService(file);
		file.deleteOnExit();
		return hypervergeResponse;

	}
	
	public ApiResponse getAccountDetails(String userName) {
		WorkFlowUser workFlowUser = workflowuserDao.findByIdmUserNameAndIsDeleted(userName, false);
		Map<String, String> workFlowMap = new HashMap<String, String>();
		if (workFlowUser != null) {
			workFlowMap.put(WfImplConstants.NATIONAL_ID_HYPERVERGE, Utils.getStringValue(workFlowUser.getNationalId()));
			workFlowMap.put(WfImplConstants.GENDER_HYPERVERGE, Utils.getStringValue(workFlowUser.getNid_gender()));
			workFlowMap.put(WfImplConstants.FULLNAME_HYPERVERGE, Utils.getStringValue(workFlowUser.getNid_user_name()));
			workFlowMap.put(WfImplConstants.NATIONALITY_HYPERVERGE, Utils.getStringValue(workFlowUser.getNationality()));
			//Temp change
			workFlowMap.put(WfImplConstants.DOB_HYPERVERGE, Utils.getStringValue(workFlowUser.getDob()));
			workFlowMap.put(Constants.MOBILE_NUMBER_KEY, Utils.getStringValue(workFlowUser.getMobileNumber()));
			workFlowMap.put(Constants.EMAIL_ADDRESS, Utils.getStringValue(workFlowUser.getEmail()));
			workFlowMap.put(WfImplConstants.GENDER_HYPERVERGE_VI, Utils.translateEngToViatnam(workFlowUser.getNid_gender()));
			workFlowMap.put(WfImplConstants.NATIONALITY_HYPERVERGE_VI,Utils.translateEngToViatnam(workFlowUser.getNationality()));
			List<WorkFlowUserAddress> wfAddressList = workFlowAddressDao.findByWfUser(workFlowUser);
			if (wfAddressList != null && !wfAddressList.isEmpty()) {
				for (WorkFlowUserAddress workFlowUserAddress : wfAddressList) {
					String addressType = Utils.getStringValue(workFlowUserAddress.getAddressType());
					String addressLine1 = workFlowUserAddress.getAddressLine1() != null
							? workFlowUserAddress.getAddressLine1()
							: "";
					String cityDist = workFlowUserAddress.getCityDistrict() != null
							? workFlowUserAddress.getCityDistrict()
							: "";
					String province = Utils.getStringValue(workFlowUserAddress.getProvince());

					if (addressType.equals(Constants.NID_ADDRESS_KEY)) {
						workFlowMap.put(WfImplConstants.ADDRESS_HYPERVERGE, addressLine1);
						workFlowMap.put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE, province);
					} else if (addressType.equals(Constants.DELIVERY_ADDRESS_KEY)) {
						workFlowMap.put(WfImplConstants.DELIVERY_ADDRESS_LINE, addressLine1);
						workFlowMap.put(WfImplConstants.DELIVERY_CITY, cityDist);
						workFlowMap.put(WfImplConstants.DELIVERY_PROVINCE, province);
					}
				}

			}
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowMap);
		} else {
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE, Constants.DATA_NOT_FOUND);
		}

	}

}
