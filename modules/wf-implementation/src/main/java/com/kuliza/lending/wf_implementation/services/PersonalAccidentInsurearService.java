package com.kuliza.lending.wf_implementation.services;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.InsurerInfoDao;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.PersonalAccidentInsurearIntegration;
import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.notification.SNSEventSubscription;
import com.kuliza.lending.wf_implementation.notification.SNSParamInit;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("personalAccidentInsurearService")
public class PersonalAccidentInsurearService extends InsurerAbstract {

	@Autowired
	private PersonalAccidentInsurearIntegration personalAccidentInsurearIntegration;

	@Autowired
	private InsurerInfoDao insurerInfoDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	public SNSParamInit snsParamInit;
	
	@Autowired
	private WorkflowApplicationDao workFlowApplicationDao;

	@Autowired
	private WfImplConfig wfImplConfig;
	
	@Autowired
	private SNSEventSubscription snsSubscription;
	
	@Autowired
	private UserTopicsDao userTopicsDao;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;
	
	@Autowired
	private WorkflowUserDao workflowUserDao;

	private static final Logger logger = LoggerFactory.getLogger(PersonalAccidentInsurearService.class);

	@Override
	public boolean postPolicyDetails(String processInstanceId, Map<String, Object> variables, String refPaymentId,
			WorkFlowApplication workflowApp, IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration)
			throws Exception {
		logger.info("-->Entering PersonalAccidentInsurearService.postPolicyDetails()");
		boolean status = false;
		String apiRequest = null;
		String apiResponse = null;
		String transId = null;
		try {

			logger.info("refPaymentId:" + refPaymentId +", transId:" + transId +" procId "+processInstanceId);
			transId = Utils.generateUUIDTransanctionId();
			refPaymentId = Utils.getStringValue(refPaymentId);
			if (refPaymentId.isEmpty()) {
				logger.error("<=======>failed to call Insurer API :transactionId is empty");
				throw new Exception("failed to call Insurer API :transactionId is empty");
			}
			JSONObject requestPayload = personalAccidentInsurearIntegration.createRequestPayload(variables, transId,
					refPaymentId);
			logger.info("PA RequestBody of "+processInstanceId +" ,"+ requestPayload.toString());
			JSONObject responseAsJson = personalAccidentInsurearIntegration.getDataFromIb(requestPayload);
			logger.info("PA response of "+processInstanceId +" ,"+ responseAsJson.toString());
			
			apiRequest = Utils.getStringValue(requestPayload);
			HashMap<String, Object> responseMap = personalAccidentInsurearIntegration
					.parseResponseDataFromIb(responseAsJson);
			boolean successStatus = Utils.getBooleanValue(responseMap.get(Constants.PA_INSUREAR_RESPONSE_STATUS_KEY));
			logger.info("insurer status :" + successStatus +" procId "+processInstanceId);
			if (successStatus) {
				apiResponse = Utils.getStringValue(responseAsJson);
				status = true;
				logger.info("Personal Accident Insurer api is Sucessfull for "+workflowApp.getId());
			} else {
				logger.info("Personal Accident Insurer api is Failed for "+workflowApp.getId());
				apiResponse = Utils.getStringValue(responseAsJson);
			}
			logger.info("parsed apiResponse --->" + apiResponse.toString());
			if (status) {
				if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
					logger.info("<=======> sms will be send for Personal Accident <=======>");
					super.sendSms(variables);
				}
				try{
					WorkFlowUserApplication wfUserapp = workflowUserApplicationDao.findByProcInstIdAndIsDeleted(processInstanceId,false);
					WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(wfUserapp.getUserIdentifier(), false);
					String subscriberArn = snsSubscription.subscribeToTopic(snsParamInit.getPaTopicARN(),"application",wfUser.getArnEndPoint());
					if(subscriberArn!=null){
						UserTopics userTopic = new UserTopics();
						userTopic.setTopic(WfImplConstants.PA_TOPIC);
						userTopic.setUserName(wfUser.getIdmUserName());
						userTopic.setSubscriberArn(subscriberArn);
						userTopic.setWfUser(wfUser);
						userTopicsDao.save(userTopic);
					}else{
						logger.info("<==Subscription To PA Topic is Failed For User==>"+wfUser.getIdmUserName());
					}
				}catch(Exception ex){
					logger.error("error in subscription ",ex);
				}
			}

		} catch (Exception e) {
			logger.error("unable to call insurer api :" + e.getMessage(), e);
			status = false;
		} finally {
			persistPolicyInfo(processInstanceId, transId, status, apiResponse, variables, workflowApp,
					ipatPaymentConfim, cmsIntegration, apiRequest);
		}
		return status;
	}
	public void schedulerInsurer(String processInstanceId, String requestBody, InsurerInfoModel insurerInfo, int count,Map<String, Object> variables) {
		logger.info("<=====> INSIDE schedulerInsurer******************" );
		boolean status = false; 
		String apiResponse = null;

		try {
			count = count + 1;
			JSONObject payload = new JSONObject(requestBody);
			status = true;
			JSONObject responseAsJson = personalAccidentInsurearIntegration.getDataFromIb(payload);
			apiResponse = responseAsJson.toString();
			updatePersistPolicyInfo(status,apiResponse,payload,insurerInfo,count);
			if (status) {
				if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
					super.sendSms(variables);
//					super.sendEmail(variables, processInstanceId);
				}
				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void updatePersistPolicyInfo(boolean status, String apiResponse, Object payload,
			InsurerInfoModel insurerInfo, int count) {
		String apiRequest = Utils.getStringValue(payload);
		insurerInfo.setApiResponse(apiResponse);
		insurerInfo.setSuccess(status);
		insurerInfo.setApiRequest(apiRequest);
		insurerInfo.setSchedularCount(count);
		insurerInfoDao.save(insurerInfo);

	}

	private void persistPolicyInfo(String processInstanceId, String transactionId, boolean insurerStatus,
			String apiResponse, Map<String, Object> variables, WorkFlowApplication workFlowApp,
			IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration, String apiRequest) {
		logger.info("-->Entering PersonalAccidentInsurearService.persistPolicyInfo()");
		InsurerInfoModel insurerInfo = new InsurerInfoModel();
		insurerInfo.setApiResponse(apiResponse);
		insurerInfo.setInsurerName(WfImplConstants.PA_INSURER);
		insurerInfo.setiPatPaymentConfirm(ipatPaymentConfim);
		insurerInfo.setCmsPaymentConfirm(cmsIntegration);
		insurerInfo.setProcessInstanceId(processInstanceId);
		insurerInfo.setSuccess(insurerStatus);
		insurerInfo.setApiRequest(apiRequest);
		// insurerInfo.setSchedularCount(1);
		insurerInfoDao.save(insurerInfo);

		if (insurerStatus) {
			String sumInsured = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
			String premium = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			String paymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE));
			String endDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
			String nextPaymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_NEXT_PAYMENT_DUE));
			String planId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PLAN_ID));
			WorkFlowPolicy workFlowPolicy = workFlowPolicyDAO
					.findByWorkFlowApplicationAndPlanIdAndIsDeleted(workFlowApp, planId, false);
			workFlowApp.setIs_purchased(true);
			workFlowApp.setProtectionAmount(sumInsured);
			workFlowApp.setMontlyPayment(premium);
			workFlowApp.setPlanExpiringDate(endDate);
			workFlowApp.setNextPaymentDue(nextPaymentDate);
			workFlowApp.setApplicationDate(paymentDate);
			workFlowApplicationDao.save(workFlowApp);
			String dmsDocId = Utils.getStringValue(workFlowApp.getDmsDocId());
			if (workFlowPolicy != null) {
				workFlowPolicy.setPurchasedDate(paymentDate);
				workFlowPolicy.setPlanExpiringDate(endDate);
				workFlowPolicy.setTransactionId(transactionId);
				workFlowPolicyDAO.save(workFlowPolicy);
				logger.info("<=======> sms will be send for Personal Accident <=======>");
				super.sendEmail(variables, processInstanceId,dmsDocId);
			} else {
				logger.error("<=======>workFlowPolicy is NULL<=========>");

			}

		}
		logger.info("--->PolicyInfo is saved SucessFully");
		logger.info("<-- Exiting PersonalAccidentInsurearService.persistPolicyInfo()");
	}
}
