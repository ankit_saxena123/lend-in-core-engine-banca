package com.kuliza.lending.wf_implementation.services;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaBlackListIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service("fECreditBancaBlackListService")
public class FECreditBancaBlackListService {

	@Autowired
	private FECreditBancaBlackListIntegration feCreditBancaBlackListIntegration;

	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private RuntimeService runtimeService;

	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaBlackListService.class);

	public ApiResponse bancaBlackListDetails(WorkFlowUser workFlowUser, String nationalId) {
		logger.info("-->Entering FECreditBancaBlackListService.bancaBlackListDetails()");
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();
		try {
			JSONObject getRequestPayloadForBancaBlackList = feCreditBancaBlackListIntegration
					.buildRequestPayload(nationalId);
			getResponseFromIntegrationBroker = feCreditBancaBlackListIntegration
					.getDataFromService(getRequestPayloadForBancaBlackList);
			JSONObject parsedResponseForBancaBlackList = getResponseFromIntegrationBroker.getBody().getObject();
			parseResponseMap = feCreditBancaBlackListIntegration.parseResponse(parsedResponseForBancaBlackList);
			logger.info("parseResponseMap :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + parseResponseMap);
			if ((boolean) parseResponseMap.get(Constants.IS_BLACK_LISTED_KEY)) {
				storeBlackListResponseInDb(workFlowUser, true, parsedResponseForBancaBlackList.toString());
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.USER_IS_BLACK_LISTED_MESSAGE);
			} else {
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.USER_IS_NOT_BLACK_LISTED_MESSAGE);
			}
		} catch (Exception e) {
			logger.info("<-- Exiting FECreditBancaBlackListService.bancaBlackListDetails()");
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					e.getMessage());
		}
	}


	public DelegateExecution bancaBlackListDetails(DelegateExecution execution) {
		logger.info("-->Entering ServiceTask FECreditBancaBlackListService.bancaBlackListDetails()");
		String processInstanceId = execution.getProcessInstanceId();

		String userName = CommonHelperFunctions.getStringValueOrDefault(runtimeService.getVariable(processInstanceId,
				com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY), "").toString();
		String nationalIdHyperVerge = CommonHelperFunctions
				.getStringValueOrDefault(
						runtimeService.getVariable(processInstanceId, WfImplConstants.NATIONAL_ID_HYPERVERGE), "")
				.toString();
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();
		try {
			JSONObject getRequestPayloadForBancaBlackList = feCreditBancaBlackListIntegration
					.buildRequestPayload(nationalIdHyperVerge);
			getResponseFromIntegrationBroker = feCreditBancaBlackListIntegration
					.getDataFromService(getRequestPayloadForBancaBlackList);
			JSONObject parsedResponseForBancaBlackList = getResponseFromIntegrationBroker.getBody().getObject();
			parseResponseMap = feCreditBancaBlackListIntegration.parseResponse(parsedResponseForBancaBlackList);
			logger.info("parseResponseMap :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + parseResponseMap);
			if ((boolean) parseResponseMap.get(Constants.IS_BLACK_LISTED_KEY)) {
				storeBlackListResponseInDb(userName, true, parsedResponseForBancaBlackList.toString());
				logger.info("User is BlackListed");
			} else {
				logger.info("User is Not BlackListed");
			}
		} catch (Exception e) {
			logger.info("<-- Exiting ServiceTask FECreditBancaBlackListService.bancaBlackListDetails()");
			logger.info(e.getMessage());
		}
		return execution;
	}

	public void storeBlackListResponseInDb(String userName, Boolean isBlackListed, String blackListedResponse) {
		logger.info("-->Entering FECreditBancaBlackListService.storeBlackListResponseInDb()");
		try {
			if (userName != null && !userName.isEmpty()) {
				WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
				if (workFlowUser != null) {
					workFlowUser.setBlackListed(isBlackListed);
					workFlowUser.setBlackListedResponse(blackListedResponse);
					workFlowUserDao.save(workFlowUser);
				}
			} else {
				logger.info("user does not exist:" + userName);
			}
		} catch (Exception e) {
			logger.info("Error Message : " + e.getMessage());
		}
		logger.info("<-- Exiting FECreditBancaBlackListService.storeBlackListResponseInDb()");
	}
	
	private void storeBlackListResponseInDb(WorkFlowUser workFlowUser,
			boolean isBlackListed, String blackListedResponse) {
		logger.info("-->Entering FECreditBancaBlackListService.storeBlackListResponseInDb()");
		workFlowUser.setBlackListed(isBlackListed);
		workFlowUser.setBlackListedResponse(blackListedResponse);
		logger.info("<-- Exiting FECreditBancaBlackListService.storeBlackListResponseInDb()");
		
	}
}
