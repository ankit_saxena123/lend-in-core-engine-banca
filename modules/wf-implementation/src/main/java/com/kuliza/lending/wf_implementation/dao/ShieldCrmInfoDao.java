package com.kuliza.lending.wf_implementation.dao;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.ShieldCrmIdInfoModel;

@Repository
public interface ShieldCrmInfoDao extends CrudRepository<ShieldCrmIdInfoModel, Long> {

	public ShieldCrmIdInfoModel findById(long id);
	@Lock(LockModeType.PESSIMISTIC_WRITE) 
	public ShieldCrmIdInfoModel findTopByOrderByCreated();
	

}
