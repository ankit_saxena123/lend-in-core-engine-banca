package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.journey.model.AbstractWfUserDeviceRegister;

@Entity
@Table(name = "topic_arn")
public class TopicArn extends BaseModel {

	@Column(nullable = true, name = "topic_name")
	private String topic;

	@Column(nullable = true, name = "arn")
	private String arn;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getArn() {
		return arn;
	}

	public void setArn(String arn) {
		this.arn = arn;
	}
	
}
