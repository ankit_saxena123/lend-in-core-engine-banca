package com.kuliza.lending.wf_implementation.pojo;

import java.util.List;

public class TLPolicyDetails {
	
	private String contractNumber;
	
	private String policyNumber;
	
	private String policyHolderName;
	private String policyHolderAge;
	private String policyHolderGender;
	private String policyHolderDOB;
	private String policyHolderAddress;
	private String policyHolderPhone;
	private String policyHolderEmail;
	
	private String policyHolderNID;
	
	
	private String productionDuraion;
	private String paymentOption;
	
	private String tlProductionAmount;
	private String criticalIllnessProductionAmount;
	private String criticalIllnessPremiumAmount;
	
	private String tlPremiumAmount;
	private String permanentDisabilityProductionAmount;
	private String permanentDisabilityPremiumAmount;
	
	private String whatYouPayAmount;
	
	private String totalAmount;

	
	private String policySellerName;
	
	
	private String occupation;
	
	private String purchasedDate;
	private String planExpiringDate;

	
	private String agentCode;
	private String illustrationNumber;
	
	
	
	private List<NomineeDetails> nomiantedDetailsList;
	
	public String getPolicyHolderName() {
		return policyHolderName;
	}

	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}

	public String getPolicyHolderAge() {
		return policyHolderAge;
	}

	public void setPolicyHolderAge(String policyHolderAge) {
		this.policyHolderAge = policyHolderAge;
	}

	public String getPolicyHolderGender() {
		return policyHolderGender;
	}

	public void setPolicyHolderGender(String policyHolderGender) {
		this.policyHolderGender = policyHolderGender;
	}

	public String getPolicyHolderDOB() {
		return policyHolderDOB;
	}

	public void setPolicyHolderDOB(String policyHolderDOB) {
		this.policyHolderDOB = policyHolderDOB;
	}

	public String getPolicyHolderAddress() {
		return policyHolderAddress;
	}

	public void setPolicyHolderAddress(String policyHolderAddress) {
		this.policyHolderAddress = policyHolderAddress;
	}

	public String getPolicyHolderPhone() {
		return policyHolderPhone;
	}

	public void setPolicyHolderPhone(String policyHolderPhone) {
		this.policyHolderPhone = policyHolderPhone;
	}

	public String getPolicyHolderEmail() {
		return policyHolderEmail;
	}

	public void setPolicyHolderEmail(String policyHolderEmail) {
		this.policyHolderEmail = policyHolderEmail;
	}

	public String getPolicyHolderNID() {
		return policyHolderNID;
	}

	public void setPolicyHolderNID(String policyHolderNID) {
		this.policyHolderNID = policyHolderNID;
	}

	public String getProductionDuraion() {
		return productionDuraion;
	}

	public void setProductionDuraion(String productionDuraion) {
		this.productionDuraion = productionDuraion;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getTlProductionAmount() {
		return tlProductionAmount;
	}

	public void setTlProductionAmount(String tlProductionAmount) {
		this.tlProductionAmount = tlProductionAmount;
	}

	public String getTlPremiumAmount() {
		return tlPremiumAmount;
	}

	public void setTlPremiumAmount(String tlPremiumAmount) {
		this.tlPremiumAmount = tlPremiumAmount;
	}

	public String getPermanentDisabilityProductionAmount() {
		return permanentDisabilityProductionAmount;
	}

	public void setPermanentDisabilityProductionAmount(String permanentDisabilityProductionAmount) {
		this.permanentDisabilityProductionAmount = permanentDisabilityProductionAmount;
	}

	public String getPermanentDisabilityPremiumAmount() {
		return permanentDisabilityPremiumAmount;
	}

	public void setPermanentDisabilityPremiumAmount(String permanentDisabilityPremiumAmount) {
		this.permanentDisabilityPremiumAmount = permanentDisabilityPremiumAmount;
	}

	public String getCriticalIllnessProductionAmount() {
		return criticalIllnessProductionAmount;
	}

	public void setCriticalIllnessProductionAmount(String criticalIllnessProductionAmount) {
		this.criticalIllnessProductionAmount = criticalIllnessProductionAmount;
	}

	public String getCriticalIllnessPremiumAmount() {
		return criticalIllnessPremiumAmount;
	}

	public void setCriticalIllnessPremiumAmount(String criticalIllnessPremiumAmount) {
		this.criticalIllnessPremiumAmount = criticalIllnessPremiumAmount;
	}


	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public List<NomineeDetails> getNomiantedDetailsList() {
		return nomiantedDetailsList;
	}

	public void setNomiantedDetailsList(List<NomineeDetails> nomiantedDetailsList) {
		this.nomiantedDetailsList = nomiantedDetailsList;
	}

	public String getPolicySellerName() {
		return policySellerName;
	}

	public void setPolicySellerName(String policySellerName) {
		this.policySellerName = policySellerName;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getPurchasedDate() {
		return purchasedDate;
	}

	public void setPurchasedDate(String purchasedDate) {
		this.purchasedDate = purchasedDate;
	}

	public String getPlanExpiringDate() {
		return planExpiringDate;
	}

	public void setPlanExpiringDate(String planExpiringDate) {
		this.planExpiringDate = planExpiringDate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getWhatYouPayAmount() {
		return whatYouPayAmount;
	}

	public void setWhatYouPayAmount(String whatYouPayAmount) {
		this.whatYouPayAmount = whatYouPayAmount;
	}

	public String getIllustrationNumber() {
		return illustrationNumber;
	}

	public void setIllustrationNumber(String illustrationNumber) {
		this.illustrationNumber = illustrationNumber;
	}
	
	
	
	
	
	
	
	


}
