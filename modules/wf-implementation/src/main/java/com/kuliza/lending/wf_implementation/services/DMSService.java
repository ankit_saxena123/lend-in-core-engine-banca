package com.kuliza.lending.wf_implementation.services;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.engine_common.services.EngineCommonService;
import com.kuliza.lending.wf_implementation.base.BaseDMSService;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDocsDao;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserDocs;
import com.kuliza.lending.wf_implementation.pojo.FileDownloadInput;
import com.kuliza.lending.wf_implementation.pojo.FileUploadForm;
import com.kuliza.lending.wf_implementation.pojo.FileUploadResponse;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class DMSService extends BaseDMSService{

	@Autowired
	private EngineCommonService engineCommonService;
	
	private static final Logger logger = LoggerFactory.getLogger(DMSService.class);
	
	@SuppressWarnings("unchecked")
	public FileUploadResponse uploadFile(String userId, FileUploadForm uploadForm) {
		String documentId = null;
		String documentName = null;
		ApiResponse uploadResponse = null;
		try {
			uploadResponse = engineCommonService.uploadFile(uploadForm.getFile(), uploadForm.getDocumentType(), uploadForm.getLabel());
		} catch (IOException exe) {
			logger.error("Io Exception in uploading: " + exe.getMessage());
			return null;
		}
		if(uploadResponse != null && uploadResponse.getStatus() == 200 && uploadResponse.getData() != null) {
			try {
				Map<String, Object> uploadMap = (Map<String, Object>) uploadResponse.getData();
				if (uploadMap.containsKey(WfImplConstants.DMS_UPLOAD_RESPONSE_KEY)) {
					Map<String, Object> responseMap = (Map<String, Object>) uploadMap.get(WfImplConstants.DMS_UPLOAD_RESPONSE_KEY);
					if (responseMap.containsKey(WfImplConstants.DMS_UPLOAD_RESPONSE_DOC_ID_KEY)) {
						documentId = CommonHelperFunctions.getStringValue(responseMap.get(WfImplConstants.DMS_UPLOAD_RESPONSE_DOC_ID_KEY));
					}
					if (responseMap.containsKey(WfImplConstants.DMS_UPLOAD_RESPONSE_DOC_LABEL_KEY)) {
						documentName = CommonHelperFunctions.getStringValue(responseMap.get(WfImplConstants.DMS_UPLOAD_RESPONSE_DOC_LABEL_KEY));
					}
					FileUploadResponse fileUploadResponse = new FileUploadResponse(documentName, documentId);
					return fileUploadResponse;
				} else {
					logger.error("Response key not present in upload response");
					return null;
				}
			} catch (Exception e) {
				logger.error("There is some error in processing upload response.");
				return null;
			}
		}
		return null;
	}

	public byte[] downloadFile(String userId, FileDownloadInput fileDownloadInput) {
		try {
			return engineCommonService.downloadFile(fileDownloadInput.getDocumentId());
		} catch (Exception e) {
			logger.error("Error in downloading: " + e.getMessage());
			return null;
		}
	}
}