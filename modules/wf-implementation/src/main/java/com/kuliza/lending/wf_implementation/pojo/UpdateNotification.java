package com.kuliza.lending.wf_implementation.pojo;

public class UpdateNotification {

	private Boolean isPushNotification;
	private Boolean isSmsNotification;
	private Boolean isEmailNotification;
	private Boolean touchIdEnabled;
	private Boolean isPromotionPushNotification;
	private Boolean isPromotionSmsNotification;
	private Boolean isPromotionEmailNotification;
	
	public Boolean getIsPushNotification() {
		return isPushNotification;
	}
	public void setIsPushNotification(Boolean isPushNotification) {
		this.isPushNotification = isPushNotification;
	}
	public Boolean getIsSmsNotification() {
		return isSmsNotification;
	}
	public void setIsSmsNotification(Boolean isSmsNotification) {
		this.isSmsNotification = isSmsNotification;
	}
	public Boolean getIsEmailNotification() {
		return isEmailNotification;
	}
	public void setIsEmailNotification(Boolean isEmailNotification) {
		this.isEmailNotification = isEmailNotification;
	}
	public Boolean getTouchIdEnabled() {
		return touchIdEnabled;
	}
	public void setTouchIdEnabled(Boolean touchIdEnabled) {
		this.touchIdEnabled = touchIdEnabled;
	}
	public Boolean getIsPromotionPushNotification() {
		return isPromotionPushNotification;
	}
	public void setIsPromotionPushNotification(Boolean isPromotionPushNotification) {
		this.isPromotionPushNotification = isPromotionPushNotification;
	}
	public Boolean getIsPromotionSmsNotification() {
		return isPromotionSmsNotification;
	}
	public void setIsPromotionSmsNotification(Boolean isPromotionSmsNotification) {
		this.isPromotionSmsNotification = isPromotionSmsNotification;
	}
	public Boolean getIsPromotionEmailNotification() {
		return isPromotionEmailNotification;
	}
	public void setIsPromotionEmailNotification(Boolean isPromotionEmailNotification) {
		this.isPromotionEmailNotification = isPromotionEmailNotification;
	}

}
