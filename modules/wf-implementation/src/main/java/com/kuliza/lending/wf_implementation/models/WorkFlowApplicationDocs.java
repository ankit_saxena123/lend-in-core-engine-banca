package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name="wf_application_docs")
public class WorkFlowApplicationDocs extends BaseModel  {
	
	@Column(name="doc_type")
	private Integer docType;
	
	@Column(name="doc_id")
	private String docId;
	
	@ManyToOne
	@JoinColumn(name="wf_application_id")
	private WorkFlowApplication workFlowApplication;

	public Integer getDocType() {
		return docType;
	}

	public void setDocType(Integer docType) {
		this.docType = docType;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public WorkFlowApplication getWorkFlowApplication() {
		return workFlowApplication;
	}

	public void setWorkFlowApplication(WorkFlowApplication workFlowApplication) {
		this.workFlowApplication = workFlowApplication;
	}
	

}
