package com.kuliza.lending.wf_implementation.services;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECreditHypervergeIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserAddress;
import com.kuliza.lending.wf_implementation.pojo.UpdateNidData;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class UpdateNidDataService {

	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private WorkFlowAddressDao workflowAdressDao;

	@Autowired
	private FECreditHypervergeIntegration feCreditHypervergeIntegration;
	@Autowired
	private FECreditBancaBlackListService bancaBlackListService;

	private static final Logger logger = LoggerFactory.getLogger(UpdateNidDataService.class);

	public ApiResponse updateNidData(String userName, UpdateNidData updateNidData) {
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			logger.info("UpdateNidData --->"+mapper.writeValueAsString(updateNidData)+" for userName : "+userName);
		} catch (JsonProcessingException e) {
			logger.error("error while print log",e);
		}
		
		WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser != null) {
			workFlowUser.setDob(updateNidData.getDateOfBirth());
			workFlowUser.setNid_dob(updateNidData.getDateOfBirth());
			String gender=updateNidData.getGender();
			gender=Utils.convertVietnamGenderToEnglish(gender);
			workFlowUser.setGender(gender);
			workFlowUser.setNid_gender(gender);
			workFlowUser.setUsername(updateNidData.getName());
			workFlowUser.setNid_user_name(updateNidData.getName());
			workFlowUser.setNationality(Utils.convertVietnamNationalityToEnglish(updateNidData.getNationality()));
			workFlowUser.setEmail(updateNidData.getEmailAddress());
			String hyperVergeResponse = workFlowUser.getHyperVergeFrontResponse();
			Map<String, Object> hyperVergeFrontResponse = feCreditHypervergeIntegration
					.parseNationalIdFrontData(hyperVergeResponse);
			String nationalId = Utils
					.getStringValue(updateNidData.getNationalId());
			if (nationalId.isEmpty())
				 nationalId = Utils
					.getStringValue(hyperVergeFrontResponse.get(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY));
			if (!nationalId.isEmpty())
				workFlowUser.setNationalId(nationalId);
			String name = Utils.getStringValue(
					hyperVergeFrontResponse.get(WfImplConstants.BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY));
			String dob = Utils
					.getStringValue(hyperVergeFrontResponse.get(WfImplConstants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY));
			gender = Utils
					.getStringValue(hyperVergeFrontResponse.get(WfImplConstants.GENDER_FROM_HYPERVERGE_KEY));
			gender=feCreditHypervergeIntegration.getGender(gender);
			/*if (!name.isEmpty()) {
				workFlowUser.setNid_user_name(name);
			}*/
			if (!dob.isEmpty()) {
				workFlowUser.setNid_dob(dob);

			}
			if (!gender.isEmpty()) {
				workFlowUser.setNid_gender(gender);
			}
			List<WorkFlowUserAddress> workFlowAddressList = workflowAdressDao.findByWfUser(workFlowUser);
			WorkFlowUserAddress workFlowUserNIDAddress = null;
			WorkFlowUserAddress workFlowUserDeliveryAddress = null;
			if (workFlowAddressList != null && !workFlowAddressList.isEmpty()) {
				for (WorkFlowUserAddress workFlowUserAddress : workFlowAddressList) {
					String addressType = workFlowUserAddress.getAddressType();
					if (addressType.equalsIgnoreCase(Constants.NID_ADDRESS_KEY)) {
						workFlowUserNIDAddress = workFlowUserAddress;
					} else if (addressType.equalsIgnoreCase(Constants.DELIVERY_ADDRESS_KEY)) {
						workFlowUserDeliveryAddress = workFlowUserAddress;

					}

				}
			}
			if (workFlowUserNIDAddress == null) {
				workFlowUserNIDAddress = new WorkFlowUserAddress();
				workFlowUserNIDAddress.setWfUser(workFlowUser);
				workFlowUserNIDAddress.setAddressType(Constants.NID_ADDRESS_KEY);
			}
			if (workFlowUserDeliveryAddress == null && !Utils.getStringValue(updateNidData.getAddressLine()).isEmpty()) {
				workFlowUserDeliveryAddress = new WorkFlowUserAddress();
				workFlowUserDeliveryAddress.setWfUser(workFlowUser);
				workFlowUserDeliveryAddress.setAddressType(Constants.DELIVERY_ADDRESS_KEY);
			}

			workFlowUserNIDAddress.setAddressLine1(updateNidData.getCurrentPlaceOfResidence());
			workFlowUserNIDAddress.setCityDistrict(updateNidData.getPlaceOfOrigin());
			workFlowUserNIDAddress.setProvince(updateNidData.getPlaceOfOrigin());
			workflowAdressDao.save(workFlowUserNIDAddress);
			if (workFlowUserDeliveryAddress != null && !Utils.getStringValue(updateNidData.getAddressLine()).isEmpty()) {
				workFlowUserDeliveryAddress.setAddressLine1(updateNidData.getAddressLine());
				workFlowUserDeliveryAddress.setCityDistrict(updateNidData.getCity());
				workFlowUserDeliveryAddress.setProvince(updateNidData.getProvince());
				workflowAdressDao.save(workFlowUserDeliveryAddress);
			}
			//check whether NID is blackListed or not
			bancaBlackListService.bancaBlackListDetails(workFlowUser, nationalId);
			workFlowUserDao.save(workFlowUser);
			logger.info("NID Data updated Successfully");
			return new ApiResponse(HttpStatus.OK, Constants.NID_DATA_UPDATED_SUCCESS);
		} else {
			logger.info("user does not exist:" + userName);
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

}
