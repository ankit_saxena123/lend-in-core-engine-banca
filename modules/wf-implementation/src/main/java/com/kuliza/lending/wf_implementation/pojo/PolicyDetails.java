package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class PolicyDetails {

	@NotNull(message = "ProcessInstanceId cannot be null")
	private String processInstanceId;
	private String language;

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	

}
