package com.kuliza.lending.wf_implementation.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.AttachmentPart;

import omnidocsapi.SysResponseType;
import omnidocsapi.UploadDocResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.wf_implementation.services.FECDMSService;
import com.kuliza.lending.wf_implementation.utils.Constants;



@RestController
@RequestMapping("/dms")
public class FECDMSController {

	@Autowired
	public FECDMSService dmsServices;
	
	private static final Logger logger = LoggerFactory.getLogger(FECDMSController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/upload-document", consumes = "multipart/form-data")
	public ApiResponse uploadDocument(Principal principal,@RequestParam String docID, @RequestParam String appID,
			@RequestParam String moduleID, @RequestParam Boolean renameFile, @RequestParam MultipartFile file) {
		logger.info("<==============uploadDocument FECDMSController============>");
		logger.info("<==============docID: "+docID+"============APPID"+appID);
		try {
			UploadDocResponse response = dmsServices.uploadDocument(principal.getName(),docID, appID, moduleID, renameFile, file);
			SysResponseType sys = response.getSys();
			int code = sys.getCode();
			
			if(code==1){
				Map<String, String> map=new HashMap<String, String>();
				String dmsDocIdFromResponse = dmsServices.getDMSDocIdFromResponse(response);
				map.put(Constants.DOC_ID, dmsDocIdFromResponse);
				CustomLogger.log(Thread.currentThread().getStackTrace()[1].getMethodName(), null, null,
						JobType.OUTBOUND_API, null, null,
						"upload response: code: " + code + " ,dmsDocIdFromResponse: " + dmsDocIdFromResponse );
				return new ApiResponse(200, "SUCCESS", map);
			}
			return new ApiResponse(500, "FAILURE", response);
		} catch (Exception e) {
			CustomLogger.logException(Thread.currentThread().getStackTrace()[1].getMethodName(), null, null,
					JobType.OUTBOUND_API, docID, null, e,
					"uploadDocument returned Exception");
			return new ApiResponse(500, "FAILURE", e.toString());
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/download-document/{docId}")
	public void downloadDocument(@PathVariable String docId, @RequestParam Optional<String> volumeId,
			@RequestParam Boolean toPDF,
			@RequestParam(required = false) Boolean view, HttpServletResponse response) throws IOException {
		try {
			String volume = volumeId.isPresent() ? volumeId.get() : "";
			Boolean viewFlag = view != null && view ? view : false;
			AttachmentPart att = dmsServices.downloadDocument(docId, volume, toPDF);
			InputStream stream = att.getRawContent();
			response.setContentLengthLong(att.getSize());
			response.setContentType(att.getContentType());
			
			String fileName = att.getContentId();
			if (fileName.startsWith("<") && fileName.endsWith(">")) {
				fileName = att.getContentId().substring(1, att.getContentId().length() - 1);
			}
			if (viewFlag) {
				response.addHeader("Content-disposition", "inline;filename=" + fileName);
			} else {
				response.addHeader("Content-disposition", "attachment;filename=" + fileName);
			}
			IOUtils.copy(stream, response.getOutputStream());
			response.flushBuffer();
		} catch (Exception e) {
			CustomLogger.logException(Thread.currentThread().getStackTrace()[1].getMethodName(), null, null,
					JobType.OUTBOUND_API, docId, null, e,
					"downloadDocument returned Exception");
			response.sendError(500);
		}
	}
	
	

}