package com.kuliza.lending.wf_implementation.base;

import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.engine_common.configs.OTPConfig;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.services.FECreditOTPService;
import com.kuliza.lending.wf_implementation.utils.OTPConstants;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DefaultOTPService extends BaseUserValidationService{


@Autowired
  private FECreditOTPService fecCreditOtpService;

  @Autowired
  private OTPServiceWithCache otpServiceWithCache;

  @Autowired
  private OTPConfig otpConfig;

  @Autowired
  private WfImplConfig wfImplConfig;
  
 @Autowired 
  private OTPService otpService;

  private static final Logger logger = LoggerFactory.getLogger(DefaultOTPService.class);

  @Override
  protected ApiResponse sendOTP(String mobileNumber, String otp) {
    return null;
  }

  private String generateOTP(HttpServletRequest request,
      UserPasswordGenerationForm userForm) {
    if(wfImplConfig.getGenerateOTP()){
     
        return fecCreditOtpService.generateOTP(userForm);
      }
    
    return OTPConstants.OTP_SKIP;
  }

  @Override
  protected ApiResponse generateAndSendPassword(HttpServletRequest request,
      UserPasswordGenerationForm userForm) {
    return null;
  }

  @Override
  @LogMethodDetails
  protected Boolean validateUser(HttpServletRequest request,
      UserValidationinputForm validationForm) {
    if (otpConfig.getUseCacheForOTP()) {
      return otpServiceWithCache.checkBlockedOrValidateOTP(request, validationForm);
    }
    return otpService.checkBlockedOrValidateOTP(request, validationForm);
  }

  @Override
  @LogMethodDetails
  public ApiResponse generateUserPassword(HttpServletRequest request,
      UserPasswordGenerationForm userForm) throws Exception {
    String otp = this.generateOTP(request, userForm);
    if (otp != null) {
      return new ApiResponse(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
    }
    return new ApiResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
  }

  /**
   * @param validationForm
   * @return apiResponse
   * @throws Exception
   * Service to validate and register user
   */
  @Override
  @LogMethodDetails
  public ApiResponse validateAndRegisterUser(HttpServletRequest request,
      UserValidationinputForm validationForm)
      throws Exception {
    return super.validateAndRegisterUser(request, validationForm);

  }
  
  /**
   * @param validationForm
   * @return apiResponse
   * @throws Exception
   * Service to validate and register user
   */
  @Override
  @LogMethodDetails
  public ApiResponse validateOtp(HttpServletRequest request,
      UserValidationinputForm validationForm)
      throws Exception {
    return super.validateOtp(request, validationForm);

  }
  
}
