package com.kuliza.lending.wf_implementation.pojo;

public class PaymentStatus {
	
	private String procInstId;
	private String status;
	
	public String getProcInstId() {
		return procInstId;
	}
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
