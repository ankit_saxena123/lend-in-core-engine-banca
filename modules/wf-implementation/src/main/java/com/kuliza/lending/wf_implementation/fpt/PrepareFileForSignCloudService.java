package com.kuliza.lending.wf_implementation.fpt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.pojo.FPTFileRequest;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.ESignCloudConstants;
import com.kuliza.lending.wf_implementation.utils.FPTUtils;
import com.kuliza.lending.wf_implementation.utils.SSLUtilities;
import com.kuliza.lending.wf_implementation.utils.Utils;

import vn.com.fis.ws.CredentialData;
import vn.com.fis.ws.MultipleSigningFileData;
import vn.com.fis.ws.Services;
import vn.com.fis.ws.Services_Service;
import vn.com.fis.ws.SignCloudMetaData;
import vn.com.fis.ws.SignCloudReq;
import vn.com.fis.ws.SignCloudResp;

@Service
public class PrepareFileForSignCloudService {
	
	@Autowired
	private WfImplConfig wfImplConfig;
	
	@Autowired
	private FPTUtils fptUtils;

	Logger logger = LoggerFactory.getLogger(PrepareFileForSignCloudService.class);

	public SignCloudResp prepareFilesForFPTSignCloud(String agreementUUID, List<FPTFileRequest> fileList, Map<String, Object> dataToEmbed) throws Exception { 
		logger.info("AgreementUUid : "+agreementUUID+" Data file size : "+fileList.size());
		SSLUtilities.trustAllHostnames();
		SSLUtilities.trustAllHttpsCertificates();
		Services_Service services = fptUtils.createFPTService();

		Services ws = services.getServicesPort();
		SignCloudReq signCloudReq = getFPTRequestForMutipleFiles(agreementUUID, fileList, dataToEmbed);

		ObjectMapper map = new ObjectMapper();
//		logger.info("PrepareFile Request For FPT : " + map.writeValueAsString(signCloudReq));

		SignCloudResp signCloudResp = ws.prepareFileForSignCloud(signCloudReq);
		logger.info("PrepareFile Response From FPT : " + map.writeValueAsString(signCloudResp));
		logger.info("Code: " + signCloudResp.getResponseCode());
		logger.info("Message: " + signCloudResp.getResponseMessage());
		logger.info("BillCode: " + signCloudResp.getBillCode());
		
		logger.info("**************************************************************");
		logger.info("NotificationMessage: " + signCloudResp.getNotificationMessage());
		logger.info("**************************************************************");
		logger.info("NotificationSubject: " + signCloudResp.getNotificationSubject());
		logger.info("RemainingCounter: " + signCloudResp.getRemainingCounter());

//		Map<String, Object> fptResponse = new HashMap<String, Object>();
//		fptResponse.put(Constants.FPT_STATUS, signCloudResp.getResponseCode());
//		fptResponse.put(Constants.FPT_OTP, signCloudResp.getAuthorizeCredential());
//		fptResponse.put(Constants.FPT_AUTH_SUBJECT, signCloudResp.getNotificationSubject());
//		fptResponse.put(Constants.FPT_AUTH_MESSAGE, signCloudResp.getNotificationMessage());
//		fptResponse.put(Constants.FPT_BILL_CODE, signCloudResp.getBillCode());
//		fptResponse.put(Constants.FPT_REMAINING_COUNTER, signCloudResp.getRemainingCounter());
		
		return signCloudResp;
	}

	private SignCloudReq getFPTRequestForMutipleFiles(String agreementUUID, List<FPTFileRequest> fileList,
			Map<String, Object> dataToEmbed) throws Exception {

		SignCloudReq signCloudReq = new SignCloudReq();
		signCloudReq.setRelyingParty(wfImplConfig.getFptRelyingParty());
		signCloudReq.setAgreementUUID(agreementUUID);
		signCloudReq.setAuthorizeMethod(ESignCloudConstants.AUTHORISATION_METHOD_SMS);
		
//		String fileName = files.get(Constants.FPT_FILE_FILE_ONE);
		
		for (FPTFileRequest fptFileRequest : fileList) {
			MultipleSigningFileData firstNewSigningFileData = getFileDetails(agreementUUID, dataToEmbed, fptFileRequest);
			
			signCloudReq.getMultipleSigningFileData().add(firstNewSigningFileData);
		}
		
		signCloudReq.setNotificationTemplate(ESignCloudConstants.NOTIFICATION_TEMPLATE);
		signCloudReq.setNotificationSubject(ESignCloudConstants.NOTIFICATION_SUBJECT);

		CredentialData credentialData = new CredentialData();
		credentialData.setUsername(wfImplConfig.getFptRelyingPartyUserName());
		credentialData.setPassword(wfImplConfig.getFptRelyingPartyPassword());
		String timestamp = String.valueOf(System.currentTimeMillis());

		credentialData.setTimestamp(timestamp);
		credentialData.setSignature(wfImplConfig.getFptRelyingPartySignature());
		
		String pkcs1Signature = fptUtils.getPKCS1Signature(timestamp);
		credentialData.setPkcs1Signature(pkcs1Signature);
		signCloudReq.setCredentialData(credentialData);
		
		return signCloudReq;
	}

	public MultipleSigningFileData getFileDetails(String agreementUUID, Map<String, Object> dataToEmbed,
			FPTFileRequest fptFileRequest) throws IOException, FileNotFoundException {
		File insuranceFile = getFile(fptFileRequest.getName());
		
		SignCloudMetaData signCloudMetaDataForInsuranceItem = new SignCloudMetaData();
		
		SignCloudMetaData.SingletonSigning customerSignin = getCustomerSigning(dataToEmbed,fptFileRequest);
		signCloudMetaDataForInsuranceItem.setSingletonSigning(customerSignin);
		
		if(fptFileRequest.isCounterSignNeed()) {
			SignCloudMetaData.CounterSigning counterSigning = getCounterSigning(fptFileRequest);
			signCloudMetaDataForInsuranceItem.setCounterSigning(counterSigning);
		}
		
		byte[] insuranceFileData = IOUtils.toByteArray(new FileInputStream(insuranceFile));
		String insuranceMimeType = ESignCloudConstants.MIMETYPE_PDF;
		String insuranceFileName = fptFileRequest.getName();
		
		MultipleSigningFileData newSigningFileData = new MultipleSigningFileData();
		newSigningFileData.setSigningFileData(insuranceFileData);
		newSigningFileData.setMimeType(insuranceMimeType);
	    newSigningFileData.setSigningFileName(insuranceFileName);
		newSigningFileData.setSignCloudMetaData(signCloudMetaDataForInsuranceItem);
		return newSigningFileData;
	}


	public File getFile(String fileName) {
		String fptFileDirectory = wfImplConfig.getFptFilePath();
		logger.info("FPT File full path : "+fptFileDirectory+fileName);
		File file = new File(fptFileDirectory+fileName);
		return file;
	}

	public SignCloudMetaData.SingletonSigning getCustomerSigning(Map<String, Object> dataToEmbed, FPTFileRequest fptFileRequest) {
		
		SignCloudMetaData.SingletonSigning.Entry counterSignEnabledForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		counterSignEnabledForInsuranceItem.setKey("COUNTERSIGNENABLED");
		counterSignEnabledForInsuranceItem.setValue("True");

		SignCloudMetaData.SingletonSigning.Entry signatureImageForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		signatureImageForInsuranceItem.setKey("SIGNATUREIMAGE");
		signatureImageForInsuranceItem.setValue(ESignCloudConstants.FEC_SIGNATURE);
		
		SignCloudMetaData.SingletonSigning.Entry pageNoForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		pageNoForInsuranceItem.setKey("PAGENO");
		pageNoForInsuranceItem.setValue("Last");

		SignCloudMetaData.SingletonSigning.Entry positionIdentifiderForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		positionIdentifiderForInsuranceItem.setKey("POSITIONIDENTIFIER");
		positionIdentifiderForInsuranceItem.setValue(fptFileRequest.getPositionIdentifier());

		SignCloudMetaData.SingletonSigning.Entry rectangleOffsetForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		rectangleOffsetForInsuranceItem.setKey("RECTANGLEOFFSET");
		rectangleOffsetForInsuranceItem.setValue(fptFileRequest.getCoordinates());
		
		SignCloudMetaData.SingletonSigning.Entry rectangleSizeForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		rectangleSizeForInsuranceItem.setKey("RECTANGLESIZE");
		rectangleSizeForInsuranceItem.setValue("170,70");

		SignCloudMetaData.SingletonSigning.Entry visibleSignatureForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		visibleSignatureForInsuranceItem.setKey("VISIBLESIGNATURE");

		visibleSignatureForInsuranceItem.setValue(fptFileRequest.isCustomerSignNeed() ? "True":"False");

		SignCloudMetaData.SingletonSigning.Entry visualStatusForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		visualStatusForInsuranceItem.setKey("VISUALSTATUS");
		visualStatusForInsuranceItem.setValue("False");

		SignCloudMetaData.SingletonSigning.Entry imageAndTextForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		imageAndTextForInsuranceItem.setKey("IMAGEANDTEXT");
		imageAndTextForInsuranceItem.setValue(fptFileRequest.isCustomerSignNeed() ? "True":"False");

		SignCloudMetaData.SingletonSigning.Entry textDirectionForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		textDirectionForInsuranceItem.setKey("TEXTDIRECTION");
		textDirectionForInsuranceItem.setValue("LEFTTORIGHT");

		SignCloudMetaData.SingletonSigning.Entry showSignerInfoForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		showSignerInfoForInsuranceItem.setKey("SHOWSIGNERINFO");
		showSignerInfoForInsuranceItem.setValue(fptFileRequest.isCustomerSignNeed() ? "True":"False");

		SignCloudMetaData.SingletonSigning.Entry signerInfoPrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		signerInfoPrefixForInsuranceItem.setKey("SIGNERINFOPREFIX");
		signerInfoPrefixForInsuranceItem.setValue("Ký bởi:");

		SignCloudMetaData.SingletonSigning.Entry showDateTimeForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		showDateTimeForInsuranceItem.setKey("SHOWDATETIME");
		showDateTimeForInsuranceItem.setValue(fptFileRequest.isCustomerSignNeed() ? "True":"False");

		SignCloudMetaData.SingletonSigning.Entry dateTimePrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		dateTimePrefixForInsuranceItem.setKey("DATETIMEPREFIX");
		dateTimePrefixForInsuranceItem.setValue("Ký ngày:");

		SignCloudMetaData.SingletonSigning.Entry showReasonForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		showReasonForInsuranceItem.setKey("SHOWREASON");
		showReasonForInsuranceItem.setValue(fptFileRequest.isCustomerSignNeed() ? "True":"False");

		SignCloudMetaData.SingletonSigning.Entry signReasonPrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		signReasonPrefixForInsuranceItem.setKey("SIGNREASONPREFIX");
		signReasonPrefixForInsuranceItem.setValue("Lí do ký:");

		SignCloudMetaData.SingletonSigning.Entry signReasonForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		signReasonForInsuranceItem.setKey("SIGNREASON");
		signReasonForInsuranceItem.setValue("Tôi xác nhận đồng ý");

		SignCloudMetaData.SingletonSigning.Entry showLocationForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		showLocationForInsuranceItem.setKey("SHOWLOCATION");
		showLocationForInsuranceItem.setValue(fptFileRequest.isCustomerSignNeed() ? "True":"False");

		SignCloudMetaData.SingletonSigning.Entry locationForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		locationForInsuranceItem.setKey("LOCATION");
		locationForInsuranceItem.setValue("Việt Nam");

		SignCloudMetaData.SingletonSigning.Entry locationPrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		locationPrefixForInsuranceItem.setKey("LOCATIONPREFIX");
		locationPrefixForInsuranceItem.setValue("Nơi ký:");

		SignCloudMetaData.SingletonSigning.Entry textColorForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		textColorForInsuranceItem.setKey("TEXTCOLOR");
		textColorForInsuranceItem.setValue("black");

		SignCloudMetaData.SingletonSigning.Entry lockAfterSigningForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
		lockAfterSigningForInsuranceItem.setKey("LOCKAFTERSIGNING");
		lockAfterSigningForInsuranceItem.setValue("True");
		
		SignCloudMetaData.SingletonSigning.Entry  showEmailForCommonItem=null, emailPrefixForCommonItem=null, showGeoCoordinateForCommonItem=null, geoCoordinatePrefixForCommonItem=null,
				geoCoordinateForCommonItem=null, showIPAddrForCommonItem=null, ipAddrPrefixForCommonItem=null, ipAddrForCommonItem=null, emailCommonItem;
		//Common data embed
		if(dataToEmbed !=null) {
			showEmailForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    showEmailForCommonItem.setKey("SHOWEMAIL");
		    showEmailForCommonItem.setValue("True");
		    
		    emailPrefixForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    emailPrefixForCommonItem.setKey("EMAILPREFIX");
		    emailPrefixForCommonItem.setValue("Email:");
		    
		    showGeoCoordinateForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    showGeoCoordinateForCommonItem.setKey("SHOWGEOCOORDINATE");
		    showGeoCoordinateForCommonItem.setValue("False");
		    
		    emailCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    emailCommonItem.setKey("EMAIL");
		    emailCommonItem.setValue(Utils.getStringValue(dataToEmbed.getOrDefault(Constants.BANCA_EMAIL_ID_KEY,"")));
		    
		    geoCoordinatePrefixForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    geoCoordinatePrefixForCommonItem.setKey("GEOCOORDINATEPREFIX");
		    geoCoordinatePrefixForCommonItem.setValue("Coordinates:");
		    
		    geoCoordinateForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    geoCoordinateForCommonItem.setKey("GEOCOORDINATE");
		    geoCoordinateForCommonItem.setValue(Utils.getStringValue(dataToEmbed.getOrDefault(Constants.LATITUDE_KEY, "")+","+dataToEmbed.getOrDefault(Constants.LONGITUDE_KEY, "")));
		    
		    showIPAddrForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    showIPAddrForCommonItem.setKey("SHOWIPADDRESS");
		    showIPAddrForCommonItem.setValue("True");
		    
		    ipAddrPrefixForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    ipAddrPrefixForCommonItem.setKey("IPADDRESSPREFIX");
		    ipAddrPrefixForCommonItem.setValue("IP Addr:");
		    
		    ipAddrForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
		    ipAddrForCommonItem.setKey("IPADDRESS");
		    ipAddrForCommonItem.setValue(Utils.getStringValue(dataToEmbed.getOrDefault(Constants.CUSTOMER_IP_ADDRESS, "")));
		}
		
		
		SignCloudMetaData.SingletonSigning singletonSigningForInsurance = new SignCloudMetaData.SingletonSigning();
		singletonSigningForInsurance.getEntry().add(counterSignEnabledForInsuranceItem);
		//singletonSigningForInsurance.getEntry().add(signatureImageForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(pageNoForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(positionIdentifiderForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(rectangleOffsetForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(rectangleSizeForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(visibleSignatureForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(visualStatusForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(imageAndTextForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(textDirectionForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(showSignerInfoForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(signerInfoPrefixForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(showDateTimeForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(dateTimePrefixForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(showReasonForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(signReasonPrefixForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(signReasonForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(showLocationForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(locationForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(locationPrefixForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(textColorForInsuranceItem);
		singletonSigningForInsurance.getEntry().add(lockAfterSigningForInsuranceItem);
		//Common data to embedd
		if(dataToEmbed !=null) {
			singletonSigningForInsurance.getEntry().add(showEmailForCommonItem);
		    singletonSigningForInsurance.getEntry().add(emailPrefixForCommonItem);
		    singletonSigningForInsurance.getEntry().add(showGeoCoordinateForCommonItem);
		    singletonSigningForInsurance.getEntry().add(geoCoordinatePrefixForCommonItem);
		    singletonSigningForInsurance.getEntry().add(geoCoordinateForCommonItem);
		    singletonSigningForInsurance.getEntry().add(showIPAddrForCommonItem);
		    singletonSigningForInsurance.getEntry().add(ipAddrPrefixForCommonItem);
		    singletonSigningForInsurance.getEntry().add(ipAddrForCommonItem);
		}
		return singletonSigningForInsurance;
	}
	
	private SignCloudReq getFPTRequestWithTwoFilesCopy(String agreementUUID, List<String> fileList,
			Map<String, Object> dataToEmbed) throws Exception {
		
			
//
//		SignCloudReq signCloudReq = new SignCloudReq();
//		signCloudReq.setRelyingParty(wfImplConfig.getFptRelyingParty());
//		signCloudReq.setAgreementUUID(agreementUUID);
//		signCloudReq.setAuthorizeMethod(ESignCloudConstants.AUTHORISATION_METHOD_SMS);
//		
////		String fileName = files.get(Constants.FPT_FILE_FILE_ONE);
//		
//		for (String fileName : fileList) {
//			MultipleSigningFileData firstNewSigningFileData = null;
////					getFileDetails(agreementUUID, dataToEmbed, fileName);
//			
//			signCloudReq.getMultipleSigningFileData().add(firstNewSigningFileData);
//		}
//		
////		MultipleSigningFileData secondNewSigningFileData = getFileDetails(agreementUUID, dataToEmbed, fileName);
////		signCloudReq.getMultipleSigningFileData().add(secondNewSigningFileData);
//
//		
//		/// ITEM 02
//		File loanFile = new File("");
//				
//		byte[] loanFileData = IOUtils.toByteArray(new FileInputStream(loanFile));
//		String loanMimeType = ESignCloudConstants.MIMETYPE_PDF;
//		String loanFileName = "LOAN-"+agreementUUID+"-" + System.nanoTime() + ".pdf";
//		MultipleSigningFileData loanItem = new MultipleSigningFileData();
//		loanItem.setSigningFileData(loanFileData);
//		loanItem.setMimeType(loanMimeType);
//		loanItem.setSigningFileName(loanFileName);
//
//		SignCloudMetaData signCloudMetaDataForLoanItem = new SignCloudMetaData();
//
//		// -- SingletonSigning (Signature properties for customer)
//
//		SignCloudMetaData.SingletonSigning singletonSigningForLoan = new SignCloudMetaData.SingletonSigning();
//
//		SignCloudMetaData.SingletonSigning.Entry counterSignEnabledForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		counterSignEnabledForLoanItem.setKey("COUNTERSIGNENABLED");
//		counterSignEnabledForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry signatureImageForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signatureImageForLoanItem.setKey("SIGNATUREIMAGE");
//		signatureImageForLoanItem.setValue(ESignCloudConstants.FEC_SIGNATURE);
//
//		SignCloudMetaData.SingletonSigning.Entry pageNoForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		pageNoForLoanItem.setKey("PAGENO");
//		pageNoForLoanItem.setValue("Last");
//
//		SignCloudMetaData.SingletonSigning.Entry positionIdentifiderForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		positionIdentifiderForLoanItem.setKey("POSITIONIDENTIFIER");
//		positionIdentifiderForLoanItem.setValue("Chữ ký của KH");
//
//		SignCloudMetaData.SingletonSigning.Entry rectangleOffsetForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		rectangleOffsetForLoanItem.setKey("RECTANGLEOFFSET");
//		rectangleOffsetForLoanItem.setValue("-30,-100");
//
//		SignCloudMetaData.SingletonSigning.Entry rectangleSizeForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		rectangleSizeForLoanItem.setKey("RECTANGLESIZE");
//		rectangleSizeForLoanItem.setValue("170,70");
//
//		SignCloudMetaData.SingletonSigning.Entry visibleSignatureForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		visibleSignatureForLoanItem.setKey("VISIBLESIGNATURE");
//		visibleSignatureForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry visualStatusForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		visualStatusForLoanItem.setKey("VISUALSTATUS");
//		visualStatusForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry imageAndTextForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		imageAndTextForLoanItem.setKey("IMAGEANDTEXT");
//		imageAndTextForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry textDirectionForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		textDirectionForLoanItem.setKey("TEXTDIRECTION");
//		textDirectionForLoanItem.setValue("LEFTTORIGHT");
//
//		SignCloudMetaData.SingletonSigning.Entry showSignerInfoForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showSignerInfoForLoanItem.setKey("SHOWSIGNERINFO");
//		showSignerInfoForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry signerInfoPrefixForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signerInfoPrefixForLoanItem.setKey("SIGNERINFOPREFIX");
//		signerInfoPrefixForLoanItem.setValue("Sign by:");
//
//		SignCloudMetaData.SingletonSigning.Entry showDateTimeForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showDateTimeForLoanItem.setKey("SHOWDATETIME");
//		showDateTimeForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry dateTimePrefixForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		dateTimePrefixForLoanItem.setKey("DATETIMEPREFIX");
//		dateTimePrefixForLoanItem.setValue("Sign on:");
//
//		SignCloudMetaData.SingletonSigning.Entry showReasonForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showReasonForLoanItem.setKey("SHOWREASON");
//		showReasonForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry signReasonPrefixForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signReasonPrefixForLoanItem.setKey("SIGNREASONPREFIX");
//		signReasonPrefixForLoanItem.setValue("Sign for:");
//
//		SignCloudMetaData.SingletonSigning.Entry signReasonForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signReasonForLoanItem.setKey("SIGNREASON");
//		signReasonForLoanItem.setValue("I agree the agreement");
//
//		SignCloudMetaData.SingletonSigning.Entry showLocationForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showLocationForLoanItem.setKey("SHOWLOCATION");
//		showLocationForLoanItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry locationForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		locationForLoanItem.setKey("LOCATION");
//		locationForLoanItem.setValue("Ho Chi Minh");
//
//		SignCloudMetaData.SingletonSigning.Entry locationPrefixForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		locationPrefixForLoanItem.setKey("LOCATIONPREFIX");
//		locationPrefixForLoanItem.setValue("Sign location:");
//
//		SignCloudMetaData.SingletonSigning.Entry textColorForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		textColorForLoanItem.setKey("TEXTCOLOR");
//		textColorForLoanItem.setValue("black");
//
//		SignCloudMetaData.SingletonSigning.Entry lockAfterSigningForLoanItem = new SignCloudMetaData.SingletonSigning.Entry();
//		lockAfterSigningForLoanItem.setKey("LOCKAFTERSIGNING");
//		lockAfterSigningForLoanItem.setValue("False");
//
//		singletonSigningForLoan.getEntry().add(counterSignEnabledForLoanItem);
//		singletonSigningForLoan.getEntry().add(signatureImageForLoanItem);
//		singletonSigningForLoan.getEntry().add(pageNoForLoanItem);
//		singletonSigningForLoan.getEntry().add(positionIdentifiderForLoanItem);
//		singletonSigningForLoan.getEntry().add(rectangleOffsetForLoanItem);
//		singletonSigningForLoan.getEntry().add(rectangleSizeForLoanItem);
//		singletonSigningForLoan.getEntry().add(visibleSignatureForLoanItem);
//		singletonSigningForLoan.getEntry().add(visualStatusForLoanItem);
//		singletonSigningForLoan.getEntry().add(imageAndTextForLoanItem);
//		singletonSigningForLoan.getEntry().add(textDirectionForLoanItem);
//		singletonSigningForLoan.getEntry().add(showSignerInfoForLoanItem);
//		singletonSigningForLoan.getEntry().add(signerInfoPrefixForLoanItem);
//		singletonSigningForLoan.getEntry().add(showDateTimeForLoanItem);
//		singletonSigningForLoan.getEntry().add(dateTimePrefixForLoanItem);
//		singletonSigningForLoan.getEntry().add(showReasonForLoanItem);
//		singletonSigningForLoan.getEntry().add(signReasonPrefixForLoanItem);
//		singletonSigningForLoan.getEntry().add(signReasonForLoanItem);
//		singletonSigningForLoan.getEntry().add(showLocationForLoanItem);
//		singletonSigningForLoan.getEntry().add(locationForLoanItem);
//		singletonSigningForLoan.getEntry().add(locationPrefixForLoanItem);
//		singletonSigningForLoan.getEntry().add(textColorForLoanItem);
//		singletonSigningForLoan.getEntry().add(lockAfterSigningForLoanItem);
////		//Common data to embedd
//
//		signCloudMetaDataForLoanItem.setSingletonSigning(singletonSigningForLoan);
//
////		// -- CounterSigning (Signature properties for FE)
//		signCloudMetaDataForLoanItem.setCounterSigning(getCounterSigning(null));
//
//		loanItem.setSignCloudMetaData(signCloudMetaDataForLoanItem);
//
////		signCloudReq.getMultipleSigningFileData().add(firstNewSigningFileData);
//		signCloudReq.getMultipleSigningFileData().add(loanItem);
//
//		signCloudReq.setNotificationTemplate(ESignCloudConstants.NOTIFICATION_TEMPLATE);
//		signCloudReq.setNotificationSubject(ESignCloudConstants.NOTIFICATION_SUBJECT);
//
//		CredentialData credentialData = new CredentialData();
//		credentialData.setUsername(wfImplConfig.getFptRelyingPartyUserName());
//		credentialData.setPassword(wfImplConfig.getFptRelyingPartyPassword());
//		String timestamp = String.valueOf(System.currentTimeMillis());
//
//		credentialData.setTimestamp(timestamp);
//		credentialData.setSignature(wfImplConfig.getFptRelyingPartySignature());
//		
////		String data2sign = ESignCloudConstants.RELYING_PARTY_USER + ESignCloudConstants.RELYING_PARTY_PASSWORD
////				+ ESignCloudConstants.RELYING_PARTY_SIGNATURE + timestamp;
////		String pkcs1Signature = FPTUtils.getPKCS1Signature(data2sign, ESignCloudConstants.RELYING_PARTY_KEY_STORE,
////				ESignCloudConstants.RELYING_PARTY_KEY_STORE_PASSWORD);
//		
//		String pkcs1Signature = fptUtils.getPKCS1Signature(timestamp);
//		credentialData.setPkcs1Signature(pkcs1Signature);
//		signCloudReq.setCredentialData(credentialData);
//		
//		return signCloudReq;
		return null;
	}

	public SignCloudMetaData.CounterSigning getCounterSigning(FPTFileRequest fptFileRequest) {
		SignCloudMetaData.CounterSigning counterSigningForLoanItem = new SignCloudMetaData.CounterSigning();

//		SignCloudMetaData.CounterSigning.Entry counterSigningSignatureImageForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
//		counterSigningSignatureImageForLoanItem.setKey("SIGNATUREIMAGE");
//		counterSigningSignatureImageForLoanItem.setValue(ESignCloudConstants.FEC_SIGNATURE);

		SignCloudMetaData.CounterSigning.Entry counterSigningPageNoForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningPageNoForLoanItem.setKey("PAGENO");
		counterSigningPageNoForLoanItem.setValue("Last");

		SignCloudMetaData.CounterSigning.Entry counterSigningPositionIdentifiderForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningPositionIdentifiderForLoanItem.setKey("POSITIONIDENTIFIER");
		counterSigningPositionIdentifiderForLoanItem.setValue(fptFileRequest.getCounterPositionIdentifier());

		SignCloudMetaData.CounterSigning.Entry counterSigningRectangleOffsetForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningRectangleOffsetForLoanItem.setKey("RECTANGLEOFFSET");
		counterSigningRectangleOffsetForLoanItem.setValue(fptFileRequest.getCounterCoordinates());

		SignCloudMetaData.CounterSigning.Entry counterSigningRectangleSizeForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningRectangleSizeForLoanItem.setKey("RECTANGLESIZE");
		counterSigningRectangleSizeForLoanItem.setValue("170,70");

		SignCloudMetaData.CounterSigning.Entry counterSigningVisibleSignatureForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningVisibleSignatureForLoanItem.setKey("VISIBLESIGNATURE");
		counterSigningVisibleSignatureForLoanItem.setValue("True");

		SignCloudMetaData.CounterSigning.Entry counterSigningVisualStatusForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningVisualStatusForLoanItem.setKey("VISUALSTATUS");
		counterSigningVisualStatusForLoanItem.setValue("False");

		SignCloudMetaData.CounterSigning.Entry counterSigningImageAndTextForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningImageAndTextForLoanItem.setKey("IMAGEANDTEXT");
		counterSigningImageAndTextForLoanItem.setValue("True");

		SignCloudMetaData.CounterSigning.Entry counterSigningTextDirectionForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningTextDirectionForLoanItem.setKey("TEXTDIRECTION");
		counterSigningTextDirectionForLoanItem.setValue("LEFTTORIGHT");

		SignCloudMetaData.CounterSigning.Entry counterSigningShowSignerInfoForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningShowSignerInfoForLoanItem.setKey("SHOWSIGNERINFO");
		counterSigningShowSignerInfoForLoanItem.setValue("True");

		SignCloudMetaData.CounterSigning.Entry counterSigningSignerInfoPrefixForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningSignerInfoPrefixForLoanItem.setKey("SIGNERINFOPREFIX");
		counterSigningSignerInfoPrefixForLoanItem.setValue("Ký bởi:");

		SignCloudMetaData.CounterSigning.Entry counterSigningShowDateTimeForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningShowDateTimeForLoanItem.setKey("SHOWDATETIME");
		counterSigningShowDateTimeForLoanItem.setValue("True");

		SignCloudMetaData.CounterSigning.Entry counterSigningDateTimePrefixForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningDateTimePrefixForLoanItem.setKey("DATETIMEPREFIX");
		counterSigningDateTimePrefixForLoanItem.setValue("Ký ngày:");

		SignCloudMetaData.CounterSigning.Entry counterSigningShowReasonForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningShowReasonForLoanItem.setKey("SHOWREASON");
		counterSigningShowReasonForLoanItem.setValue("False");

		SignCloudMetaData.CounterSigning.Entry counterSigningSignReasonPrefixForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningSignReasonPrefixForLoanItem.setKey("SIGNREASONPREFIX");
		counterSigningSignReasonPrefixForLoanItem.setValue("Sign for:");

		SignCloudMetaData.CounterSigning.Entry counterSigningSignReasonForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningSignReasonForLoanItem.setKey("SIGNREASON");
		counterSigningSignReasonForLoanItem.setValue("I agree the agreement");

		SignCloudMetaData.CounterSigning.Entry counterSigningShowLocationForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningShowLocationForLoanItem.setKey("SHOWLOCATION");
		counterSigningShowLocationForLoanItem.setValue("True");

		SignCloudMetaData.CounterSigning.Entry counterSigningLocationForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningLocationForLoanItem.setKey("LOCATION");
		counterSigningLocationForLoanItem.setValue("TP.Hồ Chí Minh");

		SignCloudMetaData.CounterSigning.Entry counterSigningLocationPrefixForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningLocationPrefixForLoanItem.setKey("LOCATIONPREFIX");
		counterSigningLocationPrefixForLoanItem.setValue("Nơi ký:");

		SignCloudMetaData.CounterSigning.Entry counterSigningTextColorForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningTextColorForLoanItem.setKey("TEXTCOLOR");
		counterSigningTextColorForLoanItem.setValue("black");

		SignCloudMetaData.CounterSigning.Entry counterSigningLockAfterSigningForLoanItem = new SignCloudMetaData.CounterSigning.Entry();
		counterSigningLockAfterSigningForLoanItem.setKey("LOCKAFTERSIGNING");
		counterSigningLockAfterSigningForLoanItem.setValue("True");
//		counterSigningForLoanItem.getEntry().add(counterSigningSignatureImageForLoanItem);
		
		counterSigningForLoanItem.getEntry().add(counterSigningPageNoForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningPositionIdentifiderForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningRectangleOffsetForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningRectangleSizeForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningVisibleSignatureForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningVisualStatusForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningImageAndTextForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningTextDirectionForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningShowSignerInfoForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningSignerInfoPrefixForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningShowDateTimeForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningDateTimePrefixForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningShowReasonForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningSignReasonPrefixForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningSignReasonForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningShowLocationForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningLocationForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningLocationPrefixForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningTextColorForLoanItem);
		counterSigningForLoanItem.getEntry().add(counterSigningLockAfterSigningForLoanItem);

		return counterSigningForLoanItem;
	}

	public SignCloudMetaData.SingletonSigning getSingletonSigning(Map<String, Object> dataToEmbed) {
		
		SignCloudMetaData.SingletonSigning.Entry counterSignEnabledForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		counterSignEnabledForInsuranceItem.setKey("COUNTERSIGNENABLED");
//		counterSignEnabledForInsuranceItem.setValue("False");
//
//		SignCloudMetaData.SingletonSigning.Entry signatureImageForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signatureImageForInsuranceItem.setKey("SIGNATUREIMAGE");
//		signatureImageForInsuranceItem.setValue(ESignCloudConstants.FEC_SIGNATURE);
//		SignCloudMetaData.SingletonSigning.Entry pageNoForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		pageNoForInsuranceItem.setKey("PAGENO");
//		pageNoForInsuranceItem.setValue("Last");
//
//		SignCloudMetaData.SingletonSigning.Entry positionIdentifiderForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		positionIdentifiderForInsuranceItem.setKey("POSITIONIDENTIFIER");
//		positionIdentifiderForInsuranceItem.setValue("Người được bảo hiểm");
//
//		SignCloudMetaData.SingletonSigning.Entry rectangleOffsetForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		rectangleOffsetForInsuranceItem.setKey("RECTANGLEOFFSET");
//		rectangleOffsetForInsuranceItem.setValue("30,10");
//
//		SignCloudMetaData.SingletonSigning.Entry rectangleSizeForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		rectangleSizeForInsuranceItem.setKey("RECTANGLESIZE");
//		rectangleSizeForInsuranceItem.setValue("170,70");
//
//		SignCloudMetaData.SingletonSigning.Entry visibleSignatureForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		visibleSignatureForInsuranceItem.setKey("VISIBLESIGNATURE");
//		visibleSignatureForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry visualStatusForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		visualStatusForInsuranceItem.setKey("VISUALSTATUS");
//		visualStatusForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry imageAndTextForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		imageAndTextForInsuranceItem.setKey("IMAGEANDTEXT");
//		imageAndTextForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry textDirectionForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		textDirectionForInsuranceItem.setKey("TEXTDIRECTION");
//		textDirectionForInsuranceItem.setValue("LEFTTORIGHT");
//
//		SignCloudMetaData.SingletonSigning.Entry showSignerInfoForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showSignerInfoForInsuranceItem.setKey("SHOWSIGNERINFO");
//		showSignerInfoForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry signerInfoPrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signerInfoPrefixForInsuranceItem.setKey("SIGNERINFOPREFIX");
//		signerInfoPrefixForInsuranceItem.setValue("Sign by:");
//
//		SignCloudMetaData.SingletonSigning.Entry showDateTimeForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showDateTimeForInsuranceItem.setKey("SHOWDATETIME");
//		showDateTimeForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry dateTimePrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		dateTimePrefixForInsuranceItem.setKey("SHOWDATETIME");
//		dateTimePrefixForInsuranceItem.setValue("Sign on:");
//
//		SignCloudMetaData.SingletonSigning.Entry showReasonForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showReasonForInsuranceItem.setKey("SHOWREASON");
//		showReasonForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry signReasonPrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signReasonPrefixForInsuranceItem.setKey("SIGNREASONPREFIX");
//		signReasonPrefixForInsuranceItem.setValue("Sign for:");
//
//		SignCloudMetaData.SingletonSigning.Entry signReasonForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		signReasonForInsuranceItem.setKey("SIGNREASON");
//		signReasonForInsuranceItem.setValue("I agree the agreement");
//
//		SignCloudMetaData.SingletonSigning.Entry showLocationForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		showLocationForInsuranceItem.setKey("SHOWLOCATION");
//		showLocationForInsuranceItem.setValue("True");
//
//		SignCloudMetaData.SingletonSigning.Entry locationForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		locationForInsuranceItem.setKey("LOCATION");
//		locationForInsuranceItem.setValue("Ho Chi Minh");
//
//		SignCloudMetaData.SingletonSigning.Entry locationPrefixForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		locationPrefixForInsuranceItem.setKey("LOCATIONPREFIX");
//		locationPrefixForInsuranceItem.setValue("Sign location:");
//
//		SignCloudMetaData.SingletonSigning.Entry textColorForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		textColorForInsuranceItem.setKey("TEXTCOLOR");
//		textColorForInsuranceItem.setValue("black");
//
//		SignCloudMetaData.SingletonSigning.Entry lockAfterSigningForInsuranceItem = new SignCloudMetaData.SingletonSigning.Entry();
//		lockAfterSigningForInsuranceItem.setKey("LOCKAFTERSIGNING");
//		lockAfterSigningForInsuranceItem.setValue("True");
//		
//		SignCloudMetaData.SingletonSigning.Entry  showEmailForCommonItem=null, emailPrefixForCommonItem=null, showGeoCoordinateForCommonItem=null, geoCoordinatePrefixForCommonItem=null,
//				geoCoordinateForCommonItem=null, showIPAddrForCommonItem=null, ipAddrPrefixForCommonItem=null, ipAddrForCommonItem=null, emailCommonItem;
//		//Common data embed
//		if(dataToEmbed !=null) {
//			showEmailForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    showEmailForCommonItem.setKey("SHOWEMAIL");
//		    showEmailForCommonItem.setValue("True");
//		    
//		    emailPrefixForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    emailPrefixForCommonItem.setKey("EMAILPREFIX");
//		    emailPrefixForCommonItem.setValue("Email:");
//		    
//		    showGeoCoordinateForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    showGeoCoordinateForCommonItem.setKey("SHOWGEOCOORDINATE");
//		    showGeoCoordinateForCommonItem.setValue("True");
//		    
//		    emailCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    emailCommonItem.setKey("EMAIL");
//		    emailCommonItem.setValue(Utils.getStringValue(dataToEmbed.getOrDefault(Constants.BANCA_EMAIL_ID_KEY,"")));
//		    
//		    geoCoordinatePrefixForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    geoCoordinatePrefixForCommonItem.setKey("GEOCOORDINATEPREFIX");
//		    geoCoordinatePrefixForCommonItem.setValue("Coordinates:");
//		    
//		    geoCoordinateForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    geoCoordinateForCommonItem.setKey("GEOCOORDINATE");
//		    geoCoordinateForCommonItem.setValue(Utils.getStringValue(dataToEmbed.getOrDefault(Constants.LATITUDE_KEY, "")+","+dataToEmbed.getOrDefault(Constants.LONGITUDE_KEY, "")));
//		    
//		    showIPAddrForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    showIPAddrForCommonItem.setKey("SHOWIPADDRESS");
//		    showIPAddrForCommonItem.setValue("True");
//		    
//		    ipAddrPrefixForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    ipAddrPrefixForCommonItem.setKey("IPADDRESSPREFIX");
//		    ipAddrPrefixForCommonItem.setValue("IP Addr:");
//		    
//		    ipAddrForCommonItem = new SignCloudMetaData.SingletonSigning.Entry();
//		    ipAddrForCommonItem.setKey("IPADDRESS");
//		    ipAddrForCommonItem.setValue(Utils.getStringValue(dataToEmbed.getOrDefault(Constants.CUSTOMER_IP_ADDRESS, "")));
//		}
//		
//		
//		SignCloudMetaData.SingletonSigning singletonSigningForInsurance = new SignCloudMetaData.SingletonSigning();
//		singletonSigningForInsurance.getEntry().add(counterSignEnabledForInsuranceItem);
//		//singletonSigningForInsurance.getEntry().add(signatureImageForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(pageNoForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(positionIdentifiderForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(rectangleOffsetForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(rectangleSizeForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(visibleSignatureForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(visualStatusForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(imageAndTextForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(textDirectionForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(showSignerInfoForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(signerInfoPrefixForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(showDateTimeForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(dateTimePrefixForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(showReasonForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(signReasonPrefixForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(signReasonForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(showLocationForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(locationForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(locationPrefixForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(textColorForInsuranceItem);
//		singletonSigningForInsurance.getEntry().add(lockAfterSigningForInsuranceItem);
//		//Common data to embedd
//		if(dataToEmbed !=null) {
//			singletonSigningForInsurance.getEntry().add(showEmailForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(emailPrefixForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(showGeoCoordinateForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(geoCoordinatePrefixForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(geoCoordinateForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(showIPAddrForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(ipAddrPrefixForCommonItem);
//		    singletonSigningForInsurance.getEntry().add(ipAddrForCommonItem);
//		}
//		return singletonSigningForInsurance;
		
		return null;
	}


}
