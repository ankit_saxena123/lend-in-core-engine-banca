package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.pojo.EmailInputForm;
import com.kuliza.lending.wf_implementation.pojo.WorkFlowUserSms;
import com.kuliza.lending.wf_implementation.services.FECreditBancaNotifyUserServices;

@RestController
@RequestMapping("/wf-notification")
public class FECreditBancaNotifyUserController {

	@Autowired
	private FECreditBancaNotifyUserServices feCreditBancaNotifyUserServices;

	@RequestMapping(method = RequestMethod.POST, value = "/sendSmsNotification")
	public ApiResponse callSendSms(@RequestBody WorkFlowUserSms workFlowUserSms) throws Exception {
		return feCreditBancaNotifyUserServices.sendSmsNotification(workFlowUserSms);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/send-email")
	public ResponseEntity<Object> sendEmail(Principal principal,@RequestBody @Valid EmailInputForm emailInputForm) {
		try {
			return CommonHelperFunctions.buildResponseEntity(feCreditBancaNotifyUserServices.sendEmail(principal.getName(),emailInputForm));
		} catch (Exception e) {
			e.printStackTrace();
			return CommonHelperFunctions.buildResponseEntity(
					new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}
	}

}
