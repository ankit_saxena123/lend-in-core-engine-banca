package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditBancaSMSIntegration extends FECreditMVPIntegrationsAbstractClass {

	@Autowired
	private WfImplConfig wfImplConfig;
	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaSMSIntegration.class);

	public JSONObject buildRequestPayload(Map<String, Object> data) throws JSONException {
		JSONObject requestPayload = new JSONObject();
		JSONObject sysRequest = new JSONObject();
		sysRequest.put("sms:SendSMS.Sys.TransID", Utils.generateUUIDTransanctionId());
		sysRequest.put("sms:SendSMS.Sys.DateTime", Utils.getCurrentDateTime());
		sysRequest.put("sms:SendSMS.Sys.RequestorID", Constants.FEC_SMS_REQUESTOR_ID);
		String mobile = Utils.getStringValue(data.get(Constants.MOBILE_NUMBER_KEY));
		if (mobile.startsWith("0")) {
			mobile = mobile.substring(1, mobile.length());
		}
		sysRequest.put("sms:SendSMS.request.Phone", "+84" + mobile);
		sysRequest.put("sms:SendSMS.request.Message", Utils.getStringValue(data.get("message")));
		requestPayload.put("body", sysRequest);
		logger.info("RequestPayload :: >>>>>>>>>>>>>>>>>>>>>" + requestPayload);
		return requestPayload;
	}

	@Override
	public HttpResponse<JsonNode> getDataFromService(JSONObject requestPayload) throws Exception {
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put("Content-Type", Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY
				+ Constants.SMS_INTEGRATION_SLUG + "/" + Constants.SEND_SMS_METHOD + "/"
				+ wfImplConfig.getIbCompany()+ "/" + Constants.IB_HASH_VALUE+"/?env="+wfImplConfig.getIbEnv()
				+ Constants.SMS_INTEGRATION_SOAP_KEY;
		logger.info("HashGenerationURL ::>>>>>>>>>>>>>>>>>>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		logger.info("ResposeAsJson :: >>>>>>>>>>>>>>>>>>>>>>>" + resposeAsJson.getBody());
		return resposeAsJson;
	}

	@Override
	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		logger.info("-->Entering FECreditBancaSMSIntegration.parseResponse()");
		Map<String, Object> responseFromSMSIntegration = new HashMap<>();
		if (responseFromSMSIntegration.isEmpty()) {
			if (responseFromIntegrationBroker.has("NS1:SendSMSResponse")
					&& responseFromIntegrationBroker.get("NS1:SendSMSResponse") instanceof JSONObject) {
				JSONObject response = responseFromIntegrationBroker.getJSONObject("NS1:SendSMSResponse");
				logger.info("Response :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + response);
				if (response.has("response") && response.get("response") instanceof JSONObject) {
					JSONObject response_sms = response.getJSONObject("response");
					if (response_sms.has("Status") && Utils.getIntegerValue(response_sms.get("Status")) == 1) {
						responseFromSMSIntegration.put(Constants.FE_CREDIT_MVP_SMS_STATUS_KEY, true);
						responseFromSMSIntegration.put(Constants.FE_CREDIT_MVP_SMS_DESCRIPTION_KEY,
								response_sms.getString("Description"));
					} else {
						responseFromSMSIntegration.put(Constants.FE_CREDIT_MVP_SMS_STATUS_KEY, false);
					}
				} else {
					responseFromSMSIntegration.put(Constants.FE_CREDIT_MVP_NO_RESPONSE_MAP_KEY,
							Constants.FE_CREDIT_MVP_NO_RESPONSE_KEY_MESSAGE);
				}
			} else {
				responseFromSMSIntegration.put(Constants.FE_CREDIT_MVP_NO_SEND_SMS_RESPONSE_MAP_KEY,
						Constants.FE_CREDIT_MVP_NO_SEND_SMS_RESPONSE_KEY_MESSAGE);
			}
		} else {
			throw new Exception(responseFromIntegrationBroker.toString());
		}
		logger.info("<-- Exiting FECreditBancaSMSIntegration.parseResponse()");
		return responseFromSMSIntegration;
	}
}
