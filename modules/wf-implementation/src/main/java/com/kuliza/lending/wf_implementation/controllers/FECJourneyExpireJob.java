package com.kuliza.lending.wf_implementation.controllers;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.services.FECJourneyExpireService;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;

@RestController
@RequestMapping("/journey")
public class FECJourneyExpireJob {

	@Autowired
	private FECJourneyExpireService fECJourneyExpireService;

	private static final Logger logger = LoggerFactory.getLogger(FECJourneyExpireJob.class);

	@RequestMapping(method = RequestMethod.POST, value = "/expire")
	public ApiResponse markDropOffs() throws Exception {
		try {
			return fECJourneyExpireService.expire();
		} catch (Exception e) {
			logger.error(Utils.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/complete-active-task")
	public void completeTask(@RequestParam String processInstanceId, @RequestBody HashMap<String, Object> variables) {
		fECJourneyExpireService.completeActiveTask(processInstanceId, variables);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/task-complete")
	public ApiResponse taskComplete(@RequestParam String processInstanceId, @RequestParam String taskDefKey) {
		return fECJourneyExpireService.completeTaskByTaskName(processInstanceId, taskDefKey);
	}
}
