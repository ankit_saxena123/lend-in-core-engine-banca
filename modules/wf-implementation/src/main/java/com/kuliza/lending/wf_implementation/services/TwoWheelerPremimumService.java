package com.kuliza.lending.wf_implementation.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.pojo.TwPremiumInput;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class TwoWheelerPremimumService {

	@Autowired
	private MasterApiIntegration masterApiIntegration;
	@Autowired
	private WorkFlowPolicyDAO workPolicyDAO;
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	@Autowired
	private RuntimeService runtimeService;

	private static final Logger logger = LoggerFactory
			.getLogger(TwoWheelerPremimumService.class);
	
	
	public ApiResponse calPremium(TwPremiumInput twInputForm){
		Map<Object, Object> responseMap=new HashMap<Object, Object>();
		logger.info("<=======calPremium ()=======>");
		try{
		String processInstanceId = twInputForm.getProcessInstanceId();
		Map<String, Object> variables = runtimeService.getVariables(processInstanceId);
		String protectionAmt= Utils.getStringValue(variables
				.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
		String monthlyPayment = Utils.getStringValue(variables
				.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
//	TemporyChange
		//String subSection2_en=Constants.TW_SUB_SECTION2_PART1_EN;
		//String subSection2_vn=Constants.TW_SUB_SECTION2_PART1_VN;
		String planIds = Utils
				.getStringValue(variables
						.get(WfImplConstants.JOURNEY_PLAN_ID));
				logger.info("planIds :"+planIds);
				String[] planIDarray = planIds.split(",");
				List<String> listOfPlanId = Arrays.asList(planIDarray);
				
		/*if(!protectionAmt.isEmpty() && !monthlyPayment.isEmpty()){
			if(listOfPlanId.contains(WfImplConstants.TW_THIRD_PARTY_PLAN_ID)){
				//subSection2_en=subSection2_en+Constants.TW_SUB_SECTION2_PART2_EN;
				//subSection2_vn=subSection2_vn+Constants.TW_SUB_SECTION2_PART2_VN;
				}
			responseMap.put("protectionAmount", protectionAmt);
			responseMap.put("premium", monthlyPayment);
			responseMap.put("subSection1_en", Constants.TW_SUB_SECTION1_EN);
			responseMap.put("subSection1_vn", Constants.TW_SUB_SECTION1_VN);
			//responseMap.put("subSection2_en", subSection2_en);
			//responseMap.put("subSection2_vn", subSection2_vn);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseMap);
		}*/
		
		String brandId = Utils.getStringValue(variables
				.get(WfImplConstants.PROCESS_VARIABLE_BRAND_ID));
		String modelId = Utils.getStringValue(variables
				.get(WfImplConstants.PROCESS_VARIABLE_MODEL_ID));
		String cc = Utils.getStringValue(variables
				.get(WfImplConstants.PROCESS_VARIABLE_CC));

		String yearOfRegistration = Utils
				.getStringValue(variables
						.get(WfImplConstants.PROCESS_VARIABLE_YEAR_OF_REGISTRATION));
		logger.info("brandId :"+brandId+" modelId "+modelId+" cc "+cc+" yearOfRegistration :"+yearOfRegistration);
		
		logger.info("yearOfRegistration :" + yearOfRegistration);
		String[] split = yearOfRegistration.split("-");
		String dayOfMonth = null;
		String year = null;
		String month = null;
		//Changes Based On Date of Year registration
		if (split.length == 3) {
			dayOfMonth = split[0];
			month = split[1];
			year = split[2];
		}

		logger.info("brandId :" + brandId + " modelId :" + modelId
				+ " year :" + year + " month :" + month + " dayOfMonth :" + dayOfMonth);
		// calls master and gets the vehicle value based on brand, model and
		// cc
		String vehicleValue = getVehicleValueFromMasters(brandId, modelId,
				cc);
		if (vehicleValue != null) {
			double protectionAmount = getProtectionAmount(vehicleValue,
					year.trim(), month.trim(),dayOfMonth.trim());
			logger.info("<=======>Protection Amount is : "+protectionAmount+"<==========>");
			if(protectionAmount>0){
			double motorLoss=getMotorLoss(protectionAmount);
			logger.info("<=======>motorLoss Amount is : "+motorLoss+"<==========>");
			double thirdPartyLiability =0;
			double driverAccident =0;
			if(listOfPlanId.contains(WfImplConstants.TW_THIRD_PARTY_PLAN_ID)){
			thirdPartyLiability = calculateThirdPartyLiability();
			//subSection2_en=subSection2_en+Constants.TW_SUB_SECTION2_PART2_EN;
			//subSection2_vn=subSection2_vn+Constants.TW_SUB_SECTION2_PART2_VN;
			}
			if(listOfPlanId.contains(WfImplConstants.TW_PERSONAL_ACCIDENT_PLAN_ID)){
			driverAccident = calculatePersonalAccident();
			protectionAmount=protectionAmount+WfImplConstants.ACCIDENT_PROTECTION_AMOUNT;
			}
			logger.info("<=======>Protection Amount is : "+protectionAmount+"<==========>");
			logger.info("<======>thirdPartyLiability:"+thirdPartyLiability+"<==========>");
			logger.info("<======>driverAccident:"+driverAccident+"<==========>");
			
			double premium=motorLoss+thirdPartyLiability+driverAccident;
			logger.info("<=======>Premium is :" + premium+"<============>");
			
			persistPolicyDetails(processInstanceId, listOfPlanId, premium, protectionAmount);
			//responseMap.put("protectionAmount", Utils.convertToViantnumCurrencyTrieu(protectionAmount));
			responseMap.put("protectionAmount",Utils.getLongValue(protectionAmount));
			//responseMap.put("premium", Utils.convertToViantnumCurrencyNgan(premium));
			responseMap.put("premium", Utils.getLongValue(premium));
			responseMap.put("subSection1_en", Constants.TW_SUB_SECTION1_EN);
			responseMap.put("subSection1_vn", Constants.TW_SUB_SECTION1_VN);
			//responseMap.put("subSection2_en", subSection2_en);
			//responseMap.put("subSection2_vn", subSection2_vn);
			Long protectionLong=Utils.getLongValue(protectionAmount);
			Long premiumLong=Utils.getLongValue(premium);
			String protectionA =Utils.getStringValue(protectionLong);
			String premiumAmount = Utils.getStringValue(premiumLong);
			
			runtimeService.setVariable(processInstanceId, WfImplConstants.JOURNEY_PROTECTION_AMOUNT, protectionA);
			runtimeService.setVariable(processInstanceId, WfImplConstants.JOURNEY_MONTHLY_PAYMENT, premiumAmount);
			runtimeService.setVariable(processInstanceId, WfImplConstants.THIRD_PARTY_LIABILITY_KEY,
					thirdPartyLiability);
			runtimeService.setVariable(processInstanceId, WfImplConstants.PERSONAL_ACCIDENT_KEY,
					driverAccident);
			runtimeService.setVariable(processInstanceId, WfImplConstants.VEHICLE_VALUE,
					protectionAmount);
			}
			else{
				runtimeService.setVariable(processInstanceId, WfImplConstants.JOURNEY_PROTECTION_AMOUNT, "0");
				runtimeService.setVariable(processInstanceId, WfImplConstants.JOURNEY_MONTHLY_PAYMENT, "0");
				logger.info("protection amount is null");
			}
		} else {
			logger.info("vehicleValue is null");
		}
		}
		catch(Exception e){
			logger.error("<Failed to get TW premium =======>:"+e.getMessage());
			e.printStackTrace();
		}
		
		
		
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseMap);
	}

	
	private void persistPolicyDetails(String processInstanceId,
			List<String> listOfPlanId, double premium, double protection) {
//		String protectionTrieu = Utils.convertToViantnumCurrencyTrieu(protection);
//		String premiumInNgan = Utils.convertToViantnumCurrencyNgan(premium);
		Long protectionLong=Utils.getLongValue(protection);
		Long premiumLong=Utils.getLongValue(premium);
		String protectionTrieu =Utils.getStringValue(protectionLong);
		String premiumInNgan = Utils.getStringValue(premiumLong);
		WorkFlowApplication workFlowApp = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if(workFlowApp!=null){
			workFlowApp.setProtectionAmount(protectionTrieu);
			workFlowApp.setMontlyPayment(premiumInNgan);
			workflowApplicationDao.save(workFlowApp);
			for(String planId:listOfPlanId){
				WorkFlowPolicy workFlowPolicy = workPolicyDAO.findByWorkFlowApplicationAndPlanIdAndIsDeleted(workFlowApp, planId, false);
				workFlowPolicy.setProtectionAmount(protectionTrieu);
				workFlowPolicy.setMontlyPayment(premiumInNgan);
				workPolicyDAO.save(workFlowPolicy);
			}
		}
		
	}

	private double getMotorLoss(double protectionAmount) {
		double motorLoss = (protectionAmount * WfImplConstants.MOTOR_LOSS) / 100;
		/*double motorLossTax = (motorLoss * 10) / 100;
		motorLoss=motorLoss+motorLossTax;*/
		logger.debug("motor loss is : "
				+ BigDecimal.valueOf(motorLoss).toPlainString());
		return motorLoss;
	}

	private double getProtectionAmount(String vehicleValue, String year,
			String month,String dayOfMonth) throws Exception {
		JSONArray filterJsonArray = null;
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		// gets the months diff between current and given date
		int ageOfTheVehicle = getAgeOfVehicle(Integer.parseInt(year),Utils.EnumMonth(month),Integer.parseInt(dayOfMonth));
		//		long monthDiff = Utils.getMonthsDiff(year.trim(), month.trim());
//		logger.info("months diff is :" + monthDiff);
//		if (monthDiff == -1) {
//			logger.info("date provided is invalid");
//			return -1;
//		} else if (monthDiff < 3) {
//			map.put(Constants.KEY, WfImplConstants.MASTER_MONTHS);
//			map.put(Constants.VALUE, "3");
//		} else if (monthDiff < 12) {
//			map.put(Constants.KEY, WfImplConstants.MASTER_MONTHS);
//			map.put(Constants.VALUE, "12");
//		} else {
//			map.put(Constants.KEY, WfImplConstants.MASTER_YEAR_OF_REGISTRATION);
//			map.put(Constants.VALUE, year);
//		}
		map.put(Constants.KEY,WfImplConstants.MASTER_AGE);
		map.put(Constants.VALUE,String.valueOf(ageOfTheVehicle));
		list.add(map);
		// constuct the request for master
		filterJsonArray = constructFilterJsonArray(list);
		// gets the percentage of vehicle values based on month or year
		String percentage = getPercentageBasedOnyear(filterJsonArray);
		if (percentage != null) {
			double percentageDouble = Double.valueOf(percentage);
			double vehicleValueLong = Double.valueOf(vehicleValue);
			double currentVehicleValue = (vehicleValueLong * percentageDouble) / 100;
			logger.info("Current Value : "
					+ BigDecimal.valueOf(currentVehicleValue).toPlainString());
			return currentVehicleValue;
		}
		return -1;

	}	
	
	private int getAgeOfVehicle(int year, Month month,int dayOfMonth) {
		LocalDate bday = LocalDate.of(year, month, dayOfMonth);
		LocalDate today = LocalDate.now();
		Period period = Period.between(bday, today);
		int years = period.getYears();
		int months = period.getMonths();
		int days = period.getDays();
		int age = 0;

		logger.info("number of years: " + years);
		logger.info("number of months: " + months);
		logger.info("number of dayOfMonth: " + dayOfMonth);
		logger.info("number of days: " + days);

		if (years == 0) {
			if (months < 3) {
				age = 0;

			} else if (months == 3 && days == 0) {
				age = 0;
			} else {
				age = 1;
			}
		} else {
			if (months == 0 && days == 0) {
				age = years;
			} else {
				age = years + 1;
			}

		}

		return age;

	}

	private double calculateThirdPartyLiability() {
		double thirdPartyLiability = WfImplConstants.THIRD_PARTY_LIABILITY;
		double thirdPartyLiabilityTax = (thirdPartyLiability * 10) / 100;
		return thirdPartyLiability + thirdPartyLiabilityTax;
	}

	private double calculatePersonalAccident() {
		double personalAccident = WfImplConstants.ACCIDENT;
		return personalAccident;
	}

	private String getPercentageBasedOnyear(JSONArray filterJsonArray)
			throws Exception {
		String percentage = null;
		JSONArray masterResponseArray = callMaster(filterJsonArray,
				WfImplConstants.BANCA_GET_PERCENTAGE_VEHICLE_VALUE);
		if (masterResponseArray.length() != 0) {
			JSONObject masterFinalJsonResponseAsJson = (JSONObject) masterResponseArray
					.get(0);
			if (masterFinalJsonResponseAsJson
					.has(WfImplConstants.PERCENTAGE_OF_THE_VEHICLE_VALUE)) {
				percentage = masterFinalJsonResponseAsJson
						.getString(WfImplConstants.PERCENTAGE_OF_THE_VEHICLE_VALUE);
			} else {
				logger.info("percentage_of_the_vehicle_value Not Found");
			}

		} else {
			logger.info("No Data Found in master");
		}
		logger.info("percentage is : " + percentage);
		return percentage;
	}

	private String getVehicleValueFromMasters(String brandId, String modelId,
			String cc) throws Exception {
		String vehicleValue = null;
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> brandMap = new HashMap<String, String>();
		HashMap<String, String> modelMap = new HashMap<String, String>();
		HashMap<String, String> ccMap = new HashMap<String, String>();
		brandMap.put(Constants.KEY, WfImplConstants.MASTER_BRAND_NAME);
		brandMap.put(Constants.VALUE, brandId);
		modelMap.put(Constants.KEY, WfImplConstants.MASTER_MODEL_NAME);
		modelMap.put(Constants.VALUE, modelId);
		ccMap.put(Constants.KEY, WfImplConstants.MASTER_CC);
		ccMap.put(Constants.VALUE, cc);
		list.add(brandMap);
		list.add(modelMap);
		list.add(ccMap);
		JSONArray filterJsonArray = constructFilterJsonArray(list);
		JSONArray masterFinalResponseAsJsonArray = callMaster(filterJsonArray,
				WfImplConstants.BANCA_GET_BIKE_DETAILS);

		if (masterFinalResponseAsJsonArray.length() != 0) {

			JSONObject masterFinalJsonResponseAsJson = (JSONObject) masterFinalResponseAsJsonArray
					.get(0);
			if (masterFinalJsonResponseAsJson
					.has(WfImplConstants.MASTER_VEHICLE_VALUE)) {
				vehicleValue = masterFinalJsonResponseAsJson
						.getString(WfImplConstants.MASTER_VEHICLE_VALUE);
			} else {
				logger.info("master reponse does not has vehicle_value key");
			}

		} else {
			logger.info("No Data Found in master");
		}

		logger.info("vehicleValue is " + vehicleValue);
		return vehicleValue;
	}

	public JSONArray callMaster(JSONArray filterJsonArray, String subUrl)
			throws Exception {
		logger.info("--->callMaster()");
		JSONArray masterFinalResponseAsJsonArray = new JSONArray();
		JSONObject masterRequestJson = masterApiIntegration
				.buildRequestPayload(filterJsonArray);
		JSONObject masterFinalJsonResponse = masterApiIntegration
				.getDataFromService(masterRequestJson, subUrl);
		if (masterFinalJsonResponse != null
				&& masterFinalJsonResponse.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
			masterFinalResponseAsJsonArray = masterApiIntegration
					.parseResponseFromMaster(masterFinalJsonResponse);
		} else {
			logger.info("Failed to call Master");
		}
		return masterFinalResponseAsJsonArray;
	}

	public JSONArray constructFilterJsonArray(
			List<HashMap<String, String>> filterList) throws JSONException {
		JSONArray constructJsonArray = new JSONArray();
		if (filterList != null) {
			for (HashMap<String, String> map : filterList) {
				String key = map.get(Constants.KEY);
				String value = map.get(Constants.VALUE);
				constructJsonArray.put(getFilterJsonObject(key, value));

			}
		}
		return constructJsonArray;
	}

	public JSONObject getFilterJsonObject(String key, String value)
			throws JSONException {
		JSONObject filterJson = new JSONObject();
		filterJson.put(Constants.KEY, key);
		filterJson.put(Constants.VALUE,  value);
		return filterJson;
	}

}
