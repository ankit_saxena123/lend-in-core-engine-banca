package com.kuliza.lending.wf_implementation.services;

import java.util.List;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.dao.InsurerInfoDao;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;

@Service
public class InsurerScheduleService {

	@Autowired
	private InsurerInfoDao insurerInfoDao;


	@Autowired
	private InsurerService insurerService;

	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory.getLogger(InsurerScheduleService.class);

	public boolean scheduler() throws Exception {
		String processInstanceId = null;
		String requestBody = null;
		Map<String, Object> variables;
		InsurerAbstract insurerObject = null;
		boolean status = false;
		try {

			List<InsurerInfoModel> insurerInfoList = insurerInfoDao.findByIsDeletedAndSuccess(false, false);
			for (int i = 0; i < insurerInfoList.size(); i++) {
				InsurerInfoModel insurerInfo = insurerInfoList.get(i);
				int count = insurerInfo.getSchedularCount();
				logger.info("insurer info PID *******************  " + count);
				if (count <= 5) {
					logger.info("insurer info list*******************  " + insurerInfo);
					processInstanceId = insurerInfo.getProcessInstanceId();
					logger.info("insurer info PID *******************  " + processInstanceId);
					requestBody = insurerInfo.getApiRequest();
					HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
					variables = processInstance.getProcessVariables();
					insurerObject = InsurerFactoryClass.getInsurer(variables);
					insurerObject.schedulerInsurer(processInstanceId, requestBody, insurerInfo, count, variables);
					status = true;
				}

			}
		} catch (Exception e) {
			logger.error("unable to call Schedular api :" + e.getMessage(), e);
		}
		return status;

	}

}
