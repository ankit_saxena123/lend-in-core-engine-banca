package com.kuliza.lending.wf_implementation.integrations;

import java.util.Map;

import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;

public abstract class FECreditMVPIntegrationsAbstractClass {
	public JSONObject buildRequestPayload() {
		return new JSONObject();
	}

	public JSONObject buildRequestPayload(Map<String, Object> data) throws Exception {
		return new JSONObject();
	}

	public abstract HttpResponse<?> getDataFromService(JSONObject requestMap) throws Exception;

	// Here you have to return the Map which needs to be stored in process
	// variables.
	public abstract Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception;
}
