package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "workflow_policy")
public class WorkFlowPolicy extends BaseModel{
	
	@Column(nullable = true, name="transaction_id")
	private String transactionId;
	
	@Column(nullable = true, name="plan_id")
	private String planId;
	
	@Column(nullable = true, name="plan_name")
	private String planName;
	
	@Column(nullable = true, name="policy_number")
	private String policyNumber;
	
	@Column(nullable = true, name="next_payment_due")
	private String nextPaymentDue;
	
	@Column(nullable = true, name="purchased_date")
	private String purchasedDate;
	
	@Column(nullable = true, name="plan_expiring_date")
	private String planExpiringDate;
	
	@Column(nullable = true, name="insurer")
	private String insurer;
	
	@Column(nullable = true, name="montly_payment")
	private String montlyPayment;
	
	@Column(nullable = true, name="protection_amount")
	private String protectionAmount;
	
	@Column(nullable = true, name="dms_doc_id")
	private String dmsDocId;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wf_application_id", nullable = false)
	private WorkFlowApplication workFlowApplication;
	
	@Column(nullable = true, name="claim_number")
	private String claimNumber;
	
	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getNextPaymentDue() {
		return nextPaymentDue;
	}

	public void setNextPaymentDue(String nextPaymentDue) {
		this.nextPaymentDue = nextPaymentDue;
	}

	public String getPlanExpiringDate() {
		return planExpiringDate;
	}

	public void setPlanExpiringDate(String planExpiringDate) {
		this.planExpiringDate = planExpiringDate;
	}

	public String getInsurer() {
		return insurer;
	}

	public void setInsurer(String insurer) {
		this.insurer = insurer;
	}

	public String getMontlyPayment() {
		return montlyPayment;
	}

	public void setMontlyPayment(String montlyPayment) {
		this.montlyPayment = montlyPayment;
	}

	public String getProtectionAmount() {
		return protectionAmount;
	}

	public void setProtectionAmount(String protectionAmount) {
		this.protectionAmount = protectionAmount;
	}

	public String getDmsDocId() {
		return dmsDocId;
	}

	public void setDmsDocId(String dmsDocId) {
		this.dmsDocId = dmsDocId;
	}

	public WorkFlowApplication getWorkFlowApplication() {
		return workFlowApplication;
	}

	public void setWorkFlowApplication(WorkFlowApplication workFlowApplication) {
		this.workFlowApplication = workFlowApplication;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPurchasedDate() {
		return purchasedDate;
	}

	public void setPurchasedDate(String purchasedDate) {
		this.purchasedDate = purchasedDate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
	
	

}
