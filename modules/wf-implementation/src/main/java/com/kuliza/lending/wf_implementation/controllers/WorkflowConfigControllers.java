package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.pojo.AppUpdateInfo;
import com.kuliza.lending.wf_implementation.pojo.ConfigInputList;
import com.kuliza.lending.wf_implementation.services.WorkflowConfigServices;

@RestController
@RequestMapping("/wf-config")
public class WorkflowConfigControllers {

	@Autowired
	private WorkflowConfigServices configServices;

	@RequestMapping(method = RequestMethod.POST, value = "/updateConfig")
	public ApiResponse updateConfig(@RequestBody @Valid ConfigInputList configInputList) {
		try {
			return configServices.updateConfig(configInputList);
		} catch (Exception e) {
			return new ApiResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getConfig")
	public ApiResponse getConfig(@RequestParam String name, Principal principal) {
		try {
			return configServices.getConfig(name);
		} catch (Exception e) {
			return new ApiResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/app-update")
	public ApiResponse getForceUpdate(@RequestBody @Valid AppUpdateInfo body) {
		try {
			return configServices.getUpdateInfo(body);
		} catch (Exception e) {
			return new ApiResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.INTERNAL_SERVER_ERROR_MESSAGE, e.getMessage());
		}
	}
}
