package com.kuliza.lending.wf_implementation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.WfImplConfig;



public class ExcelHelper {

	private static final Logger logger = LoggerFactory.getLogger(ExcelHelper.class);

	public static String CLASS_NAME = "com.kuliza.lending.wf_implementation.models.OfflinePolicyDetails";

	private List<String> fieldNames = new ArrayList<String>();
	private Workbook workbook = null;
	private String workBookName = "";

	public ExcelHelper(String workBookName) {
		this.workBookName = workBookName;
		initialize();
	}

	private void initialize() {
		setWorkbook(new HSSFWorkbook());
	}

	public void closeWorkSheet() {
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(getWorkbookName());
			getWorkbook().write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
		}
	}

	private boolean setupFiledsForClass(Class<?> clazz) throws Exception {
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			fieldNames.add(fields[i].getName());
		}
		return true;
	}

	private Sheet getSheetWithNames(String name) {
		Sheet sheet = null;
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			if (name.compareTo(workbook.getSheetName(i)) == 0) {
				sheet = workbook.getSheetAt(i);
				break;
			}
		}
		return sheet;
	}

	private void initializeForRead() throws InvalidFormatException, IOException {
		InputStream inp = new FileInputStream(getWorkbookName());
		workbook = WorkbookFactory.create(inp);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> readData(String className) throws Exception {
		logger.info("sheet====>>>>: " + className);
		initializeForRead();
		Sheet sheet = getSheetWithNames(Constants.OFFLINE_POLICIES);
		logger.info("sheet name====>>>>: " + sheet.getSheetName());
		logger.info("work sheetName ====>>>>: " + workbook.getSheetName(0));
		Class<?> clazz = Class.forName(CLASS_NAME);
		//Class<?> clazz = Class.forName(workbook.getSheetName(0));
		setupFiledsForClass(clazz);
		List<T> result = new ArrayList<T>();
		Row row;
		logger.info("row size=====>>>> : " + sheet.getLastRowNum());
		for (int rowCount = 1; rowCount <= sheet.getLastRowNum(); rowCount++) {
			T one = (T) clazz.newInstance();
			row = sheet.getRow(rowCount);
			int colCount = 0;
			result.add(one);
			for (Cell cell : row) {
				String fieldName = fieldNames.get(colCount++);
				Method method = contructMethod(clazz, fieldName);
				
				CellType cellType = cell.getCellTypeEnum();
				if (cellType == CellType.STRING) {
					String value = cell.getStringCellValue();
					Object[] values = new Object[1];
					values[0] = value;
					method.invoke(one, values);
				} else if (cellType == CellType.NUMERIC) {
					cell.setCellType(CellType.STRING);
					String num = cell.getStringCellValue();
				//	Double num = cell.getNumericCellValue();
					Class<?> returnType = getGetterReturnClass(clazz, fieldName);
					if (returnType == String.class) {
					//	String value=Double.toString(num);
						method.invoke(one, num);
					} else if (returnType == double.class || returnType == Double.class) {
						method.invoke(one, num);
					} else if (returnType == float.class || returnType == Float.class) {
						method.invoke(one, num);
					} else if (returnType == long.class || returnType == Long.class) {
						method.invoke(one, num);
					} else if (returnType == Date.class) {
						Double dateValue=Double.parseDouble(cell.getStringCellValue());
						Date date = HSSFDateUtil.getJavaDate(dateValue);
						method.invoke(one, date);
					}
				} else if (cellType == CellType.BOOLEAN) {
					boolean num = cell.getBooleanCellValue();
					Object[] values = new Object[1];
					values[0] = num;
					method.invoke(one, values);
				}
			}
		}
		//logger.info("====resuult==========>>>>>>> : " + result);
		return result;
	}

	private Class<?> getGetterReturnClass(Class<?> clazz, String fieldName) {
		String methodName = "get" + capitalize(fieldName);
		String methodIsName = "is" + capitalize(fieldName);
		Class<?> returnType = null;
		for (Method method : clazz.getMethods()) {
			if (method.getName().equals(methodName) || method.getName().equals(methodIsName)) {
				returnType = method.getReturnType();
				break;
			}
		}
		return returnType;
	}

	public String capitalize(String string) {
		String capital = string.substring(0, 1).toUpperCase();
		return capital + string.substring(1);
	}

	private Method contructMethod(Class<?> clazz, String fieldName) throws SecurityException, NoSuchMethodException {
		Class<?> fieldClass = getGetterReturnClass(clazz, fieldName);
		return clazz.getMethod("set" + capitalize(fieldName), fieldClass);
	}

	public <T> void writeData(List<T> data) throws Exception {
		try {
			Sheet sheet = getWorkbook().createSheet(data.get(0).getClass().getName());
			setupFiledsForClass(data.get(0).getClass());
			int rowCount = 0;
			int columnCount = 0;
			Row row = sheet.createRow(rowCount++);
			for (String fieldName : fieldNames) {
				Cell cel = row.createCell(columnCount++);
				cel.setCellValue(fieldName);
			}
			Class<? extends Object> classz = data.get(0).getClass();
			for (T t : data) {
				row = sheet.createRow(rowCount++);
				columnCount = 0;
				for (String fieldName : fieldNames) {
					Cell cel = row.createCell(columnCount);
					Method method = classz.getMethod("get" + capitalize(fieldName));
					Object value = method.invoke(t, (Object[]) null);
					if (value != null) {
						if (value instanceof String) {
							cel.setCellValue((String) value);
						} else if (value instanceof Long) {
							cel.setCellValue((Long) value);
						} else if (value instanceof Integer) {
							cel.setCellValue((Integer) value);
						} else if (value instanceof Double) {
							cel.setCellValue((Double) value);
						} else if (value instanceof Date) {
							cel.setCellValue((Date) value);
							CellStyle styleDate = workbook.createCellStyle();
							DataFormat dataFormatDate = workbook.createDataFormat();
							styleDate.setDataFormat(dataFormatDate.getFormat("m/d/yy"));
							cel.setCellStyle(styleDate);
						} else if (value instanceof Boolean) {
							cel.setCellValue((Boolean) value);
						}
					}
					columnCount++;
				}
			}
			// AutoFit
			for (int i = 0; i < fieldNames.size(); i++)
				sheet.autoSizeColumn(i);
			FileOutputStream out = new FileOutputStream(new File(workBookName));
			workbook.write(out);
			out.close();
			workbook.close();
		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
		}
	}

	public String getWorkbookName() {
		return workBookName;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	

}
