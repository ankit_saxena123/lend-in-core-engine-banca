package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.ClaimInfoDocuments;

@Repository
public interface PortalClaimInfoDocumentDao extends CrudRepository<ClaimInfoDocuments, Long> {
	public ClaimInfoDocuments findByDocumentId(String claimNumber);
	public ClaimInfoDocuments findByDocumentIdAndClaimsid(String DocumentNumber, String claimId);
	
	
}
