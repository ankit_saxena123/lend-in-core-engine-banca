/**
 * 
 */
package com.kuliza.lending.wf_implementation.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimDocsDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimInfoDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimInfoDocumentDao;
import com.kuliza.lending.wf_implementation.dao.PortalDashboardIPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowAddressDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.ClaimDocModel;
import com.kuliza.lending.wf_implementation.models.ClaimInfoDocuments;
import com.kuliza.lending.wf_implementation.models.ClaimInfoModel;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.PortalDashboardAuthenticationConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

/**
 * @author garun
 *
 */
@Service
public class DashBoardDocumentService {

	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private WorkFlowAddressDao workFlowAddressDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	IPATPaymentConfirmDao iPATPaymentConfirmDao;

	@Autowired
	PortalDashboardIPATPaymentConfirmDao PortalDashboardIPATPaymentConfirmDao;

	@Autowired
	PortalClaimInfoDao portalClaimInfoDao;

	@Autowired
	PortalClaimInfoDocumentDao portalClaimInfoDocumentDao;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private WfImplConfig wfImplConfig;

	@Autowired
	PortalClaimDocsDao portalClaimDocsDao;

	private static final Logger logger = LoggerFactory.getLogger(DashBoardDocumentService.class);

	/**
	 * Customer Dashboard
	 */

	public ApiResponse getClaimDocDetails(String processInstanceId) throws Exception {
		logger.info("<-- Entering PortalDashboardService.getClaimDocDetails()");
		Map<String, Object> variables = null;
		List<Map<String, Object>> documentList = new ArrayList<>();
		Map<String, Object> response = new HashMap<String, Object>();
		if (processInstanceId != null) {
			try {

				variables = runtimeService.getVariables(processInstanceId);
				logger.info("<==getClaimInfoDetails variable size ==>" + variables.size());
				for (Entry<String, Object> entry : variables.entrySet()) {
					logger.info("<==getClaimDocDetails==>" + entry.getKey());
					logger.info("<==getClaimDocDetails==>" + entry.getValue());

				}

			} catch (Exception ex) {
				logger.info("<==Claim Reg Error occur while get process variable==>" + ex);
				HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
				variables = processInstance.getProcessVariables();
			}

			List<Map<String, Object>> claimHospitalDocs = (List<Map<String, Object>>) variables
					.get(WfImplConstants.CLAIM_HOSPITAL_DOCS);

			if (claimHospitalDocs.size() > 0) {
				for (Map<String, Object> docsMap : claimHospitalDocs) {
					Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
					List<Map<String, Object>> claimHospitalDocsList = (List<Map<String, Object>>) docsMap
							.get("uploadImage");
					logger.info("<==ClaimService hospital claimHospitalDocsList  uploadImage arraylist size==>"
							+ claimHospitalDocsList.size());

					String documentDescription = Utils.getStringValue((String) docsMap.get("description"));
					String documentName = Utils.getStringValue(docsMap.get("document"));

					for (Map<String, Object> claimHospitalDocsMap : claimHospitalDocsList) {
						logger.info("<==ClaimService hospital claimHospitalDocsList  for loop ==>"
								+ claimHospitalDocsList.size());
						Map<String, String> datas = (Map<String, String>) claimHospitalDocsMap.get("data");
						String docID = datas.get("docID");

						String documentLink = null;
						if (docID != null) {
							if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
								documentLink = WfImplConstants.CLAIM_HOSPITAL_DOCS_PROD_URL + docID.trim()
										+ "?toPDF=true";
							} else {
								documentLink = WfImplConstants.CLAIM_HOSPITAL_DOCS_UAT_URL + docID.trim()
										+ "?toPDF=true";
							}

						} else {
							logger.info("<==ClaimService docID is null==>");
						}

						ClaimDocModel claimDocs = portalClaimDocsDao.findByDocID(docID);
						if (claimDocs != null) {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_STATUS,
									claimDocs.getDocStatus());
						} else {
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_STATUS, false);
						}
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_ID,
								Utils.getStringValue(docID));
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_LINK,
								Utils.getStringValue(documentLink));
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_TYPE,
								Utils.getStringValue(docsMap.get("document")));
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_DESC,
								Utils.getStringValue(docsMap.get("description")));
						documentList.add(workFlowUserMap);
					}
				}
			}

			response.put(PortalDashboardAuthenticationConstants.CLAIM_PROCESS_INSTANCE_ID, processInstanceId);

//			response.put(PortalDashboardAuthenticationConstants.CLAIM_ID, processInstanceId);
			response.put(PortalDashboardAuthenticationConstants.CLAIM_DOCUMENTS, documentList);
			logger.info("<-- Exiting PortalDashboardService.getClaimDocDetails()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}

	}

	public ApiResponse claimDocumentStatus(String processInstanceId, String document_number, String document_status) {
		logger.info("<-- Enter PortalDashboardDocumentService.claimDocumentStatus()");
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		ClaimDocModel checkDocexist = portalClaimDocsDao.findByProcessInstanceIdAndDocID(processInstanceId,
				document_number);

		if (checkDocexist != null) {
			checkDocexist.setDocStatus(Integer.parseInt(document_status));
			portalClaimDocsDao.save(checkDocexist);

			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_STATUS, checkDocexist.getDocStatus());
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_ID,
					Utils.getStringValue(checkDocexist.getDocID()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_LINK,
					Utils.getStringValue(checkDocexist.getDocUrl()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_TYPE,
					Utils.getStringValue(checkDocexist.getDocument()));
			workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_DOC_DESC,
					Utils.getStringValue(checkDocexist.getTitle()));

			logger.info(
					"<-- PortalDashboardDocumentService.SaveClaimInfoDocumentsData()   ===>> Successfully record save");

			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.FAILURE_MESSAGE,
					PortalDashboardAuthenticationConstants.CLAIM_DOC_STATUS_FAIL_MESSAGE);
		}
	}

	public ApiResponse getClaimStatusDetails(String processInstanceId) {

		List<ClaimInfoModel> claimInfStatusListByPIds = portalClaimInfoDao.findByProcessInstanceId(processInstanceId);
		Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
		if (claimInfStatusListByPIds != null) {
			for (ClaimInfoModel claimInf : claimInfStatusListByPIds) {
				if (claimInf != null) {
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID,
							Utils.getStringValue(claimInf.getClaimsid()));
					if ("SUCCESS".equals(Utils.getStringValue(claimInf.getClaimStatus()))) {
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
								Utils.getStringValue("SUCCESS"));

					} else {
						workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
								Utils.getStringValue("FAILED"));

					}

				}
			}
			logger.info("<-- Exit PortalDashboardDocumentService.claimDocumentStatus()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, workFlowUserMap);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

}
