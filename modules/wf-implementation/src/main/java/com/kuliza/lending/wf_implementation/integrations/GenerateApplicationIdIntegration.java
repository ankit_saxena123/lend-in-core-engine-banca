package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class GenerateApplicationIdIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(GenerateApplicationIdIntegration.class);

	public JSONObject buildRequestPayload() throws JSONException {
		logger.info("-->Entering GenerateApplicationIdIntegration.buildRequestPayload()");
		JSONObject requestPayload = new JSONObject();
		JSONObject bodyRequestData = new JSONObject();
		bodyRequestData.put("aps:GetApplicationID.Sys.TransID", Utils.generateUUIDTransanctionId());
		bodyRequestData.put("aps:GetApplicationID.Sys.RequestorID", Constants.FEC_REQUESTOR_ID);
		bodyRequestData.put("aps:GetApplicationID.Sys.DateTime", Utils.getCurrentDateTime());
		requestPayload.put("body", bodyRequestData);
		logger.info(" RequestPayload : " + requestPayload.toString());
		logger.info("<-- Exiting GenerateApplicationIdIntegration.buildRequestPayload()");
		return requestPayload;
	}

	public HttpResponse<JsonNode> getDataFromIb(JSONObject requestPayload) throws Exception {
		logger.info("-->Entering GenerateApplicationIdIntegration.getDataFromIb()");
		Map<String, String> headersMap = new HashMap<String, String>();
		headersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
		String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.SOAP_SLUG_KEY 
				+ Constants.DMS_APPLICATION_ID_GEN + "/" + Constants.GET_APPLICATION_ID + "/" + wfImplConfig.getIbCompany()+ "/"
				+ Constants.IB_HASH_VALUE + "/?env="+wfImplConfig.getIbEnv()+Constants.APPLICATION_SOAP_KEY;
		logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);
		HttpResponse<JsonNode> resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload)
				.asJson();
		logger.info("resposeAsJson >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> :: " + resposeAsJson.getBody());
		logger.info("<-- Exiting GenerateApplicationIdIntegration.getDataFromIb()");
		return resposeAsJson;
	}
	public Map<String, Object> parseResponse(JSONObject responseFromIntegrationBroker) throws Exception {
		logger.info("-->Entering GenerateApplicationIdIntegration.parseResponse()");
		Map<String, Object> applicationGenerationIdResponseMap = new HashMap<>();
		JSONObject apiResponse = null;
		try {
			apiResponse = responseFromIntegrationBroker.getJSONObject("NS1:GetApplicationIDResponse");
			logger.info("apiResponse >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ::" + apiResponse);
			JSONObject sysResponse = apiResponse.getJSONObject("Sys");
			if (sysResponse.getString("Description").equals("DATA FOUND")) {
				logger.debug("Success Response for ApplicationGenerationId");
				if(apiResponse.has("ApplicationID")){
					applicationGenerationIdResponseMap.put(Constants.STATUS_MAP_KEY, true);
					applicationGenerationIdResponseMap.put("ApplicationID", apiResponse.get("ApplicationID"));
				}
				else{
					applicationGenerationIdResponseMap.put(Constants.STATUS_MAP_KEY, false);
				}
				
			} else {
				logger.debug("Data Not Found In Description for ApplicationGenerationId");
				applicationGenerationIdResponseMap.put(Constants.STATUS_MAP_KEY, false);
				applicationGenerationIdResponseMap.put(Constants.DESCRIPTION_MAP_KEY, sysResponse.getString("Description"));
			}
		} catch (Exception e) {
			applicationGenerationIdResponseMap.put(Constants.STATUS_MAP_KEY, false);
			applicationGenerationIdResponseMap.put(Constants.DESCRIPTION_MAP_KEY, e.getMessage());
		}
		logger.info("<-- Exiting GenerateApplicationIdIntegration.parseResponse()");
		return applicationGenerationIdResponseMap;
		}
}
