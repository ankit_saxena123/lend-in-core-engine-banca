package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowAppPayment;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;

@Repository
public interface WorkFlowAppPaymentDao extends CrudRepository<WorkFlowAppPayment, Long> {
	
	WorkFlowAppPayment findByWorkFlowApplicationAndIsDeleted(WorkFlowApplication workFlowApplication, boolean isDeleted);

	@Query(value = " select * from wf_app_payment where date_add(now(), interval 4 day) AND status = ?1 AND is_deleted= ?2 ", nativeQuery = true)
	public List<WorkFlowAppPayment> findByStatusAndIsDeleted(String status, boolean isDeleted);
}
