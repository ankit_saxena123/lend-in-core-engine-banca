package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class UpdateNidData {

	@NotNull(message = "gender cannot be null")
	private String gender;

	@NotNull(message = "name cannot be null")
	private String name;

	@NotNull(message = "dateOfBirth cannot be null")
	private String dateOfBirth;

	@NotNull(message = "nationality cannot be null")
	private String nationality;

	@NotNull(message = "placeOfOrigin cannot be null")
	private String placeOfOrigin;

	@NotNull(message = "currentPlaceOfResidence cannot be null")
	private String currentPlaceOfResidence;
	private String addressLine;
	private String city;
	private String province;
	private String emailAddress;
	@NotNull(message = "NationalId cannot be null")
	private String nationalId;
	
	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	
	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPlaceOfOrigin() {
		return placeOfOrigin;
	}

	public void setPlaceOfOrigin(String placeOfOrigin) {
		this.placeOfOrigin = placeOfOrigin;
	}

	public String getCurrentPlaceOfResidence() {
		return currentPlaceOfResidence;
	}

	public void setCurrentPlaceOfResidence(String currentPlaceOfResidence) {
		this.currentPlaceOfResidence = currentPlaceOfResidence;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
