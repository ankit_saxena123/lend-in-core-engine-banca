package com.kuliza.lending.wf_implementation.integrations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class PVIInsurerIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(PVIInsurerIntegration.class);

	public JSONObject createRequestPayload(Map<String, Object> variables, String crmId, String refNo, String planId,
			double thirdParty, double driverLoss, String motorLoss)

			throws JSONException, ParseException {

		logger.info("-->Entering InsurerServiceTask.createRequestPayload()");

		JSONObject requestBodyAsJson = new JSONObject();

		requestBodyAsJson.put(Constants.PVI_THIRD_PARTY_LIABILITY_PREMIUM, thirdParty);

		requestBodyAsJson.put(Constants.PVI_DISCLAMER_AGREED, Constants.DISCLAIMER_AGREED_TAG_VALUE_INTEGER);

		requestBodyAsJson.put(Constants.PVI_TRANS_ID, crmId);

		requestBodyAsJson.put(Constants.PVI_REF_NUM, refNo);

		requestBodyAsJson.put(Constants.PVI_REF_AGENT_ID, Constants.AGENT_ID_VALUE);

		requestBodyAsJson.put(Constants.PVI_DRIVER_LOSS_PREMIUM, driverLoss);

		requestBodyAsJson.put(Constants.PVI_MOTOR_LOSS_PREMIUM, Utils.ConvertViatnumCurrencyToDigits(motorLoss));

		requestBodyAsJson.put(Constants.PROPOSER_DETAILS, getProposerDetails(variables));

		requestBodyAsJson.put(Constants.PRODUCT_DETAILS, getproductDetails(variables, planId));

		requestBodyAsJson.put(Constants.DELIVERY_ADDRESS, getDeliveryAddress(variables));

		requestBodyAsJson.put(Constants.VEHICLE_DETAILS, getvehicledetails(variables));

		logger.info("RequestBodyAsJson :: >>>>>>>>>>>>>>>>>>>>>>>>>" + requestBodyAsJson);

		logger.info("<-- Exiting InsurerServiceTask.createRequestPayload()");

		return requestBodyAsJson;

	}

	private JSONObject getproductDetails(Map<String, Object> variables, String planId) throws JSONException {

		JSONObject productDetails = new JSONObject();

		Date date = new Date();

		date = DateUtils.addYears(date, 1);

		productDetails.put(Constants.PVI_PRODUCT_DETAILS_PRODUCT_ID,

				Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_ID)));

		productDetails.put(Constants.PVI_PRODUCT_DETAILS_PLAN_ID, planId);

		productDetails.put(Constants.PVI_PRODUCT_DETAILS_TYPE_OF_BUSINESS,

				Constants.TYPE_OF_BUSINESS_VALUE_INTEGER);

		productDetails.put(Constants.PVI_PRODUCT_DETAILS_POLICY_START_DATE,

				Utils.getCurrentDateInFormat("dd/MM/yyyy"));

		productDetails.put(Constants.PVI_PRODUCT_DETAILS_POLICY_END_DATE,

				Utils.getDateInString(date, "dd/MM/yyyy"));

		productDetails.put(Constants.PVI_PRODUCT_DETAILS_TENNURE,

				Constants.TENURE_INT);

		return productDetails;

	}

	private JSONObject getProposerDetails(Map<String, Object> variables) throws JSONException, ParseException {

		JSONObject proposerDetails = new JSONObject();

		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_PROPOSER_NAME,

				Utils.getStringValue(variables.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		String gender = Utils.getStringValue(variables.get(WfImplConstants.GENDER_HYPERVERGE));
		gender = Utils.convertVietnamGenderToEnglish(gender);
		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_PROPOSER_GENDER, gender);

		String dob = Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE));
		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_DOB,
				Utils.formatDate(dob, Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_SLASH));

		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_NATIONALITY,

				Utils.getStringValue(variables.get(WfImplConstants.NATIONALITY_HYPERVERGE)));

		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_NATIONAL_ID,

				Utils.getStringValue(variables.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));

		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_ADDRESS,

				Utils.getStringValue(variables.get(WfImplConstants.ADDRESS_HYPERVERGE)));

		proposerDetails.put(Constants.PVI_PROPOSAL_DETAILS_PLACE_OF_ORIGIN,

				Utils.getStringValue(variables.get(WfImplConstants.PLACEOFORIGIN_HYPERVERGE)));

		return proposerDetails;

	}

	private JSONObject getDeliveryAddress(Map<String, Object> variables) throws JSONException {

		JSONObject deliveryAddress = new JSONObject();

		deliveryAddress.put(Constants.PVI_DELIVERY_ADDRESS_IS_SAME, Constants.IS_SAME_CURRENT_VALUE);

		deliveryAddress.put(Constants.PVI_DELIVERY_ADDRESS,

				Utils.getStringValue(variables.get(WfImplConstants.ADDRESS_HYPERVERGE)));

		deliveryAddress.put(Constants.PVI_DELIVERY_ADDRESS_CITY, "");

		deliveryAddress.put(Constants.PVI_DELIVERY_ADDRESS_PROVINCE, "");

		return deliveryAddress;

	}

	private JSONObject getvehicledetails(Map<String, Object> variables) throws JSONException {

		JSONObject vehicleDetails = new JSONObject();
		logger.info("PID DATE:: >>>>>>>>>>>>>>>>>>>>>>>>>"
				+ variables.get(WfImplConstants.PROCESS_VARIABLE_YEAR_OF_REGISTRATION));
		SimpleDateFormat fmt = new SimpleDateFormat("DD - MMM - yyyy");
        Date date = null;
        String reg_date=null;
        SimpleDateFormat  fmtOut = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = fmt.parse(Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_YEAR_OF_REGISTRATION)));
			 reg_date=fmtOut.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			reg_date="01/01/2019";
			 
		}

        logger.info("Converted DATE:: >>>>>>>>>>>>>>>>>>>>>>>>>"
				+ reg_date);

		vehicleDetails.put(Constants.PVI_VEHICLE_DETAILS_BRAND,Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_BRAND_ID)));

		vehicleDetails.put(Constants.PVI_VEHICLE_DETAILS_MODEL,Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_MODEL_ID)));

		vehicleDetails.put(Constants.PVI_VEHICLE_DETAILS_CC,Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_CC)));

		vehicleDetails.put(Constants.PVI_VEHICLE_DETAILS_REGISTRATION_DATE, Utils.getStringValue(reg_date));

		vehicleDetails.put(Constants.PVI_VEHICLE_DETAILS_REGISTRATION_NUMBER,
				Utils.getStringValue(variables.get(WfImplConstants.VEHICLE_REGISTRATION_NUMBER)));

		vehicleDetails.put(Constants.PVI_VEHICLE_DETAILS_VALUE_OF_VEHICLE,
				Utils.getStringValue(variables.get(WfImplConstants.VEHICLE_VALUE)));

		return vehicleDetails;

	}

	public JSONObject getDataFromIb(JSONObject requestPayload) {

		logger.info("------>>>>>>Inside PVI MOTO getData");

		HttpResponse<JsonNode> responseAsJson = null;

		JSONObject jsonResponse = new JSONObject();

		try {

			Map<String, String> mapData = new HashMap<String, String>();

			mapData.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);

			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.IB_PVIInsurer_SLUG_KEY + "/"

					+ Constants.IB_PVIInsurer_KEY + "/" + wfImplConfig.getIbCompany() + "/" + Constants.Hash_Value
					+ "/?env=" + wfImplConfig.getIbEnv();

			logger.info("------ Hash Value ----- " + hashGenerationUrl);

			responseAsJson = Unirest.post(hashGenerationUrl)

					.headers(mapData)

					.body(requestPayload)

					.asJson();

			jsonResponse = responseAsJson.getBody().getObject();

			logger.info("****** Response *******" + jsonResponse);

			return jsonResponse;

		} catch (Exception e) {

			e.printStackTrace();

			logger.error("unable to call Insurer post api :" + e.getMessage(), e);

		}

		logger.info("****** end of get data ******");

		return jsonResponse;

	}

	public HashMap<String, Object> parseResponse(JSONObject jsonResponse) throws JSONException {

		HashMap<String, Object> map = new HashMap<String, Object>();

		String message = null;

		if (jsonResponse != null) {

			Integer status = jsonResponse.getInt(Constants.PVIINSURER_RESPONSE_STATUS);

			if (status > 0) {

				map.put(Constants.PVIINSURER_RESPONSE_STATUS, true);

				logger.info("******** INSIDE PARSE RESPONSE **************  "
						+ map.get(Constants.PVIINSURER_RESPONSE_STATUS));

			}

			else {

				map.put(Constants.PVIINSURER_RESPONSE_STATUS, false);

				logger.info("******** INSIDE PARSE RESPONSE **************  "
						+ map.get(Constants.PVIINSURER_RESPONSE_STATUS));

				message = jsonResponse.getString(Constants.PVIINSURER_RESPONSE_MESSAGE);

				map.put(Constants.PVIINSURER_RESPONSE_MESSAGE, message);

			}

		}

		return map;

	}
}