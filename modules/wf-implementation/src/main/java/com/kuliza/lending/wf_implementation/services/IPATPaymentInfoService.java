package com.kuliza.lending.wf_implementation.services;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.flowable.engine.RuntimeService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kuliza.lending.common.exception.ProcessInstanceDoesNotExistException;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.ShieldCrmInfoDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.IPATIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.ShieldCrmIdInfoModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.pojo.PaymentInfoForm;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class IPATPaymentInfoService {

	@Autowired
	private IPATIntegration ipatIntegration;

	@Autowired
	ShieldCrmInfoDao shieldCrmInfoDao;

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(IPATPaymentInfoService.class);

	public ApiResponse generatePaymentBillInfo( String userName, String processInstanceId)
			throws ProcessInstanceDoesNotExistException, Exception {
		
		logger.info("payment info generation called for "+processInstanceId + ","+userName);
		WorkFlowApplication workFlowApp = workflowApplicationDao.findByProcessInstanceIdAndIsDeleted(processInstanceId,
				false);
		if (workFlowApp != null) {
			String addoneYearDateString=null;
			Map<String, Object> variables = runtimeService.getVariables(processInstanceId);
			String nationalId = (String) variables.getOrDefault(WfImplConstants.NATIONAL_ID_HYPERVERGE, "");
			String customerName = (String) variables.getOrDefault(WfImplConstants.FULLNAME_HYPERVERGE, "");
			String mobileNumber = (String) variables.getOrDefault(WfImplConstants.APPLICATION_MOBILE_NUMBER, "");
			String premium = (String) variables.getOrDefault(WfImplConstants.JOURNEY_MONTHLY_PAYMENT, "");
			String productId = (String) variables.getOrDefault(WfImplConstants.JOURNEY_PRODUCT_ID, "");
			String planId = (String) variables.getOrDefault(WfImplConstants.JOURNEY_PLAN_ID, "");
			
			logger.info("productId :" + productId + ",planId:" + planId+",premium"+premium + " for "+processInstanceId);
			premium = Utils.ConvertViatnumCurrencyToDigits(premium);
			logger.info("calculated premium Amount --->"+premium);
			
			if(premium == null || premium.isEmpty()) {
				throw new FECException(ErrorCodes.INVALID_PREMIUM_AMOUNT);
			}
			
			String currentDateInFormat = Utils.getCurrentDateInFormat(Constants.DATE_FORMAT_SLASH);
//			String dueDateStr = Utils.formatDate(currentDateInFormat, Constants.DATE_FORMAT_SLASH,
//					Constants.DATE_FORMAT_DASH_YEAR);
			Date currentdate = Utils.parseDate(currentDateInFormat, Constants.DATE_FORMAT_SLASH);
			
			Date dueDate = Utils.addDaysToDate(new Date(), 30);
			String dueDateStr =Utils.convertToZonedDate(dueDate, Constants.VIETNAM_TIME_ZONE, Constants.DATE_FORMAT_DASH_YEAR);
			
			if(productId.equals(WfImplConstants.CV_PRODUCT_ID)) {
				
				if (planId.equals(WfImplConstants.BASIC_PLAN_ID)) {
					Date addMonthsToDate = Utils.addMonthsToDate(currentdate, 3);
					addoneYearDateString = Utils.getDateInString(addMonthsToDate, Constants.DATE_FORMAT_SLASH);
				}else {
					Date addMonthsToDate = Utils.addMonthsToDate(currentdate, 6);
					 addoneYearDateString = Utils.getDateInString(addMonthsToDate, Constants.DATE_FORMAT_SLASH);
				}
			}else {
				Date addOneToDate = Utils.addYearsToDate(currentdate, 1);
				 addoneYearDateString = Utils.getDateInString(addOneToDate, Constants.DATE_FORMAT_SLASH);
			}
			runtimeService.setVariable(processInstanceId, WfImplConstants.IPAT_PAYMENT_START_DATE, currentDateInFormat);
			runtimeService.setVariable(processInstanceId, WfImplConstants.IPAT_PLAN_EXPIRING_DATE,
					addoneYearDateString);
			runtimeService.setVariable(processInstanceId, WfImplConstants.IPAT_NEXT_PAYMENT_DUE, addoneYearDateString);
			runtimeService.setVariable(processInstanceId, WfImplConstants.IPAT_PAYMENT_START_DATE_TIME,
					Utils.getCurrentDateInFormat(Constants.DATE_TIME_FORMAT_SLASH));
			logger.info("-->nationalId :" + nationalId + " customerName :" + customerName + "mobileNumber :"
					+ mobileNumber + " premium :" + premium + " dueDate :" + dueDateStr);
			PaymentInfoForm paymentInfoForm = new PaymentInfoForm();
			paymentInfoForm.setNationalId(nationalId);
			paymentInfoForm.setCustomerName(customerName);
			paymentInfoForm.setMobileNumber(mobileNumber);
			paymentInfoForm.setPremium(premium);
			paymentInfoForm.setDueDate(dueDateStr);
			ApiResponse apiResponse = generatePaymentBillInfo(paymentInfoForm, workFlowApp, processInstanceId,
					productId, planId, variables);
			if (wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
				Map<String, Object> responseMap = (Map<String, Object>) apiResponse.getData();
				String crmId = (String) responseMap.get(WfImplConstants.IPAT_CRMID);
				responseMap.put(WfImplConstants.IPAT_CRMID, WfImplConstants.IPAT_CRMID_KEY);
				responseMap.put("ipat_crmId", crmId);
			}
			return apiResponse;
		} else {
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					"No App is running for the processInstanceId :" + processInstanceId);
		}

	}

	public ApiResponse generatePaymentBillInfo(PaymentInfoForm paymentInfoForm, WorkFlowApplication workFlowApp,
			String processInstanceId, String productId, String planId, Map<String, Object> variables) {
		
		HttpResponse<JsonNode> getResponseFromIntegrationBroker = null;
		Map<String, Object> requestMap = new HashMap<String, Object>();
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();
		JSONObject requestPayload = null;
		JSONObject parsedResponseForIPATPaymentInfo = null;
		String crm_id = null;
		String transId = null;
		ApiResponse apiResponse = null;
		boolean paymentInfoStatus = false;
		try {
			crm_id = getCRMId();
			transId = Utils.generateUUIDTransanctionId();
			logger.info("new billinfo generated for "+workFlowApp.getId() +" transId : "+ transId+",crm_id : "+crm_id);
			requestMap.put(Constants.BANCA_TRANS_ID, transId);
			requestMap.put(Constants.MOBILE_NUMBER_KEY, paymentInfoForm.getMobileNumber());
			requestMap.put(Constants.BANCA_CRM_ID_KEY, crm_id);
			requestMap.put(Constants.BANCA_NATIONAL_ID_KEY, paymentInfoForm.getNationalId());
			String premium = paymentInfoForm.getPremium();
			premium = getLesserPremium(paymentInfoForm.getMobileNumber(), premium, productId, planId, variables);
			logger.info("<========>premium value :" + premium);
			requestMap.put(Constants.BANCA_PREMIUM_KEY, premium);
			requestMap.put(Constants.BANCA_DUE_DATE_KEY, paymentInfoForm.getDueDate());
			requestMap.put(Constants.BANCA_CUSTOMER_NAME_KEY, paymentInfoForm.getCustomerName());
			requestPayload = ipatIntegration.buildRequestPayload(requestMap);
			logger.info("ipat payment req "+workFlowApp.getId()+","+requestPayload.toString());
			
			getResponseFromIntegrationBroker = ipatIntegration.getDataFromService(requestPayload);
			parsedResponseForIPATPaymentInfo = getResponseFromIntegrationBroker.getBody().getObject();
			logger.info("ipat payment response "+workFlowApp.getId()+","+parsedResponseForIPATPaymentInfo.toString());
			
			parseResponseMap = ipatIntegration.parseResponse(parsedResponseForIPATPaymentInfo);
			logger.info("ipat payment parseResponseMap "+workFlowApp.getId()+","+parseResponseMap.toString());
			if ((boolean) parseResponseMap.get(Constants.STATUS_MAP_KEY)) {
				paymentInfoStatus = true;
				parseResponseMap.put(WfImplConstants.IPAT_CRMID, crm_id);
				parseResponseMap.put("ipat_crmId", crm_id);
				parseResponseMap.put(WfImplConstants.IPAT_PAYOO_SERVICE_ID, wfImplConfig.getPayooServiceId());
				parseResponseMap.put(WfImplConstants.IPAT_PAYOO_PROVIDER_ID, wfImplConfig.getPayooProviderId());
				apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, parseResponseMap);
			} else {
				paymentInfoStatus = false;
				apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.FAILURE_MESSAGE,
						parseResponseMap);

			}
			logger.info("ipat status of "+workFlowApp.getId()+" ,"+ paymentInfoStatus);

		} catch (Exception e) {
			logger.error(Constants.SOMETHING_WRONG_MESSAGE, e);
			apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, null);
		}

		saveIPATInfo(transId, crm_id, parsedResponseForIPATPaymentInfo, paymentInfoStatus, workFlowApp,
				processInstanceId, paymentInfoForm.getMobileNumber());
		return apiResponse;
	}

	private String getLesserPremium(String mobileNumber, String premium, String productId, String planId,
			Map<String, Object> variables) {
		String premiumAmount = premium;
		logger.info("<======>premiumAmount: " + premiumAmount + " mobileNumber :" + mobileNumber);
		List<String> phoneNumberList = Constants.phoneNumberList;
		String brandname = Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_BRAND_ID));
		String modelName = Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_MODEL_ID));
		String cc = Utils.getStringValue(variables.get(WfImplConstants.PROCESS_VARIABLE_CC));
		if (mobileNumber.startsWith("0")) {
			mobileNumber = mobileNumber.substring(1, mobileNumber.length());
		}
		if (mobileNumber.startsWith("+84")) {
			mobileNumber = mobileNumber.substring(3, mobileNumber.length());
		}
		logger.info("<====>mobile Number:" + mobileNumber);

		if (phoneNumberList.contains(mobileNumber) && (productId.equals("10001") || productId.equals("10005")
				|| productId.equals("10002") || productId.equals("10004") || productId.equals("20001") || productId.equals("10003")))  {
			if (planId.equals("10001001") || planId.equals("10005001")
					|| (brandname.trim().equalsIgnoreCase("honda") && modelName.trim().equalsIgnoreCase("blade"))) {
				premiumAmount = "10000";
			} else if (planId.equals("10001002") || planId.equals("10005002")
					|| (brandname.trim().equalsIgnoreCase("honda") && modelName.trim().equalsIgnoreCase("airblade")
							&& cc.trim().equals("108"))) {
				premiumAmount = "12000";
			} else if (planId.equals("10004001") || planId.equals("10003001") || planId.equals("10004001")) {
				premiumAmount = "12000";
			} else if (planId.equals("10004002") || planId.equals("10004003") || planId.equals("10003003")) {
				premiumAmount = "15000";
			} else {
				premiumAmount = "14000";
			}
		}
		logger.info("<=======>premium amount is :" + premiumAmount);
		return premiumAmount;
	}

	public String getCRMId() {
		synchronized (this) {
			String crm_id = null;
			ShieldCrmIdInfoModel crmModel = shieldCrmInfoDao.findTopByOrderByCreated();
			if (crmModel != null) {
				logger.info("<======>Updating crmId<=======> ");
				crmModel.setCrmId(crmModel.getCrmId() + 1);
				crm_id = "S" + crmModel.getCrmId();
				shieldCrmInfoDao.save(crmModel);
			} else {
				logger.info("<======>Creating new  crmId<=======> ");
				crmModel = new ShieldCrmIdInfoModel();
				crm_id = "S" + 1000000;
				crmModel.setCrmId(1000000);
				shieldCrmInfoDao.save(crmModel);
			}
			return crm_id;
		}
	}

	private void saveIPATInfo(String transId, String crmId, JSONObject parsedResponseForIPATPaymentInfo,
			boolean paymentInfoStatus, WorkFlowApplication workFlowApp, String processInstanceId,
			String contactNumber) {
		if (transId != null && parsedResponseForIPATPaymentInfo != null) {
			IPATPaymentConfirmModel ipatConfirmPaymentModel = new IPATPaymentConfirmModel();
			ipatConfirmPaymentModel.setContactNumber(contactNumber);
			ipatConfirmPaymentModel.setRefNum(transId);
			ipatConfirmPaymentModel.setCrmId(crmId);
			ipatConfirmPaymentModel.setProcessInstanceId(processInstanceId);
			ipatConfirmPaymentModel.setIPATResponse(parsedResponseForIPATPaymentInfo.toString());
			ipatConfirmPaymentModel.setPaymentInfoStatus(paymentInfoStatus);
			ipatConfirmPaymentModel.setWfApplication(workFlowApp);
			ipatPaymentConfirmDao.save(ipatConfirmPaymentModel);
			runtimeService.setVariable(processInstanceId, WfImplConstants.IPAT_TRANSACTION_ID, transId);
			runtimeService.setVariable(processInstanceId, WfImplConstants.IPAT_CRM_ID, crmId);
			logger.info("--->saveIPATInfo completed");

		}

	}

}