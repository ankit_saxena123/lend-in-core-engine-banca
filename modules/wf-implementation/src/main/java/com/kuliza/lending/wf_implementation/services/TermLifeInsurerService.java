package com.kuliza.lending.wf_implementation.services;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowApplicationDocsDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplicationDocs;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.notification.SNSEventSubscription;
import com.kuliza.lending.wf_implementation.notification.SNSParamInit;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("termLifeInsurerService")
public class TermLifeInsurerService extends InsurerAbstract{
	
	@Autowired
	private WfImplConfig wfImplConfig;
	
	@Autowired
	private WorkflowApplicationDao workFlowApplicationDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;
	
	@Autowired
	public SNSParamInit snsParamInit;
	
	@Autowired
	private WorkFlowApplicationDocsDao workFlowApplicationDocsDao;

	@Autowired
	private SNSEventSubscription snsSubscription;
	
	@Autowired
	private UserTopicsDao userTopicsDao;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;
	
	@Autowired
	private WorkflowUserDao workflowUserDao;
	
	private static final Logger logger = LoggerFactory.getLogger(TermLifeInsurerService.class);

	@Override
	public boolean postPolicyDetails(String processInstanceId, Map<String, Object> variables, String paymentId,
			WorkFlowApplication workflowApp, IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration)
			throws Exception {
		logger.info("TermLifeInsurer Service for :"+processInstanceId);
		boolean status = false;
		String policyId = null;
		String apiResponse = null;
		String policyPdf = null;
		String policyNo = null;
		String apiRequest = null;
		String transId = null;
		try {

			logger.info("paymentId:" + paymentId);
			transId = Utils.generateUUIDTransanctionId();
			logger.info("referenceNo:" + transId);

			paymentId = Utils.getStringValue(paymentId);
			if (paymentId.isEmpty()) {
				logger.error("failed to call Insurer API :transactionId || refNo is empty");
				throw new Exception("failed to call Insurer API :transactionId || refNo is empty");
			}


			// set application numbers from temp variables
			for (int i = 1; i <= com.kuliza.lending.wf_implementation.utils.Constants.FPT_ESIGN_DOCS_COUNT; i++) {
				String tlKeyName = com.kuliza.lending.wf_implementation.utils.Constants.FPT_ESIGN_DOC_ID_ + i;

				String docId = Utils.getStringValue(variables.get(tlKeyName));
				logger.info("esign docId ------>" + docId +" appId : "+workflowApp.getId());
				
				if(docId == null || docId.isEmpty()) {
					throw new FECException(ErrorCodes.INVALID_DOC_ID);
				}
				
				WorkFlowApplicationDocs workFlowApplicationDoc = new WorkFlowApplicationDocs();
				
				workFlowApplicationDoc.setDocId(docId);
				workFlowApplicationDoc.setDocType(Constants.DocType.ESIGN_FPT.getDocTypeId());
				workFlowApplicationDoc.setWorkFlowApplication(workflowApp);
				
				workFlowApplicationDocsDao.save(workFlowApplicationDoc);

			}

			
			policyId = "";
			policyPdf = null;

			policyNo = Utils.getStringValue(variables.get(Constants.POLICY_NUMBER));
			logger.info("policyNo Is :" + policyNo);
			status = true;
			
			logger.info("******************* TL docs are updated ******************* ");

			if (status) {
				if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
					super.sendSms(variables);
//					super.sendEmail(variables, processInstanceId);

				}
				try{
					WorkFlowUserApplication wfUserapp = workflowUserApplicationDao.findByProcInstIdAndIsDeleted(processInstanceId,false);
					WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(wfUserapp.getUserIdentifier(), false);
					String subscriberArn = snsSubscription.subscribeToTopic(snsParamInit.getTlTopicARN(),"application",wfUser.getArnEndPoint());
					if(subscriberArn!=null){
						UserTopics userTopic = new UserTopics();
						userTopic.setTopic(WfImplConstants.TL_TOPIC);
						userTopic.setUserName(wfUser.getIdmUserName());
						userTopic.setSubscriberArn(subscriberArn);
						userTopic.setWfUser(wfUser);
						userTopicsDao.save(userTopic);
					}else{
						logger.info("<==Subscription To TL Topic is Failed For User==>"+wfUser.getIdmUserName());
					}
				}catch(Exception ex){
					logger.info("<==Subscription To TL Topic is Failed For User==>");

				}
			}
		} catch (Exception e) {
			logger.error("unable to call insurer api :" + e.getMessage(), e);
			status = false;
		} finally {
			persistPolicyInfo(processInstanceId, policyId, policyPdf, policyNo, transId, status, apiResponse, variables,
					workflowApp, ipatPaymentConfim, cmsIntegration, apiRequest);
		}
		return status;

	}
	
	private void persistPolicyInfo(String processInstanceId, String policyId, String policyPdf, String policyNo,
			String transactionId, boolean insurerStatus, String apiResponse, Map<String, Object> variables,
			WorkFlowApplication workFlowApp, IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration,
			String apiRequest) throws Exception {

		logger.info("--->persistPolicyInfo Term Life");
		/*
		 * InsurerInfoModel insurerInfo = new InsurerInfoModel();
		 * insurerInfo.setApiResponse(apiResponse);
		 * insurerInfo.setInsurerName(Constants.INSURER_NAME);
		 * insurerInfo.setiPatPaymentConfirm(ipatPaymentConfim);
		 * insurerInfo.setCmsPaymentConfirm(cmsIntegration);
		 * insurerInfo.setPolicyId(policyId);
		 * insurerInfo.setProcessInstanceId(processInstanceId);
		 * insurerInfo.setSuccess(insurerStatus); insurerInfo.setPdfLink(policyPdf);
		 * insurerInfo.setPolicyNo(policyNo); insurerInfo.setApiRequest(apiRequest);
		 * insurerInfoDao.save(insurerInfo);
		 */
		// if insurer api is success update insurer_status in
		// ipat_payment_confirm
		if (insurerStatus) {
			String sumInsured = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
			String premium = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			
			String paymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE));
			
			logger.info("****** plan expiry date **** "+ variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
			String endDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
			
			String nextPaymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_NEXT_PAYMENT_DUE));
			
			String planId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PLAN_ID));
			
			String mainPlanId= Utils.getMainPlanId(planId);
			
			logger.info("mainPlanId is ------>"+mainPlanId);
			
			
			List<WorkFlowPolicy> policyList = workFlowPolicyDAO
					.findByWorkFlowApplicationAndIsDeleted(workFlowApp, false);
			
			if( policyList == null || policyList.isEmpty()) {
				logger.error("workFlowPolicy is null for "+mainPlanId);
				throw new Exception("Invalid workFlow Policy for "+mainPlanId);
			}

			workFlowApp.setIs_purchased(true);
			workFlowApp.setProtectionAmount(sumInsured);
			workFlowApp.setMontlyPayment(premium);
			workFlowApp.setPlanExpiringDate(endDate);
			workFlowApp.setNextPaymentDue(nextPaymentDate);
			
			workFlowApp.setApplicationDate(paymentDate);
			workFlowApp.setPolicyNumber(policyNo);
			logger.info("<================>appId:" + workFlowApp.getApplicationId() + "<============>");
			
//			String docID = fecdmsService.uploadPDftoDMS(workFlowApp.getApplicationId(), policyPdf);
			String docID = null;
			workFlowApp.setDmsDocId(docID);
			
			workFlowApplicationDao.save(workFlowApp);
			
			super.sendEmail(variables, processInstanceId,null);
			
			for (WorkFlowPolicy workFlowPolicy : policyList) {
					workFlowPolicy.setDmsDocId(docID);
					
					if( ! workFlowPolicy.getPlanId().equals(Constants.CRITICAL_ILLNESS_PROTECTION_PLAN_ID )){
						workFlowPolicy.setPolicyNumber(policyNo);
					}
					
					workFlowPolicy.setPurchasedDate(paymentDate);
					workFlowPolicy.setPlanExpiringDate(endDate);
					workFlowPolicy.setTransactionId(transactionId);
					workFlowPolicyDAO.save(workFlowPolicy);
			}
			

		}
		logger.info("--->PolicyInfo is saved SucessFully");

	}

	@Override
	public void schedulerInsurer(String processInstanceId, String requestBody, InsurerInfoModel insurerInfo, int count,
			Map<String, Object> variables) {
		
	}

}
