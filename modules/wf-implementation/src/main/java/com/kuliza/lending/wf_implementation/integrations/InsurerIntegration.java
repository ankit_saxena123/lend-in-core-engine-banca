package com.kuliza.lending.wf_implementation.integrations;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class InsurerIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(InsurerIntegration.class);

	public String getAuthToken() {
		logger.info("-->Entering InsurerIntegration.getAuthToken()");
		String accessToken = null;
		try {
			Map<String, String> headersMap = new HashMap<String, String>();
			headersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
			headersMap.put(Constants.X_API_KEY, Constants.X_API_KEY_VALUE);
			JSONObject requestPayload = new JSONObject();
			requestPayload.put(Constants.GRANT_TYPE_KEY, Constants.GRANT_TYPE_KEY_VALUE);
			requestPayload.put(Constants.USER_NAME_KEY, Constants.USER_NAME_KEY_VALUE);
			requestPayload.put(Constants.PASSWORD_KEY, Constants.PASSWORD_KEY_VAUE);
			requestPayload.put(Constants.CLIENT_ID, Constants.CLIENT_ID_KEY_VALUE);
			requestPayload.put(Constants.CLIENT_SECRET, Constants.CLIENT_SECRET_KEY_VALUE);
			logger.info("headersMap ----> " + headersMap + " | " + "requestPayload ----> " + requestPayload);
			HttpResponse<JsonNode> resposeAsJson = null;
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.IB_AUTH_SLUG_KEY + "/"
					+ Constants.IB_AUTH_SLUG_KEY + "/" + wfImplConfig.getIbCompany() + "/"
					+ Constants.IB_HASH_VALUE + "/?env="+wfImplConfig.getIbEnv();
			logger.info("HashGenerationUrl ---> " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			logger.info("resposeAsJson ---> " + resposeAsJson.getBody());
			JSONObject responseJson = resposeAsJson.getBody().getObject();
			accessToken = responseJson.getString("access_token");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call Insurer auth api :" + e.getMessage(), e);
		}
		logger.info("<-- Exiting InsurerIntegration.getAuthToken()");
		return accessToken;
	}
	
	public JSONObject createRequestPayload(Map<String, Object> variables,
			String paymentId, String transactionId) throws JSONException {
		logger.info("-->Entering InsurerServiceTask.createRequestPayload()");
		logger.info("variables :"+variables);
		JSONObject requestBodyAsJson = new JSONObject();
		String sumInsured = Utils.getStringValue(variables
				.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
		sumInsured = Utils.ConvertViatnumCurrencyToDigits(sumInsured);
		requestBodyAsJson.put(Constants.SUM_INSURED, sumInsured);
		String premium = Utils.getStringValue(variables
				.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
		premium = Utils.ConvertViatnumCurrencyToDigits(premium);
		logger.info("ConvertViatnumCurrencyToDigits ---> "+premium);
		requestBodyAsJson.put(Constants.PREMIUM_AMOUNT, premium);
		requestBodyAsJson.put(Constants.DISCLAIMER_AGREED_TAG,
				Constants.DISCLAIMER_AGREED_TAG_VALUE);
		requestBodyAsJson.put(Constants.TRANSACTION_ID, transactionId);
		requestBodyAsJson.put(Constants.REFERENCE_NUMBER, paymentId);
		requestBodyAsJson.put(Constants.AGENT_ID, Constants.AGENT_ID_VALUE);
		requestBodyAsJson.put(Constants.PAYMENT_DATE,
				Utils.getStringValue(variables
						.get(WfImplConstants.IPAT_PAYMENT_START_DATE_TIME)));
		requestBodyAsJson.put(Constants.PROPOSER_DETAILS,
				getProposerDetails(variables));
		requestBodyAsJson.put(Constants.PRODUCT_DETAILS,
				getProductDetails(variables));
		requestBodyAsJson.put(Constants.DELIVERY_ADDRESS,
				getDiliveryAddress(variables));
		requestBodyAsJson.put(Constants.MEDICAL_QUENTIONNAIRE1,
				getMedicalQuestionnaire(variables));
		requestBodyAsJson.put(Constants.INSURED_DETAILS,
				getInsuredDetails(variables));

		logger.info("RequestBodyAsJson :: >>>>>>>>>>>>>>>>>>>>>>>>>"
				+ requestBodyAsJson);
		logger.info("<-- Exiting InsurerServiceTask.createRequestPayload()");
		return requestBodyAsJson;

	}

	private JSONObject getProposerDetails(Map<String, Object> variables)
			throws JSONException {
		JSONObject proposerDetails = new JSONObject();
		proposerDetails.put(Constants.PROPOSER_NAME_KEY, Utils
				.getStringValue(variables
						.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		proposerDetails.put(Constants.PROPOSER_GENDER_KEY, Utils.convertVietnamGenderToEnglish(Utils
				.getStringValue(variables
						.get(WfImplConstants.GENDER_HYPERVERGE))).toLowerCase());
		String dob=null;
		try{
		dob=Utils.formatDate(
				Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE)), Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_SLASH);
		
		}
		catch(Exception e){
			dob=Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE));
		}
		proposerDetails.put(Constants.DATE_OF_BIRTH_KEY, dob);
		proposerDetails.put(Constants.PROPOSER_DETAILS_NATIONALITY, Utils.convertVietnamNationalityToEnglish(Utils
				.getStringValue(variables
						.get(WfImplConstants.NATIONALITY_HYPERVERGE))));
		proposerDetails.put(Constants.PROPOSER_DETAILS_NATIONAL_ID, Utils
				.getStringValue(variables
						.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
		proposerDetails.put(Constants.PROPOSER_DETAILS_ADDRESS, Utils
				.getStringValue(variables
						.get(WfImplConstants.ADDRESS_HYPERVERGE)));
		proposerDetails.put(Constants.PROPOSER_DETAILS_PLACE_OF_ORIGIN, Utils
				.getStringValue(variables
						.get(WfImplConstants.PLACEOFORIGIN_HYPERVERGE)));
		return proposerDetails;
	}

	private JSONObject getProductDetails(Map<String, Object> variables)
			throws JSONException {
		JSONObject productDetails = new JSONObject();
		Date date = new Date();
		date = DateUtils.addYears(date, 1);
		productDetails.put(Constants.PRODUCT_ID, Utils.getStringValue(variables
				.get(WfImplConstants.JOURNEY_PRODUCT_ID)));
		productDetails.put(Constants.PLAN_ID, Utils.getStringValue(variables
				.get(WfImplConstants.JOURNEY_PLAN_ID)));
		productDetails.put(Constants.TYPE_OF_BUSINESS,
				Constants.TYPE_OF_BUSINESS_VALUE);
		productDetails.put(Constants.POLICY_START_DATE,
				Utils.getStringValue(variables
						.get(WfImplConstants.IPAT_PAYMENT_START_DATE)));
		productDetails.put(Constants.POLICY_END_DATE,
				Utils.getStringValue(variables
						.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE)));
		return productDetails;
	}

	private JSONObject getDiliveryAddress(Map<String, Object> variables)
			throws JSONException {
		JSONObject deliveryAddress = new JSONObject();
		deliveryAddress.put(Constants.IS_SAME_CURRENT_KEY,
				Constants.IS_SAME_CURRENT_VALUE);
		deliveryAddress.put(Constants.ADDRESS_KEY, Utils
				.getStringValue(variables
						.get(WfImplConstants.DELIVERY_ADDRESS_LINE)));
		deliveryAddress.put(Constants.CITY_KEY, Utils
				.getStringValue(variables
						.get(WfImplConstants.DELIVERY_CITY)));
		deliveryAddress.put(Constants.PROVINCE_KEY, Utils
				.getStringValue(variables
						.get(WfImplConstants.DELIVERY_PROVINCE)));
		return deliveryAddress;
	}

	private JSONObject getMedicalQuestionnaire(Map<String, Object> variables)
			throws JSONException {
		JSONObject medicalQuestionnaire1 = new JSONObject();
		medicalQuestionnaire1.put(Constants.MEDICAL_Q1, Utils
				.getStringValue(variables.get(WfImplConstants.MEDICAL_Q1_KEY)));
		medicalQuestionnaire1.put(Constants.MEDICAL_Q2, Utils
				.getStringValue(variables.get(WfImplConstants.MEDICAL_Q2_KEY)));
		medicalQuestionnaire1.put(Constants.MEDICAL_Q3, Utils
				.getStringValue(variables.get(WfImplConstants.MEDICAL_Q3_KEY)));
		return medicalQuestionnaire1;
	}

	private JSONArray getInsuredDetails(Map<String, Object> variables)
			throws JSONException {
		JSONObject insuredDetails = new JSONObject();
		JSONArray insuredDetailsAsJsonArray = new JSONArray();
		insuredDetailsAsJsonArray.put(insuredDetails);
		insuredDetails.put(Constants.INSURED_NAME, Utils
				.getStringValue(variables
						.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		insuredDetails.put(Constants.INSURED_GENDER, Utils.convertVietnamGenderToEnglish(Utils
				.getStringValue(variables
						.get(WfImplConstants.GENDER_HYPERVERGE))).toLowerCase());
		String dob=null;
		try{
		dob=Utils.formatDate(
				Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE)), Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_SLASH);
		
		}
		catch(Exception e){
			dob=Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE));
		}
		insuredDetails.put(Constants.INSURED_DOB, dob);
		insuredDetails.put(Constants.INSURED_NATIONAL_ID, Utils
				.getStringValue(variables
						.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
		insuredDetails.put(Constants.INSURED_RELATIONSHIP, Constants.CHU_HOP_DONG);
		insuredDetails.put(Constants.INSURED_Address, Utils
				.getStringValue(variables
						.get(WfImplConstants.ADDRESS_HYPERVERGE)));
		return insuredDetailsAsJsonArray;
	}

	public JSONObject getDataFromIb(String accessToken, JSONObject requestPayload) {
		logger.info("-->Entering InsurerIntegration.getDataFromIb()");
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();
			headersMap.put(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE);
			headersMap.put(Constants.AUTHORIZATION_KEY, "Bearer " + accessToken);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.IB_PARTNER_POLICY_SLUG_KEY + "/"
					+ Constants.IB_PARTNER_POLICY_KEY + "/" + wfImplConfig.getIbCompany() + "/"
					+ Constants.IB_HASH_VALUE + "/?env="+wfImplConfig.getIbEnv();
			logger.info("HashGenerationUrl :: ==========================>" + hashGenerationUrl);

			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call Insurer post api :" + e.getMessage(), e);
		}
		logger.info("<-- Exiting InsurerIntegration.getDataFromIb()");
		return responseJson;
	}

	public HashMap<String, String> parseResponse(JSONObject responseAsJson) throws JSONException {
		HashMap<String, String> map = new HashMap<String, String>();
		String policyId = null;
		String pdfLink=null;
		if (responseAsJson != null) {
			
			JSONObject dataJson = responseAsJson.getJSONObject(Constants.INSURER_RESPONSE_DATA);
			
			policyId = Utils.getStringValue(dataJson.get(Constants.INSURER_RESPONSE_POLICYID));
			
			String policyNo = Utils.getStringValue(dataJson.get(Constants.INSURER_RESPONSE_POLICYNO));
			
			JSONArray filesJsonArray = dataJson.getJSONArray(Constants.INSURER_RESPONSE_FILES);
			
			for(int i=0; i<filesJsonArray.length();i++){
				
				JSONObject filesJsonobject = filesJsonArray.getJSONObject(i);
				
			if(filesJsonobject.has(Constants.INSURER_DOCUMENT_TYPE)){
				String documentType = filesJsonobject.getString(Constants.INSURER_DOCUMENT_TYPE);
				if(documentType.equalsIgnoreCase(Constants.INSURER_CNBH)){
					
					if(filesJsonobject.has(Constants.INSURER_RESPONSE_LINK)){
						pdfLink=filesJsonobject.getString(Constants.INSURER_RESPONSE_LINK);
					}
					break;
				}
			}
			}
			map.put(Constants.INSURER_RESPONSE_PDFLINK, pdfLink);
			map.put(Constants.INSURER_RESPONSE_POLICYID, policyId);
			map.put(Constants.INSURER_RESPONSE_POLICYNO, policyNo);

		}
		return map;
	}
}
