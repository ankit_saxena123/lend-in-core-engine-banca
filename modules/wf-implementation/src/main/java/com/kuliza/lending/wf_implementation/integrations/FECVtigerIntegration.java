package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.VtigerDropOffDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.VtigerDropoffModel;
import com.kuliza.lending.wf_implementation.models.VtigerGenericDropOffModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECVtigerIntegration {
	
	@Autowired
	private WfImplConfig wfImplConfig;
	@Autowired
	private WorkflowUserDao workflowUserDao;
	@Autowired
	private VtigerDropOffDao vtigerDropOffDao;
	
	private static final Logger logger = LoggerFactory.getLogger(FECVtigerIntegration.class);
	
	public JSONObject buildGenericRequestPayLoad(VtigerDropoffModel dropoff, boolean activeStatus,
			String lastDropOffStage) throws Exception {
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(dropoff.getUserName(), false);
		JSONObject requestPayload = new JSONObject();
		JSONObject requestBody = new JSONObject();
		String dob = null;
		requestBody.put("shi:SendDropOff.Sys.TransID", Utils.generateUUIDTransanctionId());
		requestBody.put("shi:SendDropOff.Sys.DateTime", Utils.getCurrentDateTime());
		requestBody.put("shi:SendDropOff.Sys.RequestorID", Constants.FEC_VTIGER_REQUESTOR_ID);
		if (workFlowUser != null) {
			requestBody.put("shi:SendDropOff.Info.PHONENUMBER", workFlowUser.getMobileNumber());
		}
		if (dropoff.getvTigerContactId() != null) {
			requestBody.put("shi:SendDropOff.Info.VTG_CONTACTID", dropoff.getvTigerContactId());
		}
		if(dropoff.getvTigerOpportunityId()!=null){
			requestBody.put("shi:SendDropOff.Info.VTG_CRMID", dropoff.getvTigerOpportunityId());
		}
		if(dropoff.getvTigerParentCrmId()!=null){
			requestBody.put("shi:SendDropOff.Info.VTG_PARENT_CRMID", dropoff.getvTigerParentCrmId());
		}
		if(dropoff.getvTigerParentCrmId()==null){
			VtigerDropoffModel vtigerModelList = vtigerDropOffDao.getTopParentCrmIdOfUser(dropoff.getUserName());
			if(vtigerModelList!=null){
				logger.info("set vtiger integration parent crmid"+vtigerModelList.getvTigerParentCrmId());	
				String parentCRMId = vtigerModelList.getvTigerParentCrmId();
				requestBody.put("shi:SendDropOff.Info.VTG_PARENT_CRMID", parentCRMId);
				if("Generic".equals(dropoff.getDropOffType())){
					VtigerDropoffModel vtigerModel = vtigerDropOffDao
							.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
									dropoff.getUserName(), "generic",
									Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
					if(vtigerModel!=null){
						vtigerModel.setvTigerParentCrmId(parentCRMId);
						logger.info(" VTiger Generic Integration set parent crmid"+parentCRMId);
						vtigerDropOffDao.save(vtigerModel);
					}
				}else{
					VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
					if(vtigerModel!=null){
						vtigerModel.setvTigerParentCrmId(parentCRMId);
						vtigerDropOffDao.save(vtigerModel);
						logger.info(" VTiger Enrollment Integration parent crmid "+parentCRMId);
					}
				}
				if("Generic".equals(dropoff.getDropOffType())){
					VtigerDropoffModel vtigerModel = vtigerDropOffDao
							.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
									dropoff.getUserName(), "generic",
									Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
					if(vtigerModel!=null)
						logger.info(" VTiger Generic Integration set parent crmid1"+vtigerModel.getvTigerParentCrmId());
				}else{
					VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
					if(vtigerModel!=null)
						logger.info(" VTiger Enrollment Integration parent crmid 2"+parentCRMId);
				}
				
			}else{
				logger.info("set vtiger integration parent crmid is null");
			}
		}
		if (dropoff.getDropOffType() != null) {
			requestBody.put("shi:SendDropOff.Info.DROP_OFF_TYPE", dropoff.getDropOffType());
		}
		requestBody.put("shi:SendDropOff.Info.ACTIVE_STATUS", activeStatus);
		if (workFlowUser != null) {
			requestBody.put("shi:SendDropOff.Info.FULLNAME", workFlowUser.getUsername());
		}
		if (workFlowUser != null) {
			requestBody.put("shi:SendDropOff.Info.GENDER", workFlowUser.getGender());
		}
		if (workFlowUser != null) {
			dob = workFlowUser.getDob();
			logger.info("WorkFlow User Dob================>" + dob);
		}
		if(dob==null){
			dob="00-00-0000";
		}
		logger.info("DOB--> :" + dob);
		if (dob.contains("/")) {
			requestBody.put("shi:SendDropOff.Info.DOB",
					Utils.formatDate(dob, Constants.DATE_FORMAT_SLASH, Constants.DATE_FORMAT_DASH_YEAR));
		} else {
			requestBody.put("shi:SendDropOff.Info.DOB",
					Utils.formatDate(dob, Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_DASH_YEAR));
		}
		if (lastDropOffStage != null) {
			requestBody.put("shi:SendDropOff.Info.LAST_DROP_OFF_STAGE", lastDropOffStage);
		}
		if (dropoff.getCurrentJourneyStage() != null) {
			requestBody.put("shi:SendDropOff.Info.CURRENT_JOURNEY_STAGE", dropoff.getCurrentJourneyStage());
		}
		if (dropoff.getProductSelection() != null) {
			requestBody.put("shi:SendDropOff.Info.PRODUCT_NAME", dropoff.getProductSelection());
		}
		requestBody.put("shi:SendDropOff.Info.CAMPAIGN", "Shield Drop-off");
		requestBody.put("shi:SendDropOff.Info.PREMIUM_FEE", "0");
		requestBody.put("shi:SendDropOff.Info.SUM_INSURED", "0");
		requestPayload.put("body", requestBody);
		return requestPayload;
	}
	
	public JSONObject buildRequestPayLoad(Map<String, Object> processVariable, VtigerDropoffModel dropoff,boolean activeStatus, String lastDropOffStage) throws Exception{
		WorkFlowUser workFlowUser=workflowUserDao.findByIdmUserNameAndIsDeleted(dropoff.getUserName(), false);;
		String dob=null;
		JSONObject requestPayload = new JSONObject();
		JSONObject requestBody = new JSONObject();
		requestBody.put("shi:SendDropOff.Sys.TransID", Utils.generateUUIDTransanctionId());
		requestBody.put("shi:SendDropOff.Sys.DateTime", Utils.getCurrentDateTime());
		requestBody.put("shi:SendDropOff.Sys.RequestorID", Constants.FEC_VTIGER_REQUESTOR_ID);
		if(processVariable.containsKey(WfImplConstants.APPLICATION_MOBILE_NUMBER)){
			String mobile=Utils.getStringValue(processVariable.get(WfImplConstants.APPLICATION_MOBILE_NUMBER));
			if (mobile.startsWith("0")) {
				mobile = mobile.substring(1, mobile.length());
			}
			requestBody.put("shi:SendDropOff.Info.PHONENUMBER", mobile);
		}
		else{
			if(workFlowUser!=null)
				requestBody.put("shi:SendDropOff.Info.PHONENUMBER", workFlowUser.getMobileNumber());
		}
		if(dropoff.getvTigerContactId()!=null){
			requestBody.put("shi:SendDropOff.Info.VTG_CONTACTID", dropoff.getvTigerContactId());
		}
		if(dropoff.getvTigerOpportunityId()!=null){
			requestBody.put("shi:SendDropOff.Info.VTG_CRMID", dropoff.getvTigerOpportunityId());
		}
		if(dropoff.getvTigerParentCrmId()!=null){
			requestBody.put("shi:SendDropOff.Info.VTG_PARENT_CRMID", dropoff.getvTigerParentCrmId());
		}
		if(dropoff.getvTigerParentCrmId()==null){
			VtigerDropoffModel vtigerModelList = vtigerDropOffDao.getTopParentCrmIdOfUser(dropoff.getUserName());
			if(vtigerModelList!=null){
				logger.info("set vtiger integration parent crmid"+vtigerModelList.getvTigerParentCrmId());	
				String parentCRMId = vtigerModelList.getvTigerParentCrmId();
				requestBody.put("shi:SendDropOff.Info.VTG_PARENT_CRMID", parentCRMId);
				if("Generic".equals(dropoff.getDropOffType())){
					VtigerDropoffModel vtigerModel = vtigerDropOffDao
							.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
									dropoff.getUserName(), "generic",
									Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
					if(vtigerModel!=null){
						vtigerModel.setvTigerParentCrmId(parentCRMId);
						logger.info(" VTiger Generic Integration set parent crmid11"+parentCRMId);
						vtigerDropOffDao.save(vtigerModel);
					}
				}else{
					VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
					if(vtigerModel!=null){
						vtigerModel.setvTigerParentCrmId(parentCRMId);
						vtigerDropOffDao.save(vtigerModel);
						logger.info(" VTiger Enrollment Integration parent crmid 12"+parentCRMId);
					}
				}
				if("Generic".equals(dropoff.getDropOffType())){
					VtigerDropoffModel vtigerModel = vtigerDropOffDao
							.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
									dropoff.getUserName(), "generic",
									Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
					if(vtigerModel!=null)
						logger.info(" VTiger Generic Integration set parent crmidg11"+vtigerModel.getvTigerParentCrmId());
				}else{
					VtigerDropoffModel vtigerModel = vtigerDropOffDao.findByProcessInstanceId(dropoff.getProcessInstanceId());
					if(vtigerModel!=null)
						logger.info(" VTiger Enrollment Integration parent crmid g12"+parentCRMId);
				}
				
			}else{
				logger.info("set vtiger integration parent crmid is null");
			}
		}
		if(dropoff.getDropOffType()!=null){
			requestBody.put("shi:SendDropOff.Info.DROP_OFF_TYPE", dropoff.getDropOffType());
		}
		
		if(processVariable.containsKey(WfImplConstants.NATIONAL_ID_HYPERVERGE)){
			requestBody.put("shi:SendDropOff.Info.NATIONAL_ID", processVariable.get(WfImplConstants.NATIONAL_ID_HYPERVERGE));
		}
		if(processVariable.containsKey(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)){
			String premiumFee=Utils.ConvertViatnumCurrencyToDigits((String)processVariable.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			if(premiumFee.isEmpty()){
				premiumFee="0";
			}
			requestBody.put("shi:SendDropOff.Info.PREMIUM_FEE", premiumFee);
		}
		else{
			requestBody.put("shi:SendDropOff.Info.PREMIUM_FEE", "0");
		}
		requestBody.put("shi:SendDropOff.Info.ACTIVE_STATUS", activeStatus);
		if(processVariable.containsKey(WfImplConstants.FULLNAME_HYPERVERGE)){
			requestBody.put("shi:SendDropOff.Info.FULLNAME",  Utils.getStringValue(processVariable.get(WfImplConstants.FULLNAME_HYPERVERGE)));
		}
		else{
			if(workFlowUser!=null)
				requestBody.put("shi:SendDropOff.Info.FULLNAME",workFlowUser.getUsername());
		}
		if(processVariable.containsKey(WfImplConstants.GENDER_HYPERVERGE)){
			requestBody.put("shi:SendDropOff.Info.GENDER", Utils.getStringValue(processVariable.get(WfImplConstants.GENDER_HYPERVERGE)));
		}

		else{
			if(workFlowUser!=null)
				requestBody.put("shi:SendDropOff.Info.GENDER", workFlowUser.getGender());
		}
		if(processVariable.containsKey(WfImplConstants.USER_DOB)){
			logger.info("================>Process Var DOB:"+processVariable.get(WfImplConstants.USER_DOB));
			dob=Utils.getStringValue(processVariable.get(WfImplConstants.USER_DOB));
			logger.info("Process Var================>DOB:"+dob);
			if("0000".equals(dob)){
				logger.info("Process Var equal================>DOB:0000");
				if(workFlowUser!=null){
					dob=workFlowUser.getDob();
					logger.info("WorkFlow User Dob================>DOB"+dob);
				}
			}
		}
		else{
			if(workFlowUser!=null){
				dob=workFlowUser.getDob();
			logger.info("WF User Var================>DOB:"+dob);
			}	
		}
		logger.info("================>DOB:"+dob);
		
		if(dob.contains("/"))
			requestBody.put("shi:SendDropOff.Info.DOB", Utils.formatDate(dob, Constants.DATE_FORMAT_SLASH, Constants.DATE_FORMAT_DASH_YEAR));
		else
		   requestBody.put("shi:SendDropOff.Info.DOB", Utils.formatDate(dob, Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_DASH_YEAR));
		
		if(processVariable.containsKey(WfImplConstants.PROTECTION_AMOUNT)){
			String protectionAmount=Utils.ConvertViatnumCurrencyToDigits((String)processVariable.get(WfImplConstants.PROTECTION_AMOUNT));
			if(protectionAmount.isEmpty()){
				protectionAmount="0";
			}
			requestBody.put("shi:SendDropOff.Info.SUM_INSURED", protectionAmount);
		}
		if(processVariable.containsKey(WfImplConstants.JOURNEY_PRODUCT_NAME)){
			requestBody.put("shi:SendDropOff.Info.PRODUCT_NAME", Utils.getStringValue(processVariable.get(WfImplConstants.JOURNEY_PRODUCT_NAME)));
		}
		if(processVariable.containsKey(WfImplConstants.JOURNEY_PLAN_TYPE)){
			String productId=Utils.getStringValue(processVariable.get(WfImplConstants.JOURNEY_PRODUCT_ID));
			String planType=null;
			if(productId.equals(WfImplConstants.TW_PRODUCT_ID)){
				//planType=Utils.getStringValue(processVariable.get(WfImplConstants.VTIGER_TW_PLAN_TYPE));
				planType=WfImplConstants.VTIGER_TW_PLAN_TYPE;
				logger.info("--------->>>>>>>>>>>>> TW PlanType******** "+planType);

			}
			else{
				planType=Utils.getStringValue(processVariable.get(WfImplConstants.JOURNEY_PLAN_TYPE));
				logger.info("--------->>>>>>>>>>>>> TW PlanType******** "+planType);

			}
			if(productId.equals(WfImplConstants.TL_PRODUCT_ID)){
				planType=Utils.getStringValue(processVariable.get(WfImplConstants.JOURNEY_PLAN_TYPE));
				String tlPlanType[] = planType.split(",");
				planType = tlPlanType[0]; 
				logger.info("--------->>>>>>>>>>>>> TL PlanType******** "+planType);
			}
			else{
				planType=Utils.getStringValue(processVariable.get(WfImplConstants.JOURNEY_PLAN_TYPE));
				logger.info("--------->>>>>>>>>>>>> PlanType******** "+planType);

			}	
			
			requestBody.put("shi:SendDropOff.Info.PRODUCT_PLAN", planType);
		}
		if(lastDropOffStage!=null){
			requestBody.put("shi:SendDropOff.Info.LAST_DROP_OFF_STAGE", lastDropOffStage);
		}
		if(dropoff.getCurrentJourneyStage()!=null){
			requestBody.put("shi:SendDropOff.Info.CURRENT_JOURNEY_STAGE", dropoff.getCurrentJourneyStage());
		}
		requestBody.put("shi:SendDropOff.Info.CAMPAIGN", "Shield Drop-off");
		
		if(processVariable.containsKey(WfImplConstants.JOURNEY_APPLICATION_ID)){
			requestBody.put("shi:SendDropOff.Info.APPLICATION_ID", Utils.getStringValue(processVariable.get(WfImplConstants.JOURNEY_APPLICATION_ID)));
		}
		
		
		requestPayload.put("body", requestBody);
		return requestPayload;
		
	}

	public JSONObject getDataFromService(JSONObject requestPayload) {
		
		logger.info("--------->>>>>>>>>>>>> inside VTiger Generic VTIGER getDataFromService******** ");
		
		logger.info("--------->>>>>>>>>>>>>  VTiger Generic  requestPayload******** "+requestPayload);
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();
			
			headersMap.put("Content-Type", Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.VTIGER_SOAP_SLUG_KEY
					+ Constants.VTIGER_IB_KEY + "/" + Constants.VTIGER_SEND_DROP_OFF_KEY + "/"
					+ wfImplConfig.getIbCompany() + "/" + Constants.VTIGER_HASH+"/?env="+wfImplConfig.getIbEnv()
					+ Constants.VTIGER_INTEGRATION_SOAP_KEY;
			logger.info("--------->>>>>>>>>>>>> " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(" VTiger Generic  unable to call vitger api :" + e.getMessage(), e);
		}
		logger.info("<-- ***************Exiting  VTiger Generic  getDataFromService()**************");
		return responseJson;
	}
	
public JSONObject getDataFromServiceForCreate(JSONObject requestPayload) {
		
		logger.info("--------->>>>>>>>>>>>> inside  VTiger Generic VTIGER getDataFromService******** ");
		
		logger.info("--------->>>>>>>>>>>>>  VTiger Generic  requestPayload******** "+requestPayload);
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();
			
			headersMap.put("Content-Type", Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.VTIGER_SOAP_SLUG_KEY
					+ Constants.VTIGER_IB_KEY + "/" + Constants.VTIGER_CREATE_DROP_OFF_KEY + "/"
					+ wfImplConfig.getIbCompany() + "/" + Constants.VTIGER_HASH+"/?env="+wfImplConfig.getIbEnv()
					+ Constants.VTIGER_INTEGRATION_SOAP_KEY;
			logger.info("--------->>>>>>>>>>>>> " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(" VTiger Generic  unable to call vitger api :" + e.getMessage(), e);
		}
		logger.info("<-- ***************Exiting  VTiger Generic  getDataFromService()**************");
		return responseJson;
	}

	public Map<String, Object> parseResponse(JSONObject response) throws Exception{
		Map<String, Object> parseResponseMap=new HashMap<String, Object>();
		parseResponseMap.put("status", false);
		if(response.has("NS1:SendDropOffResponse")){
			JSONObject sendDropOffJson=response.getJSONObject("NS1:SendDropOffResponse");
			if(sendDropOffJson.has("Details")){
				JSONObject detailsJson=sendDropOffJson.getJSONObject("Details");
				if(detailsJson.has("VTG_CONTACTID")){
					String contactId=detailsJson.getString("VTG_CONTACTID");
					String vtigerCrmId=detailsJson.getString("VTG_CRMID");
					parseResponseMap.put("contactId", contactId);
					parseResponseMap.put("vtigerCrmId", vtigerCrmId);
					parseResponseMap.put("status", true);
					
				}
			}
			
		}
		return parseResponseMap;
	}

}
