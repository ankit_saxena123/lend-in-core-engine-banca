package com.kuliza.lending.wf_implementation.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.notification.SNSEventSubscription;
import com.kuliza.lending.wf_implementation.notification.SNSParamInit;
import com.kuliza.lending.wf_implementation.pojo.NotificationParamForm;
import com.kuliza.lending.wf_implementation.pojo.UnSubscribeUserTopicForm;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@RestController
@RequestMapping("/snsnotify")
public class SNSNotificationController {

	@Autowired
	public SNSEventSubscription snsEventSubscription;
	
	@Autowired
	public WorkflowUserDao workflowUserDao;
	
	@Autowired
	private UserTopicsDao userTopicsDao;

	@Autowired
	public SNSParamInit snsParamInit;
	
	private final Logger logger = LoggerFactory.getLogger(SNSNotificationController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/create-topic")
	public ResponseEntity<Object> createTopic(
			@RequestBody NotificationParamForm notifyForm) {

		try {
			
			String topicArn = snsEventSubscription.createTopic(notifyForm.getTopic());
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,topicArn));
			
		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/publish-to-topic")
	public ResponseEntity<Object> publishMessageToTopic(
			@RequestBody NotificationParamForm notifyForm) {

		try {
			boolean status = snsEventSubscription.publishMessageToTopic(notifyForm.getTopic(),notifyForm.getTitle(),notifyForm.getBody());

			if(status){
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE));
			}else{
				return CommonHelperFunctions
						.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						Constants.INTERNAL_SERVER_ERROR_MESSAGE));
			}
			
		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/subscribe-topic")
	public ResponseEntity<Object> subscribeToTopic(
			@RequestBody NotificationParamForm notifyForm) {

		try {
			String topicArn = snsParamInit.getTopicsMap().get((notifyForm.getTopic()));

			String subscriberArn = snsEventSubscription.subscribeToTopic(topicArn,"application",notifyForm.getSubscriberArn());

				if(subscriberArn!=null){
					return CommonHelperFunctions
						.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,subscriberArn));
				}else{
					return CommonHelperFunctions
							.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
							Constants.INTERNAL_SERVER_ERROR_MESSAGE));
				}
				

		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/subscribeall-topic")
	public ResponseEntity<Object> subscribeAllUsersToTopic(
			@RequestBody NotificationParamForm notifyForm) {

		try {
			
			List<WorkFlowUser> workflowUsers = workflowUserDao.findAllByIsDeleted(false);

			for(WorkFlowUser workFlowUser : workflowUsers){
				if(workFlowUser.getArnEndPoint()!=null) {
					String topicArn = snsParamInit.getTopicsMap().get((notifyForm.getTopic()));

					String subscriberArn = snsEventSubscription.subscribeToTopic(topicArn,"application",workFlowUser.getArnEndPoint());
					if(subscriberArn!=null){
						UserTopics userTopic = new UserTopics();
						userTopic.setTopic(WfImplConstants.SH_TOPIC);
						userTopic.setUserName(workFlowUser.getIdmUserName());
						userTopic.setSubscriberArn(subscriberArn);
						userTopic.setWfUser(workFlowUser);
						userTopicsDao.save(userTopic);
					}else{
						logger.info("<==Subscription To Registration Topic is Failed For User==>"+workFlowUser.getIdmUserName());
					}
				}	
			}	
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE));
			
		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/unsubscribe")
	public ResponseEntity<Object> unSubscribeFromTopic(
			@RequestBody @Valid UnSubscribeUserTopicForm unSubscribeForm) {

		try {
			boolean status = snsEventSubscription.unSubscribeFromTopic(unSubscribeForm.getContactNumber(),unSubscribeForm.getTopic());

			if(status){
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE));
			}else{
				return CommonHelperFunctions
						.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						Constants.INTERNAL_SERVER_ERROR_MESSAGE));
			}
			
		} catch (Exception e) {

			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions
					.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					Constants.INTERNAL_SERVER_ERROR_MESSAGE));
		}

	}

}














