package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.wf_implementation.services.AccountManagementService;

@RestController
@RequestMapping("/account-management")
public class AccountManagementController {
	
	@Autowired
	private AccountManagementService accountManagementService;
	
private static final Logger logger = LoggerFactory.getLogger(AccountManagementController.class);
	
	@RequestMapping(method = RequestMethod.GET, value = "/getNIDData")
	public ResponseEntity<Object> getNIDData(Principal principal, @RequestParam String nidFrontDocId, @RequestParam String nidBackDocId){
	
	try{
		return CommonHelperFunctions.buildResponseEntity(accountManagementService.getNidData(principal.getName(), nidFrontDocId, nidBackDocId));
	}
	catch(Exception e){
		logger.error(CommonHelperFunctions.getStackTrace(e));
		return CommonHelperFunctions.buildResponseEntity(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
				Constants.INTERNAL_SERVER_ERROR_MESSAGE));
	}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getAccountDetails")
	public ApiResponse getAccountDetails(Principal principal) {
		return accountManagementService.getAccountDetails(principal.getName());
	}
}
