package com.kuliza.lending.wf_implementation.pojo;

import org.hibernate.validator.constraints.NotEmpty;

public class PostTransactionInput {
	
	@NotEmpty
	private String accountNumber;
	
	@NotEmpty
	private String processInstanceId;
	
	@NotEmpty
	private String tenor;
	
	@NotEmpty
	private String premiumAmount;

	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public PostTransactionInput() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PostTransactionInput(String accountNumber, String processInstanceId, String tenor) {
		super();
		this.accountNumber = accountNumber;
		this.processInstanceId = processInstanceId;
		this.tenor = tenor;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	
	
	
	

}
