package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.wf_implementation.models.WfUserDeviceRegister;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;

@Repository
public interface WfUserDeviceRegDao extends CrudRepository<WfUserDeviceRegister, Long> {

	public WfUserDeviceRegister findById(long id);

	public WfUserDeviceRegister findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<WfUserDeviceRegister> findByWfUserAndDeviceIdAndDeviceTypeAndIsDeleted(WorkFlowUser wfUser,
			String deviceId, DeviceType deviceType, boolean isDeleted);

	public List<WfUserDeviceRegister> findByWfUserAndIsDeleted(WorkFlowUser wfUser, boolean isDeleted);

	public List<WfUserDeviceRegister> findByWfUserAndDeviceIdAndDeviceTypeAndIsDeletedAndMpinNotNull(
			WorkFlowUser wfUser, String deviceId, DeviceType deviceType, boolean isDeleted);

	public List<WfUserDeviceRegister> findByWfUserAndIsDeletedAndMpinNotNull(WorkFlowUser wfUser, boolean isDeleted);
}
