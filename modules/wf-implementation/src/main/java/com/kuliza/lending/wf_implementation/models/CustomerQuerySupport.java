/**
 * 
 */
package com.kuliza.lending.wf_implementation.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kuliza.lending.common.model.BaseModel;

/**
 * @author Garun Mishra
 *
 */
@Entity
@Table(name = "customer_query_support")
public class CustomerQuerySupport extends BaseModel {

	@Column(nullable = true, name = "ticket_number")
	private String ticketNumber;

	@Column(nullable = true, name = "ticket_number_status")
	private String ticketNumberStatus;

	@Column(nullable = true, name = "idm_user_name")
	private String idmUsername;
	
	@Column(nullable = true, name = "customer_role")
	private String customerRole;

	@Column(nullable = true, name = "username")
	private String username;

	@Column(nullable = true, name = "customer_national_id")
	private String customerNationalId;
	
	@Column(nullable = true, name = "customer_mobile")
	private String customerMobileNumber;
	
	@Column(nullable = true, name = "customer_email")
	private String customerEmail;

	@Column(nullable = true, name = "customer_query_message", columnDefinition = "LONGTEXT")
	private String customerQueryMessage;

	@Column(nullable = true, name = "customer_query_message_thread", columnDefinition = "LONGTEXT")
	private String customerQueryMessageThread;

	@Column(nullable = true, name = "customer_message_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date customerMessageDateTime;

	@Column(nullable = true, name = "portal_user_name")
	private String portalUserName;

	@Column(nullable = true, name = "portal_user_email_id")
	private String portalUserEmailId;

	@Column(nullable = true, name = "portal_user_role")
	private String portalUserRole;

	@Column(nullable = true, name = "portal_query_message", columnDefinition = "LONGTEXT")
	private String portalQueryMessage;

	@Column(nullable = true, name = "portal_query_message_thread", columnDefinition = "LONGTEXT")
	private String portalQueryMessageThread;

	@Column(nullable = true, name = "portal_message_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date portalQueryMessageDateTime;

	@Column(nullable = true, name = "customer_query_topic_id")
	private long customerQueryTopicId;

	@Column(nullable = true, name = "customer_query_topic_description", columnDefinition = "LONGTEXT")
	private String customerQueryTopicDescription;
	
	

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getTicketNumberStatus() {
		return ticketNumberStatus;
	}

	public void setTicketNumberStatus(String ticketNumberStatus) {
		this.ticketNumberStatus = ticketNumberStatus;
	}

	public String getIdmUsername() {
		return idmUsername;
	}

	public void setIdmUsername(String idmUsername) {
		this.idmUsername = idmUsername;
	}

	public String getCustomerRole() {
		return customerRole;
	}

	public void setCustomerRole(String customerRole) {
		this.customerRole = customerRole;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCustomerNationalId() {
		return customerNationalId;
	}

	public void setCustomerNationalId(String customerNationalId) {
		this.customerNationalId = customerNationalId;
	}

	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerQueryMessage() {
		return customerQueryMessage;
	}

	public void setCustomerQueryMessage(String customerQueryMessage) {
		this.customerQueryMessage = customerQueryMessage;
	}

	public String getCustomerQueryMessageThread() {
		return customerQueryMessageThread;
	}

	public void setCustomerQueryMessageThread(String customerQueryMessageThread) {
		this.customerQueryMessageThread = customerQueryMessageThread;
	}

	public Date getCustomerMessageDateTime() {
		return customerMessageDateTime;
	}

	public void setCustomerMessageDateTime(Date customerMessageDateTime) {
		this.customerMessageDateTime = customerMessageDateTime;
	}

	public String getPortalUserName() {
		return portalUserName;
	}

	public void setPortalUserName(String portalUserName) {
		this.portalUserName = portalUserName;
	}

	public String getPortalUserEmailId() {
		return portalUserEmailId;
	}

	public void setPortalUserEmailId(String portalUserEmailId) {
		this.portalUserEmailId = portalUserEmailId;
	}

	public String getPortalUserRole() {
		return portalUserRole;
	}

	public void setPortalUserRole(String portalUserRole) {
		this.portalUserRole = portalUserRole;
	}

	public String getPortalQueryMessage() {
		return portalQueryMessage;
	}

	public void setPortalQueryMessage(String portalQueryMessage) {
		this.portalQueryMessage = portalQueryMessage;
	}

	public String getPortalQueryMessageThread() {
		return portalQueryMessageThread;
	}

	public void setPortalQueryMessageThread(String portalQueryMessageThread) {
		this.portalQueryMessageThread = portalQueryMessageThread;
	}

	public Date getPortalQueryMessageDateTime() {
		return portalQueryMessageDateTime;
	}

	public void setPortalQueryMessageDateTime(Date portalQueryMessageDateTime) {
		this.portalQueryMessageDateTime = portalQueryMessageDateTime;
	}

	public long getCustomerQueryTopicId() {
		return customerQueryTopicId;
	}

	public void setCustomerQueryTopicId(long customerQueryTopicId) {
		this.customerQueryTopicId = customerQueryTopicId;
	}

	public String getCustomerQueryTopicDescription() {
		return customerQueryTopicDescription;
	}

	public void setCustomerQueryTopicDescription(String customerQueryTopicDescription) {
		this.customerQueryTopicDescription = customerQueryTopicDescription;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	
	
	
	
	
}
