package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowUser;

@Repository
public interface WorkflowUserDao extends PagingAndSortingRepository<WorkFlowUser, Long>, CrudRepository<WorkFlowUser, Long> {

	public WorkFlowUser findById(long id);

	public WorkFlowUser findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public WorkFlowUser findByUsernameAndIsDeleted(String username, boolean isDeleted);
	
	public WorkFlowUser findByIdmUserNameAndIsDeleted(String username, boolean isDeleted);

	public WorkFlowUser findByMobileNumberAndIsDeleted(String mobile, boolean isDeleted);

	public List<WorkFlowUser> findAllByIsDeleted(Pageable paging, boolean isDeleted);
	public List<WorkFlowUser> findAllByIsDeleted(boolean isDeleted);
	//Search list options - portal dashboard
	public List<WorkFlowUser> findAllByEmailContainingIgnoreCase(String email);
	public List<WorkFlowUser> findAllByUsernameContainingIgnoreCase(@Param("username") String username);
	public List<WorkFlowUser> findAllByMobileNumberAndIsDeleted(String mobile, boolean isDeleted);
	public List<WorkFlowUser> findAllByNationalIdAndIsDeleted(String nationalId, boolean isDeleted);
}
