package com.kuliza.lending.wf_implementation.pojo;

import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotNull;

public class ProductRequest {

	@NotNull(message = "RequestBody cannot be null")
	private List<HashMap<String, Object>> keyList;

	public List<HashMap<String, Object>> getKeyList() {
		return keyList;
	}

	public void setKeyList(List<HashMap<String, Object>> keyList) {
		this.keyList = keyList;
	}

	

	
}
