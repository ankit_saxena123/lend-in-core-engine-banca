package com.kuliza.lending.wf_implementation.services;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.VtigerDropOffDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;

@Service
public class FECJourneyExpireService {

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;
	@Autowired
	private VtigerDropOffDao vtigerDropOffDao;

	@Autowired
	TaskService taskService;

	@Autowired
	private RuntimeService runtimeService;

	private static final Logger logger = LoggerFactory.getLogger(FECJourneyExpireService.class);

	/**
	 * marks the task as drop off in vtiger table.
	 * 
	 * @return ApiResponse
	 */
	public ApiResponse expire() {
		ApiResponse response;
		try {

			LocalDate date = LocalDate.now();
			String expiryDate = date.minusDays(7).toString();

			int vtiger_deleted_records = vtigerDropOffDao.setIsDeletedForExpiryJourney(expiryDate);

			logger.info(">>>>> vtiger_deleted_records  ><<<<<<" + vtiger_deleted_records);

			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.error("Error in expiry job :" + e.getMessage(), e);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	public void completeActiveTask(String processInstanceId, HashMap<String, Object> variables) {
		logger.info("processInstanceId--->" + processInstanceId);
		List<Task> task = taskService.createTaskQuery().processInstanceId(processInstanceId).active().list();
		if (task != null && !task.isEmpty() && task.size() == 1) {
			Task activeTask = task.get(0);
			variables.forEach((key, value) -> {
				runtimeService.setVariable(activeTask.getProcessInstanceId(), key, value);
				logger.info("<--Key-->" + key + "<--Value-->" + value);
			});
			logger.info("TaskName--->" + activeTask.getTaskDefinitionKey());
			taskService.complete(activeTask.getId());
			logger.info("TaskName--->" + activeTask.getTaskDefinitionKey());
		}
		logger.info("TaskCompletedForPID-->" + processInstanceId);
	}

	public ApiResponse completeTaskByTaskName(String processInstanceId, String taskDefKey) {
		ApiResponse apiResponse = null;
		logger.info("processInstanceId-->" + processInstanceId + "<--taskDefKey-->" + taskDefKey);
		try {
			Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).taskDefinitionKey(taskDefKey)
					.singleResult();
			if (task != null) {
				logger.info("Valid Task def key to process : " + taskDefKey);
				taskService.complete(task.getId());
				apiResponse = new ApiResponse(Constants.SUCCESS_CODE, Constants.SUCCESS_MESSAGE);
			} else {
				logger.info("Invalid Task def key to process : " + taskDefKey);
				apiResponse = new ApiResponse(Constants.FAILURE_CODE, Constants.FAILURE_MESSAGE);
			}
		} catch (Exception e) {
			logger.error("Exception in task Complete Process : " + Utils.getStackTrace(e));
		}
		return apiResponse;
	}

}