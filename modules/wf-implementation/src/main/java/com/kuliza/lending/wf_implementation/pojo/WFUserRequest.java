package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;

public class WFUserRequest {
	
	@NotNull(message = "userName cannot be null")
	private String userName;
	@NotNull(message = "gender cannot be null")
	private String gender;
	@NotNull(message = "dob cannot be null")
	private String dob;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	

}
