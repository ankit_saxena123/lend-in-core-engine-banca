package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "vtiger_generic_dropoff")
public class VtigerGenericDropOffModel extends BaseModel {

	@Column(name = "user_name", nullable = true)
	private String userName;
	@Column(name = "contact_number", nullable = true)
	private String contactNumber;
	@Column(name = "application_id", nullable = true)
	private String applicationId;
	@Column(name = "vtiger_contact_id", nullable = true)
	private String vTigerContactId;
	@Column(name = "vtiger_opportunity_id", nullable = true)
	private String vTigerOpportunityId;
	@Column(name = "status", nullable = true)
	private String status;
	@Column(name = "drop_off_type", nullable = true)
	private String dropOffType;
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(name = "is_updated_invtiger", nullable = false)
	private boolean isUpdatedInVtiger;
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	@Column(name = "inactive", nullable = false)
	private boolean inActive;
	@Column(name = "current_journey_stage", nullable = true)
	private String currentJourneyStage;
	@Column(name = "push_notification_count", nullable = false)
	private int pushNotificationCount = 0;
	@Column(name = "language_key", nullable = false)
	private String language;
	@Column(name = "product_selection", nullable = false)
	private String productSelection;


	@ManyToOne
	@JoinColumn(name = "wf_application_id")
	private WorkFlowApplication workFlowApplication;

	public String getUserName() {
		return userName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public String getvTigerContactId() {
		return vTigerContactId;
	}

	public String getvTigerOpportunityId() {
		return vTigerOpportunityId;
	}

	public String getStatus() {
		return status;
	}

	public String getDropOffType() {
		return dropOffType;
	}

	public boolean isUpdatedInVtiger() {
		return isUpdatedInVtiger;
	}

	public boolean isInActive() {
		return inActive;
	}

	public String getCurrentJourneyStage() {
		return currentJourneyStage;
	}

	public int getPushNotificationCount() {
		return pushNotificationCount;
	}

	public WorkFlowApplication getWorkFlowApplication() {
		return workFlowApplication;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public void setvTigerContactId(String vTigerContactId) {
		this.vTigerContactId = vTigerContactId;
	}

	public void setvTigerOpportunityId(String vTigerOpportunityId) {
		this.vTigerOpportunityId = vTigerOpportunityId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDropOffType(String dropOffType) {
		this.dropOffType = dropOffType;
	}

	public void setUpdatedInVtiger(boolean isUpdatedInVtiger) {
		this.isUpdatedInVtiger = isUpdatedInVtiger;
	}

	public void setInActive(boolean inActive) {
		this.inActive = inActive;
	}

	public void setCurrentJourneyStage(String currentJourneyStage) {
		this.currentJourneyStage = currentJourneyStage;
	}

	public void setPushNotificationCount(int pushNotificationCount) {
		this.pushNotificationCount = pushNotificationCount;
	}

	public void setWorkFlowApplication(WorkFlowApplication workFlowApplication) {
		this.workFlowApplication = workFlowApplication;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getProductSelection() {
		return productSelection;
	}

	public void setProductSelection(String productSelection) {
		this.productSelection = productSelection;
	}

	public VtigerGenericDropOffModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public VtigerGenericDropOffModel(String userName, String contactNumber, String applicationId,
			String vTigerContactId, String vTigerOpportunityId, String status, String dropOffType,
			boolean isUpdatedInVtiger, boolean inActive, String currentJourneyStage, int pushNotificationCount,
			String language, String productSelection, WorkFlowApplication workFlowApplication) {
		super();
		this.userName = userName;
		this.contactNumber = contactNumber;
		this.applicationId = applicationId;
		this.vTigerContactId = vTigerContactId;
		this.vTigerOpportunityId = vTigerOpportunityId;
		this.status = status;
		this.dropOffType = dropOffType;
		this.isUpdatedInVtiger = isUpdatedInVtiger;
		this.inActive = inActive;
		this.currentJourneyStage = currentJourneyStage;
		this.pushNotificationCount = pushNotificationCount;
		this.language = language;
		this.productSelection = productSelection;
		this.workFlowApplication = workFlowApplication;
	}

	@Override
	public String toString() {
		return "VtigerGenericDropOffModel [userName=" + userName + ", contactNumber=" + contactNumber
				+ ", applicationId=" + applicationId + ", vTigerContactId=" + vTigerContactId + ", vTigerOpportunityId="
				+ vTigerOpportunityId + ", status=" + status + ", dropOffType=" + dropOffType + ", isUpdatedInVtiger="
				+ isUpdatedInVtiger + ", inActive=" + inActive + ", currentJourneyStage=" + currentJourneyStage
				+ ", pushNotificationCount=" + pushNotificationCount + ", language=" + language + ", productSelection="
				+ productSelection + ", workFlowApplication=" + workFlowApplication + "]";
	}

}
