package com.kuliza.lending.wf_implementation.services;

import java.util.Map;
import java.util.Stack;

import org.flowable.engine.RuntimeService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.integrations.FECreditCMSCheckEligibilityIntegration;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class FECreditCMSCheckEligibilityService {

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private FECreditCMSCheckEligibilityIntegration fecCreditEligibility;

	private static final Logger logger = LoggerFactory.getLogger(FECreditCMSCheckEligibilityService.class);

	public ApiResponse postCMSCheckEligibility(String processInstanceId) throws Exception {
		logger.info("postCMSCheckEligibility called for : "+processInstanceId );
		Map<String, Object> variables = runtimeService.getVariables(processInstanceId);
		return postCMSCheckEligibility(processInstanceId, variables);
	}

	public double emi_calculator(double p, double r, int t) 
	{	        
   		try {
        		double emi; 
        		r = r / (12 * 100); // one month interest 
        		//t = t * 12; // one month period 
        		logger.info("<====rate is : "+r+"<==============>");
        		emi = (p * r * (double)Math.pow(1 + r, t))/(double)(Math.pow(1 + r, t) - 1); 
        		logger.info("<======emi :"+emi+"<===============>");
        		return emi;
   		}catch(Exception e){
                        logger.error("*** Exception Occured While Calculate EMI ****" + e);
                        logger.error("*** EMI intrest rate ***" + r);
                        logger.error("*** EMI Principle Amount ***" + p);
                        logger.error("*** EMI Time ***" + t);
                        return -1; 

   		}		
	}

	private ApiResponse postCMSCheckEligibility(String processInstanceId, Map<String, Object> variables)
			throws Exception {
			logger.info("------>>>>>> what you pay------->> : " + variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			String premiumAmount=Utils.ConvertViatnumCurrencyToDigits(Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT)));
			logger.info("fullPremiumAmount --->" +  premiumAmount);
			variables.put(WfImplConstants.FULL_PREMIUM_AMOUNT, premiumAmount);
			
			double permonth = emi_calculator(Utils.getDoubleValue(premiumAmount),30,12);
			//double roundOff = Math.round(permonth * 100.0) / 100.0;
			Long ceil = (long) (Math.round(permonth));
			String monthly=Utils.getStringValue(ceil);
			String perMonthAmount=addDecimal(monthly);
			logger.info("**********added decimal ************  "+ perMonthAmount);
			logger.info("**********monthly Round off ************  "+ monthly);
			logger.info("**********permonth ************  "+ permonth);
			logger.info("**********premiumAmount********  "+premiumAmount);
			
			logger.info("CMS Eligibility details for ProcessInstanceId "+processInstanceId+" ,perMonthAmount : "+perMonthAmount+",premiumAmount : "+premiumAmount);
			
			Map<String, Object> mapResponse = null;
			try {

			JSONObject requestPayload = fecCreditEligibility.getrequestPayload(variables);
			JSONObject responseAsJson = fecCreditEligibility.getDataFromService(requestPayload);
			mapResponse = fecCreditEligibility.parseResponse(responseAsJson);
			logger.info("********** postCMSCheckEligibility map response **************" + mapResponse);

			if ((Boolean) mapResponse.get(Constants.CMS_CHECK_ELIGIBILITY_STATUS) == true
					&& (Boolean) mapResponse.get(Constants.CMS_CHECK_ELIGIBILITY_STATUS) != null) {
				mapResponse.put(Constants.CMS_PREMIUM_AMOUNT, premiumAmount);
				mapResponse.put(Constants.CMS_PER_MONTH, perMonthAmount);
				
				logger.info("CMS Eligibility Response for "+processInstanceId+", Response : "+mapResponse.toString());
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, mapResponse);
			}

			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.DATA_NOT_FOUND, mapResponse);

			} catch (Exception e) {
			logger.error("unable to call insurer api :" + e.getMessage(), e);
			throw e;
			} finally {
			logger.info(" **************************End of postCMSCheckEligibility*************");
			}

			}
	
	
	
	
	
	public static String addDecimal(String amount) {
		int count=0;
		String finalAmount="";
		Stack<Character> stackedAmount=new Stack<>();
		try {
			for(int i=amount.length()-1;i>=0;i--) {
				if(count==3) {
					stackedAmount.push('.');
					count=0;
				}
				count++;
				char c=amount.charAt(i);
				stackedAmount.push(c);
			}
			for(int j=stackedAmount.size()-1;j>=0;j--) {
				finalAmount=finalAmount+stackedAmount.get(j);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return finalAmount;
	}
	
	
	
	
	
}