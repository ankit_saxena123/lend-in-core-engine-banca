package com.kuliza.lending.wf_implementation.fpt;


import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.WfImplConfig;

//import com.kuliza.baseLOS.fpt.esignCloud.utils.ESignCloudConstants;
//import com.kuliza.baseLOS.utils.Constants;

import com.kuliza.lending.wf_implementation.pojo.FPTRequest;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.FPTUtils;
import com.kuliza.lending.wf_implementation.utils.SSLUtilities;

import vn.com.fis.ws.AgreementDetails;
import vn.com.fis.ws.CredentialData;
import vn.com.fis.ws.Services;
import vn.com.fis.ws.Services_Service;
import vn.com.fis.ws.SignCloudReq;
import vn.com.fis.ws.SignCloudResp;

@Service
public class PrepareCertificateForSignCloudService {

	@Autowired
	private FPTUtils fptUtils;
	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(PrepareCertificateForSignCloudService.class);

	public Map<String, Object> prepareCertificateForSignCloud(FPTRequest fptRequest) throws Exception {

		ObjectMapper map = new ObjectMapper();
		logger.info("FPTRequest : " + map.writeValueAsString(fptRequest));
		
//		validateFPTRequest(fptRequest);

		SSLUtilities.trustAllHostnames();
		SSLUtilities.trustAllHttpsCertificates();
		Services_Service services = fptUtils.createFPTService();

		Services ws = services.getServicesPort();

		String timestamp = String.valueOf(System.currentTimeMillis());

//		String data2sign = ESignCloudConstants.RELYING_PARTY_USER + ESignCloudConstants.RELYING_PARTY_PASSWORD
//				+ ESignCloudConstants.RELYING_PARTY_SIGNATURE + timestamp;
//		String pkcs1Signature = FPTUtils.getPKCS1Signature(data2sign, ESignCloudConstants.RELYING_PARTY_KEY_STORE,
//				ESignCloudConstants.RELYING_PARTY_KEY_STORE_PASSWORD);
		
		String pkcs1Signature = fptUtils.getPKCS1Signature(timestamp);

		SignCloudReq signCloudReq = new SignCloudReq();
		signCloudReq.setRelyingParty(wfImplConfig.getFptRelyingParty());
		signCloudReq.setAgreementUUID(fptRequest.getAgreementUUID());
		signCloudReq.setEmail(fptRequest.getAuthorizationEmail());
		signCloudReq.setMobileNo(fptRequest.getAuthorizationMobileNo());

		signCloudReq.setCertificateProfile(fptRequest.getCertificateProfile());

		AgreementDetails agreementDetails = new AgreementDetails();
		agreementDetails.setPersonalName(fptRequest.getPersonalName());
		agreementDetails.setPersonalID(fptRequest.getPersonalID());
		agreementDetails.setLocation(fptRequest.getLocation());
		agreementDetails.setStateOrProvince(fptRequest.getStateProvince());
		agreementDetails.setCountry(fptRequest.getCountry());

		signCloudReq.setAgreementDetails(agreementDetails);

		CredentialData credentialData = new CredentialData();
		credentialData.setUsername(wfImplConfig.getFptRelyingPartyUserName());
		credentialData.setPassword(wfImplConfig.getFptRelyingPartyPassword());
		credentialData.setTimestamp(timestamp);
		credentialData.setSignature(wfImplConfig.getFptRelyingPartySignature());

		credentialData.setPkcs1Signature(pkcs1Signature);
		signCloudReq.setCredentialData(credentialData);

		logger.info("CredentialData For FPT : " + map.writeValueAsString(credentialData));
		
		logger.info("Request For FPT : " + map.writeValueAsString(signCloudReq));
		SignCloudResp signCloudResp = ws.prepareCertificateForSignCloud(signCloudReq);
		
		
		logger.info("Response From FPT : " + map.writeValueAsString(signCloudResp));
		logger.info("Response code form api : " + signCloudResp.getResponseCode());
		logger.info("Message from api : " + signCloudResp.getResponseMessage());

		Map<String, Object> response = new HashMap<String, Object>();
		response.put(Constants.STATUS_MAP_KEY, signCloudResp.getResponseCode());
		response.put(Constants.STATUS_MESSAGE, signCloudResp.getResponseMessage());
		response.put(Constants.FPT_BILL_CODE, signCloudResp.getBillCode());
		
		return response;
	}

	
	
	
	



}
