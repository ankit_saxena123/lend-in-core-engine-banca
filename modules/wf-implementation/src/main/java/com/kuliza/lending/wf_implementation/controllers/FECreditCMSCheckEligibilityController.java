package com.kuliza.lending.wf_implementation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.pojo.PostTransactionInput;
import com.kuliza.lending.wf_implementation.services.FECreditCMSCheckEligibilityService;
import com.kuliza.lending.wf_implementation.services.FECreditCMSPostTransactionService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping("/cms")
public class FECreditCMSCheckEligibilityController {
	@Autowired
	private FECreditCMSCheckEligibilityService cmsService;

	@Autowired
	private FECreditCMSPostTransactionService postTransaction;

	private final Logger logger = LoggerFactory.getLogger(FECreditCMSCheckEligibilityController.class);

	@PostMapping("/checkEligibility")
	public ResponseEntity<Object> postPolicyDetails(@RequestParam String processInstanceId) {

		try {
			return CommonHelperFunctions.buildResponseEntity(cmsService.postCMSCheckEligibility(processInstanceId));

		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}

	}

	@PostMapping("/postTransaction")
	public ResponseEntity<Object> postTransaction(@RequestBody PostTransactionInput body) {

		try {
			return CommonHelperFunctions.buildResponseEntity(postTransaction.CMSpostTransaction(body));

		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}

	}

}
