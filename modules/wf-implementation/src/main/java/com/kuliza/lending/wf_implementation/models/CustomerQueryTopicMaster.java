/**
 * 
 */
package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

/**
 * @author garun
 *
 */

@Entity
@Table(name = "customer_query_topics_master")
public class CustomerQueryTopicMaster  extends BaseModel{


	@Column(nullable = true, name = "customer_query_topic_id")
	private long customerQueryTopicId;
	
	@Column(nullable = true, name = "customer_query_topic_description", columnDefinition = "LONGTEXT")
	private String customerQueryTopicDescription;

	public long getCustomerQueryTopicId() {
		return customerQueryTopicId;
	}

	public void setCustomerQueryTopicId(long customerQueryTopicId) {
		this.customerQueryTopicId = customerQueryTopicId;
	}

	public String getCustomerQueryTopicDescription() {
		return customerQueryTopicDescription;
	}

	public void setCustomerQueryTopicDescription(String customerQueryTopicDescription) {
		this.customerQueryTopicDescription = customerQueryTopicDescription;
	}

	
	

}
