package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplicationDocs;

public interface WorkFlowApplicationDocsDao extends CrudRepository<WorkFlowApplicationDocs,Long>
{

	List<WorkFlowApplicationDocs> findListByWorkFlowApplicationAndDocType(WorkFlowApplication WorkFlowApplication, Integer docTypeId);
}
