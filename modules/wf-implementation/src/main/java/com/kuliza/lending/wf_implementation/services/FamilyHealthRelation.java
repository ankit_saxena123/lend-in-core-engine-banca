package com.kuliza.lending.wf_implementation.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.integrations.FECreditHypervergeIntegration;
import com.kuliza.lending.wf_implementation.integrations.MasterApiIntegration;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service("familyHealthRelation")
public class FamilyHealthRelation {
	private static final Logger logger = LoggerFactory.getLogger(FamilyHealthRelation.class);

	@Autowired
	private FECreditHypervergeIntegration hypervergeIntegration;

	@Autowired
	private FECHypervergeService fecHyperverge;

	@Autowired
	private MasterApiIntegration master;

	@SuppressWarnings("finally")
	public DelegateExecution familyRelation(DelegateExecution delegateExecution) {
		Map<String, Object> variables = delegateExecution.getVariables();
		logger.info("************** variables**** " + variables);
		String planId = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PLAN_ID));
		logger.info("************** planId**** " + planId);
		String language = Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY));
		@SuppressWarnings("unchecked")
		List<Map<String, String>> familyMemberListMap = (List<Map<String, String>>) variables
				.get(WfImplConstants.FAMILY_MEMBER_LIST);

		String gender = null;
		String relation = null;
		String premiumValues = null;
                String relationLable = null;
                String calculatedAge=null;
		try {
			switch (language) {
			case Constants.LANGUAGE_VALUE:
				for (Map<String, String> memberMap : familyMemberListMap) {
					gender = memberMap.get(WfImplConstants.HYPERVERGE_GENDER);
					relation = memberMap.get(WfImplConstants.RELATION);
					if(memberMap.containsKey(WfImplConstants.DOB_HYPERVERGE)) {
					calculatedAge = Utils
							.getStringValue(Utils.getAgeFromDOB((memberMap.get(WfImplConstants.DOB_HYPERVERGE))));
					}
					else {
						calculatedAge = Utils
								.getStringValue(Utils.getAgeFromDOB((memberMap.get(WfImplConstants.HYPERVERGE_DOB))));
						}
					
					logger.info("************** calculatedAge**** " + calculatedAge);
					premiumValues = getpremium(calculatedAge, planId);
					logger.info("************** premiumValues **** " + premiumValues);
					
					relationLable = Constants.FAMILY_RELATION.get(relation + "_" + gender);
					memberMap.put(Constants.RELATIONSHIP_LABEL, relationLable);
					memberMap.put(WfImplConstants.MEMBER_PREMIUM, premiumValues);
				}
				break;
			default:
				for (Map<String, String> memberMap : familyMemberListMap) {
					gender = memberMap.get(WfImplConstants.HYPERVERGE_GENDER);
					relation = memberMap.get(WfImplConstants.RELATION);
					if(memberMap.containsKey(WfImplConstants.DOB_HYPERVERGE)) {
						calculatedAge = Utils
								.getStringValue(Utils.getAgeFromDOB((memberMap.get(WfImplConstants.DOB_HYPERVERGE))));
						}
						else {
							calculatedAge = Utils
									.getStringValue(Utils.getAgeFromDOB((memberMap.get(WfImplConstants.HYPERVERGE_DOB))));
							}
					logger.info("************** calculatedAge**** " + calculatedAge);
					premiumValues = getpremium(calculatedAge, planId);
					
					relationLable = Constants.FAMILY_RELATION_VI.get(relation + "_" + gender);
					memberMap.put(Constants.RELATIONSHIP_LABEL, relationLable);
					memberMap.put(WfImplConstants.MEMBER_PREMIUM, premiumValues);

				}
			}
			logger.info(" variables after adding member premium========>>>>>>>>  " + variables);
			if (variables.containsKey(WfImplConstants.NATIONAL_ID_HYPERVERGE)) {
				familyMemberListMap.get(0).put(WfImplConstants.APPLICATION_MOBILE_NUMBER,
                        Utils.getStringValue(variables.get(WfImplConstants.APPLICATION_MOBILE_NUMBER)));
				familyMemberListMap.get(0).put(WfImplConstants.USER_FULL_NAME,
						Utils.getStringValue(variables.get(WfImplConstants.USER_FULL_NAME)));
				familyMemberListMap.get(0).put(WfImplConstants.USER_DOB,
						Utils.getStringValue(variables.get(WfImplConstants.USER_DOB)));
				familyMemberListMap.get(0).put(WfImplConstants.USER_GENDER,
						Utils.getStringValue(variables.get(WfImplConstants.USER_GENDER)));
				familyMemberListMap.get(0).put(WfImplConstants.NATIONAL_ID_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
				familyMemberListMap.get(0).put(WfImplConstants.GENDER_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.GENDER_HYPERVERGE)));
				familyMemberListMap.get(0).put(WfImplConstants.FULLNAME_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.FULLNAME_HYPERVERGE)));
				familyMemberListMap.get(0).put(WfImplConstants.NATIONALITY_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.NATIONALITY_HYPERVERGE)));
				familyMemberListMap.get(0).put(WfImplConstants.DOB_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE)));
				familyMemberListMap.get(0).put(WfImplConstants.ADDRESS_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.ADDRESS_HYPERVERGE)));
				familyMemberListMap.get(0).put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
						Utils.getStringValue(variables.get(WfImplConstants.PLACEOFORIGIN_HYPERVERGE)));
				if (variables.containsKey(WfImplConstants.DELIVERY_ADDRESS_LINE)) {
					familyMemberListMap.get(0).put(WfImplConstants.DELIVERY_ADDRESS_LINE,
							Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_ADDRESS_LINE)));
				} else {
					familyMemberListMap.get(0).put(WfImplConstants.DELIVERY_ADDRESS_LINE, "-");
				}
				if (variables.containsKey(WfImplConstants.DELIVERY_ADDRESS_LINE)) {
					familyMemberListMap.get(0).put(WfImplConstants.DELIVERY_CITY,
							Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_CITY)));
				} else {
					familyMemberListMap.get(0).put(WfImplConstants.DELIVERY_CITY, "-");
				}
				if (variables.containsKey(WfImplConstants.DELIVERY_PROVINCE)) {
					familyMemberListMap.get(0).put(WfImplConstants.DELIVERY_PROVINCE,
							Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_PROVINCE)));
				} else {
					familyMemberListMap.get(0).put(WfImplConstants.DELIVERY_PROVINCE, "-");
				}

			}

			logger.info("************** family members**** " + familyMemberListMap);
			delegateExecution.setVariable(WfImplConstants.FAMILY_MEMBER_LIST, familyMemberListMap);

		} catch (Exception e) {
			logger.error("error in familyRelation method ", e);
		} finally {
			return delegateExecution;

		}

	}

	@SuppressWarnings({ "finally", "unchecked" })
	public DelegateExecution getNidDetails(DelegateExecution delegateExecution) {
		logger.info("************** INSIDE getNidDetails **** ");
		Map<String, Object> processVariables = new HashMap<String, Object>();

		try {
			delegateExecution.setVariable(WfImplConstants.NID_ERROR, false);
			Map<String, Object> variables = delegateExecution.getVariables();
			String language = Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY));
			logger.info("************** variables getNidDetails **** " + variables);
			Integer index = Utils.getIntegerValue(delegateExecution.getVariable(WfImplConstants.FAMILY_MEMBER_COUNT));

			List<Map<String, String>> familyMemberListMap = (List<Map<String, String>>) variables
					.get(WfImplConstants.FAMILY_MEMBER_LIST);
			Map<String, String> singleMember = familyMemberListMap.get(index);
			String dmsFront = singleMember.get(WfImplConstants.NID_FRONT);
			String dmsBack = singleMember.get(WfImplConstants.NID_BACK);

//			String mobileNumber = Utils
//					.getStringValue(delegateExecution.getVariable(WfImplConstants.APPLICATION_MOBILE_NUMBER));
			logger.info("**********dmsFront******** :" + dmsFront);
			logger.info("**********dmsBackend******* :" + dmsBack);
			String nationalId = null;
			Map<String, Object> hypervergeFrontData = new HashMap<String, Object>();
			Map<String, Object> hypervergeBackData = new HashMap<String, Object>();
			Map<String, Object> cifDataMap = new HashMap<String, Object>();
			// nid front
			CloseableHttpResponse closeableHttpResponse = fecHyperverge.callHyperVerge(dmsFront);
			int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
			String hypervergeFrontResponse = EntityUtils.toString(closeableHttpResponse.getEntity());
			logger.info("hyperverge Front Response : " + hypervergeFrontResponse);
			delegateExecution.setVariable(WfImplConstants.HYPERVERGE_FRONT_RESPONSE, hypervergeFrontResponse);
			closeableHttpResponse.close();

			if (statusCode != HttpStatus.OK.value()) {
				String error = new JSONObject(hypervergeFrontResponse).getString(WfImplConstants.CIF_ERROR);
				logger.info("hyperverge call failed : " + error);
				processVariables.put(WfImplConstants.NID_ERROR, true);
				processVariables.put(WfImplConstants.NID_ERROR_MESSAGE, "error in nid front :" + error);
			} else {
				hypervergeFrontData = hypervergeIntegration.parseNationalIdFrontData(hypervergeFrontResponse);

				nationalId = (String) hypervergeFrontData.getOrDefault(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY,
						"");
			}
			// nid back
			closeableHttpResponse = fecHyperverge.callHyperVerge(dmsBack);
			statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
			String hypervergeBackResponse = EntityUtils.toString(closeableHttpResponse.getEntity());
			logger.info("hyperverge back Response : " + hypervergeBackResponse);
			closeableHttpResponse.close();
			delegateExecution.setVariable(WfImplConstants.HYPERVERGE_BACK_RESPONSE, hypervergeBackResponse);
			if (statusCode != HttpStatus.OK.value()) {
				String error = new JSONObject(hypervergeFrontResponse).getString(WfImplConstants.CIF_ERROR);
				logger.info("hyperverge call failed : " + error);
				processVariables.put(WfImplConstants.NID_ERROR, true);
				processVariables.put(WfImplConstants.NID_ERROR_MESSAGE, " error in nid back :" + error);

			} else {
				hypervergeBackData = hypervergeIntegration.parseNationalIdBackData(hypervergeBackResponse);
			}
			logger.info("nationalId is : " + nationalId);
			// cif
			if (singleMember.get(WfImplConstants.RELATION).equals(Constants.YOU)) {
				String mobileNumber = Utils
						.getStringValue(delegateExecution.getVariable(WfImplConstants.APPLICATION_MOBILE_NUMBER));
				cifDataMap = fecHyperverge.getCIFDataMap(nationalId, mobileNumber, delegateExecution);
				processVariables = fecHyperverge.addProcessVariables(hypervergeFrontData, hypervergeBackData,
						cifDataMap, nationalId, delegateExecution);
				if (processVariables.get(WfImplConstants.GENDER_HYPERVERGE).equals(Constants.NID_MALE)
						&& language.equals(Constants.BANCA_DEFAULT_LANGUAGE)) {
					delegateExecution.setVariable(WfImplConstants.GENDER_HYPERVERGE, Constants.NID_MALE_VI);
					delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE, Constants.NATIONALITY_VI);

				} else if (processVariables.get(WfImplConstants.GENDER_HYPERVERGE).equals(Constants.NID_FEMALE)
						&& language.equals(Constants.BANCA_DEFAULT_LANGUAGE)) {
					delegateExecution.setVariable(WfImplConstants.GENDER_HYPERVERGE, Constants.NID_FEMALE_VI);
					delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE, Constants.NATIONALITY_VI);
				}
				if (processVariables.isEmpty()) {
					logger.info("user data not found in both hyperverge and cif");
					delegateExecution.setVariable(WfImplConstants.NID_ERROR, true);
					delegateExecution.setVariable(WfImplConstants.NID_ERROR_MESSAGE,
							"user data not found in both hyperverge and cif");
				}
				processVariables.put(WfImplConstants.APPLICANT_MOBILE_NO, mobileNumber);
				delegateExecution.setVariables(processVariables);

			} else {
				boolean hypervergeFrontStatus = (boolean) hypervergeFrontData
						.getOrDefault(WfImplConstants.HYPERVERGE_DATA_FRONT_COMPLETE_STATUS_FLAG, false);
				boolean hypervergeBackStatus = (boolean) hypervergeBackData
						.getOrDefault(WfImplConstants.HYPERVERGE_DATA_BACK_COMPLETE_STATUS_FLAG, false);
				logger.info("hypervergeFrontStatus : " + hypervergeFrontStatus);
				logger.info("hypervergeBackStatus : " + hypervergeBackStatus);
				if (hypervergeFrontStatus && hypervergeBackStatus) {
					logger.info("hyperverge data is available");
					String nidName = (String) hypervergeFrontData
							.getOrDefault(WfImplConstants.BORROWER_FULL_NAME_FROM_HYPERVERGE_KEY, "");
					String nidGender = (String) hypervergeFrontData
							.getOrDefault(WfImplConstants.GENDER_FROM_HYPERVERGE_KEY, "");
					nidGender = hypervergeIntegration.getGender(nidGender);
					if (nidGender.isEmpty()) {
						nidGender = singleMember.get(WfImplConstants.HYPERVERGE_GENDER);
					}
					String nidDob = (String) hypervergeFrontData
							.getOrDefault(WfImplConstants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY, "");
					delegateExecution.setVariable(WfImplConstants.NATIONAL_ID_HYPERVERGE, Utils
							.getStringValue(hypervergeFrontData.get(WfImplConstants.NATIONAL_ID_FROM_HYPERVERGE_KEY)));
					delegateExecution.setVariable(WfImplConstants.FULLNAME_HYPERVERGE, nidName);
					delegateExecution.setVariable(WfImplConstants.GENDER_HYPERVERGE, nidGender);
					delegateExecution.setVariable(WfImplConstants.DOB_HYPERVERGE, nidDob);
					delegateExecution.setVariable(WfImplConstants.PLACEOFORIGIN_HYPERVERGE, Utils
							.getStringValue(hypervergeFrontData.get(WfImplConstants.PROVINCE_FROM_HYPERVERGE_KEY)));
					delegateExecution.setVariable(WfImplConstants.ADDRESS_HYPERVERGE,
							Utils.getStringValue(hypervergeFrontData.get(WfImplConstants.ADDRESS_FROM_HYPERVERGE_KEY)));
					delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE, WfImplConstants.VIETNAMESE);
					delegateExecution.setVariable(WfImplConstants.USER_DOB, singleMember.get(Constants.DOB));
					delegateExecution.setVariable(WfImplConstants.USER_GENDER,
							singleMember.get(WfImplConstants.HYPERVERGE_GENDER));
					// delegateExecution.setVariable(WfImplConstants.USER_FULL_NAME, "");
					delegateExecution.setVariable(WfImplConstants.APPLICANT_MOBILE_NO, "");
					delegateExecution.setVariable(WfImplConstants.APPLICATION_MOBILE_NUMBER, "");
					delegateExecution.setVariable(WfImplConstants.APPLICANT_EMAIL, "");
				}
			}
		} catch (Exception e) {
			delegateExecution.setVariable(WfImplConstants.NID_ERROR, true);
			delegateExecution.setVariable(WfImplConstants.NID_ERROR_MESSAGE, e.getMessage());
			logger.error("failed to get nid details : " + e.getMessage(), e);

		} finally {
			return delegateExecution;
		}

	}

	@SuppressWarnings("unchecked")
	public DelegateExecution setFamilyDataHyperverse(DelegateExecution delegateExecution) {
		Map<String, Object> variables = delegateExecution.getVariables();
		logger.info("************** variables**** inside setFamilyDataHyperverse: " + variables);
		try {
			Integer index = Utils.getIntegerValue(delegateExecution.getVariable(WfImplConstants.FAMILY_MEMBER_COUNT));

			List<Map<String, String>> familyMemberListMap = (List<Map<String, String>>) variables
					.get(WfImplConstants.FAMILY_MEMBER_LIST);
			Map<String, String> singleMember = familyMemberListMap.get(index);
			singleMember.put(WfImplConstants.NATIONAL_ID_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.NATIONAL_ID_HYPERVERGE)));
			singleMember.put(WfImplConstants.GENDER_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.GENDER_HYPERVERGE)));
			singleMember.put(WfImplConstants.FULLNAME_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.FULLNAME_HYPERVERGE)));
			singleMember.put(WfImplConstants.DOB_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.DOB_HYPERVERGE)));
			singleMember.put(WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.PLACEOFORIGIN_HYPERVERGE)));
			singleMember.put(WfImplConstants.ADDRESS_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.ADDRESS_HYPERVERGE)));
			singleMember.put(WfImplConstants.NATIONALITY_HYPERVERGE,
					Utils.getStringValue(variables.get(WfImplConstants.NATIONALITY_HYPERVERGE)));
			singleMember.put(WfImplConstants.APPLICANT_MOBILE_NO,
					Utils.getStringValue(variables.get(WfImplConstants.APPLICANT_MOBILE_NO)));
			singleMember.put(WfImplConstants.APPLICATION_MOBILE_NUMBER,
					Utils.getStringValue(variables.get(WfImplConstants.APPLICATION_MOBILE_NUMBER)));
			singleMember.put(WfImplConstants.DELIVERY_CITY,
					Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_CITY)));
			singleMember.put(WfImplConstants.DELIVERY_PROVINCE,
					Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_PROVINCE)));
			singleMember.put(WfImplConstants.DELIVERY_ADDRESS_LINE,
					Utils.getStringValue(variables.get(WfImplConstants.DELIVERY_ADDRESS_LINE)));
			singleMember.put(WfImplConstants.APPLICANT_EMAIL,
					Utils.getStringValue(variables.get(WfImplConstants.APPLICANT_EMAIL)));
			singleMember.put(WfImplConstants.HYPERVERGE_FRONT_RESPONSE,
					Utils.getStringValue(variables.get(WfImplConstants.HYPERVERGE_FRONT_RESPONSE)));
			singleMember.put(WfImplConstants.HYPERVERGE_BACK_RESPONSE,
					Utils.getStringValue(variables.get(WfImplConstants.HYPERVERGE_BACK_RESPONSE)));

			delegateExecution.setVariable(WfImplConstants.FAMILY_MEMBER_LIST, familyMemberListMap);

			logger.info("**************familyMemberListMap  inside setFamilyDataHyperverse: " + familyMemberListMap);
		} catch (Exception e) {
			logger.error("error in setFamilyDataHyperverse ", e);
		}
		return delegateExecution;
	}

	@SuppressWarnings("unchecked")
	public DelegateExecution setChildNidDetails(DelegateExecution delegateExecution) {
		logger.info("****** INSIDE setChildNidDetails*******");
		Map<String, Object> variables = delegateExecution.getVariables();
		logger.info("******setChildNidDetails variables******* : " + variables);
		try {
			Integer index = Utils.getIntegerValue(delegateExecution.getVariable(WfImplConstants.FAMILY_MEMBER_COUNT));
			List<Map<String, String>> familyMemberListMap = (List<Map<String, String>>) variables
					.get(WfImplConstants.FAMILY_MEMBER_LIST);
			Map<String, String> singleMember = familyMemberListMap.get(index);
			String userName = Utils.getStringValue(variables.get(WfImplConstants.Q1_CHILDREN));
			String isVietnamese = Utils.getStringValue(variables.get(WfImplConstants.Q2_CHILDERN));
			if (isVietnamese.equals(Constants.YES)) {
				delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE, WfImplConstants.VIETNAMESE);
			} else if (isVietnamese.equals("Có")) {
				delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE, WfImplConstants.VIETNAMESE_VI);
			} else {
				delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE, WfImplConstants.DASH);
			}
			delegateExecution.setVariable(WfImplConstants.FULLNAME_HYPERVERGE, userName);
			delegateExecution.setVariable(WfImplConstants.GENDER_HYPERVERGE,
					singleMember.get(WfImplConstants.HYPERVERGE_GENDER));
			delegateExecution.setVariable(WfImplConstants.DOB_HYPERVERGE,
					singleMember.get(WfImplConstants.HYPERVERGE_DOB));
			delegateExecution.setVariable(WfImplConstants.PLACEOFORIGIN_HYPERVERGE, WfImplConstants.DASH);
			delegateExecution.setVariable(WfImplConstants.ADDRESS_HYPERVERGE, WfImplConstants.DASH);
			delegateExecution.setVariable(WfImplConstants.USER_DOB, singleMember.get(Constants.DOB));
			delegateExecution.setVariable(WfImplConstants.USER_GENDER,
					singleMember.get(WfImplConstants.HYPERVERGE_GENDER));
			delegateExecution.setVariable(WfImplConstants.USER_FULL_NAME, "");
			delegateExecution.setVariable(WfImplConstants.APPLICANT_MOBILE_NO, "");
			delegateExecution.setVariable(WfImplConstants.APPLICANT_EMAIL, "");
			delegateExecution.setVariable(WfImplConstants.NATIONAL_ID_HYPERVERGE, "");

			logger.info("******setChildNidDetails variables after adding******* : " + variables);
		} catch (Exception e) {

			logger.error("error in setChildNidDetails ", e);
		}
		return delegateExecution;
	}

	@SuppressWarnings("unchecked")
	public DelegateExecution getMemberDetails(DelegateExecution delegateExecution) {
		logger.info("****** INSIDE getMemberDetails*******");
		Map<String, Object> variables = delegateExecution.getVariables();
		List<Map<String, String>> familyMemberList = (List<Map<String, String>>) variables
				.get(WfImplConstants.FAMILY_MEMBER_LIST);
		List<Map<String, String>> expandableHyperverge = new ArrayList<Map<String, String>>();
		try {
			logger.info("******setChildNidDetails variables******* : " + variables);

			Map<String, String> singleMember = familyMemberList.get(0);
			delegateExecution.setVariable(WfImplConstants.NATIONALITY_HYPERVERGE,
					singleMember.get(WfImplConstants.NATIONALITY_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.NATIONAL_ID_HYPERVERGE,
					singleMember.get(WfImplConstants.NATIONAL_ID_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.FULLNAME_HYPERVERGE,
					singleMember.get(WfImplConstants.FULLNAME_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.GENDER_HYPERVERGE,
					singleMember.get(WfImplConstants.GENDER_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.DOB_HYPERVERGE,
					singleMember.get(WfImplConstants.DOB_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.PLACEOFORIGIN_HYPERVERGE,
					singleMember.get(WfImplConstants.PLACEOFORIGIN_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.ADDRESS_HYPERVERGE,
					singleMember.get(WfImplConstants.ADDRESS_HYPERVERGE));
			delegateExecution.setVariable(WfImplConstants.APPLICATION_MOBILE_NUMBER,
					singleMember.get(WfImplConstants.APPLICATION_MOBILE_NUMBER));
			delegateExecution.setVariable(WfImplConstants.DELIVERY_CITY,
					singleMember.get(WfImplConstants.DELIVERY_CITY));
			delegateExecution.setVariable(WfImplConstants.DELIVERY_PROVINCE,
					singleMember.get(WfImplConstants.DELIVERY_PROVINCE));
			delegateExecution.setVariable(WfImplConstants.DELIVERY_ADDRESS_LINE,
					singleMember.get(WfImplConstants.DELIVERY_ADDRESS_LINE));
			delegateExecution.setVariable(WfImplConstants.APPLICANT_EMAIL,
					singleMember.get(WfImplConstants.APPLICANT_EMAIL));
			for (int i = 1; i < familyMemberList.size(); i++) {
				expandableHyperverge.add(familyMemberList.get(i));
			}
			delegateExecution.setVariable(WfImplConstants.EXPANDABLE_HYPERVERGE, expandableHyperverge);

			logger.info("******getMemberDetails variables after adding******* : " + variables);
		} catch (Exception e) {
			logger.error("error in getMemberDetails ", e);
		}
		return delegateExecution;

	}

	public String getpremium(String singleMemberAge, String planId) {
		logger.info("******inside getpremium*******" + singleMemberAge +" planID " + planId );
		String memberPremiumAmount = null;
		JSONArray premiumList = new JSONArray();
		JSONObject premiumMasterResponse;
		try {
			premiumMasterResponse = master.getDataFromService(WfImplConstants.BANCA_GET_FH_PREMIUM);
			if (premiumMasterResponse != null
					&& premiumMasterResponse.getInt(Constants.STATUS_MAP_KEY) == Constants.SUCCESS_CODE) {
				premiumList = master.parseResponseFromMaster(premiumMasterResponse);

				int age = Utils.getIntegerValue(singleMemberAge);

				for (int i = 0; i < premiumList.length(); i++) {
					JSONObject premium = premiumList.getJSONObject(i);

					if (age >= Utils.getIntegerValue(premium.getString(Constants.AGE_FROM))
							&& age <= Utils.getIntegerValue(premium.getString(Constants.AGE_TO))) {
						
						if (premium.getString(Constants.PLAN_ID_FAMILY).equals(planId)) {
							memberPremiumAmount = (Utils
									.getStringValue(premium.getString(Constants.PAYABLE_PREMIUM_AMOUNT)));
						}

					}
				}
			}

			logger.info("*****memberPremiumAmount*******  " + memberPremiumAmount);

		} catch (Exception e) {
			logger.error("can't find premium in get premium", e);
		}

		return memberPremiumAmount;

	}

}
