package com.kuliza.lending.wf_implementation.utils;

public class OTPConstants {

	// OTP Constants
	public static final String OTP_SKIP = "OTP Skip";
	public static final String OTP_GENERATION_TIME_KEY = "otpGenerationTime";
	public static final long ONE_MINUTE_TO_MILLIS = 60000;
	public static final String OTP_INVALID = "Invalid OTP for %s!";
	public static final String USER_BLOCKED = "Blocked for %s minutes.";
	public static final String OTP_VALIDATE_SKIP_SUCCESS = "Skipping OTP validation based on environment flag for : %s";
	public static final String OTP_VALIDATE_SUCCESS = "OTP is Valid";
	public static final String OTP_GENERATE_SKIP_SUCCESS = "Skipping OTP generation based on environment flag for : %s";
	public static final String OTP_TIME_DETAILS = "User Block Time: %s, User OTP Threshold Time: %s";

}
