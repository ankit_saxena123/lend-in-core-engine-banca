package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.pojo.CashPaymentSmsRequest;
import com.kuliza.lending.wf_implementation.services.CashPaymentService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping("payment")
public class CashPaymentController {

	private final Logger logger = LoggerFactory.getLogger(CashPaymentController.class);

	@Autowired
	private CashPaymentService cashService;

	@PostMapping("/cash")
	public ResponseEntity<?> cashPayment(Principal principal, @RequestParam String processInstanceId) {
		try {
			return CommonHelperFunctions.buildResponseEntity(cashService.cashPayment(principal.getName(), processInstanceId));

		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}
	}
	
	@PostMapping("/cashSms")
	public ResponseEntity<?> CashInitiateSms(@RequestBody CashPaymentSmsRequest cashSms) {
		try {
			logger.info("cash sms: "+  cashSms.getProcessInstanceId() + "crmID:  "+ cashSms.getCrmId());
			return CommonHelperFunctions.buildResponseEntity(cashService.sendSms(cashSms.getCrmId(),cashSms.getProcessInstanceId(),Constants.SMS_CASH_PAY_INITIATE,false));

		} catch (Exception e) {
			logger.info(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}
	}

}
