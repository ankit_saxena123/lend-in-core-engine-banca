package com.kuliza.lending.wf_implementation.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "shield_crm_id_info")
public class ShieldCrmIdInfoModel extends BaseModel {
	
	public ShieldCrmIdInfoModel(String sid, long crmId) {
		super();
		this.crmId = crmId;
	}
	
	public ShieldCrmIdInfoModel() {
		super();
	}

	@Column(nullable = false, name = "crm_id")
	private long crmId;

	

	public long getCrmId() {
		return crmId;
	}

	public void setCrmId(long crmId) {
		this.crmId = crmId;
	}
	
}
