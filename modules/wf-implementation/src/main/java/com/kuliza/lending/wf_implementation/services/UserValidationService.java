package com.kuliza.lending.wf_implementation.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.utils.AuthUtils;
import com.kuliza.lending.wf_implementation.base.BaseUserValidationService;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WfUserDeviceRegDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WfUserDeviceRegister;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.notification.SNSEventSubscription;
import com.kuliza.lending.wf_implementation.notification.SNSParamInit;
import com.kuliza.lending.wf_implementation.notification.SNSRegisterPhone;
import com.kuliza.lending.wf_implementation.pojo.UserMPINInputForm;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class UserValidationService extends BaseUserValidationService {

	@Autowired
	private WorkflowUserDao workflowUserDao;

	@Autowired
	private WfUserDeviceRegDao wfUserDeviceRegDao;

	@Autowired
	private SNSRegisterPhone snsRegisterPhone;

	@Autowired
	private SNSEventSubscription snsSubscription;

	@Autowired
	private UserTopicsDao userTopicsDao;

	@Autowired
	public SNSParamInit snsParamInit;

	private static final Logger logger = LoggerFactory.getLogger(UserValidationService.class);

	/**
	 * @param userForm
	 * @return
	 * @throws Exception Service to generate password
	 */
	@Override
	public ApiResponse generateUserPassword(HttpServletRequest request, UserPasswordGenerationForm userForm)
			throws Exception {
		return super.generateUserPassword(request, userForm);
	}

	@Override
	protected ApiResponse sendOTP(String mobileNumber, String otp) {
		return null;
	}

	/**
	 * @param userForm
	 * @return Utility to create and send password
	 */
	public ApiResponse generateAndSendPassword(HttpServletRequest request, UserPasswordGenerationForm userForm) {
		return null;
		// Here, you will need to write your own otp sending code
	}

	/**
	 * @param validationForm
	 * @return apiResponse
	 * @throws Exception Service to validate and register user
	 */
	@Override
	public ApiResponse validateAndRegisterUser(HttpServletRequest request, UserValidationinputForm validationForm)
			throws Exception {
		return super.validateAndRegisterUser(request, validationForm);

	}

	/**
	 * @param validationForm
	 * @return Function to validate user
	 */
	public Boolean validateUser(HttpServletRequest request, UserValidationinputForm validationForm) {
		// TODO Write validation logic here
		return true;
	}

	/**
	 * Service to Set MPIN.
	 * 
	 * @param username
	 * @param mpinForm
	 * @return ApiResponse
	 */
	public ApiResponse setMpin(String username, UserMPINInputForm mpinForm) {
		logger.info("SetMpin-->mpinForm--> :" + mpinForm);
		WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(username, false);
		if (wfUser == null) {
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE, WfImplConstants.USER_NOT_FOUND);
		}
		List<WfUserDeviceRegister> wfUserDeviceObjs = null;
		WfUserDeviceRegister wfUserDeviceObj = null;
		wfUserDeviceObjs = wfUserDeviceRegDao.findByWfUserAndIsDeleted(wfUser, false);
		if (wfUserDeviceObjs != null && !wfUserDeviceObjs.isEmpty()) {
			wfUserDeviceObj = wfUserDeviceObjs.get(0);
		} else {
			wfUserDeviceObj = new WfUserDeviceRegister(wfUser, mpinForm.getDeviceType(), mpinForm.getDeviceId());
		}
		wfUserDeviceObj.setMpin(AuthUtils.textEncoding(mpinForm.getMpin()));
		wfUserDeviceObj.setDeviceId((mpinForm.getDeviceId()));
		setDeviceInfoToWfDeviceRegister(wfUserDeviceObj, mpinForm.getDeviceInfo(), mpinForm.getAppsFlyerData());
		wfUserDeviceObj.setDeviceType((mpinForm.getDeviceType()));
		setDeviceInfoToWfDeviceRegister(wfUserDeviceObj, mpinForm.getDeviceInfo(), mpinForm.getAppsFlyerData());
		String arnEndpoint = snsRegisterPhone.regDevice(mpinForm.getDeviceId(), mpinForm.getDeviceType());
		wfUser.setArnEndPoint(arnEndpoint);
		wfUserDeviceRegDao.save(wfUserDeviceObj);
		workflowUserDao.save(wfUser);
		try {
			String subscriberArn = snsSubscription.subscribeToTopic(snsParamInit.getRegTopicARN(), "application",
					arnEndpoint);
			logger.info("<==Subscription To Registration Topic ==>" + arnEndpoint);
			if (subscriberArn!=null) {
				UserTopics userTopic = new UserTopics();
				userTopic.setTopic(WfImplConstants.REG_TOPIC);
				logger.info("<==Subscription To Registration user topic==>" + WfImplConstants.REG_TOPIC);
				userTopic.setUserName(wfUser.getIdmUserName());
				logger.info("<==Subscription To Registration user topic==>" + wfUser.getIdmUserName());
				userTopic.setSubscriberArn(subscriberArn);
				userTopic.setWfUser(wfUser);
				userTopicsDao.save(userTopic);
				logger.info("<==Subscription To Registration user topic is added==>");

			} else {
				logger.info("<==Subscription To Registration Topic is Failed For User==>" + wfUser.getIdmUserName());
			}
		} catch (Exception ex) {
			logger.info("<==Subscription To Registration Topic is Failed For User==>" + wfUser.getIdmUserName());

		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, WfImplConstants.MPIN_SET_SUCCESS_MESSAGE);
	}

	private void setDeviceInfoToWfDeviceRegister(WfUserDeviceRegister wfUserDeviceObj, String deviceInfo,
			String appsFlyerData) {
		logger.info("deviceInfo-->: " + deviceInfo + "<--appsFlyerData-->: " + appsFlyerData);
		if (deviceInfo != null && appsFlyerData != null) {
			JsonObject deviceInfoAsJson = new JsonParser().parse(deviceInfo).getAsJsonObject();
			logger.info("deviceInfoAsJson-->: "+deviceInfoAsJson.toString());
			wfUserDeviceObj.setDeviceInfoDetails(deviceInfo);
			wfUserDeviceObj.setAppsFlyerData(appsFlyerData);
			wfUserDeviceObj.setAdvertisingId(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_ADVERTISING_ID_FOR_ANDROID)));
			wfUserDeviceObj.setIdfa(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_IDFA_FOR_IOS)));
			wfUserDeviceObj.setPhoneNumber(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_PHONE_NUMBER)));
			wfUserDeviceObj.setUniqueId(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_UNIQUE_ID)));
			wfUserDeviceObj.setAppsFlyerId(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_APPSFLYER_ID)));
			wfUserDeviceObj.setMacAddress(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_MAC_ADDRESS)));
			wfUserDeviceObj.setIp((Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_IP_ADDRESS))));
			// wfUserDeviceObj.setDeviceId(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_DEVICE_ID)));
			wfUserDeviceObj.setBrandName(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_BRAND)));
			wfUserDeviceObj.setCarrier(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_CARRIER)));
			wfUserDeviceObj.setAppbundleId(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_BUNDLE_ID)));
			wfUserDeviceObj.setAppbuildNumber(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_BUILD_NUMBER)));
			wfUserDeviceObj.setModel(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_MODEL)));
			wfUserDeviceObj.setVersion(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_VERSION)));
			wfUserDeviceObj.setManufacturer(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_MANUFACTURER)));
			wfUserDeviceObj
					.setApplicationName(Utils.getStringValue(deviceInfoAsJson.get(Constants.GET_APPLICATION_NAME)));
		}
	}

	public ApiResponse validateMpin(String username, UserMPINInputForm mpinForm) {
		logger.info("<==============> username <===========>" + username);
		WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(username, false);
		logger.info("<==============> validateMpin <===========>");
		if (wfUser == null) {
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE, WfImplConstants.USER_NOT_FOUND);
		}
		List<WfUserDeviceRegister> wfUserDeviceObjs = null;
		WfUserDeviceRegister wfUserDeviceObj = null;
		wfUserDeviceObjs = wfUserDeviceRegDao.findByWfUserAndDeviceIdAndDeviceTypeAndIsDeletedAndMpinNotNull(wfUser,
				mpinForm.getDeviceId(), mpinForm.getDeviceType(), false);
		if (wfUserDeviceObjs != null && !wfUserDeviceObjs.isEmpty()) {
			wfUserDeviceObj = wfUserDeviceObjs.get(0);
			if (wfUserDeviceObj.getMpin().equals(AuthUtils.textEncoding(mpinForm.getMpin()))) {
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						WfImplConstants.MPIN_VALIDATION_SUCCESS_MESSAGE);
			}
		} else {
			logger.info("<==============> Failure Response <===========>");
			return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
					WfImplConstants.USER_DEVICE_NOT_REGISTERED);
		}
		return new ApiResponse(HttpStatus.NOT_FOUND, Constants.FAILURE_MESSAGE,
				WfImplConstants.INVALID_MPIN_ERROR_MESSAGE);
	}
}