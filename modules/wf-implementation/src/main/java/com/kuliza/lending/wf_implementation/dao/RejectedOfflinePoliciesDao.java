package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;

import com.kuliza.lending.wf_implementation.models.RejectedOfflinePolicies;


public interface RejectedOfflinePoliciesDao extends CrudRepository<RejectedOfflinePolicies, Long> {

}
