package com.kuliza.lending.wf_implementation.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.wf_implementation.notification.SNSConfiguration.Platform;

@Service
public class SNSRegisterPhone extends SNSClient{
   
	CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
	
	private static final Logger logger = LoggerFactory
			.getLogger(SNSRegisterPhone.class);
	
	
	public String regDevice(String deviceId, DeviceType deviceType) {
		
		try{
		
			AmazonSNSClient snsClient = getSNSClient();
	
			CreatePlatformEndpointResult createPlatformEndpoint = null;
		
			if (DeviceType.android ==deviceType||DeviceType.ios==deviceType) 
				createPlatformEndpoint = regDeviceToPlatformEndpoint(snsClient, Platform.GCM, deviceId, SNSConfiguration.AWS_BANCA_PLATFORM_APPLICATION_ARN);
			else 
				createPlatformEndpoint = regDeviceToPlatformEndpoint(snsClient, Platform.APNS, deviceId, SNSConfiguration.AWS_BANCA_PLATFORM_IOS_APPLICATION_ARN);
			
			return createPlatformEndpoint.getEndpointArn();
		
		}catch(Exception ex){
			logger.error("exception occured while register device in sns" + ex);
			return null;
		}
		
	}
	
	public CreatePlatformEndpointResult regDeviceToPlatformEndpoint(
			AmazonSNSClient snsClient,Platform platform, String deviceId,
			String applicationArn) {
		
		try {
		
			platformEndpointRequest.setCustomUserData("user");
			platformEndpointRequest.setToken(deviceId);
			platformEndpointRequest.setPlatformApplicationArn(applicationArn);
			
			return snsClient.createPlatformEndpoint(platformEndpointRequest);
		
		}catch(Exception ex){
			
			logger.error("exception occured while register device to platform endpoint" + ex);
			return null;
		}
	}
	
}

