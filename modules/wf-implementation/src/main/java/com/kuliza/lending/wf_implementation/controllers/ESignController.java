package com.kuliza.lending.wf_implementation.controllers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.wf_implementation.esign_services.IESignService;
import com.kuliza.lending.wf_implementation.pojo.ESignRequest;

@RestController
@RequestMapping(value = "/e-sign")
public class ESignController {

	@Autowired
	private IESignService eSignService;

	private static final Logger logger = LoggerFactory.getLogger(ESignController.class);

	@RequestMapping(value = "/init")
	public Map<String, Object> initESign(@RequestBody ESignRequest eSignRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		logger.info("-------------- Init E-Sign called for ProcInstId : "+eSignRequest.getProcInstId());
		logger.info("ESignRequest : " + mapper.writeValueAsString(eSignRequest));
		validateESignRequest(eSignRequest);

		return eSignService.initESignService(eSignRequest);
	}
	@RequestMapping(value = "/test")
	public Map<String, Object> htmlToPdf(@RequestBody ESignRequest eSignRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		logger.info("-------------- Init E-Sign called for ProcInstId : "+eSignRequest.getProcInstId());
		logger.info("ESignRequest : " + mapper.writeValueAsString(eSignRequest));
		validateESignRequest(eSignRequest);

		eSignService.htmlToPdf(eSignRequest.getAuthorizeCode());
		return null;
	}

	@RequestMapping(value = "/prepare-file")
	public Map<String, Object> prepareESignFiles(@RequestBody ESignRequest eSignRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		logger.info("-------------- prepare-file E-Sign called for ProcInstId : "+eSignRequest.getProcInstId());
		logger.info("ESignRequest : " + mapper.writeValueAsString(eSignRequest));
		validateESignRequest(eSignRequest);
		return eSignService.prepareESignFiles(eSignRequest);
	}

	@RequestMapping(value = "/authorize")
	public Map<String, Object> authorizeESign(@RequestBody ESignRequest eSignRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		logger.info("-------------- authorize E-Sign called  for ProcInstId : "+eSignRequest.getProcInstId());
		logger.info("ESignRequest : " + mapper.writeValueAsString(eSignRequest));
		validateESignRequest(eSignRequest);
		return eSignService.authorizeESign(eSignRequest);
	}

	@RequestMapping(value = "/regenerate-otp")
	public Map<String, Object> regenerateESignOTP(@RequestBody ESignRequest eSignRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		logger.info("-------------- regenerate-otp E-Sign called  for ProcInstId : "+eSignRequest.getProcInstId());
		logger.info("ESignRequest : " + mapper.writeValueAsString(eSignRequest));
		validateESignRequest(eSignRequest);
		return eSignService.regenerateESignOTP(eSignRequest);
	}

	@RequestMapping(value = "/download")
	public Map<String, Object> downLoadESignDoc(@RequestBody ESignRequest eSignRequest) throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		logger.info("-------------- download doc E-Sign called for ProcInstId : "+eSignRequest.getProcInstId());
		logger.info("ESignRequest : " + mapper.writeValueAsString(eSignRequest));
		validateESignRequest(eSignRequest);
		return eSignService.downLoadESignDoc(eSignRequest);
	}

	private void validateESignRequest(ESignRequest eSignRequest) throws Exception {

		if (eSignRequest == null) {
			throw new Exception("invalid ESign request");
		}

		if (eSignRequest.getProcInstId() == null || eSignRequest.getProcInstId().isEmpty()) {
			throw new Exception("invalid Process Instance Id");
		}

	}

}
