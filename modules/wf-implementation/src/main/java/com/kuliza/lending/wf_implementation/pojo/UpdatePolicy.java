package com.kuliza.lending.wf_implementation.pojo;

import java.util.List;
import java.util.Map;

public class UpdatePolicy {
	
	private List<Map<String,String>> policyDetails;

	public List<Map<String, String>> getPolicyDetails() {
		return policyDetails;
	}

	public void setPolicyDetails(List<Map<String, String>> policyDetails) {
		this.policyDetails = policyDetails;
	}
	

}
