package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.VtigerGenericDropOffModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service("fecVtigerGenericDropOffIntegration")
public class FECVtigerGenericDropOffIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;
	@Autowired
	private WorkflowUserDao workflowUserDao;

	private static final Logger logger = LoggerFactory.getLogger(FECVtigerGenericDropOffIntegration.class);

	public JSONObject buildRequestPayLoad(VtigerGenericDropOffModel dropoff, boolean activeStatus,
			String lastDropOffStage) throws Exception {
		WorkFlowUser workFlowUser = workflowUserDao.findByIdmUserNameAndIsDeleted(dropoff.getUserName(), false);
		JSONObject requestPayload = new JSONObject();
		JSONObject requestBody = new JSONObject();
		String dob = null;
		requestBody.put("shi:SendDropOff.Sys.TransID", Utils.generateUUIDTransanctionId());
		requestBody.put("shi:SendDropOff.Sys.DateTime", Utils.getCurrentDateTime());
		requestBody.put("shi:SendDropOff.Sys.RequestorID", Constants.FEC_VTIGER_REQUESTOR_ID);
		if (workFlowUser != null) {
			requestBody.put("shi:SendDropOff.Info.PHONENUMBER", workFlowUser.getMobileNumber());
		}
		if (dropoff.getvTigerContactId() != null) {
			requestBody.put("shi:SendDropOff.Info.VTG_CONTACTID", dropoff.getvTigerContactId());
		}
		if (dropoff.getvTigerOpportunityId() != null) {
			requestBody.put("shi:SendDropOff.Info.VTG_CRMID", dropoff.getvTigerOpportunityId());
			requestBody.put("shi:SendDropOff.Info.VTG_PARENT_CRMID", dropoff.getvTigerOpportunityId());
		}
		if (dropoff.getDropOffType() != null) {
			requestBody.put("shi:SendDropOff.Info.DROP_OFF_TYPE", dropoff.getDropOffType());
		}
		requestBody.put("shi:SendDropOff.Info.ACTIVE_STATUS", activeStatus);
		if (workFlowUser != null) {
			requestBody.put("shi:SendDropOff.Info.FULLNAME", workFlowUser.getUsername());
		}
		if (workFlowUser != null) {
			requestBody.put("shi:SendDropOff.Info.GENDER", workFlowUser.getGender());
		}
		if (workFlowUser != null) {
			dob = workFlowUser.getDob();
			logger.info("WorkFlow User Dob================>" + dob);
		}
		logger.info("DOB--> :" + dob);
		if (dob.contains("/")) {
			requestBody.put("shi:SendDropOff.Info.DOB",
					Utils.formatDate(dob, Constants.DATE_FORMAT_SLASH, Constants.DATE_FORMAT_DASH_YEAR));
		} else {
			requestBody.put("shi:SendDropOff.Info.DOB",
					Utils.formatDate(dob, Constants.DATE_FORMAT_DASH, Constants.DATE_FORMAT_DASH_YEAR));
		}
		if (lastDropOffStage != null) {
			requestBody.put("shi:SendDropOff.Info.LAST_DROP_OFF_STAGE", lastDropOffStage);
		}
		if (dropoff.getCurrentJourneyStage() != null) {
			requestBody.put("shi:SendDropOff.Info.CURRENT_JOURNEY_STAGE", dropoff.getCurrentJourneyStage());
		}
		requestBody.put("shi:SendDropOff.Info.CAMPAIGN", "Shield Drop-off");
		requestPayload.put("body", requestBody);
		return requestPayload;
	}

	public JSONObject getDataFromService(JSONObject requestPayload) {

		logger.info("FECVtigerGenericDropOff-->getDataFromService-->:");

		logger.info("requestPayload--->: " + requestPayload);
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();

			headersMap.put("Content-Type", Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.VTIGER_SOAP_SLUG_KEY
					+ Constants.VTIGER_IB_KEY + "/" + Constants.VTIGER_SEND_DROP_OFF_KEY + "/"
					+ wfImplConfig.getIbCompany() + "/" + Constants.VTIGER_HASH + "/?env=" + wfImplConfig.getIbEnv()
					+ Constants.VTIGER_INTEGRATION_SOAP_KEY;
			logger.info("HashGenerationUrl--->: " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call vitger api :" + e.getMessage(), e);
		}
		logger.info("FECVtigerGenericDropOff<--getDataFromService()");
		return responseJson;
	}

	public JSONObject getDataFromServiceForCreate(JSONObject requestPayload) {

		logger.info("FECVtigerGenericDropOff-->getDataFromService");

		logger.info("requestPayload--->: " + requestPayload);
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();

			headersMap.put("Content-Type", Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.VTIGER_SOAP_SLUG_KEY
					+ Constants.VTIGER_IB_KEY + "/" + Constants.VTIGER_CREATE_DROP_OFF_KEY + "/"
					+ wfImplConfig.getIbCompany() + "/" + Constants.VTIGER_HASH + "/?env=" + wfImplConfig.getIbEnv()
					+ Constants.VTIGER_INTEGRATION_SOAP_KEY;
			logger.info("HashGenerationUrl--->: " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call vitger api :" + e.getMessage(), e);
		}
		logger.info("FECVtigerGenericDropOff<--getDataFromServiceForCreate()");
		return responseJson;
	}

	public Map<String, Object> parseResponse(JSONObject response) throws Exception {
		Map<String, Object> parseResponseMap = new HashMap<String, Object>();
		parseResponseMap.put("status", false);
		if (response.has("NS1:SendDropOffResponse")) {
			JSONObject sendDropOffJson = response.getJSONObject("NS1:SendDropOffResponse");
			if (sendDropOffJson.has("Details")) {
				JSONObject detailsJson = sendDropOffJson.getJSONObject("Details");
				if (detailsJson.has("VTG_CONTACTID")) {
					String contactId = detailsJson.getString("VTG_CONTACTID");
					String vtigerCrmId = detailsJson.getString("VTG_CRMID");
					parseResponseMap.put("contactId", contactId);
					parseResponseMap.put("vtigerCrmId", vtigerCrmId);
					parseResponseMap.put("status", true);

				}
			}

		}
		return parseResponseMap;
	}
}
