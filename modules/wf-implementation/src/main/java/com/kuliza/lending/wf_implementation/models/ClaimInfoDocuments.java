/**
 * 
 */
package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

/**
 * @author Garun Mishra
 *
 */
@Entity
@Table(name = "claim_info_documents")
public class ClaimInfoDocuments extends BaseModel {

	@Column(nullable = true, name = "process_instance_id")
	private String processInstanceId;

	@Column(nullable = true, name = "claim_number")
	private String claimsid;

	@Column(nullable = true, name = "document_id")
	private String documentId;

	@Column(nullable = true, name = "document_link")
	private String DocumentLink;
	
	@Column(nullable = true, name = "claim_document_name")
	private String DocumentName;
	

	@Column(nullable = false, name = "document_status")
	private String documentStatus;

	@Column(nullable = true, name = "claim_docs_description")
	private String claimDocumentDescription;

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getClaimsid() {
		return claimsid;
	}

	public void setClaimsid(String claimsid) {
		this.claimsid = claimsid;
	}
	
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


	
	public String getClaimDocumentDescription() {
		return claimDocumentDescription;
	}

	public void setClaimDocumentDescription(String claimDocumentDescription) {
		this.claimDocumentDescription = claimDocumentDescription;
	}

	public String getDocumentLink() {
		return DocumentLink;
	}

	public void setDocumentLink(String documentLink) {
		DocumentLink = documentLink;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getDocumentName() {
		return DocumentName;
	}

	public void setDocumentName(String documentName) {
		DocumentName = documentName;
	}
	
	
	
}
