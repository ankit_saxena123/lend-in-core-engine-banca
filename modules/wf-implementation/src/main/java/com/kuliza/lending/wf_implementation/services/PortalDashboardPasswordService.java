/**
 * 
 */
package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.flowable.engine.history.HistoricProcessInstance;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakConfig;
import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.config.keycloak.OpenIdService;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.PortalDashboardAuthenticationConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;

/**
 * @author Garun Mishra
 * @Description This file is to do required operations on password
 *              authentication.
 * @Date 2020-05-06
 *
 */
@Service
@EnableJms
@EnableConfigurationProperties({ KeyCloakConfig.class })
public class PortalDashboardPasswordService {

	private static final Logger logger = LoggerFactory.getLogger(PortalDashboardPasswordService.class);

	@Autowired
	KeyCloakManager keyCloakManager;

	@Autowired
	KeyCloakService keyCloakService;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private JavaMailSenderImpl javaMailSenderImpl;

	@Autowired
	private WfImplConfig wfImplConfig;

	private KeyCloakConfig keyCloakConfig;
	private Keycloak keycloak;
	private OpenIdService openIdService;
	private ClientRepresentation clientRepresentation;
	private RealmResource realmResource;
	private GroupsResource groupsResource;
	private ClientsResource clientsResource;
	private UsersResource usersResource;

	// Initialize the keyclock config
	public PortalDashboardPasswordService(KeyCloakConfig keyCloakConfig) {
		this.keyCloakConfig = keyCloakConfig;
		this.buildKeyCloak();
		this.buildOpenIdService();
		this.buildResources();
		this.buildClientRepresentation();
	}

	private void buildKeyCloak() {
		this.keycloak = KeycloakBuilder.builder().serverUrl(keyCloakConfig.getAuthEndpoint())
				.realm(keyCloakConfig.getRealm()).grantType(OAuth2Constants.PASSWORD)
				.clientId(keyCloakConfig.getClientId()).clientSecret(keyCloakConfig.getClientSecret())
				.username(keyCloakConfig.getAdminUsername()).password(keyCloakConfig.getAdminPassword()).build();
	}

	private void buildOpenIdService() {
		this.openIdService = this.keycloak.proxy(OpenIdService.class,
				URI.create(this.keyCloakConfig.getAuthEndpoint()));
	}

	private void buildResources() {
		this.realmResource = this.keycloak.realm(this.keyCloakConfig.getRealm());
		this.groupsResource = this.realmResource.groups();
		this.clientsResource = this.realmResource.clients();
		this.usersResource = this.realmResource.users();
	}

	private void buildClientRepresentation() {
		this.clientRepresentation = this.clientsResource.findByClientId(this.keyCloakConfig.getClientId()).get(0);
	}

	public ApiResponse updatePassword(String email, String current_password, String new_password) {
		logger.info("--> Entering changePassword()");
		try {

			List<UserRepresentation> users = usersResource.search(email);
			if (users != null && users.size() > 0) {
				for (UserRepresentation user : users) {
					CredentialRepresentation credentialRepresentation = this.createCredentials(new_password);
					usersResource.get(user.getId()).resetPassword(credentialRepresentation);
				}
			} else {
				return new ApiResponse(HttpStatus.BAD_REQUEST, "BAD_REQUEST",
						PortalDashboardAuthenticationConstants.PASSWORD_CHANGED_USER_NOT_EXIST);
			}
		} catch (Exception ex) {
			String errorMessage = "Error while changing the password for user  " + email;
			logger.error(errorMessage, ex);
			return new ApiResponse(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED",
					PortalDashboardAuthenticationConstants.PASSWORD_CHANGED_FAILED_MESSAGE);
		}
		logger.info("<-- Exiting changePassword()");
		return new ApiResponse(HttpStatus.OK, "OK",
				PortalDashboardAuthenticationConstants.PASSWORD_SUCCESSFULLY_CHANGED_MESSAGE);

	}

	private CredentialRepresentation createCredentials(String password) {
		CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
		credentialRepresentation.setTemporary(false);
		credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
		credentialRepresentation.setValue(password);
		return credentialRepresentation;
	}

	/**
	 * @Descritpion send a link to mentioned mail to reset password
	 * @param email
	 * @return SEND_EMAIL_NOTIFICTION_TO_RESET_PASSWORD
	 */
	public ApiResponse forgetPasswordEmailNotification(String email, String emailSession)
			throws IOException, MailAuthenticationException {
		logger.info("--> Entering forgetPasswordEmailNotification()");
		try {
			String language = "en-US";
			logger.info("-->Entering PortalDashboardPasswordService.sendEmail()");
			Properties prop = new Properties();
			prop.put(Constants.MAIL_SMTP_HOST_KEY, wfImplConfig.getSmtpHostProperties());
			prop.put(Constants.MAIL_SMTP_PORT_KEY, wfImplConfig.getSmtpPortProperties());
			prop.put(Constants.MAIL_SMTP_AUTH_KEY, wfImplConfig.getSmtpAuthProperties());
			prop.put(Constants.MAIL_SMTP_STARTTLS_KEY, wfImplConfig.getSmtpStarttlsProperties());
			Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					logger.info("-->PortalDashboardPasswordService.sendEmail()==>> Authenication Credentials \n ");
					logger.info("-->Username == >>  "+wfImplConfig.getSmtpUserNameProperties());
					logger.info("-->Password ===>>  "+wfImplConfig.getSmtpPasswordProperties());
					return new PasswordAuthentication(wfImplConfig.getSmtpUserNameProperties(),
							wfImplConfig.getSmtpPasswordProperties());
				}
			});
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(wfImplConfig.getSmtpUserNameProperties()));
			message.setSubject(createEmailSubject(language));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setRecipients(Message.RecipientType.CC,
					InternetAddress.parse(PortalDashboardAuthenticationConstants.EMAIL_CC));
//				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(PortalDashboardAuthenticationConstants.EMAIL_BCC));
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDescription(createEmailSubject(language));
			String bodyContent =  createEmailBody(email, language, emailSession);
			  if(!bodyContent.equals("user_not_exist")) {
					messageBodyPart.setContent(bodyContent,	Constants.EMAIL_BODY_CONTENT_TYPE);
			  }else { 	
					return new ApiResponse(HttpStatus.BAD_REQUEST, "BAD_REQUEST",
							PortalDashboardAuthenticationConstants.PASSWORD_CHANGED_USER_NOT_EXIST);
				}
				
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			logger.info("<=========>Mail sent SuccessFully<=========>");
		} catch (Exception e) {
			logger.error("Error While Calling sendMail : " + e.getMessage(), e);

			return new ApiResponse(HttpStatus.UNAUTHORIZED, "User Doesn't exist..!!",
					PortalDashboardAuthenticationConstants.SEND_EMAIL_NOTIFICTION_TO_RESET_PASSWORD_FAIL);
		}
		logger.info("--> Exiting forgetPasswordEmailNotification()");
		return new ApiResponse(HttpStatus.OK, "OK",
				PortalDashboardAuthenticationConstants.SEND_EMAIL_NOTIFICTION_TO_RESET_PASSWORD_SUCCESS);
	}

	private String createEmailSubject(String language) {
		logger.info("-->Entering PortalDashboardPasswordService.createEmailSubject()");
		String subjectContent = null;
		if (language.equalsIgnoreCase("en-US")) {
			subjectContent = PortalDashboardAuthenticationConstants.EMAIL_SUBJECT_ENGLISH;
		} else {
			subjectContent = PortalDashboardAuthenticationConstants.EMAIL_SUBJECT_VITENAM;
		}
		logger.info("<=========>EmailSubject<=========> ::" + subjectContent);
		logger.info("<--Exiting PortalDashboardPasswordService.createEmailSubject()()");
		return subjectContent;
	}

	private String createEmailBody(String userEmail, String language, String session) {
		logger.info("-->Entering PortalDashboardPasswordService.createEmailBody()");
		List<UserRepresentation> users = usersResource.search(userEmail);
		String customerName = null;
		if (users.size() > 0) {
			for (UserRepresentation user : users) {
				if (user.getFirstName().toString() != null && user.getLastName().toString() != null) {
					customerName = user.getFirstName().toString() + " " + user.getLastName().toString();
				} else {
					customerName = user.getUsername();
				}
			}
		} else {
			return "user_not_exist";
		}

		String userNameWelcome = "";
		String resetBodyMessage = "";
		String signatureLine = "";

		if (language.equalsIgnoreCase("en-US")) {
			userNameWelcome = PortalDashboardAuthenticationConstants.PASSWORD_RESET_CUSTOMER_NAME;
			resetBodyMessage = PortalDashboardAuthenticationConstants.PASSWORD_RESET_BODY_MESSAGE_IN_EN;
			signatureLine = PortalDashboardAuthenticationConstants.PASSWORD_RESET_BODY_MESSAGE_THANKS;
		} else {
			userNameWelcome = PortalDashboardAuthenticationConstants.PASSWORD_RESET_CUSTOMER_NAME;
			resetBodyMessage = PortalDashboardAuthenticationConstants.PASSWORD_RESET_BODY_MESSAGE_IN_EN;
			signatureLine = PortalDashboardAuthenticationConstants.PASSWORD_RESET_BODY_MESSAGE_THANKS;
		}
		userNameWelcome = userNameWelcome.replace("${customer_name}", customerName);
		resetBodyMessage = resetBodyMessage.replace("${password_reset_link}", createResetLink(userEmail, session));
		String emailBodyContent = userNameWelcome + resetBodyMessage + signatureLine;
		logger.info("<--Exiting FECreditBancaNotifyUserServices.createEmailBody()");
		return emailBodyContent;
	}

	private String createResetLink(String userEmail, String session) {
		String url = PortalDashboardAuthenticationConstants.PASSWORD_RESET_LINK + userEmail + "/" + session;
		String PASSWORD_RESET_LINK = "<a href=" + url
				+ "><button style=\"background-color:#88c999;color: Black;padding: 12px 40px;font-size: 16px;\">Reset Password</button></a>";
		return PASSWORD_RESET_LINK;
	}

}
