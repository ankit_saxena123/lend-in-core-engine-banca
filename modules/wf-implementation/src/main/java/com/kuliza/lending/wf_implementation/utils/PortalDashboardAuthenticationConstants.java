
/**
 * 
 */
package com.kuliza.lending.wf_implementation.utils;

/**
 * @author garun
 *
 */
public class PortalDashboardAuthenticationConstants {

	//claims dashboard
	public static final String CLAIM_ID = "claim_id";
	public static final String CLAIM_APPLICATION_NO = "application_number";
	public static final String CLAIM_LAST_UPDATE = "claim_last_update";
	public static final String CLAIM_AMOUNT = "claim_amount";
	public static final String CLAIM_STATUS = "claim_status";
	public static final String CLAIM_SEARCH_BY_CLAIM_ID = "search_by_claim_id";
	public static final String CLAIM_SEARCH_BY_POLICY_NO = "search_by_claim_policy_number";
	public static final String CLAIM_SEARCH_BY_PLAN_NAME = "search_by_claim_plan_name";
	public static final String CLAIM_SEARCH_BY_PRODUCT_NAME = "search_by_claim_product_name";
	public static final String CLAIM_LAST_ACTIVE_ON = "claim_last_active_on";
	public static final String CLAIM_POLICY_DOCUMENT_LINK = "claim_policy_document_link";
	public static final String CLAIM_POLICY_COVERAGE_AMOUNT = "claim_policy_coverage_amount";
	public static final String CLAIM_SUB_DASHBOARD_CUSTOMER = "claim_detail_customer_info";
	public static final String CLAIM_SUB_DASHBOARD_POLICY = "claim_detail_policy_info";
	public static final String CLAIM_SUB_DASHBOARD_CLAIM_INFO = "claim_detail_claim_info";
	public static final String CLAIM_SUB_DASHBOARD_CLAIM_DOCS = "claim_detail_claim_document";
	public static final String CLAIM_SUB_DASHBOARD_CLAIM_STATUS = "claim_detail_claim_status";
	public static final String GROUP_ID_EXAMPLE = "a2db397c-3e93-4681-be30-69291d1e9315";
	public static final String PASSWORD_SUCCESSFULLY_CHANGED_MESSAGE = "Password Changed";
	public static final String PASSWORD_CHANGED_FAILED_MESSAGE = "Password Change Failed";
	public static final String PASSWORD_CHANGED_USER_NOT_EXIST = "User Doesn't exist.";
	public static final String CLAIM_DOCUMENT_CATEGORY = "claim_document_category";
	public static final String CLAIM_QUESTION = "claim_question";
	public static final String CLAIM_ANSWER = "claim_answer";
	public static final String CLAIM_DOC_ID = "claim_doc_id";
	public static final String CLAIM_DOC_DESC = "claim_doc_description";
	public static final String CLAIM_DOCUMENTS = "claim_documents";
	public static final String CLAIM_DOC_TYPE = "claim_doc_type";
	public static final String CLAIM_DOC_LINK = "claim_doc_link";
	public static final String CLAIM_DOC_STATUS = "claim_doc_status"; 
	public static final String CLAIM_DOC_STATUS_SUCCESS_MESSAGE = "Document has been successfully approved.";
	public static final String CLAIM_DOC_STATUS_FAIL_MESSAGE = "Document approval failed as document id is not exist";
	public static final String CLAIM_PROCESS_INSTANCE_ID = "process_instance_id";
	public static final String CLAIM_SEARCH_BY_CLAIM_APPLICATION_ID = "search_by_claim_id";
	//variable Declerataion 
	
	public static final String CLAIM_VAR_POLICY_NUMBER = "policyNumber";
	public static final String CLAIM_VAR_APPLICATION_NUMBER = "applicationNumber";
	public static final String CLAIM_VAR_PRODUCT_NAME = "product_name";
	public static final String CLAIM_VAR_PLAN_NAME = "plan_type";
	public static final String CLAIM_VAR_PROTECTION_AMOUNT = "protectionAmount";
	public static final String CLAIM_VAR_ASSIGNEE = "assignee";
	
//	public static final String CLAIM_DOC_LINK = "claim_doc_link";
	
	
	// EMAIL SEND NOTIFICATION
	public static final String SEND_EMAIL_NOTIFICTION_TO_RESET_PASSWORD_SUCCESS = "Email sent successfully..";
	public static final String SEND_EMAIL_NOTIFICTION_TO_RESET_PASSWORD_FAIL = "Email sent fail";
	public static final String SEND_EMAIL_AUTH_USER = "garun1mishra@gmail.com";
	public static final String SEND_EMAIL_AUTH_PASSWORD = "garun1mishra";

	public static final String EMAIL_SUBJECT_ENGLISH = "[SHIELD Dashboard] Notice of customer reset password on SHIELD Dashboard Application";
	public static final String EMAIL_SUBJECT_VITENAM = "Test  Subject Vitenai Language  :  Thông tin chi tiết về quyền lợi bảo hiểm ${policy_name} của khách hàng.";

	// Mail Body
	public static final String PASSWORD_RESET_CUSTOMER_NAME = "Dear ${customer_name}, <br><br>";
	public static final String PASSWORD_RESET_BODY_MESSAGE_IN_EN = "You recently requested to reset your password for your SHIELD Dashboard Account."
			+" Click the <b>'Reset Password'</b> button to reset it."
			+ "<br><br>${password_reset_link} <br><br> If you did not request a password reset, Please ignore this mail or reply to let us know.<br><br>";
	public static final String PASSWORD_RESET_BODY_MESSAGE_THANKS = "<br><br> Thanks & Best Regards, <br> SHIELD Dashboard Team";
	public static final String PASSWORD_RESET_LINK = "http://10.30.11.26/cd/forgot/";
	public static final String EMAIL_CC = "ankit.saxena@kuliza.com";
//	public static final String EMAIL_BCC = "anand.s@kuliza.com,samriddhi.sengupta@kuliza.com";
	
//CUSTOMER CHAT SUPPORT	
	public static final String CUSTOMER_SUPPORT_TICKET_NUMBER = "customer_support_ticket_number";
	public static final String CUSTOMERCUSTOMER_SUPPORT_TICKET_STATUS = "customer_support_ticket_status";
	public static final String CUSTOMER_SUPPORT_CUSTOMER_NAME = "customer_support_customer_name";
	public static final String CUSTOMER_SUPPORT_CUSTOMER_ID = "customer_support_customer_id";
	public static final String CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO = "customer_support_mobile_number";
	public static final String CUSTOMER_SUPPORT_CUSTOMER_EMAIL = "customer_support_email";
	public static final String CUSTOMER_SUPPORT_TOPIC = "customer_support_topic";
	public static final String CUSTOMER_SUPPORT_LAST_UPDATE = "customer_support_last_update";
	public static final String CUSTOMER_SUPPORT_MESSAGE = "customer_support_message";
	public static final String CUSTOMER_SUPPORT_ALL_MESSAGE = "customer_support_all_message";
	public static final String CUSTOMER_SUPPORT_MESSAGE_DATE = "customer_support_message_date";
	public static final String CUSTOMER_SUPPORT_MESSAGE_TIME = "customer_support_message_time";
	public static final String CUSTOMER_SUPPORT_MESSAGE_THREAD = "customer_support_message_thread";
	public static final String CUSTOMER_SUPPORT_MESSAGE_IDENTIFIER = "customer_support_message_identifier";
	
	public static final String SEARCH_DASHBOARD_CUSTOMER_SUPPORT = "search_dashboard_customer_support";
	public static final String SEARCH_BY_CUSTOMER_SUPPORT_TICKET_NUMBER = "search_by_customer_ticket_number";
	public static final String SEARCH_BY_CUSTOMER_SUPPORT_TOPIC = "search_by_customer_topic";
	public static final String SEARCH_BY_CUSTOMER_SUPPORT_TICKET_STATUS = "search_by_customer_ticket_status";
	
	public static final String CHAT_SUPPORT_CSR_MESSAGE_SAVE_FAILED = "Chat Customer support message save failed.";
	public static final String CHAT_LIST_BY_NEW_AND_OPEN = "chat_list_by_new_and_open";
	public static final String CHAT_LIST_BY_NEW_AND_CLOSED = "chat_list_by_closed";
	public static final String CHAT_LIST_BY_NEW_AND_RESOLVED = "chat_list_by_resolved";
	
	
	
	

}
	