package com.kuliza.lending.wf_implementation.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.WorkFlowSource;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;

@Repository
public interface WorkflowSourceDao extends CrudRepository<WorkFlowSource, Long> {

	public WorkFlowSource findById(long id);

	public WorkFlowSource findByIdAndIsDeleted(long id, boolean isDeleted);

	public WorkFlowSource findByWfUserAppAndSourceIdAndSourceTypeAndSourceNameAndIsDeleted(WorkFlowUserApplication wfUserApp,String sourceId, String sourceType, 
			String sourceName, boolean isDeleted);
	
	public WorkFlowSource findByWfUserAppAndIsDeleted(WorkFlowUserApplication wfUserApp, boolean isDeleted);
}
