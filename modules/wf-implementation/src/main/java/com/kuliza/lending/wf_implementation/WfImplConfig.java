package com.kuliza.lending.wf_implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WfImplConfig {

	@Value("${otp.generateOTP}")
	private Boolean generateOTP;

	@Value("${otp.validateDefaultOTP}")
	private Boolean validateDefaultOTP;

	@Value("${ib.ibHost}")
	private String ibHost;
	
	@Value("${ib.ibEnv}")
	private String ibEnv;
	
	@Value("${ib.ibCompany}")
	private String ibCompany;

	@Value("${api.masterEndpoint}")
	private String masterEndpoint;

	@Value("${env}")
	private String env;
	
	@Value("${payoo.serviceId}")
	private String payooServiceId;
	
	@Value("${payoo.providerId}")
	private String payooProviderId;

	@Value("${spring.mail.host}")
    private String smtpHostProperties;
    
    @Value("${spring.mail.port}")
    private String smtpPortProperties;
    
    @Value("${mail.smtpAuthProperties}")
    private String smtpAuthProperties;
    
    @Value("${mail.smtpStarttlsProperties}")
    private String smtpStarttlsProperties;
    
    @Value("${spring.mail.username}")
    private String smtpUserNameProperties;
    
    @Value("${spring.mail.password}")
    private String smtpPasswordProperties;
    
    @Value("${mail.verifyEmailUrl}")
    private String verifyEmailUrl;

	@Value("${omnidocs.ENDPOINT}")
	private String omnidocsEndPoint;
	
	@Value("${vtiger.trigger.time}")
	private String vTigerTriggerTime;
	
	@Value("${vtiger.expire.time}")
	private String vTigerExpireTime;
	
	@Value("${dms.termsAndConditionDocId}")
	private String termsAndCondDocId;
	
	@Value("${hyperverge.URL}")
	private String hypervergeUrl;
	
	@Value("${hyperverge.APPID}")
	private String hypervergeAppId;
	
	@Value("${hyperverge.KEY}")
	private String hypervergeKey;
	
	@Value("${dropoff.min.time}")
	private String dropOffMinTime;
	
	@Value("${dropoff.max.time}")
	private String dropOffMaxTime;
	
	@Value("${pushnotification.time}")
	private String pushNotificationTime;
	
	@Value("${pushfirstnotification.time}")
	private String pushFirstNotificationTime;
	
	@Value("${pushsecondnotification.time}")
	private String pushSecondNotificationTime;
	
	@Value("${fpt.file.path}")
	private String fptFilePath;
	@Value("${fpt.outbound.file.path}")
	private String fptOutBoundFilePath;
	
	@Value("${fpt.service.url}")
	private String fptServiceUrl;

	
	@Value("${fpt.relying.party}")
	private String fptRelyingParty;
	
	@Value("${fpt.relying.party.username}")
	private String fptRelyingPartyUserName;
	
	@Value("${fpt.relying.party.password}")
	private String fptRelyingPartyPassword;
	
	@Value("${fpt.relying.party.signature}")
	private String fptRelyingPartySignature;
	
	
	@Value("${fpt.relying.party.key.path}")
	private String fptRelyingPartyKeyPath;
	
	@Value("${fpt.relying.party.keystore.password}")
	private String fptRelyingPartyKeyStorePassword;
	
	@Value("${fpt.certificate.profile}")
	private String fptCertificateProfile;
	
	@Value("${vtiger.refcode.trigger.time}")
	private String vTigerRefCodeTriggerTime;
	
	@Value("${offline.policy.excel.path}")
	private String localDirectory;
	
	@Value("${offline.policy.path}")
	private String offlinePolicyPath;
	
	public String getvTigerRefCodeTriggerTime() {
		return vTigerRefCodeTriggerTime;
	}

	public void setvTigerRefCodeTriggerTime(String vTigerRefCodeTriggerTime) {
		this.vTigerRefCodeTriggerTime = vTigerRefCodeTriggerTime;
	}
	
	public String getVerifyEmailUrl() {
		return verifyEmailUrl;
	}

	public void setVerifyEmailUrl(String verifyEmailUrl) {
		this.verifyEmailUrl = verifyEmailUrl;
	}
	public Boolean getGenerateOTP() {
		return generateOTP;
	}

	public void setGenerateOTP(Boolean generateOTP) {
		this.generateOTP = generateOTP;
	}

	public Boolean getValidateDefaultOTP() {
		return validateDefaultOTP;
	}

	public void setValidateDefaultOTP(Boolean validateDefaultOTP) {
		this.validateDefaultOTP = validateDefaultOTP;
	}

	public String getIbHost() {
		return ibHost;
	}

	public void setIbHost(String ibHost) {
		this.ibHost = ibHost;
	}

	public String getMasterEndpoint() {
		return masterEndpoint;
	}

	public void setMasterEndpoint(String masterEndpoint) {
		this.masterEndpoint = masterEndpoint;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getPayooServiceId() {
		return payooServiceId;
	}

	public void setPayooServiceId(String payooServiceId) {
		this.payooServiceId = payooServiceId;
	}

	public String getPayooProviderId() {
		return payooProviderId;
	}

	public void setPayooProviderId(String payooProviderId) {
		this.payooProviderId = payooProviderId;
	}
	public String getSmtpHostProperties() {
		return smtpHostProperties;
	}

	public void setSmtpHostProperties(String smtpHostProperties) {
		this.smtpHostProperties = smtpHostProperties;
	}

	public String getSmtpPortProperties() {
		return smtpPortProperties;
	}

	public void setSmtpPortProperties(String smtpPortProperties) {
		this.smtpPortProperties = smtpPortProperties;
	}

	public String getSmtpAuthProperties() {
		return smtpAuthProperties;
	}

	public void setSmtpAuthProperties(String smtpAuthProperties) {
		this.smtpAuthProperties = smtpAuthProperties;
	}

	public String getSmtpStarttlsProperties() {
		return smtpStarttlsProperties;
	}

	public void setSmtpStarttlsProperties(String smtpStarttlsProperties) {
		this.smtpStarttlsProperties = smtpStarttlsProperties;
	}

	public String getSmtpUserNameProperties() {
		return smtpUserNameProperties;
	}

	public void setSmtpUserNameProperties(String smtpUserNameProperties) {
		this.smtpUserNameProperties = smtpUserNameProperties;
	}

	public String getSmtpPasswordProperties() {
		return smtpPasswordProperties;
	}

	public void setSmtpPasswordProperties(String smtpPasswordProperties) {
		this.smtpPasswordProperties = smtpPasswordProperties;
	}
	public String getOmnidocsEndPoint() {
		return omnidocsEndPoint;
	}

	public void setOmnidocsEndPoint(String omnidocsEndPoint) {
		this.omnidocsEndPoint = omnidocsEndPoint;
	}


	public String getvTigerTriggerTime() {
		return vTigerTriggerTime;
	}

	public void setvTigerTriggerTime(String vTigerTriggerTime) {
		this.vTigerTriggerTime = vTigerTriggerTime;
	}

	public String getTermsAndCondDocId() {
		return termsAndCondDocId;
	}

	public void setTermsAndCondDocId(String termsAndCondDocId) {
		this.termsAndCondDocId = termsAndCondDocId;
	}

	public String getHypervergeUrl() {
		return hypervergeUrl;
	}

	public void setHypervergeUrl(String hypervergeUrl) {
		this.hypervergeUrl = hypervergeUrl;
	}

	public String getHypervergeAppId() {
		return hypervergeAppId;
	}

	public void setHypervergeAppId(String hypervergeAppId) {
		this.hypervergeAppId = hypervergeAppId;
	}

	public String getHypervergeKey() {
		return hypervergeKey;
	}

	public void setHypervergeKey(String hypervergeKey) {
		this.hypervergeKey = hypervergeKey;
	}

	public String getIbEnv() {
		return ibEnv;
	}

	public void setIbEnv(String ibEnv) {
		this.ibEnv = ibEnv;
	}

	public String getIbCompany() {
		return ibCompany;
	}

	public void setIbCompany(String ibCompany) {
		this.ibCompany = ibCompany;
	}
	
	public String getvTigerExpireTime() {
		return vTigerExpireTime;
	}

	public void setvTigerExpireTime(String vTigerExpireTime) {
		this.vTigerExpireTime = vTigerExpireTime;
	}

	public String getDropOffMinTime() {
		return dropOffMinTime;
	}

	public void setDropOffMinTime(String dropOffMinTime) {
		this.dropOffMinTime = dropOffMinTime;
	}

	public String getDropOffMaxTime() {
		return dropOffMaxTime;
	}

	public void setDropOffMaxTime(String dropOffMaxTime) {
		this.dropOffMaxTime = dropOffMaxTime;
	}

	public String getPushNotificationTime() {
		return pushNotificationTime;
	}

	public void setPushNotificationTime(String pushNotificationTime) {
		this.pushNotificationTime = pushNotificationTime;
	}

	public String getPushFirstNotificationTime() {
		return pushFirstNotificationTime;
	}

	public void setPushFirstNotificationTime(String pushFirstNotificationTime) {
		this.pushFirstNotificationTime = pushFirstNotificationTime;
	}

	public String getPushSecondNotificationTime() {
		return pushSecondNotificationTime;
	}

	public void setPushSecondNotificationTime(String pushSecondNotificationTime) {
		this.pushSecondNotificationTime = pushSecondNotificationTime;
	}
	
	public String getFptFilePath() {
		return fptFilePath;
	}

	public void setFptFilePath(String fptFilePath) {
		this.fptFilePath = fptFilePath;
	}

	public String getFptServiceUrl() {
		return fptServiceUrl;
	}

	public void setFptServiceUrl(String fptServiceUrl) {
		this.fptServiceUrl = fptServiceUrl;
	}

	public String getFptRelyingPartyKeyPath() {
		return fptRelyingPartyKeyPath;
	}

	public void setFptRelyingPartyKeyPath(String fptRelyingPartyKeyPath) {
		this.fptRelyingPartyKeyPath = fptRelyingPartyKeyPath;
	}

	public String getFptRelyingPartyUserName() {
		return fptRelyingPartyUserName;
	}

	public void setFptRelyingPartyUserName(String fptRelyingPartyUserName) {
		this.fptRelyingPartyUserName = fptRelyingPartyUserName;
	}

	public String getFptRelyingPartyPassword() {
		return fptRelyingPartyPassword;
	}

	public void setFptRelyingPartyPassword(String fptRelyingPartyPassword) {
		this.fptRelyingPartyPassword = fptRelyingPartyPassword;
	}

	public String getFptOutBoundFilePath() {
		return fptOutBoundFilePath;
	}

	public void setFptOutBoundFilePath(String fptOutBoundFilePath) {
		this.fptOutBoundFilePath = fptOutBoundFilePath;
	}

	public String getFptRelyingParty() {
		return fptRelyingParty;
	}

	public void setFptRelyingParty(String fptRelyingParty) {
		this.fptRelyingParty = fptRelyingParty;
	}

	public String getFptRelyingPartySignature() {
		return fptRelyingPartySignature;
	}

	public void setFptRelyingPartySignature(String fptRelyingPartySignature) {
		this.fptRelyingPartySignature = fptRelyingPartySignature;
	}

	public String getFptRelyingPartyKeyStorePassword() {
		return fptRelyingPartyKeyStorePassword;
	}

	public void setFptRelyingPartyKeyStorePassword(String fptRelyingPartyKeyStorePassword) {
		this.fptRelyingPartyKeyStorePassword = fptRelyingPartyKeyStorePassword;
	}

	public String getFptCertificateProfile() {
		return fptCertificateProfile;
	}

	public void setFptCertificateProfile(String fptCertificateProfile) {
		this.fptCertificateProfile = fptCertificateProfile;
	}

	public String getLocalDirectory() {
		return localDirectory;
	}

	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}

	public String getOfflinePolicyPath() {
		return offlinePolicyPath;
	}

	public void setOfflinePolicyPath(String offlinePolicyPath) {
		this.offlinePolicyPath = offlinePolicyPath;
	}
	



}
