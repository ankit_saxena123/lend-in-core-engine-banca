package com.kuliza.lending.wf_implementation.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.common.ErrorCodes;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.exception.FECException;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaSMSIntegration;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.pojo.EmailInputForm;
import com.kuliza.lending.wf_implementation.pojo.WorkFlowUserSms;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

@Service
public class FECreditBancaNotifyUserServices {

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	private WorkflowUserDao workflowuserDao;

	@Autowired
	private FECreditBancaSMSIntegration feCreditBancaSMSIntegration;

	@Autowired
	private WfImplConfig wfImplConfig;

	@Autowired
	private FECDMSService fecdmsService;

	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory.getLogger(FECreditBancaNotifyUserServices.class);

	public ApiResponse sendSmsNotification(WorkFlowUserSms workFlowUserSms) throws Exception {
		logger.info("-->Entering FECreditBancaNotifyUserServices.sendSmsNotification()");
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Object> parseResponse = new HashMap<String, Object>();
		HttpResponse<JsonNode> apiResponse = null;
		data.put(Constants.MOBILE_NUMBER_KEY, workFlowUserSms.getMobileNumber());
		data.put(Constants.FE_CREDIT_MVP_SMS_MESSAGE_KEY, workFlowUserSms.getMessage());
		JSONObject request = feCreditBancaSMSIntegration.buildRequestPayload(data);
		apiResponse = feCreditBancaSMSIntegration.getDataFromService(request);
		logger.info("apiResponse --->" + apiResponse);
		if (apiResponse != null) {
			parseResponse = feCreditBancaSMSIntegration.parseResponse(apiResponse.getBody().getObject());
			logger.info("parseResponse --->" + parseResponse);
		} else {
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE,
					parseResponse);
		}
		logger.info("<-- Exiting FECreditBancaNotifyUserServices.sendSmsNotification()");
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, parseResponse);
	}

	public ApiResponse sendEmail(String userName, EmailInputForm emailInputForm) {
		try {
			logger.info("-->Entering FECreditBancaNotifyUserServices.sendEmail()");
			String processInstanceId = emailInputForm.getProcessInstanceId();
			logger.info("<=========>processInstanceId<=========> ::" + processInstanceId + "<--- :: UserEmail :: ---> "
					+ emailInputForm.getUserEmail());
			sendEmail(processInstanceId, userName, emailInputForm.getLanguage(), emailInputForm.getUserEmail(),
					emailInputForm.getDmsDocId());
			logger.info("<--Exiting FECreditBancaNotifyUserServices.sendEmail()");
		} catch (Exception e) {
			logger.error("Error While Calling sendMail : " + e.getMessage(), e);
		}
		return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, Constants.EMAIL_SENT_SUCCESS);
	}

	private String createEmailSubject(String processInstanceId, WorkFlowApplication workFlowApp, String language) {
		logger.info("-->Entering FECreditBancaNotifyUserServices.createEmailSubject()");
		String subjectContent = null;
		WorkFlowApplication workFlowApplication = workflowApplicationDao
				.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		String productName = workFlowApplication.getProductName();
		if (language.equalsIgnoreCase("en-US")) {
			subjectContent = Constants.EMAIL_SUBJECT_ENGLISH;
			subjectContent = subjectContent.replace("${policy_name}", productName);
		} else {
			subjectContent = Constants.EMAIL_SUBJECT_VITENAM;
			subjectContent = subjectContent.replace("${policy_name}", productName);
		}
		logger.info("<=========>EmailSubject<=========> ::" + subjectContent);
		logger.info("<--Exiting FECreditBancaNotifyUserServices.createEmailSubject()()");
		return subjectContent;
	}

	private String createEmailBody(String userName, String processInstanceId, WorkFlowApplication workFlowApp,
			String language) throws Exception {
		logger.info("-->Entering FECreditBancaNotifyUserServices.createEmailBody()");
		String customerName = null;
		WorkFlowUser workFlowUser = workflowuserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser != null) {
			customerName = workFlowUser.getUsername();
			logger.info("<=========>customerName<=========> :: " + customerName);
		} else {
			throw new UsernameNotFoundException("user does not exist");
		}
		WorkFlowApplication workFlowApplication = workflowApplicationDao
				.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if (workFlowApplication == null) {
			throw new Exception("WorkFlowApplication does not exist");
		}
		String productName = Utils.getStringValue(workFlowApplication.getProductName());
		String sumInsured = Utils.getStringValue(workFlowApplication.getProtectionAmount());
		logger.info("<=========>sumInsured<=========> ::" + sumInsured);
		String customerNamePolicyName = null;
		String policyBodyMessage = null;
		String policyMessage = null;
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
		Map<String, Object> variables = processInstance.getProcessVariables();
		String tenor = Utils.getStringValue(variables.get(Constants.USER_PREMIUM_TENURE));
		if (tenor.isEmpty()) {
			tenor = "yearly";
		}
		if (language.equalsIgnoreCase("en-US")) {
			customerNamePolicyName = Constants.CUSTOMER_NAME_POLICY_NAME;
			policyBodyMessage = Constants.POLICY_BODY_MESSAGE_IN_EN;
			policyMessage = Constants.POLICY_MESSAGE;
		} else {
			customerNamePolicyName = Constants.CUSTOMER_NAME_POLICY_NAME_IN_VI;
			policyBodyMessage = Constants.POLICY_BODY_MESSAGE_IN_VN;
			policyMessage = Constants.POLICY_MESSAGE_IN_VI;
		}

		customerNamePolicyName = customerNamePolicyName.replace("${customer_name}", customerName)
				.replace("${policy_name}", productName);
		policyBodyMessage = policyBodyMessage.replace("${policy_name}", productName);
		String emailBodyContent = customerNamePolicyName + policyBodyMessage + policyMessage;
		logger.info("<--Exiting FECreditBancaNotifyUserServices.createEmailBody()");
		return emailBodyContent;
	}

	public void sendEmail(String processInstanceId, String userName, String language, String userEmail,
			String dmsDocId) {
		
		try {
			logger.info("Send email called for procId : " + processInstanceId + ", DocId : " + dmsDocId);
			
			WorkFlowApplication workFlowApplication = workflowApplicationDao
					.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
			if (dmsDocId == null || dmsDocId.isEmpty()) {
				logger.error("invalid doc id : " + dmsDocId);
				throw new FECException(ErrorCodes.INVALID_DOC_ID);
			}


			logger.info("<=========>dmsDocId<=========> ::" + dmsDocId);
			Properties prop = new Properties();
			prop.put(Constants.MAIL_SMTP_HOST_KEY, wfImplConfig.getSmtpHostProperties());
			prop.put(Constants.MAIL_SMTP_PORT_KEY, wfImplConfig.getSmtpPortProperties());
			prop.put(Constants.MAIL_SMTP_AUTH_KEY, wfImplConfig.getSmtpAuthProperties());
			prop.put(Constants.MAIL_SMTP_STARTTLS_KEY, wfImplConfig.getSmtpStarttlsProperties()); // TLS
			Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(wfImplConfig.getSmtpUserNameProperties(),
							wfImplConfig.getSmtpPasswordProperties());
				}
			});
			Message message = new MimeMessage(session);
			logger.info("setFrom ----> " + wfImplConfig.getSmtpUserNameProperties());
			message.setFrom(new InternetAddress(wfImplConfig.getSmtpUserNameProperties()));
			message.setSubject(createEmailSubject(processInstanceId, workFlowApplication, language));
			logger.info("userEmail ----> " + userEmail);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(userEmail));

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDescription(createEmailSubject(processInstanceId, workFlowApplication, language));
			messageBodyPart.setContent(createEmailBody(userName, processInstanceId, workFlowApplication, language),
					Constants.EMAIL_BODY_CONTENT_TYPE);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			MimeBodyPart attachPart = new MimeBodyPart();
			logger.info("Send email called for procId : " + processInstanceId + ", DocId : " + dmsDocId);
			if (workFlowApplication.getDmsDocId() != null && !workFlowApplication.getDmsDocId().isEmpty()) {
				dmsDocId = workFlowApplication.getDmsDocId();
			}
			logger.info("Send email called for procId : " + processInstanceId + ", DocId : " + dmsDocId);
			if (dmsDocId == null || dmsDocId.isEmpty()) {
				logger.error("invalid doc id : " + dmsDocId);
				throw new FECException(ErrorCodes.INVALID_DOC_ID);
			}
			File file = fecdmsService.downloadDocumentAsFile(dmsDocId, "", true);
			attachPart.attachFile(file);
			multipart.addBodyPart(attachPart);
			message.setContent(multipart);

			Transport.send(message);
			logger.info("Mail sent SuccessFully for "+userEmail);
			if (file.exists()) {
				file.delete();
				logger.info("<=========>file.delete() SuccessFully<=========>");
			}
		} catch (Exception e) {
			logger.error("Error while sending mail .." ,e);
		}

	}
}
