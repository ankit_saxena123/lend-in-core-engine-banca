package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.utils.Constants.DeviceType;
import com.kuliza.lending.journey.model.AbstractWfUserDeviceRegister;

@Entity
@Table(name = "wf_user_device_register")
public class WfUserDeviceRegister extends AbstractWfUserDeviceRegister {

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	private WorkFlowUser wfUser;

	@Column(nullable = true, name = "appbuild_number")
	private String appbuildNumber;

	@Column(nullable = true, name = "appbundle_id")
	private String appbundleId;

	@Column(nullable = true, name = "brand_name")
	private String brandName;

	@Column(nullable = true, name = "carrier")
	private String carrier;

	@Column(nullable = true, name = "device_locale")
	private String deviceLocale;

	@Column(nullable = true, name = "device_name")
	private String deivceName;

	@Column(nullable = true, name = "device_number_id")
	private String deviceNumberId;

	@Column(nullable = true, name = "imei")
	private String imei;

	@Column(nullable = true, name = "ip")
	private String ip;

	@Column(nullable = true, name = "mac_address")
	private String macAddress;

	@Column(nullable = true, name = "model")
	private String model;

	@Column(nullable = true, name = "phone_number")
	private String phoneNumber;

	@Column(nullable = true, name = "version")
	private String version;

	@Column(nullable = true, name = "application_name")
	private String applicationName;

	@Column(nullable = true, name = "manufacturer")
	private String manufacturer;

	@Column(nullable = true, name = "device_info_details", columnDefinition = "LONGTEXT")
	private String deviceInfoDetails;

	@Column(nullable = true, name = "apps_flyer_data", columnDefinition = "LONGTEXT")
	private String appsFlyerData;

	@Column(nullable = true, name = "unique_id")
	private String uniqueId;

	@Column(nullable = true, name = "apps_flyer_id")
	private String appsFlyerId;
	
	@Column(nullable = true, name = "advertising_id")
	private String advertisingId;
	
	@Column(nullable = true, name = "idfa_for_ios")
	private String idfa;
	

	public String getAppbuildNumber() {
		return appbuildNumber;
	}

	public void setAppbuildNumber(String appbuildNumber) {
		this.appbuildNumber = appbuildNumber;
	}

	public String getAppbundleId() {
		return appbundleId;
	}

	public void setAppbundleId(String appbundleId) {
		this.appbundleId = appbundleId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getDeviceLocale() {
		return deviceLocale;
	}

	public void setDeviceLocale(String deviceLocale) {
		this.deviceLocale = deviceLocale;
	}

	public String getDeivceName() {
		return deivceName;
	}

	public void setDeivceName(String deivceName) {
		this.deivceName = deivceName;
	}

	public String getDeviceNumberId() {
		return deviceNumberId;
	}

	public void setDeviceNumberId(String deviceNumberId) {
		this.deviceNumberId = deviceNumberId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public WfUserDeviceRegister() {
		super();
	}

	public WfUserDeviceRegister(WorkFlowUser wfUser, DeviceType deviceType, String deviceId, String mpin) {
		super(deviceType, deviceId, mpin);
		this.wfUser = wfUser;
	}

	public WfUserDeviceRegister(WorkFlowUser wfUser, DeviceType deviceType, String deviceId) {
		super(deviceType, deviceId);
		this.wfUser = wfUser;
	}

	public WorkFlowUser getWfUser() {
		return wfUser;
	}

	public void setWfUser(WorkFlowUser wfUser) {
		this.wfUser = wfUser;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getDeviceInfoDetails() {
		return deviceInfoDetails;
	}

	public void setDeviceInfoDetails(String deviceInfoDetails) {
		this.deviceInfoDetails = deviceInfoDetails;
	}

	public String getAppsFlyerData() {
		return appsFlyerData;
	}

	public void setAppsFlyerData(String appsFlyerData) {
		this.appsFlyerData = appsFlyerData;
	}
	// Add more fields to this table if required in the format given below

	public String getUniqueId() {
		return uniqueId;
	}

	public String getAppsFlyerId() {
		return appsFlyerId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public void setAppsFlyerId(String appsFlyerId) {
		this.appsFlyerId = appsFlyerId;
	}

	@Override
	public String toString() {
		return "WfUserDeviceRegister [wfUser=" + wfUser + ", appbuildNumber=" + appbuildNumber + ", appbundleId="
				+ appbundleId + ", brandName=" + brandName + ", carrier=" + carrier + ", deviceLocale=" + deviceLocale
				+ ", deivceName=" + deivceName + ", deviceNumberId=" + deviceNumberId + ", imei=" + imei + ", ip=" + ip
				+ ", macAddress=" + macAddress + ", model=" + model + ", phoneNumber=" + phoneNumber + ", version="
				+ version + ", applicationName=" + applicationName + ", manufacturer=" + manufacturer
				+ ", deviceInfoDetails=" + deviceInfoDetails + ", appsFlyerData=" + appsFlyerData + ", uniqueId="
				+ uniqueId + ", appsFlyerId=" + appsFlyerId + ", advertisingId=" + advertisingId + ", idfa=" + idfa
				+ "]";
	}

	public String getAdvertisingId() {
		return advertisingId;
	}

	public String getIdfa() {
		return idfa;
	}

	public void setAdvertisingId(String advertisingId) {
		this.advertisingId = advertisingId;
	}

	public void setIdfa(String idfa) {
		this.idfa = idfa;
	}

	// @Column(nullable = true)
	// private String destinationType;
	//
	// public WfUserDeviceRegister() {
	// super();
	// this.setIsDeleted(false);
	// }
	//
	// public WfUserDeviceRegister(String sourceId, String sourceType,
	// String sourceName, String destinationType) {
	// super();
	// this.setSourceId(sourceId);
	// this.setSourceType(sourceType);
	// this.setSourceName(sourceName);
	// this.destinationType = destinationType;
	// }
	//
	// public String getDestinationType() {
	// return destinationType;
	// }
	//
	// public void setDestinationType(String destinationType) {
	// this.destinationType = destinationType;
	// }

}
