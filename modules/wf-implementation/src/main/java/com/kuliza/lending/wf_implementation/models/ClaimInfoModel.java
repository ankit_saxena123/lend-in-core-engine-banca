package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "claim_info")
public class ClaimInfoModel extends BaseModel{

@Column(nullable = true, name = "process_instance_id")
private String processInstanceId;

@Column(nullable = true, name = "insurer_name")
private String insurerName;

@Column(nullable = true, name = "api_response", columnDefinition = "LONGTEXT")
private String apiResponse;

@Column(nullable = true, name = "claim_status")
private String claimStatus;

@Column(nullable = true, name = "claim_number")
private String claimsid;

@Column(nullable = true, name = "claim_code")
private String code;

@Column(nullable = true, name = "policy_number")
private String policyNo;

@Column(nullable = true, name = "claim_ref_number")
private String referenceNumber;

@Column(nullable = true, name = "claim_remarks")
private String remarks;

@Column(nullable = true, name = "status")
private String status;

@Column(nullable = true, name = "claim_amount")
private String claimAmount;

@Column(nullable = true, name = "api_request", columnDefinition = "LONGTEXT")
private String apiRequest;

@Type(type = "org.hibernate.type.NumericBooleanType")
@ColumnDefault("0")
@Column(nullable=false)
private Boolean success = false;

@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(nullable = true, name="ipat_payment_confirm_id")
private IPATPaymentConfirmModel iPatPaymentConfirm;


@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(nullable = true, name="cms_payment_confirm_id")
private CMSIntegration cmsPaymentConfirm;


public String getClaimAmount() {
	return claimAmount;
}

public void setClaimAmount(String claimAmount) {
	this.claimAmount = claimAmount;
}

public String getClaimStatus() {
	return claimStatus;
}

public void setClaimStatus(String claimStatus) {
	this.claimStatus = claimStatus;
}

public String getClaimsid() {
	return claimsid;
}

public void setClaimsid(String claimsid) {
	this.claimsid = claimsid;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public String getReferenceNumber() {
	return referenceNumber;
}

public void setReferenceNumber(String referenceNumber) {
	this.referenceNumber = referenceNumber;
}

public String getRemarks() {
	return remarks;
}

public void setRemarks(String remarks) {
	this.remarks = remarks;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getApiRequest() {
	return apiRequest;
}

public void setApiRequest(String apiRequest) {
	this.apiRequest = apiRequest;
}

public CMSIntegration getCmsPaymentConfirm() {
	return cmsPaymentConfirm;
}

public void setCmsPaymentConfirm(CMSIntegration cmsPaymentConfirm) {
	this.cmsPaymentConfirm = cmsPaymentConfirm;
}

public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public String getInsurerName() {
	return insurerName;
}

public void setInsurerName(String insurerName) {
	this.insurerName = insurerName;
}

public String getApiResponse() {
	return apiResponse;
}

public void setApiResponse(String apiResponse) {
	this.apiResponse = apiResponse;
}

public Boolean getSuccess() {
	return success;
}

public void setSuccess(Boolean success) {
	this.success = success;
}


public IPATPaymentConfirmModel getiPatPaymentConfirm() {
	return iPatPaymentConfirm;
}

public void setiPatPaymentConfirm(IPATPaymentConfirmModel iPatPaymentConfirm) {
	this.iPatPaymentConfirm = iPatPaymentConfirm;
}

public String getPolicyNo() {
	return policyNo;
}

public void setPolicyNo(String policyNo) {
	this.policyNo = policyNo;
}


}