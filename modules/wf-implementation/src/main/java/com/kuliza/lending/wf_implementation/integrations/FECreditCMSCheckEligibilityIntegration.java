package com.kuliza.lending.wf_implementation.integrations;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class FECreditCMSCheckEligibilityIntegration {

	@Autowired
	private WfImplConfig wfImplConfig;

	private static final Logger logger = LoggerFactory.getLogger(FECreditCMSCheckEligibilityIntegration.class);

	public JSONObject getrequestPayload(Map<String, Object> data) throws Exception {
		logger.info("*********************** INSIDE CE getrequestPayload ****************** ");
		JSONObject requestPayload = new JSONObject();
		JSONObject requestBody = new JSONObject();

		String mobile = Utils.getStringValue(data.get(WfImplConstants.APPLICATION_MOBILE_NUMBER));
		if (mobile.startsWith("0")) {
			mobile = mobile.substring(1, mobile.length());
		}
		logger.info("mobile no: " + mobile);
		if(mobile.contains("+")) {
			mobile=mobile.replace("+","");
			logger.info("========>>>>> mobile no after removal of + " + mobile);
		}
		requestBody.put("cms:CheckEligibility.Phone", mobile);
		requestBody.put("cms:CheckEligibility.NationalID", "");
//		if(wfImplConfig.getEnv().equalsIgnoreCase("uat")){
//			requestBody.put("cms:CheckEligibility.Phone", Constants.CMS_UAT_PHONE_NO);
//			requestBody.put("cms:CheckEligibility.NationalID", Constants.CMS_UAT_NATIONALID);
//		}
		requestBody.put("cms:CheckEligibility.Sys.TransID", Utils.generateUUIDTransanctionId());
		requestBody.put("cms:CheckEligibility.Sys.DateTime", Utils.getCurrentDateTime());
		requestBody.put("cms:CheckEligibility.PremiumAmount",
				Utils.ConvertViatnumCurrencyToDigits(Utils.getStringValue(data.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT))));
		requestBody.put("cms:CheckEligibility.Sys.RequestorID", Constants.FEC_REQUESTOR_ID);
		requestPayload.put("body", requestBody);
		logger.info("********* REQUEST PAYLOAD ******** " + requestPayload);
		logger.info("*********************** EXITING CE getrequestPayload ****************** ");
		return requestPayload;
	}

	public JSONObject getDataFromService(JSONObject requestPayload) throws Exception {
		logger.info("********* inside CE getDataFromService******** ");
		HttpResponse<JsonNode> resposeAsJson = null;
		JSONObject responseJson = new JSONObject();
		try {
			Map<String, String> headersMap = new HashMap<String, String>();

			headersMap.put("Content-Type", Constants.CONTENT_TYPE);
			String hashGenerationUrl = wfImplConfig.getIbHost() + Constants.CMS_CHECK_ELIGIBILITY_SOAP_SLUG_KEY
					+ Constants.CMS_CHECK_ELIGIBILITY_SLUG + "/" + Constants.CMS_CHECK_ELIGIBILITY_KEY + "/"
					+ wfImplConfig.getIbCompany() + "/" + Constants.CMS_CHECK_ELIGIBILITY_HASH+"/?env="+wfImplConfig.getIbEnv()
					+ Constants.CMS_CHECK_ELIGIBILITY_INTEGRATION_SOAP_KEY;
			 logger.info("--------->>>>>>>>>>>>> " + hashGenerationUrl);
			resposeAsJson = Unirest.post(hashGenerationUrl).headers(headersMap).body(requestPayload).asJson();
			responseJson = resposeAsJson.getBody().getObject();
			logger.info("responseJson : " + responseJson);
			return responseJson;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("unable to call CMS api :" + e.getMessage(), e);
		}
		logger.info("<-- ***************Exiting CE getDataFromService()**************");
		return responseJson;
	}

	public Map<String, Object> parseResponse(JSONObject responseFromIB) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		logger.info("*********** Innside CE Parse Response ***************  ->");
		JSONObject details = new JSONObject();
		JSONObject checkEligibilityResponse = new JSONObject();
		try {
			if (responseFromIB != null) {
				if(responseFromIB.has(Constants.CHECK_ELIGIBILITY_RESPONSE)){
					checkEligibilityResponse = responseFromIB.getJSONObject(Constants.CHECK_ELIGIBILITY_RESPONSE);

				if (checkEligibilityResponse.has(Constants.CMS_CHECK_ELIGIBILITY_DETAILS)) {

					details = checkEligibilityResponse.getJSONObject(Constants.CMS_CHECK_ELIGIBILITY_DETAILS);
					logger.info("inside integration details *************** -> " + details);
					boolean eligibility = (boolean) details
							.getBoolean(Constants.CHECK_ELIGIBILITY_BANCA_INSURANCE_ELIGIBILITY);
					if (eligibility) {

						map.put(Constants.CMS_CHECK_ELIGIBILITY_STATUS, true);
						map.put(Constants.CMS_CHECK_ELIGIBILITY_ACCOUNT,
								details.get(Constants.CMS_CHECK_ELIGIBILITY_ACCOUNT));
//						if(wfImplConfig.getEnv().equalsIgnoreCase("uat"))
//							map.put(Constants.CMS_CHECK_ELIGIBILITY_ACCOUNT,
//									details.get(Constants.CMS_CHECK_ELIGIBILITY_ACCOUNT));
						map.put(Constants.CMS_CHECK_ELIGIBILITY_CREDITLIMIT,
								details.get(Constants.CMS_CHECK_ELIGIBILITY_CREDITLIMIT));
						map.put(Constants.CMS_CHECK_ELIGIBILITY_AVAILABLE_LIMIT,
								details.get(Constants.CMS_CHECK_ELIGIBILITY_AVAILABLE_LIMIT));

					} else {
						map.put(Constants.CMS_CHECK_ELIGIBILITY_STATUS, false);
						map.put(Constants.CMS_MESSAGE, Constants.CMS_NOT_ELIGIBLE);
					}
				}else{
					map.put(Constants.CMS_CHECK_ELIGIBILITY_STATUS, false);
					map.put(Constants.CMS_MESSAGE, Constants.CMS_DETAILS_NOT_FOUND);
				}

				} else {
					map.put(Constants.CMS_CHECK_ELIGIBILITY_STATUS, false);
					map.put(Constants.CMS_MESSAGE, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
				}
			}
		} catch (Exception e) {
			map.put(Constants.CMS_CHECK_ELIGIBILITY_STATUS, false);
			map.put(Constants.CMS_MESSAGE, e.getMessage());
			e.printStackTrace();
		}
		logger.info("*********** EXITING CE Parse Response and map response***************" + map);
		return map;
	}

}