/**
 * 
 */
package com.kuliza.lending.wf_implementation.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimDocsDao;
import com.kuliza.lending.wf_implementation.dao.PortalClaimInfoDao;
import com.kuliza.lending.wf_implementation.dao.PortalDashboardChatSupportDao;
import com.kuliza.lending.wf_implementation.dao.PortalDashboardIPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.models.ClaimDocModel;
import com.kuliza.lending.wf_implementation.models.ClaimInfoModel;
import com.kuliza.lending.wf_implementation.models.CustomerQuerySupport;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.PortalDashboardAuthenticationConstants;
import com.kuliza.lending.wf_implementation.utils.Utils;

/**
 * @author Garun Mishra
 * @Description This service includes search for all four customer, policy,
 *              transaction, and claims dashboard list. Once user hit the search
 *              by entering search value with respective earch by option then
 *              this service is called.
 *
 */
@Service
public class PortalDashboardSearchService {
	@Autowired
	private WorkflowUserDao workFlowUserDao;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	IPATPaymentConfirmDao iPATPaymentConfirmDao;

	@Autowired
	PortalDashboardIPATPaymentConfirmDao portalDashboardIPATPaymentConfirmDao;

	@Autowired
	PortalClaimInfoDao portalClaimInfoDao;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	PortalDashboardChatSupportDao portalDashboardChatSupportDao;

	@Autowired
	private PortalClaimDocsDao portalClaimDocsDao;

	private static final Logger logger = LoggerFactory.getLogger(PortalDashboardSearchService.class);

	public ApiResponse getSearchCustomerList(String searchByOption, String searchInputValue) {
		logger.info("-->Entering PortalDashboardSearchService.getSearchCustomerList()");
		List<WorkFlowUser> workFlowUser = null;
		switch (searchByOption) {
		case Constants.CUSTOMER_SEARCH_BY_NAME:
			workFlowUser = (List<WorkFlowUser>) workFlowUserDao.findAllByUsernameContainingIgnoreCase(searchInputValue);
			break;
		case Constants.CUSTOMER_SEARCH_BY_MOBILE_NO:
			workFlowUser = (List<WorkFlowUser>) workFlowUserDao.findAllByMobileNumberAndIsDeleted(searchInputValue,
					false);
			break;
		case Constants.CUSTOMER_SEARCH_BY_EMAIL:
			workFlowUser = (List<WorkFlowUser>) workFlowUserDao.findAllByEmailContainingIgnoreCase(searchInputValue);
			break;
		case Constants.CUSTOMER_SEARCH_BY_NID:
			workFlowUser = (List<WorkFlowUser>) workFlowUserDao.findAllByNationalIdAndIsDeleted(searchInputValue,
					false);
			break;
		}

		List<Map<String, Object>> response = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

		Map<String, Object> totalRecords = new HashMap<String, Object>();
		if (workFlowUser != null) {

			for (WorkFlowUser wfCustomer : workFlowUser) {
				Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
				Date date = wfCustomer.getModified();
				String strDate = dateFormat.format(date);
				String splitDateTime[] = strDate.split(" ");
				workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(wfCustomer.getId()));
				String customerAction = "getCustomerInfo/" + Utils.getStringValue(wfCustomer.getId());
				workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(wfCustomer.getUsername()));
				workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER,
						Utils.getStringValue(wfCustomer.getMobileNumber()));
				workFlowUserMap.put(Constants.CUSTOMER_LAST_UPDATE_DATE, Utils.getStringValue(splitDateTime[0]));
				workFlowUserMap.put(Constants.CUSTOMER_LAST_UPDATE_TIME, Utils.getStringValue(splitDateTime[1]));
				workFlowUserMap.put(Constants.CUSTOMER_EMAIL, Utils.getStringValue(wfCustomer.getEmail()));
				workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID, Utils.getStringValue(wfCustomer.getNationalId()));
				workFlowUserMap.put(Constants.ACTION, customerAction);
				response.add(workFlowUserMap);
			}
			totalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(workFlowUser.size())));
			response.add(totalRecords);
			logger.info("<-- Exiting PortalDashboardSearchService.getSearchCustomerList()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	public ApiResponse getSearchPolicyList(String searchByOption, String searchInputValue) {
		logger.info("-->Entering PortalDashboardSearchService.getSearchPolicyList()");
		List<WorkFlowPolicy> policylists = null;
		switch (searchByOption) {
		case Constants.POLICY_SEARCH_BY_POLICY_NO:
			policylists = workFlowPolicyDAO.findAllByPolicyNumberAndIsDeleted(searchInputValue, false);
			break;
		case Constants.POLICY_SEARCH_BY_PLAN_ID:
			policylists = workFlowPolicyDAO.findAllByPlanIdAndIsDeleted(searchInputValue, false);
			break;
		case Constants.POLICY_SEARCH_BY_PLAN_NAME:
			policylists = workFlowPolicyDAO.findAllByPlanNameAndIsDeleted(searchInputValue, false);
			break;
		case Constants.POLICY_SEARCH_BY_INSURER:
			policylists = workFlowPolicyDAO.findAllByInsurerAndIsDeleted(searchInputValue, false);
			break;
		}
		List<Map<String, Object>> response = new ArrayList<>();
		int slno = 1;
		if (policylists != null) {
			Map<String, Object> addTotalRecords = new HashMap<String, Object>();
			for (WorkFlowPolicy policylist : policylists) {
				WorkFlowUserApplication workflowuserapplication = workflowUserApplicationDao
						.findById(policylist.getWorkFlowApplication().getId());
				if (workflowuserapplication != null) {
					WorkFlowUser workFlowUser = workFlowUserDao
							.findByIdmUserNameAndIsDeleted(workflowuserapplication.getUserIdentifier(), false);
					if (workFlowUser != null) {
						Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
						workFlowUserMap.put(Constants.SERIAL_NO, slno);
						workFlowUserMap.put(Constants.POLICY_NUMBER,
								Utils.getStringValue(policylist.getPolicyNumber()));
						workFlowUserMap.put(Constants.PLAN_ID_KEY, Utils.getStringValue(policylist.getPlanId()));
						workFlowUserMap.put(Constants.PLAN_NAME, Utils.getStringValue(policylist.getPlanName()));

						workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workFlowUser.getId()));
						workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(workFlowUser.getUsername()));
						workFlowUserMap.put(Constants.CUSTOMER_MOBILE_NUMBER,
								Utils.getStringValue(workFlowUser.getMobileNumber()));
						workFlowUserMap.put(Constants.CUSTOMER_EMAIL, Utils.getStringValue(workFlowUser.getEmail()));
						workFlowUserMap.put(Constants.CUSTOMER_NATIONAL_ID,
								Utils.getStringValue(workFlowUser.getNationalId()));
						String customerAction = "getCustomerInfo/" + Utils.getStringValue(workFlowUser.getId());
						workFlowUserMap.put(Constants.CUSTOMER_DETAILS, customerAction);
						workFlowUserMap.put(Constants.POLICY_DOCUMENT, "View");

						workFlowUserMap.put(Constants.PORTAL_POLICY_START_DATE,
								Utils.getStringValue(policylist.getPurchasedDate()));
						workFlowUserMap.put(Constants.PORTAL_POLICY_END_DATE,
								Utils.getStringValue(policylist.getPlanExpiringDate()));
						String ploicy_renewal_date = "";
						String startDate_String = policylist.getPurchasedDate();
						String planExpiryDate = policylist.getPlanExpiringDate();
						if ((startDate_String != null && startDate_String != "")) {
							ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(startDate_String, "start_date_exist");
						} else {
							if (planExpiryDate != null && planExpiryDate != "") {
								ploicy_renewal_date = Utils.dateFormatAddDaysAndYear(planExpiryDate,
										"exipry_date_exist");
							}
						}
						workFlowUserMap.put(Constants.POLICY_RENEWAL_DATE, Utils.getStringValue(ploicy_renewal_date));

						workFlowUserMap.put(Constants.POLICY_INSURER, Utils.getStringValue(policylist.getInsurer()));
						workFlowUserMap.put(Constants.POLICY_SUM_ASSURED,
								Utils.getStringValue(policylist.getProtectionAmount()));

						response.add(workFlowUserMap);
						slno = slno + 1;
					}
				}

			}
			addTotalRecords.put(Constants.TOTAL_RECORDS, Utils.getStringValue(Integer.toString(policylists.size())));
			response.add(addTotalRecords);
			logger.info("<-- Exiting PortalDashboardSearchService.getSearchPolicyList()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	// Transaction Search
	public ApiResponse getSearchTransactionList(String searchByOption, String searchInputValue) {
		logger.info("-->Entering PortalDashboardService.getSearchTransactionList()");
		List<Map<String, Object>> response = new ArrayList<>();
		List<WorkFlowPolicy> workFlowPolicies = null;
		switch (searchByOption) {
		case Constants.POLICY_SEARCH_BY_TRANSACTION_ID:
			workFlowPolicies = workFlowPolicyDAO.findAllByTransactionIdAndIsDeleted(searchInputValue, false);
			break;
		case Constants.POLICY_SEARCH_BY_TRANSACTION_POLICY_NO:
			workFlowPolicies = workFlowPolicyDAO.findAllByPolicyNumberAndIsDeleted(searchInputValue, false);
			break;
		case Constants.POLICY_SEARCH_BY_TRANSACTION_PLAN_ID:
			workFlowPolicies = workFlowPolicyDAO.findAllByPlanIdAndIsDeleted(searchInputValue, false);
			break;
		}
		if (workFlowPolicies != null) {
			for (WorkFlowPolicy workFlowPolicy : workFlowPolicies) {
				WorkFlowUserApplication workflowuserapplication = workflowUserApplicationDao
						.findById(workFlowPolicy.getWorkFlowApplication().getId());
				IPATPaymentConfirmModel ipatPaymentConfirm = portalDashboardIPATPaymentConfirmDao
						.findById(workFlowPolicy.getWorkFlowApplication().getId());
				if (ipatPaymentConfirm != null && workflowuserapplication != null) {
					Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
					WorkFlowUser workFlowUser = workFlowUserDao
							.findByIdmUserNameAndIsDeleted(workflowuserapplication.getUserIdentifier(), false);
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_ID,
							Utils.getStringValue(workFlowPolicy.getTransactionId()));
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_METHOD, "Payoo");
					boolean tranasctionStatus = ipatPaymentConfirm.isPaymentConfirmStatus();
					if (!tranasctionStatus) {
						workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Fail..!!");
					} else {
						workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_STATUS, "Success..!!");
					}
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_DATE,
							Utils.getStringValue(ipatPaymentConfirm.getPaymentDate()));

					workFlowUserMap.put(Constants.PLAN_ID_KEY, Utils.getStringValue(workFlowPolicy.getPlanId()));
					if (workFlowUser != null) {
						workFlowUserMap.put(Constants.CUSTOMER_ID, Utils.getStringValue(workFlowUser.getId()));
						workFlowUserMap.put(Constants.CUSTOMER_NAME, Utils.getStringValue(workFlowUser.getUsername()));
					} else {
						workFlowUserMap.put(Constants.CUSTOMER_ID, "");
						workFlowUserMap.put(Constants.CUSTOMER_NAME, "");
					}
					workFlowUserMap.put(Constants.PLAN_NAME, Utils.getStringValue(workFlowPolicy.getPlanName()));
					workFlowUserMap.put(Constants.TRANSACTION_PAYMENT_AMOUNT,
							Utils.getStringValue(ipatPaymentConfirm.getPaymentAmount()));

					workFlowUserMap.put(Constants.POLICY_DOCUMENT, "View");
					workFlowUserMap.put(Constants.TRANSACTION_DETAILS, "View");

					workFlowUserMap.put(Constants.TRANSACTION_REMARKS,
							Utils.getStringValue(ipatPaymentConfirm.getDescription()));
					workFlowUserMap.put(Constants.POLICY_NUMBER,
							Utils.getStringValue(workFlowPolicy.getPolicyNumber()));

					response.add(workFlowUserMap);
				}
			}
			logger.info("<-- Exiting PortalDashboardService.getAllTransaction()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);

		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

//Claim search api
	public ApiResponse getSearchClaimsList(String searchByOption, String searchInputValue) {
		logger.info("-->Entering PortalDashboardSearchService.getSearchClaimsList()");
		List<ClaimDocModel> allRecords = portalClaimDocsDao.findAllByIsDeleted(false);
		Map<String, Object> variables = null;
		Set<Map<String, Object>> response = new HashSet<>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		if (allRecords.size() > 0) {
			for (ClaimDocModel claim : allRecords) {

				try {
					variables = runtimeService.getVariables(claim.getProcessInstanceId());

				} catch (Exception ex) {
					logger.info("<==Claim saveDocs occur while get process variable==>" + ex);
					HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(claim.getProcessInstanceId()).includeProcessVariables().singleResult();
					variables = processInstance.getProcessVariables();
				}

				if (variables != null) {
					if (Utils
							.getStringValue(
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_APPLICATION_NUMBER))
							.equals(searchInputValue.toString())) {
						String policyNo = Utils.getStringValue(
								variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER));
						List<ClaimInfoModel> claimInfs = portalClaimInfoDao.findByPolicyNo(policyNo);
						if (claimInfs.size() > 0) {
							for (ClaimInfoModel claimInf : claimInfs) {
								Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_APPLICATION_NO,
										Utils.getStringValue(variables.get(
												PortalDashboardAuthenticationConstants.CLAIM_VAR_APPLICATION_NUMBER)));
								if (claimInf != null) {
									workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID,
											Utils.getStringValue(claimInf.getClaimsid()));

									workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
											Utils.getStringValue(claimInf.getClaimStatus()));
								} else {
									workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID,
											Utils.getStringValue(""));

									workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
											Utils.getStringValue("0"));
								}

								if (!Utils.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT))
										.isEmpty()) {
									workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT, Utils
											.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT)));
								} else {
									workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
											Utils.getStringValue("0"));
								}
								workFlowUserMap.put(Constants.POLICY_NUMBER, Utils.getStringValue(
										variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER)));

								workFlowUserMap.put(Constants.PRODUCT_NAME,
										variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PRODUCT_NAME));
								workFlowUserMap.put(Constants.PLAN_NAME,
										variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PLAN_NAME));
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_PROCESS_INSTANCE_ID,
										Utils.getStringValue(claim.getProcessInstanceId()));

								Date date = claim.getModified();
								String strDate = dateFormat.format(date);
								String splitDateTime[] = strDate.split(" ");
								String last_update = splitDateTime[0] + splitDateTime[0];
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_LAST_UPDATE,
										Utils.getStringValue(last_update));
								String action = "getClaimDetails/" + Utils.getStringValue(claim.getProcessInstanceId());
								workFlowUserMap.put(Constants.ACTION, action);
								response.add(workFlowUserMap);
							}
						} else {
							Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_APPLICATION_NO,
									Utils.getStringValue(variables
											.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_APPLICATION_NUMBER)));

							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_ID,
									Utils.getStringValue(""));

							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_STATUS,
									Utils.getStringValue("0"));

							if (!Utils.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT))
									.isEmpty()) {
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT, Utils
										.getStringValue(variables.get(Constants.CLAIM_PAYMENT_TOTAL_CLAIM_AMOUNT)));
							} else {
								workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_AMOUNT,
										Utils.getStringValue("0"));
							}
							workFlowUserMap.put(Constants.POLICY_NUMBER, Utils.getStringValue(
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_POLICY_NUMBER)));

							workFlowUserMap.put(Constants.PRODUCT_NAME,
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PRODUCT_NAME));
							workFlowUserMap.put(Constants.PLAN_NAME,
									variables.get(PortalDashboardAuthenticationConstants.CLAIM_VAR_PLAN_NAME));
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_PROCESS_INSTANCE_ID,
									Utils.getStringValue(claim.getProcessInstanceId()));

							Date date = claim.getModified();
							String strDate = dateFormat.format(date);
							String splitDateTime[] = strDate.split(" ");
							String last_update = splitDateTime[0] + splitDateTime[0];
							workFlowUserMap.put(PortalDashboardAuthenticationConstants.CLAIM_LAST_UPDATE,
									Utils.getStringValue(last_update));
							String action = "getClaimDetails/" + Utils.getStringValue(claim.getProcessInstanceId());
							workFlowUserMap.put(Constants.ACTION, action);
							response.add(workFlowUserMap);
						}
					}
				}

			}
			logger.info("<-- Exiting PortalDashboardService.getSearchClaimsList()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

	private boolean CheckToProcessProductSearch(String searchInputValue, WorkFlowApplication wFApplication) {
		boolean returnValue = false;
		if (searchInputValue.equalsIgnoreCase(Utils.getStringValue(wFApplication.getProductName()))) {
			returnValue = true;
		}

		if (Utils.getStringValue(wFApplication.getProductName()).toLowerCase()
				.contains(searchInputValue.toLowerCase())) {
			returnValue = true;
		}
		return returnValue;

	}

	private boolean CheckToProcessPlanSearch(String searchInputValue, WorkFlowPolicy claimPolicy) {
		boolean returnValue = false;
		if (searchInputValue.equalsIgnoreCase(Utils.getStringValue(claimPolicy.getPlanName()))) {
			returnValue = true;
		}

		if (Utils.getStringValue(claimPolicy.getPlanName()).toLowerCase().contains(searchInputValue.toLowerCase())) {
			returnValue = true;
		}
		return returnValue;
	}

//Customer Support search	
	public ApiResponse getSearchTicketList(String searchByOption, String searchInputValue) {
		logger.info("-->Entering PortalDashboardSearchService.getSearchCustomerList()");
		List<CustomerQuerySupport> allTickets = null;
		List<Map<String, Object>> response = new ArrayList<>();
		switch (searchByOption) {
		case Constants.CUSTOMER_SEARCH_BY_NAME:
			allTickets = portalDashboardChatSupportDao.findByUsernameContainingIgnoreCase(searchInputValue);
			break;
		case Constants.CUSTOMER_SEARCH_BY_MOBILE_NO:
			allTickets = portalDashboardChatSupportDao.findByCustomerMobileNumberContainingIgnoreCase(searchInputValue);
			break;
		case Constants.CUSTOMER_SEARCH_BY_EMAIL:
			allTickets = portalDashboardChatSupportDao.findByCustomerEmailContainingIgnoreCase(searchInputValue);
			break;

		case PortalDashboardAuthenticationConstants.SEARCH_BY_CUSTOMER_SUPPORT_TICKET_NUMBER:
			allTickets = portalDashboardChatSupportDao.findByTicketNumberContainingIgnoreCase(searchInputValue);
			break;
		case PortalDashboardAuthenticationConstants.SEARCH_BY_CUSTOMER_SUPPORT_TICKET_STATUS:
			allTickets = portalDashboardChatSupportDao.findByticketNumberStatusContainingIgnoreCase(searchInputValue);
			break;
		case PortalDashboardAuthenticationConstants.SEARCH_BY_CUSTOMER_SUPPORT_TOPIC:
			allTickets = portalDashboardChatSupportDao
					.findByCustomerQueryTopicDescriptionContainingIgnoreCase(searchInputValue);
			break;
		}

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		if (allTickets != null) {
			for (CustomerQuerySupport allTicket : allTickets) {
				Map<String, Object> workFlowUserMap = new HashMap<String, Object>();
				String newTicket = Utils.getStringValue(allTicket.getTicketNumberStatus());
				if ("NEW".equals(newTicket) || "OPEN".equals(newTicket)) {
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TICKET_NUMBER,
							Utils.getStringValue(allTicket.getTicketNumber()));
					String customerAction = "getTicketConversationDetails/"
							+ Utils.getStringValue(allTicket.getTicketNumber());
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_NAME,
							Utils.getStringValue(allTicket.getUsername()));
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_MOBILE_NO,
							Utils.getStringValue(allTicket.getCustomerMobileNumber()));
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_CUSTOMER_EMAIL,
							Utils.getStringValue(allTicket.getCustomerEmail()));
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_TOPIC,
							Utils.getStringValue(allTicket.getCustomerQueryTopicDescription()));
					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMERCUSTOMER_SUPPORT_TICKET_STATUS,
							Utils.getStringValue(allTicket.getTicketNumberStatus()));

					workFlowUserMap.put(PortalDashboardAuthenticationConstants.CUSTOMER_SUPPORT_LAST_UPDATE,
							Utils.getStringValue(""));
					workFlowUserMap.put(Constants.ACTION, customerAction);
					response.add(workFlowUserMap);
				}
			}
			logger.info("<-- Exiting PortalDashboardChatSupportService.getAllNewAndOpenTickets()");

			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, response);
		} else {
			return new ApiResponse(HttpStatus.UNAUTHORIZED, Constants.USER_DOES_NOT_EXIST_MESSAGE);
		}
	}

}
