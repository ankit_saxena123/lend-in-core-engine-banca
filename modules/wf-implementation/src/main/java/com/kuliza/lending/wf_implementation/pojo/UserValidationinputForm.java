package com.kuliza.lending.wf_implementation.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kuliza.lending.common.utils.Constants.DeviceType;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Value;

public class UserValidationinputForm {

	@NotNull
	String mobile;

	@NotNull(message = "OTP cannot be null")
	@Size(min = 4, max = 6)
	@Pattern(regexp = "[0-9]+")
	String otp;

	@NotNull(message = "Id type cannot be null")
	@Size(min = 1)
	String idType;

	String deviceId;

	DeviceType deviceType;

	// TODO - Discuss if needed
	// Boolean skipValidation;

	public UserValidationinputForm() {
		super();
	}

	public UserValidationinputForm(String mobile, String otp, String idType,
			String deviceId, DeviceType deviceType) {
		super();
		this.mobile = mobile;
		this.otp = otp;
		this.idType = idType;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
	}

	

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@JsonIgnore
	public String getOtp() {
		return otp;
	}

	@JsonProperty
	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	// public Boolean getSkipValidation() {
	// return skipValidation;
	// }
	//
	// public void setSkipValidation(Boolean skipValidation) {
	// this.skipValidation = skipValidation;
	// }


	@Override
	public String toString() {
		return "UserValidationinputForm{" +
				"username='" + mobile + '\'' +
				", idType='" + idType + '\'' +
				", deviceId='" + deviceId + '\'' +
				", deviceType=" + deviceType +
				'}';
	}
}
