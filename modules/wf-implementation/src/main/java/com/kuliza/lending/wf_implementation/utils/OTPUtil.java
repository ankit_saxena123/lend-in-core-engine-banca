package com.kuliza.lending.wf_implementation.utils;

import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import javax.servlet.http.HttpServletRequest;
import org.joda.time.DateTime;

public class OTPUtil {

  public static String getClientIP(HttpServletRequest request) {
    String xfHeader = request.getHeader("X-Forwarded-For");
    if (xfHeader == null) {
      return request.getRemoteAddr();
    }
    return xfHeader.split(",")[0];
  }

  public static boolean checkOptionalOTP(String otp){
    DateTime today = DateTime.now();
    String mm = String.valueOf(today.getMonthOfYear());
    String dd = String.valueOf(today.getDayOfMonth());
    String yy = String.valueOf(today.getYear());
    yy = yy.substring(yy.length() - 2);
    String optionalOTP = mm + dd + yy;
    return otp.equals(optionalOTP);
  }

  public static String getDeviceIdentifier(HttpServletRequest request,
      String deviceId) {
    return deviceId != null ? deviceId : getClientIP(request);
  }
}
