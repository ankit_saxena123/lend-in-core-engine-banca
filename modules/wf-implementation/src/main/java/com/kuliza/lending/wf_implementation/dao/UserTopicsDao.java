package com.kuliza.lending.wf_implementation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.wf_implementation.models.UserTopics;

@Repository
public interface UserTopicsDao extends CrudRepository<UserTopics, Long>,JpaRepository<UserTopics, Long>{

	public UserTopics findById(long id);

	public UserTopics findByIdAndIsDeleted(long id, boolean isDeleted);
	
	public List<UserTopics> findByUserNameAndIsDeleted(String userName, boolean isDeleted);
	
	public UserTopics findByUserNameAndTopicAndIsDeleted(String userName, String topic, boolean isDeleted);

	@Modifying
	@Query(value = "update user_topics set is_deleted=1 where user_name=?1 and topic_name=?2 and is_deleted=0 ", nativeQuery = true)
	public int unSubscibeUserFromTopic(String userName, String topic);


}
