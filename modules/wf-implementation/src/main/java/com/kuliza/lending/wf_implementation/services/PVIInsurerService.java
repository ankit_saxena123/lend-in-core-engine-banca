package com.kuliza.lending.wf_implementation.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.CMSIntegrationDao;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.InsurerInfoDao;
import com.kuliza.lending.wf_implementation.dao.UserTopicsDao;
import com.kuliza.lending.wf_implementation.dao.WorkFlowPolicyDAO;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.PVIInsurerIntegration;
import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;
import com.kuliza.lending.wf_implementation.models.UserTopics;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowPolicy;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.models.WorkFlowUserApplication;
import com.kuliza.lending.wf_implementation.notification.SNSEventSubscription;
import com.kuliza.lending.wf_implementation.notification.SNSParamInit;
import com.kuliza.lending.wf_implementation.pojo.PVIInsurerInput;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;

@Service
public class PVIInsurerService extends InsurerAbstract {

	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;
	@Autowired
	private InsurerInfoDao insurerInfoDao;
	@Autowired
	private CMSIntegrationDao cmsIntegrationDao;
	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;
	@Autowired
	private FECDMSService fecdmsService;

	@Autowired
	public SNSParamInit snsParamInit;

	@Autowired
	private PVIInsurerIntegration pviIntegration;

	@Autowired
	private WorkFlowPolicyDAO workFlowPolicyDAO;

	@Autowired
	private WfImplConfig wfImplConfig;

	@Autowired
	private SNSEventSubscription snsSubscription;

	@Autowired
	private UserTopicsDao userTopicsDao;

	@Autowired
	private WorkflowUserApplicationDao workflowUserApplicationDao;

	@Autowired
	private WorkflowUserDao workflowUserDao;

	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory.getLogger(PVIInsurerService.class);

	public boolean postPolicyDetails(String processInstanceId, Map<String, Object> variables, String paymentId,
			WorkFlowApplication workFlowApplication, IPATPaymentConfirmModel ipConfirmModel,
			CMSIntegration cmsIntegration) throws Exception {
		boolean status = false;
		try {
			logger.info(
					"<*********inside postVehiclePolicyDetails processInstanceId :" + processInstanceId + "**********");
			double thirdParty = 0;
			double driverLoss = 0;
			boolean insurerCallOne = false;
			boolean insurerCallTwo = false;
			String motorLoss = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			logger.info("<*********inside postVehiclePolicyDetails**********    " + motorLoss);
			List<WorkFlowPolicy> policyList = workFlowPolicyDAO
					.findByWorkFlowApplicationAndIsDeleted(workFlowApplication, false);
			Map<String, WorkFlowPolicy> policyMap = new HashMap<String, WorkFlowPolicy>();
			if (policyList != null) {
				for (WorkFlowPolicy workFlowPolicy : policyList) {
					policyMap.put(workFlowPolicy.getPlanId(), workFlowPolicy);
				}
			}
			paymentId = Utils.getStringValue(paymentId);
			logger.info("********paymentId******  " + paymentId);

			if (paymentId.isEmpty()) {

				logger.info("<=======>failed to get workFlowId and crmId for processId :" + processInstanceId);

				throw new Exception("failed to get workFlowId and crmId for processId :" + processInstanceId);
			}

			logger.info("--->>>>>>>>>>>>>>*********===------->>>> VARIABLES **************  " + variables);

			String planIds = Utils.getStringValue(variables.get("plan_id"));

			List<String> planIdList = Arrays.asList(planIds.split("\\s*,\\s*"));

			if (planIdList.contains(WfImplConstants.TW_PERSONAL_ACCIDENT_PLAN_ID)) {
				driverLoss = Utils.getDoubleValue(variables.get(WfImplConstants.PERSONAL_ACCIDENT_KEY));
				logger.info("<========>driverLoss :" + driverLoss + "<==========>");
			}

			for (String planId : planIdList) {
				if (planId.equals(WfImplConstants.TW_MOTOR_LOSS_PLAN_ID)) {
					WorkFlowPolicy workFlowPolicy = policyMap.get(WfImplConstants.TW_MOTOR_LOSS_PLAN_ID);
					insurerCallOne = callapi(processInstanceId, workFlowApplication, ipConfirmModel, cmsIntegration,
							variables, workFlowPolicy.getTransactionId(), paymentId,
							WfImplConstants.TW_MOTOR_LOSS_PLAN_ID, 0, 0, motorLoss, workFlowPolicy);

				} else if (planId.equals(WfImplConstants.TW_THIRD_PARTY_PLAN_ID)) {
					WorkFlowPolicy workFlowPolicy = policyMap.get(WfImplConstants.TW_THIRD_PARTY_PLAN_ID);
					thirdParty = Utils.getDoubleValue(variables.get(WfImplConstants.THIRD_PARTY_LIABILITY_KEY));
					logger.info("<========>thirdParty :" + thirdParty + "<==========>");

					insurerCallTwo = callapi(processInstanceId, workFlowApplication, ipConfirmModel, cmsIntegration,
							variables, workFlowPolicy.getTransactionId(), paymentId,
							WfImplConstants.TW_THIRD_PARTY_PLAN_ID, thirdParty, driverLoss, motorLoss, workFlowPolicy);

				}
			}
			if (insurerCallOne && insurerCallTwo) {
				status = true;
			}
			if (status) {
				try {
					WorkFlowUserApplication wfUserapp = workflowUserApplicationDao
							.findByProcInstIdAndIsDeleted(processInstanceId, false);
					WorkFlowUser wfUser = workflowUserDao.findByIdmUserNameAndIsDeleted(wfUserapp.getUserIdentifier(),
							false);
					String subscriberArn = snsSubscription.subscribeToTopic(snsParamInit.getTwTopicARN(), "application",
							wfUser.getArnEndPoint());
					if (subscriberArn!=null) {
						UserTopics userTopic = new UserTopics();
						userTopic.setTopic(WfImplConstants.TW_TOPIC);
						userTopic.setUserName(wfUser.getIdmUserName());
						userTopic.setSubscriberArn(subscriberArn);
						userTopic.setWfUser(wfUser);
						userTopicsDao.save(userTopic);
					} else {
						logger.info("<==Subscription To TW Topic is Failed For User==>" + wfUser.getIdmUserName());
					}
				} catch (Exception ex) {
					logger.info("<==Subscription To TW Topic is Failed For User==>" + ex);

				}
			}
			logger.info("<*********Exiting PVIServiceTask**********");
		} catch (Exception e) {
			logger.error("unable to call insurer api :" + e.getMessage(), e);
			status = false;
		}
		return status;
	}

	public boolean callapi(String processInstanceId, WorkFlowApplication workFlowApp,

			IPATPaymentConfirmModel ipatConfirm, CMSIntegration cmsIntegration, Map<String, Object> variables,
			String crmId, String appNum,

			String plan, double thirdParty, double driverLoss, String motorLoss, WorkFlowPolicy workFlowPolicy)
			throws Exception {

		logger.info("<==========>inside callapi()<==========>");
		String apiResponse = null;
		boolean status = false;
		String apiRequest = null;
		try {
			JSONObject requestPayload = pviIntegration.createRequestPayload(variables, crmId, appNum, plan, thirdParty,
					driverLoss, motorLoss);
			apiRequest = Utils.getStringValue(requestPayload);
			JSONObject responseAsJson = pviIntegration.getDataFromIb(requestPayload);

			HashMap<String, Object> responseMap = pviIntegration.parseResponse(responseAsJson);

			boolean Success = (boolean) responseMap.get(Constants.PVIINSURER_RESPONSE_STATUS);

			logger.info("--->>>>>>>>>>>>>> responseMAP json outside**************  " + responseMap);

			if (Success) {

				logger.info("--->>>>>>>>>>>>>> responseAs json inside  " + responseAsJson);

				apiResponse = Utils.getStringValue(responseAsJson);

				logger.info("api response to be saved*****************    " + apiResponse);

				status = true;
				logger.info("<=======> PVI  Insurer api is Sucessfull <=======>");

			} else {
				logger.info("<=======> PVI  Insurer api is failed <=======>");

				apiResponse = Utils.getStringValue(responseAsJson);

				logger.info("--->>>>>>>>>>>>>> inside else api response  " + apiResponse);

			}

			if (status) {
				if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
					logger.info("<=======> sms will be send for PVI Insurer <=======>");
					super.sendSms(variables);
				}
			}

		} catch (Exception e) {
			logger.error("unable to call PVIinsurer api :" + e.getMessage(), e);
		} finally {
			persisVehicletPolicyInfo(processInstanceId, workFlowApp, ipatConfirm, cmsIntegration, status, apiResponse,
					variables, workFlowPolicy, apiRequest);

		}

		return status;

	}

	public void schedulerInsurer(String processInstanceId, String requestBody, InsurerInfoModel insurerInfo, int count,
			Map<String, Object> variables) {
		logger.info("<=====> INSIDE schedulerInsurer******************");
		boolean status = false;
		String apiResponse = null;

		try {
			count = count + 1;
			JSONObject payload = new JSONObject(requestBody);
			status = true;
			JSONObject responseAsJson = pviIntegration.getDataFromIb(payload);
			apiResponse = responseAsJson.toString();
			updatePersistPolicyInfo(status, apiResponse, payload, insurerInfo, count);
			if (status) {
				if (!wfImplConfig.getEnv().equalsIgnoreCase("uat")) {
					super.sendSms(variables);
//					super.sendEmail(variables, processInstanceId);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void updatePersistPolicyInfo(boolean status, String apiResponse, Object payload,
			InsurerInfoModel insurerInfo, int count) {
		String apiRequest = Utils.getStringValue(payload);
		insurerInfo.setApiResponse(apiResponse);
		insurerInfo.setSuccess(status);
		insurerInfo.setApiRequest(apiRequest);
		insurerInfo.setSchedularCount(count);
		insurerInfoDao.save(insurerInfo);

	}

	private void persisVehicletPolicyInfo(String processInstanceId, WorkFlowApplication workFlowApp,
			IPATPaymentConfirmModel ipatConfirm, CMSIntegration cmsIntegration, boolean insurerStatus,
			String apiResponse, Map<String, Object> variables, WorkFlowPolicy workFlowPolicy, String apiRequest) {
		logger.info("********* inside persisVehicletPolicyInfo *********");
		persisInsurerInfo(processInstanceId, ipatConfirm, cmsIntegration, insurerStatus, apiResponse, apiRequest);
		ifInsurerStatusTrue(processInstanceId, workFlowApp, insurerStatus, variables, workFlowPolicy);
		logger.info("***********Vehicle policy is saved SucessFully**********");
	}

	private void persisInsurerInfo(String processInstanceId, IPATPaymentConfirmModel ipatConfirm,
			CMSIntegration cmsIntegration, boolean insurerStatus, String apiResponse, String apiRequest) {
		InsurerInfoModel insurerInfo = new InsurerInfoModel();
		insurerInfo.setApiResponse(apiResponse);
		insurerInfo.setiPatPaymentConfirm(ipatConfirm);
		insurerInfo.setCmsPaymentConfirm(cmsIntegration);
		insurerInfo.setInsurerName(WfImplConstants.PVI_INSURER);
		insurerInfo.setiPatPaymentConfirm(ipatConfirm);
		insurerInfo.setProcessInstanceId(processInstanceId);
		insurerInfo.setSuccess(insurerStatus);
		insurerInfo.setApiRequest(apiRequest);
		insurerInfoDao.save(insurerInfo);
	}

	private void ifInsurerStatusTrue(String processInstanceId, WorkFlowApplication workFlowApp, boolean insurerStatus,
			Map<String, Object> variables, WorkFlowPolicy workFlowPolicy) {
		if (insurerStatus) {
			String sumInsured = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT));
			String premium = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_MONTHLY_PAYMENT));
			String paymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PAYMENT_START_DATE));
			String endDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
			String nextPaymentDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_NEXT_PAYMENT_DUE));
			storeDetailsInWorkFlowAppAndWorkFlowPolicy(workFlowApp, workFlowPolicy, sumInsured, premium, paymentDate,
					endDate, nextPaymentDate);
			String dmsDocId = Utils.getStringValue(workFlowApp.getDmsDocId());
			logger.info("<=======> email will be send for PVI Insurer  <=======>");
			super.sendEmail(variables, processInstanceId, dmsDocId);
		}
	}

	private void storeDetailsInWorkFlowAppAndWorkFlowPolicy(WorkFlowApplication workFlowApp,
			WorkFlowPolicy workFlowPolicy, String sumInsured, String premium, String paymentDate, String endDate,
			String nextPaymentDate) {
		workFlowApp.setIs_purchased(true);
		workFlowApp.setProtectionAmount(sumInsured);
		workFlowApp.setMontlyPayment(premium);
		workFlowApp.setPlanExpiringDate(endDate);
		workFlowApp.setNextPaymentDue(nextPaymentDate);
		workFlowApp.setApplicationDate(paymentDate);
		workFlowPolicy.setMontlyPayment(premium);
		workFlowPolicy.setPlanExpiringDate(endDate);
		workFlowPolicy.setNextPaymentDue(nextPaymentDate);
		workFlowPolicyDAO.save(workFlowPolicy);
		workflowApplicationDao.save(workFlowApp);
	}

	public ApiResponse pviInsurerConfirm(PVIInsurerInput pviInsurerInput) throws Exception {
		logger.info("pviInsurerRequest---> : "+pviInsurerInput.toString());
		logger.info("<<<<<<<<<<<pviInsurerConfirm()>>>>>>>>>>>>>");
		String transactionID = pviInsurerInput.getTransactionID();
		String refernceNumber = pviInsurerInput.getReferenceNumber();
		String policyType = pviInsurerInput.getPolicyType();
		String policyNo = pviInsurerInput.getPolicyNo();
		String pdfLink = pviInsurerInput.getPolicyDocumentPDF();
		logger.info("<=======>TransactionId :" + transactionID + "=====ReferenceNumber: " + refernceNumber
				+ "=====PolicyType :" + policyType + "<====>PolicyNo :" + policyNo + " pdf Link :" + pdfLink);
		WorkFlowApplication workFlowApp = checkPaymentModeAndReturnWorkFlowApp(refernceNumber);
		return savingDataIntoDbTables(pviInsurerInput, transactionID, policyType, pdfLink, workFlowApp);

	}

	private WorkFlowApplication checkPaymentModeAndReturnWorkFlowApp(String refernceNumber) {
		WorkFlowApplication workFlowApp = null;
		IPATPaymentConfirmModel ipatConfirmPaymentModel = ipatPaymentConfirmDao.findByCrmIdAndIsDeleted(refernceNumber,
				false);
		// checking whether is payment is done through ipat.
		if (ipatConfirmPaymentModel != null) {
			workFlowApp = ipatConfirmPaymentModel.getWfApplication();
		} else {
			// if payment is not done through ipat , checking whether payment is done
			// through cms.
			CMSIntegration cmsModel = cmsIntegrationDao.findBytransactionIdAndIsDeleted(refernceNumber, false);
			if (cmsModel != null)
				workFlowApp = cmsModel.getWfApplication();
		}
		return workFlowApp;
	}

	private ApiResponse savingDataIntoDbTables(PVIInsurerInput pviInsurerInput, String transactionID, String policyType,
			String pdfLink, WorkFlowApplication workFlowApp) throws Exception {
		logger.info("savingDataIntoDbTables--->pviInsurerRequest---> : "+pviInsurerInput.toString());
		if (workFlowApp != null) {
			return getWorkFlowPolicyObjectAndSavePolicyDetails(pviInsurerInput, transactionID, policyType, pdfLink,
					workFlowApp);
		} else {
			logger.error("referenceNumber is Invalid");
			return new ApiResponse(HttpStatus.BAD_REQUEST, "referenceNumber is Invalid", null);
		}
	}

	private ApiResponse getWorkFlowPolicyObjectAndSavePolicyDetails(PVIInsurerInput pviInsurerInput,
			String transactionID, String policyType, String pdfLink, WorkFlowApplication workFlowApp) throws Exception {
		logger.info("getWorkFlowPolicyObjectAndSavePolicyDetails--->pviInsurerRequest---> : "+pviInsurerInput.toString());
		WorkFlowPolicy workflowPolicy = workFlowPolicyDAO.findByTransactionIdAndIsDeleted(transactionID, false);
		if (workflowPolicy != null) {
			savePolicyDetailsForTWAndOtherPVIProducts(pviInsurerInput, policyType, pdfLink, workFlowApp,
					workflowPolicy);
			logger.info("<=========>policy deatils are saved<============>");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, null);
		} else {
			logger.error("transactionId is Invalid");
			return new ApiResponse(HttpStatus.BAD_REQUEST, "transactionId is Invalid", null);
		}
	}

	private void savePolicyDetailsForTWAndOtherPVIProducts(PVIInsurerInput pviInsurerInput, String policyType,
			String pdfLink, WorkFlowApplication workFlowApp, WorkFlowPolicy workflowPolicy) throws Exception {
		logger.info("savePolicyDetailsForTWAndOtherPVIProducts ---> pviInsurerRequest---> : "+pviInsurerInput.toString());
		if (policyType.trim().equalsIgnoreCase("ThirdPartyLiability")
				|| policyType.trim().equalsIgnoreCase("DriverLoss")) {
			logger.info("<======> Policy Type is : ThirdPartyLiability || DriverLoss");
			workflowPolicy.setPolicyNumber(pviInsurerInput.getPolicyNo());
			workFlowPolicyDAO.save(workflowPolicy);
		} else {
			checkPdfIsNotNullAndUploadPdfToDms(pdfLink, workFlowApp, workflowPolicy);
			savePolicyDetailsInWorkFlowPolicyAndWorkFlowApp(pviInsurerInput, workFlowApp, workflowPolicy);
		}
	}

	private void savePolicyDetailsInWorkFlowPolicyAndWorkFlowApp(PVIInsurerInput pviInsurerInput,
			WorkFlowApplication workFlowApp, WorkFlowPolicy workflowPolicy) {
		workflowPolicy.setPolicyNumber(pviInsurerInput.getPolicyNo());
		workFlowPolicyDAO.save(workflowPolicy);
		workFlowApp.setPolicyNumber(pviInsurerInput.getPolicyNo());
		workflowApplicationDao.save(workFlowApp);
	}

	private void checkPdfIsNotNullAndUploadPdfToDms(String pdfLink, WorkFlowApplication workFlowApp,
			WorkFlowPolicy workflowPolicy) throws Exception {
		if (pdfLink != null && !pdfLink.isEmpty()) {
			logger.info("<=================>appId: " + workFlowApp.getApplicationId() + "<==============>");
			String dmsId = fecdmsService.uploadPDftoDMS(workFlowApp.getApplicationId(), pdfLink);
			logger.info("dmsId--->: " + dmsId);
			workflowPolicy.setDmsDocId(dmsId);
			workFlowApp.setDmsDocId(dmsId);
			autoTriggerEmail(workFlowApp);
		}
	}

	private void autoTriggerEmail(WorkFlowApplication workFlowApp) {
		logger.info("processInstanceId---> " + workFlowApp.getProcessInstanceId());
		HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceId(workFlowApp.getProcessInstanceId()).includeProcessVariables().singleResult();
		Map<String, Object> variables = historicProcessInstance.getProcessVariables();
		variables.forEach((key, value) -> {
			logger.info("Key : " + key + " Value : " + value);
		});
		logger.info("processInstanceId---> " + workFlowApp.getProcessInstanceId() + "<--dmsDocId---> "
				+ workFlowApp.getDmsDocId());
		super.sendEmail(variables, workFlowApp.getProcessInstanceId(), workFlowApp.getDmsDocId());
	}

}
