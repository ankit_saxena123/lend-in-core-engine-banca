package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class EmailInputForm {
	
	@NotNull(message = "userEmail cannot be null")
	@Pattern(regexp = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$", message = "Not a valid emailID")
	private String userEmail;
	@NotNull(message = "dmsDocId cannot be null")
	private String dmsDocId;
	@NotNull(message = "processInstanceId cannot be null")
	private String processInstanceId;
	private String language;
	
	
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getDmsDocId() {
		return dmsDocId;
	}
	public void setDmsDocId(String dmsDocId) {
		this.dmsDocId = dmsDocId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	

}
