package com.kuliza.lending.wf_implementation.dao;

import com.kuliza.lending.wf_implementation.models.OTPDetails;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//@ConditionalOnExpression("${otp.useCacheForOTP:false}")
@Repository
@Transactional
public interface OTPDetailsDao extends JpaRepository<OTPDetails, Long> {

  OTPDetails findById(long id);

  OTPDetails findByIdAndIsDeleted(long id, boolean isDeleted);

  List<OTPDetails> findByUserIdentifierAndDeviceIdentifierAndIsDeletedOrderByIdDesc(String userIdentifier,
      String deviceIdentifier, boolean isDeleted);

  List<OTPDetails> findByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndIsDeleted(
      String userIdentifier, String deviceIdentifier,
      boolean blockedOnGenerate, boolean isDeleted);

  List<OTPDetails> findByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
      String userIdentifier, String deviceIdentifier,
      boolean blockedOnGenerate, boolean blockedOnValidate, boolean isDeleted);

  List<OTPDetails> findByUserIdentifierAndDeviceIdentifierAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
      String userIdentifier, String deviceIdentifier,
      boolean blockedOnValidate, boolean isDeleted);

  OTPDetails findTopByUserIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
      String userIdentifier, boolean blockedOnGenerate, boolean blockedOnValidate, boolean isDeleted);

  OTPDetails findTopByUserIdentifierAndDeviceIdentifierAndBlockedOnGenerateAndBlockedOnValidateAndIsDeletedOrderByIdDesc(
      String userIdentifier, String deviceIdentifier, boolean blockedOnGenerate, boolean blockedOnValidate, boolean isDeleted);

  OTPDetails findTopByUserIdentifierAndIsDeletedOrderByIdDesc(String userIdentifier,
      boolean isDeleted);
}
