package com.kuliza.lending.wf_implementation.services;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.kuliza.lending.wf_implementation.dao.WorkFlowApplicationDocsDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowApplicationDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECreditBancaSMSIntegration;
import com.kuliza.lending.wf_implementation.models.CMSIntegration;
import com.kuliza.lending.wf_implementation.models.IPATPaymentConfirmModel;
import com.kuliza.lending.wf_implementation.models.InsurerInfoModel;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplication;
import com.kuliza.lending.wf_implementation.models.WorkFlowApplicationDocs;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;
import com.kuliza.lending.wf_implementation.utils.WfImplConstants;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

public abstract class InsurerAbstract {

	@Autowired
	private FECreditBancaSMSIntegration feCreditBancaSMSIntegration;

	@Autowired
	private FECreditBancaNotifyUserServices feCreditBancaNotifyUserServices;

	@Autowired
	private WorkflowUserDao workflowuserDao;

	@Autowired
	private WorkflowApplicationDao workflowApplicationDao;

	@Autowired
	private WorkFlowApplicationDocsDao workflowApplicationDocsDao;

	private static final Logger logger = LoggerFactory.getLogger(InsurerAbstract.class);

	public abstract boolean postPolicyDetails(String processInstanceId, Map<String, Object> variables, String paymentId,
			WorkFlowApplication workflowApp, IPATPaymentConfirmModel ipatPaymentConfim, CMSIntegration cmsIntegration)
			throws Exception;

	public abstract void schedulerInsurer(String processInstanceId, String requestBody, InsurerInfoModel insurerInfo,
			int count, Map<String, Object> variables);

	public void sendEmail(Map<String, Object> variables, String processInstanceId, String dmsDocId) {
		try {
			String userName = Utils.getStringValue(
					variables.get(com.kuliza.lending.common.utils.Constants.PROCESS_TASKS_ASSIGNEE_KEY));
			String language = Utils.getStringValue(variables.get(Constants.LANGUAGE_KEY));
			logger.info("<=========>language:" + language);
			String userEmail = null;
			WorkFlowUser workFlowUser = workflowuserDao.findByIdmUserNameAndIsDeleted(userName, false);
			if (workFlowUser != null) {
				userEmail = Utils.getStringValue(variables.get(WfImplConstants.APPLICANT_EMAIL_USER));
				logger.info("InsurerAbstract ---> userEmail --->" + userEmail);

				if (userEmail != null && !userEmail.isEmpty()) {
					logger.info("<============================>Email will be triggered for the user:: " + userEmail);
					WorkFlowApplication workFlowApplication = workflowApplicationDao
							.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
					logger.info("email triggering for product name : "+workFlowApplication.getProductName());
					if(workFlowApplication.getProductName().equals(Constants.TL_PRODUCT_NAME)) {
						logger.info("mail trigger startng for Termlife product");
						List<WorkFlowApplicationDocs> workFlowApplicationDocsList =	workflowApplicationDocsDao.findListByWorkFlowApplicationAndDocType(workFlowApplication,Constants.DocType.ESIGN_FPT.getDocTypeId());
						
						if (workFlowApplicationDocsList != null && !workFlowApplicationDocsList.isEmpty()) {
							for (WorkFlowApplicationDocs workFlowApplicationDocs : workFlowApplicationDocsList) {
								feCreditBancaNotifyUserServices.sendEmail(processInstanceId, userName, language, userEmail, workFlowApplicationDocs.getDocId());
							}
						}
					}else {
						feCreditBancaNotifyUserServices.sendEmail(processInstanceId, userName, language, userEmail, dmsDocId);
					}
						
					
				} else {
					logger.info("<=============> UserEmail is not present <===========>");
				}
			} else {
				logger.info("<=============> User does not exist <===========>");

				throw new UsernameNotFoundException("User does not exist");
			}
			logger.info("userName:" + userName + ",language:" + language);
			logger.info("sendEmail called for dmsDocId : " + dmsDocId + ", processInstanceId : " + processInstanceId);
			getWorkFlowUserObject(variables, processInstanceId, dmsDocId, userName, language);
		} catch (Exception e) {
			logger.error("Unable to send email :", e);

		}

	}

	private void getWorkFlowUserObject(Map<String, Object> variables, String processInstanceId, String dmsDocId,
			String userName, String language) {
		String userEmail;
		WorkFlowUser workFlowUser = workflowuserDao.findByIdmUserNameAndIsDeleted(userName, false);
		if (workFlowUser != null) {
			userEmail = Utils.getStringValue(variables.get(WfImplConstants.APPLICANT_EMAIL_USER));
			logger.info(
					"userEmail " + userEmail + " ,processInstanceId : " + processInstanceId + ",dmsDocId :" + dmsDocId);
			getWorkFlowApplicationObject(processInstanceId, dmsDocId, userName, language, userEmail);
		} else {
			logger.info("<=============> User does not exist <===========>");
			throw new UsernameNotFoundException("User does not exist");
		}
	}

	private void getWorkFlowApplicationObject(String processInstanceId, String dmsDocId, String userName,
			String language, String userEmail) {
		if (userEmail != null && !userEmail.isEmpty()) {
			logger.info("Email will be triggered for the user:: " + userEmail + " ," + processInstanceId);
			WorkFlowApplication workFlowApplication = getWorkFlowAppAndcheckDocIdPresentOrNotInWorkFlowApp(
					processInstanceId);
			logger.info("email triggering for product name : " + workFlowApplication.getProductName());
			sentEmail(processInstanceId, dmsDocId, userName, language, userEmail, workFlowApplication);
		} else {
			logger.info("<=============> UserEmail is not present <===========>");
		}
	}

	private WorkFlowApplication getWorkFlowAppAndcheckDocIdPresentOrNotInWorkFlowApp(String processInstanceId) {
		WorkFlowApplication workFlowApplication = workflowApplicationDao
				.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if (workFlowApplication.getDmsDocId() != null && !workFlowApplication.getDmsDocId().isEmpty()) {
			logger.error("invalid doc id saved " + workFlowApplication.getDmsDocId());
		}
		return workFlowApplication;
	}

	private void sentEmail(String processInstanceId, String dmsDocId, String userName, String language,
			String userEmail, WorkFlowApplication workFlowApplication) {
		if (workFlowApplication.getProductName().equals(Constants.TL_PRODUCT_NAME) || workFlowApplication
				.getProductName().equals(Constants.engToViatnamMap.get(Constants.TL_PRODUCT_NAME))) {
			getWorkFlowApplicationDocsList(processInstanceId, userName, language, userEmail, workFlowApplication);
		} else {
			feCreditBancaNotifyUserServices.sendEmail(processInstanceId, userName, language, userEmail, dmsDocId);
		}
	}

	private void getWorkFlowApplicationDocsList(String processInstanceId, String userName, String language,
			String userEmail, WorkFlowApplication workFlowApplication) {
		logger.info("mail trigger startng for Termlife product");
		List<WorkFlowApplicationDocs> workFlowApplicationDocsList = workflowApplicationDocsDao
				.findListByWorkFlowApplicationAndDocType(workFlowApplication,
						Constants.DocType.ESIGN_FPT.getDocTypeId());
		getDocIdFromWorkFlowApplicationDocsListAndSendEmail(processInstanceId, userName, language, userEmail,
				workFlowApplicationDocsList);
	}

	private void getDocIdFromWorkFlowApplicationDocsListAndSendEmail(String processInstanceId, String userName,
			String language, String userEmail, List<WorkFlowApplicationDocs> workFlowApplicationDocsList) {
		if (workFlowApplicationDocsList != null && !workFlowApplicationDocsList.isEmpty()) {
			for (WorkFlowApplicationDocs workFlowApplicationDocs : workFlowApplicationDocsList) {
				feCreditBancaNotifyUserServices.sendEmail(processInstanceId, userName, language, userEmail,
						workFlowApplicationDocs.getDocId());
			}
		}
	}

	public String decode(String str) throws Exception {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	public void sendSms(Map<String, Object> variables) {
		try {
			Map<String, Object> requestMap = buildRequestPayloadForSms(variables);
			JSONObject request = feCreditBancaSMSIntegration.buildRequestPayload(requestMap);
			HttpResponse<JsonNode> apiResponse = feCreditBancaSMSIntegration.getDataFromService(request);
			logger.info("<===========> sms trigger apiResponse <===========>" + apiResponse);
			if (apiResponse != null) {
				Map<String, Object> parseResponse = feCreditBancaSMSIntegration
						.parseResponse(apiResponse.getBody().getObject());
				logger.info("<===========> sms send sucessfully<===========>" + parseResponse);
				logger.info("<===========> sms parseResponse <===========>" + parseResponse);
			} else {
				logger.error("<=== failed to send sms ===>");
			}
		} catch (Exception e) {
			logger.error("<===========> error while sending sms <===========>" + e.getMessage());
		}
	}

	private Map<String, Object> buildRequestPayloadForSms(Map<String, Object> variables) throws Exception {
		Map<String, Object> requestMap = new HashMap<String, Object>();
		String mobileNumber = Utils.getStringValue(variables.get(WfImplConstants.APPLICATION_MOBILE_NUMBER));
		logger.info("<===========> mobileNumber <===========>" + mobileNumber);
		String expiredDate = Utils.getStringValue(variables.get(WfImplConstants.IPAT_PLAN_EXPIRING_DATE));
		String protectionAmount = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PROTECTION_AMOUNT))
				.replace("triệu", "trieu");
		logger.info("<===========> protectionAmount <===========>" + protectionAmount);
		String productName = Utils.getStringValue(variables.get(WfImplConstants.JOURNEY_PRODUCT_NAME));
		String pdfLink = Utils.getStringValue(variables.get(Constants.INSURER_RESPONSE_PDFLINK));
		if ("" == pdfLink)
			pdfLink = "https://fecbancainsurance.onelink.me/hkNw/5136f34d";
		logger.info("<===========> pdflink <===========>" + pdfLink);
		String content = Constants.SMS_CONTENT;
		if (mobileNumber != null)
			mobileNumber = mobileNumber.trim();
		if (productName != null)
			productName = productName.trim();
		if (protectionAmount != null)
			protectionAmount = protectionAmount.trim();
		if (expiredDate != null)
			expiredDate = expiredDate.trim();
		if (pdfLink != null)
			pdfLink = pdfLink.trim();
		content = content.replace("${policy_name}", productName).replace("${sum_insured}", protectionAmount)
				.replace("${expiry_date}", expiredDate).replace("${Link_to_Shield}", pdfLink);
		content = decode(content);
		logger.info("<===========> SMSContent <===========>" + content);

		if (mobileNumber.startsWith("+84")) {
			mobileNumber = mobileNumber.substring(3, mobileNumber.length());
		}
		logger.info("<===========> mobileNumber <===========>" + mobileNumber);
		requestMap.put(Constants.MOBILE_NUMBER_KEY, mobileNumber);
		requestMap.put("message", content);
		return requestMap;
	}
}
