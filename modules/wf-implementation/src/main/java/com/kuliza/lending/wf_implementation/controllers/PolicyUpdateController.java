package com.kuliza.lending.wf_implementation.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.pojo.UpdatePolicy;
import com.kuliza.lending.wf_implementation.services.PolicyUpdateAutomationService;
import com.kuliza.lending.wf_implementation.utils.Constants;

@RestController
@RequestMapping("/policy")
public class PolicyUpdateController {

	private static final Logger logger = LoggerFactory.getLogger(PolicyUpdateController.class);

	@Autowired
	private PolicyUpdateAutomationService policyUpdate;

	@PostMapping("/save")
	public ResponseEntity<Object> updatePolicy(@Valid @RequestBody UpdatePolicy policyDetails) {
			logger.info("======>>>>>>>>updatePolicy Controller====>>>>: " + policyDetails );
		try {
			return CommonHelperFunctions.buildResponseEntity(policyUpdate.updatePolicy(policyDetails));

		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return CommonHelperFunctions.buildResponseEntity(
					(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE)));
		}

	}

}
