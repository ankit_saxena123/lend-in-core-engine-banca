package com.kuliza.lending.wf_implementation.pojo;

public class FPTRequest {
	
	String personalID;
	String personalName;
	String location;
	String stateProvince;
	String country;
	String agreementUUID;
	String certificateProfile;
	String authorizationEmail;
	String authorizationMobileNo;
	
	private String authorizeCode;
	private String billCode;
	

	public String getPersonalID() {
		return personalID;
	}

	public void setPersonalID(String personalID) {
		this.personalID = personalID;
	}

	public String getPersonalName() {
		return personalName;
	}

	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCertificateProfile() {
		return certificateProfile;
	}

	public void setCertificateProfile(String certificateProfile) {
		this.certificateProfile = certificateProfile;
	}

	public String getAuthorizationEmail() {
		return authorizationEmail;
	}

	public void setAuthorizationEmail(String authorizationEmail) {
		this.authorizationEmail = authorizationEmail;
	}

	public String getAuthorizationMobileNo() {
		return authorizationMobileNo;
	}

	public void setAuthorizationMobileNo(String authorizationMobileNo) {
		this.authorizationMobileNo = authorizationMobileNo;
	}

	public String getAgreementUUID() {
		return agreementUUID;
	}

	public void setAgreementUUID(String agreementUUID) {
		this.agreementUUID = agreementUUID;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAuthorizeCode() {
		return authorizeCode;
	}

	public void setAuthorizeCode(String authorizeCode) {
		this.authorizeCode = authorizeCode;
	}

	public String getBillCode() {
		return billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	@Override
	public String toString() {
		return "CustomerData [personalID=" + personalID + ", personalName=" + personalName + ", stateProvince="
				+ stateProvince + ", country=" + country + ", agreementUUID=" + agreementUUID + ", certificateProfile="
				+ certificateProfile + ", authorizationEmail=" + authorizationEmail + ", authorizationMobileNo="
				+ authorizationMobileNo + ", location=" + location + "]";
	}
	
	
	


}
