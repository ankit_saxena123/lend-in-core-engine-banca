package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.utils.Constants.SourceType;
import com.kuliza.lending.journey.model.AbstractWorkFlowSource;

@Entity
@Table(name = "workflow_source")
public class WorkFlowSource extends AbstractWorkFlowSource {

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = false)
	private WorkFlowUserApplication wfUserApp;

	public WorkFlowSource(String sourceId, SourceType sourceType, String sourceName,
			WorkFlowUserApplication wfUserApp) { 
		super(sourceId, sourceType, sourceName);
		this.wfUserApp = wfUserApp;
	}
	
	public WorkFlowSource() { 
		super();
	}

	public WorkFlowUserApplication getWfUserApp() {
		return wfUserApp;
	}

	public void setWfUserApp(WorkFlowUserApplication wfUserApp) {
		this.wfUserApp = wfUserApp;
	}
	
//	Add more fields to this table if required in the format given below
	
//	@Column(nullable = true)
//	private String destinationType;	
//
//	public WorkFlowSource() {
//		super();
//		this.setIsDeleted(false);
//	}
//
//	public WorkFlowSource(String sourceId, String sourceType,
//			String sourceName, String destinationType) {
//		super();
//		this.setSourceId(sourceId);
//		this.setSourceType(sourceType);
//		this.setSourceName(sourceName);
//		this.destinationType = destinationType;
//	}
//
//	public String getDestinationType() {
//		return destinationType;
//	}
//
//	public void setDestinationType(String destinationType) {
//		this.destinationType = destinationType;
//	}
	
}
