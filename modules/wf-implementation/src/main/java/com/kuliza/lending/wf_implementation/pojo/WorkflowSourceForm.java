package com.kuliza.lending.wf_implementation.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.kuliza.lending.common.utils.Constants.SourceType;


public class WorkflowSourceForm {

	@NotNull(message = "Source id cannot be null")
	String id;

	@NotNull(message = "Source type cannot be null")
	@Size(min = 1)
	SourceType type;

	@NotNull(message = "Source name cannot be null")
	String name;

	Boolean copyFromSource;

	public WorkflowSourceForm() {
		super();
	}


	public WorkflowSourceForm(String id, SourceType type, String name,
			Boolean copyFromSource) {
		super();
		this.id = id;
		this.type = type;
		this.name = name;
		this.copyFromSource = copyFromSource;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SourceType getType() {
		return type;
	}

	public void setType(SourceType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Boolean getCopyFromSource() {
		return copyFromSource;
	}

	public void setCopyFromSource(Boolean copyFromSource) {
		this.copyFromSource = copyFromSource;
	}

}
