package com.kuliza.lending.wf_implementation.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "wf_user_address")
public class WorkFlowUserAddress extends BaseModel{

	@Column(nullable = true, name="address_type")
	private String addressType;

	@Column(nullable = true, name="address_line1")
	private String addressLine1;

	@Column(nullable = true, name="address_line2")
	private String addressLine2;

	@Column(nullable = true, name="city_district")
	private String cityDistrict;

	@Column(nullable = true)
	private String province;
	
	@Column(nullable = true)
	private String state;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wf_user_id", nullable = false)
	private WorkFlowUser wfUser;

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCityDistrict() {
		return cityDistrict;
	}

	public void setCityDistrict(String cityDistrict) {
		this.cityDistrict = cityDistrict;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public WorkFlowUser getWfUser() {
		return wfUser;
	}

	public void setWfUser(WorkFlowUser wfUser) {
		this.wfUser = wfUser;
	}
	
	
	
}
