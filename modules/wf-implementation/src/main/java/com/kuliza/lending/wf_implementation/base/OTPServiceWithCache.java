package com.kuliza.lending.wf_implementation.base;

import static com.kuliza.lending.wf_implementation.utils.OTPUtil.checkOptionalOTP;

import com.kuliza.lending.authorization.config.keycloak.Secret;
import com.kuliza.lending.authorization.config.keycloak.TOTPManager;
import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.engine_common.configs.OTPConfig;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.pojo.UserPasswordGenerationForm;
import com.kuliza.lending.wf_implementation.pojo.UserValidationinputForm;
import com.kuliza.lending.wf_implementation.utils.OTPConstants;
import com.kuliza.lending.wf_implementation.utils.OTPUtil;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kuliza.lending.common.pojo.ApiResponse;


@Service
public class OTPServiceWithCache {

  @Autowired
  private OTPConfig otpConfig;

  @Autowired
  private WfImplConfig wfImplConfig;

  private TOTPManager manager;

  private LoadingCache<String, Integer> otpGenerationCache;
  private LoadingCache<String, Integer> otpValidationCache;
  private LoadingCache<String, String> otpSecretCache;

  private static final Logger logger = LoggerFactory.getLogger(OTPServiceWithCache.class);

  /**
   * Constructor instantiates the cache where we keep track of number of times user tried to
   * generate otp
   */
  @Autowired
  public OTPServiceWithCache(@Value("${otp.OTPThresholdInMinutes}") final Integer otpThresholdInMinutes,
      @Value("${otp.otpExpiryTimeInMinutes}") final Integer otpExpiryTimeInMinutes,
      @Value("${otp.otpLength}") final Integer otpLength) {
    super();

    otpGenerationCache = CacheBuilder.newBuilder()
        .expireAfterWrite(otpThresholdInMinutes, TimeUnit.MINUTES)
        .build(new CacheLoader<String, Integer>() {
          public Integer load(String key) {
            return 0;
          }
        });

    // Keeps track of count of wrong submission of otp
    otpValidationCache = CacheBuilder.newBuilder()
        .expireAfterWrite(otpThresholdInMinutes, TimeUnit.MINUTES)
        .build(new CacheLoader<String, Integer>() {
          public Integer load(String key) {
            return 0;
          }
        });

    // Saves otp secret of user, used in validation of otp
    otpSecretCache = CacheBuilder.newBuilder()
        .expireAfterWrite(otpExpiryTimeInMinutes, TimeUnit.MINUTES)
        .build(new CacheLoader<String, String>() {
          public String load(String key) {
            return "";
          }
        });

    manager = new TOTPManager(otpLength, otpExpiryTimeInMinutes*60);
  }

  /**
   * Generates the OTP and sends it to user
   *
   * @return ApiResponse
   */
  public String otpGenerate(HttpServletRequest request, UserPasswordGenerationForm userForm) {
    String mobileNumber = userForm.getMobile();
    logger.info("OTP generate: "+ mobileNumber);
    ApiResponse apiResponse = null;
    String deviceIdentifier = OTPUtil.getDeviceIdentifier(request, userForm.getDeviceId());
    apiResponse = checkForBlockedUser(otpGenerationCache, mobileNumber,
        otpConfig.getMaxOtpGenerateAttempts(), deviceIdentifier);
    if (apiResponse == null) {
      byte[] secret = Secret.generate();
      Long otpGenerationTime = System.currentTimeMillis();
      String otp = manager.generate(secret, otpGenerationTime);
      String key = Secret.toHex(secret);
      logger.info(key);
      otpSecretCache.put(deviceIdentifier + "@" + mobileNumber, key);
      otpSecretCache
          .put(deviceIdentifier + "@" + mobileNumber + OTPConstants.OTP_GENERATION_TIME_KEY
              , CommonHelperFunctions.getStringValue(otpGenerationTime));
      return otp;
    }
    return null;
  }

  /**
   * Checks for otp validation
   *
   * @param request: User Request
   * @param validationForm: user submitted details of validation
   * @return ApiResponse
   */
  @LogMethodDetails
  public boolean checkBlockedOrValidateOTP(HttpServletRequest request,
      UserValidationinputForm validationForm) {
    String secret = null;
    Long otpGenerationTime = null;
    String mobileNumber = validationForm.getMobile();
    String otp = validationForm.getOtp();
    String deviceIdentifier = OTPUtil.getDeviceIdentifier(request, validationForm.getDeviceId());
    try {
      secret = otpSecretCache.get(deviceIdentifier + "@" + mobileNumber);
      otpGenerationTime = Long.parseLong(otpSecretCache
          .get(deviceIdentifier + "@" + mobileNumber + OTPConstants.OTP_GENERATION_TIME_KEY));
    } catch (NumberFormatException | ExecutionException e) {
      logger.warn(CommonHelperFunctions.getStackTrace(e));
      return false;
    }
    ApiResponse apiResponse = this.checkForBlockedUser(otpValidationCache, mobileNumber,
        otpConfig.getMaxOtpValidateAttempts(), deviceIdentifier);
    if (apiResponse == null) {
      if (this.otpValidate(otp, secret, otpGenerationTime)) {
        otpValidationCache.invalidate(deviceIdentifier + "#" + mobileNumber);
        otpSecretCache.invalidate(deviceIdentifier + "@" + mobileNumber);
        otpSecretCache.invalidate(
            deviceIdentifier + "@" + mobileNumber + OTPConstants.OTP_GENERATION_TIME_KEY);
        return true;
      } else {
        logger.info(String.format(OTPConstants.OTP_INVALID, mobileNumber));
        return false;
      }
    }
    logger.info(String.format(OTPConstants.USER_BLOCKED, otpConfig.getUserBlockTimeInMinutes()));
    return false;
  }

  /**
   * Validates the otp submitted by user
   *
   * @return boolean flag
   */
  private Boolean otpValidate(String otp, String secret, long otpGenerationTime) {
    if(wfImplConfig.getValidateDefaultOTP() && checkOptionalOTP(otp))
      return true;
    byte[] v = Secret.fromHex(secret);
    return manager.validate(v, otp, otpGenerationTime);
  }

  /**
   * This method checks number of requests from user and decides whether current user should be
   * blocked or not
   */
  @LogMethodDetails
  public ApiResponse checkForBlockedUser(LoadingCache<String, Integer> cache,
      String user, int maxAttempts, String deviceIdentifier) {
    ApiResponse response = null;
    int count = 0;
    try {
      count = cache.get(deviceIdentifier + "#" + user);
    } catch (ExecutionException e) {
      count = 0;
    }
    if (count >= maxAttempts) {
      response = new ApiResponse(HttpStatus.FORBIDDEN, HttpStatus.FORBIDDEN.getReasonPhrase(),
          String.format(OTPConstants.USER_BLOCKED, otpConfig.getUserBlockTimeInMinutes()));
    } else {
      count++;
      cache.put(deviceIdentifier + "#" + user, count);
    }
    return response;
  }
}
