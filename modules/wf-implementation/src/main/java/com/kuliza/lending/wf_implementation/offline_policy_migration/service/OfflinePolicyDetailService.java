package com.kuliza.lending.wf_implementation.offline_policy_migration.service;

import com.kuliza.lending.common.pojo.ApiResponse;

public interface OfflinePolicyDetailService {

	public ApiResponse saveOfflinePolicyDetails() throws Exception;

	public ApiResponse getOfflinePolicyDetails(String name, String mobileNumber) throws Exception;

}
