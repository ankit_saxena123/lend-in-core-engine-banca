package com.kuliza.lending.wf_implementation.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.wf_implementation.pojo.ProductRequest;
import com.kuliza.lending.wf_implementation.pojo.TlPremiumInput;
import com.kuliza.lending.wf_implementation.services.CancerCareService;

@RestController
@RequestMapping(value="/cancer-care")
public class CancerCareController {
	
	@Autowired
	private CancerCareService cancerCareService;

	
	@RequestMapping(method = RequestMethod.POST,value="/quick-quote/{slug}")
	public ResponseEntity<Object> getQuickQuote(Principal principal,
			@RequestBody ProductRequest productRequest, @PathVariable(value = "slug") String slug) {
		return CommonHelperFunctions
				.buildResponseEntity(cancerCareService.getQuickQuote(principal.getName(), productRequest, slug));
	}
	
	
	@RequestMapping(method = RequestMethod.POST,value="/quote-details/{slug}")
	public ResponseEntity<Object> getQuickQuoteDetails(Principal principal,
			@RequestBody TlPremiumInput productRequest, @PathVariable(value = "slug") String slug) {
		return CommonHelperFunctions
				.buildResponseEntity(cancerCareService.getQuickQuoteDetails(principal.getName(), productRequest, slug));
	}
	
}
