package com.kuliza.lending.wf_implementation.services;

import java.security.Principal;
import java.text.Normalizer;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.wf_implementation.WfImplConfig;
import com.kuliza.lending.wf_implementation.dao.IPATPaymentConfirmDao;
import com.kuliza.lending.wf_implementation.dao.UserNotificationDao;
import com.kuliza.lending.wf_implementation.dao.VtigerDropOffDao;
import com.kuliza.lending.wf_implementation.dao.VtigerGenericDropOffDao;
import com.kuliza.lending.wf_implementation.dao.WfUserDeviceRegDao;
import com.kuliza.lending.wf_implementation.dao.WorkflowUserDao;
import com.kuliza.lending.wf_implementation.integrations.FECVtigerIntegration;
import com.kuliza.lending.wf_implementation.models.UserNotificationModel;
import com.kuliza.lending.wf_implementation.models.VtigerDropoffModel;
import com.kuliza.lending.wf_implementation.models.WfUserDeviceRegister;
import com.kuliza.lending.wf_implementation.models.WorkFlowUser;
import com.kuliza.lending.wf_implementation.notification.SNSMessagePublish;
import com.kuliza.lending.wf_implementation.notification.SNSNotificationMessages;
import com.kuliza.lending.wf_implementation.pojo.GenericDropOffRequest;
import com.kuliza.lending.wf_implementation.utils.Constants;
import com.kuliza.lending.wf_implementation.utils.Utils;

@Service("genericDropOffService")
public class GenericDropOffService {

	@Autowired
	private WfImplConfig wfImplConfig;
	@Autowired
	private VtigerDropOffDao vtigerDropOffDao;
	@Autowired
	private IPATPaymentConfirmDao ipatPaymentConfirmDao;
	@Autowired
	private VtigerGenericDropOffDao vtigerGenericDropOffDao;
	@Autowired
	private WorkflowUserDao workFlowUserDao;
	@Autowired
	private WfUserDeviceRegDao wfUserDeviceRegDao;
	@Autowired
	private UserNotificationDao userNotificationDao;
	@Autowired
	private SNSMessagePublish snsMessagePublish;
	@Autowired
	private FECVtigerIntegration vtigerIntegration;
	private static final Logger logger = LoggerFactory.getLogger(GenericDropOffService.class);

	public ApiResponse markGenericDropOffs(Principal principal,GenericDropOffRequest genericDropOffRequest) {
		logger.info("genericDropOffRequest-->" + genericDropOffRequest.toString());
		ApiResponse response;
		try {
			updateVtigerDataInTable(principal,genericDropOffRequest);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.error("Error In  VTiger Generic  markGenericDropOffs --> :" + e.getMessage(), e);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	private void updateVtigerDataInTable(Principal principal,GenericDropOffRequest genericDropOffRequest) {
		
		VtigerDropoffModel vtigerModel = vtigerDropOffDao
				.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
						principal.getName(), "generic",
						Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
		if (vtigerModel == null) {
			vtigerModel = new VtigerDropoffModel();
			vtigerModel.setStatus(Constants.VTIGER_DROP_OFF);
			vtigerModel.setDropOffType(Constants.VTIGER_GENERIC_DROP_OFF_TYPE);
			vtigerModel.setUserName(principal.getName());
			vtigerModel.setCurrentJourneyStage(genericDropOffRequest.getCurrentJourneyScreenName());
			if (genericDropOffRequest.getContactNumber().startsWith("+"))
				vtigerModel.setContactNumber(genericDropOffRequest.getContactNumber());
			else {
				String mobile = genericDropOffRequest.getContactNumber();
				if (mobile.startsWith("0")) {
					 mobile = mobile.substring(1, mobile.length());
				}
				vtigerModel.setContactNumber("+84"+mobile);
			}
			vtigerModel.setProcessInstanceId("generic");
			vtigerModel.setUpdatedInVtiger(false);
			vtigerModel.setLanguage(genericDropOffRequest.getLanguage());
			vtigerModel.setProductSelection(genericDropOffRequest.getProductSelection());
			vtigerModel.setPushNotificationCount(0);
			VtigerDropoffModel vtigerModelList = vtigerDropOffDao.getTopParentCrmIdOfUser(principal.getName());
			if(vtigerModelList!=null){
				String parentCRMId = vtigerModelList.getvTigerParentCrmId();
				vtigerModel.setvTigerParentCrmId(parentCRMId);
				logger.info(" VTiger Generic set parent crmid in markGenericDropOffs 1"+parentCRMId);

				
			}
			vtigerDropOffDao.save(vtigerModel);
		} else {
			if (!vtigerModel.getCurrentJourneyStage()
					.equalsIgnoreCase(genericDropOffRequest.getCurrentJourneyScreenName())
				) {
				vtigerModel.setPushNotificationCount(0);

				vtigerModel.setUpdatedInVtiger(false);
				if (genericDropOffRequest.getContactNumber().startsWith("+"))
					vtigerModel.setContactNumber(genericDropOffRequest.getContactNumber());
				else {
					String mobile = genericDropOffRequest.getContactNumber();
					if (mobile.startsWith("0")) {
						 mobile = mobile.substring(1, mobile.length());
					}
					vtigerModel.setContactNumber("+84"+mobile);
				}
				vtigerModel
					.setCurrentJourneyStage(genericDropOffRequest.getCurrentJourneyScreenName());
				vtigerModel.setUserName(principal.getName());
				vtigerModel.setProcessInstanceId("generic");
				vtigerModel.setLanguage(genericDropOffRequest.getLanguage());
				vtigerModel.setProductSelection(genericDropOffRequest.getProductSelection());					
				VtigerDropoffModel vtigerModelList = vtigerDropOffDao.getTopParentCrmIdOfUser(principal.getName());
				if(vtigerModelList!=null){
					String parentCRMId = vtigerModelList.getvTigerParentCrmId();
					vtigerModel.setvTigerParentCrmId(parentCRMId);
					logger.info(" VTiger Generic  else set parent crmid in markGenericDropOffs 2"+parentCRMId);
					
				}
				vtigerDropOffDao.save(vtigerModel);
			}
		}
	}

	public ApiResponse vtigerUpdateGenericDropOffs() {
		ApiResponse response;
		try {
			List<VtigerDropoffModel> dropoffList = vtigerDropOffDao
					.findByDropOffTypeAndIsUpdatedInVtigerAndIsDeleted(
							Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false, false);
			logger.info(" VTiger Generic  VtigerUpdateDropOffs List size---> :" + dropoffList.size());
			updateDropOffInTable(dropoffList);
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.error("Error in  VTiger Generic  vtigerUpdateDropOffs :" + e.getMessage(), e);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	private void updateDropOffInTable(List<VtigerDropoffModel> dropoffList) throws Exception {
		for (VtigerDropoffModel dropoff : dropoffList) {
			String userName = dropoff.getUserName();
			logger.info(" VTiger Generic Vtiger Dropoff UserName : " + userName);
			if (userName == null) {
				dropoff.setUpdatedInVtiger(true);
				vtigerDropOffDao.save(dropoff);
				continue;
			}
			logger.info("PushNotification Count : " + dropoff.getPushNotificationCount());
			sendPushNotificationAndCallVtiger(dropoff, userName);
		}
	}

	private void sendPushNotificationAndCallVtiger(VtigerDropoffModel dropoff, String userName)
			throws Exception {
		
		LocalDate date = LocalDate.now();
		String expiryDate = date.minusDays(15).toString();
		logger.info(">>>>>  VTiger Generic dropoff policy date ><<<<<<" + expiryDate);
		int purchasedPolicy = ipatPaymentConfirmDao.getTotalPolicyPurchased(dropoff.getContactNumber(), expiryDate);
		if(purchasedPolicy>0){
			logger.info("<=============>There is policy purchased in last 15 days by the userName<====>  :" + userName);

		}else if (dropoff.getPushNotificationCount() < 2) {
			sendPushNotification(dropoff, userName);
		}else {
			Date modified = dropoff.getModified();
			Date currentDate = new Date();
			long diff = currentDate.getTime() - modified.getTime();
			long diffInMillSec = TimeUnit.MILLISECONDS.toMillis(diff);
			String millSec = wfImplConfig.getvTigerTriggerTime();
			logger.info("vtiger generic trigger time"+millSec);
				
			long millSeclong = Long.valueOf(millSec);
			logger.info("<=======>vtiger generic diffInMillSec : " + diffInMillSec
					+ "<=====>vtiger generic millSeclong :" + millSeclong);

			boolean callVtiger=false;
			
			if (diffInMillSec > millSeclong) {
				callVtiger=true;
			}
			if (callVtiger) {
				logger.info("<=============>Call vtiger generic Vtiger true for userName<====>  :" + userName);
				updateProductJourneyStagesInVtiger(dropoff, userName, currentDate);
			}
			
		}
	}

	private void updateProductJourneyStagesInVtiger(VtigerDropoffModel dropoff, String userName,
			Date currentDate) throws Exception {
		if (dropoff.getvTigerContactId() != null && dropoff.getvTigerOpportunityId() != null) {
			logger.info("vtiger generic VtigerDropoff And vTigerOpportunityId Not Null with userName : " + userName);
			if (dropoff.isInActive() == true) {
				logger.info("vtiger generic VtigerDropoff And vTigerOpportunityId Not Null with userName And isInActive true : "
						+ userName);
				updateActiveGenericDropoffAtVtiger(dropoff);
			} else {
				createNewGenericDropoffAtVtiger(dropoff);

			}
		} else {
			logger.info(
					"Vtiger Generic New Dropoff for userName<====>  :" + userName);
			createNewGenericDropoffAtVtiger(dropoff);
		}
	}

	public void createNewGenericDropoffAtVtiger(VtigerDropoffModel dropoff) {
		try {

			JSONObject requestPayload = vtigerIntegration.buildGenericRequestPayLoad(dropoff, true,
					dropoff.getCurrentJourneyStage());
			JSONObject response = null;
			JSONObject uresponse = null;
			response = vtigerIntegration.getDataFromServiceForCreate(requestPayload);
			Map<String, Object> responseMap = vtigerIntegration.parseResponse(response);
			logger.info(
					"<=============>Inside vtiger generic createNewGenericDropoffAtVtiger responseforcreate status:<==>"
							+ responseMap.get("status") + "for userName <==>" + dropoff.getUserName());
			logger.info("<<<<<<<<<<<<<ResponseMap :" + responseMap);
			setNewGenericJourneyStageInVtigerTable(dropoff, responseMap);
			
		} catch (Exception ex) {
			logger.error("Error in vtiger generic createNewGenericDropoffAtVtiger for user <==>:" + dropoff.getUserName()
					+ "<=========>" + ex);
		}

	}

	private void setNewGenericJourneyStageInVtigerTable(VtigerDropoffModel dropoff,
			Map<String, Object> responseMap) {
		if ((boolean) responseMap.get("status")) {
			String contactId = (String) responseMap.get("contactId");
			String vtigerCrmId = (String) responseMap.get("vtigerCrmId");
			if (contactId != null && vtigerCrmId != null) {
				dropoff.setvTigerContactId(contactId);
				dropoff.setvTigerOpportunityId(vtigerCrmId);
				dropoff.setUpdatedInVtiger(true);
				dropoff.setInActive(true);
				/*if(dropoff.getvTigerParentCrmId()==null){
					logger.info("set parent crmid"+vtigerCrmId);
    				dropoff.setvTigerParentCrmId(vtigerCrmId);

				}*/
				if(dropoff.getvTigerParentCrmId()==null){
					VtigerDropoffModel vtigerModel = vtigerDropOffDao
						.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
								dropoff.getUserName(), "generic",
								Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
					if(vtigerModel!=null){
						if(vtigerModel.getvTigerParentCrmId()!=null){
							logger.info(" VTiger Generic set parent crmid 1"+vtigerModel.getvTigerParentCrmId());
							dropoff.setvTigerParentCrmId(vtigerModel.getvTigerParentCrmId());
						}else{
							logger.info(" VTiger Generic set parent crmid 2"+vtigerCrmId);
							dropoff.setvTigerParentCrmId(vtigerCrmId);
						
						}

					}else{
						logger.info("VTiger Generic else set parent crmid setNewGenericJourneyStageInVtigerTable"+vtigerCrmId);
	    				dropoff.setvTigerParentCrmId(vtigerCrmId);
					}
				}
				logger.info("<=============>Vtiger generic Vtiger createNewGenericDropoffAtVtiger contactid <====>  :"
						+ contactId + "for user name <===>" + dropoff.getUserName());
				logger.info(
						"<=============>Vtiger gender createNewGenericDropoffAtVtiger crmid <====>  :" + vtigerCrmId);
				logger.info(
						"<=============>Vtiger  gender createNewGenericDropoffAtVtiger  setInActive<====>  :" + true);
				logger.info("<=============>Vtiger  gener createNewGenericDropoffAtVtiger  setUpdatedInVtiger<====>  :"
						+ true);
			} else {
				logger.info(
						"<=============>Vtiger gener createNewGenericDropoffAtVtiger contactid and crm id is null for username <====>  :"
								+ dropoff.getUserName());
			}
			vtigerDropOffDao.save(dropoff);
			VtigerDropoffModel vtigerModel = vtigerDropOffDao
					.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
							dropoff.getUserName(), "generic",
							Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
			if(vtigerModel!=null)
				logger.info(" VTiger Generic set parent crmid 3"+vtigerModel.getvTigerParentCrmId());

		}
	}

	public void updateActiveGenericDropoffAtVtiger(VtigerDropoffModel dropoff) throws Exception {
		try {
			logger.info("<=============>Inside vtiger generic Vtiger updateActiveProductJourneyStageInVtiger   for pid:<==>"
					+ dropoff.getUserName());
			Map<String, Object> responseMap = pushDataToVtiger(dropoff,true);
			logger.info("<=============>Inside vtiger generic Vtiger updateActiveProductJourneyStageInVtiger response status:<==>"
					+ responseMap.get("status") + "for username <==>" + dropoff.getUserName());
			parseResponseSavingIntoVtigerTable(dropoff, responseMap,true);
		} catch (Exception e) {
			logger.error("Error in vtiger generic updateActiveGenericDropoffAtVtiger for userName:<===>" + dropoff.getUserName()
					+ e.getMessage(), e);
		}

	}

	public void disableGenericDropoffStatusAtVtiger(VtigerDropoffModel dropoff) throws Exception {
		try {
			logger.info("<=============>Inside Vtiger gender disableGenericDropoffStatusAtVtiger   for pid:<==>"
					+ dropoff.getUserName());
			Map<String, Object> responseMap = pushDataToVtiger(dropoff,false);
			logger.info("<=============>Inside Vtiger gender disableGenericDropoffStatusAtVtiger response status:<==>"
					+ responseMap.get("status") + "for username <==>" + dropoff.getUserName());
			parseResponseSavingIntoVtigerTable(dropoff, responseMap,false);
		} catch (Exception e) {
			logger.error("Error in Vtiger gender  disableGenericDropoffStatusAtVtiger for userName:<===>" + dropoff.getUserName()
					+ e.getMessage(), e);
		}

	}
	
	private Map<String, Object> pushDataToVtiger(VtigerDropoffModel dropoff,boolean status) throws Exception {
		JSONObject requestPayload = vtigerIntegration.buildGenericRequestPayLoad(dropoff, status,
				dropoff.getCurrentJourneyStage());
		JSONObject response = null;

		response = vtigerIntegration.getDataFromService(requestPayload);
		Map<String, Object> responseMap = vtigerIntegration.parseResponse(response);
		return responseMap;
	}

	private void parseResponseSavingIntoVtigerTable(VtigerDropoffModel dropoff,
			Map<String, Object> responseMap,boolean status) {
		if ((boolean) responseMap.get("status")) {
			String contactId = (String) responseMap.get("contactId");
			String vtigerCrmId = (String) responseMap.get("vtigerCrmId");
			if (contactId != null && vtigerCrmId != null) {
				dropoff.setvTigerContactId(contactId);
				dropoff.setvTigerOpportunityId(vtigerCrmId);
				dropoff.setUpdatedInVtiger(true);
				dropoff.setInActive(status);
				/*if(dropoff.getvTigerParentCrmId()==null){
					logger.info("set parent crmid"+vtigerCrmId);
    				dropoff.setvTigerParentCrmId(vtigerCrmId);

				}*/
				if(dropoff.getvTigerParentCrmId()==null){
					VtigerDropoffModel vtigerModel = vtigerDropOffDao
						.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
								dropoff.getUserName(), "generic",
								Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
					if(vtigerModel!=null){
						if(vtigerModel.getvTigerParentCrmId()!=null){
							logger.info(" VTiger Generic set parent crmid 4"+vtigerModel.getvTigerParentCrmId());
							dropoff.setvTigerParentCrmId(vtigerModel.getvTigerParentCrmId());
						}else{
							logger.info(" VTiger Generic set parent crmid 5"+vtigerCrmId);
							dropoff.setvTigerParentCrmId(vtigerCrmId);
						
						}

					}else{
						logger.info("VTiger Generic else set parent crmid 6"+vtigerCrmId);
	    				dropoff.setvTigerParentCrmId(vtigerCrmId);
					}
				}
				vtigerDropOffDao.save(dropoff);
				logger.info("<=============> VTiger Generic  Vtiger updateActiveProductJourneyStageInVtiger contactid <====>  :"
						+ contactId + "for userName <===>" + dropoff.getUserName());
				logger.info(
						"<=============> VTiger Generic  Vtiger updateActiveProductJourneyStageInVtiger crmid <====>  :" + vtigerCrmId);
				logger.info(
						"<=============> VTiger Generic  Vtiger updateActiveProductJourneyStageInVtiger  setInActive<====>  :" + true);
				logger.info("<=============> VTiger Generic  Vtiger updateActiveProductJourneyStageInVtiger  setUpdatedInVtiger<====>  :"
						+ true);
				VtigerDropoffModel vtigerModel = vtigerDropOffDao
						.findByUserNameAndProcessInstanceIdAndDropOffTypeAndIsDeleted(
								dropoff.getUserName(), "generic",
								Constants.VTIGER_GENERIC_DROP_OFF_TYPE, false);
				if(vtigerModel!=null)
					logger.info(" VTiger Generic set parent crmid 7 "+vtigerModel.getvTigerParentCrmId());
			} else {
				logger.info("<=============> VTiger Generic  Inside Vtiger updateActiveProductJourneyStageInVtiger   for userName:<==>"
						+ dropoff.getUserName() + "contact id and crm id is null");
			}

		}
	}

	private void sendPushNotification(VtigerDropoffModel dropoff, String username) {
		try {
			Date modified = dropoff.getModified();
			Date currentDate = new Date();
			long diff = currentDate.getTime() - modified.getTime();
			long diffInMillSec = TimeUnit.MILLISECONDS.toMillis(diff);
			String millSec = "";
			if(dropoff.getPushNotificationCount() == 0)
				millSec = wfImplConfig.getPushFirstNotificationTime();
			else 
				millSec = wfImplConfig.getPushSecondNotificationTime();
			
			// String millSec = "300000";
			int notifyCount = 0;
			long millSeclong = Long.valueOf(millSec);
			logger.info("<=======>diffInMillSec : " + diffInMillSec + "<=====>millSeclong :" + millSeclong);
			boolean sendPushNotification = false;

			if (diffInMillSec > millSeclong) {
				if (dropoff.getPushNotificationCount() == 0) {
					sendPushNotification = true;
					notifyCount = 1;
				} else {
					sendPushNotification = true;
					notifyCount = 2;
				}

			}

			extracted(dropoff, notifyCount, sendPushNotification);
		} catch (Exception e) {
			logger.error("<========>Exception vtiger generic while sending push notiofication: " + e.getMessage(), e);
		}

	}

	private void extracted(VtigerDropoffModel dropoff, int notifyCount, boolean sendPushNotification) {
		if (sendPushNotification) {
			logger.info("<============>vtiger genericsendPushNotification is true<============>");
			WorkFlowUser workFlowUser = workFlowUserDao.findByIdmUserNameAndIsDeleted(dropoff.getUserName(), false);
			if (workFlowUser != null) {
				String arnEndPoint = workFlowUser.getArnEndPoint();
				logger.info("<========>arnEndPoint :" + arnEndPoint);
				logger.info("<========>mobileNo:" + workFlowUser.getMobileNumber());
				logger.info("<========>customer name:" + workFlowUser.getUsername());
				String language = Utils.getStringValue(dropoff.getLanguage());

				logger.info("<========>Process Var language :" + language);
				if (Constants.BANCA_DEFAULT_LANGUAGE.equals(language.trim()))
					language = "VN";
				else if (Constants.LANGUAGE_VALUE.equals(language.trim()))
					language = "EN";
				else
					language = "VN";
				logger.info("<========>language :" + language);

				extracted(dropoff, notifyCount, workFlowUser, arnEndPoint, language);

			}
		}
	}

	private void extracted(VtigerDropoffModel dropoff, int notifyCount, WorkFlowUser workFlowUser,
			String arnEndPoint, String language) {
		List<WfUserDeviceRegister> workFlowDeviceList = wfUserDeviceRegDao.findByWfUserAndIsDeleted(workFlowUser,
				false);
		if (workFlowDeviceList.size() > 0) {
			WfUserDeviceRegister wfRegister = workFlowDeviceList.get(0);
			String customer_name = workFlowUser.getUsername();
			String productName = Utils.getStringValue(dropoff.getProductSelection());
			logger.info("<==========>Befpdecode:" + productName);
			String dproductName = Utils.getStringValue(decode(productName));
			logger.info("<==========>pdecode:" + dproductName);
			String curJourney = dropoff.getCurrentJourneyStage();
			logger.info("<==========>Befjourney:" + curJourney);
			logger.info("<==========>Aftjourney:" + Utils.getStringValue(decode(curJourney)));
			logger.info("<==========>current mobile number :" + dropoff.getContactNumber());
			String notifyProductAndStage = chooseProductAndStage(dproductName.trim(), curJourney.trim());
			logger.info("<==========>notifyProductAndStage:" + notifyProductAndStage + "<=====>");

			String notifyMsgKey = notifyProductAndStage + language + "N" + notifyCount;
			logger.info("<========>notifyMsgKey :" + notifyMsgKey);
			String title = "";
			if ("VN" == language) {
				title = Constants.GENERIC_DROP_OFF_PUSH_NOTIFICATION_TITLE_VN;
			} else {
				title = Constants.GENERIC_DROP_OFF_PUSH_NOTIFICATION_TITLE_EN;
			}

			String notifyMsg = SNSNotificationMessages.notificationMsgs.get(notifyMsgKey);

			if (notifyMsg == null) {
				if ("EN" == language) {
					if (notifyCount == 1) {
						notifyMsgKey = "DGENN1";
						notifyMsg = SNSNotificationMessages.notificationMsgs.get("DGENN1");
					} else {
						notifyMsgKey = "DGENN2";
						notifyMsg = SNSNotificationMessages.notificationMsgs.get("DGENN2");
					}
					title = Constants.GENERIC_DROP_OFF_PUSH_NOTIFICATION_TITLE_EN;
				} else {
					if (notifyCount == 1) {
						notifyMsgKey = "DGVNN1";
						notifyMsg = SNSNotificationMessages.notificationMsgs.get("DGVNN1");
					} else {
						notifyMsgKey = "DGVNN2";
						notifyMsg = SNSNotificationMessages.notificationMsgs.get("DGVNN2");
					}
					title = Constants.GENERIC_DROP_OFF_PUSH_NOTIFICATION_TITLE_VN;

				}
			}
			logger.info("<========>notifyMsg :" + notifyMsg);
			logger.info("<========>notifyMsg title:" + title);

			//setUserNotificationDetail(dropoff, workFlowUser, arnEndPoint, wfRegister, customer_name, productName, title,
			//		notifyMsg);
			logger.info("<===========>vtiger genericpush notification customer_name :"+customer_name);
			logger.info("<===========>vtiger genericpush notification productName :"+productName);
			if(customer_name==null){
				notifyMsg = notifyMsg.replace("<customer_name>", "");
			}else{
				notifyMsg = notifyMsg.replace("<customer_name>", customer_name);
			}
			if(productName==null){
				notifyMsg = notifyMsg.replace("<product_name>", "");
			}else{
				notifyMsg = notifyMsg.replace("<product_name>", productName);
			}
			logger.info("<===========>vtiger genericpush notification body :" + notifyMsg);
			UserNotificationModel userNotificationModel = new UserNotificationModel();
			userNotificationModel.setNotificationMessage(notifyMsg);
			userNotificationModel.setNotificationDetailedMessage(notifyMsg);
			userNotificationModel.setContactNumber(workFlowUser.getMobileNumber());
			userNotificationModel.setNationalId(workFlowUser.getNationalId());
			userNotificationModel.setUserName(dropoff.getUserName());

			userNotificationDao.save(userNotificationModel);
			String notificationMessage = snsMessagePublish.publishMessageToEndpoint(notifyMsg, title, arnEndPoint,
					wfRegister.getDeviceType());
			logger.info("<===========>vtiger genericnotificationMessage:" + notificationMessage);
			int count = dropoff.getPushNotificationCount();
			dropoff.setPushNotificationCount(count + 1);
			vtigerDropOffDao.save(dropoff);
		}
	}

	private void setUserNotificationDetail(VtigerDropoffModel dropoff, WorkFlowUser workFlowUser,
			String arnEndPoint, WfUserDeviceRegister wfRegister, String customer_name, String productName, String title,
			String notifyMsg) {
		String body;
		body = notifyMsg;

		// body=body.replace("${policy_name}", productName);
		// String body=Constants.DROP_OFF_PUSH_NOTIFICATION_BODY;
		if(customer_name==null){
			notifyMsg = notifyMsg.replace("<customer_name>", "");
		}else{
			notifyMsg = notifyMsg.replace("<customer_name>", customer_name);
		}
		if(productName==null){
			notifyMsg = notifyMsg.replace("<product_name>", "");
		}else{
			notifyMsg = notifyMsg.replace("<product_name>", productName);
		}
		logger.info("<===========>vtiger genericpush notification body :" + notifyMsg);
		UserNotificationModel userNotificationModel = new UserNotificationModel();
		userNotificationModel.setNotificationMessage(notifyMsg);
		userNotificationModel.setNotificationDetailedMessage(notifyMsg);
		userNotificationModel.setContactNumber(workFlowUser.getMobileNumber());
		userNotificationModel.setNationalId(workFlowUser.getNationalId());
		userNotificationModel.setUserName(dropoff.getUserName());

		userNotificationDao.save(userNotificationModel);
		String notificationMessage = snsMessagePublish.publishMessageToEndpoint(notifyMsg, title, arnEndPoint,
				wfRegister.getDeviceType());
		logger.info("<===========>vtiger genericnotificationMessage:" + notificationMessage);
		int count = dropoff.getPushNotificationCount();
		dropoff.setPushNotificationCount(count + 1);
		vtigerDropOffDao.save(dropoff);
	}

	private String decode(String str) {
		String decoded = null;
		try {
			String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
			Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
			decoded = pattern.matcher(nfdNormalizedString).replaceAll("");
		} catch (Exception e) {
			logger.error("<========>vtiger generic Exception while decode push notiofication: " + e.getMessage(), e);
		}
		return decoded;
	}

	@SuppressWarnings("unlikely-arg-type")
	private String chooseProductAndStage(String productName, String journey) {

		String product = "";
		String stage = "";
		if (productName.equals(Constants.Products.SINGLE_HEALTH.getName())) {
			product = "GSH";
			logger.info("<==***Single Health Protection product***==>");
			stage = chooseSHStage(journey);
			logger.info("<==***Single Health Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.Products.TWO_WHEELER.getName())) {
			product = "GTW";
			logger.info("<==***TW  Protection product***==>");
			stage = chooseTWStage(journey);
			logger.info("<==***TW Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.Products.TERM_LIFE.getName())) {
			product = "GTL";
			logger.info("<==***TL  Protection product***==>");
			stage = chooseTLStage(journey);
			logger.info("<==***TL Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.Products.PERSONAL_ACCIDENT.getName())) {
			product = "GPA";
			logger.info("<==***Personal Accident Protection product***==>");
			stage = choosePAStage(journey);
			logger.info("<==***Personal Accident Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.Products.CANCER_CARE.getName())) {
			product = "GCC";
			logger.info("<==***Cancer Care  Protection product***==>");
			stage = chooseCCStage(journey);
			logger.info("<==***Cancer Care Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.Products.FAMILY_HEALTH.getName())) {
			product = "GFH";
			logger.info("<==***Family Health  Protection product***==>");
			stage = chooseFHStage(journey);
			logger.info("<==***Family Health Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.ProductsInVi.SH.getName())) {
			product = "GSH";
			logger.info("<==***Single Health Protection product***==>");
			stage = chooseSHStage(journey);
			logger.info("<==***Single Health Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.ProductsInVi.TW.getName())) {
			product = "GTW";
			logger.info("<==***TW  Protection product***==>");
			stage = chooseTWStage(journey);
			logger.info("<==***TW Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.ProductsInVi.TL.getName())) {
			product = "GTL";
			logger.info("<==***TL  Protection product***==>");
			stage = chooseTLStage(journey);
			logger.info("<==***TL Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.ProductsInVi.PA.getName())) {
			product = "GPA";
			logger.info("<==***Personal Accident Protection product***==>");
			stage = choosePAStage(journey);
			logger.info("<==***Personal Accident Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.ProductsInVi.CC.getName())) {
			product = "GCC";
			logger.info("<==***Cancer Care  Protection product***==>");
			stage = chooseCCStage(journey);
			logger.info("<==***Cancer Care Protection stage***==>" + stage + "<==***==>");
		} else if (productName.equals(Constants.ProductsInVi.FH.getName())) {
			product = "GFH";
			logger.info("<==***Family Health  Protection product***==>");
			stage = chooseFHStage(journey);
			logger.info("<==***Family Health Protection stage***==>" + stage + "<==***==>");
		} else {
			product = "";
			logger.info("<==***No Product and No stage***==>");
			stage = "";
		}
		if ("" == stage) {
			logger.info("<==***No stage***==>");
			return "";
		}
		return product + stage;
	}

	private String choosePAStage(String journey) {

		String stage = "";

		switch (journey) {

		case Constants.ENQUIRY:
			stage = Constants.S1;
			break;

		case Constants.PA_ENQ:
			stage = Constants.S2;
			break;

		default:
			stage = "";
			break;

		}

		return stage;
	}

	private String chooseCCStage(String journey) {

		String stage = "";

		switch (journey) {

		case Constants.ENQUIRY:
			stage = Constants.S1;
			break;

		case Constants.CC_ENQ:
			stage = Constants.S2;
			break;
			
		default:
			stage = "";
			break;

		}

		return stage;
	}

	private String chooseFHStage(String journey) {

		String stage = "";

		switch (journey) {

		case Constants.ENQUIRY:
			stage = Constants.S1;
			break;

		case Constants.FH_ENQ:
			stage = Constants.S2;
			break;
			
		case Constants.FH_FAM_ENQ:
			stage = Constants.S3;
			break;
			
		default:
			stage = "";
			break;

		}

		return stage;
	}

	private String chooseSHStage(String journey) {

		String stage = "";

		switch (journey) {

		case Constants.LOGIN:
			stage = Constants.S1;
			break;

		case Constants.ONBOARD:
			stage = Constants.S2;
			break;
			
		case Constants.ENQUIRY:
			stage = Constants.S3;
			break;

		case Constants.SH_ENQ:
			stage = Constants.S4;
			break;

		default:
			stage = "";
			break;

		}

		return stage;
	}

	private String chooseTWStage(String journey) {

		String stage = "";

		switch (journey) {

		case Constants.ENQUIRY:
			stage = Constants.S1;
			break;

		case Constants.TW_ENQ:
			stage = Constants.S2;
			break;

		default:
			stage = "";
			break;

		}

		return stage;
	}

	private String chooseTLStage(String journey) {

		String stage = "";

		switch (journey) {

		case Constants.ENQUIRY:
			stage = Constants.S1;
			break;

		case Constants.TL_ENQ:
			stage = Constants.S2;
			break;

		default:
			stage = "";
			break;

		}

		return stage;
	}
}
