package com.kuliza.lending.wf_implementation.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "offline_policy_detail")
public class OfflinePolicyDetails extends BaseModel {

	@Column(nullable = true, name = "plan_id")
	private String planId;
	
	@Column(nullable = true, name = "product_id")
	private String productId;
	
	@Column(nullable = true, name = "product_name")
	private String productName;

	@Column(nullable = true, name = "plan_name")
	private String planName;

	@Column(nullable = true, name = "policy_number")
	private String policyNumber;

	@Column(nullable = true, name = "insurer_name")
	private String insurerName;

	@Column(nullable = true, name = "policy_purchase_date")
	private Date policyPurchaseDate;

	@Column(nullable = true, name = "policy_expiry_date")
	private Date policyExpiryDate;

	@Column(nullable = true, name = "policy_next_premium_due_date")
	private Date policyNextPremiumDueDate;

	@Column(nullable = true, name = "protection_amount")
	private String protectionAmount;

	@Column(nullable = true, name = "total_premium_amount")
	private String totalPremiumAmount;

	@Column(nullable = true, name = "policy_dms_doc_id")
	private String policyDmsDocId;

	@Column(nullable = true, name = "policy_mode")
	private String policyMode;

	@Column(nullable = true, name = "source_of_policy")
	private String sourceOfPolicy;

	@Column(nullable = true, name = "mobile_number")
	private String mobileNumber;

	@Column(nullable = true, name = "national_id")
	private String nationalId;
	
	@Column(nullable = true, name = "idm_user_name")
	private String idmUserName;
	
	@ColumnDefault("0")
	@Column(nullable = true, name = "display_status")
	private Boolean displayStatus ;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getInsurerName() {
		return insurerName;
	}

	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

	public Date getPolicyPurchaseDate() {
		return policyPurchaseDate;
	}

	public void setPolicyPurchaseDate(Date policyPurchaseDate) {
		this.policyPurchaseDate = policyPurchaseDate;
	}

	public Date getPolicyExpiryDate() {
		return policyExpiryDate;
	}

	public void setPolicyExpiryDate(Date policyExpiryDate) {
		this.policyExpiryDate = policyExpiryDate;
	}

	public Date getPolicyNextPremiumDueDate() {
		return policyNextPremiumDueDate;
	}

	public void setPolicyNextPremiumDueDate(Date policyNextPremiumDueDate) {
		this.policyNextPremiumDueDate = policyNextPremiumDueDate;
	}

	public String getProtectionAmount() {
		return protectionAmount;
	}

	public void setProtectionAmount(String protectionAmount) {
		this.protectionAmount = protectionAmount;
	}

	public String getTotalPremiumAmount() {
		return totalPremiumAmount;
	}

	public void setTotalPremiumAmount(String totalPremiumAmount) {
		this.totalPremiumAmount = totalPremiumAmount;
	}

	public String getPolicyDmsDocId() {
		return policyDmsDocId;
	}

	public void setPolicyDmsDocId(String policyDmsDocId) {
		this.policyDmsDocId = policyDmsDocId;
	}

	public String getPolicyMode() {
		return policyMode;
	}

	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}

	public String getSourceOfPolicy() {
		return sourceOfPolicy;
	}

	public void setSourceOfPolicy(String sourceOfPolicy) {
		this.sourceOfPolicy = sourceOfPolicy;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getIdmUserName() {
		return idmUserName;
	}

	public void setIdmUserName(String idmUserName) {
		this.idmUserName = idmUserName;
	}
	
	public Boolean getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(Boolean displayStatus) {
		this.displayStatus = displayStatus;
	}

	@Override
	public String toString() {
		return "OfflinePolicyDetails [planId=" + planId + ", productId=" + productId + ", productName=" + productName
				+ ", planName=" + planName + ", policyNumber=" + policyNumber + ", insurerName=" + insurerName
				+ ", policyPurchaseDate=" + policyPurchaseDate + ", policyExpiryDate=" + policyExpiryDate
				+ ", policyNextPremiumDueDate=" + policyNextPremiumDueDate + ", protectionAmount=" + protectionAmount
				+ ", totalPremiumAmount=" + totalPremiumAmount + ", policyDmsDocId=" + policyDmsDocId + ", policyMode="
				+ policyMode + ", sourceOfPolicy=" + sourceOfPolicy + ", mobileNumber=" + mobileNumber + ", nationalId="
				+ nationalId + ", idmUserName=" + idmUserName + ", displayStatus=" + displayStatus + "]";
	}


	
	
}
