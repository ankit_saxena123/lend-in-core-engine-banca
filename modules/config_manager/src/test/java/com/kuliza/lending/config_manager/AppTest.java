package com.kuliza.lending.config_manager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.config_manager.dao.ConfigsDao;
import com.kuliza.lending.config_manager.models.ConfigModel;
import com.kuliza.lending.config_manager.pojo.ConfigInput;
import com.kuliza.lending.config_manager.services.ConfigServices;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest

{
  @Mock
  private ConfigsDao configDao;

  @InjectMocks
  private ConfigServices configServices = new ConfigServices();

  private String clientId;
  private String jsonString;
  private ConfigModel configModel;

  @Before
  public void init() {
    this.clientId = "123456";
    this.jsonString = "{\"glossary\":{\"title\":\"example glossary\",\"GlossDiv\":{\"title\":\"S\"}}}";
    this.configModel = new ConfigModel("123456",
        "{\"glossary\":{\"title\":\"example glossary\",\"GlossDiv\":{\"title\":\"S\"}}}");
  }

  @Test
  public void getConfig1() throws Exception {
    when(configDao.findByClientId(this.clientId)).thenReturn(this.configModel);
    ApiResponse response = configServices.getConfigs(this.clientId);
    assertEquals(Constants.SUCCESS_CODE, response.getStatus());
  }

  @Test
  public void getConfig2() throws Exception {
    when(configDao.findByClientId(this.clientId)).thenReturn(this.configModel);
    ApiResponse response = configServices.getConfigs("99999");
    assertEquals(Constants.FAILURE_CODE, response.getStatus());
  }

  @Test
  public void setConfig1() throws Exception {
    when(configDao.findByClientId(this.clientId)).thenReturn(this.configModel);
    ConfigInput input = new ConfigInput("99999",
        "{\"glossary\":{\"title\":\"example glossary\",\"GlossDiv\":{\"title\":\"S\"}}}");
    ApiResponse response = configServices.setConfig(input);
    assertEquals(Constants.SUCCESS_CODE, response.getStatus());
  }

  @Test
  public void setConfig2() throws Exception {
    when(configDao.findByClientId(this.clientId)).thenReturn(this.configModel);
    ConfigInput input = new ConfigInput("99999", "NOT A JSON");
    ApiResponse response = configServices.setConfig(input);
    assertEquals(Constants.FAILURE_CODE, response.getStatus());
  }
}
