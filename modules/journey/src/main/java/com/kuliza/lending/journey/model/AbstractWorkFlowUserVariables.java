package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.kuliza.lending.common.model.BaseModel;

@MappedSuperclass
public class AbstractWorkFlowUserVariables extends BaseModel {

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String name;
	
	@Column(columnDefinition = "LONGTEXT")
	private String valueText;

	@Column(nullable = false)
	private String lastProcInstId;
	
	@Column(nullable = false)
	private String lastTaskId;
	
	public AbstractWorkFlowUserVariables() {
		super();
		this.setIsDeleted(false);
	}

	public AbstractWorkFlowUserVariables(String username, String name,
			String valueText, String lastProcInstId, String lastTaskId) {
		super();
		this.username = username;
		this.name = name;
		this.valueText = valueText;
		this.lastProcInstId = lastProcInstId;
		this.lastTaskId = lastTaskId;
		this.setIsDeleted(false);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValueText() {
		return valueText;
	}

	public void setValueText(String valueText) {
		this.valueText = valueText;
	}

	public String getLastProcInstId() {
		return lastProcInstId;
	}

	public void setLastProcInstId(String lastProcInstId) {
		this.lastProcInstId = lastProcInstId;
	}

	public String getLastTaskId() {
		return lastTaskId;
	}

	public void setLastTaskId(String lastTaskId) {
		this.lastTaskId = lastTaskId;
	}
	
}
