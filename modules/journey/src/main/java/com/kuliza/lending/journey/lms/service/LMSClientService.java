package com.kuliza.lending.journey.lms.service;

import com.kuliza.lending.journey.lms.data.ClientConfiguration;
import com.kuliza.lending.journey.lms.data.LoanConfiguration;
import com.kuliza.lending.journey.lms.model.LMSIntegrationConfig;
import com.kuliza.lending.journey.lms.model.LMSIntegrationConfigRepository;
import org.flowable.engine.HistoryService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.history.HistoricProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("LMSClientService")
public class LMSClientService {

    private LMSIntegrationConfigRepository lmsIntegrationConfigRepository;
    private LMSIntegrationService lmsIntegrationService;
    private HistoryService historyService;

    @Autowired
    public LMSClientService(LMSIntegrationConfigRepository lmsIntegrationConfigRepository,
                            LMSIntegrationService lmsIntegrationService, HistoryService historyService){
        this.lmsIntegrationConfigRepository = lmsIntegrationConfigRepository;
        this.lmsIntegrationService = lmsIntegrationService;
        this.historyService = historyService;
    }

    public DelegateExecution processClients(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(rootProcessInstanceId).singleResult();
        System.out.println(historicProcessInstance.getProcessDefinitionKey());
        System.out.println(historicProcessInstance.getProcessDefinitionName());
        String processDefinitionKey = historicProcessInstance.getProcessDefinitionKey();

        LMSIntegrationConfig lmsIntegration = lmsIntegrationConfigRepository.findByJourneyIdentifier(processDefinitionKey);
        if (lmsIntegration == null)
            throw new RuntimeException("Integration config not available for " + processDefinitionKey);

        Collection<ClientConfiguration> clientConfigurations = lmsIntegration.getClientConfigurations();
        lmsIntegrationService.processClientConfigurations(rootProcessInstanceId, clientConfigurations);

        return execution;

    }


    public DelegateExecution processLoans(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(rootProcessInstanceId).singleResult();
        System.out.println(historicProcessInstance.getProcessDefinitionKey());
        System.out.println(historicProcessInstance.getProcessDefinitionName());
        String processDefinitionKey = historicProcessInstance.getProcessDefinitionKey();

        LMSIntegrationConfig lmsIntegration = lmsIntegrationConfigRepository.findByJourneyIdentifier(processDefinitionKey);
        if (lmsIntegration == null)
            throw new RuntimeException("Integration config not available for " + processDefinitionKey);

        Collection<LoanConfiguration> loanConfigurations = lmsIntegration.getLoanConfigurations();
        lmsIntegrationService.processLoanConfigurations(rootProcessInstanceId, loanConfigurations);

        return execution;

    }


}
