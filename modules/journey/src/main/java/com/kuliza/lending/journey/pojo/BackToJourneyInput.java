package com.kuliza.lending.journey.pojo;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class BackToJourneyInput {
	@NotEmpty(message = "applicationId cannot be empty")
	String applicationId;
	
	@NotEmpty(message = "journeyType cannot be empty")
	String journeyType;

	String processInstanceId;
	
	Map<String, Object> variablesToSave;
	
	boolean closeTask;

	List<String> taskList;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public Map<String, Object> getVariablesToSave() {
		return variablesToSave;
	}

	public void setVariablesToSave(Map<String, Object> variablesToSave) {
		this.variablesToSave = variablesToSave;
	}

	public String getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	public boolean isCloseTask() {
		return closeTask;
	}

	public void setCloseTask(boolean closeTask) {
		this.closeTask = closeTask;
	}

	public List<String> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<String> taskList) {
		this.taskList = taskList;
	}
}
