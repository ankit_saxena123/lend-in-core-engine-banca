package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "los_user")
public class LOSUserModel extends BaseModel {

	@Column(nullable = true)
	private String name;

	@Column(nullable = true)
	private int age;

	@Column(nullable = true)
	private String dateOfBirth;

	@Column(nullable = true)
	private String sex;

	@Column(nullable = false)
	private String contactNumber;

	@Column(nullable = true)
	private String email;

	@Column(nullable = true)
	private String password;

	public LOSUserModel() {
		super();
		this.setIsDeleted(false);
	}

	public LOSUserModel(String name, int age, String dateOfBirth, String sex, String contactNumber, String email,
			String passowrd) {
		super();
		this.setIsDeleted(false);
		this.name = name;
		this.age = age;
		this.dateOfBirth = dateOfBirth;
		this.sex = sex;
		this.contactNumber = contactNumber;
		this.email = email;
		this.password = passowrd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}