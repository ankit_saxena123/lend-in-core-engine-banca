package com.kuliza.lending.journey.lms.service;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("HomeLoanDemoService")
public class HomeLoanDemoService extends CommonDemoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeLoanDemoService.class);

    @Autowired
    public HomeLoanDemoService(RuntimeService runtimeService){
        super(runtimeService);
    }


    public DelegateExecution createClientApiIntegration(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("address", new JSONArray());
        requestBody.put("familyMembers", new JSONArray());
        requestBody.put("officeId", this.defaultOfficeId);
        requestBody.put("firstname", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "namePAN")));
        requestBody.put("lastname", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "lastNamePAN")));
        requestBody.put("genderId", 22);
        requestBody.put("active", false);
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("activationDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("savingsProductId", null);
        String clientId;
        try{

            LOGGER.debug("request body createClientApiIntegration: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+ "clients")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("createClientApiIntegration-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("createClientApiIntegration api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            clientId = body.optString("clientId");

            runtimeService.setVariable(rootProcessInstanceId, "clientIdFromLMS", clientId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }

        this.createDatatableForClient(execution, clientId);
        this.activateClient(execution, clientId);

        return execution;
    }


    public void createDatatable(DelegateExecution execution) throws Exception{


        String rootProcessInstanceId = execution.getRootProcessInstanceId();
        Map<String, Object> processVariables = runtimeService.getVariables(rootProcessInstanceId);

        String cId = String.valueOf(processVariables.getOrDefault("clientIdFromLMS", ""));
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("PAN", String.valueOf(processVariables.getOrDefault("pan", "")));
        requestBody.put("Aadhaar", String.valueOf(processVariables.getOrDefault("aadhaar", "")));
        requestBody.put("Cibil", String.valueOf(processVariables.getOrDefault("cibil", "")));
        requestBody.put("Mobile Number", String.valueOf(processVariables.getOrDefault("mobileNumber", "")));
        requestBody.put("Email Address", String.valueOf(processVariables.getOrDefault("emailAddress", "")));
        requestBody.put("Property Category", String.valueOf(processVariables.getOrDefault("propertyCategory", "")));
        requestBody.put("Purpose of loan", String.valueOf(processVariables.getOrDefault("loanPurpose", "")));
        requestBody.put("Builder Name", String.valueOf(processVariables.getOrDefault("builderName", "")));
        requestBody.put("Project Name", String.valueOf(processVariables.getOrDefault("projectNamePrestige", "")));
        requestBody.put("Wing Name", String.valueOf(processVariables.getOrDefault("wingName", "")));
        requestBody.put("Flat Number", String.valueOf(processVariables.getOrDefault("flatNumber", "")));
        requestBody.put("Property Cost", String.valueOf(processVariables.getOrDefault("costOfFlat", "")));
        requestBody.put("Down Payment", String.valueOf(processVariables.getOrDefault("downPaymentAmount", "")));
        requestBody.put("First Disbursal Amount", String.valueOf(processVariables.getOrDefault("firstDisbursalAmount", "")));
        requestBody.put("First Disbursal Date", String.valueOf(processVariables.getOrDefault("firstDisbursalDate", "")));
        requestBody.put("Account holder name", String.valueOf(processVariables.getOrDefault("disbursalAccountHolderName", "")));
        requestBody.put("Bank name", String.valueOf(processVariables.getOrDefault("disbursalSalaryBank", "")));
        requestBody.put("Bank account number", String.valueOf(processVariables.getOrDefault("disbursalAccountNumber", "")));
        requestBody.put("IFSC", String.valueOf(processVariables.getOrDefault("disbursalIfscCode", "")));
        requestBody.put("Branch", String.valueOf(processVariables.getOrDefault("disbursalSalaryBranch", "")));
        requestBody.put("Employment Type", String.valueOf(processVariables.getOrDefault("employmentType", "")));
        requestBody.put("Residential address line 1", String.valueOf(processVariables.getOrDefault("permanentAddressLine1", "")));
        requestBody.put("Residential address line 2", String.valueOf(processVariables.getOrDefault("permanentAddressLine2", "")));
        requestBody.put("Residential Pincode", String.valueOf(processVariables.getOrDefault("pincodePermanent", "")));
        requestBody.put("Residential City", String.valueOf(processVariables.getOrDefault("permanentCity", "")));
        requestBody.put("Residential State", String.valueOf(processVariables.getOrDefault("permanentState", "")));
        requestBody.put("Monthly Income", String.valueOf(processVariables.getOrDefault("netMonthlySalary", "")));
        requestBody.put("EMI if any", String.valueOf(processVariables.getOrDefault("existingEMI", "")));
        requestBody.put("Resident type", String.valueOf(processVariables.getOrDefault("currentResidenceTypeCA", "")));
        requestBody.put("Property Documents", String.valueOf(processVariables.getOrDefault("propertyDocumentEmpannelledHeading", "")));
        requestBody.put("Property documents additional", String.valueOf(processVariables.getOrDefault("conveyanceDeedNewPropertyUpload", "")));
        requestBody.put("Identity Documents", String.valueOf(processVariables.getOrDefault("photographUploadGA", "")));
        requestBody.put("Identity documents additional", String.valueOf(processVariables.getOrDefault("uploadIdentityProofGA", "")));
        requestBody.put("Uploaded PAN", String.valueOf(processVariables.getOrDefault("uploadPanCard", "")));
        requestBody.put("Uploaded Aadhaar", String.valueOf(processVariables.getOrDefault("uploadAadhar", "")));


        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        try{
            LOGGER.debug("request body createDatatableDataForClient: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"datatables/Home Loan Additional Information 1/"
                    + String.valueOf(cId))
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("createDatatableDataForClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }


    public DelegateExecution createLoanApiIntegration(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();



        Map<String, Object> requestBody = new HashMap<>();

        Map<String, Object> disbursementData = new HashMap<>();
        disbursementData.put("expectedDisbursementDate", convertDate(String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "firstDisbursalDate")),
                "dd-MM-yyyy",this.defaultDateFormat));

        disbursementData.put("principal", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "firstDisbursalAmount")));
        requestBody.put("clientId", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "clientIdFromLMS")));
        requestBody.put("productId", 20);
        requestBody.put("disbursementData", new JSONArray().put(disbursementData));
        requestBody.put("principal",  String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount")));
        requestBody.put("loanTermFrequency", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure")));
        requestBody.put("loanTermFrequencyType", 2);
        requestBody.put("numberOfRepayments", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure")));
        requestBody.put("repaymentEvery", 1);
        requestBody.put("repaymentFrequencyType", 2);
        requestBody.put("interestRatePerPeriod", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "roi")));
        requestBody.put("amortizationType", 1);
        requestBody.put("isEqualAmortization", false);
        requestBody.put("interestType", 0);
        requestBody.put("interestCalculationPeriodType", 1);
        requestBody.put("allowPartialPeriodInterestCalcualtion", true);
        requestBody.put("transactionProcessingStrategyId", 1);
        requestBody.put("maxOutstandingLoanBalance", "100000000");

        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("loanType", "individual");
        requestBody.put("expectedDisbursementDate", convertDate(String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "firstDisbursalDate")),
				                 "dd-MM-yyyy",this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));


        try{
            LOGGER.debug("request body LMS: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"loans")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("LMS-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            String loanId = body.optString("loanId");

            runtimeService.setVariable(rootProcessInstanceId, "loanIdFromLMS", loanId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }


        return execution;

    }

}
