package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.common.utils.Constants.DeviceType;

@MappedSuperclass
public class AbstractWfUserDeviceRegister extends BaseModel {

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private DeviceType deviceType;

	@Column(nullable = false)
	private String deviceId;

	@Column(nullable = true)
	private String mpin;

	public AbstractWfUserDeviceRegister() {
		super();
		this.setIsDeleted(false);
	}

	public AbstractWfUserDeviceRegister(DeviceType deviceType, String deviceId,
			String mpin) {
		super();
		this.deviceType = deviceType;
		this.deviceId = deviceId;
		this.mpin = mpin;
	}
	
	public AbstractWfUserDeviceRegister(DeviceType deviceType, String deviceId) {
		super();
		this.deviceType = deviceType;
		this.deviceId = deviceId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

}
