package com.kuliza.lending.journey.customer_dashboard.pojo;

public class CustomerDashboardApplicationExpired {
	String loanAmount;
	String emi;
	String tenure;
	String applicationDate;
	String applicationStatus;
	String loanContractNumber;
	String loanContractUrl;

	public CustomerDashboardApplicationExpired() {
		super();
		this.loanAmount = "";
		this.emi = "";
		this.tenure = "";
		this.applicationStatus = "";
		this.loanContractNumber = "";
		this.loanContractUrl = "";
	}

	public CustomerDashboardApplicationExpired(String loanAmount, String emi, String tenure, String applicationDate,
			String applicationStatus, String loanContractNumber, String loanContractUrl) {
		super();
		this.loanAmount = loanAmount;
		this.emi = emi;
		this.tenure = tenure;
		this.applicationDate = applicationDate;
		this.applicationStatus = applicationStatus;
		this.loanContractNumber = loanContractNumber;
		this.loanContractUrl = loanContractUrl;
	}

	public String getLoanContractNumber() {
		return loanContractNumber;
	}

	public void setLoanContractNumber(String loanContractNumber) {
		this.loanContractNumber = loanContractNumber;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getEmi() {
		return emi;
	}

	public void setEmi(String emi) {
		this.emi = emi;
	}

	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
	
	public String getLoanContractUrl() {
		return loanContractUrl;
	}
	public void setLoanContractUrl(String loanContractUrl) {
		this.loanContractUrl = loanContractUrl;
	}

}
