package com.kuliza.lending.journey.lms.controller;

import com.kuliza.lending.common.connection.UnirestConnection;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.journey.lms.data.ClientConfiguration;
import com.kuliza.lending.journey.lms.data.DatatableConfiguration;
import com.kuliza.lending.journey.lms.data.LoanConfiguration;
import com.kuliza.lending.journey.lms.model.LMSIntegrationConfig;
import com.kuliza.lending.journey.lms.model.LMSIntegrationConfigRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.ProcessDefinition;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/lms-integration/config/")
public class LmsIntegrationConfigController {

    private RepositoryService repositoryService;
    private LMSIntegrationConfigRepository lmsIntegrationConfigRepository;

    @Autowired
    public LmsIntegrationConfigController(RepositoryService repositoryService, LMSIntegrationConfigRepository lmsIntegrationConfigRepository){
        this.repositoryService = repositoryService;
        this.lmsIntegrationConfigRepository = lmsIntegrationConfigRepository;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{journeyIdentifier}/clients/")
    public ApiResponse createOrUpdateClient(Principal principal, @PathVariable String journeyIdentifier,
                                       @RequestParam String command,
                                       @RequestBody ClientConfiguration.ClientConfigurationRequest request) {

        List<ProcessDefinition> processDefinition = repositoryService.createProcessDefinitionQuery().
                processDefinitionKey(journeyIdentifier).list();

        if (processDefinition == null || processDefinition.isEmpty())
            return new ApiResponse(HttpStatus.BAD_REQUEST,
                    "no process exist with " + journeyIdentifier + " journeyIdentifier/processIdentifier",
                    null);
        LMSIntegrationConfig lmsIntegrationConfig = lmsIntegrationConfigRepository.findByJourneyIdentifier(journeyIdentifier);
        if (Objects.isNull(lmsIntegrationConfig)){
            lmsIntegrationConfig = new LMSIntegrationConfig();
            lmsIntegrationConfig.setJourneyIdentifier(journeyIdentifier);
            lmsIntegrationConfigRepository.save(lmsIntegrationConfig);
        }
        try {
            if (command.equalsIgnoreCase("createOrUpdate"))
                lmsIntegrationConfig.createOrUpdateClient(request);
            else if (command.equalsIgnoreCase("delete"))
                lmsIntegrationConfig.deleteClient(request);
            else
                throw new RuntimeException("unknown command");
        }catch (RuntimeException e){
            return new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
        lmsIntegrationConfig.ConvertToPersistable();
        lmsIntegrationConfigRepository.save(lmsIntegrationConfig);
        return new ApiResponse(HttpStatus.OK, "", null);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{journeyIdentifier}/clients/{clientType}/datatables/")
    public ApiResponse createOrUpdateDatatable(Principal principal, @PathVariable String journeyIdentifier,
                                       @PathVariable String clientType,
                                       @RequestParam String command,
                                       @RequestBody DatatableConfiguration.DatatableConfigurationRequest request) {

        List<ProcessDefinition> processDefinition = repositoryService.createProcessDefinitionQuery().
                processDefinitionKey(journeyIdentifier).list();

        if (processDefinition == null || processDefinition.isEmpty())
            return new ApiResponse(HttpStatus.BAD_REQUEST,
                    "no process exist with " + journeyIdentifier + " journeyIdentifier/processIdentifier",
                    null);
        LMSIntegrationConfig lmsIntegrationConfig = lmsIntegrationConfigRepository.findByJourneyIdentifier(journeyIdentifier);
        if (Objects.isNull(lmsIntegrationConfig)){
            return new ApiResponse(HttpStatus.BAD_REQUEST,
                    "no los-lms config exist with " + journeyIdentifier + " journeyIdentifier/processIdentifier",
                    null);
        }
        try {
            if (command.equalsIgnoreCase("createOrUpdate"))
                lmsIntegrationConfig.createOrUpateDatatable(clientType, request);
            else if (command.equalsIgnoreCase("delete"))
                lmsIntegrationConfig.deleteDataTable(clientType, request);
            else
                throw new RuntimeException("unknown command");
        }catch (RuntimeException e){
            return new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }

        lmsIntegrationConfig.ConvertToPersistable();
        lmsIntegrationConfigRepository.save(lmsIntegrationConfig);
        return new ApiResponse(HttpStatus.OK, "", null);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/{journeyIdentifier}/loans/")
    public ApiResponse createOrUpdateLoan(Principal principal, @PathVariable String journeyIdentifier,
                                            @RequestParam String command,
                                            @RequestBody LoanConfiguration.LoanConfigurationRequest request) {

        List<ProcessDefinition> processDefinition = repositoryService.createProcessDefinitionQuery().
                processDefinitionKey(journeyIdentifier).list();

        if (processDefinition == null || processDefinition.isEmpty())
            return new ApiResponse(HttpStatus.BAD_REQUEST,
                    "no process exist with " + journeyIdentifier + " journeyIdentifier/processIdentifier",
                    null);
        LMSIntegrationConfig lmsIntegrationConfig = lmsIntegrationConfigRepository.findByJourneyIdentifier(journeyIdentifier);
        if (Objects.isNull(lmsIntegrationConfig)){
            lmsIntegrationConfig = new LMSIntegrationConfig();
            lmsIntegrationConfig.setJourneyIdentifier(journeyIdentifier);
            lmsIntegrationConfigRepository.save(lmsIntegrationConfig);
        }
        try {
            if (command.equalsIgnoreCase("createOrUpdate"))
                lmsIntegrationConfig.createOrUpdateLoan(request);
            else if (command.equalsIgnoreCase("delete"))
                lmsIntegrationConfig.deleteLoan(request);
            else
                throw new RuntimeException("unknown command");
        }catch (RuntimeException e){
            return new ApiResponse(HttpStatus.BAD_REQUEST, e.getMessage(), null);
        }
        lmsIntegrationConfig.ConvertToPersistable();
        lmsIntegrationConfigRepository.save(lmsIntegrationConfig);
        return new ApiResponse(HttpStatus.OK, "", null);
    }

    public static String convertDate(Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/test/")
    public ApiResponse test(@RequestBody Map<String, Object> body) throws UnirestException {

        String baseUrl = body.get("baseUrl").toString();
        String token = body.get("token").toString();
        String tenant = body.get("tenant").toString();
        Map<String, String> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Content-Type", "application/json");

        body.remove("baseUrl");
        body.remove("token");
        body.remove("tenant");

        System.out.println("create LMS client:BASE-URL:: " + baseUrl);
        System.out.println("create LMS client:REQUEST-BODY:: " + new JSONObject(body).toString());
        Map<String, String> headers = new HashMap<>(defaultHeaders);
        headers.put("Authorization", "Basic " + token);
        headers.put("Fineract-Platform-TenantId", tenant);
        System.out.println("create LMS client:REQUEST-HEADERS:: " + new JSONObject(headers).toString());
        HttpResponse<String> response = UnirestConnection.sendPOST(new JSONObject(body), headers, baseUrl + "clients");
        System.out.println("create LMS client:RESPONSE-BODY:: " + response.getBody());

        if (response.getStatus() != 200)
            throw new RuntimeException(response.getBody());
        JSONObject responseBody = new JSONObject(response.getBody());
        System.out.println(responseBody.toString());
        return new ApiResponse(HttpStatus.OK, "", null);
    }
}
