package com.kuliza.lending.journey.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.annotations.LogMethodDetails;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.service.GenericServices;
import com.kuliza.lending.common.utils.*;
import com.kuliza.lending.common.utils.Enums.APPLICATION_STAGE;
import com.kuliza.lending.journey.model.*;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.BackToJourneyInput;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.utils.HelperFunctions;
import com.kuliza.lending.journey.utils.Validation;
import com.kuliza.lending.portal.service.CMMNService;
import com.kuliza.lending.utils.AuthUtils;
import org.apache.commons.codec.binary.Base64;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.*;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.api.FormService;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.*;

@Service("UserJourneyServices")
public class UserJourneyServices extends GenericServices {

	private static final Logger logger = LoggerFactory.getLogger(UserJourneyServices.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private IdentityService identityService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private LOSApplicationNumberDao losApplicationNumberDao;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private LOSUserDao losUserDao;

	@Autowired
	private KeyCloakManager keycloakManager;

	@Autowired
	private LOSUserLoanDataDao losUserLoanDataDao;

	@Autowired
	private LOSUserAddressDao losUserAddressDao;

	@Autowired
	private LOSUserEmploymentDao losUserEmploymentDao;

	@Autowired
	private CMMNService cmmnService;

	@Autowired
	private FormService formService;

	/**
	 * This functions takes Parent/Root Process Instance Id and returns List of All
	 * Process Instance Ids of Running Child Executions
	 * 
	 * @param parentProcessInstanceId
	 * @return List of String Process Instance Ids
	 * 
	 * @author Arpit Agrawal
	 */
	public List<String> allProcessInstances(String parentProcessInstanceId) {
		List<Execution> allRunningExecutions = runtimeService.createExecutionQuery()
				.rootProcessInstanceId(parentProcessInstanceId).list();
		List<String> allProcessInstanceIds = new ArrayList<>();
		for (Execution singleExecution : allRunningExecutions) {
			allProcessInstanceIds.add(singleExecution.getProcessInstanceId());
		}
		allProcessInstanceIds.add(parentProcessInstanceId);
		return allProcessInstanceIds;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given process instance id.
	 * 
	 * @param userId
	 * @param parentProcessInstanceId
	 * @param errorcode
	 * @param errorMessage
	 * @return ApiResponse
	 * @throws Exception
	 * 
	 * @author Arpit Agrawal
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse getTaskData(String userId, String parentProcessInstanceId, HttpStatus errorcode,
			String errorMessage) throws Exception {
		ApiResponse response;
		List<String> allProcessInstanceIds = allProcessInstances(parentProcessInstanceId);

		List<Task> allTasks = taskService.createTaskQuery().processInstanceIdIn(allProcessInstanceIds)
				.taskAssignee(userId).active().list();
		if (allTasks.size() > 1) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.TOO_MANY_TASKS);
		} else {
			if (allTasks.size() < 1) {
				response = new ApiResponse(errorcode, errorMessage);
			} else {
				/* can be child or can be main */
				// TODO: optimize this as much as possible
				ProcessInstance childProcessInstance = runtimeService.createProcessInstanceQuery()
						.processInstanceId(allTasks.get(0).getProcessInstanceId()).singleResult();
				HistoricProcessInstance parentProcessInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(parentProcessInstanceId).singleResult();
				Map<String, Object> processVariables = runtimeService.getVariables(childProcessInstance.getId());
				Map<String, Object> data = new HashMap<>();
				// checking if parent process instance Id is indeed parent
				// process instance
				if (parentProcessInstance.getSuperProcessInstanceId() != null) {
					parentProcessInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(parentProcessInstance.getSuperProcessInstanceId()).singleResult();
				}
				data.put(Constants.LOAN_APPLICATION_ID_KEY, parentProcessInstance.getBusinessKey());
				LinkedHashSet<Map<String, String>> completedTasks = null;
				if (processVariables.get(Constants.COMPLETED_TASK_KEY) == null) {
					completedTasks = new LinkedHashSet<>();
				} else {
					completedTasks = (LinkedHashSet<Map<String, String>>) processVariables
							.get(Constants.COMPLETED_TASK_KEY);
				}
				data.put(Constants.COMPLETED_TASK_KEY, completedTasks);
				SimpleFormModel formModel = getSimpleFormModel(allTasks.get(0), processVariables);
				Map<String, FormField> formFields = formModel.allFieldsAsMap();
				response = HelperFunctions.makeResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						parentProcessInstance.getId(), allTasks.get(0), data, formFields);
			}
		}
		return response;
	}

	@LogMethodDetails(userId = "userId")
	public ApiResponse startOrResumeProcess(String userId, String journeyName) {
		ApiResponse response;
		ProcessInstance processInstance = null;
		try {
			// checking if any process is deployed by given journey name or not
			if (repositoryService.createDeploymentQuery().processDefinitionKey(journeyName).count() < 1) {
				// no process deployed with given journey name
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						Constants.NO_PROCESS_DEPLOYED_FOR_GIVEN_KEY_MESSAGE);
			} else {
				// process is deployed by given journey name.
				// Checking for running process instances for current user for
				// given
				// journey name.
				List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
						.processDefinitionKey(journeyName).active().startedBy(userId).includeProcessVariables()
						.orderByProcessInstanceId().desc().list();
				if (!processInstanceList.isEmpty()) {
					// process exist for user. So will be returning the active
					// task
					// (if any) for the given user.
					processInstance = processInstanceList.get(0);
					response = getTaskData(userId, processInstance.getId(), HttpStatus.BAD_REQUEST,
							Constants.NO_TASKS_MESSAGE);
				} else {
					// no process exist for user. So starting new process.
					
					LOSUserModel user = losUserDao.findByIdAndIsDeleted(CommonHelperFunctions.getIdFromUserName(userId),
							false);
					if(user != null) {
					
					Map<String, Object> processVariable = new HashMap<>();
					processVariable.put(Constants.PROCESS_TASKS_ASSIGNEE_KEY, userId);
					processVariable.put(Constants.APPLICANT_MOBILE_NUMBER, user.getContactNumber());

					Authentication.setAuthenticatedUserId(userId);
					LOSApplicationNumber losApplicationNumber = new LOSApplicationNumber();
					losApplicationNumberDao.save(losApplicationNumber);
					processVariable.put(Constants.APPLICATION_ID_KEY, losApplicationNumber.getId());
					// here setting LOS application number as business key for
					// process instance. It will help in searching process later
					// on if we have business key.
					processInstance = runtimeService.startProcessInstanceByKey(journeyName,
							Long.toString(losApplicationNumber.getId()), processVariable);
					losApplicationNumber.setProcessInstanceId(processInstance.getId());
					losApplicationNumberDao.save(losApplicationNumber);

					
					
						LOSUserLoanDataModel loan = new LOSUserLoanDataModel();
						loan.setProcessInstanceId(processInstance.getId());
						loan.setLosApplicationNumber(losApplicationNumber);
						loan.setLosUserModel(user);
						loan.setApplicationCompleted(false);
						loan.setApplicationStage(APPLICATION_STAGE.NEW.toString());
						losUserLoanDataDao.save(loan);
						response = getTaskData(userId, processInstance.getId(), HttpStatus.BAD_REQUEST,
								Constants.NO_TASKS_MESSAGE);
					} else {
						response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
								Constants.INVALID_USER_ID_MESSAGE);
					}
				}
			}
			if(processInstance!=null)
				logger.info("Process Start or Resume for UserId {}, ProcessInstanceId {}, Business Key {}",
						userId, processInstance.getId(), processInstance.getBusinessKey());
			else
				logger.info("No Process Instance for User: {}", userId);
		} catch (Exception e) {
			logger.warn(Constants.LOG_EXCEPTION_USER_JOURNEY, userId, journeyName, e.getMessage());
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	@LogMethodDetails(userId = "initiator")
	public ApiResponse startOrResumeProcessFromPortal(String initiator, String journeyName, Map<String, Object> variablestoSave) {
		ApiResponse response;
		ProcessInstance processInstance = null;
		try {
			// checking if any process is deployed by given journey name or not
			if (repositoryService.createDeploymentQuery().processDefinitionKey(journeyName).count() < 1) {
				// no process deployed with given journey name
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						Constants.NO_PROCESS_DEPLOYED_FOR_GIVEN_KEY_MESSAGE);
			} else {
				// process is deployed by given journey name.
				// Checking for running process instances for current user for
				// given
				// journey name.
			variablestoSave.put(Constants.PROCESS_TASKS_ASSIGNEE_KEY, initiator);

			Authentication.setAuthenticatedUserId(initiator);
			LOSApplicationNumber losApplicationNumber = new LOSApplicationNumber();
			losApplicationNumberDao.save(losApplicationNumber);
			variablestoSave.put(Constants.APPLICATION_ID_KEY, losApplicationNumber.getId());
			// here setting LOS application number as business key for
			// process instance. It will help in searching process later
			// on if we have business key.
			processInstance = runtimeService.startProcessInstanceByKey(journeyName,
					Long.toString(losApplicationNumber.getId()), variablestoSave);
			losApplicationNumber.setProcessInstanceId(processInstance.getId());
			losApplicationNumberDao.save(losApplicationNumber);
			response = getTaskData(initiator, processInstance.getId(), HttpStatus.BAD_REQUEST,
						Constants.NO_TASKS_MESSAGE);
				
			}
			if(processInstance!=null)
				logger.info("Process Start or Resume from Portal for UserId {}, ProcessInstanceId {}, Business Key {}",
						initiator, processInstance.getId(), processInstance.getBusinessKey());
			else
				logger.info("No Process Instance for User: {}", initiator);
		} catch (Exception e) {
			logger.warn(Constants.LOG_EXCEPTION_USER_JOURNEY, initiator, journeyName, e.getMessage());
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	
	@LogMethodDetails(userId = "userId", processInstanceId = "parentProcessInstanceId")
	public ApiResponse getFormData(String userId, String parentProcessInstanceId) {
		ApiResponse response;
		try {
			if (parentProcessInstanceId == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			} else {
				response = getTaskData(userId, parentProcessInstanceId, HttpStatus.BAD_REQUEST,
						Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			}
		} catch (Exception e) {
			logger.warn(Constants.LOG_EXCEPTION_USER_PROCESSINSTANCE, userId, parentProcessInstanceId, e.getMessage());
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	@LogMethodDetails(userId = "userId")
	public ApiResponse submitFormData(String userId, SubmitFormClass input) {
		ApiResponse response;
		try {
			Task task = taskService.createTaskQuery().taskAssignee(userId).active().taskId(input.getTaskId())
					.singleResult();
			if (task == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
			} else {
				
				 logger.info("Submitted task called for TaskName :"+task.getName()+",TaskId :"+input.getTaskId()+",ProcId : "+task.getProcessInstanceId());

				List<Execution> executionProcessInstance = runtimeService.createExecutionQuery()
						.processInstanceId(task.getProcessInstanceId()).list();
				if (!executionProcessInstance.get(0).getRootProcessInstanceId().equals(input.getProcessInstanceId())) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				} else {
					SimpleFormModel formModel = getSimpleFormModel(task, null);
					List<FormField> listOfFormProp = formModel.getFields();
					Map<String, Object> formPropertiesMap = (!input.getFormProperties().isEmpty()
							? input.getFormProperties()
							: new HashMap<String, Object>());
					Map<Object, Object> validationResult = Validation.submitValidation(listOfFormProp,
							formPropertiesMap, true);
					if (!(boolean) validationResult.get("status")) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								validationResult.get(Constants.MESSAGE_KEY).toString());
					} else {
						runtimeService.setVariables(task.getProcessInstanceId(), formPropertiesMap);
						taskService.complete(input.getTaskId());
						// String taskConfiguration = task.getDescription();
						// try {
						// JSONObject taskConfigurationObject = new JSONObject(taskConfiguration);
						// if (taskConfigurationObject.has(Constants.CAN_COMPLETE_TASK_KEY) &&
						// taskConfigurationObject
						// .get(Constants.CAN_COMPLETE_TASK_KEY) instanceof Boolean) {
						// if (taskConfigurationObject.getBoolean(Constants.CAN_COMPLETE_TASK_KEY)) {
						// taskService.complete(input.getTaskId());
						// } else {
						// logger.debug("Cannot Complete Task : " + task.getName());
						// }
						// }
						// } catch (JSONException e) {
						// logger.warn("Task Configuration for task : " + task.getName() + " is not a
						// valid JSON.");
						// }
						response = getTaskData(userId, input.getProcessInstanceId(), HttpStatus.OK,
								Constants.SUCCESS_MESSAGE);
					}
				}
			}
		} catch (Exception e) {
			logger.warn(Constants.LOG_EXCEPTION_USER_PROCESSINSTANCE, userId, input.getProcessInstanceId(), e.getMessage());
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;

	}

	@SuppressWarnings("unchecked")
	@LogMethodDetails(userId = "userId")
	public ApiResponse changeProcessState(String userId, BackTaskInput input, BindingResult result) {
		ApiResponse response;
		try {
			HistoricProcessInstance processInstance = null;
			response = checkErrors(result);
			if (response == null) {
				List<String> allProcessInstanceIds = allProcessInstances(input.getProcessInstanceId());
				if (allProcessInstanceIds.size() < 1) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				} else {
					Task task = taskService.createTaskQuery().taskId(input.getCurrentTaskId()).taskAssignee(userId)
							.active().singleResult();
					// Map<String, Object> processVariables =
					// runtimeService.getVariables(task.getProcessInstanceId());
					// LinkedHashSet<Map<String, String>> completedProcessTasks =
					// (LinkedHashSet<Map<String, String>>) processVariables
					// .get("completedProcessTasks");
					processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(task.getProcessInstanceId()).singleResult();
					List<HistoricTaskInstance> taskToGoTo = historyService.createHistoricTaskInstanceQuery()
							.processInstanceId(processInstance.getId())
							.taskDefinitionKey(input.getBackTaskDefinitionKey()).list();
					boolean flag = !taskToGoTo.isEmpty();
					if (!taskToGoTo.isEmpty()) {
						// Iterator<Map<String, String>> completedTasksIterator =
						// completedProcessTasks.iterator();
						// while (completedTasksIterator.hasNext()) {
						// Map<String, String> taskInfo = completedTasksIterator.next();
						// if
						// (taskInfo.get("taskDefinitionKey").equals(input.getBackTaskDefinitionKey())
						// && !flag) {
						// completedTasksIterator.remove();
						// flag = true;
						// } else if (flag) {
						// completedTasksIterator.remove();
						// }
						// }
						// if (flag) {
						// if
						// (input.getBackTaskDefinitionKey().equals(Constants.NO_OF_COBORROWERS_TASK_KEY))
						// {
						// runtimeService.removeVariable(processInstance.getId(),
						// Constants.COBORROWERS_LIST_KEY);
						// }
						// runtimeService.setVariable(processInstance.getId(),
						// Constants.COMPLETED_TASK_KEY,
						// completedProcessTasks);
						runtimeService.createChangeActivityStateBuilder().processInstanceId(task.getProcessInstanceId())
								.moveActivityIdTo(task.getTaskDefinitionKey(), input.getBackTaskDefinitionKey())
								.changeState();
						response = getTaskData(userId, input.getProcessInstanceId(), HttpStatus.BAD_REQUEST,
								Constants.INVALID_TASK_ID_MESSAGE);
						// }
					}
					if (taskToGoTo.isEmpty()) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.TASK_NOT_DONE_FOR_PROC_INST);
					} else if (!flag) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_BACK_TASK_DEFINITION_KEY);
					}
				}
			}
		} catch (Exception e) {
			logger.warn(Constants.LOG_EXCEPTION_USER_PROCESSINSTANCE, userId, input.getProcessInstanceId(), e.getMessage());
			logger.error(CommonHelperFunctions.getStackTrace(e));
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE, e);
		}
		return response;
	}

	// public ApiResponse login(Map<String, Object> requestBody){
	// ApiResponse response;
	// String username = requestBody.get("username").toString();
	// String password = requestBody.get("password").toString();
	// boolean result = identityService.checkPassword(username, password);
	// if (!result) {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_CREDENTIALS);
	// return response;
	// }
	// String signature = username.concat(":").concat(password);
	// String token = Base64.getEncoder().encodeToString(signature.getBytes());
	// Map<String, Object> data = new HashMap<String, Object>();
	// data.put("token", token);
	// response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
	// return response;
	//
	// }

	// @SuppressWarnings("unchecked")
	// public ApiResponse changeProcessStateNew(String userId, BackTaskInput
	// input, BindingResult result) {
	// ApiResponse response;
	// try {
	// try {
	// log(userId, input.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
	// JobType.INBOUND_API, null, null,
	// input);
	// } catch (JsonProcessingException e) {
	// logException(userId, input.getProcessInstanceId(),
	// LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API,
	// null, null, e, input);
	// }
	// response = checkErrors(result);
	// if (response == null) {
	// List<String> allProcessInstanceIds =
	// allProcessInstances(input.getProcessInstanceId());
	// if (allProcessInstanceIds.size() < 1) {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
	// } else {
	// Task task =
	// taskService.createTaskQuery().taskId(input.getCurrentTaskId()).taskAssignee(userId)
	// .active().singleResult();
	// if (task != null) {
	// String taskConfiguration = task.getDescription();
	// try {
	// JSONObject taskConfigurationObject = new JSONObject(taskConfiguration);
	// if (taskConfigurationObject.has(Constants.BACK_TASK_LIST_KEY)) {
	// if (taskConfigurationObject.get(Constants.BACK_TASK_LIST_KEY) instanceof
	// JSONArray) {
	// JSONArray backTaskArray = taskConfigurationObject
	// .getJSONArray(Constants.BACK_TASK_LIST_KEY);
	// List<Object> backTaskList = backTaskArray.toList();
	// if (backTaskList.contains(input.getBackTaskDefinitionKey())) {
	// runtimeService.createChangeActivityStateBuilder()
	// .processInstanceId(task.getProcessInstanceId())
	// .moveActivityIdTo(task.getTaskDefinitionKey(),
	// input.getBackTaskDefinitionKey())
	// .changeState();
	// response = getTaskData(userId, input.getProcessInstanceId(),
	// HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
	// } else {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_BACK_TASK_DEFINITION_KEY);
	// }
	// } else {
	// response = new ApiResponse(HttpStatus.EXPECTATION_FAILED,
	// Constants.BACK_TASK_LIST_NOT_ARRAY_MESSAGE);
	// }
	// } else {
	// response = new ApiResponse(HttpStatus.EXPECTATION_FAILED,
	// Constants.BACK_TASK_LIST_NOT_AVAILABLE_MESSAGE);
	// }
	// } catch (JSONException e) {
	// response = new ApiResponse(HttpStatus.EXPECTATION_FAILED,
	// Constants.TASK_CONFIGURATION_INVALID_JSON_MESSAGE);
	// }
	// } else {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_TASK_ID_MESSAGE);
	// }
	// }
	// }
	// try {
	// log(userId, input.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
	// JobType.INBOUND_API, "", "",
	// response);
	// } catch (JsonProcessingException e) {
	// logException(userId, input.getProcessInstanceId(),
	// LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
	// "", "", e, response);
	// }
	// } catch (Exception e) {
	// logException(userId, input.getProcessInstanceId(), LogType.EXCEPTION,
	// JobType.INBOUND_API, "", "", e,
	// input);
	// response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
	// Constants.SOMETHING_WRONG_MESSAGE);
	// }
	// return response;
	// }

	/**
	 * This functions returns the access and refresh tokens for normal user on
	 * successful login.
	 * 
	 * @param customerLoginInput
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse loginUser(CustomerLoginInput customerLoginInput) {
		ApiResponse response = null;
		logger.info("Login User with mobile: {}", customerLoginInput.getMobile());
		LOSUserModel user = losUserDao.findByContactNumberAndIsDeleted(customerLoginInput.getMobile(), false);
		if (user == null) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		} else {
			String keycloakId = "user@" + user.getId() + ".com";
			// String password = Base64.encodeBase64String(keycloakId.getBytes());
			String password = AuthUtils.textEncoding(keycloakId);
			response = keycloakManager.loginWithEmailAndPassword(keycloakId, password, false);
		}
		logger.info("Login Response for user mobile {} is: {}", customerLoginInput.getMobile(), response);
		return response;
	}

	/**
	 * This functions copies all variables of root process to sub processes and vice
	 * versa.
	 * 
	 * @param execution
	 * @param copyToRoot
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution copyVariablesFromToRootProcess(Execution execution, boolean copyToRoot) {
		logger.info(Constants.LOG_START_METHOD, Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		String processInstanceId = execution.getProcessInstanceId();
		String rootProcessInstanceId = execution.getRootProcessInstanceId();
		if (processInstanceId != null && rootProcessInstanceId != null) {
			if (copyToRoot) {
				runtimeService.setVariables(rootProcessInstanceId, runtimeService.getVariables(processInstanceId));
			} else {
				runtimeService.setVariables(processInstanceId, runtimeService.getVariables(rootProcessInstanceId));
			}
		}
		logger.info(Constants.LOG_COMPLETE_METHOD,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		return execution;
	}

	/**
	 * This functions that updates user basic information in table los_user from
	 * data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserData(Execution execution) {
		logger.info(Constants.LOG_START_METHOD, Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		Long userId = null;
		if (processVariables.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
			userId = CommonHelperFunctions.getIdFromUserName(
					CommonHelperFunctions.getStringValue(processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY)));
			LOSUserModel user = losUserDao.findByIdAndIsDeleted(userId, false);
			if (user != null) {
				if (processVariables.containsKey(Constants.BORROWER_FULL_NAME_KEY)) {
					user.setName(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.BORROWER_FULL_NAME_KEY)));
				}
				if (processVariables.containsKey(Constants.BORROWER_AGE_KEY)) {
					user.setAge(
							CommonHelperFunctions.getIntegerValue(processVariables.get(Constants.BORROWER_AGE_KEY)));
				}
				if (processVariables.containsKey(Constants.DATE_OF_BIRTH_KEY)) {
					user.setDateOfBirth(
							CommonHelperFunctions.getStringValue(processVariables.get(Constants.DATE_OF_BIRTH_KEY)));
				}
				if (processVariables.containsKey(Constants.GENDER_KEY)) {
					user.setSex(CommonHelperFunctions.getStringValue(processVariables.get(Constants.GENDER_KEY)));
				}
				if (processVariables.containsKey(Constants.EMAIL_ID_KEY)) {
					user.setEmail(CommonHelperFunctions.getStringValue(processVariables.get(Constants.EMAIL_ID_KEY)));
				}

				losUserDao.save(user);
			}
		}
		logger.info(Constants.LOG_COMPLETE_METHOD,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		return execution;
	}

	/**
	 * This functions that updates user loan information in table los_user_loan_data
	 * from data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserLoanData(Execution execution) {
		logger.info(Constants.LOG_START_METHOD, Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		LOSUserLoanDataModel loan = losUserLoanDataDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if (loan != null) {
			if (processVariables.containsKey(Constants.LOAN_AMOUNT_KEY)) {
				loan.setLoanAmount(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.LOAN_AMOUNT_KEY)));
			}
			if (processVariables.containsKey(Constants.TENURE_KEY)) {
				loan.setTenure(CommonHelperFunctions.getIntegerValue(processVariables.get(Constants.TENURE_KEY)));
			}
			if (processVariables.containsKey(Constants.APPLICATION_START_DATE)) {
				loan.setApplicationDate(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.APPLICATION_START_DATE)));
			}
			if (processVariables.containsKey(Constants.LOAN_EMI_KEY)) {
				loan.setEmi(CommonHelperFunctions.getStringValue(processVariables.get(Constants.LOAN_EMI_KEY)));
			}
			if (processVariables.containsKey(Constants.DISBURSAL_DATE)) {
				loan.setDisbursementDate(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_DATE)));
			}
			if (processVariables.containsKey(Constants.DISBURSAL_CHANNEL)) {
				loan.setDisbursementChannel(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_CHANNEL)));
			}
			if (processVariables.containsKey(Constants.DISBURSEMENT_STATUS_CODE)) {
				loan.setDisbursementStatus(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSEMENT_STATUS_CODE)));
			}
			losUserLoanDataDao.save(loan);
		}
		logger.info(Constants.LOG_COMPLETE_METHOD,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		return execution;
	}

	/**
	 * This functions that updates user address information in table
	 * los_user_address from data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserAddressData(Execution execution) {
		logger.info(Constants.LOG_START_METHOD, Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		Long userId = null;
		if (processVariables.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
			userId = CommonHelperFunctions.getIdFromUserName(
					CommonHelperFunctions.getStringValue(processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY)));
			LOSUserModel user = losUserDao.findByIdAndIsDeleted(userId, false);
			if (user != null) {
				LOSUserAddressModel permanentAddress = losUserAddressDao.findByLosUserModelAndAddressType(user,
						Enums.AddressType.PERMANENT.toString());
				if (permanentAddress == null) {
					permanentAddress = new LOSUserAddressModel();
				}
				permanentAddress.setAddressLine1(CommonHelperFunctions
						.getStringValue(processVariables.get(Constants.PERMANENT_ADDRESS_LINE_1_KEY)));
				permanentAddress.setAddressLine2(CommonHelperFunctions
						.getStringValue(processVariables.get(Constants.PERMANENT_ADDRESS_LINE_2_KEY)));
				permanentAddress.setAddressType(Enums.AddressType.PERMANENT.toString());
				permanentAddress.setCityDistrict(CommonHelperFunctions
						.getStringValue(processVariables.get(Constants.PERMANENT_CITY_DISTRICT_KEY)));
				permanentAddress.setState(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.PERMANENT_PROVINCE_KEY)));
				permanentAddress.setLosUserModel(user);
				losUserAddressDao.save(permanentAddress);

				if (!processVariables.containsKey(Constants.TEMPORARY_ADDRESS_SAME_AS_PERMANENT_ADDRESS_KEY)
						|| !CommonHelperFunctions.getBooleanValue(
								processVariables.get(Constants.TEMPORARY_ADDRESS_SAME_AS_PERMANENT_ADDRESS_KEY))) {
					LOSUserAddressModel temporaryAddress = losUserAddressDao.findByLosUserModelAndAddressType(user,
							Enums.AddressType.CURRENT.toString());
					if (temporaryAddress == null) {
						temporaryAddress = new LOSUserAddressModel();
					}
					temporaryAddress.setAddressLine1(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_ADDRESS_LINE_1_KEY)));
					temporaryAddress.setAddressLine2(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_ADDRESS_LINE_2_KEY)));
					temporaryAddress.setAddressType(Enums.AddressType.CURRENT.toString());
					temporaryAddress.setCityDistrict(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_CITY_DISTRICT_KEY)));
					temporaryAddress.setState(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_PROVINCE_KEY)));
					temporaryAddress.setLosUserModel(user);
					losUserAddressDao.save(temporaryAddress);
				}

			}
		}
		logger.info(Constants.LOG_COMPLETE_METHOD,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		return execution;
	}

	/**
	 * This functions that updates user employment information in table
	 * los_user_employment from data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserEmploymentData(Execution execution) {
		logger.info(Constants.LOG_START_METHOD, Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		Long userId = null;
		if (processVariables.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
			userId = CommonHelperFunctions.getIdFromUserName(
					CommonHelperFunctions.getStringValue(processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY)));
			LOSUserModel user = losUserDao.findByIdAndIsDeleted(userId, false);
			if (user != null) {
				LOSUserEmploymentModel employmentDetails = losUserEmploymentDao.findByLosUserModel(user);
				if (employmentDetails == null) {
					employmentDetails = new LOSUserEmploymentModel();
				}
				employmentDetails.setCompanyName(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.COMPANY_NAME_KEY)));
				employmentDetails.setCompanyType(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.COMPANY_TYPE_KEY)));
				employmentDetails
						.setEmploymentStatus(CommonHelperFunctions.getStringValue(Enums.EmploymentStatus.SALARIED));
				employmentDetails.setOccupation(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.OCCUPATION_KEY)));
				employmentDetails.setPosition(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DESIGNATION_KEY)));
				employmentDetails.setLosUserModel(user);
				losUserEmploymentDao.save(employmentDetails);
			}
		}
		logger.info(Constants.LOG_COMPLETE_METHOD,
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				execution.getProcessInstanceId(), execution.getId());
		return execution;
	}
	
	/**
	 * @param businessKey:
	 *            applicationId for the user
	 * @param journeyType:
	 *            Process Definition Key
	 * 
	 * @return processInstanceId
	 * 
	 *         The function takes processDefinitionKey and businessKey as input and
	 *         returns processInstanceId
	 * 
	 **/
	public String getProcessInstanceId(String businessKey, String journeyType) {
		String processInstanceId = "";
		List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
				.processInstanceBusinessKey(businessKey, journeyType).desc().list();
		if(!processInstanceList.isEmpty()) {
			ProcessInstance processInstance = processInstanceList.get(0);
			processInstanceId = processInstance.getProcessInstanceId();
		}
		return processInstanceId;
	}

	/**
	 * Function updates the processVariables for the given businessKey from the POJO
	 * BackToJourneyInput
	 * 
	 * @param backToJourneyInput
	 * 
	 * @return ApiResponse
	 * 
	 **/

	public ApiResponse setUserData(BackToJourneyInput backToJourneyInput) {
		String processInstanceId = backToJourneyInput.getProcessInstanceId();
		ApiResponse response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
		if (!processInstanceId.isEmpty()) {
			runtimeService.setVariables(processInstanceId, backToJourneyInput.getVariablesToSave());
			if (backToJourneyInput.isCloseTask()) {
				for (String taskKey: backToJourneyInput.getTaskList()){
					if (taskKey != null && !taskKey.isEmpty()) {
						try {
							Task task = taskService.createTaskQuery().processInstanceId(processInstanceId)
									.active()
									.taskDefinitionKey(taskKey.trim())
									.singleResult();
							if (task != null) {
								taskService.complete(task.getId());
							}
						} catch (Exception exception) {
							exception.printStackTrace();
						}
					}
				}
			}
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		}
		return response;

	}

	public SimpleFormModel getSimpleFormModel(String key, Map<String, Object> processVariables) {
		return ((SimpleFormModel) formService.getFormModelWithVariablesByKey(key, "", processVariables).getFormModel());
	}

	public SimpleFormModel getSimpleFormModel(Task task, Map<String, Object> processVariables) {
		SimpleFormModel simpleFormModel = null;
		if (task.getFormKey() != null && !task.getFormKey().isEmpty()) {
			simpleFormModel = getSimpleFormModel(task.getFormKey(), processVariables);
		} else {
			simpleFormModel = (SimpleFormModel) taskService.getTaskFormModel(task.getId())
					.getFormModel();
		}
		return simpleFormModel;
	}
}


