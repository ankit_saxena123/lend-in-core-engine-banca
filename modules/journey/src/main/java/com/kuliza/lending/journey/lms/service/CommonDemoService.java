package com.kuliza.lending.journey.lms.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class CommonDemoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonDemoService.class);

    protected String baseUrl;
    protected Map<String, String> defaultHeaders;
    protected String defaultDateFormat;
    protected String defaultLocale;
    protected int defaultOfficeId;
    protected RuntimeService runtimeService;

    public CommonDemoService(RuntimeService runtimeService){
        this.runtimeService = runtimeService;
        this.initDefault();
    }

    private void initDefault(){
        this.defaultHeaders = new HashMap<>();
        defaultHeaders.put("Authorization", "Basic bWlmb3M6cGFzc3dvcmQ=");
        defaultHeaders.put("Content-Type", "application/json");
        defaultHeaders.put("Fineract-Platform-TenantId", "default");

        this.baseUrl = "https://lms-demo.getlend.in/fineract-provider/api/v1/";
        this.defaultDateFormat = "dd MMMM yyyy";
        this.defaultLocale = "en";
        this.defaultOfficeId = 2;
    }

    public static String convertDate(Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String convertDate(String date, String fromFormat,String toFormat){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromFormat);
        try {
            Date parsedDate = simpleDateFormat.parse(date);
            return convertDate(parsedDate, toFormat);
        }catch (ParseException e){
            return "";
        }
    }

    protected void createDatatableForClient(DelegateExecution execution, String cId) throws Exception{

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("first", "Active");
        requestBody.put("second", "1");
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);

        try{

            HttpResponse<String> response = Unirest.post(this.baseUrl + "datatables/one/"
                    + String.valueOf(cId))
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("createDatatableForClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    protected void activateClient(DelegateExecution execution, String cId) throws Exception{


        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("activationDate",  convertDate(new Date(), defaultDateFormat));
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);

        try{
            LOGGER.debug("request body activateClient: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"clients/"
                    + cId + "?command=activate")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("activateClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("activateClient api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }
}
