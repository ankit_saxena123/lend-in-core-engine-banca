package com.kuliza.lending.journey.model;

import com.kuliza.lending.common.model.BaseModel;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

@MappedSuperclass
public class AbstractOTPDetails extends BaseModel {

  private static final long ONE_MINUTE_IN_MILLIS = 60000;
  @Column
  private String otpHash;
  @Column(nullable = false)
  private String userIdentifier;
  @Column
  private String deviceIdentifier;
  @Column(nullable = false)
  private String secretKey;
  @Type(type = "org.hibernate.type.NumericBooleanType")
  @ColumnDefault("0")
  @Column(nullable = false)
  private boolean blockedOnGenerate = false;
  @Type(type = "org.hibernate.type.NumericBooleanType")
  @ColumnDefault("0")
  @Column(nullable = false)
  private boolean blockedOnValidate = false;
  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date userBlockTime;
  @Column
  private long otpGenerationTime;
  @Column
  private int otpGenerateCount = 1;
  @Column
  private int otpValidateCount = 0;

  public String getOtpHash() {
    return otpHash;
  }

  public void setOtpHash(String otpHash) {
    this.otpHash = otpHash;
  }

  public String getUserIdentifier() {
    return userIdentifier;
  }

  public void setUserIdentifier(String userIdentifier) {
    this.userIdentifier = userIdentifier;
  }

  public String getDeviceIdentifier() {
    return deviceIdentifier;
  }

  public void setDeviceIdentifier(String deviceIdentifier) {
    this.deviceIdentifier = deviceIdentifier;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public boolean isBlockedOnGenerate() {
    return blockedOnGenerate;
  }

  public void setBlockedOnGenerate(boolean blockedOnGenerate) {
    this.blockedOnGenerate = blockedOnGenerate;
  }

  public boolean isBlockedOnValidate() {
    return blockedOnValidate;
  }

  public void setBlockedOnValidate(boolean blockedOnValidate) {
    this.blockedOnValidate = blockedOnValidate;
  }

  public Date getUserBlockTime() {
    return userBlockTime;
  }

  public void setUserBlockTime(int addMinutesToBlockTime) {
    Date currentDate = new Date();
    this.userBlockTime = new Date(
        currentDate.getTime() + (addMinutesToBlockTime * ONE_MINUTE_IN_MILLIS));
  }

  public int getOtpGenerateCount() {
    return otpGenerateCount;
  }

  public void setOtpGenerateCount(int otpGenerateCount) {
    this.otpGenerateCount = otpGenerateCount;
  }

  public int getOtpValidateCount() {
    return otpValidateCount;
  }

  public void setOtpValidateCount(int otpValidateCount) {
    this.otpValidateCount = otpValidateCount;
  }

  public long getOtpGenerationTime() {
    return otpGenerationTime;
  }

  public void setOtpGenerationTime(long otpGenerationTime) {
    this.otpGenerationTime = otpGenerationTime;
  }

  public AbstractOTPDetails() {
    super();
    this.setIsDeleted(false);
  }

  public AbstractOTPDetails(String otpHash, String userIdentifier, String deviceIdentifier,
      String secretKey, long otpGenerationTime) {
    super();
    this.setIsDeleted(false);
    this.otpHash = otpHash;
    this.userIdentifier = userIdentifier;
    this.deviceIdentifier = deviceIdentifier;
    this.secretKey = secretKey;
    this.otpGenerationTime = otpGenerationTime;
  }

  public AbstractOTPDetails(String userIdentifier, String deviceIdentifier,
      String secretKey, long otpGenerationTime) {
    super();
    this.setIsDeleted(false);
    this.userIdentifier = userIdentifier;
    this.deviceIdentifier = deviceIdentifier;
    this.secretKey = secretKey;
    this.otpGenerationTime = otpGenerationTime;
  }


}
