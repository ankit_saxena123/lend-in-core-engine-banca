package com.kuliza.lending.journey.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.BackToJourneyInput;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.InitiateFromPortalInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.service.UserJourneyServices;

@RestController
@RequestMapping("/lending/journey")
public class UserJourneyControllers {
	
	private static final Logger logger = LoggerFactory.getLogger(UserJourneyServices.class);

	@Autowired
	private UserJourneyServices userJourneyServices;

	@RequestMapping(method = RequestMethod.GET, value = "/initiate")
	public ResponseEntity<Object> initiateProcess(Principal principal,
			@RequestParam(required = true, value = "journeyName") String requestJourneyName) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.startOrResumeProcess(principal.getName(), requestJourneyName));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/submit-form")
	public ResponseEntity<Object> allSubmit(Principal principal, @RequestBody SubmitFormClass input) {
		
		// logs added
		ObjectMapper map = new ObjectMapper();
		try {
			logger.info("task submited input ------>"+map.writeValueAsString(input));
		} catch (Exception e) {
			logger.error("error while print log in submiited api",e);
		}
		
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.submitFormData(principal.getName(), input));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/submit-form")
	public ResponseEntity<Object> getPresentState(Principal principal, @RequestParam String processInstanceId) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.getFormData(principal.getName(), processInstanceId));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/back")
	public ResponseEntity<Object> moveBack(Principal principal, @Valid @RequestBody BackTaskInput input,
			BindingResult result) {
		return CommonHelperFunctions
				.buildResponseEntity(userJourneyServices.changeProcessState(principal.getName(), input, result));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ResponseEntity<Object> loginUser(@RequestBody @Valid CustomerLoginInput customerLoginInput) {
		return CommonHelperFunctions.buildResponseEntity(userJourneyServices.loginUser(customerLoginInput));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/back-to-journey")
	public ResponseEntity<Object> backToJourney(@RequestBody @Valid BackToJourneyInput backToJourneyInput) {
		return CommonHelperFunctions.buildResponseEntity(userJourneyServices.setUserData(backToJourneyInput));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/initiate-by-portal")
	public ResponseEntity<Object> initiateProcessByPortal(
			@RequestBody @Valid InitiateFromPortalInput initiateFromPortalInput) {
		return CommonHelperFunctions.buildResponseEntity(
				userJourneyServices.startOrResumeProcessFromPortal(initiateFromPortalInput.getInitiator(),
						initiateFromPortalInput.getJourneyKey(), initiateFromPortalInput.getVariablesToSave()));
	}

}
