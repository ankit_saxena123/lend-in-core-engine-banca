package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.kuliza.lending.common.model.BaseModel;

@MappedSuperclass
public class AbstractWorkFlowApplication extends BaseModel {

	public AbstractWorkFlowApplication() {
		super();
		this.setIsDeleted(false);
	}


}
