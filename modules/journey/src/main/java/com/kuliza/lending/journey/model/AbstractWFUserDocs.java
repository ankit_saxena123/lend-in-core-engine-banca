package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@MappedSuperclass
public class AbstractWFUserDocs extends BaseModel {

	@Column(nullable = false)
	private String userId;

	@Column(nullable = false, unique = true)
	private String documentId;

	@Column(nullable = true)
	private String documentLabel;

	@Column(nullable = true)
	private String documentType;

	@Column(nullable = true)
	private String docExtension;

	@Column(nullable = true)
	private String docName;

	public AbstractWFUserDocs() {
		super();
		this.setIsDeleted(false);
	}

	public AbstractWFUserDocs(String userId, String documentId,
			String documentLabel, String documentType, String docExtension,
			String docName) {
		super();
		this.userId = userId;
		this.documentId = documentId;
		this.documentLabel = documentLabel;
		this.documentType = documentType;
		this.docExtension = docExtension;
		this.docName = docName;
		this.setIsDeleted(false);
	}

	public String getDocExtension() {
		return docExtension;
	}

	public void setDocExtension(String docExtension) {
		this.docExtension = docExtension;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentLabel() {
		return documentLabel;
	}

	public void setDocumentLabel(String documentLabel) {
		this.documentLabel = documentLabel;
	}

}
