package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@MappedSuperclass
public class AbstractWorkFlowUser extends BaseModel {

	@Column(nullable = true)
	private String mobileNumber;

	@Column(nullable = true)
	private String email;

	@Column(nullable = true)
	private String username;

	@Column(nullable = true)
	private String idmUserName;

	public AbstractWorkFlowUser() {
		super();
		this.setIsDeleted(false);
	}

	public AbstractWorkFlowUser(String mobileNumber, String email,
			String username, String idmUserName) {
		super();
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.username = username;
		this.idmUserName = idmUserName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIdmUserName() {
		return idmUserName;
	}

	public void setIdmUserName(String idmUserName) {
		this.idmUserName = idmUserName;
	}
}