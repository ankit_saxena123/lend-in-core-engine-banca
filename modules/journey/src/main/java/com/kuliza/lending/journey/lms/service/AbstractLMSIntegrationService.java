package com.kuliza.lending.journey.lms.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.kuliza.lending.journey.lms.service.CommonDemoService.convertDate;

public class AbstractLMSIntegrationService {

    protected String baseUrl;
    protected String tenant;
    protected String token;
    protected String officeId;
    protected Map<String, String> defaultHeaders;
    protected String defaultDateFormat;
    public static String staticDefaultDateFormat;
    protected String defaultLocale;
    protected int defaultOfficeId;

    public AbstractLMSIntegrationService(){
        this.initDefault();
    }

    private void initDefault(){
        this.defaultHeaders = new HashMap<>();
//        defaultHeaders.put("Authorization", "Basic bWlmb3M6cGFzc3dvcmQ=");
        defaultHeaders.put("Content-Type", "application/json");
//        defaultHeaders.put("Fineract-Platform-TenantId", "default");
        this.officeId = "1";
        this.baseUrl = "https://lms-demo.getlend.in/fineract-provider/api/v1/";
        this.tenant = "default";
        this.token = "bWlmb3M6cGFzc3dvcmQ=";
        this.defaultDateFormat = "dd MMMM yyyy";
        staticDefaultDateFormat = defaultDateFormat;
        this.defaultLocale = "en";
        this.defaultOfficeId = 2;
    }

    public static String convertDate(Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String convertDate(String date, String fromFormat,String toFormat){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromFormat);
        try {
            Date parsedDate = simpleDateFormat.parse(date);
            return convertDate(parsedDate, toFormat);
        }catch (ParseException e){
            return "";
        }
    }

    private void putDefaultValues(Map<String, Object> requestBody){
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
    }

    public JSONObject createClient(String clientType, Map<String, Object> partialRequestBody, String baseUrl, String tenant,
                                   String token, String officeId) throws Exception {

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("address", new JSONArray());
        requestBody.put("familyMembers", new JSONArray());
        requestBody.put("officeId", Integer.parseInt(officeId));
        requestBody.put("active", false);
        requestBody.put("activationDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("savingsProductId", null);
        requestBody.putAll(partialRequestBody);
        this.putDefaultValues(requestBody);
        System.out.println("create LMS client:BASE-URL:: " + baseUrl);
        System.out.println("create LMS client:REQUEST-BODY:: " + new JSONObject(requestBody).toString());
        Map<String, String> headers = new HashMap<>(defaultHeaders);
        headers.put("Authorization", "Basic " + token);
        headers.put("Fineract-Platform-TenantId", tenant);
        System.out.println("create LMS client:REQUEST-HEADERS:: " + new JSONObject(headers).toString());
        HttpResponse<String> response = Unirest.post(baseUrl + "clients")
                .headers(headers)
                .body(new JSONObject(requestBody)).asString();
        System.out.println("create LMS client:RESPONSE-BODY:: " + response.getBody());

        if (response.getStatus() != 200)
            throw new RuntimeException(response.getBody());
        JSONObject body = new JSONObject(response.getBody());
        String clientId = body.optString("clientId");
        JSONObject result = new JSONObject();
        result.put("clientId", clientId);
        result.put("clientType", clientType);
        return result;
    }


    public void activateClient(String clientId, String baseUrl, String tenant,
                               String token, String officeId) throws Exception{
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("activationDate",  convertDate(new Date(), defaultDateFormat));
        this.putDefaultValues(requestBody);

        System.out.println("activate LMS client:BASE-URL:: " + baseUrl);
        System.out.println("activate LMS client:REQUEST-BODY:: " + new JSONObject(requestBody).toString());
        Map<String, String> headers = new HashMap<>(defaultHeaders);
        headers.put("Authorization", "Basic " + token);
        headers.put("Fineract-Platform-TenantId", tenant);
        System.out.println("activate LMS client:REQUEST-HEADERS:: " + new JSONObject(headers).toString());
        HttpResponse<String> response = Unirest.post(baseUrl+"clients/"
                + clientId + "?command=activate")
                .headers(headers)
                .body(new JSONObject(requestBody)).asString();
        System.out.println("activate LMS client:RESPONSE-BODY:: " + response.getBody());

        if (response.getStatus() != 200)
            throw new RuntimeException(response.getBody());

    }

    public void createDataTable(String clientId, String dataTableName,
                                Map<String, Object> partialRequestBody, String baseUrl, String tenant,
                                String token, String officeId) throws Exception{
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.putAll(partialRequestBody);
        this.putDefaultValues(requestBody);
        System.out.println("create LMS datatable:BASE-URL:: " + baseUrl);
        System.out.println("create LMS datatable:DATATABLE:: " + dataTableName +
                " :REQUEST-BODY:: " + new JSONObject(requestBody).toString());
        Map<String, String> headers = new HashMap<>(defaultHeaders);
        headers.put("Authorization", "Basic " + token);
        headers.put("Fineract-Platform-TenantId", tenant);
        System.out.println("create LMS datatable:REQUEST-HEADERS:: " + new JSONObject(headers).toString());
        HttpResponse<String> response = Unirest.post(baseUrl+"datatables/"+ dataTableName +"/"
                + String.valueOf(clientId))
                .headers(headers)
                .body(new JSONObject(requestBody)).asString();
        System.out.println("create LMS datatable:RESPONSE-BODY:: " + response.getBody());
        if (response.getStatus() != 200)
            throw new RuntimeException(response.getBody());
    }


    public JSONObject createLoan(Integer productId, Map<String, Object> partialRequestBody, String baseUrl, String tenant,
                                 String token, String officeId) throws Exception {

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("productId", productId);
        requestBody.put("loanTermFrequencyType", 2);
        requestBody.put("repaymentEvery", 1);
        requestBody.put("repaymentFrequencyType", 2);
        requestBody.put("isEqualAmortization", false);
        requestBody.put("interestType", 0);
        requestBody.put("interestCalculationPeriodType", 1);
        requestBody.put("allowPartialPeriodInterestCalcualtion", true);
        requestBody.put("transactionProcessingStrategyId", 1);
        requestBody.put("loanType", "individual");
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.putAll(partialRequestBody);
        this.putDefaultValues(requestBody);
        System.out.println("create LMS loan:BASE-URL:: " + baseUrl);
        System.out.println("create LMS loan:REQUEST-BODY:: " + new JSONObject(requestBody).toString());
        Map<String, String> headers = new HashMap<>(defaultHeaders);
        headers.put("Authorization", "Basic " + token);
        headers.put("Fineract-Platform-TenantId", tenant);
        System.out.println("create LMS loan:REQUEST-HEADERS:: " + new JSONObject(headers).toString());
        HttpResponse<String> response = Unirest.post(baseUrl + "loans")
                .headers(headers)
                .body(new JSONObject(requestBody)).asString();
        System.out.println("create LMS loan:RESPONSE-BODY:: " + response.getBody());

        if (response.getStatus() != 200)
            throw new RuntimeException(response.getBody());
        JSONObject body = new JSONObject(response.getBody());
        String loanId = body.optString("loanId");
        JSONObject result = new JSONObject();
        result.put("loanId", loanId);
        result.put("productId", String.valueOf(productId));
        return result;
    }
}
