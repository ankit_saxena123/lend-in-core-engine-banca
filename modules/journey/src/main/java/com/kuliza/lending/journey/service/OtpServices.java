package com.kuliza.lending.journey.service;

import com.kuliza.lending.common.annotations.LogMethodDetails;

import javax.servlet.http.HttpServletRequest;

import com.kuliza.lending.utils.AuthUtils;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.model.LOSUserDao;
import com.kuliza.lending.journey.model.LOSUserModel;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.OtpLoginInputform;

@Service
public class OtpServices {

	private static final Logger logger = LoggerFactory.getLogger(OtpServices.class);

	@Autowired
	private LOSUserDao losUserDao;

	@Autowired
	private KeyCloakManager keycloakManager;

	/**
	 * This functions creates returns an existing user with given mobile number or
	 * creates a new user if not already present in table los_user and returns that
	 * user.
	 * 
	 * @param mobileNumber
	 * @return LOSUserModel
	 * @throws Exception
	 * 
	 * @author Kunal Arneja
	 */
	public LOSUserModel createOrUpdateUser(String mobileNumber) throws Exception {
		LOSUserModel userData = losUserDao.findByContactNumberAndIsDeleted(mobileNumber, false);
		if (userData == null) {
			userData = new LOSUserModel();
			userData.setContactNumber(mobileNumber);
			losUserDao.save(userData);
		}
		return userData;
	}

	/**
	 * This functions generates a OTP and sends it to user's mobile.
	 *
	 * @return ApiResponse
	 * @throws Exception
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse generateOTP(HttpServletRequest request, CustomerLoginInput customerLoginInput) throws Exception {
		logger.info("Generate OTP for mobile : {}", customerLoginInput.getMobile());
		ApiResponse apiResponse = null;
		// TODO GENERATE AND SEND OTP
		apiResponse = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, "OTP sent successfully!");
		logger.info("Generate OTP Response {} for mobile {}", apiResponse, customerLoginInput.getMobile());
		return apiResponse;

	}

	/**
	 * This functions fetch auth token for the given user, creates a new user if
	 * user not already present and returns tokens for the given user.
	 *
	 * @param uniqueIdentifier
	 * @return ApiResponse
	 * @throws Exception
	 *
	 * @author Mandip Gothadiya
	 */
	public ApiResponse getUserTokenWithoutOTPValidation(String uniqueIdentifier) throws Exception {
		OtpLoginInputform otpLoginInputform = new OtpLoginInputform();
		otpLoginInputform.setMobile(uniqueIdentifier);
		return validateOtpAndCreateOrUpdateUser(null, otpLoginInputform, true);
	}

	/**
	 * This functions validates the OTP for the given user, creates a new user if
	 * user not already present and returns tokens for the given user.
	 * 
	 * @param otpLoginInputform
	 * @return ApiResponse
	 * @throws Exception
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails
	public ApiResponse validateOtpAndCreateOrUpdateUser(HttpServletRequest request, OtpLoginInputform otpLoginInputform, Boolean isAuthorised)
			throws Exception {
		ApiResponse apiResponse = null;
		try {
			logger.info("Validate: {} with request {}", Thread.currentThread().getStackTrace()[1].getMethodName(), otpLoginInputform);

			if (!isAuthorised) {
				// TODO Validate OTP and get success or return with Error Message.
			}
			logger.debug("Creating/Updating user for mobile number : " + otpLoginInputform.getMobile());
			LOSUserModel userData = losUserDao.findByContactNumberAndIsDeleted(otpLoginInputform.getMobile(), false);
			boolean isNewUser = false;
			if (userData == null) {
				isNewUser = true;
				userData = createOrUpdateUser(otpLoginInputform.getMobile());
			}
			if (userData != null) {
				String keycloakId = "user@" + userData.getId() + ".com";
				// String password = Base64.encodeBase64String(keycloakId.getBytes());
				String password = AuthUtils.textEncoding(keycloakId);
				ApiResponse registerResponse = null;
				if (isNewUser) {
					try {
						registerResponse = keycloakManager.createUserWithRole(keycloakId, password, "user");

					} catch (Exception e) {
						logger.warn("Raised Exception for mobile: {}", otpLoginInputform.getMobile());
						logger.error(CommonHelperFunctions.getStackTrace(e));
						apiResponse = keycloakManager.loginWithEmailAndPassword(keycloakId, password, false);

					}
				}
				if (registerResponse == null || registerResponse.getStatus() == 200) {
					apiResponse = keycloakManager.loginWithEmailAndPassword(keycloakId, password, false);
				} else {
					apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);

				}
			} else {
				apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);

			}

		}

		catch (Exception e) {
			logger.warn("Raised Exception for mobile: {}", otpLoginInputform.getMobile());
			logger.error(CommonHelperFunctions.getStackTrace(e));
			apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return apiResponse;

	}
}