package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import com.kuliza.lending.common.model.BaseModel;
import com.kuliza.lending.common.utils.Constants.SourceType;

@MappedSuperclass
public class AbstractWorkFlowSource extends BaseModel {

	@Column(nullable = false)
	private String sourceId;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private SourceType sourceType;
	
	@Column(nullable = false)
	private String sourceName;	

	public AbstractWorkFlowSource() {
		super();
		this.setIsDeleted(false);
	}

	public AbstractWorkFlowSource(String sourceId, SourceType sourceType,
			String sourceName) {
		super();
		this.sourceId = sourceId;
		this.sourceType = sourceType;
		this.sourceName = sourceName;
		this.setIsDeleted(false);
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	
}
