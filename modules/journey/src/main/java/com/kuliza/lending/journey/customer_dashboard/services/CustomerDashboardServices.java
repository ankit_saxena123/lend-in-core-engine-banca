package com.kuliza.lending.journey.customer_dashboard.services;

import com.kuliza.lending.common.annotations.LogMethodDetails;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.Enums.APPLICATION_STAGE;
import com.kuliza.lending.config_manager.services.ConfigServices;
import com.kuliza.lending.journey.customer_dashboard.pojo.CustomerDashboardApplicationApproved;
import com.kuliza.lending.journey.customer_dashboard.pojo.CustomerDashboardApplicationClosed;
import com.kuliza.lending.journey.customer_dashboard.pojo.CustomerDashboardApplicationDisbursed;
import com.kuliza.lending.journey.customer_dashboard.pojo.CustomerDashboardApplicationExpired;
import com.kuliza.lending.journey.customer_dashboard.pojo.CustomerDashboardDetails;
import com.kuliza.lending.journey.customer_dashboard.pojo.CustomerDashboardProfile;
import com.kuliza.lending.journey.model.LOSUserAddressDao;
import com.kuliza.lending.journey.model.LOSUserAddressModel;
import com.kuliza.lending.journey.model.LOSUserDao;
import com.kuliza.lending.journey.model.LOSUserEmploymentDao;
import com.kuliza.lending.journey.model.LOSUserEmploymentModel;
import com.kuliza.lending.journey.model.LOSUserLoanDataDao;
import com.kuliza.lending.journey.model.LOSUserLoanDataModel;
import com.kuliza.lending.journey.model.LOSUserModel;

@Service
public class CustomerDashboardServices {

	@Autowired
	ConfigServices configServices;

	@Autowired
	LOSUserLoanDataDao losUserLoanDataDao;

	@Autowired
	LOSUserDao losUserDao;

	@Autowired
	private LOSUserAddressDao losUserAddressDao;

	@Autowired
	private LOSUserEmploymentDao losUserEmploymentDao;

	@Autowired
	private HistoryService historyService;

	/**
	 * This functions return the notifications data for the given user.
	 * 
	 * @param userName
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails(userId = "userName")
	public ApiResponse getNotifications(String userName) {
		ApiResponse response = null;
		// TODO LMS Integration
		response = configServices.getConfigs("notifications_" + CommonHelperFunctions.getIdFromUserName(userName));
		return response;
	}

	/**
	 * This functions return the repayment schedule for the given user.
	 * 
	 * @param userName
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails(userId="userName")
	public ApiResponse getRepaymentSchedule(String userName) {
		ApiResponse response = null;
		// TODO LMS Integration
		response = configServices.getConfigs("repayment_" + CommonHelperFunctions.getIdFromUserName(userName));

		return response;
	}

	/**
	 * This functions return the general dashboard information for the given user.
	 * 
	 * @param userName
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails(userId = "userName")
	public ApiResponse getDashboardData(String userName) {
		ApiResponse response = null;
		LOSUserModel losUserModel = losUserDao.findByIdAndIsDeleted(
				CommonHelperFunctions.getLongValue(CommonHelperFunctions.getIdFromUserName(userName)), false);
		if (losUserModel != null) {
			LOSUserLoanDataModel losUserLoanDataModel = losUserLoanDataDao
					.findFirstByLosUserModelOrderByCreatedDesc(losUserModel);
			if (losUserLoanDataModel != null) {
				try {

					CustomerDashboardDetails customerDashboardDetails = new CustomerDashboardDetails();
					HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(losUserLoanDataModel.getProcessInstanceId()).includeProcessVariables()
							.singleResult();
					Map<String, Object> processVariables = new HashMap<>();

					if (processInstance != null) {
						processVariables = processInstance.getProcessVariables();
					}

					customerDashboardDetails.setBorrowerFullName(losUserModel.getName());
					customerDashboardDetails.setEmailId(losUserModel.getEmail());
					customerDashboardDetails.setContactNumber(losUserModel.getContactNumber());
					customerDashboardDetails.setCustomerSince(losUserModel.getCreated().toString());

					customerDashboardDetails.setDate(new Date().toString());
					APPLICATION_STAGE stage = APPLICATION_STAGE
							.valueOf(losUserLoanDataModel.getApplicationStage().toUpperCase());
					switch (stage) {
					case APPROVED:
					case CANCELLED:
					case EXPIRED:
						customerDashboardDetails.setRemainingTenure(CommonHelperFunctions
								.getStringValue(processVariables.get(Constants.FINAL_LOAN_TENURE)));
						customerDashboardDetails.setTotalAmount(CommonHelperFunctions
								.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
						customerDashboardDetails.setRemainingAmount(CommonHelperFunctions
								.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
						break;
					case REJECTED:
					case NEW:
						customerDashboardDetails.setRemainingTenure("");
						customerDashboardDetails.setTotalAmount("");
						customerDashboardDetails.setRemainingAmount("");
						break;
					case CLOSED:
						customerDashboardDetails.setRemainingTenure("0");
						customerDashboardDetails.setTotalAmount(CommonHelperFunctions
								.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
						customerDashboardDetails.setRemainingAmount("0");
						break;
					case DISBURSED:

						customerDashboardDetails.setRemainingTenure(CommonHelperFunctions.getStringValue(""));
						customerDashboardDetails.setRemainingAmount(CommonHelperFunctions.getStringValue(""));
						customerDashboardDetails.setTotalAmount(CommonHelperFunctions
								.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));

						break;
					}
					response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, customerDashboardDetails);

				} catch (IllegalArgumentException | NullPointerException e) {
					response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
				}

			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No data for " + "given user");
			}
		} else {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"No data for " + "given user");
		}
		return response;
	}

	/**
	 * This functions return the profile for the given user.
	 * 
	 * @param userName
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails(userId = "userName")
	public ApiResponse getProfileData(String userName) {
		ApiResponse response = null;
		try {
			LOSUserModel losUserModel = losUserDao
					.findByIdAndIsDeleted(CommonHelperFunctions.getIdFromUserName(userName), false);
			if (losUserModel != null) {
				List<LOSUserAddressModel> losUserAddresses = losUserAddressDao.findByLosUserModel(losUserModel);
				LOSUserEmploymentModel losUserEmployment = losUserEmploymentDao.findByLosUserModel(losUserModel);
				CustomerDashboardProfile profileData = new CustomerDashboardProfile(losUserModel, losUserAddresses,
						losUserEmployment);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, profileData);
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "user not found");
			}

		} catch (Exception e) {

			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	/**
	 * This functions return the settings for the given user.
	 * 
	 * @param userName
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails(userId = "userName")
	public ApiResponse getSettings(String userName) {
		ApiResponse response = null;
		try {
			LOSUserModel losUserModel = losUserDao
					.findByIdAndIsDeleted(CommonHelperFunctions.getIdFromUserName(userName), false);
			if (losUserModel != null) {
				Map<String, Object> data = new HashMap<>();
				data.put("language_preference", "NA");
				data.put("notification_preference", "NA");
				data.put("touch_id_enabled", "NA");
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE, "user not found");
			}

		} catch (Exception e) {

			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	/**
	 * This functions return all the applications for the given user.
	 * 
	 * @param userName
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails(userId = "userName")
	public ApiResponse getApplicationsData(String userName) {
		ApiResponse response = null;

		LOSUserModel losUserModel = losUserDao.findByIdAndIsDeleted(CommonHelperFunctions.getIdFromUserName(userName),
				false);
		if (losUserModel != null) {
			LOSUserLoanDataModel losUserLoanDataModel = losUserLoanDataDao
					.findFirstByLosUserModelOrderByCreatedDesc(losUserModel);
			if (losUserLoanDataModel != null) {
				try {
					APPLICATION_STAGE stage;
					String processInstanceId = "";
					List<LOSUserLoanDataModel> losUserLoanDataModellist = losUserLoanDataDao
							.findByLosUserModelOrderByCreatedDesc(losUserModel);
					boolean canApplyForNewLoan = true;
					for (LOSUserLoanDataModel losUserLoanData : losUserLoanDataModellist) {
						stage = APPLICATION_STAGE.valueOf(losUserLoanData.getApplicationStage().toUpperCase());
						if (stage.equals(APPLICATION_STAGE.NEW) || stage.equals(APPLICATION_STAGE.APPROVED)) {
							canApplyForNewLoan = false;
						}
						if (stage.equals(APPLICATION_STAGE.DISBURSED) || stage.equals(APPLICATION_STAGE.APPROVED))
							processInstanceId = losUserLoanData.getProcessInstanceId();
					}
					HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
					Map<String, Object> processVariables = new HashMap<>();
					if (processInstance != null) {
						processVariables = processInstance.getProcessVariables();
					}
					stage = APPLICATION_STAGE.valueOf(losUserLoanDataModel.getApplicationStage().toUpperCase());
					Map<String, Object> data = getLoanDetails(stage, processVariables);
					data.put("canApplyForNewLoan", canApplyForNewLoan);
					data.put("MyApplication", getMyApplicationDetails(losUserLoanDataModellist));
					data.put("MyLoans", getMyLoanDetails(losUserLoanDataModellist));

					response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
				} catch (ParseException e) {

					response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
				}

			} else {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						"No data for " + "given user");
			}
		} else {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
					"No data for " + "given user");
		}
		return response;
	}

	/**
	 * This functions return the list of applications for the given user.
	 * 
	 * @param losUserLoanDataModellist
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	@LogMethodDetails
	List<Object> getMyApplicationDetails(List<LOSUserLoanDataModel> losUserLoanDataModellist) throws ParseException {
		Map<String, Object> loanDetails;
		List<Object> result = new ArrayList<>();
		for (LOSUserLoanDataModel losUserLoanDataModel : losUserLoanDataModellist) {
			APPLICATION_STAGE stage = APPLICATION_STAGE
					.valueOf(losUserLoanDataModel.getApplicationStage().toUpperCase());
			if (stage.equals(APPLICATION_STAGE.DISBURSED) || stage.equals(APPLICATION_STAGE.CLOSED))
				continue;
			HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(losUserLoanDataModel.getProcessInstanceId()).includeProcessVariables()
					.singleResult();
			if (processInstance != null) {
				loanDetails = getLoanDetails(stage, processInstance.getProcessVariables());
				result.add(loanDetails.getOrDefault("loanDetails", "{}"));
			}
		}
		return result;
	}

	/**
	 * This functions return the list of loan details for the given user.
	 * 
	 * @param losUserLoanDataModellist
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	List<Object> getMyLoanDetails(List<LOSUserLoanDataModel> losUserLoanDataModellist) throws ParseException {
		Map<String, Object> loanDetails;
		List<Object> result = new ArrayList<>();
		for (LOSUserLoanDataModel losUserLoanDataModel : losUserLoanDataModellist) {
			APPLICATION_STAGE stage = APPLICATION_STAGE
					.valueOf(losUserLoanDataModel.getApplicationStage().toUpperCase());
			if (!stage.equals(APPLICATION_STAGE.DISBURSED) && !stage.equals(APPLICATION_STAGE.CLOSED))
				continue;
			HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(losUserLoanDataModel.getProcessInstanceId()).includeProcessVariables()
					.singleResult();
			if (processInstance != null) {
				loanDetails = getLoanDetails(stage, processInstance.getProcessVariables());
				result.add(loanDetails.getOrDefault("loanDetails", "{}"));
			}
		}
		return result;
	}

	/**
	 * This functions return the details of a single loan for the given user.
	 * 
	 * @param stage
	 * @param processVariables
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	Map<String, Object> getLoanDetails(APPLICATION_STAGE stage, Map<String, Object> processVariables)
			throws ParseException {
		Map<String, Object> data = new HashMap<>();
		switch (stage) {
		case APPROVED:
			data.put("loanDetails", getLoanDetailsForApproved(stage, processVariables));
			break;
		case REJECTED:
		case NEW:
		case CANCELLED:
		case EXPIRED:
			data.put("loanDetails", getLoanDetailsForExpired(stage, processVariables));
			break;
		case CLOSED:
			data.put("loanDetails", getLoanDetailsForClosed(stage, processVariables));
			break;
		case DISBURSED:
			data.put("loanDetails", getLoanDetailsForDisbursed(stage, processVariables));
			break;
		}
		return data;
	}

	/**
	 * This functions return the details of a single approved loan for the given
	 * user.
	 *
	 * @param processVariables
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	CustomerDashboardApplicationApproved getLoanDetailsForApproved(APPLICATION_STAGE applicationStatus,
			Map<String, Object> processVariables) {
		CustomerDashboardApplicationApproved applicationApproved = new CustomerDashboardApplicationApproved();
		applicationApproved.setDisbursementDate(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_DATE)));
		applicationApproved.setDisbursementMethod("");

		applicationApproved.setEmi(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_EMI_KEY)));
		applicationApproved.setLoanAmount(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
		applicationApproved.setSecurityCode("");
		applicationApproved
				.setTenure(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_TENURE)));
		applicationApproved.setLoanContractNumber(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.CONTRACT_ID_KEY)));
		applicationApproved.setApplicationStatus(CommonHelperFunctions.getStringValue(applicationStatus));
		applicationApproved.setLoanContractUrl("");
		Date startDate = new Date(CommonHelperFunctions.getLongValue(processVariables.get(Constants.START_TIME_KEY)));
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		applicationApproved.setApplicationDate(df.format(startDate));
		return applicationApproved;
	}

	/**
	 * This functions return the details of a single closed loan for the given user.
	 *
	 * @param processVariables
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	CustomerDashboardApplicationClosed getLoanDetailsForClosed(APPLICATION_STAGE applicationStatus,
			Map<String, Object> processVariables) {
		CustomerDashboardApplicationClosed applicationClosed = new CustomerDashboardApplicationClosed();
		applicationClosed.setEmi(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_EMI_KEY)));
		applicationClosed.setLoanAmount(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
		applicationClosed
				.setTenure(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_TENURE)));
		applicationClosed.setApplicationStatus(CommonHelperFunctions.getStringValue(applicationStatus));
		applicationClosed.setLoanContractUrl("");
		applicationClosed.setLoanContractNumber(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.CONTRACT_ID_KEY)));
		Date startDate = new Date(CommonHelperFunctions.getLongValue(processVariables.get(Constants.START_TIME_KEY)));
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		applicationClosed.setApplicationDate(df.format(startDate));
		return applicationClosed;
	}

	/**
	 * This functions return the details of a single expired loan for the given
	 * user.
	 *
	 * @param processVariables
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	CustomerDashboardApplicationExpired getLoanDetailsForExpired(APPLICATION_STAGE applicationStatus,
			Map<String, Object> processVariables) {
		CustomerDashboardApplicationExpired applicationExpired = new CustomerDashboardApplicationExpired();
		applicationExpired.setEmi(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_EMI_KEY)));
		applicationExpired.setLoanAmount(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
		applicationExpired
				.setTenure(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_TENURE)));
		applicationExpired.setApplicationStatus(CommonHelperFunctions.getStringValue(applicationStatus));
		applicationExpired.setLoanContractUrl("");
		applicationExpired.setLoanContractNumber(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.CONTRACT_ID_KEY)));
		Date startDate = new Date(CommonHelperFunctions.getLongValue(processVariables.get(Constants.START_TIME_KEY)));

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		applicationExpired.setApplicationDate(df.format(startDate));
		return applicationExpired;
	}

	/**
	 * This functions return the details of a single disbursed loan for the given
	 * user.
	 *
	 * @param processVariables
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	CustomerDashboardApplicationDisbursed getLoanDetailsForDisbursed(APPLICATION_STAGE applicationStatus,
			Map<String, Object> processVariables) throws ParseException {
		CustomerDashboardApplicationDisbursed applicationDisbursed = new CustomerDashboardApplicationDisbursed();

		applicationDisbursed.setLoanAmount(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_AMOUNT_APPROVED)));
		applicationDisbursed
				.setTenure(CommonHelperFunctions.getStringValue(processVariables.get(Constants.FINAL_LOAN_TENURE)));
		applicationDisbursed.setApplicationStatus(CommonHelperFunctions.getStringValue(applicationStatus));
		applicationDisbursed.setLoanContractNumber(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.CONTRACT_ID_KEY)));
		applicationDisbursed.setLoanContractUrl(CommonHelperFunctions.getStringValue(""));
		Date startDate = new Date(CommonHelperFunctions.getLongValue(processVariables.get(Constants.START_TIME_KEY)));
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		applicationDisbursed.setApplicationDate(df.format(startDate));
		applicationDisbursed.setDisbursementDate(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_DATE)));
		applicationDisbursed.setDisbursementMethod(
				CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_CHANNEL)));
		applicationDisbursed.setRepaymentScheduleUrl("");

		// TODO LMS Integration
		Map<String, Object> lmsData = new HashMap<>();

		Long emiAmount = CommonHelperFunctions
				.getLongValue(CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_MONTHLY_INSTALLMENT_KEY)));

		applicationDisbursed.setEmi(CommonHelperFunctions.getStringValue(emiAmount));

		if (lmsData.containsKey(Constants.LMS_REMAINING_TENURE_KEY)) {
			applicationDisbursed.setRemainingTenure(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_REMAINING_TENURE_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_UPCOMING_PAYMENT_DATE_KEY)) {
			applicationDisbursed.setNextRepaymentDate(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_UPCOMING_PAYMENT_DATE_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_OUTSTANDING_AMOUNT_KEY)) {
			applicationDisbursed.setOutstandingLoanAmount(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_OUTSTANDING_AMOUNT_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_TOTAL_OVERDUE_AMOUNT_KEY)) {
			applicationDisbursed.setTotalOverdueLoanAmount(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_TOTAL_OVERDUE_AMOUNT_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_OVERDUE_AMOUNT_KEY)) {
			applicationDisbursed.setOverdueLoanAmount(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_OVERDUE_AMOUNT_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_OVERDUE_FEE_AMOUNT_KEY)) {
			applicationDisbursed.setOverdueFeeAmount(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_OVERDUE_FEE_AMOUNT_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_NO_OF_MONTHS_OVERDUE_KEY)) {
			applicationDisbursed.setNoOfMonthsOverdue(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_NO_OF_MONTHS_OVERDUE_KEY)));
		}
		if (lmsData.containsKey(Constants.LMS_PAYMENT_HISTORY_KEY)) {
			applicationDisbursed.setPaymentHistory((ArrayList<Object>) lmsData.get(Constants.LMS_PAYMENT_HISTORY_KEY));
		}
		if (lmsData.containsKey(Constants.LMS_REPAYMENTS_KEY)) {
			applicationDisbursed.setRepaymentInfo((ArrayList<Object>) lmsData.get(Constants.LMS_REPAYMENTS_KEY));
		}

		if (lmsData.containsKey(Constants.LMS_TOTAL_PAYMENT_AMOUNT_KEY)) {
			Long totalPaymentAmount = CommonHelperFunctions
					.getLongValue(lmsData.get(Constants.LMS_TOTAL_PAYMENT_AMOUNT_KEY))
					+ CommonHelperFunctions.getLongValue(lmsData.get(Constants.LMS_TOTAL_OVERDUE_AMOUNT_KEY));
			applicationDisbursed.setTotalPaymentAmount(CommonHelperFunctions.getStringValue(totalPaymentAmount));

		}
		if (lmsData.containsKey(Constants.LMS_NO_OF_MONTHS_OVERDUE_KEY)) {
			applicationDisbursed.setNoOfMonthsOverdue(
					CommonHelperFunctions.getStringValue(lmsData.get(Constants.LMS_NO_OF_MONTHS_OVERDUE_KEY)));
		}
		return applicationDisbursed;
	}

}
