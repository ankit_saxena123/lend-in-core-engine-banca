package com.kuliza.lending.journey.lms.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("LAPDemoService")
public class LoanAgainstPropertyService extends CommonDemoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoanAgainstPropertyService.class);

    @Autowired
    public LoanAgainstPropertyService(RuntimeService runtimeService) {
        super(runtimeService);
    }


    public DelegateExecution createClientApiIntegration(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("address", new JSONArray());
        requestBody.put("familyMembers", new JSONArray());
        requestBody.put("officeId", this.defaultOfficeId);
        requestBody.put("firstname", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "firstNameApplicant")));
        requestBody.put("lastname", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "lastNameApplicant")));
        requestBody.put("genderId", 22);
        requestBody.put("active", false);
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("activationDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("savingsProductId", null);
        String clientId;
        try{

            LOGGER.debug("request body createClientApiIntegration: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+ "clients")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("createClientApiIntegration-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("createClientApiIntegration api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            clientId = body.optString("clientId");

            runtimeService.setVariable(rootProcessInstanceId, "clientIdFromLMS", clientId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }

        this.createDatatableForClient(execution, clientId);
        this.activateClient(execution, clientId);

        return execution;
    }




    public void createDatatable(DelegateExecution execution) throws Exception{


        String rootProcessInstanceId = execution.getRootProcessInstanceId();
        Map<String, Object> processVariables = runtimeService.getVariables(rootProcessInstanceId);

        String cId = String.valueOf(processVariables.getOrDefault("clientIdFromLMS", ""));
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("Current Primary Bank Account No", String.valueOf(processVariables.getOrDefault("currentAccountNumber", "")));
        requestBody.put("Current Account IFSC Code", String.valueOf(processVariables.getOrDefault("currentAccountIfsc", "")));
        requestBody.put("Current Account Bank Name", String.valueOf(processVariables.getOrDefault("currentAccountBankName", "")));
        requestBody.put("Current Account Branch", String.valueOf(processVariables.getOrDefault("currentAccountBranch", "")));
        requestBody.put("Cibil Score", String.valueOf(processVariables.getOrDefault("cibilScoreApplicant", "")));
        requestBody.put("Current Address Line 1", String.valueOf(processVariables.getOrDefault("addressLine1CAApplicant", "")));
        requestBody.put("Current Address Line 2", String.valueOf(processVariables.getOrDefault("addressLine2CAApplicant", "")));
        requestBody.put("Current Landmark", String.valueOf(processVariables.getOrDefault("landmarkCAApplicant", "")));
        requestBody.put("Current Pincode", String.valueOf(processVariables.getOrDefault("pinCodeCAApplicant", "")));
        requestBody.put("Current State", String.valueOf(processVariables.getOrDefault("stateCAApplicant", "")));
        requestBody.put("Current City", String.valueOf(processVariables.getOrDefault("cityCAApplicant", "")));
        requestBody.put("PAN", String.valueOf(processVariables.getOrDefault("panApplicant", "")));
        requestBody.put("Aadhaar Number", String.valueOf(processVariables.getOrDefault("aadhaarApplicant", "")));
        requestBody.put("Name of Business", String.valueOf(processVariables.getOrDefault("nameOfBusiness", "")));

        requestBody.put("Age of Business in years", String.valueOf(processVariables.getOrDefault("ageOfBusiness", "")));
        requestBody.put("Type of Property", String.valueOf(processVariables.getOrDefault("propertyType", "")));
        requestBody.put("Built-up Area in sq mtrs", String.valueOf(processVariables.getOrDefault("builtupAreaProperty", "")));
        requestBody.put("Age of Property", String.valueOf(processVariables.getOrDefault("ageOfProperty", "")));
        requestBody.put("Address of Property Line 1", String.valueOf(processVariables.getOrDefault("addressOfPropertyLine1", "")));
        requestBody.put("Address of Property Line 2", String.valueOf(processVariables.getOrDefault("addressOfPropertyLine2", "")));
        requestBody.put("Address of Property PIN Code", String.valueOf(processVariables.getOrDefault("landmarkProperty", "")));
        requestBody.put("Address of Property City", String.valueOf(processVariables.getOrDefault("cityProperty", "")));
        requestBody.put("Address of Property State", String.valueOf(processVariables.getOrDefault("stateProperty", "")));
        requestBody.put("Estimated Worth of Property", String.valueOf(processVariables.getOrDefault("estimatedWorthOfProperty", "")));
        requestBody.put("Designation", String.valueOf(processVariables.getOrDefault("designationApplicant", "")));
        requestBody.put("Employee ID", String.valueOf(processVariables.getOrDefault("employeeIdApplicant", "")));
        requestBody.put("Upload Identity Proof", String.valueOf(processVariables.getOrDefault("uploadIdProofPartner1", "")));
        requestBody.put("Upload Address Proof", String.valueOf(processVariables.getOrDefault("uploadAddressProofPartner1", "")));

        try{
            LOGGER.debug("request body createDatatableDataForClient: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"datatables/Loan Against Property Additional Information 1/"
                    + String.valueOf(cId))
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("createDatatableDataForClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }


    public DelegateExecution createLoanApiIntegration(DelegateExecution execution) throws Exception{
        String rootProcessInstanceId = execution.getRootProcessInstanceId();
        String loanAmount1 = String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount1"));
        String loanAmount2 = String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount2"));
        if(!StringUtils.isEmpty(loanAmount1))
            createLoanAgainstPropertyApiIntegration(execution);
        if(!StringUtils.isEmpty(loanAmount2))
            createPersonalLoanApiIntegration(execution);
        return execution;
    }



    public DelegateExecution createLoanAgainstPropertyApiIntegration(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("clientId", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "clientIdFromLMS")));
        requestBody.put("productId", 22);
        requestBody.put("disbursementData", new JSONArray());
        requestBody.put("principal",  String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount1")));
        requestBody.put("loanTermFrequency", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure1")));
        requestBody.put("loanTermFrequencyType", 2);
        requestBody.put("numberOfRepayments", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure1")));
        requestBody.put("repaymentEvery", 1);
        requestBody.put("repaymentFrequencyType", 2);
        requestBody.put("interestRatePerPeriod", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "roi1")));
        requestBody.put("amortizationType", 1);
        requestBody.put("isEqualAmortization", false);
        requestBody.put("interestType", 0);
        requestBody.put("interestCalculationPeriodType", 1);
        requestBody.put("allowPartialPeriodInterestCalcualtion", true);
        requestBody.put("transactionProcessingStrategyId", 1);
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("loanType", "individual");
        requestBody.put("expectedDisbursementDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));


        try{
            LOGGER.debug("request body LMS: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"loans")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("LMS-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            String loanId = body.optString("loanId");

            runtimeService.setVariable(rootProcessInstanceId, "loanIdFromLMS", loanId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }


        return execution;

    }


    public DelegateExecution createPersonalLoanApiIntegration(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("clientId", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "clientIdFromLMS")));
        requestBody.put("productId", 17);
        requestBody.put("disbursementData", new JSONArray());
        requestBody.put("principal",  String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount2")));
        requestBody.put("loanTermFrequency", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure2")));
        requestBody.put("loanTermFrequencyType", 2);
        requestBody.put("numberOfRepayments", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure2")));
        requestBody.put("repaymentEvery", 1);
        requestBody.put("repaymentFrequencyType", 2);
        requestBody.put("interestRatePerPeriod", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "roi2")));
        requestBody.put("amortizationType", 1);
        requestBody.put("isEqualAmortization", false);
        requestBody.put("interestType", 0);
        requestBody.put("interestCalculationPeriodType", 1);
        requestBody.put("allowPartialPeriodInterestCalcualtion", true);
        requestBody.put("transactionProcessingStrategyId", 1);
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("loanType", "individual");
        requestBody.put("expectedDisbursementDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        try{
            LOGGER.debug("request body LMS: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"loans")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            LOGGER.debug("LMS-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            String loanId = body.optString("loanId");

            runtimeService.setVariable(rootProcessInstanceId, "loanIdFromLMS", loanId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
        return execution;

    }
}
