package com.kuliza.lending.journey.pojo;

import java.util.Map;

public class SubmitFormClass {

	private String taskId;
	private String processInstanceId;
	private Map<String, Object> formProperties;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public Map<String, Object> getFormProperties() {

		return formProperties;
	}

	public void setFormProperties(Map<String, Object> formProperties) {
		this.formProperties = formProperties;
	}

	public SubmitFormClass(String taskId, String processInstanceId, Map<String, Object> formProperties) {
		super();
		this.taskId = taskId;
		this.formProperties = formProperties;
		this.processInstanceId = processInstanceId;
	}

	public SubmitFormClass() {
	}
}