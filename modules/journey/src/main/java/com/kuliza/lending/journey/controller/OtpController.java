package com.kuliza.lending.journey.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.OtpLoginInputform;
import com.kuliza.lending.journey.service.OtpServices;

@RestController
@RequestMapping("/otp")
public class OtpController {

	private final Logger logger = LoggerFactory.getLogger(OtpController.class);

	@Autowired
	public OtpServices otpService;

	@RequestMapping(method = RequestMethod.POST, value = "/generate-otp")
	public ApiResponse generateOtpController(HttpServletRequest request,
			@RequestBody @Valid CustomerLoginInput customerLoginInput) {
		logger.info("  Mobile Number : " + customerLoginInput.getMobile());
		try {
			return otpService.generateOTP(request, customerLoginInput);
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/validate-otp")
	public ApiResponse validateOtpController(HttpServletRequest request,
			@RequestBody @Valid OtpLoginInputform otpLoginInputForm) {
		logger.info(
				" Otp Number : " + otpLoginInputForm.getOtp() + "  Mobile Number : " + otpLoginInputForm.getMobile());
		try {
			return otpService.validateOtpAndCreateOrUpdateUser(request, otpLoginInputForm, false);
		} catch (Exception e) {
			logger.error(CommonHelperFunctions.getStackTrace(e));
			return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.INTERNAL_SERVER_ERROR_MESSAGE);
		}
	}

}
