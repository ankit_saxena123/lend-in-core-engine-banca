package com.kuliza.lending.journey.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.form.model.FormField;
import org.flowable.form.model.Option;
import org.flowable.form.model.OptionFormField;
import org.flowable.task.api.Task;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

import com.kuliza.lending.common.pojo.ExtendedApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.StaticContextAccessor;
import com.kuliza.lending.journey.URLConfiguratorComponent;

public class HelperFunctions extends CommonHelperFunctions {

	public static String replaceServiceNameWithUrl(String url) {
		/*// Integration Broker URL Replacement
		url = url.replace(
				StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
						+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
								.getIntegrationBrokerServiceName()
						+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getEndServiceNameSymbol(),
				StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMockIntegrationBrokerIpAddress() + ":"
						+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMockIntegrationBrokerPort());*/

		// DMS URL Replacement
		url = url
				.replace(
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getDMSServiceName()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
										.getEndServiceNameSymbol(),

						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getDmsIpAddress() + ":"
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getDmsPort());

		/*// Masters URL Replacement
		url = url
				.replace(
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMastersServiceName()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
										.getEndServiceNameSymbol(),
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMastersIpAddress() + ":"
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getMastersPort());*/

		// Backend URL Replacement
		/*url = url
				.replace(
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getStartServiceNameSymbol()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getBackendServiceName()
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class)
										.getEndServiceNameSymbol(),
						StaticContextAccessor.getBean(URLConfiguratorComponent.class).getBackendIpAddress() + ":"
								+ StaticContextAccessor.getBean(URLConfiguratorComponent.class).getBackendPort());*/
		return url;
	}

	public static Map<String, Object> getPresentScreenInfo(Map<String, FormField> formFields) throws Exception {
		Map<String, Object> formData = new HashMap<>();
		for (Map.Entry<String, FormField> entry : formFields.entrySet()) {
			FormField property = entry.getValue();
			Map<String, Object> tempMap = new HashMap<>();
			if (property.getType().equals("dropdown") || property.getType().equals("radio-buttons")) {
				OptionFormField property2 = (OptionFormField) property;
				List<Option> options = property2.getOptions();
				if (options.get(0).getId() == null) {
					options.remove(0);
				}
				tempMap.put("enumValues", options);
			} else {
				tempMap.put("enumValues", null);
			}
			tempMap.put("name", property.getName());
			tempMap.put("id", property.getId());
			tempMap.put("isWritable", !property.isReadOnly());
			tempMap.put("isRequired", property.isRequired());
			tempMap.put("value", property.getValue());
			tempMap.put("type", property.getType());
			Map<String, Object> params = property.getParams();
			Map<String, Object> metaData = (Map<String, Object>) params.getOrDefault("meta", new HashMap<>());
			if ((CommonHelperFunctions.getStringValue(metaData.get("type")).equals("rest")
					|| CommonHelperFunctions.getStringValue(metaData.get("form_type")).equals("rest-dropdown")
					|| CommonHelperFunctions.getStringValue(metaData.get("form_type")).equals("upload")
					|| CommonHelperFunctions.getStringValue(metaData.get("type")).equals("upload"))
					&& metaData.get("api") instanceof Map) {
				Map<String, Object> apiData = (Map<String, Object>) metaData.getOrDefault("api", new HashMap<>());
				apiData.put("url", replaceServiceNameWithUrl(CommonHelperFunctions.getStringValue(apiData.get("url"))));
				metaData.put("api", apiData);
			} else if ((CommonHelperFunctions.getStringValue(metaData.get("type")).equals("rest")
					|| CommonHelperFunctions.getStringValue(metaData.get("form_type")).equals("rest-dropdown")
					|| CommonHelperFunctions.getStringValue(metaData.get("form_type")).equals("upload")
					|| CommonHelperFunctions.getStringValue(metaData.get("type")).equals("upload"))
					&& metaData.get("api") instanceof String && metaData.get("api").toString().length() > 0) {
				JSONArray apisData = new JSONArray(metaData.get("api").toString());
				JSONArray newApisData = new JSONArray();
				for (int i = 0; i < apisData.length(); i++) {
					JSONObject apiData = apisData.getJSONObject(i);
					apiData.put("url",
							replaceServiceNameWithUrl(CommonHelperFunctions.getStringValue(apiData.get("url"))));
					newApisData.put(apiData);
				}
				metaData.put("api", newApisData.toString());
			}
			params.put("meta", metaData);
			tempMap.put("params", params);
			formData.put(property.getId(), tempMap);
		}
		return formData;
	}

	public static ExtendedApiResponse makeResponse(HttpStatus status, String message, String processInstanceId,
			Task task, Map<String, Object> data, Map<String, FormField> formFields) throws Exception {
		String taskDefinitionKey = task.getTaskDefinitionKey();
		String taskId = task.getId();
		Map<String, Object> formData = getPresentScreenInfo(formFields);
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("taskId", taskId);
		responseData.put("processInstanceId", processInstanceId);
		responseData.put("data", data);
		responseData.put(taskDefinitionKey, formData);
		responseData.put("stepperData", task.getDescription());
		return new ExtendedApiResponse(status, message, responseData, taskDefinitionKey, task.getName(), "");
	}

}
