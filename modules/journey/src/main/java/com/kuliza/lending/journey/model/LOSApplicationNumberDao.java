package com.kuliza.lending.journey.model;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface LOSApplicationNumberDao extends CrudRepository<LOSApplicationNumber, Long> {

	public LOSApplicationNumber findById(long id);

	public LOSApplicationNumber findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<LOSApplicationNumber> findAll();

}